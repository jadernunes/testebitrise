//
//  LayoutManager.h
//  Example
//
//  Created by Luciano Acosta on 27/04/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LayoutManager : NSObject

@property (nonatomic, strong) UIColor *primaryColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *red;
@property (nonatomic, strong) UIColor *errorColor;
@property (nonatomic, strong) UIColor *darkGray;
@property (nonatomic, strong) UIColor *lightGray;
@property (nonatomic, strong) UIColor *darkGreen;
@property (nonatomic, strong) UIColor *lightGreen;
@property (nonatomic, strong) UIColor *gradientColor;

@property (assign) CGFloat            miniFontSize;
@property (assign) CGFloat            regularFontSize;
@property (assign) CGFloat            titleFontSize;
@property (assign) CGFloat            subTitleFontSize;
@property (assign) CGFloat            navigationTitleFontSize;

//Subscription status color
@property (nonatomic, strong) UIColor *status_undefined;
@property (nonatomic, strong) UIColor *status_paidOut;
@property (nonatomic, strong) UIColor *status_awaitingPayment;
@property (nonatomic, strong) UIColor *status_paymentDenied;
@property (nonatomic, strong) UIColor *status_canceled;
@property (nonatomic, strong) UIColor *status_unApprovedPayment;
@property (nonatomic, strong) UIColor *status_awaitingReversal;
@property (nonatomic, strong) UIColor *status_reversal;
@property (nonatomic, strong) UIColor *status_awaitingcontested;
@property (nonatomic, strong) UIColor *status_contested;
@property (nonatomic, strong) UIColor *status_processing;
@property (nonatomic, strong) UIColor *status_unApprovedReversal;


+ (instancetype)sharedManager;
- (UIFont *)fontWithSize:(CGFloat)size;
- (UIFont *)boldFontWithSize:(CGFloat)size;

@end
