//
//  LayoutManager.m
//  Example
//
//  Created by Luciano Acosta on 27/04/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "LayoutManager.h"
#import "UIColor+HexString.h"

@implementation LayoutManager

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.primaryColor       = [UIColor colorWithRed:95.0/255.0 green:183/255.0 blue:63.0/255.0 alpha:1.0];
        self.backgroundColor    = [UIColor whiteColor];
        self.red                = [UIColor colorWithHexString:@"#d33031"];
        self.errorColor         = [UIColor colorWithHexString:@"#D20300"];
        self.darkGray           = [UIColor colorWithHexString:@"#777777"];
        self.lightGray          = [UIColor colorWithHexString:@"#d3d3d3"];
        self.darkGreen          = [UIColor colorWithHexString:@"#3d8438"];
        self.lightGreen         = [UIColor colorWithHexString:@"#4fa444"];
        self.gradientColor      = [UIColor colorWithHexString:@"#12B6B6"];
        self.miniFontSize       = 11.0;
        self.regularFontSize    = 16.0;
        self.titleFontSize      = 24.0;
        self.subTitleFontSize   = 18.0;
        self.navigationTitleFontSize    = 18.0;
        
        //Subscription status color
        self.status_undefined           = [UIColor colorWithHexString:@"#999999"];
        self.status_paidOut             = [UIColor colorWithHexString:@"#739e73"];
        self.status_awaitingPayment     = [UIColor colorWithHexString:@"#c79121"];
        self.status_paymentDenied       = [UIColor colorWithHexString:@"#E9573E"];
        self.status_canceled            = [UIColor colorWithHexString:@"#D46A6A"];
        self.status_unApprovedPayment   = [UIColor colorWithHexString:@"#a90329"];
        self.status_awaitingReversal    = [UIColor colorWithHexString:@"#AC92ED"];
        self.status_reversal            = [UIColor colorWithHexString:@"#764B8E"];
        self.status_awaitingcontested   = [UIColor colorWithHexString:@"#EC87BF"];
        self.status_contested           = [UIColor colorWithHexString:@"#D86FAF"];
        self.status_processing          = [UIColor colorWithHexString:@"#57889c"];
        self.status_unApprovedReversal  = [UIColor colorWithHexString:@"#482996"];
        
    }
    
    return self;
}

+ (instancetype)sharedManager {
    static LayoutManager *sharedUser = nil;
    
    @synchronized(self) {
        if (sharedUser == nil)
            sharedUser = [[self alloc] init];
    }
    
    return sharedUser;
}

- (UIFont *)fontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Dosis-Regular" size:size];
}

- (UIFont *)boldFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"Dosis-Bold" size:size];
}

@end
