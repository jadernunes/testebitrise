//
//  UIImage+Color.h
//  Example
//
//  Created by Adriano Soares on 07/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

- (UIImage *)withColor:(UIColor *)color;

@end
