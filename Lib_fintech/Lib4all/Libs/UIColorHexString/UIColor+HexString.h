//
//  UIColor+HexString.h
//  Example
//
//  Created by Cristiano Matte on 05/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#ifndef UIColor_HexString_h
#define UIColor_HexString_h

@interface UIColor(HexString)

+ (UIColor *) colorWithHexString: (NSString *) hexString;

@end

#endif /* UIColor_HexString_h */
