//
//  UIFloatLabelTextField+Border.h
//  Example
//
//  Created by Adriano Soares on 13/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIFloatLabelTextField.h"

@interface UIFloatLabelTextField (Border)

- (void) setBottomBorderWithColor:(UIColor *) color;

- (void) showFieldWithError:(BOOL) showError;


@end
