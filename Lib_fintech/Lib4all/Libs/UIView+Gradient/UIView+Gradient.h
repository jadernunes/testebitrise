//
//  UIButton+Color.h
//  Example
//
//  Created by Adriano Soares on 31/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Gradient)

-(CAGradientLayer *)setGradientFromColor:(UIColor *)firstColor toColor:(UIColor *)secondColor;

@end
