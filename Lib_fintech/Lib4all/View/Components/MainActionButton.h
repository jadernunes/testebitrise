//
//  MainActionButton.h
//  Example
//
//  Created by Cristiano Matte on 13/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MainActionButton : UIButton

@end
