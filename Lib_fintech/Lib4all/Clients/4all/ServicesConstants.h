//
//  ServicesConstants.h
//  Example
//
//  Created by Cristiano Matte on 04/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#ifndef ServicesConstants_h
#define ServicesConstants_h

/*
 * URLs
 */
static NSString* const            ProductionBaseURL = @"https://conta.api.4all.com";
static NSString* const PrepareCardProductionBaseURL = @"https://vault.api.4all.com";
static NSString* const               HomologBaseURL = @"https://conta.homolog-interna.4all.com";
static NSString* const    PrepareCardHomologBaseURL = @"https://vault.homolog-interna.4all.com";
static NSString* const                  TestBaseURL = @"https://conta.test.4all.com";
static NSString* const       PrepareCardTestBaseURL = @"https://vault.test.4all.com";

/*
 * Server Data Formats
 */
static NSString* const DateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ssZ";

/*
 * JSON Keys
 */
static NSString* const           DeviceTypeKey = @"deviceType";
static NSString* const          DeviceModelKey = @"deviceModel";
static NSString* const            OSVersionKey = @"osVersion";
static NSString* const           IdentifierKey = @"identifier";
static NSString* const              SendSMSKey = @"sendSms";
static NSString* const         SendLoginSMSKey = @"sendLoginSms";
static NSString* const               DeviceKey = @"device";
static NSString* const          GeoLocationKey = @"geolocation";
static NSString* const           LoginTokenKey = @"loginToken";
static NSString* const            ChallengeKey = @"challenge";
static NSString* const         SessionTokenKey = @"sessionToken";
static NSString* const            SessionIDKey = @"sessionId";
static NSString* const        CreationTokenKey = @"creationToken";
static NSString* const        ApplicationIDKey = @"applicationId";
static NSString* const         EmailAddressKey = @"emailAddress";
static NSString* const   MaskedEmailAddressKey = @"maskedEmailAddress";
static NSString* const          PhoneNumberKey = @"phoneNumber";
static NSString* const          MaskedPhoneKey = @"maskedPhone";
static NSString* const         RequestVaultKey = @"requestVaultKey";
static NSString* const               AccessKey = @"accessKey";
static NSString* const             CardTypeKey = @"type";
static NSString* const              BrandIDKey = @"brandId";
static NSString* const           LastDigitsKey = @"lastDigits";
static NSString* const            IsDefaultKey = @"default";
static NSString* const             CardDataKey = @"cardData";
static NSString* const            CardNonceKey = @"cardNonce";
static NSString* const          WaitForCardKey = @"waitForCard";
static NSString* const               CardIDKey = @"cardId";
static NSString* const               StatusKey = @"status";
static NSString* const            ItemIndexKey = @"itemIndex";
static NSString* const            ItemCountKey = @"itemCount";
static NSString* const             CardListKey = @"cardList";
static NSString* const      TransactionListKey = @"transactionList";
static NSString* const     SubscriptionListKey = @"subscriptionList";
static NSString* const        TransactionIDKey = @"transactionId";
static NSString* const       SubscriptionIDKey = @"subscriptionId";
static NSString* const               AmountKey = @"amount";
static NSString* const            CreatedAtKey = @"createdAt";
static NSString* const               PaidAtKey = @"paidAt";
static NSString* const         MerchantInfoKey = @"merchantInfo";
static NSString* const             CardInfoKey = @"cardInfo";
static NSString* const         MerchantNameKey = @"name";
static NSString* const           CategoryIDKey = @"categoryId";
static NSString* const        StreetAddressKey = @"streetAddress";
static NSString* const                 CityKey = @"city";
static NSString* const                StateKey = @"state";
static NSString* const                  UrlKey = @"url";
static NSString* const                 DataKey = @"data";
static NSString* const                ErrorKey = @"error";
static NSString* const            ErrorCodeKey = @"code";
static NSString* const         ErrorMessageKey = @"message";
static NSString* const                  CPFKey = @"cpf";
static NSString* const            BirthdateKey = @"birthdate";
static NSString* const           CardNumberKey = @"cardNumber";
static NSString* const           CardholderKey = @"cardholderName";
static NSString* const       ExpirationDateKey = @"expirationDate";
static NSString* const         SecurityCodeKey = @"securityCode";
static NSString* const          PreferencesKey = @"preferences";
static NSString* const ReceivePaymentEmailsKey = @"receivePaymentEmails";
static NSString* const     PhoneChangeTokenKey = @"phoneChangeToken";
static NSString* const                  CpfKey = @"cpf";
static NSString* const             FullNameKey = @"fullName";
static NSString* const           CustomerIdKey = @"customerId";
static NSString* const         RequiredDataKey = @"requiredAccountData";
static NSString* const          LackingDataKey = @"lackingAccountData";
static NSString* const          AccountDataKey = @"accountData";
static NSString* const             EmployerKey = @"employer";
static NSString* const          JobPositionKey = @"jobPosition";
static NSString* const      RecurringAmountKey = @"recurringAmount";
static NSString* const      NextPaymentDateKey = @"nextPaymentDate";
static NSString* const             PasswordKey = @"password";
static NSString* const     RecurringBalanceKey = @"recurringBalance";
static NSString* const         IntervalTypeKey = @"intervalType";
static NSString* const        IntervalValueKey = @"intervalValue";
static NSString* const             IsSharedKey = @"shared";
static NSString* const        SharedDetailsKey = @"sharedDetails";
static NSString* const           IsProviderKey = @"sharedProvider";
static NSString* const   PendingSharedCardsKey = @"pendingSharedCards";
static NSString* const          HasPasswordKey = @"hasPassword";
static NSString* const    IsPasswordBlockedKey = @"isPasswordBlocked";
static NSString* const          OldPasswordKey = @"oldPassword";
static NSString* const          NewPasswordKey = @"newPassword";
static NSString* const      WaitForTransaction = @"waitForTransaction";
static NSString* const                 PayMode = @"paymentMode";
static NSString* const           MerchantKeyId = @"merchantKeyId";
static NSString* const         TransactionData = @"transactionData";
static NSString* const            Installments = @"installments";
static NSString* const              CouponUUID = @"couponUUID";
static NSString* const            CampaignUUID = @"campaignUUID";

/*
 * Methods
 */
static NSString* const               StartLoginMethod = @"/customer/startLogin";
static NSString* const            CompleteLoginMethod = @"/customer/completeLogin";
static NSString* const             SendLoginSMSMethod = @"/customer/sendLoginSms";
static NSString* const           SendLoginEmailMethod = @"/customer/sendLoginEmail";
static NSString* const                   LogoutMethod = @"/customer/logout";
static NSString* const    StartCustomerCreationMethod = @"/customer/startCustomerCreation";
static NSString* const CompleteCustomerCreationMethod = @"/customer/completeCustomerCreation";
static NSString* const          RequestVaultKeyMethod = @"/customer/requestVaultKey";
static NSString* const              PrepareCardMethod = @"/prepareCard";
static NSString* const                  AddCardMethod = @"/customer/addCard";
static NSString* const               DeleteCardMethod = @"customer/deleteCard";
static NSString* const           GetCardDetailsMethod = @"/customer/getCardDetails";
static NSString* const           SetDefaultCardMethod = @"/customer/setDefaultCard";
static NSString* const                ListCardsMethod = @"/customer/listCards";
static NSString* const           PayTransactionMethod = @"/customer/payTransaction";
static NSString* const           GetAccountDataMethod = @"/customer/getAccountData";
static NSString* const           SetAccountDataMethod = @"/customer/setAccountData";
static NSString* const    GetAccountPreferencesMethod = @"/customer/getAccountPreferences";
static NSString* const    SetAccountPreferencesMethod = @"/customer/setAccountPreferences";
static NSString* const       ChangeEmailAddressMethod = @"/customer/changeEmailAddress";
static NSString* const RequestEmailConfirmationMethod = @"/customer/requestEmailConfirmation";
static NSString* const           SetPhoneNumberMethod = @"/customer/setPhoneNumber";
static NSString* const       ConfirmPhoneNumberMethod = @"/customer/confirmPhoneNumber";
static NSString* const       ResendSMSChallengeMethod = @"/customer/resendSmsChallenge";
static NSString* const         ListTransactionsMethod = @"/customer/listTransactions";
static NSString* const        ListSubscriptionsMethod = @"/customer/listSubscriptions";
static NSString* const   GetSubscriptionDetailsMethod = @"/customer/getSubscriptionDetails";
static NSString* const           SetGeolocationMethod = @"/customer/setGeolocation";
static NSString* const              SetPasswordMethod = @"/customer/setPassword";
static NSString* const    StartPasswordRecoveryMethod = @"/customer/startPasswordRecovery";
static NSString* const              ValidateCpfMethod = @"/customer/validateCpf";
static NSString* const        ValidateChallengeMethod = @"/customer/validateSmsOrEmailChallenge";
static NSString* const                 PayTransaction = @"/customer/payTransaction";
static NSString* const          OfflinePayTransaction = @"/customer/offlinePayTransaction";

static NSString* const            AddSharedCardMethod = @"/customer/addSharedCard";
static NSString* const         AcceptSharedCardMethod = @"/customer/acceptSharedCard";
static NSString* const         DeleteSharedCardMethod = @"/customer/deleteSharedCard";
static NSString* const         UpdateSharedCardMethod = @"/customer/updateSharedCard";



#endif /* ServicesConstants_h */
