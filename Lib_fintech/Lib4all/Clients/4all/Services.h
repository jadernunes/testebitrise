//
//  Services.h
//  Example
//
//  Created by 4all on 4/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Lib4allPreferences.h"


@interface Services : NSObject

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSURL *prepareCardBaseURL;
@property (copy) void (^successCase)(id data);
@property (copy) void (^failureCase)(NSString *errorID, NSString * errorMessage);

- (id)init;
- (void)startLoginWithIdentifier:(NSString *)identifier;
- (void)startLoginWithIdentifier:(NSString *)identifier requiredData:(NSArray *)data;
- (void)completeLoginWithChallenge:(NSString *)challenge;
- (void)completeLoginWithChallenge:(NSString *)challenge accountData:(NSDictionary *)data;
- (void)completeLoginWithPassword:(NSString *)password;
- (void)sendLoginSms;
- (void)sendLoginEmail;
- (void)logout;
- (void)startCustomerCreationWithPhoneNumber:(NSString *)phone emailAddress:(NSString *)email;
- (void)completeCustomerCreationWithChallenge:(NSString *)challenge password:(NSString *)password;
- (void)completeCustomerCreationWithChallenge:(NSString *)challenge password:(NSString *)password accountData:(NSDictionary *)data;
- (void)requestVaultKey;
- (void)prepareCard:(NSMutableDictionary *)card;
- (void)addCardWithCardNonce:(NSString *)cardNonce;
- (void)deleteCardWithCardID:(NSString *)cardID;
- (void)getCardDetailsWithCardID:(NSString *)cardID;
- (void)setDefaultCardWithCardID:(NSString *)cardID;
- (void)listCards;
- (void)getAccountData:(NSArray *)data;
- (void)setAccountData:(NSDictionary *)data;
- (void)getAccountPreferences:(NSArray *)preferences;
- (void)setAccountPreferences:(NSDictionary *)preferences;
- (void)changeEmailAddress:(NSString *)email;
- (void)requestEmailConfirmation;
- (void)setPhoneNumber:(NSString *)phoneNumber;
- (void)confirmPhoneNumber:(NSString *)phone withChallenge:(NSString *)challenge phoneChangeToken:(NSString *)changeToken;
- (void)resendSMSChallengeForPhoneChangeToken:(NSString *)changeToken;
- (void)listTransactionsWithStartingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount;
- (void)listSubscriptionsWithStartingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount;
- (void)listTransactionsWithSubscriptionID:(NSString *)subscriptionID startingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount;
- (void)getSubscriptionDetailsWithSubscriptionID:(NSString *)subscriptionId;
- (void)setGeolocation;
- (void)setNewPassword:(NSString *)newPassword oldPassword:(NSString *)oldPassword;
- (void)startPasswordRecoveryWithIdentifier:(NSString *)identifier;
- (void) validateCpf:(NSString *)cpf;
- (void)validateSmsOrEmailWithChallenge:(NSString *)challenge;
- (void) addSharedCard:(NSString *) cardId phoneNumber:(NSString *) phoneNumber withData: (NSDictionary *) data intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue;
- (void) addSharedCard:(NSString *) cardId phoneNumber:(NSString *) phoneNumber withBalance: (NSNumber *) balance intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue;
- (void) acceptSharedCard:(NSString *) cardId;
- (void) deleteSharedCard:(NSString *) cardId custumerId:(NSString *) customerId;
- (void) updateSharedCard:(NSString *) cardId custumerId:(NSString *) custumerId withBalance: (NSNumber *) balance;
- (void) updateSharedCard:(NSString *) cardId custumerId:(NSString *) custumerId withBalance: (NSNumber *) balance intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue ;
- (void) updateSharedCard:(NSString *) cardId customerId:(NSString *) customerId withData: (NSDictionary *) data;

//Payment
-(void)payTransaction:(NSString *)sessionToken withTransactionId:(NSString *)transactionId andCardId:(NSString *)cardId payMode:(PaymentMode)payMode amount:(NSNumber *)amount installments:(NSString *)installments waitForTransaction:(BOOL)waitForTransaction;
//Offline Payment
-(void)offlinePayTransaction:(NSString *)sessionToken withTransactionId:(NSString *)transactionId andCardId:(NSString *)cardId payMode:(PaymentMode)payMode amount:(NSNumber *)amount installments:(NSString *)installments cupomUIID:(NSString *)cupomUIID campaignUUID:(NSString *)campaignUUID merchantKeyId:(NSString *)merchantKeyId blob:(NSString *)blob waitForTransaction:(BOOL)waitForTransaction;


@end
