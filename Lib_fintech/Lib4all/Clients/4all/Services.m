    //
//  Services.m
//  Example
//
//  Created by 4all on 4/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "Services.h"
#import "Lib4all.h"
#import "LocationManager.h"
#import "ServicesConstants.h"
#import "User.h"
#import "CreditCardsList.h"
#import "CreditCard.h"
#import "Transaction.h"
#import "Subscription.h"
#import "Preferences.h"
#import "PersistenceHelper.h"
#import "Lib4allPreferences.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface Services ()

@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;

@end


@implementation Services

- (id)init {
    self = [super init];
  
    if(self) {
        NSString *baseURLString;
        NSString *prepareCardBaseURLString;

        switch ([Lib4allPreferences sharedInstance].environment) {
            case EnvironmentTest:
                baseURLString = TestBaseURL;
                prepareCardBaseURLString = PrepareCardTestBaseURL;
                break;
            case EnvironmentHomologation:
                baseURLString = HomologBaseURL;
                prepareCardBaseURLString = PrepareCardHomologBaseURL;
                break;
            case EnvironmentProduction:
                baseURLString = ProductionBaseURL;
                prepareCardBaseURLString = PrepareCardProductionBaseURL;
                break; 
        }

        self.baseURL = [NSURL URLWithString:baseURLString];
        self.prepareCardBaseURL = [NSURL URLWithString:prepareCardBaseURLString];

        self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:self.baseURL];
        self.manager.requestSerializer  = [AFJSONRequestSerializer serializer];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.manager.requestSerializer.timeoutInterval = 60;
    }
    
    return self;
}

- (void)startLoginWithIdentifier:(NSString *)identifier {
    [self startLoginWithIdentifier:identifier requiredData:nil];
}

- (void)startLoginWithIdentifier:(NSString *)identifier requiredData:(NSArray *)data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:identifier forKey:IdentifierKey];
    [parameters setValue:@NO forKey:SendSMSKey];
    
    if (data != nil) {
        [parameters setObject:data forKey:RequiredDataKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    NSString *applicationID = [[Lib4allPreferences sharedInstance] applicationID];
    if (applicationID != nil) {
        [parameters setValue:applicationID forKey:ApplicationIDKey];
    }
    
    NSMutableDictionary *device = [[NSMutableDictionary alloc] init];
    UIDevice *currentDevice = [UIDevice currentDevice];
    [device setValue:@1 forKey:DeviceTypeKey];
    [device setValue:currentDevice.localizedModel forKey:DeviceModelKey];
    [device setValue:currentDevice.systemVersion forKey:OSVersionKey];
    [parameters setObject:device forKey:DeviceKey];
    
    [self.manager POST:StartLoginMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
       
        User *sharedUser = [User sharedUser];
        if (sharedUser.currentState != UserStateOnCreation) {
            // Garante que os dados do usuário anterior foram removidos
            [[PersistenceHelper sharedHelper] removeEntities];
            sharedUser.currentState = UserStateOnLogin;
            sharedUser.token = [responseObject valueForKey:LoginTokenKey];
        }
        
        sharedUser.maskedPhone = [responseObject valueForKey:MaskedPhoneKey];
        sharedUser.maskedEmail = [responseObject valueForKey:MaskedEmailAddressKey];
        sharedUser.hasPassword = [[responseObject valueForKey:HasPasswordKey] boolValue];
        sharedUser.isPasswordBlocked = [[responseObject valueForKey:IsPasswordBlockedKey] boolValue];
        
        
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)completeLoginWithChallenge:(NSString *)challenge {
    [self completeLoginWithChallenge:challenge accountData:nil];
}

- (void)completeLoginWithChallenge:(NSString *)challenge accountData:(NSDictionary *)data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:LoginTokenKey];
    [parameters setValue:challenge forKey:ChallengeKey];
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    if (data != nil) {
        [parameters setObject:data forKey:AccountDataKey];
    }
    
    [self.manager POST:CompleteLoginMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        User *sharedUser = [User sharedUser];
        sharedUser.currentState = UserStateLoggedIn;
        sharedUser.token = [responseObject valueForKey:SessionTokenKey];
        sharedUser.sessionId = [responseObject valueForKey:SessionIDKey];
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)completeLoginWithPassword:(NSString *)password {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:LoginTokenKey];
    [parameters setValue:password forKey:PasswordKey];
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:CompleteLoginMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        User *sharedUser = [User sharedUser];
        sharedUser.currentState = UserStateLoggedIn;
        sharedUser.token = [responseObject valueForKey:SessionTokenKey];
        sharedUser.sessionId = [responseObject valueForKey:SessionIDKey];
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)sendLoginSms {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    User *sharedUser = [User sharedUser];
    
    if (sharedUser.currentState == UserStateOnLogin) {
        [parameters setValue:sharedUser.token forKey:LoginTokenKey];
    } else if (sharedUser.currentState == UserStateOnCreation) {
        [parameters setValue:sharedUser.token forKey:CreationTokenKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SendLoginSMSMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)sendLoginEmail {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:LoginTokenKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SendLoginEmailMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)logout {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];

    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    // O logout local é feito tanto em caso de sucesso quanto em caso de falha na requisição ao servidor
    [[User sharedUser] remove];
    [[CreditCardsList sharedList] remove];
    [[Preferences sharedPreferences] remove];

    
    [self.manager POST:LogoutMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)startCustomerCreationWithPhoneNumber:(NSString *)phone emailAddress:(NSString *)email {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *device = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:phone forKey:PhoneNumberKey];
    
    if (email != nil) {
        [parameters setValue:email forKey:EmailAddressKey];
    }
    [parameters setValue:@YES forKey:SendSMSKey];
    
    UIDevice *myDevice = [UIDevice currentDevice];
    [device setValue:@1 forKey:DeviceTypeKey];
    [device setValue:myDevice.localizedModel forKey:DeviceModelKey];
    [device setValue:myDevice.systemVersion forKey:OSVersionKey];
    [parameters setObject:device forKey:DeviceKey];
    
    NSString *applicationID = [[Lib4allPreferences sharedInstance] applicationID];
    if (applicationID != nil) {
        [parameters setValue:applicationID forKey:ApplicationIDKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:StartCustomerCreationMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        
        User *sharedUser = [User sharedUser];
        sharedUser.currentState = UserStateOnCreation;
        sharedUser.token = [response valueForKey:CreationTokenKey];
        sharedUser.accessKey = [response valueForKey:AccessKey];
        
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)completeCustomerCreationWithChallenge:(NSString *)challenge password:(NSString *)password {
    [self completeCustomerCreationWithChallenge:challenge password:password accountData:nil];
}

- (void)completeCustomerCreationWithChallenge:(NSString *)challenge password:(NSString *)password accountData:(NSDictionary *)data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:CreationTokenKey];
    if (challenge != nil) {
        [parameters setValue:challenge forKey:ChallengeKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    if (data != nil) {
        [parameters setObject:data forKey:AccountDataKey];
    }
    if (password != nil) {
        [parameters setObject:password forKey:PasswordKey];
    }
    
    [self.manager POST:CompleteCustomerCreationMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        User *sharedUser = [User sharedUser];
        sharedUser.currentState = UserStateLoggedIn;
        sharedUser.token = [responseObject valueForKey:SessionTokenKey];
        sharedUser.sessionId = [responseObject valueForKey:SessionIDKey];
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)requestVaultKey {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    User *sharedUser = [User sharedUser];
    
    if (sharedUser.currentState == UserStateOnCreation) {
        [parameters setValue:sharedUser.token forKey:CreationTokenKey];
    } else if (sharedUser.currentState == UserStateLoggedIn) {
        [parameters setValue:sharedUser.token forKey:SessionTokenKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:RequestVaultKeyMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        [[User sharedUser] setAccessKey:[response valueForKey:AccessKey]];
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)prepareCard:(NSMutableDictionary *)card {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:card forKey:CardDataKey];
    [parameters setValue:[[User sharedUser] accessKey] forKey:AccessKey];

    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:self.prepareCardBaseURL];
    manager.requestSerializer  = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:PrepareCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)addCardWithCardNonce:(NSString *)cardNonce {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:cardNonce forKey:CardNonceKey];
    [parameters setValue:@YES forKey:WaitForCardKey];

    User *sharedUser = [User sharedUser];
    if (sharedUser.currentState == UserStateOnCreation) {
        [parameters setValue:sharedUser.token forKey:CreationTokenKey];
        [parameters setValue:@YES forKey:SendLoginSMSKey];
    } else if (sharedUser.currentState == UserStateLoggedIn) {
        [parameters setValue:sharedUser.token forKey:SessionTokenKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:AddCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)deleteCardWithCardID:(NSString *)cardID {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:cardID forKey:CardIDKey];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:DeleteCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        CreditCardsList *cardsList = [CreditCardsList sharedList];
        CreditCard *deletedCreditCard = [cardsList getCardWithID:cardID];
        [cardsList.creditCards removeObject:deletedCreditCard];
                
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)getCardDetailsWithCardID:(NSString *)cardID {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    User* sharedUser = [User sharedUser];
    
    if (sharedUser.currentState == UserStateOnCreation) {
        [parameters setValue:sharedUser.token forKey:CreationTokenKey];
    } else if (sharedUser.currentState == UserStateLoggedIn) {
        [parameters setValue:sharedUser.token forKey:SessionTokenKey];
    }
    
    if (cardID != nil) {
        [parameters setValue:cardID forKey:CardIDKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:GetCardDetailsMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        
        // Confirma que cartão é o default antes de setar localmente
        if ([[response objectForKey:IsDefaultKey] boolValue]) {
            [[CreditCardsList sharedList] setDefaultCardWithCardID:[response objectForKey:CardIDKey]];
        }
        
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];

}

- (void)setDefaultCardWithCardID:(NSString *)cardID {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:cardID forKey:CardIDKey];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetDefaultCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [[CreditCardsList sharedList] setDefaultCardWithCardID:cardID];
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)listCards {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:@0 forKey:ItemIndexKey];
    [parameters setValue:@100 forKey:ItemCountKey];
    [parameters setValue:@YES forKey:PendingSharedCardsKey];
    
    self.manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ListCardsMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        
        // Remove todos os cartões locais antes de inserir os novos
        [[CreditCardsList sharedList] remove];
        
        NSMutableArray *cardsList = (NSMutableArray *)[response objectForKey:CardListKey];
        for (NSDictionary* cardDictionary in cardsList) {
            CreditCard *card = [[CreditCard alloc] init];
            card.status = [cardDictionary objectForKey:StatusKey];
            
            if ([card.status isEqual: @1]) {
                card.type = [[cardDictionary objectForKey:CardTypeKey] intValue];
                card.cardId = [cardDictionary objectForKey:CardIDKey];
                card.brandId = [cardDictionary objectForKey:BrandIDKey];
                card.lastDigits = [cardDictionary objectForKey:LastDigitsKey];
                card.isDefault = [[cardDictionary objectForKey:IsDefaultKey] boolValue];
                card.isShared = [[cardDictionary objectForKey:IsSharedKey] boolValue];
                
                if (card.isShared) {
                    card.sharedDetails = [cardDictionary objectForKey:SharedDetailsKey];
                }
                
                
                [[[CreditCardsList sharedList] creditCards] addObject:card];
            }
        }
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)getAccountData:(NSArray *)data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:data forKey:DataKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:GetAccountDataMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        User* sharedUser = [User sharedUser];
        
        // Atualiza os dados do usuário localmente
        for (NSString *key in response) {
            if ([response[key] isEqual:[NSNull null]]) continue;

            if ([sharedUser respondsToSelector:NSSelectorFromString(key)]) {
                [sharedUser setValue:response[key] forKey:key];
            }
        }
        
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)setAccountData:(NSDictionary *)data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:data forKey:DataKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetAccountDataMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        User* sharedUser = [User sharedUser];
        
        // Atualiza os dados do usuário localmente
        for (NSString *key in data) {
            if ([sharedUser respondsToSelector:NSSelectorFromString(key)]) {
                [sharedUser setValue:data[key] forKey:key];
            }
        }
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)getAccountPreferences:(NSArray *)preferences {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:preferences forKey:PreferencesKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:GetAccountPreferencesMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        Preferences *sharedPreferences = [Preferences sharedPreferences];
        
        // Atualiza os dados localmente
        for (NSString *key in response) {
            if ([sharedPreferences respondsToSelector:NSSelectorFromString(key)]) {
                [sharedPreferences setValue:response[key] forKey:key];
            }
        }
        
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)setAccountPreferences:(NSDictionary *)preferences {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:preferences forKey:PreferencesKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetAccountPreferencesMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        Preferences *sharedPreferences = [Preferences sharedPreferences];
        
        // Atualiza os dados localmente
        for (NSString *key in preferences) {
            if ([sharedPreferences respondsToSelector:NSSelectorFromString(key)]) {
                [sharedPreferences setValue:preferences[key] forKey:key];
            }
        }
        
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)changeEmailAddress:(NSString *)email {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:email forKey:EmailAddressKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ChangeEmailAddressMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)requestEmailConfirmation {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:RequestEmailConfirmationMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)setPhoneNumber:(NSString *)phone {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:phone forKey:PhoneNumberKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetPhoneNumberMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
        self.successCase(response);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)confirmPhoneNumber:(NSString *)phone withChallenge:(NSString *)challenge phoneChangeToken:(NSString *)changeToken {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:changeToken forKey:PhoneChangeTokenKey];
    [parameters setValue:challenge forKey:ChallengeKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ConfirmPhoneNumberMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [[User sharedUser] setPhoneNumber:phone];
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)resendSMSChallengeForPhoneChangeToken:(NSString *)changeToken {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:changeToken forKey:PhoneChangeTokenKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ResendSMSChallengeMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)listTransactionsWithStartingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount {
    [self listTransactionsWithSubscriptionID:nil startingItemIndex:itemIndex itemCount:itemCount];
}

- (void)listSubscriptionsWithStartingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:itemIndex forKey:ItemIndexKey];
    [parameters setValue:itemCount forKey:ItemCountKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ListSubscriptionsMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary *response = (NSDictionary *)responseObject;
        NSMutableArray *subscriptions = [[NSMutableArray alloc] initWithCapacity:response.count];
                
        for (NSDictionary* dictionary in (NSArray *)[response objectForKey:SubscriptionListKey]) {
            [subscriptions addObject:[[Subscription alloc] initWithJSONDictionary:dictionary]];
        }
        
        self.successCase(subscriptions);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)listTransactionsWithSubscriptionID:(NSString *)subscriptionID startingItemIndex:(NSNumber *)itemIndex itemCount:(NSNumber *)itemCount {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:itemIndex forKey:ItemIndexKey];
    [parameters setValue:itemCount forKey:ItemCountKey];
    
    if (subscriptionID) [parameters setValue:subscriptionID forKey:SubscriptionIDKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:ListTransactionsMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary *response = (NSDictionary *)responseObject;
        NSMutableArray *transactions = [[NSMutableArray alloc] initWithCapacity:response.count];
        
        for (NSDictionary* dictionary in (NSArray *)[response objectForKey:TransactionListKey]) {
            [transactions addObject:[[Transaction alloc] initWithJSONDictionary:dictionary]];
        }
        
        self.successCase(transactions);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)getSubscriptionDetailsWithSubscriptionID:(NSString *)subscriptionId {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:subscriptionId forKey:SubscriptionIDKey];
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:GetSubscriptionDetailsMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)setGeolocation {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    
    NSDictionary *location = [[LocationManager sharedManager] getLocation];
    if (location != nil) {
        [parameters setObject:location forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetGeolocationMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void) addSharedCard:(NSString *) cardId phoneNumber:(NSString *) phoneNumber withData: (NSDictionary *) data intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue  {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:data];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:phoneNumber forKey:PhoneNumberKey];
    [parameters setValue:intervalType forKey:IntervalTypeKey];
    [parameters setValue:intervalValue forKey:IntervalValueKey];
    
    
    [self.manager POST:AddSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void) addSharedCard:(NSString *) cardId phoneNumber:(NSString *) phoneNumber withBalance: (NSNumber *) balance intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue  {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:phoneNumber forKey:PhoneNumberKey];
    [parameters setValue:balance forKey:RecurringBalanceKey];
    [parameters setValue:intervalType forKey:IntervalTypeKey];
    [parameters setValue:intervalValue forKey:IntervalValueKey];

    
    [self.manager POST:AddSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void) acceptSharedCard:(NSString *) cardId {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    
    
    [self.manager POST:AcceptSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
    
}

- (void) deleteSharedCard:(NSString *) cardId custumerId:(NSString *) customerId {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:customerId forKey:CustomerIdKey];

    
    
    [self.manager POST:DeleteSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
    
}

- (void) updateSharedCard:(NSString *) cardId customerId:(NSString *) customerId withData: (NSDictionary *) data {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:data];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:customerId forKey:CustomerIdKey];
    
    [self.manager POST:UpdateSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void) updateSharedCard:(NSString *) cardId customerId:(NSString *) customerId withBalance: (NSNumber *) balance {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:customerId forKey:CustomerIdKey];
    [parameters setValue:balance forKey:RecurringBalanceKey];
    
    
    [self.manager POST:UpdateSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void) updateSharedCard:(NSString *) cardId customerId:(NSString *) customerId withBalance: (NSNumber *) balance intervalType:(NSNumber *) intervalType intervalValue: (NSNumber *) intervalValue  {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:customerId forKey:CustomerIdKey];
    [parameters setValue:balance forKey:RecurringBalanceKey];
    [parameters setValue:intervalType forKey:IntervalTypeKey];
    [parameters setValue:intervalValue forKey:IntervalValueKey];
    
    
    [self.manager POST:UpdateSharedCardMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)setNewPassword:(NSString *)newPassword oldPassword:(NSString *)oldPassword {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
    [parameters setValue:newPassword forKey:NewPasswordKey];
    
    if (oldPassword != nil) {
        [parameters setValue:oldPassword forKey:OldPasswordKey];
    } else {
        [parameters setValue:[NSNull null] forKey:OldPasswordKey];
    }
    
    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    [self.manager POST:SetPasswordMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}



- (void)startPasswordRecoveryWithIdentifier:(NSString *)identifier {
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if (![identifier containsString:@"@"] && identifier.length < 13) {
        identifier = [NSString stringWithFormat:@"55%@", identifier];
    }
    
    [parameters setValue:identifier forKey:IdentifierKey];
    
    [self.manager POST:StartPasswordRecoveryMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

- (void)validateCpf:(NSString *)cpf{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:cpf forKey:CpfKey];
    if([[Lib4all sharedInstance] hasUserLogged]){
        [parameters setValue:[[User sharedUser] token] forKey:SessionTokenKey];
        
    }else{
        [parameters setValue:[[User sharedUser] token] forKey:CreationTokenKey];

    }
    
    [self.manager POST:ValidateCpfMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];

}

- (void)validateSmsOrEmailWithChallenge:(NSString *)challenge{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:challenge forKey:ChallengeKey];
    [parameters setValue:[[User sharedUser] token] forKey:CreationTokenKey];
    
    [self.manager POST:ValidateChallengeMethod parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(nil);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];

}

-(void)payTransaction:(NSString *)sessionToken withTransactionId:(NSString *)transactionId andCardId:(NSString *)cardId payMode:(PaymentMode)payMode amount:(NSNumber *)amount installments:(NSString *)installments waitForTransaction:(BOOL)waitForTransaction{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:transactionId forKey:TransactionIDKey];
    [parameters setValue:@([amount intValue]) forKey:AmountKey];
    [parameters setValue:@(payMode) forKey:PayMode];
    [parameters setValue:installments forKey:Installments];
    [parameters setValue:[NSNumber numberWithBool:waitForTransaction] forKey:WaitForTransaction];

    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    NSLog(@"%@", parameters);
    [self.manager POST:PayTransaction parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
    
}

-(void)offlinePayTransaction:(NSString *)sessionToken withTransactionId:(NSString *)transactionId andCardId:(NSString *)cardId payMode:(PaymentMode)payMode amount:(NSNumber *)amount installments:(NSString *)installments cupomUIID:(NSString *)cupomUIID campaignUUID:(NSString *)campaignUUID merchantKeyId:(NSString *)merchantKeyId blob:(NSString *)blob waitForTransaction:(BOOL)waitForTransaction{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *installmentsNumber = @(1);
    
    formatter.numberStyle = NSNumberFormatterNoStyle;
    installmentsNumber = [formatter numberFromString:installments];
    
    //Encode Blob in base64
    if (blob != nil) {
        //Decode from URLEncoding
        blob = [blob stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    [parameters setValue:[User sharedUser].token forKey:SessionTokenKey];
    [parameters setValue:cardId forKey:CardIDKey];
    [parameters setValue:transactionId forKey:TransactionIDKey];
    [parameters setValue:@([amount intValue]) forKey:AmountKey];
    [parameters setValue:@(payMode) forKey:PayMode];
    [parameters setValue:merchantKeyId forKey:MerchantKeyId];
    [parameters setValue:blob  forKey:TransactionData];
    [parameters setValue:[NSNumber numberWithBool:waitForTransaction] forKey:WaitForTransaction];

    
    //Parametro só é enviado quando parcelado e não a vista
    if (installmentsNumber.intValue > 1){
        [parameters setValue:@([installmentsNumber intValue]) forKey:Installments];
    }
    
    if (cupomUIID != nil) {
        [parameters setValue:cupomUIID forKey:CouponUUID];
    }

    if (campaignUUID != nil) {
        [parameters setValue:campaignUUID forKey:CampaignUUID];
    }

    if([[LocationManager sharedManager] getLocation] != nil){
        [parameters setObject:[[LocationManager sharedManager] getLocation] forKey:GeoLocationKey];
    }
    
    NSLog(@"%@", parameters);
    [self.manager POST:OfflinePayTransaction parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];

    
}

- (void)handleError:(NSDictionary *)responseObj {
    if (responseObj[ErrorKey]){
        self.failureCase(responseObj[ErrorKey][ErrorCodeKey],responseObj[ErrorKey][ErrorMessageKey]);
    }else{
        self.failureCase(nil, @"Erro ao se comunicar com o servidor");
    }
}

@end
