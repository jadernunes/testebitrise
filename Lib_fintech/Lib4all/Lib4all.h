//
//  Lib4all.h
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComponentViewController.h"
#import "CardComponentViewController.h"
#import "User.h"
#import "ProfileViewController.h"
#import "LoadingViewController.h"
#import "PopUpBoxViewController.h"
#import "Lib4allPreferences.h"
#import "CardUtil.h"
#import "Button4all.h"

// MARK : - User state delegate

@protocol UserStateDelegate <NSObject>

@optional
- (void)userDidLogin;
- (void)userDidLogout;

@end

// MARK: - Lib4all

@interface Lib4all : NSObject

@property (strong, nonatomic) id<UserStateDelegate> userStateDelegate;

+ (instancetype)sharedInstance;

// MARK: - Métodos de classe de configuração da biblioteca

+ (void)setEnvironment:(Environment)environment;

+ (Environment)environment;

+ (void)setProductionEnvironment:(BOOL)isProductionEnvironment __deprecated;

+ (void)setApplicationID:(NSString *)applicationID;

+ (NSString *)applicationID;

+ (void)setApplicationVersion:(NSString *)applicationVersion;

+ (NSString *)applicationVersion;

//métodos antigos

+ (void)setAcceptedPaymentMode:(PaymentMode)paymentMode __deprecated_msg("Use setAcceptedPaymentTypes:(NSArray of PaymentType) instead.");

+ (PaymentMode)acceptedPaymentMode __deprecated_msg("use (NSArray of PaymentType)acceptedPaymentTypes instead.");

//métodos novos:

+ (void) setAcceptedPaymentTypes: (NSArray *) paymentTypes;

+ (NSArray *) acceptedPaymentTypes;

+ (void)setAcceptedBrands:(NSArray *)acceptedBrands;

+ (NSArray *)acceptedBrands;

+ (void)setRequireFullName:(BOOL)requireFullName;

+ (BOOL)requireFullName;

+ (void)setRequireCpfOrCnpj:(BOOL)requireCpfOrCnpj;

+ (BOOL)requireCpfOrCnpj;

+ (void)setRequireBirthdate:(BOOL)requireBirthdate;

+ (BOOL)requireBirthdate;

+ (void)setTermsOfServiceURL:(NSURL *)termsOfServiceURL;

+ (NSURL *)termsOfServiceURL;

+ (void)setCustomerData:(NSDictionary *)data;

+ (NSDictionary *)customerData;

+ (void)disableAntiFraudRuleForProperty:(NSString *)property;

+ (void)setButtonColor:(UIColor *)color;

+ (void)setLoaderColor:(UIColor *)color;

// MARK: - Métodos de instância

- (NSString *)versionAccount;

- (void)showProfileController:(UIViewController *)controller;

- (void)callSignUp:(UIViewController *)vc completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion;

- (void)callLogin:(UIViewController *)vc completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion;

- (void)callLogin:(UIViewController *)vc termsOfServiceUrl:(NSString *)url completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion __deprecated;

- (void)callLogin:(UIViewController *)vc requireFullName:(BOOL)requireFullName requireCpfOrCnpj:(BOOL)requireCpfOrCnpj completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion __deprecated;

- (void)callLogin:(UIViewController *)vc termsOfServiceUrl:(NSString *)url requireFullName:(BOOL)requireFullName requireCpfOrCnpj:(BOOL)requireCpfOrCnpj completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion __deprecated;

-(void)callLoginWithPayment:(UIViewController *)vc delegate:(id<CallbacksDelegate>) delegate __deprecated;
-(void)callLoginWithPayment:(UIViewController *)vc delegate:(id<CallbacksDelegate>) delegate paymentTypes:(NSArray *)paymentTypes andAcceptedBrands:(NSArray *)acceptedBrands;

- (void)callLogout: (void (^)(BOOL success))completion;

- (BOOL)hasUserLogged;

- (NSDictionary *)getAccountData;

- (void)getAccountData:(NSArray *)data completionBlock:(void(^)(NSDictionary *data))completion;

- (void)setAccountData:(NSDictionary *)data completionBlock:(void(^)(NSDictionary *error))completion;

- (void)showCardPickerInViewController:(UIViewController *)viewController completionBlock:(void(^)(NSString *cardID))completion;

- (void)showCardPickerInViewController:(UIViewController *)viewController completionBlock:(void(^)(NSString *cardID))completion withAcceptedPaymentTypes: (NSArray *)paymentTypes andAcceptedBrands: (NSArray *) acceptedBrands;

- (void)openDebitWebViewInViewController:(UIViewController *)viewController withUrl:(NSURL *)url completionBlock:(void(^)(BOOL success))completion;

- (void)generateAndShowOfflineQrCode:(UIViewController *)viewController ec:(NSString *) ec transactionId:(NSString *)transactionId amount:(int) amount campaignUUID:(NSString *)campaignUUID couponUUID:(NSString *) couponUUID;

- (void)generateAndShowOfflineQrCode:(UIViewController *)viewController ec:(NSString *) ec transactionId:(NSString *)transactionId amount:(int) amount;

- (NSDictionary *)unwrapBase64OfflineQrCode:(NSString *)qrCodeBase64;

- (NSString *)generateOfflinePaymentStringForTransactionID:(NSString *)transactionID cardID:(NSString *)cardID amount:(int)amount campaignUUID:(NSString *)campaignUUID couponUUID:(NSString *) couponUUID;

- (NSString *)generateOfflinePaymentStringForTransactionID:(NSString *)transactionID cardID:(NSString *)cardID amount:(int)amount;

- (void)showChat;

- (void)openAccountScreen:(ProfileOption)profileOption inViewController:(UIViewController *)viewController;

- (BOOL)qrCodeIsSupported:(NSString *)contentQrCode;

- (void)handleQrCode:(NSString *)contentQrCode inViewController:(UIViewController *)viewController;
@end
