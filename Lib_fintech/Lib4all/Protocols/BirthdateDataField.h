//
//  BirthdateDataField.h
//  Example
//
//  Created by Adriano Soares on 02/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFieldProtocol.h"

@interface BirthdateDataField : NSObject < DataFieldProtocol >

@end
