//
//  CPFDataField.h
//  Example
//
//  Created by Cristiano Matte on 02/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFieldProtocol.h"

@interface CPFDataField : NSObject < DataFieldProtocol >


@end
