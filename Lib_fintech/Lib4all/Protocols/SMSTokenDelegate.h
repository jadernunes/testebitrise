//
//  SMSTokenDelegate.h
//  Example
//
//  Created by 4all on 18/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMSTokenDelegate : NSObject < UITextFieldDelegate >

@property (nonatomic, strong) UIViewController *rootController;

@end
