//
//  Lib4allPreferences.h
//  Example
//
//  Created by Cristiano Matte on 21/09/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ComponentViewController;

typedef NS_ENUM(NSInteger, PaymentMode) {
    PaymentModeCredit = 1,
    PaymentModeDebit = 2,
    PaymentModeCreditAndDebit = 3
};


typedef NS_ENUM(NSInteger, PaymentType) {
    Credit = 0,
    Debit = 1,
    NumOfTypes = 2
};

typedef NS_ENUM(NSInteger, Environment) {
    EnvironmentTest = 1,
    EnvironmentHomologation = 2,
    EnvironmentProduction = 3
};

typedef NS_ENUM(NSInteger, ProfileOption) {
    ProfileOptionReceipt = 1,
    ProfileOptionSubscriptions = 2,
    ProfileOptionUserData = 3,
    ProfileOptionUserCards = 4,
    ProfileOptionSettings = 5,
    ProfileOptionHelp = 6,
    ProfileOptionAbout = 7,
    ProfileOptionFamily = 8
};



@interface Lib4allPreferences : NSObject

@property Environment environment;
@property BOOL requireFullName;
@property BOOL requireCpfOrCnpj;
@property BOOL requireBirthdate;
//@property PaymentMode acceptedPaymentMode; esta propriedade não será mais usada, foi substituida por acceptedPaymentTypes, que é mais dinâmica
@property NSArray * acceptedPaymentTypes;
@property (copy, nonatomic) NSURL *termsOfServiceURL;
@property (copy, nonatomic) NSSet *acceptedBrands;
@property (copy, nonatomic) NSString *applicationID;
@property (copy, nonatomic) NSString *applicationVersion;
@property (strong, nonatomic) ComponentViewController *currentVisibleComponent;
@property (strong, nonatomic) NSMutableDictionary *requiredAntiFraudItems;
@property (strong, nonatomic) NSDictionary *customerData;

@property (strong, nonatomic) UIColor *mainButtonColor;
@property (strong, nonatomic) UIColor *loaderColor;

+ (instancetype)sharedInstance;

@end
