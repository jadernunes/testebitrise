//
//  CreditCard.h
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardUtil.h"

@interface CreditCard : NSObject

@property (nonatomic, assign) CardType type;
@property (nonatomic, strong) NSString *cardId;
@property (nonatomic, strong) NSNumber *brandId;
@property (nonatomic, strong) NSString *lastDigits;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, assign) BOOL isDefault;
@property (nonatomic, assign) BOOL isShared;
@property (nonatomic, strong) NSArray *sharedDetails;

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary;
- (NSString *)getMaskedPan;
- (UIImage *)getBrandImage;

- (BOOL) isProvider;

@end
