//
//  Transaction.m
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "Transaction.h"
#import "ServicesConstants.h"
#import "Lib4allPreferences.h"

@implementation Transaction

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        self.transactionID = dictionary[TransactionIDKey];
        
        if (![dictionary[SubscriptionIDKey] isEqual:[NSNull null]]) {
            self.subscriptionID = dictionary[SubscriptionIDKey];
        }
        
        self.amount = dictionary[AmountKey];
        self.status = dictionary[StatusKey];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = DateFormat;
        
        self.createdAt = [dateFormatter dateFromString:dictionary[CreatedAtKey]];
        self.paidAt = [dateFormatter dateFromString:dictionary[PaidAtKey]];
        id installments = dictionary[@"installments"];
        if (installments && installments != [NSNull null]) {
            self.installments = [installments integerValue];
        }
        

        self.merchant = [[Merchant alloc] initWithJSONDictionary:dictionary[MerchantInfoKey]];
    }
    
    return self;
}

-(NSArray *)getAcceptedModes{
    NSMutableArray *arrayAcceptedModes = [[NSMutableArray alloc] init];
    
    int length = (int)self.acceptedModes.length;
    
    for (int i = 0; i < length; i++) {
        
        switch (i) {
            case 0:
                if ([self.acceptedModes characterAtIndex:i] == '1') {
                    [arrayAcceptedModes addObject:@(PaymentModeCredit)];
                }
                break;
            case 1:
                if ([self.acceptedModes characterAtIndex:i] == '1') {
                    [arrayAcceptedModes addObject:@(PaymentModeDebit)];
                }
                break;
                    default:
                break;
        }
    }

    if (arrayAcceptedModes.count == 2) {
        [arrayAcceptedModes addObject:@(PaymentModeCreditAndDebit)];
    }
    
    return arrayAcceptedModes;
}

-(NSArray *)getAcceptedBrands{
    NSMutableArray *arrayAcceptedBrands = [[NSMutableArray alloc] init];
    int length = (int)self.acceptedBrands.length;
    
    for (int i = 1; i < length+1; i++) {
        
        switch (i) {
            case CardBrandMastercard:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandMastercard)];
                }
                break;
            case CardBrandVisa:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandVisa)];
                }
                break;
            case CardBrandJCB:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandJCB)];
                }
                break;
            case CardBrandElo:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandElo)];
                }
                break;
            case CardBrandAmex:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandAmex)];
                }
                break;
            case CardBrandAura:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandAura)];
                }
                break;
            case CardBrandHiper:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandHiper)];
                }
                break;
            case CardBrandDiners:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandDiners)];
                }
                break;
            case CardBrandDiscover:
                if ([self.acceptedBrands characterAtIndex:i-1] == '1') {
                    [arrayAcceptedBrands addObject:@(CardBrandDiscover)];
                }
                break;
            default:
                break;
        }
    }
    
    return arrayAcceptedBrands;
}

@end
