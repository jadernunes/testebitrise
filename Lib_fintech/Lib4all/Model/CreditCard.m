//
//  CreditCard.m
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "CreditCard.h"
#import "ServicesConstants.h"

@implementation CreditCard

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        self.type = [dictionary[CardTypeKey] intValue];
        self.cardId = dictionary[CardIDKey];
        self.brandId = dictionary[BrandIDKey];
        self.lastDigits = dictionary[LastDigitsKey];
        self.status = dictionary[StatusKey];
        self.isDefault = [dictionary[IsDefaultKey] boolValue];
        self.isShared = [dictionary[IsSharedKey] boolValue];
        if (self.isShared) {
            self.sharedDetails = [dictionary[SharedDetailsKey] array];
        }
        
    }
    
    return self;
}

- (NSString *)getMaskedPan {
    return [NSString stringWithFormat:@"•••• •••• •••• %@",self.lastDigits];
}

- (UIImage *)getBrandImage {
    NSString *imageName;
    
    switch ([self.brandId intValue]) {
        case CardBrandVisa:
            imageName = @"visa";
            break;
        case CardBrandMastercard:
            imageName = @"mastercard";
            break;
        case CardBrandDiners:
            imageName = @"diners";
            break;
        case CardBrandElo:
            imageName = @"elo";
            break;
        case CardBrandAmex:
            imageName = @"amex";
            break;
        case CardBrandDiscover:
            imageName = @"discover";
            break;
        case CardBrandAura:
            imageName = @"aura";
            break;
        case CardBrandJCB:
            imageName = @"jcb";
            break;
        case CardBrandHiper:
            imageName = @"hiper";
            break;
        default:
            break;
    }
    
    return [UIImage imageNamed:imageName];
}

- (BOOL) isProvider {
    if (!self.isShared) {
        return YES;
    } else if ([self.sharedDetails[0][@"provider"] boolValue]) {
        return YES;
    }
    return NO;
    
}

@end
