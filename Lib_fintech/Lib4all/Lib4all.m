//
//  Lib4all.m
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "Lib4all.h"
#import "Transaction.h"
#import "CreditCard.h"
#import "User.h"
#import "BaseNavigationController.h"
#import "CardsTableViewController.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "Preferences.h"
#import "PersistenceHelper.h"
#import "WebViewController.h"
#import "QRCodeViewController.h"
#import "LayoutManager.h"
#import "NSData+AES.h"
#import "SignFlowController.h"
#import "SignInViewController.h"
#import <ZDCChat/ZDCChat.h>
#import "CreditCardsList.h"
#import "CallbacksDelegate.h"
#import "CardAdditionFlowController.h"
#import "PaymentFlowController.h"
#import "LoginPaymentAction.h"
#import "PaymentDetailsViewController.h"
#import "QRCodeParser.h"
#import "FlowPagerViewController.h"
#import "GenericDataViewController.h"
#import "SISUNameDataField.h"
#import "Lib4allInfo.h"

@implementation Lib4all

+ (instancetype)sharedInstance {
    static Lib4all *sharedInstance = nil;
    
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[Lib4all alloc] init];
        }
    }
    
    return sharedInstance;
}

- (id)init {
    self = [super init];
    
    if (self) {
        [PersistenceHelper sharedHelper];
        
        NSAssert([Lib4allPreferences sharedInstance].applicationID != nil && [Lib4allPreferences sharedInstance].applicationVersion != nil,
                 @"LIB4ALL: Antes de instanciar a classe Lib4all, você deve configurar o applicationID e o applicationVersion.");
        
        NSArray *stringParts = [[Lib4allPreferences sharedInstance].applicationID componentsSeparatedByString:@"_"];
        NSAssert(stringParts.count == 3 && [stringParts[0] isEqualToString:@"IOS"],
                 @"LIB4ALL: ApplicationID deve ter o formato IOS_ApplicationName_Version.");
    }
    
    return self;
}

// MARK: - Métodos de classe de configuração da biblioteca

+ (void)setEnvironment:(Environment)environment {
    [[Lib4allPreferences sharedInstance] setEnvironment:environment];
}

+ (Environment)environment {
    return [Lib4allPreferences sharedInstance].environment;
}

+ (void)setProductionEnvironment:(BOOL)isProductionEnvironment {
    if (isProductionEnvironment) {
        [[Lib4allPreferences sharedInstance] setEnvironment:EnvironmentProduction];
        // [Services setBaseURL:ProductionBaseURL];
        // [Services setPrepareCardBaseURL:PrepareCardProductionBaseURL];
    } else {
        [[Lib4allPreferences sharedInstance] setEnvironment:EnvironmentTest];
        // [Services setBaseURL:TestBaseURL];
        // [Services setPrepareCardBaseURL:PrepareCardTestBaseURL];
    }
}

+ (void)setApplicationID:(NSString *)applicationID {
    [[Lib4allPreferences sharedInstance] setApplicationID:applicationID];
}

+ (NSString *)applicationID {
    return [[Lib4allPreferences sharedInstance] applicationID];
}

+ (void)setApplicationVersion:(NSString *)applicationVersion {
    [[Lib4allPreferences sharedInstance] setApplicationVersion:applicationVersion];
}

+ (NSString *)applicationVersion {
    return [[Lib4allPreferences sharedInstance] applicationVersion];
}

//métodos antigos deprecados:
+ (void)setAcceptedPaymentMode:(PaymentMode)paymentMode {
    
    NSAssert(paymentMode == PaymentModeCredit || paymentMode == PaymentModeDebit || paymentMode == PaymentModeCreditAndDebit, @"Payment mode não suportado. Utilize a função setAcceptedPaymentTypes.");
    
    if (paymentMode == PaymentModeCredit) {
        [[Lib4allPreferences sharedInstance] setAcceptedPaymentTypes:@[@(Credit)]];
    } else if (paymentMode == PaymentModeDebit) {
        [[Lib4allPreferences sharedInstance] setAcceptedPaymentTypes:@[@(Debit)]];
    } else if (paymentMode == PaymentModeCreditAndDebit) {
        [[Lib4allPreferences sharedInstance] setAcceptedPaymentTypes:@[@(Credit), @(Debit)]];
    }
    
}

+ (PaymentMode)acceptedPaymentMode {
    
    if ([[[Lib4allPreferences sharedInstance] acceptedPaymentTypes] containsObject:@(Credit)] && [[[Lib4allPreferences sharedInstance] acceptedPaymentTypes] containsObject:@(Debit)])
        return PaymentModeCreditAndDebit;
    else if ([[[Lib4allPreferences sharedInstance] acceptedPaymentTypes] containsObject:@(Credit)])
        return PaymentModeCredit;
    else if ([[[Lib4allPreferences sharedInstance] acceptedPaymentTypes] containsObject:@(Debit)])
        return PaymentModeDebit;
    else {
        NSAssert(false, @"O app aceita outro modo de pagamento que não é crédito ou débito. Utilize a função acceptedPaymentTypes.");
        return 0;
    }
}

//métodos novos:

+ (void) setAcceptedPaymentTypes: (NSArray *) paymentTypes {
    [[Lib4allPreferences sharedInstance] setAcceptedPaymentTypes:paymentTypes];
}

+ (NSArray *) acceptedPaymentTypes {
    return [[Lib4allPreferences sharedInstance] acceptedPaymentTypes];
}


+ (void)setAcceptedBrands:(NSArray *)acceptedBrands {
    NSSet *acceptedBrandsSet = [[NSSet alloc] initWithArray:acceptedBrands];
    [[Lib4allPreferences sharedInstance] setAcceptedBrands:acceptedBrandsSet];
}

+ (NSArray *)acceptedBrands {
    NSSet* acceptedBrandsSet = [[Lib4allPreferences sharedInstance] acceptedBrands];
    NSMutableArray *acceptedBrands = [[NSMutableArray alloc] initWithCapacity:acceptedBrandsSet.count];
    
    for (NSNumber *brand in acceptedBrandsSet) {
        [acceptedBrands addObject:brand];
    }
    
    return acceptedBrands;
}

+ (void)setRequireFullName:(BOOL)requireFullName {
    [[Lib4allPreferences sharedInstance] setRequireFullName:requireFullName];
}

+ (BOOL)requireFullName {
    return [[Lib4allPreferences sharedInstance] requireFullName];
}

+ (void)setRequireCpfOrCnpj:(BOOL)requireCpfOrCnpj {
    [[Lib4allPreferences sharedInstance] setRequireCpfOrCnpj:requireCpfOrCnpj];
}

+ (BOOL)requireCpfOrCnpj {
    return [[Lib4allPreferences sharedInstance] requireCpfOrCnpj];
}

+ (void)setRequireBirthdate:(BOOL)requireBirthdate {
    [Lib4allPreferences sharedInstance].requireBirthdate = requireBirthdate;
}

+ (BOOL)requireBirthdate {
    return [Lib4allPreferences sharedInstance].requireBirthdate;
}

+ (void)setTermsOfServiceURL:(NSURL *)termsOfServiceURL {
    [[Lib4allPreferences sharedInstance] setTermsOfServiceURL:termsOfServiceURL];
}

+ (NSURL *)termsOfServiceURL {
    return [[Lib4allPreferences sharedInstance] termsOfServiceURL];
}

+ (void)setCustomerData:(NSDictionary *)data {
    [[Lib4allPreferences sharedInstance] setCustomerData:data];
}

+ (NSDictionary *)customerData {
    return [[Lib4allPreferences sharedInstance] customerData];
}

+ (void)disableAntiFraudRuleForProperty:(NSString *)property {
    [Lib4allPreferences sharedInstance].requiredAntiFraudItems[property] = @NO;
}

+ (void)setButtonColor:(UIColor *)color {
    [Lib4allPreferences sharedInstance].mainButtonColor = color;
}

+ (void)setLoaderColor:(UIColor *)color {
    [Lib4allPreferences sharedInstance].loaderColor = color;
}

- (NSString *)versionAccount {
    return Lib4allVersion;
}

// MARK: - Métodos de instância

- (void)showProfileController:(UIViewController *)controller {
    NSAssert([[User sharedUser] currentState] == UserStateLoggedIn,
             @"LIB4ALL: Não existe usuário logado no aplicativo. Deve ser feito login antes de exibir o perfil.");
    
    [controller presentViewController:[[ProfileViewController alloc] init] animated:true completion:nil];
}

- (void)callLogin:(UIViewController *)vc completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion {
    [self callLogin:vc termsOfServiceUrl:nil completion:completion];
    
}


-(void)callSignUp:(UIViewController *)vc completion:(void (^)(NSString *, NSString *, NSString *))completion{
    NSAssert([[User sharedUser] currentState] != UserStateLoggedIn,
             @"LIB4ALL: Já existe usuário logado no aplicativo. Deve ser feito logout antes de iniciar novo cadastro.");

    SignFlowController *signFlowController = [[SignFlowController alloc] init];
    signFlowController.requirePaymentData = NO;
    signFlowController.isLogin = NO;
    signFlowController.loginCompletion = completion;
    
    GenericDataViewController *startController = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUNameDataField alloc] init]];
    startController.signFlowController = signFlowController;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:startController];
    
    //Torna a navigation bar transparente
    [navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navigationController.navigationBar.shadowImage = [UIImage new];
    navigationController.navigationBar.translucent = YES;
    navigationController.view.backgroundColor = [UIColor clearColor];
    navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[[LayoutManager sharedManager] fontWithSize:[[LayoutManager sharedManager] navigationTitleFontSize]]}];
    [navigationController setTitle:@"Cadastro"];
    [navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    [vc presentViewController:navigationController animated:YES completion:nil];
}

- (void)callLogin:(UIViewController *)vc termsOfServiceUrl:(NSString *)url completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion {
    NSAssert([[User sharedUser] currentState] != UserStateLoggedIn,
             @"LIB4ALL: Já existe usuário logado no aplicativo. Deve ser feito logout antes de iniciar novo login.");
    
    if (url != nil) {
        [[Lib4allPreferences sharedInstance] setTermsOfServiceURL:[NSURL URLWithString:url]];
    }
    
    SignFlowController *signFlowController = [[SignFlowController alloc] init];
    signFlowController.requirePaymentData = NO;
    signFlowController.loginCompletion = completion;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Lib4all" bundle: nil];
    UINavigationController *destinationNv = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    SignInViewController *destination = [[destinationNv viewControllers] objectAtIndex:0];
    destination.signFlowController = signFlowController;
    
    [vc presentViewController:destinationNv animated:true completion:nil];
}

-(void)callLoginWithPayment:(UIViewController *)vc delegate:(id<CallbacksDelegate>) delegate{
    
    [self callLoginWithPayment:vc delegate:delegate paymentTypes:[[Lib4allPreferences sharedInstance] acceptedPaymentTypes] andAcceptedBrands: [[[Lib4allPreferences sharedInstance] acceptedBrands] allObjects]] ;
}

-(void)callLoginWithPayment:(UIViewController *)vc delegate:(id<CallbacksDelegate>)delegate paymentTypes:(NSArray *)paymentTypes andAcceptedBrands:(NSArray *)acceptedBrands{
    
    NSAssert([[User sharedUser] currentState] != UserStateLoggedIn,
             @"LIB4ALL: Já existe usuário logado no aplicativo. Deve ser feito logout antes de iniciar novo login.");
    
    [[[LoginPaymentAction alloc] init] callMainAction:vc delegate:delegate acceptedPaymentTypes:paymentTypes acceptedBrands:acceptedBrands];
}

- (void)callPrevendaWithCardId:(NSString *)cardId paymentMode:(PaymentMode)paymentMode delegate:(id<CallbacksDelegate>) delegate{
    if ([delegate respondsToSelector:@selector(callbackPreVenda:cardId:paymentMode:)]) {
        PaymentFlowController *paymentFlowController = [[PaymentFlowController alloc] init];
        paymentFlowController.paymentCompletion = ^() {
            CreditCard *card = [[CreditCardsList sharedList] getCardWithID:cardId];
            [delegate callbackPreVenda:[[User sharedUser] token] cardId:card.cardId paymentMode:paymentMode];
        };
        
        [paymentFlowController startFlowWithViewController:self];
    }
}

- (void)callLogin:(UIViewController *)vc requireFullName:(BOOL)requireFullName requireCpfOrCnpj:(BOOL)requireCpfOrCnpj completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion {
    [self callLogin:vc termsOfServiceUrl:nil requireFullName:requireFullName requireCpfOrCnpj:requireCpfOrCnpj completion:completion];
}

- (void)callLogin:(UIViewController *)vc termsOfServiceUrl:(NSString *)url requireFullName:(BOOL)requireFullName requireCpfOrCnpj:(BOOL)requireCpfOrCnpj completion:(void (^)(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken))completion {
    NSAssert([[User sharedUser] currentState] != UserStateLoggedIn,
             @"LIB4ALL: Já existe usuário logado no aplicativo. Deve ser feito logout antes de iniciar novo login.");
    
    if (url != nil) {
        [[Lib4allPreferences sharedInstance] setTermsOfServiceURL:[NSURL URLWithString:url]];
    }
    [[Lib4allPreferences sharedInstance] setRequireFullName:requireFullName];
    [[Lib4allPreferences sharedInstance] setRequireCpfOrCnpj:requireCpfOrCnpj];
    
    SignFlowController *signFlowController = [[SignFlowController alloc] init];
    signFlowController.requirePaymentData = NO;
    signFlowController.loginCompletion = completion;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Lib4all" bundle: nil];
    UINavigationController *destinationNv = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    SignInViewController *destination = [[destinationNv viewControllers] objectAtIndex:0];
    destination.signFlowController = signFlowController;
    
    [vc presentViewController:destinationNv animated:true completion:nil];
}

- (void)callLogout:(void(^)(BOOL success))completion {
    Services *service = [[Services alloc] init];
    ComponentViewController *component = [[Lib4allPreferences sharedInstance] currentVisibleComponent];

    service.successCase = ^(NSDictionary *response) {
        if (component != nil) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [component updateComponentViews];
            }];
        }
        
        if (completion) completion(YES);
        [Lib4all.sharedInstance.userStateDelegate userDidLogout];
    };
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        if (component != nil) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [component updateComponentViews];
            }];
        }
        
        if (completion) completion(NO);
        [Lib4all.sharedInstance.userStateDelegate userDidLogout];
    };
    
    [[[ZDCChat instance] api] endChat];
    [service logout];
}

- (BOOL)hasUserLogged {
    if ([[User sharedUser] currentState] == UserStateLoggedIn) {
        return YES;
    } else {
        return NO;
    }
}

- (NSDictionary *)getAccountData {
    if ([[User sharedUser] currentState] != UserStateLoggedIn) {
        return nil;
    }
    
    User *user = [User sharedUser];
    NSMutableDictionary* accountData = [[NSMutableDictionary alloc] init];
    [accountData setValue:[user customerId] forKey:@"customerId"];
    [accountData setValue:[user emailAddress] forKey:@"email"];
    [accountData setValue:[user emailAddress] forKey:@"emailAddress"];
    [accountData setValue:[user phoneNumber] forKey:@"phone"];
    [accountData setValue:[user phoneNumber] forKey:@"phoneNumber"];
    [accountData setValue:[user token] forKey:@"sessionToken"];
    [accountData setValue:[user cpf] forKey:@"cpf"];
    [accountData setValue:[user fullName] forKey:@"fullName"];
    [accountData setValue:[user birthdate] forKey:@"birthdate"];
    [accountData setValue:[user employer] forKey:@"employer"];
    [accountData setValue:[user jobPosition] forKey:@"jobPosition"];
    [accountData setValue:[user sessionId] forKey:@"sessionId"];
    
    return accountData;
}

- (void)getAccountData:(NSArray *)data completionBlock:(void(^)(NSDictionary *data))completion {
    Services *service = [[Services alloc] init];
    
    service.successCase = ^(NSDictionary *response) {
        completion(response);
    };
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        NSMutableDictionary *error = [[NSMutableDictionary alloc] init];
        [error setValue:cod forKey:ErrorCodeKey];
        [error setValue:msg forKey:ErrorMessageKey];

        completion(error);
    };
    
    [service getAccountData:data];
}

- (void)setAccountData:(NSDictionary *)data completionBlock:(void(^)(NSDictionary *error))completion {
    Services *service = [[Services alloc] init];
    
    service.successCase = ^(NSDictionary *response) {
        completion(nil);
    };
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        NSMutableDictionary *error = [[NSMutableDictionary alloc] init];
        [error setValue:cod forKey:ErrorCodeKey];
        [error setValue:msg forKey:ErrorMessageKey];
        
        completion(error);
    };
    
    [service setAccountData:data];
}

- (void)showCardPickerInViewController:(UIViewController *)viewController completionBlock:(void(^)(NSString *cardID))completion{
    NSAssert([[User sharedUser] currentState] == UserStateLoggedIn,
             @"LIB4ALL: Não existe usuário logado no aplicativo. Deve ser feito login antes de exibir o seletor de cartões.");

    CardsTableViewController *cardPickerViewController = [[CardsTableViewController alloc] init];
    cardPickerViewController.onSelectCardAction = OnSelectCardReturnCardId;
    cardPickerViewController.didSelectCardBlock = completion;
    
    BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:cardPickerViewController];
    
    [viewController presentViewController:navigationController animated:true completion:nil];
}

- (void)openDebitWebViewInViewController:(UIViewController *)viewController withUrl:(NSURL *)url completionBlock:(void(^)(BOOL success))completion {
    WebViewController *webViewController = (WebViewController *)[[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                                                 instantiateViewControllerWithIdentifier:@"WebViewController"];
    
    /*
     * Configura as constraints e o layout do webViewController
     */
    webViewController.view.layer.opacity = 0.0;
    webViewController.view.layer.cornerRadius = 5.0;
    webViewController.view.layer.borderWidth = 1.0;
    webViewController.view.layer.borderColor = [[[LayoutManager sharedManager] primaryColor] CGColor];
    webViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint constraintWithItem:webViewController.view
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:viewController.view
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1.0
                                                                          constant:0.0];
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:webViewController.view
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:viewController.view
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:10.0];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:webViewController.view
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:viewController.view
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:-10.0];
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:webViewController.view
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:viewController.view
                                                                        attribute:NSLayoutAttributeHeight
                                                                       multiplier:0.5
                                                                         constant:0.0];
    [viewController.view addSubview:webViewController.view];
    centerYConstraint.active = YES;
    leftConstraint.active = YES;
    rightConstraint.active = YES;
    heightConstraint.active = YES;
    
    /*
     * Configura a url a ser exibida e o bloco a ser executado quando o pagamento for finalizado
     */
    webViewController.url = url;
    
    __weak WebViewController *weakWebViewController = webViewController;
    webViewController.paymentCompletion = ^(BOOL success) {
        /*
         * Esconde o webViewController com animação
         */
        [weakWebViewController willMoveToParentViewController:nil];

        [UIView animateWithDuration:0.5 animations:^{
            weakWebViewController.view.layer.opacity = 0.0;
        } completion:^(BOOL finished) {
            [weakWebViewController removeFromParentViewController];
            [weakWebViewController.view removeFromSuperview];
            
            // Chama o callback passado por parâmetro
            completion(success);
        }];
    };
    
    /*
     * Exibe o webViewController com animação
     */
    [webViewController willMoveToParentViewController:viewController];
    
    [UIView animateWithDuration:0.5 animations:^{
        webViewController.view.layer.opacity = 1.0;
    } completion:^(BOOL finished) {
        [viewController addChildViewController:webViewController];
        [webViewController didMoveToParentViewController:viewController];
    }];
}

- (void)generateAndShowOfflineQrCode:(UIViewController *)viewController ec:(NSString *) ec transactionId:(NSString *)transactionId amount:(int) amount campaignUUID:(NSString *)campaignUUID couponUUID:(NSString *) couponUUID {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Lib4all" bundle: nil];
    UINavigationController *destinationNv = [storyboard instantiateViewControllerWithIdentifier:@"QRCodeVC"];
    QRCodeViewController *destination = [[destinationNv viewControllers] objectAtIndex:0];
    
    destination.transactionId = transactionId;
    destination.nameEC = ec;
    destination.amount = amount;
    destination.campaignUUID = campaignUUID;
    destination.couponUUID = couponUUID;
    
    [viewController presentViewController:destinationNv animated:true completion:nil];

}

- (void)generateAndShowOfflineQrCode:(UIViewController *)viewController ec:(NSString *) ec transactionId:(NSString *)transactionId amount:(int) amount {
    [self generateAndShowOfflineQrCode:viewController ec:ec transactionId:transactionId amount:amount campaignUUID:nil couponUUID:nil];
}

- (NSDictionary *)unwrapBase64OfflineQrCode: (NSString *)qrCodeBase64 {
    NSMutableDictionary *transactionData = [[NSMutableDictionary alloc] init];
    @try {
        //Remove X
        NSString *transactionString = [[qrCodeBase64 substringToIndex:3] stringByAppendingString:[qrCodeBase64 substringFromIndex:4]];
        
        //Decodifica o base64
        NSData *data = [[NSData alloc] initWithBase64EncodedString:transactionString options:0];
        
        transactionString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSArray *infos = [transactionString componentsSeparatedByString:@"_"];
        NSString *transactionID = infos[2];
        NSNumber *amount = [NSNumber numberWithInt:[infos[3] intValue]];
        NSString *EC = infos[4];
        
        
        [transactionData setValue:transactionID forKey:@"transactionId"];
        [transactionData setValue:amount forKey:@"amount"];
        [transactionData setValue:EC forKey:@"ec"];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
    return transactionData;
}

- (NSString *)generateOfflinePaymentStringForTransactionID:(NSString *)transactionID cardID:(NSString *)cardID amount:(int)amount campaignUUID:(NSString *)campaignUUID couponUUID:(NSString *) couponUUID  {
    NSAssert([[User sharedUser] currentState] == UserStateLoggedIn,
             @"LIB4ALL: Não existe usuário logado no aplicativo. Deve ser feito login antes de gerar query string para pagamento offline.");
    
    User *sharedUser = [User sharedUser];
    NSCharacterSet *urlBase64CharacterSet = [[NSCharacterSet characterSetWithCharactersInString:@"/+=\n"] invertedSet];
    
    // URL-codifica o sessionToken
    NSString *encodedSessionToken = [sharedUser.token stringByAddingPercentEncodingWithAllowedCharacters:urlBase64CharacterSet];
    
    // Gera a queryString de transactionData
    NSString *transactionData = [NSString stringWithFormat:@"A=%@&B=%@&C=%@&E=%d", encodedSessionToken, cardID, transactionID, amount];
    if (campaignUUID != nil && couponUUID != nil) {
        transactionData = [transactionData stringByAppendingString:[NSString stringWithFormat:@"&G=%@&F=%@", campaignUUID, couponUUID]];
    }
    
    // Encripta a queryString de transactionData, converte para base 64 e url-codifica o resultado
    NSData *encondedTransactionData = [[transactionData dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptedDataWithKey:sharedUser.token];
    transactionData = [[encondedTransactionData base64EncodedStringWithOptions:0] stringByAddingPercentEncodingWithAllowedCharacters:urlBase64CharacterSet];
    
    // Retorna a queryString final com o sessionId e o transactionData
    return [NSString stringWithFormat:@"A=%@&B=%@", sharedUser.sessionId, transactionData];
}

- (NSString *)generateOfflinePaymentStringForTransactionID:(NSString *)transactionID cardID:(NSString *)cardID amount:(int)amount {
    return [self generateOfflinePaymentStringForTransactionID:transactionID cardID:cardID amount:amount campaignUUID:nil couponUUID:nil];
}

- (void)showChat {
    [ZDCChat initializeWithAccountKey:@"41j6mInD9i6LHjwvOXPmlvBQVbG6fceJ"];
    
    // Personaliza layout do chat
    LayoutManager *layoutManager = [LayoutManager sharedManager];
    [[ZDCPreChatFormView appearance] setFormBackgroundColor:layoutManager.backgroundColor];
    [[ZDCOfflineMessageView appearance] setFormBackgroundColor:layoutManager.backgroundColor];
    [[ZDCChatView appearance] setChatBackgroundColor:layoutManager.backgroundColor];
    [[ZDCLoadingView appearance] setLoadingBackgroundColor:layoutManager.backgroundColor];
    [[ZDCLoadingErrorView appearance] setErrorBackgroundColor:layoutManager.backgroundColor];
    [[ZDCChatUI appearance] setChatBackgroundColor:layoutManager.backgroundColor];
    
    [[ZDCLoadingErrorView appearance] setButtonTitleColor:[UIColor whiteColor]];
    [[ZDCLoadingErrorView appearance] setButtonBackgroundColor:layoutManager.darkGreen];
    
    [[ZDCVisitorChatCell appearance] setTextColor:[UIColor whiteColor]];
    [[ZDCVisitorChatCell appearance] setBubbleColor:layoutManager.darkGreen];
    [[ZDCVisitorChatCell appearance] setBubbleBorderColor:layoutManager.darkGray];
    [[ZDCAgentChatCell appearance] setBubbleColor:layoutManager.lightGreen];
    [[ZDCAgentChatCell appearance] setBubbleBorderColor:layoutManager.darkGray];
    [[[ZDCChat instance] overlay] setEnabled:NO];
    
    // Adiciona dados do usuário logado
    User *user = [User sharedUser];
    [ZDCChat updateVisitor:^(ZDCVisitorInfo *visitor) {
        if ([self hasUserLogged]) {
            visitor.phone = user.phoneNumber;
            visitor.email = user.emailAddress;
            visitor.name = user.fullName;
        } else {
            visitor.phone = nil;
            visitor.email = nil;
            visitor.name = nil;
        }
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:Lib4all.sharedInstance selector:@selector(chatDidLayout:) name:ZDC_CHAT_UI_DID_LAYOUT object:nil];
    
    [ZDCChat startChat:^(ZDCConfig *config) {
        config.preChatDataRequirements.name = ZDCPreChatDataOptional;
        config.preChatDataRequirements.email = ZDCPreChatDataRequired;
        config.preChatDataRequirements.phone = ZDCPreChatDataRequired;
        config.preChatDataRequirements.message = ZDCPreChatDataRequiredEditable;
        config.preChatDataRequirements.department = ZDCPreChatDataRequiredEditable;
    }];
}

- (void)openAccountScreen:(ProfileOption)profileOption inViewController:(UIViewController *)viewController {
    NSAssert([[User sharedUser] currentState] == UserStateLoggedIn,
             @"LIB4ALL: Não existe usuário logado no aplicativo. Deve ser feito login antes de exibir a tela solicitada.");
    
    NSString *storyboardIdentifier;
    
    switch (profileOption) {
        case ProfileOptionReceipt:
            storyboardIdentifier = @"CompleteTransactionStatementViewController";
            break;
        case ProfileOptionSubscriptions:
            storyboardIdentifier = @"SubscriptionsViewController";
            break;
        case ProfileOptionUserData:
            storyboardIdentifier = @"MyDataViewController";
            break;
        case ProfileOptionUserCards:
            storyboardIdentifier = @"CardsTableViewController";
            break;
        case ProfileOptionSettings:
            storyboardIdentifier = @"NotificationsViewController";
            break;
        case ProfileOptionHelp:
            storyboardIdentifier = @"HelpViewController";
            break;
        case ProfileOptionAbout:
            storyboardIdentifier = @"AboutViewController";
            break;
        case ProfileOptionFamily:
            storyboardIdentifier = @"FamilyProfileTableViewController";
            break;
        default:
            break;
    }
    
    UIViewController *destinationViewController;
    if (storyboardIdentifier != nil) {
        destinationViewController = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                     instantiateViewControllerWithIdentifier:storyboardIdentifier];
    }
    
    if (destinationViewController != nil) {
        BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:destinationViewController];
        [viewController presentViewController:navigationController animated:YES completion:nil];
    }
}

-(BOOL)qrCodeIsSupported:(NSString *)contentQrCode{
    BOOL isValid = YES;
    if (contentQrCode.length < 5) {
        return NO;
    }
    
    NSDictionary *paymentInfo = [[QRCodeParser new] generateDictionaryFromQRContent:contentQrCode];
    
    isValid = paymentInfo != nil ? YES : NO;
    
    return isValid;
    
}

-(void)handleQrCode:(NSString *)contentQrCode inViewController:(UIViewController *)viewController{
    
    NSAssert([self qrCodeIsSupported:contentQrCode],
             @"LIB4ALL: QR Code não suportado pela aplicação. Deve ser verificado antes de chamar a função handleQrCode.");
    
    PaymentDetailsViewController *destinationViewController;
    
    BaseNavigationController *baseDestinationController = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                                           instantiateViewControllerWithIdentifier:@"PaymentDetailsViewController"];
    

   Transaction *transactionInfo = [[[QRCodeParser alloc] init] parseToTransaction:contentQrCode];
    
    destinationViewController = (PaymentDetailsViewController *)[[baseDestinationController viewControllers] objectAtIndex:0];
    destinationViewController.transactionInfo   = transactionInfo;
    destinationViewController.isMerchantOffline = (transactionInfo.blob != nil && transactionInfo.merchant.merchantKeyId != nil);
    
    [viewController presentViewController:baseDestinationController animated:YES completion:nil];
}



// MARK: - Chat navigation bar

- (void)chatDidLayout:(NSNotification*)notification {
    ZDCChatViewController *controller = [ZDCChat instance].chatViewController;

    controller.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    controller.navigationController.navigationBar.translucent = NO;
    controller.navigationController.navigationBar.barTintColor = [[LayoutManager sharedManager] darkGreen];
    controller.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [controller.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                            NSFontAttributeName:[[LayoutManager sharedManager] fontWithSize:15.0]}];
    
    controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar"
                                                                                   style:UIBarButtonItemStylePlain
                                                                                  target:Lib4all.sharedInstance
                                                                                  action:@selector(closeChat)];
}

- (void)closeChat {
    ZDCChatViewController *controller = [ZDCChat instance].chatViewController;
    controller.navigationItem.leftBarButtonItem = nil;
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
