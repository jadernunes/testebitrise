//
//  MainButtonAction.m
//  Example
//
//  Created by 4all on 02/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "LoginPaymentAction.h"
#import "CreditCardsList.h"
#import "Lib4allPreferences.h"
#import "User.h"
#import "CardAdditionFlowController.h"
#import "SignFlowController.h"
#import "SignInViewController.h"
#import "PaymentFlowController.h"

@interface LoginPaymentAction() <UIActionSheetDelegate>

@end

@implementation LoginPaymentAction

-(void)callMainAction:(UIViewController *)controller
             delegate:(id<CallbacksDelegate>)delegate
 acceptedPaymentTypes:(NSArray*) paymentTypes
       acceptedBrands:(NSArray *) brands {
    
    self.controller = controller;
    self.delegate   = delegate;
    
    BOOL shouldContinue = true;
    
    /*
     * Chama o callback onClick do botão, caso seja configurado pelo chamador
     */
    if ([self.delegate respondsToSelector:@selector(callbackShouldPerformButtonAction)]) {
        shouldContinue = [self.delegate callbackShouldPerformButtonAction];
    }
    
    //NSArray * acceptedPaymentMode = [[Lib4allPreferences sharedInstance] acceptedPaymentTypes];
    
    if (shouldContinue) {
        /*
         * Se usuário está logado, prossegue com o pagamento.
         * Caso contrário, abre a tela de login/cadastro com pagamento.
         */
        if ([[User sharedUser] currentState] == UserStateLoggedIn){
            CreditCard *defaultCard = [[CreditCardsList sharedList] getDefaultCard];
            
            /*
             * Caso o usuário já possua cartão adicionado, paga com este cartão.
             * Caso contrário, abre a tela de adição de cartão
             */
            if (defaultCard != nil && ![defaultCard.cardId isEqualToString:@""]) {
                /*
                 * Se o cartão selecionado é de modalidade ou bandeira não aceita, exibe alerta de erro.
                 * Se o cartão é de crédito e débito e ambos são aceitos, exibe action sheet para
                 * que seja selecionado o modo de pagamento.
                 * Se apenas crédito ou débito são aceitos e o cartão selecionado possui a modalidade
                 * aceita, chama o callback pré venda.
                 */
                if (![self cardIsAccepted:defaultCard withAcceptedPaymentTypes:paymentTypes andAcceptedBrands:brands]) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                                   message:@"Este cartão não é aceito neste aplicativo. Por favor, escolha outro cartão."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:ok];
                    
                    [controller presentViewController:alert animated:YES completion:nil];
                } else if (defaultCard.type == CardTypeCreditAndDebit && [paymentTypes containsObject:@(Credit)] && [paymentTypes containsObject:@(Debit)]) {
                    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                             delegate:self
                                                                    cancelButtonTitle:@"Cancelar"
                                                               destructiveButtonTitle:nil
                                                                    otherButtonTitles:@"Pagar com Crédito", @"Pagar com Débito", nil];
                    [actionSheet showInView:self.controller.view];
                } else {
                    PaymentMode payMode = PaymentModeCredit;
                    if (defaultCard.type == CardTypeCredit) payMode = PaymentModeCredit;
                    if (defaultCard.type == CardTypeDebit) payMode = PaymentModeDebit;
                    
                    [self callPrevendaWithCardId:defaultCard.cardId paymentMode:payMode];
                }
            } else {
                // Inicia o fluxo de adição de cartão e finaliza com o pagamento
                CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] initWithAcceptedPaymentTypes:paymentTypes andAcceptedBrands:brands];
                flowController.loginWithPaymentCompletion = ^(NSString *sessionToken, NSString *cardId) {
                    PaymentMode payMode = PaymentModeCredit;
                    if (defaultCard.type == CardTypeCredit) payMode = PaymentModeCredit;
                    if (defaultCard.type == CardTypeDebit) payMode = PaymentModeDebit;
                    
                    [self callPrevendaWithCardId:cardId paymentMode:payMode];

                };
                
                [flowController startFlowWithViewController:self.controller];
            }
        } else {
            SignFlowController *signFlowController = [[SignFlowController alloc] initWithAcceptedPaymentTypes: paymentTypes andAcceptedBrands: brands];
            signFlowController.requirePaymentData = YES;
            
            // Nos casos de login de usuário existente, será chamado o callbackLogin ao finalizar o login
            signFlowController.loginCompletion = ^(NSString *phoneNumber, NSString *emailAddress, NSString *sessionToken) {
                if (self.delegate != nil && [self.delegate respondsToSelector:@selector(callbackLogin:email:phone:)]) {
                    [self.delegate callbackLogin:sessionToken email:emailAddress phone:phoneNumber];
                }
            };
            
            // Nos casos de cadastro de novo usuário, será chamado o callBackPreVenda ao finalizar o login
            signFlowController.loginWithPaymentCompletion = ^(NSString *sessionToken, NSString *cardId) {
                if (self.delegate != nil) {
                    CreditCard *card = [[CreditCardsList sharedList] getCardWithID:cardId];
                    
                    /*
                     * Se o cartão selecionado é de modalidade não aceita, exibe alerta de erro.
                     * Se o cartão é de crédito e débito e ambos são aceitos, exibe action sheet para
                     * que seja selecionado o modo de pagamento.
                     * Se apenas crédito ou débito são aceitos e o cartão selecionado possui a modalidade
                     * aceita, chama o callback pré venda.
                     */
                    if (![self cardIsAccepted:card withAcceptedPaymentTypes:paymentTypes andAcceptedBrands:brands]) {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                                       message:@"Este cartão não é aceito neste aplicativo. Por favor, escolha outro cartão."
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                        [alert addAction:ok];
                        
                        [controller presentViewController:alert animated:YES completion:nil];
                    } else if (card.type == CardTypeCreditAndDebit) {
                        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                                 delegate:self
                                                                        cancelButtonTitle:@"Cancelar"
                                                                   destructiveButtonTitle:nil
                                                                        otherButtonTitles:@"Pagar com Crédito", @"Pagar com Débito", nil];
                        [actionSheet showInView:self.controller.view];
                    } else {
                        
                        //Inicia com o modo padrão de Crédito
                        PaymentMode payMode = PaymentModeCredit;
                        
                        if ([paymentTypes containsObject:@(Credit)])
                            payMode = PaymentModeCredit;
                        else if ([paymentTypes containsObject:@(Debit)])
                            payMode = PaymentModeDebit;
                        else
                            NSAssert(false, @"Tipo de pagamento não reconhecido!");
                        
                        [self callPrevendaWithCardId:card.cardId paymentMode:payMode];
                    }

                }
            };
            
            UINavigationController *navigationController = [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil]
                                                            instantiateViewControllerWithIdentifier:@"LoginVC"];
            SignInViewController *viewController = [[navigationController viewControllers] objectAtIndex:0];
            viewController.signFlowController = signFlowController;
            
            [controller presentViewController:navigationController animated:true completion:nil];
        }
    }

}

- (bool) cardIsAccepted:(CreditCard *) card withAcceptedPaymentTypes: (NSArray*) acceptedPaymentTypes andAcceptedBrands: (NSArray *) acceptedBrands {
    
    BOOL isValid = NO;
    
    //verifica os meios de pagamento do cartão
    
    //Verifica crédito
    if ((card.type == CardTypeCredit) && ([acceptedPaymentTypes containsObject:@(Credit)])) isValid = YES;
    
    //Verifica débito
    if ((card.type == CardTypeDebit) && ([acceptedPaymentTypes containsObject:@(Debit)])) isValid = YES;
    
    //Verifica ambos
    if ((card.type == CardTypeCreditAndDebit) && ([acceptedPaymentTypes containsObject:@(Credit)] || [acceptedPaymentTypes containsObject:@(Debit)])) isValid = YES;
    
    //Verifica brands
    if (![acceptedBrands containsObject:card.brandId]) isValid = NO;
    
    
    return isValid;
    
}

// MARK: - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    CreditCard *defaultCard = [[CreditCardsList sharedList] getDefaultCard];
    
    if (buttonIndex == 0) { // Crédito
        [self callPrevendaWithCardId:defaultCard.cardId paymentMode:PaymentModeCredit];
    } else if (buttonIndex == 1) { // Débito
        [self callPrevendaWithCardId:defaultCard.cardId paymentMode:PaymentModeDebit];
    }
}

// MARK: - Services

- (void)callPrevendaWithCardId:(NSString *)cardId paymentMode:(PaymentMode)paymentMode {
    if ([self.delegate respondsToSelector:@selector(callbackPreVenda:cardId:paymentMode:)]) {
        PaymentFlowController *paymentFlowController = [[PaymentFlowController alloc] init];
        paymentFlowController.paymentCompletion = ^() {
            CreditCard *card = [[CreditCardsList sharedList] getCardWithID:cardId];
            [self.delegate callbackPreVenda:[[User sharedUser] token] cardId:card.cardId paymentMode:paymentMode];
        };
        
        [paymentFlowController startFlowWithViewController:self.controller];
    }
}

@end
