//
//  PaymentDetailsViewController.m
//  Example
//
//  Created by 4all on 12/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "PaymentDetailsViewController.h"
#import "ComponentViewController.h"
#import "LayoutManager.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Lib4all.h"
#import "Services.h"
#import "CreditCardsList.h"
#import "ReceiptViewController.h"
#import "QRCodeMerchantOfflineViewController.h"

@interface PaymentDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleAmountToPay;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelMerchantName;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIView *containerPayment;
@property (weak, nonatomic) IBOutlet UILabel *labelParcelsTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelParcels;
@property (weak, nonatomic) NSString *transactionConfirmation;
@end

@implementation PaymentDetailsViewController

ComponentViewController *component;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self configurePaymentComponent];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureLayout];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configureLayout {
    LayoutManager *layoutManager = [LayoutManager sharedManager];
    self.view.backgroundColor = [UIColor colorWithRed:107.00/255.00 green:107.00/255.00 blue:107.00/255.00 alpha:1.0];
    self.labelTitle.font = [layoutManager fontWithSize:layoutManager.titleFontSize];
    self.labelTitle.textColor = layoutManager.primaryColor;
    
    self.labelTitleAmountToPay.font = [layoutManager fontWithSize:layoutManager.subTitleFontSize];
    self.labelTitleAmountToPay.textColor = layoutManager.lightGray;
    
    self.labelAmount.font = [layoutManager fontWithSize:50];
    self.labelAmount.textColor = layoutManager.primaryColor;
    self.labelAmount.text = [NSString stringWithFormat:@"R$ %.2f", ([[self.transactionInfo amount] doubleValue]/100.0)];
    
    self.labelDate.font = [layoutManager fontWithSize:layoutManager.regularFontSize];
    self.labelDate.textColor = layoutManager.primaryColor;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    [self.labelDate setText:[formatter stringFromDate:[NSDate new]]];
    
    self.labelMerchantName.font = [layoutManager fontWithSize:layoutManager.regularFontSize];
    self.labelMerchantName.textColor = layoutManager.lightGray;
    self.labelMerchantName.text = [[[self.transactionInfo merchant] name] stringByRemovingPercentEncoding];
    
    _labelParcelsTitle.font = [layoutManager fontWithSize:layoutManager.subTitleFontSize];
    _labelParcelsTitle.textColor = layoutManager.lightGray;
    
    _labelParcels.font = [layoutManager fontWithSize:layoutManager.titleFontSize];
    _labelParcels.textColor = layoutManager.lightGray;
    
    if (_transactionInfo.parcels != nil && ![_transactionInfo.parcels isEqualToString:@""] && [_transactionInfo.parcels integerValue] > 1) {
        _labelParcelsTitle.hidden = NO;
        //_labelParcelsTitle.text = @"Parcelado em";
        _labelParcels.hidden = YES;
        _labelParcelsTitle.text = [NSString stringWithFormat:@"Parcelado em %@x", _transactionInfo.parcels];
    }else{
        _labelParcelsTitle.hidden = YES;
        _labelParcels.hidden = YES;
    }
    
}

- (void) configurePaymentComponent{
    
    if (component != nil) {
        [component.view removeFromSuperview];
        [component removeFromParentViewController];
    }
    
    component = [[ComponentViewController alloc] init];

    //Set delegate para callbacks pre e pós venda
    component.delegate = self;
    
    //Define o titulo do botão do componente
    component.buttonTitleWhenNotLogged = @"ENTRAR";
    
    //Define o titulo do botão após estar logado
    component.buttonTitleWhenLogged = @"PAGAR";
    
    //Define o tamanho que o componente deverá ter em tela de acordo com o container.
    component.view.frame = self.containerPayment.bounds;
    
    //Adiciona view do component ao controller
    [self.containerPayment addSubview:component.view];
    
    //Adiciona a parte funcional ao container
    [self addChildViewController:component];
    [component didMoveToParentViewController:component];
    
}

- (IBAction)closeViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)callbackPreVenda:(NSString *)sessionToken cardId:(NSString *)cardId paymentMode:(PaymentMode)paymentMode{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    CreditCard *card = [[CreditCardsList sharedList] getCardWithID:cardId];
    
    //Se lista de bandeiras aceita for igual a zero, significa que o QRCode é antigo e não possui suporte, dessa forma, a validação fica por conta do default da lib e backend
    if ([[_transactionInfo getAcceptedBrands] containsObject:[card brandId]] || [_transactionInfo getAcceptedBrands].count == 0) {
        
        if ([card type] == CardTypeCreditAndDebit || [[_transactionInfo getAcceptedModes] containsObject:@([card type])] || [_transactionInfo getAcceptedModes].count == 0){
            if (networkStatus == NotReachable) {
                NSLog(@"There IS NO internet connection");
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                    message:@"Pagamento não pode ser efetuado,\ndeseja tentar pagamento offline?"
                                                                   delegate:self
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"NÃO", @"SIM", nil];
                
                [alertView show];
                
            } else {
                NSLog(@"There IS internet connection");
                Services *client = [Services new];
                
                client.successCase = ^(NSDictionary *response) {
                    [[LoadingViewController sharedManager] finishLoading:nil];
                    
                    if ([[response valueForKey:@"status"] integerValue] == 3) {
                        if (self.isMerchantOffline) {
                            self.transactionConfirmation = [response valueForKey:@"transactionConfirmation"];
                            [self performSegueWithIdentifier:@"segueMerchantOffline" sender:self];
                        }else{
                            [self performSegueWithIdentifier:@"segueReceipt" sender:self];
                        }
                    } else {
                        NSString *msg = [response valueForKey:@"reasonMessage"];
                        if (msg) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                                message:msg
                                                                               delegate:nil
                                                                      cancelButtonTitle:nil
                                                                      otherButtonTitles:@"Ok", nil];
                            [alertView show];
                        }
                    }

                };
                
                client.failureCase = ^(NSString *cod, NSString *msg){
                    NSLog(@"%@",msg);
                    [[LoadingViewController sharedManager] finishLoading:nil];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                        message:msg
                                                                       delegate:nil
                                                              cancelButtonTitle:nil
                                                              otherButtonTitles:@"Ok", nil];
                    [alertView show];
                };
                
                [[LoadingViewController sharedManager] startLoading:self title:@"Aguarde..."];
                
                if (self.isMerchantOffline == true){
                    
                    [client offlinePayTransaction:sessionToken
                                withTransactionId:_transactionInfo.transactionID
                                        andCardId:cardId
                                          payMode:paymentMode
                                           amount:_transactionInfo.amount
                                     installments:_transactionInfo.parcels
                                        cupomUIID:nil
                                     campaignUUID:nil
                                    merchantKeyId:_transactionInfo.merchant.merchantKeyId
                                             blob:_transactionInfo.blob
                               waitForTransaction:YES];
                }else{
                    [client payTransaction:sessionToken withTransactionId:_transactionInfo.transactionID andCardId:cardId payMode:paymentMode amount:_transactionInfo.amount installments:_transactionInfo.parcels waitForTransaction:YES];
                }
            }
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                message:@"Tipo de cartão ativo não é compatível com o tipo de pagamento a ser realizado."
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"Entendi", nil];
            [alertView show];

        }
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:@"Bandeira do cartão ativo não é aceito para esta transação."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"Entendi", nil];
        [alertView show];
    }
    
    
    
}

//mark: UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [[Lib4all sharedInstance] generateAndShowOfflineQrCode:self ec:self.transactionInfo.merchant.name transactionId:self.transactionInfo.transactionID amount:[_transactionInfo.amount intValue]];
    }
}

//mark: Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"segueReceipt"]) {
        ReceiptViewController *destVc = (ReceiptViewController *) segue.destinationViewController;
        destVc.transactionInfo = _transactionInfo;
    }else if([segue.identifier isEqualToString:@"segueMerchantOffline"]){
        QRCodeMerchantOfflineViewController *destVc = (QRCodeMerchantOfflineViewController *) segue.destinationViewController;
        destVc.contentQRCode = self.transactionConfirmation;
    }

    
}

@end
