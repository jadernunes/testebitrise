//
//  AcceptedCardsViewController.h
//  Example
//
//  Created by Cristiano Matte on 26/09/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptedCardsViewController : UIViewController

- (void) setAcceptedBrands: (NSArray *) brands;

@end
