//
//  GenericDataViewController.m
//  Example
//
//  Created by 4all on 12/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "GenericDataViewController.h"
#import "UIFloatLabelTextField.h"
#import "UIFloatLabelTextField+Border.h"
#import "LayoutManager.h"
#import "MainActionButton.h"
#import "SISUNameDataField.h"
#import "SISUPhoneNumberDataField.h"
#import "SISUTokenSmsDataField.h"
#import "SISUEmailDataField.h"
#import "SISUPasswordDataField.h"
#import "SISUPasswordConfirmationDataField.h"
#import "SMSTokenDelegate.h"
#import "PopUpBoxViewController.h"
#import "User.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "NSStringMask.h"

@interface GenericDataViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *textFieldCustom;
@property (weak, nonatomic) IBOutlet MainActionButton *mainButton;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelSubDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightGradientView;


//Items of the first controller(input full name)
@property (weak, nonatomic) IBOutlet UILabel *labelLogin;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

//Items of SMS controller
@property (strong) SMSTokenDelegate *delegateToken;
@property (weak, nonatomic) IBOutlet UIView *viewContainerPin;
@property (weak, nonatomic) IBOutlet UITextField *textToken1;
@property (weak, nonatomic) IBOutlet UITextField *textToken2;
@property (weak, nonatomic) IBOutlet UITextField *textToken3;
@property (weak, nonatomic) IBOutlet UITextField *textToken4;
@property (weak, nonatomic) IBOutlet UITextField *textToken5;
@property (weak, nonatomic) IBOutlet UITextField *textToken6;
@property (weak, nonatomic) IBOutlet UIButton *buttonResendSms;
@property (weak, nonatomic) IBOutlet UIButton *buttonConfirmToken;

@end

@implementation GenericDataViewController

static CGFloat const kBottomConstraintMin = 60.0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupController];
    
    if ([_dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]] && self.signFlowController.isLogin) {
        [self sendLoginSms:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([_dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]]) {
        [_textToken1 becomeFirstResponder];
    }else{
        [_textFieldCustom becomeFirstResponder];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = self.navigationController.title;

    if ([_dataFieldProtocol isKindOfClass:[SISUNameDataField class]]) {
        self.bottomConstraint.constant = kBottomConstraintMin;
    } else{
        self.bottomConstraint.constant = 22.0;
    }
    
}



- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [self dismissKeyboard];
    
}


- (void) dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    if ([[UIScreen mainScreen] bounds].size.height<=480.0f) {
        NSLog(@"App is running on iPhone with screen 3.5 inch");
        _heightGradientView.constant = _heightGradientView.constant - 70;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        self.bottomConstraint.constant = 3 + keyboardSize.height;
        
        [self.view updateConstraints];
        [self.view layoutIfNeeded];
        
    }];
    
}

-(void)keyboardWillHide:(NSNotification *)notification {
    if ([[UIScreen mainScreen] bounds].size.height<=480.0f) {
        NSLog(@"App is running on iPhone with screen 3.5 inch");
        _heightGradientView.constant = 222;
    }

    
    [UIView animateWithDuration:0.4 animations:^{
        if ([_dataFieldProtocol isKindOfClass:[SISUNameDataField class]]) {
            self.bottomConstraint.constant = kBottomConstraintMin;
        } else{
            self.bottomConstraint.constant = 22.0;
        }
        
        [self.view updateConstraints];
        [self.view layoutIfNeeded];
    }];
    
}


- (void)setupController{
    self.labelDescription.text = [self.dataFieldProtocol title];
    self.labelSubDescription.text = [self.dataFieldProtocol subTitle];
    
    LayoutManager *lm = [LayoutManager sharedManager];
    _loadingView = [[LoadingViewController alloc] init];
    
    [[UIFloatLabelTextField appearance] setBackgroundColor:[UIColor clearColor]];
    [self.textFieldCustom setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.textFieldCustom.floatLabelFont = [lm fontWithSize:[lm miniFontSize]];
    self.textFieldCustom.floatLabelActiveColor = [lm darkGray];
    [self.textFieldCustom setBottomBorderWithColor:[lm lightGray]];
    self.textFieldCustom.clearButtonMode = UITextFieldViewModeNever;
    self.textFieldCustom.delegate = _dataFieldProtocol;
    
    self.textFieldCustom.font = [lm fontWithSize:[lm regularFontSize]];
    self.textFieldCustom.textColor = [lm darkGray];
    [self.textFieldCustom setPlaceholder:[self.dataFieldProtocol textFieldPlaceHolder]];
    self.textFieldCustom.keyboardType = [self.dataFieldProtocol keyboardType];
    
    [_labelDescription setFont:[lm fontWithSize:[lm subTitleFontSize]]];
    [_labelSubDescription setFont:[lm fontWithSize:[lm regularFontSize]]];
    [_labelLogin setFont:[lm fontWithSize:[lm regularFontSize]]];
    [_labelLogin setTextColor:[lm darkGray]];
    [_buttonLogin.titleLabel setFont:[lm fontWithSize:[lm regularFontSize]]];
    [_buttonLogin setTitleColor:[lm primaryColor] forState:UIControlStateNormal];
    
    
    [_buttonResendSms setTitleColor:[lm primaryColor] forState:UIControlStateNormal];
    [[_buttonResendSms layer] setCornerRadius:6.0];
    [[_buttonResendSms layer] setBorderColor:[[lm primaryColor] CGColor]];
    [[_buttonResendSms layer] setBorderWidth:1.0];
    [_buttonResendSms.titleLabel setFont:[lm fontWithSize:[lm regularFontSize]]];
    
    //Action to dismiss keyboard
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    

    //Name Protocol
    [self nameProtocolConfigurations];
    
    //SMS Protocol
    [self smsProtocolConfiguration];
    
    //E-mail Protocol
    [self emailProtocolConfiguration];
    
    //Password Protocol
    [self passwordProtocolConfiguration];
    
    //Phone Protocol
    [self phoneNumberProtocolConfiguration];
    
    User* user = [User sharedUser];
    if ([_labelDescription.text containsString:@"@name"]) {
        NSArray *fullNameSplitted;

        if (user.fullName) {
            fullNameSplitted = [user.fullName componentsSeparatedByString:@" "];
        } else  if (_signFlowController.enteredFullName) {
            fullNameSplitted = [_signFlowController.enteredFullName componentsSeparatedByString:@" "];
        }
        if (fullNameSplitted.count > 0) {
            [_labelDescription setText:[_labelDescription.text stringByReplacingOccurrencesOfString:@"@name"
                                                                              withString:fullNameSplitted[0]]];
        } else if ([_dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]])  {
            [_labelDescription setText:[_labelDescription.text stringByReplacingOccurrencesOfString:@"@name, v"
                                                                                         withString:@"V"]];
        
        } else {
            [_labelDescription setText:[_labelDescription.text stringByReplacingOccurrencesOfString:@"@name"
                                                                                         withString:@""]];
        }

    }
    
    if ([_labelDescription.text containsString:@"@phoneNumber"]) {
        NSString *phone;
        if (_signFlowController.enteredPhoneNumber) {
            phone = (NSString *)[NSStringMask maskString:[_signFlowController.enteredPhoneNumber substringFromIndex:2] withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
        } else {
            phone = user.maskedPhone;
        }
        [_labelDescription setText:[_labelDescription.text stringByReplacingOccurrencesOfString:@"@phoneNumber"
                                                                                     withString:phone]];
    }
    
}


#pragma mark - Methods

+ (GenericDataViewController *)getConfiguredControllerWithdataFieldProtocol:(id<DataFieldProtocol>) protocol{
    
    GenericDataViewController *controller = [[GenericDataViewController alloc] initWithNibName:@"GenericDataViewController" bundle:nil];
    controller.dataFieldProtocol = protocol;
    return controller;
}


-(void)nameProtocolConfigurations{
    
    //Se for a primeira tela de signup (protocolo de nome) a label e botão de login são exibidos
    if ([_dataFieldProtocol isKindOfClass:[SISUNameDataField class]]) {
        [_labelLogin setHidden:NO];
        [_buttonLogin setHidden:NO];
        
        UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithTitle:@"Fechar"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(closeController:)];
        
        if (self.navigationController.viewControllers.count == 1) {
            [self.navigationItem setLeftBarButtonItem:menuItem];
        }
        _textFieldCustom.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
    }else{
        [_labelLogin setHidden:YES];
        [_buttonLogin setHidden:YES];
    }

}

-(void)emailProtocolConfiguration{
    if ([_dataFieldProtocol isKindOfClass:[SISUEmailDataField class]]) {
        
        //Remove a tela de SMS da pilha, caso usuario clique em voltar, não retorna para ela
        NSMutableArray *viewControllers = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
        
        //Validando o protocol, evita-se que sejam removidas outras controllers novamente caso avance e volte
        
        NSInteger currentIndex = [viewControllers indexOfObject:self];
        GenericDataViewController *previousController = [viewControllers objectAtIndex:currentIndex-1];
        
        if ([previousController.dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]]) {
            [viewControllers removeObject:previousController];
        }
        
        self.navigationController.viewControllers = viewControllers;
        
        //Preenche automaticamente caso tenha vindo do login
        if (self.signFlowController.enteredEmailAddress != nil) {
            _textFieldCustom.text = self.signFlowController.enteredEmailAddress;
        }

    }
    
}

-(void)passwordProtocolConfiguration{
    //if password or passwords confirmation
    if ([_dataFieldProtocol isKindOfClass:[SISUPasswordDataField class]] || [_dataFieldProtocol isKindOfClass:[SISUPasswordConfirmationDataField class]]) {
        _textFieldCustom.secureTextEntry = YES;
    }else{
        _textFieldCustom.secureTextEntry = NO;
    }
}

-(void)smsProtocolConfiguration{
    
    LayoutManager *lm = [LayoutManager sharedManager];

    //if SMS token screen
    if ([_dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]]) {
        
        //Change font and colors
        for (int i = 1; i < 7 ; i++) {
            UITextField *fieldDigit = (UITextField *)[self.view viewWithTag:i];
            [fieldDigit setFont:[lm fontWithSize:lm.regularFontSize]];
            [fieldDigit setTextColor:lm.primaryColor];
        }
        
        //Delegate responsible for digits fields
        _delegateToken = [[SMSTokenDelegate alloc] init];
        _delegateToken.rootController = self;
        
        _textToken1.delegate = _delegateToken;
        _textToken2.delegate = _delegateToken;
        _textToken3.delegate = _delegateToken;
        _textToken4.delegate = _delegateToken;
        _textToken5.delegate = _delegateToken;
        _textToken6.delegate = _delegateToken;
        
        [_viewContainerPin setHidden:NO];
    }else{
        [_viewContainerPin setHidden:YES];
    }
}

-(void)phoneNumberProtocolConfiguration{
    if ([_dataFieldProtocol isKindOfClass:[SISUPhoneNumberDataField class]]) {
        //Preenche automaticamente caso tenha vindo do login
        if (self.signFlowController.enteredPhoneNumber != nil) {
            _textFieldCustom.text =  (NSString *)[NSStringMask maskString:[self.signFlowController.enteredPhoneNumber substringFromIndex:2] withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
        }
    }

}

#pragma mark - Actions
-(void) closeController:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clickButonAction:(id)sender {
    
    NSString *dataText;
    BOOL fieldIsValid;
    
    if ([_dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]]) {
        dataText = [NSString stringWithFormat:@"%@%@%@%@%@%@", _textToken1.text,_textToken2.text,_textToken3.text,_textToken4.text,_textToken5.text, _textToken6.text];
    }else{
        dataText = _textFieldCustom.text;
    }
    
    fieldIsValid = [_dataFieldProtocol isDataValid:dataText];
    
    //If not valid, displays the error on the text field
    [_textFieldCustom showFieldWithError:!fieldIsValid];

    
    if (fieldIsValid) {
        _signFlowController.isLogin = NO;
        
        //Executa ação customizada
        [self.dataFieldProtocol saveData:self data:dataText withCompletion:nil];
    }

}

- (IBAction)resendSmsToken:(id)sender {
    [self sendLoginSms:YES];
}


-(void) sendLoginSms:(BOOL)showAlertOnSuccess {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        [_loadingView finishLoading:^{
            [[[PopUpBoxViewController alloc] init] show:self
                                                  title:@"Atenção!"
                                            description:msg
                                              imageMode:Error
                                           buttonAction:^{
                                               
                                           }];
            self.buttonResendSms.enabled = YES;
        }];
    };
    
    service.successCase = ^(NSDictionary *response){
        if (showAlertOnSuccess) {
            [_loadingView finishLoading:^{
                [[[PopUpBoxViewController alloc] init] show:self
                                                      title:@"Código reenviado por SMS."
                                                description:@"Em instantes você receberá um SMS \ncontendo um código."
                                                  imageMode:Success
                                               buttonAction:^{
                                                   
                                               }];
                self.buttonResendSms.enabled = YES;
            }];
        }
    };
    
    self.buttonResendSms.enabled = NO;
    [_loadingView startLoading:self title:@"Aguarde..."];
    [service sendLoginSms];
}

- (IBAction)confirmSmsToken:(id)sender {
    
    //TODO: Implement confirm sms
    [self clickButonAction:nil];
    
}

- (IBAction)callLogin:(id)sender {
    self.signFlowController.isLogin = YES;
    [self.signFlowController viewControllerDidFinish:self];
}


#pragma mark - Gesture Recognizer
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    //Evita a necessidade de tocar duas vezes  no botão
    if ([touch.view isDescendantOfView:_mainButton]) {
        return NO;
    }
    
    return YES;
}
@end
