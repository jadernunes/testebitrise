//
//  BaseNavigationController.h
//  Example
//
//  Created by 4all on 5/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController <UINavigationControllerDelegate>

- (void)configureLayout;

@end
