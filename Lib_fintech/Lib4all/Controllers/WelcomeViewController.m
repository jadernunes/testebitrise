//
//  WelcomeViewController.m
//  Example
//
//  Created by 4all on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "WelcomeViewController.h"
#import "BEMCheckBox.h"
#import "Lib4allPreferences.h"
#import "LayoutManager.h"
#import "Services.h"
#import "User.h"
#import "ServicesConstants.h"
#import "Lib4all.h"
#import "CardAdditionFlowController.h"

@interface WelcomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *buttonNewCard;
@property (weak, nonatomic) IBOutlet UIButton *buttonFinishFlow;
@property (weak, nonatomic) IBOutlet UILabel *labelGreetings;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupController];
}

- (void) setupController {
    LayoutManager *lm = [LayoutManager sharedManager];
    [_buttonFinishFlow.titleLabel setFont:[lm fontWithSize:lm.regularFontSize]];
    [_buttonFinishFlow setTitleColor:[lm darkGray] forState:UIControlStateNormal];
    [_buttonFinishFlow.layer setCornerRadius:6.0f];
    [_buttonFinishFlow.layer setBorderColor:lm.primaryColor.CGColor];
    
    [_labelGreetings setFont:[lm fontWithSize:lm.regularFontSize]];
    [_labelGreetings setTextColor:lm.darkGray];
    
    [_labelDescription setFont:[lm fontWithSize:lm.regularFontSize]];
    [_labelGreetings setTextColor:lm.darkGray];
    
    
    self.labelGreetings.font = [lm fontWithSize:[lm titleFontSize]];
    self.labelGreetings.textColor = [lm darkGray];
    NSArray *name = [_signFlowController.accountData[@"fullName"] componentsSeparatedByString:@" "];
    [self.labelGreetings setText:[self.labelGreetings.text stringByReplacingOccurrencesOfString:@"@name" withString: name[0]]];
    
    [self.navigationItem setHidesBackButton:YES];
    
    
    if (_signFlowController.requirePaymentData) {
        [_buttonNewCard setTitle:@"Continuar" forState:UIControlStateNormal];
        [_buttonFinishFlow setTitle:@"Cancelar" forState:UIControlStateNormal];
    }
    
}


- (IBAction)addNewCard {
    // Inicia fluxo de adição de cartão
    
    CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] initWithAcceptedPaymentTypes:_signFlowController.acceptedPaymentTypes andAcceptedBrands:_signFlowController.acceptedBrands];
    flowController.loginWithPaymentCompletion = _signFlowController.loginWithPaymentCompletion;
    flowController.onLoginOrAccountCreation = YES;
    
    [flowController startFlowWithViewController:self];
}

- (IBAction)finishFlow {
    // Exibe alerta para confirmar a alteração do dado
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                   message:@"Ao cancelar, o pagamento não será realizado.\nVocê confirma essa ação?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Não"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Sim"
                                              style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                [self cancelAndContinueToAppAction];
                                            }]];
    
    if (_signFlowController.requirePaymentData) {
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self cancelAndContinueToAppAction];
    }
}

-(void)cancelAndContinueToAppAction{
    [[Lib4all sharedInstance].userStateDelegate userDidLogin];
    _signFlowController.isLogin = NO;
    _signFlowController.skipPayment = YES;
    [_signFlowController viewControllerDidFinish:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
