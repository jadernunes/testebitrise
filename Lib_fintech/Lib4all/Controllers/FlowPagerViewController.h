//
//  FlowPagerViewController.h
//  Example
//
//  Created by 4all on 12/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowController.h"

@interface FlowPagerViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, FlowController>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
