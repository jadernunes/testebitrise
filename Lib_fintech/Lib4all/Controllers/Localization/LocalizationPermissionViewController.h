//
//  LocalizationPermissionViewController.h
//  Example
//
//  Created by Cristiano Matte on 01/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizationFlowController.h"

@interface LocalizationPermissionViewController : UIViewController

@property (strong, nonatomic) LocalizationFlowController *flowController;

@end
