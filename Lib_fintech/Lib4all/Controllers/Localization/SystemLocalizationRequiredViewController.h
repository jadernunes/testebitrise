//
//  SystemLocalizationRequiredViewController.h
//  Example
//
//  Created by Cristiano Matte on 05/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizationFlowController.h"

@interface SystemLocalizationRequiredViewController : UIViewController

@property (strong, nonatomic) LocalizationFlowController *flowController;

@end
