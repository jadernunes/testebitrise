//
//  ChangeEmailAddressViewController.m
//  Example
//
//  Created by Cristiano Matte on 02/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "ChangeEmailAddressViewController.h"
#import "ErrorTextField.h"
#import "BaseNavigationController.h"
#import "LayoutManager.h"
#import "Services.h"
#import "UIImage+Color.h"

@interface ChangeEmailAddressViewController ()

@property (weak, nonatomic) IBOutlet ErrorTextField *emailAddressTextField;
@property (weak, nonatomic) IBOutlet UIView *waitingForConfirmationView;
@property (weak, nonatomic) IBOutlet UILabel *waitingForConfirmationLabel;
@property (weak, nonatomic) IBOutlet UILabel *accessEmailToConfirmLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailNotReceivedLabel;
@property (weak, nonatomic) IBOutlet UIButton *resendEmailButton;
@property (strong, nonatomic) UIBarButtonItem *saveButton;

@end

@implementation ChangeEmailAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    self.emailAddressTextField.delegate = self;
    
    [self configureLayout];
}


#pragma mark - Actions
- (IBAction)sendEmailAgain {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        self.resendEmailButton.enabled = YES;
        [alert show];
    };
    
    service.successCase = ^(NSDictionary *response) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:@"Email reenviado!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        self.resendEmailButton.enabled = YES;
        [alert show];
    };
    
    self.resendEmailButton.enabled = NO;
    [service requestEmailConfirmation];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)saveButtonTouched {
    [self.view endEditing:YES];
    
    if ([self.emailAddressTextField checkIfContentIsValid:YES]) {
        Services *service = [[Services alloc] init];
        
        service.failureCase = ^(NSString *cod, NSString *msg) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:@"Fechar"
                                                  otherButtonTitles:nil];
            self.emailAddressTextField.userInteractionEnabled = YES;
            self.saveButton.enabled = YES;
            [alert show];
        };
        
        service.successCase = ^(NSDictionary *response) {
            self.navigationItem.rightBarButtonItem = nil;
            
            // Exibe a view da confirmação
            [UIView transitionWithView:self.waitingForConfirmationView
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{ self.waitingForConfirmationView.hidden = NO; }
                            completion:NULL];
        };
        
        self.saveButton.enabled = NO;
        self.emailAddressTextField.userInteractionEnabled = NO;
        [service changeEmailAddress:self.emailAddressTextField.text];
    }
}

#pragma mark - Text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailAddressTextField && [self.emailAddressTextField checkIfContentIsValid:YES]) {
        [self saveButtonTouched];
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.emailAddressTextField) {
        [self.emailAddressTextField checkIfContentIsValid:YES];
    }
}

#pragma mark - Layout
- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"E-mail";
    self.saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                    target:self
                                                                    action:@selector(saveButtonTouched)];
    self.navigationItem.rightBarButtonItem = self.saveButton;
    
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    // Configura o text field do email
    [self.emailAddressTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    self.emailAddressTextField.backgroundColor = [UIColor whiteColor];
    [self.emailAddressTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    self.emailAddressTextField.font = [[LayoutManager sharedManager] fontWithSize:16.0];
    [self.emailAddressTextField setIconsImages:[UIImage imageNamed:@"iconMail"]
                                      errorImg:[[UIImage imageNamed:@"iconMail"] withColor:[[LayoutManager sharedManager] red]]];
    self.emailAddressTextField.textColor = [[LayoutManager sharedManager] darkGray];
    self.emailAddressTextField.text = self.currentEmailAddress;
    self.emailAddressTextField.regex = @"^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$";
    
    self.waitingForConfirmationView.hidden = YES;

    // Configura as labels de confirmação do email
    self.waitingForConfirmationLabel.font = [[LayoutManager sharedManager] fontWithSize:18.0];
    self.waitingForConfirmationLabel.textColor = [[LayoutManager sharedManager] lightGreen];
    
    self.accessEmailToConfirmLabel.font = [[LayoutManager sharedManager] fontWithSize:14.0];
    self.accessEmailToConfirmLabel.textColor = [[LayoutManager sharedManager] darkGray];
    
    self.emailNotReceivedLabel.font = [[LayoutManager sharedManager] fontWithSize:14.0];
    self.emailNotReceivedLabel.textColor = [[LayoutManager sharedManager] darkGray];
}

@end
