//
//  FamilyDetailsViewController.h
//  Example
//
//  Created by Adriano Soares on 20/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FamilyDetailsViewController : UIViewController

@property (strong, nonatomic) NSString *cardID;
@property (strong, nonatomic) NSMutableDictionary *sharedDetails;

@end
