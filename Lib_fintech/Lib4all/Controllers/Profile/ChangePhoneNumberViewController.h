//
//  ChangePhoneNumberViewController.h
//  Example
//
//  Created by Cristiano Matte on 01/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePhoneNumberViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, copy) NSString *currentPhoneNumber;

@end
