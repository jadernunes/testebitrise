//
//  ChangePhoneNumberViewController.m
//  Example
//
//  Created by Cristiano Matte on 01/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "ChangePhoneNumberViewController.h"
#import "ErrorTextField.h"
#import "BaseNavigationController.h"
#import "LayoutManager.h"
#import "NSStringMask.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "LoadingViewController.h"
#import "User.h"
#import "UIImage+Color.h"

@interface ChangePhoneNumberViewController ()

@property (weak, nonatomic) IBOutlet ErrorTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIView *challengeView;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet ErrorTextField *challengeTextField;
@property (weak, nonatomic) IBOutlet UILabel *challengeNotReceivedLabel;
@property (weak, nonatomic) IBOutlet UIButton *resendChallengeButton;
@property (strong, nonatomic) UIBarButtonItem *saveButton;
@property (strong, nonatomic) NSString* phoneChangeToken;

@end

@implementation ChangePhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    self.phoneNumberTextField.delegate = self;
    self.challengeTextField.delegate = self;
    
    [self configureLayout];
}

#pragma mark - Actions
- (IBAction)sendChallengeAgain {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        self.resendChallengeButton.enabled = YES;
        [alert show];
    };
    
    service.successCase = ^(NSDictionary *response) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:@"Código reenviado por SMS!"
                                                       delegate:nil
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        self.resendChallengeButton.enabled = YES;
        [alert show];
    };
    
    self.resendChallengeButton.enabled = NO;
    [service resendSMSChallengeForPhoneChangeToken:self.phoneChangeToken];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)saveButtonTouched {
    [self.view endEditing:YES];
    
    if ([self.phoneNumberTextField checkIfContentIsValid:YES]) {
        NSString *cleanPhoneNumber = [NSString stringWithFormat:@"%@%@", @"55", [self getCleanPhoneNumberFromFormattedPhoneNumber:self.phoneNumberTextField.text]];
        
        Services *service = [[Services alloc] init];
        
        service.failureCase = ^(NSString *cod, NSString *msg) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:@"Fechar"
                                                  otherButtonTitles:nil];
            self.phoneNumberTextField.userInteractionEnabled = YES;
            self.saveButton.enabled = YES;
            [alert show];
        };
        
        service.successCase = ^(NSDictionary *response) {
            self.phoneChangeToken = response[PhoneChangeTokenKey];
            
            self.navigationItem.rightBarButtonItem = nil;
            
            // Formata o número de telefone para exibir na label
            NSString *formattedPhone = [self getCleanPhoneNumberFromFormattedPhoneNumber:self.phoneNumberTextField.text];
            formattedPhone = (NSString *)[NSStringMask maskString:formattedPhone withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
            
            NSString *maskedPhone;;
            maskedPhone = [formattedPhone substringToIndex:4];
            maskedPhone = [maskedPhone stringByAppendingString:@"******"];
            maskedPhone = [maskedPhone stringByAppendingString:[formattedPhone substringWithRange:NSMakeRange(formattedPhone.length-2, 2)]];
            
            self.phoneNumberLabel.text = [self.phoneNumberLabel.text stringByReplacingOccurrencesOfString:@"<phone>"
                                                                                               withString:maskedPhone];
            
            // Exibe a view do challenge
            [UIView transitionWithView:self.challengeView
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{ self.challengeView.hidden = NO; }
                            completion:NULL];
        };
        
        self.saveButton.enabled = NO;
        self.phoneNumberTextField.userInteractionEnabled = NO;
        [service setPhoneNumber:cleanPhoneNumber];
    }
}

- (void)checkChallenge:(NSString *)challenge {
    NSString *cleanPhoneNumber = [NSString stringWithFormat:@"%@%@", @"55", [self getCleanPhoneNumberFromFormattedPhoneNumber:self.phoneNumberTextField.text]];
    LoadingViewController *loading = [[LoadingViewController alloc] init];
    [loading startLoading:self title:@"Aguarde..."];
    
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        
        [loading finishLoading:^{
            [alert show];
            [self.challengeTextField showFieldWithError:NO];
        }];
    };
    
    service.successCase = ^(NSDictionary *response) {
        [loading finishLoading:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    };
    
    [service confirmPhoneNumber:cleanPhoneNumber withChallenge:challenge phoneChangeToken:self.phoneChangeToken];
}

#pragma mark - Text field delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.phoneNumberTextField) {
        NSString *cleanNumber = [[[[textField.text stringByReplacingOccurrencesOfString:@"(" withString:@""]
                                  stringByReplacingOccurrencesOfString:@")" withString:@""]
                                 stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if ([string isEqualToString:@""] && range.location != 0) {
            textField.text = (NSString *)[NSStringMask maskString:cleanNumber withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
            return YES;
        }
        
        if (cleanNumber.length < 11) {
            textField.text = (NSString *)[NSStringMask maskString:cleanNumber withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
            return YES;
        } else {
            return NO;
        }
    } else if (textField == self.challengeTextField) {
        [self.challengeTextField showFieldWithError:YES];
        
        NSString* challenge = [textField text];
        challenge = [challenge stringByReplacingCharactersInRange:range withString:string];
        
        NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:@"^[0-9]{0,6}$" options:0 error:nil];
        int numberOfMatches = (int)[regex numberOfMatchesInString:challenge options:0 range:NSMakeRange(0, challenge.length)];
        
        if (![challenge isEqualToString:@""] && numberOfMatches == 0) {
            return NO;
        }
        
        if (challenge.length == 6) {
            [textField resignFirstResponder];
            [self checkChallenge:challenge];
            self.challengeTextField.text = challenge;
        }
        
        return challenge.length < 6 ? YES : NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.phoneNumberTextField) {
        [self.phoneNumberTextField checkIfContentIsValid:YES];
    }
}

- (NSString *)getCleanPhoneNumberFromFormattedPhoneNumber:(NSString *)formattedPhoneNumber {
    NSString *cleanNumber = [[[[formattedPhoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""]
                             stringByReplacingOccurrencesOfString:@")" withString:@""]
                             stringByReplacingOccurrencesOfString:@"-" withString:@""]
                             stringByReplacingOccurrencesOfString:@" " withString:@""];

    return cleanNumber;
}

#pragma mark - Layout
- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Telefone";
    self.saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                   target:self
                                                                   action:@selector(saveButtonTouched)];
    self.navigationItem.rightBarButtonItem = self.saveButton;
    
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    // Configura o text field do telefone
    [self.phoneNumberTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    self.phoneNumberTextField.backgroundColor = [UIColor whiteColor];
    [self.phoneNumberTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    self.phoneNumberTextField.font = [[LayoutManager sharedManager] fontWithSize:16.0];
    [self.phoneNumberTextField setIconsImages:[UIImage imageNamed:@"iconMobi"]
                                     errorImg:[[UIImage imageNamed:@"iconMobi"] withColor:[[LayoutManager sharedManager] red]]];
    self.phoneNumberTextField.textColor = [[LayoutManager sharedManager] darkGray];
    self.phoneNumberTextField.text = self.currentPhoneNumber;
    self.phoneNumberTextField.regex = @"^\\d{11}$";
    
    self.challengeView.hidden = YES;
    
    // Configura label do código enviado
    self.phoneNumberLabel.font = [[LayoutManager sharedManager] fontWithSize:14.0];
    self.phoneNumberLabel.textColor = [[LayoutManager sharedManager] darkGray];
    self.challengeNotReceivedLabel.font = [[LayoutManager sharedManager] fontWithSize:14.0];
    self.challengeNotReceivedLabel.textColor = [[LayoutManager sharedManager] darkGray];
    
    // Configura o text field do desafio
    [self.challengeTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    self.challengeTextField.backgroundColor = [UIColor whiteColor];
    [self.challengeTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    self.challengeTextField.font = [[LayoutManager sharedManager] fontWithSize:16.0];
    self.challengeTextField.placeholder = @"000000";
    //self.challengeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"000000"
    //                                                                                attributes:@{NSForegroundColorAttributeName: [[LayoutManager sharedManager] lightGray]}];
    [self.challengeTextField setIconsImages:[UIImage imageNamed:@"iconMessage"]
                                   errorImg:[[UIImage imageNamed:@"iconMessage"] withColor:[[LayoutManager sharedManager] red]]];
    self.challengeTextField.textColor = [[LayoutManager sharedManager] darkGray];
    self.challengeTextField.regex = @"^[0-9]{6}$";
}

@end
