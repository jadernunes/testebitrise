//
//  FamilyMaxTransactionsDataField.h
//  Example
//
//  Created by Adriano Soares on 26/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FamilyDataFieldProtocol.h"

@interface FamilyMaxTransactionsDataField : NSObject <FamilyDataFieldProtocol>

@end
