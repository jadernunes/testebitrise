//
//  FamilyPerTransactionDataField.h
//  Example
//
//  Created by Adriano Soares on 20/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FamilyDataFieldProtocol.h"

@interface FamilyPerTransactionDataField : NSObject <FamilyDataFieldProtocol>

@end
