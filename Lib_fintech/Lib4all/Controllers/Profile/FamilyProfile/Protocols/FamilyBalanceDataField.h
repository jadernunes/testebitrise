//
//  FamilyBalanceDataField.h
//  Example
//
//  Created by Adriano Soares on 23/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FamilyDataFieldViewController.h"

@interface FamilyBalanceDataField : NSObject <FamilyDataFieldProtocol>

@end
