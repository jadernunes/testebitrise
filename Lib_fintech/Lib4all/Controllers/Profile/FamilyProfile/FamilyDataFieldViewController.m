//
//  FamilyDataFieldViewController.m
//  Example
//
//  Created by Adriano Soares on 20/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "FamilyDataFieldViewController.h"
#import "ErrorTextField.h"
#import "BaseNavigationController.h"
#import "LayoutManager.h"
#import "UIImage+Color.h"


@interface FamilyDataFieldViewController ()
@property (strong, nonatomic) LayoutManager *LM;

@property (weak, nonatomic) IBOutlet UILabel *currentLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet ErrorTextField *dataTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property CGFloat oldBottomConstant;


@end

@implementation FamilyDataFieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.LM = [LayoutManager sharedManager];
    // Do any additional setup after loading the view.
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    self.oldBottomConstant = self.bottomConstraint.constant;
    
    
    [self configureLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.dataTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [self.dataTextField resignFirstResponder];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void) configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    
    if (self.dataFieldProtocol != nil) {
        self.navigationItem.title = [self.dataFieldProtocol navigationTitle];
        self.currentLabel.text    = [self.dataFieldProtocol currentLabel];
        self.titleLabel.text      = [self.dataFieldProtocol title];

        self.dataTextField.placeholder      = _dataFieldProtocol.textFieldPlaceHolder;
        self.dataTextField.keyboardType     = _dataFieldProtocol.keyboardType;
        self.dataTextField.delegate         = _dataFieldProtocol;
        
        if ([self.dataFieldProtocol textFieldImageName]) {
            [self.dataTextField setIconsImages:[UIImage imageNamed:_dataFieldProtocol.textFieldImageName]
                                      errorImg:[[UIImage imageNamed:_dataFieldProtocol.textFieldWithErrorImageName] withColor:[[LayoutManager sharedManager] red]]];
        
        }
        
        if (self.data) {
            self.currentValueLabel.text = [self.dataFieldProtocol currentValueFormatted:self.data];
        } else {
            self.currentValueLabel.text = @"Indefinido";
        }
    }
    

    self.view.backgroundColor = self.LM.backgroundColor;
    
    self.currentLabel.font = [self.LM fontWithSize:self.LM.regularFontSize];
    self.currentLabel.textColor = self.LM.darkGray;
    
    self.currentValueLabel.font = [self.LM fontWithSize:self.LM.regularFontSize];
    self.currentValueLabel.textColor = self.LM.lightGreen;
    
    self.titleLabel.font = [self.LM fontWithSize:self.LM.regularFontSize];
    self.titleLabel.textColor = self.LM.darkGray;
    
    [self.dataTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    [self.dataTextField setBorder:[self.LM lightGray] width:1];
    [self.dataTextField setFont  :[self.LM fontWithSize:self.LM.regularFontSize]];
    self.dataTextField.textColor = [self.LM darkGray];
    
 
}

- (IBAction)saveButton:(id)sender {
    if ([self.dataFieldProtocol isDataValid:self.dataTextField.text]) {
        NSString *formattedData = [self.dataFieldProtocol serverFormattedData:self.dataTextField.text];
        [self.dataTextField showFieldWithError:YES];
        if (self.isCreation) {
            if (self.completion != nil) {
                self.completion(formattedData);
            }
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self.dataFieldProtocol saveData:self
                                        data:formattedData
                              withCompletion:^(NSString *data) {
                                  
                if (self.completion) {
                    self.completion(data);
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
        
    } else {
        [self.dataTextField showFieldWithError:NO];
    }

}

- (IBAction)cancelButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;

    [UIView animateWithDuration:0.4 animations:^{
        self.bottomConstraint.constant = self.oldBottomConstant +  keyboardSize.height;
        [self.view updateConstraints];
    }];
    
    
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.4 animations:^{
        self.bottomConstraint.constant = self.oldBottomConstant;
        [self.view updateConstraints];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
