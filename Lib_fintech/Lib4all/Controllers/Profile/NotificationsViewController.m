//
//  NotificationsViewController.m
//  Example
//
//  Created by Cristiano Matte on 27/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "NotificationsViewController.h"
#import "LayoutManager.h"
#import "BaseNavigationController.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "Preferences.h"

@interface NotificationsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *receiveByEmailLabel;
@property (weak, nonatomic) IBOutlet UISwitch *receivePaymentEmailsSwitch;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
    
    self.receivePaymentEmailsSwitch.on = [[Preferences sharedPreferences] receivePaymentEmails];
    
    Services *service = [[Services alloc] init];
    
    service.successCase = ^(NSDictionary *response) {
        BOOL receivePaymentEmails = [[response objectForKey:ReceivePaymentEmailsKey] boolValue];
        [self.receivePaymentEmailsSwitch setOn:receivePaymentEmails animated:YES];
    };
    
    service.failureCase = ^(NSString *cod, NSString *msg){ };
    
    [service getAccountPreferences:@[ReceivePaymentEmailsKey]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.receivePaymentEmailsSwitch.on != [[Preferences sharedPreferences] receivePaymentEmails]) {
        NSDictionary *preferences;
        if (self.receivePaymentEmailsSwitch.on) {
            preferences = @{ReceivePaymentEmailsKey: @YES};
        } else {
            preferences = @{ReceivePaymentEmailsKey: @NO};
        }
        
        Services *service = [[Services alloc] init];
        service.successCase = ^(NSDictionary *response) { };
        service.failureCase = ^(NSString *cod, NSString *msg) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:@"Fechar"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        [service setAccountPreferences:preferences];
    }
}

- (void)closeButtonTouched {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Layout
- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Configurações";
    
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    self.separatorView.backgroundColor = [[LayoutManager sharedManager] lightGreen];
    
    self.receiveByEmailLabel.font = [[LayoutManager sharedManager] fontWithSize:16.0];
    self.receiveByEmailLabel.textColor = [[LayoutManager sharedManager] darkGray];
    
    // Configura botão de fechar se a view for apresentada modalmente
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count == 1) {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Fechar"
                                                                        style:UIBarButtonItemStyleDone
                                                                       target:self
                                                                       action:@selector(closeButtonTouched)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
}

@end
