//
//  ChangeEmailAddressViewController.h
//  Example
//
//  Created by Cristiano Matte on 02/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeEmailAddressViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, copy) NSString *currentEmailAddress;

@end
