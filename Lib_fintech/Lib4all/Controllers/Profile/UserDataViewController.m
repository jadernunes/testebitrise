//
//  UserDataTableViewController.m
//  Example
//
//  Created by 4all on 5/27/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "UserDataViewController.h"
#import "LayoutManager.h"
#import "User.h"
#import "BaseNavigationController.h"
#import "NSStringMask.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "ChangePhoneNumberViewController.h"
#import "ChangeEmailAddressViewController.h"
#import "LoadingViewController.h"
#import "ErrorTextField.h"
#import "UITextFieldMask.h"
#import "NSString+Mask.h"
#import "NSString+NumberArray.h"
#import "CpfCnpjUtil.h"
#import "DateUtil.h"
#import "UIImage+Color.h"

@interface UserDataViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) ErrorTextField *phoneTextField;
@property (strong, nonatomic) ErrorTextField *emailTextField;
@property (strong, nonatomic) ErrorTextField *fullNameTextField;
@property (strong, nonatomic) ErrorTextField *cpfTextField;
@property (strong, nonatomic) UITextFieldMask *birthdateTextField;
@property (strong, nonatomic) ErrorTextField *employerTextField;
@property (strong, nonatomic) ErrorTextField *jobPositionTextField;

@property (strong, nonatomic) NSMutableArray *activeFields;

@end

@implementation UserDataViewController

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
    
    // Adiciona texto nos campos com os dados do usuário
    User *user = [User sharedUser];
    self.fullNameTextField.text = user.fullName;
    [self setCpfTextFieldText:user.cpf];
    [self setBirthdateTextFieldText:user.birthdate];
    self.employerTextField.text = user.employer;
    self.jobPositionTextField.text = user.jobPosition;
    
    // Atualiza os dados buscando no servidor
    Services *service = [[Services alloc] init];
    service.successCase = ^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *data;
            
            if ((data = (NSString *)[response objectForKey:PhoneNumberKey]) && ![data isEqual:[NSNull null]]) {
                self.phoneTextField.text = [self getTextFieldFormattedPhoneFromPhoneString:data];
            }
            
            if ((data = (NSString *)[response objectForKey:EmailAddressKey]) && ![data isEqual:[NSNull null]]) {
                self.emailTextField.text = data;
            }
            
            if ((data = (NSString *)[response objectForKey:FullNameKey]) && ![data isEqual:[NSNull null]]) {
                self.fullNameTextField.text = data;
            }
            
            if ((data = (NSString *)[response objectForKey:CPFKey]) && ![data isEqual:[NSNull null]]) {
                [self setCpfTextFieldText:data];
            }
            
            if ((data = (NSString *)[response objectForKey:BirthdateKey]) && ![data isEqual:[NSNull null]]) {
                [self setBirthdateTextFieldText:user.birthdate];
            }
            
            if ((data = (NSString *)[response objectForKey:EmployerKey]) && ![data isEqual:[NSNull null]]) {
                self.employerTextField.text = data;
            }
            
            if ((data = (NSString *)[response objectForKey:JobPositionKey]) && ![data isEqual:[NSNull null]]) {
                self.jobPositionTextField.text = data;
            }
        });
    };
    service.failureCase = ^(NSString *cod, NSString *msg){ };
    [service getAccountData:@[PhoneNumberKey, EmailAddressKey, CpfKey, FullNameKey, BirthdateKey, EmployerKey, JobPositionKey]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Atualiza o telefone e email sempre que a tela aparecer pois eles são alterados em outras telas
    User *user = [User sharedUser];
    self.phoneTextField.text = [self getTextFieldFormattedPhoneFromPhoneString:user.phoneNumber];
    self.emailTextField.text = user.emailAddress;
}

// MARK: - Actions

- (void)editButtonTouched:(UIButton *)sender {
    if (sender.superview == self.phoneTextField) {
        [self performSegueWithIdentifier:@"ChangePhoneNumberSegue" sender:nil];
    } else if (sender.superview == self.emailTextField) {
        [self performSegueWithIdentifier:@"ChangeEmailAddressSegue" sender:nil];
    } else {
        // Altera o botão para o estado de salvar
        [sender setTitle:@"salvar" forState:UIControlStateNormal];
        [sender removeTarget:self action:@selector(editButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
        [sender addTarget:self action:@selector(saveButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
        
        [(ErrorTextField *)sender.superview becomeFirstResponder];
    }

    return;
}

- (void)saveButtonTouched:(UIButton *)sender {
    if (sender == nil) {
        return;
    }
    
    // Checa a validade do campo inserido
    BOOL isValidData = YES;
    ErrorTextField *textField = (ErrorTextField *)sender.superview;
    if (textField == self.employerTextField || textField == self.jobPositionTextField) {
        isValidData = textField.text.length > 1;
    } else if (textField == self.fullNameTextField) {
        isValidData = [self.fullNameTextField.text componentsSeparatedByString:@" "].count > 1;
    } else if (textField == self.cpfTextField) {
        NSArray *cpfCnpj = [[CpfCnpjUtil getClearCpfOrCnpjNumberFromMaskedNumber:self.cpfTextField.text] toNumberArray];
        isValidData =  [CpfCnpjUtil isValidCpfOrCnpj:cpfCnpj];
    } else if (textField == self.birthdateTextField) {
        isValidData = [DateUtil isValidBirthdateString:self.birthdateTextField.text];
    }

    // Exibe alerta para confirmar a alteração do dado
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                   message:@"Deseja realmente salvar este dado?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancelar"
                                              style:UIAlertActionStyleCancel
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                // Finaliza a edição do campo
                                                [sender.superview resignFirstResponder];
                                                
                                                // Altera o botão para o estado de edição
                                                [sender setTitle:@"editar" forState:UIControlStateNormal];
                                                [sender removeTarget:self action:@selector(saveButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                                                [sender addTarget:self action:@selector(editButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                                                
                                                User *user = [User sharedUser];
                                                
                                                // Se o usuário cancelou a alteração, atribui o dado original ao campo
                                                if (textField == self.fullNameTextField) {
                                                    textField.text = user.fullName;
                                                } else if (textField == self.cpfTextField) {
                                                    [self setCpfTextFieldText:user.cpf];
                                                } else if (textField == self.birthdateTextField) {
                                                    [self setBirthdateTextFieldText:user.birthdate];
                                                } else if (textField == self.employerTextField) {
                                                    textField.text = user.employer;
                                                } else if (textField == self.jobPositionTextField) {
                                                    textField.text = user.jobPosition;
                                                }
                                            }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Salvar"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * _Nonnull action) {
                                                // Se o dado inserido é inválido, alerta o usuário
                                                if (!isValidData) {
                                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                                                                   message:@"Por favor, verifique o dado inserido."
                                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                                    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                                              style:UIAlertActionStyleDefault
                                                                                            handler:nil]];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                    return;
                                                }
                                                
                                                // Finaliza a edição do campo
                                                [sender.superview resignFirstResponder];
                                                
                                                // Altera o botão para o estado de edição
                                                [sender setTitle:@"editar" forState:UIControlStateNormal];
                                                [sender removeTarget:self action:@selector(saveButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                                                [sender addTarget:self action:@selector(editButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                                                
                                                // Formata o dado para envio ao servidor
                                                NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                                                
                                                if (textField == self.fullNameTextField) {
                                                    data[FullNameKey] = self.fullNameTextField.text;
                                                } else if (textField == self.cpfTextField) {
                                                    data[CPFKey] = [CpfCnpjUtil getClearCpfOrCnpjNumberFromMaskedNumber:self.cpfTextField.text];
                                                } else if (textField == self.birthdateTextField) {
                                                    data[BirthdateKey] = [DateUtil convertDateString:self.birthdateTextField.text fromFormat:@"dd/MM/yyyy" toFormat:@"yyyy-MM-dd"];
                                                } else if (textField == self.employerTextField) {
                                                    data[EmployerKey] = self.employerTextField.text;
                                                } else if (textField == self.jobPositionTextField) {
                                                    data[JobPositionKey] = self.jobPositionTextField.text;
                                                }
                                                
                                                // Envia o dado alterado ao servidor
                                                LoadingViewController *loadingViewController = [[LoadingViewController alloc] init];
                                                Services *service = [[Services alloc] init];
                                                service.successCase = ^(NSDictionary *response) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [loadingViewController finishLoading:nil];
                                                    });
                                                };
                                                
                                                service.failureCase = ^(NSString *cod, NSString *msg) {
                                                    // Em caso de erro ao alterar o dado, exibe alerta para o usuário
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [loadingViewController finishLoading:^{
                                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                                                                           message:msg
                                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                                            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                                                      style:UIAlertActionStyleDefault
                                                                                                    handler:nil]];
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                        }];
                                                    });
                                                };
                                                
                                                [loadingViewController startLoading:self title:@"Aguarde..."];
                                                [service setAccountData:data];
                                            }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)closeButtonTouched {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setCpfTextFieldText:(NSString *)text {
    NSString *mask;
    
    if (text.length <= 11) {
        mask = @"###.###.###-##";
    } else {
        mask = @"##.###.###/####-##";
    }
    
    self.cpfTextField.text = [text stringByApplyingMask:mask maskCharacter:'#'];
}

- (void)setBirthdateTextFieldText:(NSString *)text {
    // Converte a data de nascimento do formato de data do servidor para o formato dd/mm/aaaa
    self.birthdateTextField.text = [DateUtil convertDateString:text fromFormat:@"yyyy-MM-dd" toFormat:@"dd/MM/yyyy"];
}

// MARK: - Keyboard handling

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    // viewHighestY calcula a posição Y do bottom da view mais abaixo da tela
    CGFloat viewHighestY = self.scrollView.subviews[0].frame.origin.y + self.scrollView.subviews[0].frame.size.height;
    CGFloat keyboardOriginY = (self.view.frame.size.height - self.view.frame.origin.y) - kbRect.size.height;
    
    // Se o teclado esconde alguma parte da view, adiciona scroll
    if (viewHighestY > keyboardOriginY) {
        // Adiciona inset to tamanho que o teclado está escondendo
        UIEdgeInsets contentInset = self.scrollView.contentInset;
        contentInset.bottom = kbRect.size.height + 20;
        self.scrollView.contentInset = contentInset;
        self.scrollView.scrollIndicatorInsets = contentInset;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self.scrollView setContentOffset:CGPointZero animated:NO];
    self.scrollView.contentInset = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

// MARK: - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ChangePhoneNumberSegue"]) {
        ChangePhoneNumberViewController *viewController = (ChangePhoneNumberViewController *)segue.destinationViewController;
        viewController.currentPhoneNumber = self.phoneTextField.text;
    } else if ([segue.identifier isEqualToString:@"ChangeEmailAddressSegue"]) {
        ChangeEmailAddressViewController *viewController = (ChangeEmailAddressViewController *)segue.destinationViewController;
        viewController.currentEmailAddress = self.emailTextField.text;
    }
}

// MARK: - Text field delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // O campo só pode ser editado quando estiver no estado de edição
    if ([[(UIButton *)textField.rightView currentTitle] isEqualToString:@"salvar"]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField ==  self.cpfTextField) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        // Permite backspace apenas com cursor no último caractere
        if (range.length == 1 && string.length == 0 && range.location != newString.length) {
            textField.selectedTextRange = [textField textRangeFromPosition:textField.endOfDocument toPosition:textField.endOfDocument];
            return NO;
        }
        
        newString = [CpfCnpjUtil getClearCpfOrCnpjNumberFromMaskedNumber:newString];
        
        if (newString.length <= 11) {
            textField.text = [newString stringByApplyingMask:@"###.###.###-##" maskCharacter:'#'];
        } else {
            textField.text = [newString stringByApplyingMask:@"##.###.###/####-##" maskCharacter:'#'];
        }
        
        return NO;
    }
    
    return YES;
}

// MARK: - Layout

- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Meus dados";

    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    // Verifica os campos existentes e cria um text field para eles
    self.activeFields = [[NSMutableArray alloc] init];
    User *user = [User sharedUser];
    
    if (user.fullName != nil && ![user.fullName isEqualToString:@""]) {
        self.fullNameTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.fullNameTextField];
        [self.fullNameTextField setIconsImages:[UIImage imageNamed:@"iconUserData"]
                                      errorImg:[[UIImage imageNamed:@"iconUserData"] withColor:[[LayoutManager sharedManager] red]]];
    }
    if (user.phoneNumber != nil && ![user.phoneNumber isEqualToString:@""]) {
        self.phoneTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.phoneTextField];
        [self.phoneTextField setIconsImages:[UIImage imageNamed:@"iconMobi"]
                                   errorImg:[[UIImage imageNamed:@"iconMobi"] withColor:[[LayoutManager sharedManager] red]]];
    }
    if (user.emailAddress != nil && ![user.emailAddress isEqualToString:@""]) {
        self.emailTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.emailTextField];
        [self.emailTextField setIconsImages:[UIImage imageNamed:@"iconMail"]
                                   errorImg:[[UIImage imageNamed:@"iconMail"] withColor:[[LayoutManager sharedManager] red]]];
    }
    if (user.cpf != nil && ![user.cpf isEqualToString:@""]) {
        self.cpfTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.cpfTextField];
        [self.cpfTextField setIconsImages:[UIImage imageNamed:@"iconCpf"]
                                 errorImg:[[UIImage imageNamed:@"iconCpf"] withColor:[[LayoutManager sharedManager] red]]];
        self.cpfTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    if (user.birthdate != nil && ![user.birthdate isEqualToString:@""]) {
        self.birthdateTextField = [[UITextFieldMask alloc] init];
        [self.activeFields addObject:self.birthdateTextField];
        [self.birthdateTextField setIconsImages:[UIImage imageNamed:@"iconStar"]
                                       errorImg:[[UIImage imageNamed:@"iconStar"] withColor:[[LayoutManager sharedManager] red]]];
        self.birthdateTextField.keyboardType = UIKeyboardTypeNumberPad;
        self.birthdateTextField.mask = [NSStringMask maskWithPattern:@"(\\d{2})/(\\d{2})/(\\d{4})"];
    }
    if (user.employer != nil && ![user.employer isEqualToString:@""]) {
        self.employerTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.employerTextField];
        [self.employerTextField setIconsImages:[UIImage imageNamed:@"iconEmployer"]
                                      errorImg:[[UIImage imageNamed:@"iconEmployer"] withColor:[[LayoutManager sharedManager] red]]];
    }
    if (user.jobPosition != nil && ![user.jobPosition isEqualToString:@""]) {
        self.jobPositionTextField = [[ErrorTextField alloc] init];
        [self.activeFields addObject:self.jobPositionTextField];
        [self.jobPositionTextField setIconsImages:[UIImage imageNamed:@"iconJobPosition"]
                                         errorImg:[[UIImage imageNamed:@"iconJobPosition"] withColor:[[LayoutManager sharedManager] red]]];
    }
    
    if (self.activeFields.count > 0) {
        UIView *topView;
        
        // Adiciona as constraints dos campos
        NSMutableArray *constraints = [[NSMutableArray alloc] init];
        for (int i = 0; i < self.activeFields.count; i++) {
            UIView *textField = self.activeFields[i];
            textField.translatesAutoresizingMaskIntoConstraints = NO;
            
            [self.contentView addSubview:textField];
            if (i == 0) {
                [constraints addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[textField(47)]"
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{@"textField":textField}]];
            } else {
                [constraints addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView]-(-1)-[textField(47)]"
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{@"topView":topView,
                                                                                                    @"textField":textField}]];
            }
            
            [constraints addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[textField]-20-|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:@{@"textField":textField}]];
            
            topView = textField;
        }
        
        [constraints addObjectsFromArray: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView]-0-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:@{@"topView":topView}]];
        [NSLayoutConstraint activateConstraints:constraints];
        
        // Após adicionar as constraints, configura o layout dos campos
        [self.view layoutIfNeeded];
        for (int i = 0; i < self.activeFields.count; i++) {
            ErrorTextField *textField = self.activeFields[i];
            textField.clipsToBounds = YES;
            textField.backgroundColor = [UIColor whiteColor];
            textField.font = [[LayoutManager sharedManager] fontWithSize:15];
            textField.textColor = [[LayoutManager sharedManager] darkGray];
            //textField.userInteractionEnabled = NO;
            textField.delegate = self;
            
            if (i == 0) {
                [textField roundTopCornersRadius:5];
            } else if (i == self.activeFields.count - 1){
                [textField roundBottomCornersRadius:5];
            }
            
            [textField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
            //Campo CPF não pode ser editado
            if (textField == self.cpfTextField) {
                continue;
            }
            
            //Campo data de Nascimento não pode ser editado
            if (textField == self.birthdateTextField) {
                continue;
            }
            
            BOOL shouldAddDisclosure = (textField == self.emailTextField) || (textField == self.phoneTextField);
            UIButton *editButton = [self generateEditButtonWithDisclosure:shouldAddDisclosure];
            [editButton addTarget:self action:@selector(editButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
            textField.rightViewMode = UITextFieldViewModeAlways;
            textField.rightView = editButton;
        }
    }
    
    // Configura botão de fechar se a view for apresentada modalmente
    NSArray *viewControllers = self.navigationController.viewControllers;
    if (viewControllers.count == 1) {
        UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Fechar"
                                                                        style:UIBarButtonItemStyleDone
                                                                       target:self
                                                                       action:@selector(closeButtonTouched)];
        self.navigationItem.leftBarButtonItem = closeButton;
    }
}

- (UIButton *)generateEditButtonWithDisclosure:(BOOL)shouldAddDisclosure {
    NSString* buttonTitle = @"editar";
    
    // Obtém o tamanho de frame necessário para comportar o título do botão
    CGSize titleSize = [buttonTitle sizeWithAttributes:@{NSFontAttributeName: [[LayoutManager sharedManager] fontWithSize:15.0]}];
    
    UIButton *editButton = [[UIButton alloc] init];
    editButton.titleLabel.font = [[LayoutManager sharedManager] fontWithSize:15.0];
    [editButton setTitleColor:[[LayoutManager sharedManager] lightGreen] forState:UIControlStateNormal];
    [editButton setTitleColor:[[LayoutManager sharedManager] darkGreen] forState:UIControlStateHighlighted];
    
    [editButton setTitle:buttonTitle forState:UIControlStateNormal];
    [editButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    /*
     * Cria uma image view para a imagem de disclosure com altura e largura iguais à altura do título do botão.
     * O ponto x da image view é o ponto x final do título do botão.
     */
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(titleSize.width, 0, titleSize.height, titleSize.height)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    /*
     * Instancia o botão com a altura do título e largura igual à soma
     * da largura do título, da image view e de um padding de 5.0
     */
    editButton.frame = CGRectMake(0, 0, titleSize.width + imageView.frame.size.width + 5, titleSize.height);
    [editButton addSubview:imageView];
    
    if (shouldAddDisclosure) {
        imageView.image = [UIImage imageNamed:@"disclosure"];
    }
    
    return editButton;
}

- (NSString *)getTextFieldFormattedPhoneFromPhoneString:(NSString *)phoneString {
    NSString* phone = [phoneString substringFromIndex:2];
    
    if (phone.length <= 10){
        phone = (NSString *)[NSStringMask maskString:phone withPattern:@"\\((\\d{2})\\) (\\d{4})-(\\d{4})"];
    } else {
        phone = (NSString *)[NSStringMask maskString:phone withPattern:@"\\((\\d{2})\\) (\\d{4,5})-(\\d{4})"];
    }
    
    return phone;
}

@end
