//
//  AddCardViewController.m
//  Example
//
//  Created by Cristiano Matte on 09/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "AddCardViewController.h"
#import "LayoutManager.h"
#import "UITextFieldMask.h"
#import "User.h"
#import "Services.h"
#import "CreditCard.h"
#import "ServicesConstants.h"
#import "LoadingViewController.h"
#import "CardsTableViewController.h"
#import "BaseNavigationController.h"
#import "CardUtil.h"
#import "Lib4allPreferences.h"
#import "NSString+Mask.h"
#import "NSString+NumberArray.h"
#import "DateUtil.h"
#import "CpfCnpjUtil.h"
#import "CreditCardsList.h"
#import "LocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import "UIImage+Color.h"
#import "AcceptedCardsViewController.h"

@interface AddCardViewController () <UIActionSheetDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardViewTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet ErrorTextField *cardTypeTextField;
@property (weak, nonatomic) IBOutlet ErrorTextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextFieldMask *expDateTextField;
@property (weak, nonatomic) IBOutlet ErrorTextField *holderNameTextField;
@property (weak, nonatomic) IBOutlet UITextFieldMask *cvvTextField;
@property (weak, nonatomic) IBOutlet UIButton *acceptedCardsButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property LoadingViewController *loadingView;
@property CardType selectedType;
@property NSArray * acceptedPaymentTypes;
@property NSArray * acceptedBrands;
@property int currentTextFieldTag;

@end

@implementation AddCardViewController

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
    
    if (self.acceptedPaymentTypes == nil)
        self.acceptedPaymentTypes = [[Lib4allPreferences sharedInstance] acceptedPaymentTypes];
    
    if (self.acceptedBrands == nil)
        self.acceptedBrands = [[[Lib4allPreferences sharedInstance] acceptedBrands] allObjects];
    
    self.selectedType = 0;
    
    /*
     Alteração Bruno e Adriano 7/2/2017
     Agora, quando a aplicação aceitar somente Crédito, pode aceitar cartões do tipo Crédito e "Crédito e Débito"
     Agora, quando a aplicação aceitar somente Débito, pode aceitar cartões do tipo Débito e "Crédito e Débito"
     
    if (self.acceptedPaymentTypes.count == 1) {
        if ([self.acceptedPaymentTypes containsObject:@(Credit)]) {
            self.selectedType = PaymentModeCredit;
            self.cardTypeTextField.text = @"Crédito";
        } else if ([self.acceptedPaymentTypes containsObject:@(Debit)]) {
            self.selectedType = PaymentModeDebit;
            self.cardTypeTextField.text = @"Débito";
        } else
            NSAssert(false, @"Tipo de pagamento não reconhecido!");
    }
    */

    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _loadingView = [[LoadingViewController alloc] init];
}

- (void)setAccceptedPaymentTypes: (NSArray *) paymentTypes andAcceptedBrands: (NSArray *) brands {
    self.acceptedPaymentTypes = paymentTypes;
    self.acceptedBrands = brands;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    self.navigationItem.title = @"";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureLayout];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationItem.title = @"Cartão";
}

// MARK: - Actions

- (IBAction)addCard {
    BOOL cardNumberValid = [self.cardNumberTextField checkIfContentIsValid:YES];
    BOOL holderNameValid = self.holderNameTextField.text.length == 0 || [self.holderNameTextField checkIfContentIsValid:YES];
    BOOL expirationDateValid = [self.expDateTextField checkIfContentIsValid:YES];
    BOOL cvvValid = [self.cvvTextField checkIfContentIsValid:YES];
    BOOL cardTypeValid = self.selectedType != 0;
    
    if (!cardTypeValid) {
        [self.cardTypeTextField showFieldWithError:NO];
    }
    
    if (!holderNameValid) {
        [self.holderNameTextField showFieldWithError:NO];
    }
    
    // Obtém a bandeira do cartão inserido
    NSNumber *cardBrand = [NSNumber numberWithInt:[CardUtil getBrandWithCardNumber:self.cardNumberTextField.text]];
    
    if (cardNumberValid && holderNameValid && expirationDateValid && cvvValid && cardTypeValid) {
        // Se a bandeira do cartão inserido não for aceita ou o cartão for de uma modalidade não aceita, exibe uma mensagem de erro
        if (![self cardIsAcceptedWithType:self.selectedType andBrand:[cardBrand integerValue]]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                           message:@"Este cartão não é aceito neste aplicativo. Por favor, escolha outro cartão."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        
        BOOL requestLocalizationPermission = ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) &&
                                             ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) &&
                                             ([[Lib4allPreferences sharedInstance].requiredAntiFraudItems[@"geolocation"] isEqual: @YES]);
        
        if (requestLocalizationPermission) {
            if (!_loadingView.isLoading) {
                [_loadingView startLoading:self title:@"Aguarde..."];
            }
            
            [[LocationManager sharedManager] updateLocationWithCompletion:^(BOOL success, NSDictionary *location) {
                if (success) {
                    [self callRequestVaultKey];
                } else {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                                   message:@"Não foi possível obter sua localização. Ative os serviços de localização nos Ajustes."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:ok];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_loadingView finishLoading:^{
                            [self presentViewController:alert animated:YES completion:nil];
                        }];
                    });
                }
            }];
        } else {
            [self callRequestVaultKey];
        }
    }
}

- (IBAction)closeAction:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)acceptedCardsButtonTouched {
    UIViewController *vc =  [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil] instantiateViewControllerWithIdentifier:@"AcceptedCardsViewController"];
    
    [((AcceptedCardsViewController *) vc) setAcceptedBrands:self.acceptedBrands];
    
    [self presentViewController:vc animated:YES completion:nil];
}

// MARK: - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.holderNameTextField) {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (newString.length <= 26) {
            return YES;
        } else {
            return NO;
        }
    }
    
    //Card Number Field
    if (textField == self.cardNumberTextField) {
        int cleanLenght = (int)[textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length;

        //Size limit
        if (cleanLenght == 18 && ![string isEqualToString:@""]){
            return NO;
        }
        
        
        if([string isEqualToString:@""] == NO
           && cleanLenght % 4 == 0 &&
           cleanLenght > 0 &&
           cleanLenght<16) {
            textField.text = [textField.text stringByAppendingString:@" "];
        }else if ([string isEqualToString:@""] &&
                   cleanLenght % 4 == 0 &&
                  textField.text.length > 0){
            //remove automatically the space
            textField.text = [textField.text substringToIndex:textField.text.length-1];
        }
    }
    
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.cardTypeTextField) {
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *creditAndDebit = [UIAlertAction actionWithTitle:@"Crédito e Débito"
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * _Nonnull action) {
                                                                   self.cardTypeTextField.text = action.title;
                                                                   self.selectedType = CardTypeCreditAndDebit;
                                                                   [self.cardTypeTextField showFieldWithError:YES];
                                                                   [self.cardView sendSubviewToBack:self.cardTypeTextField];
                                                               }];
        UIAlertAction *debit = [UIAlertAction actionWithTitle:@"Débito"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          self.cardTypeTextField.text = action.title;
                                                          self.selectedType = CardTypeDebit;
                                                          [self.cardTypeTextField showFieldWithError:YES];
                                                          [self.cardView sendSubviewToBack:self.cardTypeTextField];
                                                      }];
        UIAlertAction *credit = [UIAlertAction actionWithTitle:@"Crédito"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           self.cardTypeTextField.text = action.title;
                                                           self.selectedType = CardTypeCredit;
                                                           [self.cardTypeTextField showFieldWithError:YES];
                                                           [self.cardView sendSubviewToBack:self.cardTypeTextField];
                                                       }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                         style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           if (self.selectedType == 0) {
                                                               [self.cardTypeTextField showFieldWithError:NO];
                                                               [self.cardView bringSubviewToFront:self.cardTypeTextField];
                                                           }
                                                       }];
       
        /*
         Alteração Bruno e Adriano 7/2/2017
         Agora, quando a aplicação aceitar somente Crédito, pode aceitar cartões do tipo Crédito e "Crédito e Débito"
         Agora, quando a aplicação aceitar somente Débito, pode aceitar cartões do tipo Débito e "Crédito e Débito"
         */
        
        [actionSheet addAction:creditAndDebit];
        
        if ([self.acceptedPaymentTypes containsObject:@(Credit)])
            [actionSheet addAction:credit];
        
        if ([self.acceptedPaymentTypes containsObject:@(Debit)])
            [actionSheet addAction:debit];
        
        [actionSheet addAction:cancel];
        
        [self.view endEditing:YES];
        [self presentViewController:actionSheet animated:YES completion:nil];
    } else {
        _currentTextFieldTag = textField.tag;
    }
    
    return (textField == self.cardTypeTextField) ? NO : YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.cardTypeTextField) {
        return;
    } else if (textField == self.holderNameTextField) {
        BOOL valid = [(ErrorTextField*)textField checkIfContentIsValid:NO];
        valid = valid || textField.text.length == 0;
        [(ErrorTextField*)textField showFieldWithError:valid];
    } else {
        [(ErrorTextField*)textField checkIfContentIsValid:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// MARK: - Keyboard handling

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    // viewHighestY calcula a posição Y do bottom da view mais abaixo da tela
    CGFloat viewHighestY = self.scrollView.subviews[0].frame.origin.y + self.scrollView.subviews[0].frame.size.height + 40;
    CGFloat keyboardOriginY = (self.view.frame.size.height - self.view.frame.origin.y) - kbRect.size.height;
    
    // Se o teclado esconde alguma parte da view, adiciona scroll
    if (viewHighestY > keyboardOriginY) {
        // Adiciona inset to tamanho que o teclado está escondendo
        UIEdgeInsets contentInset = self.scrollView.contentInset;
        contentInset.bottom = viewHighestY - keyboardOriginY + 10;
        
        // No iOS 10, deve adicionar valor da navigation bar e constraint superior (why?)
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
//            contentInset.bottom += 84;
//        }
        
        self.scrollView.contentInset = contentInset;
        self.scrollView.scrollIndicatorInsets = contentInset;
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    self.scrollView.contentInset = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)dismissKeyboard:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    _currentTextFieldTag++;
    
    // Try to find next responder
    UIResponder *nextResponder = (UIResponder *)[self.view viewWithTag:_currentTextFieldTag];
    
    if (nextResponder != nil){
        [nextResponder becomeFirstResponder];
    } else {
        [self.view endEditing:YES];
    }
}

- (void)previousTextField:(UIBarButtonItem *)sender{
    if(_currentTextFieldTag > 1){
        _currentTextFieldTag--;
    }
    
    // Try to find next responder
    UIResponder *nextResponder = (UIResponder *)[self.view viewWithTag:_currentTextFieldTag];
    
    if (nextResponder != nil){
        [nextResponder becomeFirstResponder];
    } else {
        [self.view endEditing:YES];
    }
}

// MARK: - Services calls

- (void)callRequestVaultKey {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        
        [_loadingView finishLoading:nil];
        [alert show];
    };
    
    service.successCase = ^(NSDictionary *response){
        [self callPrepareCard];
    };
    
    if (!_loadingView.isLoading) {
        [_loadingView startLoading:self title:@"Aguarde..."];
    }
    [service requestVaultKey];
}

- (void)callPrepareCard{
    Services *service = [[Services alloc] init];
    
    NSString *cardnumber    = [self.cardNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *expDate       = [self.expDateTextField.text stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    NSMutableDictionary *card = @{CardTypeKey: [[NSNumber alloc] initWithInteger:self.selectedType],
                                  CardNumberKey:cardnumber,
                                  ExpirationDateKey:expDate,
                                  SecurityCodeKey:self.cvvTextField.text}.mutableCopy;
    
    if (![self.holderNameTextField.text isEqualToString:@""]) {
        card[CardholderKey] = self.holderNameTextField.text;
    }
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        
        [_loadingView finishLoading:nil];
        [alert show];
    };
    
    service.successCase = ^(NSDictionary *response){
        NSString *cardNonce = [response valueForKey:CardNonceKey];
        [self callAddCardWithCardNonce:cardNonce];
    };
    
    [service prepareCard:card];
}

- (void)callAddCardWithCardNonce:(NSString *)cardNonce {
    Services *service = [[Services alloc] init];
        
    service.failureCase = ^(NSString *cod, NSString *msg){
        if([cod isEqualToString:@"16.6"]){
            [self callAddCardWithCardNonce:cardNonce];
        } else {
            NSString *cancelButtonTitle = @"Entendi";
            if (cod == nil) {
                cancelButtonTitle = @"OK";
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:cancelButtonTitle
                                                  otherButtonTitles:nil];
            
            [_loadingView finishLoading:nil];
            [alert show];
        }
    };
    
    service.successCase = ^(NSDictionary *response){
        /*
         * Em caso de erro, exibe alerta.
         * Se houver bloco de login com pagamento a ser chamado, deve listar os cartões
         * para obter os parâmetros do bloco.
         * Caso contrário, apenas fecha a view.
         */
        
        if ([(NSString*)[response valueForKey:@"status"] intValue] != 1) {
            [_loadingView finishLoading:^{
                NSString *msg = (NSString*)[response valueForKey:@"message"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                message:msg
                                                               delegate:self
                                                      cancelButtonTitle:@"Fechar"
                                                      otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            [self callListCards];
        }
    };
    
    [service addCardWithCardNonce:cardNonce];
}

- (void)callListCards {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        [_loadingView finishLoading:^{
            [_flowController viewControllerDidFinish:self];
        }];
    };
    
    service.successCase = ^(NSDictionary *response) {
        [_loadingView finishLoading:^{
            [_flowController viewControllerDidFinish:self];
        }];
    };
    
    [service listCards];
}

// MARK: - Layout

- (void)configureLayout {
    
    // Configura navigation bar
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Cartão";
    
    // Configura toolbar do teclado
    UIToolbar *keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    keyboardToolBar.barStyle    = UIBarStyleDefault;
    keyboardToolBar.tintColor   = [UIColor grayColor];
    
    UIBarButtonItem *previousButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"left-nav-arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(previousTextField:)];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"right-nav-arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(nextTextField:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    NSArray<UIBarButtonItem *> *items = [[NSArray alloc] initWithObjects:previousButton,nextButton,flexSpace,doneButton, nil];
    [keyboardToolBar setItems:items];
    
    // Adiciona toolbar ao teclado dos campos
    self.cardNumberTextField.tag = 1;
    self.holderNameTextField.tag = 2;
    self.expDateTextField.tag = 3;
    self.cvvTextField.tag = 4;
    self.cardNumberTextField.inputAccessoryView = keyboardToolBar;
    self.holderNameTextField.inputAccessoryView = keyboardToolBar;
    self.expDateTextField.inputAccessoryView = keyboardToolBar;
    self.cvvTextField.inputAccessoryView = keyboardToolBar;
    
    // Configura view
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    //self.cardNumberTextField.mask  = [NSStringMask maskWithPattern:@"(\\d{4}) (\\d{4}) (\\d{4}) (\\d{7})"];
    self.cardNumberTextField.regex = @"^[0-9]{12,19}$";
    self.holderNameTextField.regex = @"^[a-zA-Z]{2,26}$";
    self.expDateTextField.mask     = [NSStringMask maskWithPattern:@"(\\d{2})/(\\d{2})"];
    self.expDateTextField.regex    = @"[0-9]{2}/[0-9]{2}";
    self.cvvTextField.mask         = [NSStringMask maskWithPattern:@"(\\d{6})"];
    self.cvvTextField.regex        = @"^[0-9]{3,6}$";
    
    // Configura campos do cartão
    [self.cardTypeTextField roundTopCornersRadius:5];
    [self.cardTypeTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.cardTypeTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    self.cardTypeTextField.placeholder = @"Selecione o tipo de cartão";
    self.cardTypeTextField.textColor = [[LayoutManager sharedManager] darkGray];
    [self.cardTypeTextField setIconsImages:[UIImage imageNamed:@"iconInfo"]
                                  errorImg:[[UIImage imageNamed:@"iconInfo"] withColor:[[LayoutManager sharedManager] red]]];
    self.cardTypeTextField.tintColor = self.cardTypeTextField.backgroundColor;
    self.cardTypeTextField.delegate = self;
    
    /*
     Alteração Bruno e Adriano 7/2/2017
     Agora, quando a aplicação aceitar somente Crédito, pode aceitar cartões do tipo Crédito e "Crédito e Débito"
     Agora, quando a aplicação aceitar somente Débito, pode aceitar cartões do tipo Débito e "Crédito e Débito"
     */
    
    //if ([self.acceptedPaymentTypes containsObject:@(Credit)] && [self.acceptedPaymentTypes containsObject:@(Debit)]) {
        [self.cardTypeTextField layoutIfNeeded];
        UIImageView *downArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iconDownArrow"]];
        downArrow.contentMode = UIViewContentModeScaleAspectFit;
        [self.cardTypeTextField addSubview:downArrow];
        
        downArrow.frame = CGRectMake(0.0, 0.0, self.cardTypeTextField.bounds.size.height / 4, self.cardTypeTextField.bounds.size.height / 4);
        CGFloat centerY = self.cardTypeTextField.bounds.size.height / 2;
        CGFloat centerX = (self.cardTypeTextField.bounds.origin.x + self.cardTypeTextField.bounds.size.width) - downArrow.frame.size.width - 5;
        downArrow.center = CGPointMake(centerX, centerY);
    //}
    
    [self.cardNumberTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.cardNumberTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    self.cardNumberTextField.placeholder = @"0000 0000 0000 0000";
    //self.cardNumberTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"0000 0000 0000 0000"
    //                                                                                 attributes:@{NSForegroundColorAttributeName: [[LayoutManager sharedManager] lightGray]}];
    self.cardNumberTextField.textColor = [[LayoutManager sharedManager] darkGray];
    [self.cardNumberTextField setIconsImages:[UIImage imageNamed:@"iconCard"]
                                    errorImg:[[UIImage imageNamed:@"iconCard"] withColor:[[LayoutManager sharedManager] red]]];
    self.cardNumberTextField.delegate = self;
    
    [self.holderNameTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.holderNameTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    self.holderNameTextField.placeholder = @"Nome impresso no cartão";
    //self.holderNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nome impresso no cartão"
    //                                                                                 attributes:@{NSForegroundColorAttributeName: [[LayoutManager sharedManager] lightGray]}];
    self.holderNameTextField.textColor = [[LayoutManager sharedManager] darkGray];
    [self.holderNameTextField setIconsImages:[UIImage imageNamed:@"iconUser"]
                                    errorImg:[[UIImage imageNamed:@"iconUser"] withColor:[[LayoutManager sharedManager] red]]];
    self.holderNameTextField.delegate = self;
    
    [self.expDateTextField roundCustomCornerRadius:5 corners:UIRectCornerBottomLeft];
    [self.expDateTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.expDateTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    self.expDateTextField.placeholder = @"MM/AA";
    //self.expDateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"MM/AA"
    //                                                                                 attributes:@{NSForegroundColorAttributeName: [[LayoutManager sharedManager] lightGray]}];
    self.expDateTextField.textColor = [[LayoutManager sharedManager] darkGray];
    [self.expDateTextField setIconsImages:[UIImage imageNamed:@"iconCalendars"]
                                 errorImg:[[UIImage imageNamed:@"iconCalendars"] withColor:[[LayoutManager sharedManager] red]]];
    self.expDateTextField.delegate = self;
    
    [self.cvvTextField roundCustomCornerRadius:5 corners:UIRectCornerBottomRight];
    [self.cvvTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.cvvTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    self.cvvTextField.placeholder = @"CVV";
    //self.cvvTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"CVV"
    //                                                                                 attributes:@{NSForegroundColorAttributeName: [[LayoutManager sharedManager] lightGray]}];
    self.cvvTextField.textColor = [[LayoutManager sharedManager] darkGray];
    [self.cvvTextField setIconsImages:[UIImage imageNamed:@"iconLock"]
                             errorImg:[[UIImage imageNamed:@"iconLock"] withColor:[[LayoutManager sharedManager] red]]];
    self.cvvTextField.delegate = self;
    
    self.acceptedCardsButton.titleLabel.font = [[LayoutManager sharedManager] fontWithSize:15];
    self.acceptedCardsButton.tintColor = [[LayoutManager sharedManager] darkGreen];
}


//Alteração Bruno Fernandes 3/2/2017
//isso aqui é errado, fiz um copy and paste de LoginPaymentAction.m, o correto seria colocar isto em um lugar só
//TODO: ver com o Adriano

- (bool) cardIsAcceptedWithType:(CardType) type andBrand: (CardBrand) brand {
    
    BOOL isValid = NO;
    
    //verifica os meios de pagamento do cartão
    
    //Verifica crédito
    if ((type == PaymentModeCredit) && ([self.acceptedPaymentTypes containsObject:@(Credit)])) isValid = YES;
    
    //Verifica débito
    if ((type == PaymentModeDebit) && ([self.acceptedPaymentTypes containsObject:@(Debit)])) isValid = YES;
    
    //Verifica ambos
    if ((type == PaymentModeCreditAndDebit) && ([self.acceptedPaymentTypes containsObject:@(Credit)] || [self.acceptedPaymentTypes containsObject:@(Debit)])) isValid = YES;
    
    //Verifica brands
    if (![self.acceptedBrands containsObject:@(brand)]) isValid = NO;
    
    
    return isValid;
    
}


@end
