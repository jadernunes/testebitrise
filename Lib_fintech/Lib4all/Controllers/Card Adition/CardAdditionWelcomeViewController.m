//
//  CardAdditionWelcomeViewController.m
//  Example
//
//  Created by Cristiano Matte on 07/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "CardAdditionWelcomeViewController.h"
#import "LayoutManager.h"
#import "User.h"
#import "BaseNavigationController.h"

@interface CardAdditionWelcomeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CardAdditionWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureLayout];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationItem.title = @"Cartão";
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
}

- (IBAction)continueButtonTouched {
    [_flowController viewControllerDidFinish:self];
}

- (IBAction)closeButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)configureLayout {
    
    // Configura navigation bar
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Cartão";
    
    // Configura view
    LayoutManager *layoutManager = [LayoutManager sharedManager];
    self.view.backgroundColor = [layoutManager backgroundColor];
    
    self.titleLabel.font = [layoutManager fontWithSize:[layoutManager titleFontSize]];
    self.titleLabel.textColor = [layoutManager darkGray];
    
    NSString *firstName = [[User sharedUser].fullName componentsSeparatedByString:@" "][0];
    if (firstName == nil) {
        firstName = @"";
    }
    self.titleLabel.text = [self.titleLabel.text stringByReplacingOccurrencesOfString:@"<name>" withString:firstName];
}

@end
