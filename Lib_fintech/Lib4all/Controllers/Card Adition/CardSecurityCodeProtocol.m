//
//  CardSecurityCodeProtocol.m
//  Example
//
//  Created by 4all on 26/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "CardSecurityCodeProtocol.h"

#import "Services.h"
#import "ServicesConstants.h"
#import "LoadingViewController.h"

#import "LocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import "PopUpBoxViewController.h"

@interface CardSecurityCodeProtocol ()

@property LoadingViewController *loadingView;
@property (weak) UIViewController *parentVC;

@end


@implementation CardSecurityCodeProtocol

@synthesize title = _title;
@synthesize subTitle = _subTitle;
@synthesize textFieldPlaceHolder = _textFieldPlaceHolder;
@synthesize textFieldImageName = _textFieldImageName;
@synthesize textFieldWithErrorImageName = _textFieldWithErrorImageName;
@synthesize serverKey = _serverKey;
@synthesize keyboardType = _keyboardType;
@synthesize preSettedField = _preSettedField;
@synthesize onUpdateField = _onUpdateField;
@synthesize flowController = _flowController;


- (instancetype)init
{
    self = [super init];
    if (self) {
        _title = @"Ótimo, agora falta pouco, só precisamos das informações do seu cartão.";
        _textFieldPlaceHolder = @"Código de segurança";
        _keyboardType = UIKeyboardTypeNumberPad;
        
        _loadingView = [[LoadingViewController alloc] init];
        
    }
    return self;
}


- (BOOL)isDataValid:(NSString *)data {
    NSString *regex = @"^[0-9]{3,6}$";

    return [self checkIfContentIsValid:data regex:regex];
}
- (NSString *)serverFormattedData:(NSString *)data {
    
    return data;
}


// MARK: - Services calls

- (void)callRequestVaultKey: (void (^)(NSString *))completion  {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        [_loadingView finishLoading:^{
            PopUpBoxViewController *modal = [[PopUpBoxViewController alloc] init];
            [modal show:_parentVC
                  title:@"Atenção!"
            description:msg
              imageMode:Error
           buttonAction:nil];
        }];
    };
    
    service.successCase = ^(NSDictionary *response){
        [self callPrepareCard: completion];
    };
    
    if (!_loadingView.isLoading) {
        [_loadingView startLoading:_parentVC title:@"Aguarde..."];
    }
    [service requestVaultKey];
}

- (void)callPrepareCard: (void (^)(NSString *))completion {
    Services *service = [[Services alloc] init];
    
    NSString *cardnumber    = _flowController.cardNumber;
    NSString *expDate       = _flowController.expirationDate;
    
    NSMutableDictionary *card = @{CardTypeKey: [[NSNumber alloc] initWithInteger:_flowController.selectedType],
                                  CardNumberKey:cardnumber,
                                  ExpirationDateKey:expDate,
                                  SecurityCodeKey:_flowController.CVV}.mutableCopy;
    
    if (![_flowController.cardName isEqualToString:@""]) {
        card[CardholderKey] = _flowController.cardName;
    }
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        [_loadingView finishLoading:^{
            PopUpBoxViewController *modal = [[PopUpBoxViewController alloc] init];
            [modal show:_parentVC
                  title:@"Atenção!"
            description:msg
              imageMode:Error
           buttonAction:nil];
        }];
    };
    
    service.successCase = ^(NSDictionary *response){
        NSString *cardNonce = [response valueForKey:CardNonceKey];
        [self callAddCardWithCardNonce:cardNonce withCompletion:completion];
    };
    
    [service prepareCard:card];
}

- (void)callAddCardWithCardNonce:(NSString *)cardNonce withCompletion: (void (^)(NSString *))completion  {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        if([cod isEqualToString:@"16.6"]){
            [self callAddCardWithCardNonce:cardNonce withCompletion:completion];
        } else {
            NSString *cancelButtonTitle = @"Entendi";
            if (cod == nil) {
                cancelButtonTitle = @"OK";
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                            message:msg
                                                           delegate:self
                                                  cancelButtonTitle:cancelButtonTitle
                                                  otherButtonTitles:nil];
            
            [_loadingView finishLoading:nil];
            [alert show];
        }
    };
    
    service.successCase = ^(NSDictionary *response){
        /*
         * Em caso de erro, exibe alerta.
         * Se houver bloco de login com pagamento a ser chamado, deve listar os cartões
         * para obter os parâmetros do bloco.
         * Caso contrário, apenas fecha a view.
         */
        
        if ([(NSString*)[response valueForKey:@"status"] intValue] != 1) {
            [_loadingView finishLoading:^{
                NSString *msg = (NSString*)[response valueForKey:@"message"];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                                message:msg
                                                               delegate:self
                                                      cancelButtonTitle:@"Fechar"
                                                      otherButtonTitles:nil];
                [alert show];
            }];
        } else {
            [self callListCards:completion];
        }
    };
    
    [service addCardWithCardNonce:cardNonce];
}

- (void)callListCards: (void (^)(NSString *))completion  {
    Services *service = [[Services alloc] init];
    
    service.failureCase = ^(NSString *cod, NSString *msg) {
        [_loadingView finishLoading:^{
            completion(nil);
        }];
    };
    
    service.successCase = ^(NSDictionary *response) {
        [_loadingView finishLoading:^{
            completion(nil);
        }];
    };
    
    [service listCards];
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (![string isEqualToString:@""] && textField.text.length == 6) {
        return NO;
    }

    if (_onUpdateField != nil) {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        _onUpdateField(nil, nil, nil, text);
    }
    
    return YES;
}

- (void) saveData:(UIViewController *)vc data:(NSString *)data withCompletion: (void (^)(NSString *))completion {
    self.flowController.CVV = data;
    self.flowController.enteredCVV = data;
    _parentVC = vc;
    
    BOOL requestLocalizationPermission = ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) &&
    ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) &&
    ([[Lib4allPreferences sharedInstance].requiredAntiFraudItems[@"geolocation"] isEqual: @YES]);
    
    if (requestLocalizationPermission) {
        if (!_loadingView.isLoading) {
            [_loadingView startLoading:_parentVC title:@"Aguarde..."];
        }
        
        [[LocationManager sharedManager] updateLocationWithCompletion:^(BOOL success, NSDictionary *location) {
            if (success) {
                [self callRequestVaultKey: completion];
            } else {
                PopUpBoxViewController *modal = [[PopUpBoxViewController alloc] init];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_loadingView finishLoading:^{
                        [modal show:_parentVC
                              title:@"Atenção!"
                        description:@"Não foi possível obter sua localização. Ative os serviços de localização nos Ajustes."
                          imageMode:Error
                       buttonAction:nil];
                    }];
                });
            }
        }];
    } else {
        [self callRequestVaultKey: completion];
    }

    
}

-(BOOL)checkIfContentIsValid:(NSString *)text regex:(NSString *)regex{
    
    BOOL returnValue;
    NSString *cleanText = [text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    cleanText = [cleanText stringByReplacingOccurrencesOfString:@")" withString:@""];
    cleanText = [cleanText stringByReplacingOccurrencesOfString:@"-" withString:@""];
    cleanText = [cleanText stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if([text length]==0){
        returnValue = NO;
    }else{
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regex options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger regExMatches = [regEx numberOfMatchesInString:cleanText options:0 range:NSMakeRange(0, [cleanText length])];
        
        if (regExMatches == 0) {
            returnValue = NO;
        } else {
            returnValue = YES;
        }
    }
    
    return returnValue;
}
@end
