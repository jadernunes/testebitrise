//
//  AddCardViewController.h
//  Example
//
//  Created by Cristiano Matte on 09/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardAdditionFlowController.h"

@interface AddCardViewController : UIViewController

@property (strong, nonatomic) CardAdditionFlowController *flowController;

- (void)setAccceptedPaymentTypes: (NSArray *) paymentTypes andAcceptedBrands: (NSArray *) brands;

@end
