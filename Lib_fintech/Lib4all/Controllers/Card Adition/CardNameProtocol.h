//
//  CardNameProtocol.h
//  Example
//
//  Created by 4all on 26/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardFieldProtocol.h"

@interface CardNameProtocol : NSObject <CardFieldProtocol>

@end
