//
//  CardAdditionFlowController.h
//  Example
//
//  Created by Cristiano Matte on 01/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlowController.h"
#import "CardUtil.h"

@interface CardAdditionFlowController : NSObject <FlowController>

@property (copy, nonatomic) void (^loginWithPaymentCompletion)(NSString *sessionToken, NSString *cardId);

@property NSArray * acceptedPaymentTypes;
@property NSArray * acceptedBrands;

@property CardType selectedType;

@property NSString *cardNumber;
@property NSString *cardName;
@property NSString *expirationDate;
@property NSString *CVV;

@property NSString *enteredCardNumber;
@property NSString *enteredCardName;
@property NSString *enteredExpirationDate;
@property NSString *enteredCVV;



- (instancetype)initWithAcceptedPaymentTypes: (NSArray *) paymentTypes andAcceptedBrands: (NSArray *) brands;

@end
