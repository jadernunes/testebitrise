//
//  CardNumberProtocol.h
//  Example
//
//  Created by Adriano Soares on 26/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardFieldProtocol.h"

@interface CardNumberProtocol : NSObject <CardFieldProtocol>

@end
