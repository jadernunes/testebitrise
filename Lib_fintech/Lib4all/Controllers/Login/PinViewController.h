//
//  PinViewController.h
//  Example
//
//  Created by Cristiano Matte on 30/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface PinViewController : UIViewController

@property (strong, nonatomic) SignFlowController *signFlowController;

@end
