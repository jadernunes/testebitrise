//
//  SignInViewController.m
//  Example
//
//  Created by Cristiano Matte on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "LoadingViewController.h"
#import "ChallengeViewController.h"
#import "LayoutManager.h"
#import "BaseNavigationController.h"
#import "UIFloatLabelTextField.h"
#import "UIFloatLabelTextField+Border.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "Lib4allPreferences.h"
#import "LocationManager.h"
#import "Lib4allInfo.h"
#import "NSStringMask.h"
#import "UIImage+Color.h"
#import "Lib4all.h"
#import "UIView+Gradient.h"
#import "MainActionButton.h"

@interface SignInViewController () < UITextFieldDelegate, UIGestureRecognizerDelegate >

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *phoneOrEmaiLTextField;
@property (weak, nonatomic) IBOutlet UILabel *noLoginLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet MainActionButton *mainButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeader;

@property (strong, nonatomic) NSMutableString *rawId;

@end

@implementation SignInViewController

static CGFloat const kBottomConstraintMin = 60.0;
static NSString* const kNavigationTitle = @"Entrar";

// MARK: - View contoller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    
    NSDictionary *customerData = [Lib4all customerData];
    
    
    [self configureLayout];
    
    
    /*
     Alteração Bruno Fernandes 7/2/2017
     Isto aqui nunca é para acontecer o.o
     coloquei um assert para forçar um erro
    if (self.signFlowController == nil) {
     
        self.signFlowController = [[SignFlowController alloc] init];
    }
    */
    NSAssert(self.signFlowController != nil, @"SignFlowController é nil!!");
    
    self.rawId = [[NSMutableString alloc] init];
    
    if (customerData[@"phoneNumber"] != nil) {
        NSString *data = customerData[@"phoneNumber"];
        self.rawId = data.mutableCopy;
        self.phoneOrEmaiLTextField.text = (NSString *)[NSStringMask maskString:data
                                                                   withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
    }
    
    if (customerData[@"emailAddress"] != nil) {
        NSString *data = customerData[@"emailAddress"];
        self.rawId = data.mutableCopy;
        self.phoneOrEmaiLTextField.text = data;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    [self configureLayout];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationItem.title = @"";
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
    [self dismissKeyboard];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.bottomConstraint.constant = kBottomConstraintMin;
    
    
    if (self.navigationController.viewControllers.count == 1) {
        /*
         * Os dados do controlador de fluxo a seguir devem ser resetados toda vez
         * que este view controller aparecer, para evitar que um "Voltar" para esta
         * tela mantenha dados de tentativas de login ou cadastro anteriores
         */
        _signFlowController.enteredPhoneNumber = nil;
        _signFlowController.enteredEmailAddress = nil;        
        _signFlowController.accountData = nil;

        [[Lib4all sharedInstance] callLogout:nil];
    }
    
    Lib4allPreferences *preferences = [Lib4allPreferences sharedInstance];
    _signFlowController.requireFullName = [preferences requireFullName];
    // CPF e data de nascimento são exigidos se for login com pagamento e o anti-fraude estiver ativo ou se usuário da biblioteca solicitou
    _signFlowController.requireCpfOrCnpj = (_signFlowController.requirePaymentData && [preferences.requiredAntiFraudItems[@"cpf"] isEqual: @YES]) || [preferences requireCpfOrCnpj];
    _signFlowController.requireBirthdate = (_signFlowController.requirePaymentData && [preferences.requiredAntiFraudItems[@"birthdate"] isEqual: @YES]) || [preferences requireBirthdate];
}

- (void) dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.4 animations:^{
        if([[UIScreen mainScreen] bounds].size.height < 568){
            NSLog(@"App is running on iPhone with screen 3.5 inch");
            _heightHeader.constant = _heightHeader.constant - 95;
        }

        self.bottomConstraint.constant = 3 + keyboardSize.height;
        [self.view updateConstraints];
        [self.view layoutIfNeeded];

    }];
    
    
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.4 animations:^{
        if ([[UIScreen mainScreen] bounds].size.height<=480.0f) {
            NSLog(@"App is running on iPhone with screen 3.5 inch");
            _heightHeader.constant = 222;
        }

        self.bottomConstraint.constant = kBottomConstraintMin;
        [self.view updateConstraints];
        [self.view layoutIfNeeded];

    }];
    
}

// MARK: - Actions

- (IBAction)signInButtonTouched {
    if (![self loginIdValid]) {
        [self.phoneOrEmaiLTextField showFieldWithError:YES];
        return;
    } else {
        [self.phoneOrEmaiLTextField showFieldWithError:NO];
    }
    
    // Verifica se o ID entrado é um número de telefone
    BOOL isPhoneNumber = NO;
    NSRegularExpression *phoneRegex = [NSRegularExpression regularExpressionWithPattern:@"^[\\d]*$"
                                                                                options:0
                                                                                  error:nil];
    if ([phoneRegex numberOfMatchesInString:self.rawId options:0 range:NSMakeRange(0, self.rawId.length)] > 0) {
        isPhoneNumber = YES;
    }
    
    LoadingViewController *loadingViewController = [[LoadingViewController alloc] init];
    Services *service = [[Services alloc] init];
    
    if (isPhoneNumber) {
        _signFlowController.enteredPhoneNumber = [NSString stringWithFormat:@"55%@", self.rawId];
    } else {
        _signFlowController.enteredEmailAddress = self.rawId;
    }
    
    service.failureCase = ^(NSString *cod, NSString *msg){
        /*
         * Caso o erro seja "Não há usuário com o telefone informado",
         * redireciona para a tela de cadastro.
         * Para qualquer outro erro, exibe um alerta.
         */
        if ([cod isEqualToString:@"3.25"]) {
            _signFlowController.isLogin = NO;

            dispatch_async(dispatch_get_main_queue(), ^{
                [loadingViewController finishLoading:^{
                    [_signFlowController viewControllerDidFinish:self];
                }];
            });
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                           message:msg
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [loadingViewController finishLoading:^{
                    [self presentViewController:alert animated:YES completion:nil];
                }];
            });
        }
    };
    
    service.successCase = ^(NSDictionary *response){
        _signFlowController.isLogin = YES;
        
        _signFlowController.maskedPhoneNumber = response[MaskedPhoneKey];
        _signFlowController.maskedEmailAddress = response[MaskedEmailAddressKey];
        
        // Deve solicitar os dados se eles foram exigidos e não estão presentes no banco de dados
        NSArray *lackingData = response[LackingDataKey];
        _signFlowController.requireFullName = _signFlowController.requireFullName && (lackingData != nil) && [lackingData containsObject:FullNameKey];
        _signFlowController.requireCpfOrCnpj = _signFlowController.requireCpfOrCnpj && (lackingData != nil) && [lackingData containsObject:CpfKey];
        _signFlowController.requireBirthdate = _signFlowController.requireBirthdate && (lackingData != nil) && [lackingData containsObject:BirthdateKey];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            /*
             * Se há dados a serem inseridos, direciona para tela de adição de dados.
             * Caso contrário, direciona para tela de challenge.
             */
            [loadingViewController finishLoading:^{
                [_signFlowController viewControllerDidFinish:self];
            }];
        });
    };
    
    NSMutableArray *requiredData = [[NSMutableArray alloc] init];
    if (_signFlowController.requireFullName) [requiredData addObject:FullNameKey];
    if (_signFlowController.requireCpfOrCnpj) [requiredData addObject:CPFKey];
    if (_signFlowController.requireBirthdate) [requiredData addObject:BirthdateKey];
    
    // Se o ID inserido for número de telefone, deve adicionar o código do país (Brasil - 55)
    NSString *userId = self.rawId;
    if (isPhoneNumber) {
        userId = [NSString stringWithFormat:@"55%@", userId];
    }
    
    [loadingViewController startLoading:self title:@"Aguarde..."];
    [service startLoginWithIdentifier:userId requiredData:requiredData];
}

- (IBAction)signUpButtonTouched {
    _signFlowController.isLogin = NO;
    [_signFlowController viewControllerDidFinish:self];
}

- (IBAction)closeButtonTouched:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (BOOL)loginIdValid {
    if ([self.rawId isEqualToString:@""]) {
        return NO;
    }
    
    NSRegularExpression *phoneRegex = [NSRegularExpression regularExpressionWithPattern:@"^[\\d]*$"
                                                                                options:0
                                                                                  error:nil];
    
    if ([phoneRegex numberOfMatchesInString:self.rawId options:0 range:NSMakeRange(0, self.rawId.length)] > 0) {
        if (self.rawId.length == 11) {
            return YES;
        }
        
    }
    
    NSRegularExpression *emailRegex = [NSRegularExpression regularExpressionWithPattern:@"^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$"
                                                                                options:0
                                                                                  error:nil];
    
    if ([emailRegex numberOfMatchesInString:self.rawId options:0 range:NSMakeRange(0, self.rawId.length)] > 0) {
        return YES;
    }
    
    return NO;
}

// MARK: - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSMutableString *newRawId = [[NSMutableString alloc] initWithString:self.rawId];
    
    // Se for backspace, remove último caractere, caso contrário, anexa nova string ao fim da string atual
    if ((string == nil || [string isEqualToString:@""]) && newRawId.length > 0) {
        [newRawId deleteCharactersInRange:NSMakeRange(self.rawId.length-1, 1)];
    } else {
        [newRawId appendString:string];
    }
    
    // Verifica se string atual é número de telefone
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[\\d]*$"
                                                                           options:0
                                                                             error:nil];
    unsigned long regexMatches = [regex numberOfMatchesInString:newRawId
                                                        options:0
                                                          range:NSMakeRange(0, newRawId.length)];
    
    /*
     * Se foi digitado backspace, apaga último caractere. Caso contrário, adiciona
     * caractere ao final da string se for e-mail ou telefone quando ainda não foi
     * adicionado o número máximo de caracteres.
     */
    if ((string == nil || [string isEqualToString:@""]) && self.rawId.length > 0) {
        [self.rawId deleteCharactersInRange:NSMakeRange(self.rawId.length-1, 1)];
    } else if (regexMatches == 0 || (regexMatches > 0 && self.rawId.length < 11)) {
        [self.rawId appendString:string];
    }
    
    // Aplica máscara se for número de telefone
    if (regexMatches > 0) {
        textField.text = (NSString *)[NSStringMask maskString:self.rawId withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
    } else {
        textField.text = self.rawId;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self signInButtonTouched];
    return YES;
}

// MARK: - Layout

- (void)configureLayout {
    
    LayoutManager *layout = [LayoutManager sharedManager];
    
    // Configura view
    self.view.backgroundColor = layout.backgroundColor;
    
    // Configura navigation bar
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.title = kNavigationTitle;
    self.navigationItem.title = kNavigationTitle;
    
    [[UIFloatLabelTextField appearance] setBackgroundColor:[UIColor clearColor]];
    [self.phoneOrEmaiLTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.phoneOrEmaiLTextField.floatLabelFont = [layout fontWithSize:11.0];
    self.phoneOrEmaiLTextField.floatLabelActiveColor = layout.darkGray;
    [self.phoneOrEmaiLTextField setBottomBorderWithColor: layout.lightGray];
    self.phoneOrEmaiLTextField.clearButtonMode = UITextFieldViewModeNever;
    self.phoneOrEmaiLTextField.delegate = self;
    
    self.phoneOrEmaiLTextField.font = [layout fontWithSize:layout.regularFontSize];
    self.phoneOrEmaiLTextField.textColor = layout.darkGray;
    [self.phoneOrEmaiLTextField setPlaceholder:@"Telefone ou e-mail"];//.placeholder = @"Telefone ou e-mail";
    
    
    // Configura label de cadastro
    self.noLoginLabel.font = [layout fontWithSize:layout.regularFontSize];
    self.noLoginLabel.textColor = layout.darkGray;
    
    self.labelDescription.font = [layout fontWithSize:layout.subTitleFontSize];
    self.labelDescription.textColor = [UIColor whiteColor];
    
    // Configura botão de cadastro
    self.signUpButton.titleLabel.font = [layout fontWithSize:layout.regularFontSize];
    [self.signUpButton setTitleColor:layout.lightGreen forState:UIControlStateNormal];
    [self.signUpButton setTitleColor:layout.darkGreen forState:UIControlStateSelected];


    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];


    
    UIView *box = [self.view viewWithTag:77];
    [box setGradientFromColor:layout.primaryColor toColor:layout.gradientColor];
    
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithTitle:@"Fechar"
                                                                 style:UIBarButtonItemStylePlain target:self
                                                                action:@selector(closeButtonTouched:)];
    
    if (self.navigationController.viewControllers.count == 1) {
        [self.navigationItem setLeftBarButtonItem:menuItem];
    }

}

#pragma mark - Gesture Recognizer
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    //Evita a necessidade de tocar duas vezes  no botão
    if ([touch.view isDescendantOfView:_mainButton]) {
        return NO;
    }
    
    return YES;
}


@end
