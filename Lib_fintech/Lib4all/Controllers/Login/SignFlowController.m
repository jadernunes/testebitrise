//
//  SignFlowController.m
//  Example
//
//  Created by Cristiano Matte on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "SignFlowController.h"
#import <CoreLocation/CoreLocation.h>
#import "LocalizationFlowController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "PinViewController.h"
#import "PinConfirmationViewController.h"
#import "PasswordViewController.h"
#import "ChallengeViewController.h"
#import "AccountCreatedViewController.h"
#import "CardAdditionFlowController.h"
#import "DataFieldViewController.h"
#import "ChoosePaymentCardViewController.h"
#import "BlockedPasswordViewController.h"
#import "NameDataField.h"
#import "CPFDataField.h"
#import "BirthdateDataField.h"
#import "Lib4allPreferences.h"
#import "User.h"
#import "CreditCardsList.h"
#import "Lib4all.h"
#import "GenericDataViewController.h"
#import "SISUPhoneNumberDataField.h"
#import "SISUNameDataField.h"
#import "SISUTokenSmsDataField.h"
#import "SISUEmailDataField.h"
#import "SISUCPFDataField.h"
#import "SISUBirthdateDataField.h"
#import "SISUPasswordDataField.h"
#import "SISUPasswordConfirmationDataField.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "TermsViewController.h"
#import "WelcomeViewController.h"
#import "TermsViewController.h"

@interface SignFlowController()
@property (strong, nonatomic) LoadingViewController *loadingView;
@end

@implementation SignFlowController


@synthesize onLoginOrAccountCreation = _onLoginOrAccountCreation;

//este init utiliza os tipos de pagamentos e bandeiras definidas em Preferences
- (instancetype)init {
    self = [super init];
    
    if (self) {
        _onLoginOrAccountCreation = YES;
    }
    
    self.acceptedPaymentTypes = [[Lib4allPreferences sharedInstance] acceptedPaymentTypes];
    self.acceptedBrands = [[[Lib4allPreferences sharedInstance] acceptedBrands] allObjects];
    self.loadingView = [[LoadingViewController alloc] init];
    
    return self;
}


//este init recebe configuração de tipos de pagamento e bandeiras!
- (instancetype)initWithAcceptedPaymentTypes: (NSArray *) paymentTypes andAcceptedBrands: (NSArray *) brands {
    self = [super init];
    
    if (self) {
        _onLoginOrAccountCreation = YES;
    }
    
    self.acceptedPaymentTypes = paymentTypes;
    self.acceptedBrands = brands;
    
    return self;
}

- (void)startFlowWithViewController:(UIViewController *)viewController {
    
}

- (void)viewControllerDidFinish:(UIViewController *)viewController {
    // O fluxo varia se for login ou cadastro
    if (_isLogin) {
        [self continueSignInFlowWihViewController:viewController];
    } else {
        [self continueSignUpFlowWihViewController:viewController];
    }
}

- (void)continueSignInFlowWihViewController:(UIViewController *)viewController {
    UIViewController *destination;
    User *user = [User sharedUser];
    
    BOOL requestLocalizationPermission = ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) &&
    ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) &&
    ([[Lib4allPreferences sharedInstance].requiredAntiFraudItems[@"geolocation"] isEqual: @YES]);
    
    if ([viewController isKindOfClass:[GenericDataViewController class]]) {
        GenericDataViewController *vc = (GenericDataViewController *)viewController;
        BOOL isFromChallenge = [vc.dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]];
        if (isFromChallenge) {
            destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                           instantiateViewControllerWithIdentifier:@"PinViewController"];
            _enteredPhoneNumber = user.phoneNumber;
            _enteredEmailAddress = user.emailAddress;
            ((PinViewController *)destination).signFlowController = self;
        
        } else {
            //Redireciona para tela de login
            UINavigationController *navDestionation = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                                       instantiateViewControllerWithIdentifier:@"LoginVC"];
            destination = [[navDestionation viewControllers] objectAtIndex:0];
            
            ((SignInViewController *)destination).signFlowController = self;
        }

    } else if ([viewController isKindOfClass:[SignInViewController class]] && user.hasPassword) {
        // Redireciona para tela de inserção de senha
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"PasswordViewController"];
        ((PasswordViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[SignInViewController class]]) {
        destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUTokenSmsDataField alloc] init]];
        
        ((GenericDataViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[ChoosePaymentCardViewController class]]) {
        // Finaliza o fluxo de login com pagamento
        [viewController dismissViewControllerAnimated:YES completion:^{
            if (_loginWithPaymentCompletion != nil) _loginWithPaymentCompletion(user.token, _selectedCardId);
        }];
    } else {
        BOOL requireFullName = (user.fullName == nil || [user.fullName isEqualToString:@""]);
        BOOL requireCpf = [[Lib4allPreferences sharedInstance] requireCpfOrCnpj] && (user.cpf == nil || [user.cpf isEqualToString:@""]);
        BOOL requireBirthdate = (user.birthdate == nil || [user.birthdate isEqualToString:@""]);
        
        if (requireFullName || requireCpf || requireBirthdate) {
            destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                           instantiateViewControllerWithIdentifier:@"DataFieldViewController"];
            
            if (requireFullName) {
                // Redireciona para tela de inserção de nome
                ((DataFieldViewController *)destination).dataFieldProtocol = [[NameDataField alloc] init];
            } else if (requireCpf) {
                // Redireciona para tela de inserção de cpf
                ((DataFieldViewController *)destination).dataFieldProtocol = [[CPFDataField alloc] init];
            } else if (requireBirthdate) {
                // Redireciona para tela de inserção de data de nascimento
                ((DataFieldViewController *)destination).dataFieldProtocol = [[BirthdateDataField alloc] init];
            }
            
            ((DataFieldViewController *)destination).flowController = self;
        } else if (_requirePaymentData && [[CreditCardsList sharedList] getDefaultCard] == nil) {
            // Inicia fluxo de adição de cartão
            CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] initWithAcceptedPaymentTypes:self.acceptedPaymentTypes andAcceptedBrands:self.acceptedBrands];
            flowController.loginWithPaymentCompletion = _loginWithPaymentCompletion;
            flowController.onLoginOrAccountCreation = YES;
            
            [flowController startFlowWithViewController:viewController];
            return;
        } else if (_requirePaymentData && requestLocalizationPermission) {
            LocalizationFlowController *localizationFlowController = [[LocalizationFlowController alloc] init];
            localizationFlowController.onLoginOrAccountCreation = _onLoginOrAccountCreation;
            localizationFlowController.completionBlock = ^(UIViewController *viewController) {
                [self viewControllerDidFinish:viewController];
            };
            [localizationFlowController startFlowWithViewController:viewController];
            return;
            
        } else if (_requirePaymentData) {
            // Redireciona para tela de escolha de cartão
            destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                           instantiateViewControllerWithIdentifier:@"ChoosePaymentCardViewController"];
            ((ChoosePaymentCardViewController *)destination).signFlowController = self;
        } else {
            // Finaliza o fluxo de login
            [viewController dismissViewControllerAnimated:YES completion:^{
                if (_loginCompletion != nil) _loginCompletion(user.phoneNumber, user.emailAddress, user.token);
            }];
        }
    }
    
    if (destination != nil) {
        [viewController.navigationController pushViewController:destination animated:YES];
    }
}

- (void)continueSignUpFlowWihViewController:(UIViewController *)viewController {
    __block UIViewController *destination;
    
    if ([viewController isKindOfClass:[SignInViewController class]]) {
        // Após a tela de cadastro, apresenta a tela de senha
        destination  = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUNameDataField alloc] init]];
        ((GenericDataViewController *)destination).signFlowController = self;
        
        
    } else {
        
        if ([viewController isKindOfClass:[GenericDataViewController class]]) {
            GenericDataViewController *dataController = (GenericDataViewController *) viewController;
        
            if ([dataController.dataFieldProtocol isKindOfClass:[SISUNameDataField class]]) {
                // Após a tela de inserção de nome, solicita o telefone
                destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUPhoneNumberDataField alloc] init]];
                
                ((GenericDataViewController *)destination).signFlowController = self;
                
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUPhoneNumberDataField class]]) {
                
                // Após a tela de inserção do telefone, solicita o SMS Token caso telefone não tenha sido validado
                if (_isPhoneValidated == NO) {
                
                    destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUTokenSmsDataField alloc] init]];
                
                }else{
                    // Caso celular já validado, pula para inserção de e-mail
                    destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUEmailDataField alloc] init]];
                }
                
                ((GenericDataViewController *)destination).signFlowController = self;
                
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUTokenSmsDataField class]]) {
                
                // Após a tela de inserção do SMS Token, solicita o E-mail
                destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUEmailDataField alloc] init]];
                
                ((GenericDataViewController *)destination).signFlowController = self;
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUEmailDataField class]]) {
                
                // Após a tela de inserção de E-mail, solicita o CPF se obrigatório
                if ([[Lib4allPreferences sharedInstance] requireCpfOrCnpj]) {
                    destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUCPFDataField alloc] init]];
                }else{
                    destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUBirthdateDataField alloc] init]];
                }
                
                
                ((GenericDataViewController *)destination).signFlowController = self;
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUCPFDataField class]]) {
                
                // Após a tela de inserção de CPF, solicita a data de nascimento
                destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUBirthdateDataField alloc] init]];
                
                ((GenericDataViewController *)destination).signFlowController = self;
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUBirthdateDataField class]]) {
                
                // Após a tela de inserção da data de nascimento, solicita a senha

                //Parametros necessarios para protocolo de password
                SISUPasswordDataField *protocol = [[SISUPasswordDataField alloc] init];
                protocol.emailAddress = [_accountData valueForKey:EmailAddressKey];
                protocol.phoneNumber  = [_accountData valueForKey:PhoneNumberKey];
                protocol.cpf          = [_accountData valueForKey:CPFKey];
                
                destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:protocol];
                
                ((GenericDataViewController *)destination).signFlowController = self;
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUPasswordDataField class]]) {
                
                // Após a tela de inserção da senha, solicita a confirmação da mesma
                destination = [GenericDataViewController getConfiguredControllerWithdataFieldProtocol:[[SISUPasswordConfirmationDataField alloc] init]];
                
                ((GenericDataViewController *)destination).signFlowController = self;
            }else if ([dataController.dataFieldProtocol isKindOfClass:[SISUPasswordConfirmationDataField class]]) {
                
                TermsViewController *termsViewController = [[TermsViewController alloc] init];
                termsViewController.signFlowController = self;
                destination = termsViewController;
            }
        }else if([viewController isKindOfClass:[TermsViewController class]]){
            //Se veio da tela de termos é pq foi cancelado o processo
            [viewController dismissViewControllerAnimated:YES completion:nil];
        
        }else if ([viewController isKindOfClass:[WelcomeViewController class]]){
            
            if (_requirePaymentData && _skipPayment == NO) {
                // Após a tela de welcome, se exige dados de pagamento, inicia fluxo de adição de cartão
                CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] initWithAcceptedPaymentTypes:self.acceptedPaymentTypes andAcceptedBrands:self.acceptedBrands];
                flowController.loginWithPaymentCompletion = _loginWithPaymentCompletion;
                flowController.onLoginOrAccountCreation = YES;
                
                [flowController startFlowWithViewController:viewController];
            }else{
                User *user = [User sharedUser];
                
                // Finaliza o fluxo de login
                [viewController dismissViewControllerAnimated:YES completion:^{
                    if (_loginCompletion != nil) _loginCompletion(user.phoneNumber, user.emailAddress, user.token);
                }];
            }
            
        }
    }
    
    //    } else if ([viewController isKindOfClass:[SignUpViewController class]]) {
    //        // Após a tela de cadastro, apresenta a tela de senha
    //        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
    //                       instantiateViewControllerWithIdentifier:@"PinViewController"];
    //        ((PinViewController *)destination).signFlowController = self;
    //    } else if ([viewController isKindOfClass:[PinConfirmationViewController class]]) {
    //        // Após a tela de senha, apresenta a tela de challenge
    //        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
    //                       instantiateViewControllerWithIdentifier:@"ChallengeViewController"];
    //        ((ChallengeViewController *)destination).signFlowController = self;
    //    } else if ([viewController isKindOfClass:[ChallengeViewController class]] && _requirePaymentData) {
    //        // Após a tela de challenge, se exige dados de pagamento, inicia fluxo de adição de cartão
    //        CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] initWithAcceptedPaymentTypes:self.acceptedPaymentTypes andAcceptedBrands:self.acceptedBrands];
    //        flowController.loginWithPaymentCompletion = _loginWithPaymentCompletion;
    //        flowController.onLoginOrAccountCreation = YES;
    //
    //        [flowController startFlowWithViewController:viewController];
    //    } else if ([viewController isKindOfClass:[ChallengeViewController class]]) {
    //        // Após a tela de challenge, se não exige dados de pagamento, apresenta tela final
    //        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
    //                       instantiateViewControllerWithIdentifier:@"AccountCreatedViewController"];
    //        ((AccountCreatedViewController *)destination).signFlowController = self;
    //    } else if ([viewController isKindOfClass:[AccountCreatedViewController class]]) {
    //        // Após tela final, chama o callback de login
    //        [viewController dismissViewControllerAnimated:YES completion:^{
    //            if (_loginCompletion != nil) {
    //                User *sharedUser = [User sharedUser];
    //                _loginCompletion(sharedUser.phoneNumber, sharedUser.emailAddress, sharedUser.token);
    //            }
    //        }];
    //    }
    
    if (destination != nil) {
        destination.title = @"Cadastro";
        [viewController.navigationController pushViewController:destination animated:YES];
    }
}

- (void)viewControllerWillClose:(UIViewController *)viewController {
    // Caso o usuário feche uma tela de inserção de dados após o login, o login deve ser desfeito
    if (_isLogin &&
        ([viewController isKindOfClass:[DataFieldViewController class]] ||
         [viewController isKindOfClass:[PinViewController class]])) {
            [[Lib4all sharedInstance] callLogout:nil];
        }
}


@end
