//
//  PasswordViewController.h
//  Example
//
//  Created by Cristiano Matte on 14/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface PasswordViewController : UIViewController

@property (strong, nonatomic) SignFlowController *signFlowController;

@end
