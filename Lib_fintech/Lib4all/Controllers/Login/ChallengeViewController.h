//
//  ChallengeViewController.h
//  Example
//
//  Created by 4all on 4/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface ChallengeViewController : UIViewController

@property (strong, nonatomic) SignFlowController *signFlowController;

@end
