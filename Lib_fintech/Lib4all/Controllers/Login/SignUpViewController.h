//
//  SignUpViewController.h
//  Lib4all
//
//  Created by 4all on 3/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface SignUpViewController : UIViewController
    
@property (strong, nonatomic) SignFlowController *signFlowController;

@end
