//
//  ChoosePaymentCardViewController.m
//  Example
//
//  Created by Cristiano Matte on 19/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "ChoosePaymentCardViewController.h"
#import "CardComponentViewController.h"
#import "LoadingViewController.h"
#import "LayoutManager.h"
#import "CreditCardsList.h"
#import "BaseNavigationController.h"
#import "Services.h"

@interface ChoosePaymentCardViewController ()

@property (weak, nonatomic) IBOutlet UILabel *chooseCardLabel;
@property (weak, nonatomic) IBOutlet UIView *cardComponentView;

@property (copy, nonatomic) NSString *selectedCardId;

@end

@implementation ChoosePaymentCardViewController

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
}

// MARK: - Actions

- (IBAction)continueButtonTouched {
    _signFlowController.selectedCardId = _selectedCardId;
    [_signFlowController viewControllerDidFinish:self];
}

- (IBAction)closeButtonTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self configureLayout];
}

// MARK: - Layout

- (void)configureLayout {
    // Configura view
    LayoutManager *layoutManager = [LayoutManager sharedManager];
    
    self.view.backgroundColor = [layoutManager backgroundColor];
    
    // Configura navigation bar
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Cartão";
    /*
    UIImageView *imgTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    imgTitle.image = [UIImage imageNamed:@"4allwhite"];
    imgTitle.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = imgTitle;
    */
    
    self.chooseCardLabel.font = [layoutManager fontWithSize:[layoutManager titleFontSize]];
    self.chooseCardLabel.textColor = [layoutManager darkGray];
    
    _selectedCardId = [[[CreditCardsList sharedList] getDefaultCard] cardId];
    CardComponentViewController *cardComponent = [[CardComponentViewController alloc] initWithCardId:_selectedCardId];
    cardComponent.view.frame = CGRectMake(0.0, 0.0, _cardComponentView.frame.size.width, _cardComponentView.frame.size.height);
    
    cardComponent.didSelectCardCompletionBlock = ^(NSString *cardId) {
        LoadingViewController *loading = [[LoadingViewController alloc] init];
        [loading startLoading:self title:@"Aguarde..."];

        [cardComponent changeCardId:cardId];
        _selectedCardId = cardId;
        
        // Altera o cartão padrão no servidor (best effort)
        Services *service = [[Services alloc] init];
        
        service.failureCase = ^(NSString *cod, NSString *msg){ [loading finishLoading:nil]; };
        service.successCase = ^(NSDictionary *response){ [loading finishLoading:nil]; };
        
        [service setDefaultCardWithCardID:cardId];
    };
    
    [_cardComponentView addSubview:cardComponent.view];
    [self addChildViewController:cardComponent];
    [cardComponent didMoveToParentViewController:self];
}

@end
