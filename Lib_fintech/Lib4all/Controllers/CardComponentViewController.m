//
//  CardComponentViewController.m
//  Example
//
//  Created by Cristiano Matte on 20/07/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "CardComponentViewController.h"
#import "LayoutManager.h"
#import "Lib4all.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "CreditCardsList.h"
#import "CreditCard.h"

@interface CardComponentViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *cardBrandImageView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeCardButton;

@property (nonatomic, copy) NSString *cardId;

@end

@implementation CardComponentViewController

- (id)initWithCardId:(NSString *)cardId {
    self =  [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil] instantiateViewControllerWithIdentifier:@"CardComponentViewController"];
    
    if (self) {
        self.cardId = cardId;
    }
    
    return self;
}

- (void)changeCardId:(NSString *)cardId {
    self.cardId = cardId;
    [self getCardDetails];
}

#pragma mark - View controller life cycle
- (void)viewDidLoad {
    [super viewDidLoad];

    //Button 'Alterar'
    self.changeCardButton.userInteractionEnabled = YES;
    [self.changeCardButton.titleLabel setFont:[[LayoutManager sharedManager] fontWithSize:15.0]];
    [self.changeCardButton setTitleColor:[[LayoutManager sharedManager] lightGreen] forState:UIControlStateNormal];
    
    //Labels
    self.cardNumberLabel.font = [[LayoutManager sharedManager] fontWithSize:15.0];
    
    [self getCardDetails];
}

#pragma mark - Actions
- (IBAction)changeCardButtonTouched {
    [[[Lib4all alloc] init] showCardPickerInViewController:self.parentViewController completionBlock:^(NSString *cardID) {
        if (self.didSelectCardCompletionBlock != nil) {
            self.didSelectCardCompletionBlock(cardID);
        }
    }];
}

- (void)getCardDetails {
    /*
     * Se o cartão selecionado está salvo localmente, recupera os dados locais.
     * Caso contrário, obtém os dados do servidor.
     */
    CreditCard *card = [[CreditCardsList sharedList] getCardWithID:_cardId];
    
    if (card != nil) {
        self.cardBrandImageView.image = [card getBrandImage];
        self.cardNumberLabel.text = [card getMaskedPan];
    } else {
        Services *service = [[Services alloc] init];
        
        service.failureCase = ^(NSString *cod, NSString *msg){ };
        
        service.successCase = ^(NSDictionary *response){
            CreditCard *card = [[CreditCard alloc] init];
            card.brandId = response[BrandIDKey];
            card.lastDigits = response[LastDigitsKey];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.cardBrandImageView.hidden = NO;
                self.cardNumberLabel.hidden = NO;
                self.changeCardButton.hidden = NO;
                self.cardBrandImageView.image = [card getBrandImage];
                self.cardNumberLabel.text = [card getMaskedPan];
            });
        };
        
        self.cardBrandImageView.hidden = YES;
        self.cardNumberLabel.hidden = YES;
        self.changeCardButton.hidden = YES;
        [service getCardDetailsWithCardID:self.cardId];
    }
}

@end
