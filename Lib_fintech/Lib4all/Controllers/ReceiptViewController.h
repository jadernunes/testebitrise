//
//  ReceiptViewController.h
//  Example
//
//  Created by 4all on 30/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

@interface ReceiptViewController : UIViewController

@property (nonatomic, strong) Transaction *transactionInfo;

@end
