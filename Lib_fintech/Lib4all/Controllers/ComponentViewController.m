//
//  ComponentViewController.m
//  Lib4all
//
//  Created by 4all on 3/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "ComponentViewController.h"
#import "User.h"
#import "Services.h"
#import "CreditCardsList.h"
#import "CreditCard.h"
#import "LayoutManager.h"
#import "CompleteDataViewController.h"
#import "BaseNavigationController.h"
#import "Lib4allPreferences.h"
#import "SignFlowController.h"
#import "SignInViewController.h"
#import "CardAdditionFlowController.h"
#import <CoreLocation/CoreLocation.h>
#import "PaymentFlowController.h"
#import "LocalizationFlowController.h"
#import "LocationManager.h"
#import "LoginPaymentAction.h"
#import "CardsTableViewController.h"
@interface ComponentViewController () <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIButton *mainButton;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIButton *changeCardButton;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardBrandImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonPipelineWidthConstraint;

@property LoginPaymentAction * loginPaymentAction;

@end

@implementation ComponentViewController

@synthesize requireFullName = _requireFullName;
@synthesize requireCpfOrCnpj = _requireCpfOrCnpj;
//@synthesize acceptedPaymentMode = _acceptedPaymentMode;
@synthesize termsOfServiceUrl = _termsOfServiceUrl;

- (id)init {
    
    self = [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil] instantiateViewControllerWithIdentifier:@"ComponentVC"];
    
    if (self) {
        self.acceptedPaymentTypes = [[Lib4allPreferences sharedInstance] acceptedPaymentTypes];
        self.acceptedBrands = [[[Lib4allPreferences sharedInstance] acceptedBrands] allObjects];
    }

    return self;
}

- (id)initWithAcceptedPaymentMode:(PaymentMode)paymentMode {
    self = [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil] instantiateViewControllerWithIdentifier:@"ComponentVC"];
    
    if (self) {
        
        NSAssert(paymentMode == PaymentModeDebit || paymentMode == PaymentModeCredit || paymentMode == PaymentModeCreditAndDebit,
                 @"LIB4ALL: initWithAcceptedPaymentMode: deve receber PaymentModeDebit, PaymentModeCredit ou PaymentModeCreditAndDebit");
        
        if (paymentMode == PaymentModeDebit) {
            self.acceptedPaymentTypes = @[@(Debit)];
        } else if (paymentMode == PaymentModeCredit) {
            self.acceptedPaymentTypes = @[@(Credit)];
        } else {
            self.acceptedPaymentTypes = @[@(Credit), @(Debit)];
        }
        
        [[Lib4allPreferences sharedInstance] setAcceptedPaymentTypes:self.acceptedPaymentTypes];
        
        self.acceptedBrands = [[[Lib4allPreferences sharedInstance] acceptedBrands] allObjects];
    }
    
    return self;
}

-(id)initWithAcceptedPaymentTypes:(NSArray *)arrayPaymentTypes andAcceptedBrands:(NSArray *)arrayBrands{
    self = [[UIStoryboard storyboardWithName:@"Lib4all" bundle: nil] instantiateViewControllerWithIdentifier:@"ComponentVC"];
    if (self) {
        for (int i = 0; i < [arrayPaymentTypes count]; i++) {
            NSInteger type = [(NSNumber *)[arrayPaymentTypes objectAtIndex:i] integerValue];
            
            NSAssert(type >= 0 && type < NumOfTypes, @"LIB4ALL: initWithAcceptedPaymentTypes deve receber um array de PaymentType.");
        }
        
        for (int i = 0; i < [arrayBrands count]; i++) {
            NSInteger brand = [(NSNumber *)[arrayBrands objectAtIndex:i] integerValue];
            
            NSAssert(brand >= 0 && brand < NumOfBrands, @"LIB4ALL: initWithAcceptedPaymentTypes deve receber um array de CardBrand.");
        }
        
        self.acceptedPaymentTypes = arrayPaymentTypes;
        self.acceptedBrands = arrayBrands;
    }
    
    
    return self;
}

- (void)setRequireFullName:(BOOL)requireFullName {
    [[Lib4allPreferences sharedInstance] setRequireFullName:requireFullName];
    _requireFullName = requireFullName;
}

- (BOOL)requireFullName {
    return [[Lib4allPreferences sharedInstance] requireFullName];
}

- (void)setRequireCpfOrCnpj:(BOOL)requireCpfOrCnpj {
    [[Lib4allPreferences sharedInstance] setRequireCpfOrCnpj:requireCpfOrCnpj];
    _requireCpfOrCnpj = requireCpfOrCnpj;
}

- (BOOL)requireCpfOrCnpj {
    return [[Lib4allPreferences sharedInstance] requireCpfOrCnpj];
}


- (void)setTermsOfServiceUrl:(NSString *)termsOfServiceUrl {
    _termsOfServiceUrl = termsOfServiceUrl;
    [[Lib4allPreferences sharedInstance] setTermsOfServiceURL:[NSURL URLWithString:termsOfServiceUrl]];
}

- (NSString *)termsOfServiceUrl {
    return _termsOfServiceUrl;
}

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSAssert([Lib4allPreferences sharedInstance].applicationID != nil && [Lib4allPreferences sharedInstance].applicationVersion != nil,
             @"LIB4ALL: Antes de instanciar a classe ComponentViewController, você deve configurar o applicationID e o applicationVersion.");
    
    [self configureLayout];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[Lib4allPreferences sharedInstance] setCurrentVisibleComponent:self];
    
    [self updateComponentViews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([[Lib4allPreferences sharedInstance] currentVisibleComponent] == self) {
        [[Lib4allPreferences sharedInstance] setCurrentVisibleComponent:nil];
    }
}

// MARK: - Actions

- (IBAction)mainButtonTouched {
    
    self.loginPaymentAction = [[LoginPaymentAction alloc] init];
    
    [self.loginPaymentAction callMainAction:self delegate:self.delegate acceptedPaymentTypes:self.acceptedPaymentTypes acceptedBrands:self.acceptedBrands];
}

- (void)showDefaultCard {
    if ([[User sharedUser] currentState] == UserStateLoggedIn) {
        // Obtém o cartão default de maneira assíncrona pois a leitura do arquivo pode demorar
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            CreditCard *defaultCard = [[CreditCardsList sharedList] getDefaultCard];
            
            if (defaultCard != nil) {
                UIImage *brandImage = [defaultCard getBrandImage];
                NSString *maskedPan = [defaultCard getMaskedPan];
                NSString *cardType;
                
                switch (defaultCard.type) {
                    case CardTypeDebit:
                        cardType = @"DÉBITO";
                        break;
                    case CardTypeCredit:
                        cardType = @"CRÉDITO";
                        break;
                    case CardTypeCreditAndDebit:
                        cardType = @"CRÉDITO E DÉBITO";
                        break;
                }
                
                if (!defaultCard.isProvider) {
                    cardType = defaultCard.sharedDetails[0][@"identifier"];
                }
                
                // Atualiza os dados do cartão default e exibe a view do cartão
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (brandImage) self.cardBrandImage.image = brandImage;
                    self.cardTypeLabel.text = cardType;
                    self.cardNumberLabel.text = maskedPan;
                    self.cardView.hidden = NO;
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.cardView.hidden = YES;
                });
            }
        });
    } else {
        self.cardView.hidden = YES;
    }
}

// MARK: - Layout

- (void)configureLayout {
    // cardView
    self.cardView.layer.cornerRadius        = 4.0f;
    self.cardView.layer.borderColor         = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6].CGColor;
    self.cardView.layer.masksToBounds       = NO;
    self.cardView.layer.shadowOffset        = CGSizeMake(0, 1);
    self.cardView.layer.shadowRadius        = 2;
    self.cardView.layer.shadowColor         = [[LayoutManager sharedManager] darkGray].CGColor;
    self.cardView.layer.shadowOpacity       = 0.5;
    self.cardView.hidden = YES;
    
    // changeCardNumber
    [self.changeCardButton.titleLabel setFont:[[LayoutManager sharedManager] fontWithSize:15.0]];
    [self.changeCardButton setTitleColor:[[LayoutManager sharedManager] lightGreen] forState:UIControlStateNormal];
    
    // Labels
    self.cardNumberLabel.font = [[LayoutManager sharedManager] fontWithSize:15.0];
    self.cardTypeLabel.font = [[LayoutManager sharedManager] fontWithSize:12.0];
    
    // mainButton
    self.mainButton.layer.cornerRadius = 5.0f;
    [self.mainButton.titleLabel setFont:[[LayoutManager sharedManager] fontWithSize:15.0]];
    [self.mainButton setBackgroundColor:[[Lib4allPreferences sharedInstance] mainButtonColor]];
    //[self.mainButton setBackgroundColor:[[LayoutManager sharedManager] lightGreen]];
    
    
    // buttonPipelineWidthConstraint
    self.buttonPipelineWidthConstraint.constant = 0.5f;
}

- (void)updateComponentViews {
    // Exibe o título do botão de acordo com o estado do usuário
    if ([[User sharedUser] currentState] == UserStateLoggedIn) {
        [self.mainButton setTitle:self.buttonTitleWhenLogged forState:UIControlStateNormal];
        
        // Atualiza a lista de cartões no servidor e o cartão default exibido
        Services *service = [[Services alloc] init];
        service.failureCase = ^(NSString *code, NSString *message) { };
        service.successCase = ^(NSDictionary *response){
            dispatch_async(dispatch_get_main_queue(), ^{ [self showDefaultCard]; });
        };
        
        [service listCards];
    } else {
        [self.mainButton setTitle:self.buttonTitleWhenNotLogged forState:UIControlStateNormal];
    }
    
    // Atualiza o cartão default exibido
    [self showDefaultCard];
}

//alteração Bruno Fernandes 3/2/17
//agora passa os tipos de pagamento e bandeiras do componente para o CardsTableViewController
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ((segue.identifier != nil) && ([segue.identifier isEqualToString:@"changePaymentTypeComponentButtonSegue"])) {
        BaseNavigationController * baseNavController = segue.destinationViewController;
        CardsTableViewController * cardTableViewController = [[baseNavController viewControllers] objectAtIndex:0];
        
        cardTableViewController.acceptedPaymentTypes = self.acceptedPaymentTypes;
        cardTableViewController.acceptedBrands = self.acceptedBrands;
        
        
    }
}



@end
