//
//  QRCodeMerchantOfflineViewController.h
//  Example
//
//  Created by 4all on 20/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCodeMerchantOfflineViewController : UIViewController

@property (nonatomic, strong) NSString *contentQRCode;

@end
