//
//  AcceptedCardsViewController.m
//  Example
//
//  Created by Cristiano Matte on 26/09/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "AcceptedCardsViewController.h"
#import "Lib4allPreferences.h"
#import "CardUtil.h"

@interface AcceptedCardsViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *acceptedCardsView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (strong, nonatomic) NSArray *acceptedBrandsImages;

@property NSArray * acceptedBrands;

@end

@implementation AcceptedCardsViewController

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _acceptedCardsView.layer.cornerRadius = 15.0;
    
    NSSet *acceptedBrands;
    if (self.acceptedBrands == nil)
        acceptedBrands = [[Lib4allPreferences sharedInstance] acceptedBrands];
    else
        acceptedBrands = [[NSSet alloc] initWithArray:self.acceptedBrands];
    
    NSMutableArray *brandsImages = [[NSMutableArray alloc] initWithCapacity:acceptedBrands.count];
    
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandVisa]]) {
        [brandsImages addObject:[UIImage imageNamed:@"visa"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandMastercard]]) {
        [brandsImages addObject:[UIImage imageNamed:@"mastercard"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandDiners]]) {
        [brandsImages addObject:[UIImage imageNamed:@"diners"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandElo]]) {
        [brandsImages addObject:[UIImage imageNamed:@"elo"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandAmex]]) {
        [brandsImages addObject:[UIImage imageNamed:@"amex"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandDiscover]]) {
        [brandsImages addObject:[UIImage imageNamed:@"discover"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandAura]]) {
        [brandsImages addObject:[UIImage imageNamed:@"aura"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandJCB]]) {
        [brandsImages addObject:[UIImage imageNamed:@"jcb"]];
    }
    if ([acceptedBrands containsObject:[NSNumber numberWithInt:CardBrandHiper]]) {
        [brandsImages addObject:[UIImage imageNamed:@"hiper"]];
    }
    
    _acceptedBrandsImages = brandsImages;
}

- (void)viewDidLayoutSubviews {    
    _collectionViewHeightConstraint.constant = _collectionView.contentSize.height;
}

// MARK: - Actions

- (IBAction)okButtonTouched {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// MARK: - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _acceptedBrandsImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cardBrandCell" forIndexPath:indexPath];
    UIImageView *imageView = [cell viewWithTag:1];
    
    imageView.image = _acceptedBrandsImages[indexPath.row];
    
    return cell;
}

// MARK: - Collection view delegate flow layout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {    
    long cellCount = [self collectionView:collectionView numberOfItemsInSection:section];
    double totalCellWidth = 46.0 * cellCount;
    double totalSpacingWidth = 10.0 * (cellCount - 1);
    
    [collectionView setNeedsLayout];
    
    // Se a collection view possui apenas uma linha, centraliza as células dessa linha
    if (collectionView.frame.size.width > totalCellWidth + totalSpacingWidth) {
        double inset = (collectionView.frame.size.width - (totalCellWidth + totalSpacingWidth)) / 2;
        return UIEdgeInsetsMake(0, inset, 0, inset);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
