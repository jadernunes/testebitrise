//
//  ReceiptViewController.m
//  Example
//
//  Created by 4all on 30/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "ReceiptViewController.h"
#import "LayoutManager.h"

@interface ReceiptViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelSuccess;
@property (weak, nonatomic) IBOutlet UIImageView *imageCheck;
@property (weak, nonatomic) IBOutlet UILabel *labelMerchantName;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelThanks;
@property (weak, nonatomic) IBOutlet UILabel *labelHour;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *installmentsLabel;


@end

@implementation ReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)closeController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) configureLayout{
    
    LayoutManager *layout = [LayoutManager sharedManager];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    self.navigationItem.hidesBackButton = YES;
    
    [self.imageCheck setImage:[self.imageCheck.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    self.imageCheck.tintColor = [layout primaryColor];
    
    _labelThanks.font = [layout fontWithSize:layout.titleFontSize];
    _labelThanks.textColor = layout.primaryColor;
    
    _labelSuccess.font = [layout fontWithSize:layout.subTitleFontSize];
    _labelSuccess.textColor = layout.darkGray;
    
    _labelMerchantName.font = [layout fontWithSize:layout.regularFontSize];
    _labelMerchantName.textColor = layout.lightGray;
    
    _labelDate.font = [layout fontWithSize:layout.regularFontSize];
    _labelDate.textColor = layout.primaryColor;
    
    _labelHour.font = [layout fontWithSize:layout.regularFontSize];
    _labelHour.textColor = layout.darkGray;
    [formatter setDateFormat:@"HH:mm"];
    [_labelHour setText:[formatter stringFromDate:[NSDate new]]];
    
    _labelAmount.font = [layout fontWithSize:layout.regularFontSize];
    _labelAmount.textColor = layout.darkGray;
    
    self.labelAmount.font = [layout fontWithSize:layout.regularFontSize];
    self.labelAmount.textColor = layout.darkGray;
    self.labelAmount.text = [NSString stringWithFormat:@"R$ %.2f", ([[self.transactionInfo amount] doubleValue]/100.0)];
    
    self.labelDate.font = [layout fontWithSize:layout.regularFontSize];
    self.labelDate.textColor = layout.primaryColor;
    
    [formatter setDateFormat:@"dd/MM/yyyy"];
    [self.labelDate setText:[formatter stringFromDate:[NSDate new]]];
    
    if (_transactionInfo.parcels != nil && ![_transactionInfo.parcels isEqualToString:@""] && [_transactionInfo.parcels integerValue] > 1) {
        [self.installmentsLabel setHidden:NO];
        self.installmentsLabel.text = [NSString stringWithFormat:@"Parcelado em %@x", _transactionInfo.parcels];
        self.installmentsLabel.font = [layout fontWithSize:layout.regularFontSize];
        self.installmentsLabel.textColor = layout.darkGray;
    }
    
    self.labelMerchantName.font = [layout fontWithSize:layout.regularFontSize];
    self.labelMerchantName.textColor = layout.lightGray;
    self.labelMerchantName.text = [[[self.transactionInfo merchant] name] stringByRemovingPercentEncoding];
}

@end
