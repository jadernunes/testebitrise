//
//  FlowPagerViewController.m
//  Example
//
//  Created by 4all on 12/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import "FlowPagerViewController.h"
#import "GenericDataViewController.h"
#import "SISUNameDataField.h"
#import "SISUPhoneNumberDataField.h"

@interface FlowPagerViewController ()
{
    UIPageControl *pageControl;
    NSArray *dataSource;
}
@end

@implementation FlowPagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUp];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Methods

-(void)setUp{
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                              navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                            options:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    [[self.pageViewController view] setFrame:self.view.bounds];
    
    [self setUpControllers];
    
    [self.pageViewController setViewControllers:@[dataSource[0]]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:YES
                                     completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

/*
 if (requireFullName) {
 // Redireciona para tela de inserção de nome
 ((DataFieldViewController *)destination).dataFieldProtocol = [[NameDataField alloc] init];
 } else if (requireCpf) {
 // Redireciona para tela de inserção de cpf
 ((DataFieldViewController *)destination).dataFieldProtocol = [[CPFDataField alloc] init];
 } else if (requireBirthdate) {
 // Redireciona para tela de inserção de data de nascimento
 ((DataFieldViewController *)destination).dataFieldProtocol = [[BirthdateDataField alloc] init];
 }

 */

-(void) setUpControllers{
//    ControllerObject *objct1 = [[ControllerObject alloc] initWithDescription:@"Description 1" subDescription:@"Subdescription 1" buttonText:@"Ok" controllerType:ControllerTypeUnknown buttonAction:^{
//        NSLog(@"%@", @"CLICOU CONTROLLER 1");
//    } ];
//    
//    GenericDataViewController *nameController = [GenericDataViewController getConfiguredControllerWithObject:objct1];
//    nameController.dataFieldProtocol = [SISUNameDataField init];
//    nameController.index = 0;
//    
//    ControllerObject *objct2 = [[ControllerObject alloc] initWithDescription:@"Description 2" subDescription:@"Subdescription 2" buttonText:@"Ok" controllerType:ControllerTypeUnknown buttonAction:^{
//        NSLog(@"%@", @"CLICOU CONTROLLER 2");
//    } ];
//    GenericDataViewController *controller2 = [GenericDataViewController getConfiguredControllerWithObject:objct2];
//    controller2.index = 1;
//    
//    ControllerObject *objct3 = [[ControllerObject alloc] initWithDescription:@"Description 3" subDescription:@"Subdescription 3" buttonText:@"Ok" controllerType:ControllerTypeUnknown buttonAction:^{
//        NSLog(@"%@", @"CLICOU CONTROLLER 3");
//    } ];
//    
//    GenericDataViewController *controller3 = [GenericDataViewController getConfiguredControllerWithObject:objct3];
//    controller3.index = 2;
//    
//    dataSource = @[nameController, controller2, controller3];
}

//- (void)viewWillLayoutSubviews
//{
//    [super viewWillLayoutSubviews];
//    self.pageViewController.view.frame = CGRectMake(0, 0, 320, CGRectGetHeight(self.view.frame));
//}


-(void)viewControllerDidFinish:(UIViewController *)viewController{
    
}

-(void)viewControllerWillClose:(UIViewController *)viewController{
    
}

#pragma mark - Page Controller
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [(id)viewController index];
    
    if(index == 0) {
        return nil;
    }
    
    index--;
    
    return dataSource[index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(id)viewController index];
    
    index++;
    
    if(index == dataSource.count) {
        return nil;
    }
    
    return dataSource[index];
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return dataSource.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

@end
