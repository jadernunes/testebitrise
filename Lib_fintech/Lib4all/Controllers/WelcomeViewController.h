//
//  WelcomeViewController.h
//  Example
//
//  Created by 4all on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface WelcomeViewController : UIViewController

@property (nonatomic, strong) SignFlowController *signFlowController;
@end
