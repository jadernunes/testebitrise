//
//  Lib4allInfo.h
//  Example
//
//  Created by Cristiano Matte on 02/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

#ifndef Lib4allInfo_h
#define Lib4allInfo_h

static NSString* const Lib4allVersion = @"1.17.2-beta";

#endif /* Lib4allInfo_h */
