//
//  ApiServices.m
//  Recarga
//
//  Created by MacMini on 01/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "ApiServicesMobility.h"
#import "MobilityFilesUtil.h"

@interface ApiServicesMobility ()

@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;

@end

@implementation ApiServicesMobility

- (id)init {
    self = [super init];
    
    if(self) {
        NSString *baseURLString;

        //busca a URL do servidor das configurações (info.plist)
        NSString *configPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSDictionary *dicInfo = [NSDictionary dictionaryWithContentsOfFile:configPath];
        NSDictionary *dicConfig = [dicInfo objectForKey:@"CONFIG"];
        
        baseURLString = [NSString stringWithFormat:@"%@/v1/",[dicConfig objectForKey:@"URL_BASE_MOBILITY"]];
            
        self.baseURL = [NSURL URLWithString:baseURLString];
        
        self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:self.baseURL];
        self.manager.requestSerializer  = [AFJSONRequestSerializer serializer];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.manager.requestSerializer.timeoutInterval = 60;
    }
    
    return self;
}

- (void) recharge: (NSDictionary *) parameters {
    
    NSString *url = @"recharge";
    //NSString *url = @"fakeRecharge";
    
    [self.manager POST:url parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

-(void) completeDebitRecharge: (NSDictionary *) parameters
{
    NSString *url = [NSString stringWithFormat:@"completeRecharge?idRecharge=%@",parameters[@"idRecharge"]];

    [self.manager POST:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

-(void)consultaSaldoECartaoValido:(NSString *)issuerNumber andIssuerId:(NSString *)issuerId andCityID:(NSString *)cityId andSessionToken:(NSString *)sessionToken {
    
    NSString *url = [NSString stringWithFormat:@"cards?issuerNumber=%@&issuerId=%@",issuerNumber,issuerId];
    
    if (cityId != nil) {
        url = [NSString stringWithFormat:@"%@&cityId=%@", url, cityId];
    }
    
    if ([[Lib4all sharedInstance] hasUserLogged]) {
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&sessionToken=%@",sessionToken]];
    }
    
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];

    }];
}

-(void)deletarCartaoUsuario:(NSString *)issuerNumber andSessionToken:(NSString *)sessionToken
{
    NSString *url = [NSString stringWithFormat:@"cards?issuerNumber=%@&sessionToken=%@",issuerNumber,sessionToken];
    
    [self.manager DELETE:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        //[[SessionManager sharedInstance] setNeedUpdateList:YES];
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        
        [self handleError:operation.responseObject];
        
    }];
}

-(void)meusCartoes:(NSString *)sessionToken
{
    NSString *url = [NSString stringWithFormat:@"me/cards?sessionToken=%@",sessionToken];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
        
    }];
}

-(void)consultaHome {
    NSString *url = @"homes?appId=1";
    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary *responseDict = [[NSDictionary alloc] initWithDictionary:responseObject copyItems:YES];
        [MobilityFilesUtil storeInFile:responseDict fileName:@"mobilityCatalog"];
        
        self.successCase(responseObject);
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
        
    }];
}

-(void)buscaSemCheckBalance:(NSString *)issuerId {
    //issuers/4/products
    NSString *url = [NSString stringWithFormat:@"issuers/%@/products",issuerId];
    
    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {                
        self.successCase(responseObject);
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
        
    }];
}

- (void)handleError:(NSDictionary *)responseObj {
    
    if ([responseObj objectForKey:@"message"]) {
        if ([responseObj objectForKey:@"code"]) {
            NSNumber *code = [responseObj objectForKey:@"code"];
            NSString *message = [responseObj objectForKey:@"message"];
            
            self.failureCase(code.stringValue,message);
        }
        
        return;
    }
    self.failureCase(@"-1",@"Não foi possível completar a operação.");

}


-(void)buscaValores:(NSString *)issuerId productId:(NSString *)productId {
    //issuers/4/products
    NSString *url = [NSString stringWithFormat:@"issuers/%@/products/%@/values",issuerId, productId];
    
    [self.manager GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        self.successCase(responseObject);
        
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [self handleError:operation.responseObject];
    }];
}

@end
