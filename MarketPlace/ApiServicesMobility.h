//
//  ApiServices.h
//  Recarga
//
//  Created by MacMini on 01/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Lib4all.h"

@interface ApiServicesMobility : NSObject

@property (nonatomic, strong) NSURL *baseURL;
@property (copy) void (^successCase)(id data);
@property (copy) void (^failureCase)(NSString *errorID, NSString * errorMessage);

- (void) recharge: (NSDictionary *) dic ;

-(void) completeDebitRecharge: (NSDictionary *) dic ;

-(void)consultaSaldoECartaoValido:(NSString *)issuerNumber andIssuerId:(NSString *)issuerId andCityID:(NSString *)cityId andSessionToken:(NSString *)sessionToken;

-(void)deletarCartaoUsuario:(NSString *)issuerNumber andSessionToken:(NSString *)sessionToken;

-(void)meusCartoes:(NSString *)sessionToken;

-(void)consultaHome;

-(void)buscaSemCheckBalance:(NSString *)issuerId;

-(void)buscaValores:(NSString *)issuerId productId:(NSString *)productId;

@end
