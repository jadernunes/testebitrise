//
//  ConstantsMessge.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 08/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

//MARK: - Controller Titles

let kMySchedulesTitle = "MEUS AGENDAMENTOS"
let kMyAddressesTitle = "MEUS ENDEREÇOS"
let kQRScannerTitle = "LEITOR DE CÓDIGO QR"
let kCouponsTitle = "Cupons de desconto"
let kMyCouponsTitle = "Meus cupons"
let kDiscountCouponTitle = "CUPOM DE DESCONTO"
let kMyFidelitiesTitle = "MINHAS FIDELIDADES"
let kScheduleTitle = "AGENDAMENTO"
let kOrdersTitle = "MEUS PEDIDOS"
let kMyPushesTitle = "MINHAS NOTIFICAÇÕES"
let kTrackingTitle = "PEDIDO"
let kBookingTitle = "RESERVA"
let kPaymentConfirmedTitle = "PAGAMENTO CONFIRMADO"
let kAddAddressTitle = "ADICIONAR ENDEREÇO"
let kPayCheckTitle = "PAGAR COMANDA"
let kCheckPaymentSuccessTitle = "PAGAMENTO DE COMANDA"
let kConfirmScheduleTitle = "CONFIRMAR AGENDAMENTO"
let kOndePareiTitle = "ONDE PAREI"
let kDetailEventTitle = "EVENTO"
let kDetailPromotionTitle = "PROMOÇÃO"
let kDetailWarningTitle = "AVISOS"
let kDetailCampaignTitle = "CUPOM"
let kMapTitle = "MAPA"
let kPaymentQRTitle = "PAGAMENTO"
let kPaymentQRConfirmationTitle = "CONFIRMAÇÃO DE PAGAMENTO"
let kCartTitle = "CARRINHO"
let kOrderDoneTitle = "PEDIDO REALIZADO"
let kPaymentParkTitle = "PAGAR ESTACIONAMENTO"

//MARK: - Messages Showing in Alerts

let kMessageNoTicketsAvailable = "Nenhum ingresso disponível para o evento!"
let kMessageChooseNumberOfTickets = "Selecione a quantidade de ingressos"
let kMessageChangeTable = "Você deseja trocar a mesa selecionada?"
let kMessageChangeAddress = "Você deseja trocar o endereço selecionado?"
let kMessageInvalidQR = "QR Code inválido!"
let kMessageAddressNotAvailable = "Endereço selecionado não atendido pela loja"
let kMessageInsertZip = "Digite um CEP!"
let kMessageInsertAddressIdentifier = "Escolha um apelido (identificador) para o seu endereço!"
let kMessageInsertHouseNumber = "Digite o número do apartamento/casa."
let kMessageInsertStreet = "Digite o nome da sua rua"
let kMessageInsertNeighborhood = "Digite o nome do seu bairro"
let kMessageChooseAtLeast = "Você deve escolher no mínimo:\n"
let kMessageAllExtrasChosen = "Você já escolheu todos os itens deste grupo. \nVocê pode remover e escolher outros!"
let kMessageTicketsSoonInApp = "Em breve a compra de ingresso será feita diretamente na app. Aguarde!"
let kMessageLoadingItems = "Carregando itens"
let kMessageLoadingSchedules = "Carregando horários"
let kMessageLoadingItemsAndSchedules = "Carregando itens e horários"
let kMessageNoItemsAvailable = "Não há horários disponíveis"
let kMessageNoSchedulesAvailable = "Não há produtos ou horários disponíveis"
let kMessageNoItemsAndSchedulesAvailable = "Não há produtos disponíveis"
let kMessageNoItemsOrSchedulesFound = "Não foram encontrados produtos/serviços."
let kMessageEventAddedToCalendar = "Evento adicionado com sucesso."
let kMessageSelectDeliveryMode = "Selecione o modo de entrega."
let kMessageStoreClosed = "Essa loja encontra-se fechada no momento."
let kMessageInvalidTicket = "Ticket Invalido"
let kMessageCityNotAvailable = "Ainda não temos estabelecimentos credenciados em %@, mas estamos trabalhando para, muito em breve, estarmos juntos conectando momentos"
let kMessageShortDeliveryUnavailable = "Esta loja não possui pedido na mesa habilitado!"
let kMessageUnityNotFound = "Loja não encontrada!"
let kMessageOtherUnityActive = "Você já possui um carrinho ativo de outra loja!"
let kMessagePaymentOffline = "Informe ao estabelecimento e realize o pagamento offline com segurança."
let kMessageChooseAddress = "Por favor, selecione um endereço!"
let kMessageChooseTable = "Por favor, escaneie a mesa desejada!"
//MARK: - Alert Title

let kAlertTitle = "Atenção"
let kAlertBuyTicketTitle = "Compra de Ingresso"
let kAlertCalendarTitle = "Calendário"
let kAlertNoConnection = "Sem conexão"

//MARK: - Alert Button Action Titles

let kAlertButtonYes = "Sim"
let kAlertButtonNo = "Não"
let kAlertButtonOk = "OK"

//MARK: - Messages No Content Available

let kMessageWithoutDataAddresses    = "Você não possui endereços cadastrados"
let kMessageWithoutDataSchedules    = "Você não possui agendamentos"
let kMessageWithoutDataCoupons      = "Você não possui cupons"
let kMessageWithoutDataFidelities   = "Você não possui fidelidades"
let kMessageWithoutDataOrders       = "Você não possui pedidos"
let kMessageWithoutDataPushes       = "Você não possui notificações"
let kMessageWithoutDataVouchers     = "Você não possui vouchers"
let kMessageWithoutDataComercial    = "Ainda não há estabelecimentos cadastrados"

//MARK: - Messages Failure

let kMessageFailTicketParkValue = "Não foi possível consultar o valor do ticket"
let kMessageFailCheckValue = "Não foi possível consultar o valor da comanda"
let kMessageFailTicketValue = "Não foi possível consultar o valor do ingresso"
let kMessageUnableToExecuteAction = "Não foi possível completar a ação."


//MARK: - Messages Successful

let kMessageSuccessPayment = "Pagamento realizado com sucesso"
let kMessageSuccessBuyOneTicket = "Ticket comprado com sucesso."
let kMessageSuccessBuyMultipleTickets = "Tickets comprados com sucesso."
let kMessageSuccessPayCheck = "Comanda paga com sucesso."

//MARK: - Messages Schedule

let kMessageSolicitationSchedule = "Você receberá uma mensagem quando seu agendamento for confirmado."
let kTitleSolicitationSchedule = "SOLICITAÇÃO"
let kButtonSolicitationScheduleTitle = "SOLICITAR AGENDAMENTO"
let kSubtitleSolicitationSchedule = "Agendamento Solicitado"

let kMessageInstantSchedule = "Agendamento confirmado"
let kTitleInstantSchedule = "CONFIRMAÇÃO"
let kButtonInstantSchedule = "CONFIRMAR AGENDAMENTO"
let kSubtitleInstantSchedule = "MUITO OBRIGADO"

let kConfirmationSchedule = "Você confirma esta solicitação?"
let kErrorSelectingSchedule = "Você não pode agendar um horário anterior ao atual"

//MARK: - Messages Campaign

let kCoupomDownloaded = "CUPOM BAIXADO"
let kDownloadCoupom = "BAIXAR CUPOM"

//MARK: - Messages Pickers

let kMessageTableSizeBooking = "Quantos lugares você precisa?"
let kMessageDayBooking = "Para qual dia?"
let kMessageDateBooking = "Qual horário de sua preferência?"

//MARK: - Messages Cart

let kMessageRemoveItemFromCart = "Deseja remover o item do carrinho?"
let kMessageErrorRemovingFromCart = "Não foi possível remover item"

//MARK: - Onde Parei

let kButtonTitlePhotoOndeParei = "TIRAR UMA FOTO DO LOCAL"
let kButtonTitlePayParkOndeParei = "PAGAR ESTACIONAMENTO"

//MARK: - Generic Messages

let kPay = "PAGAR"
let kEnter = "ENTRAR"
let kClose = "Fechar"
let kWait = "Aguarde"
let kPullToRefresh = "Atualizar"
let kNewAddress = "Novo Endereço"
let kAdd = "Adicionar"
let kInsertZipCode = "Insira seu CEP"
let kDescription = "Descrição"
let kPremiumFidelity = "Fidelidade Premiada!"
let kOrderNumber = "Pedido #"
let kPromotion = "Promoção"
let kUsed = "Utilizado"
let kFree = "Liberado"
let kSeeAll = "VER TODOS"
let kEstablishment = "Estabelecimento"
let kService = "Serviço"
let kDate = "Data"
let kTime = "Horário"
let kProfessional = "Profissional"
let kAddress = "Endereço"
let kValue = "Valor"
let kAmountOrderItem = "Qtde: "
let kSubtitled = "Legendado"
let kDubbed = "Dublado"
let kFinished = "ENCERRADAS"
let kActive = "VIGENTES"
let kPromotions = "PROMOÇÕES"
let kEvents = "EVENTOS"
let kMovies = "FILMES"
let kStores = "LOJAS"
let kServices = "SERVIÇOS"
let kCategories = "CATEGORIAS"
let kFullList = "LISTA COMPLETA"
let kFoodcourt = "PRAÇA DE ALIMENTAÇÃO"
let kAccessTickets = "ACESSAR TICKETS"
let kCancelPayment = "CANCELAR PAGAMENTO"
let kTitleListUnities = "LOJAS PARTICIPANTES"
let kAccessVouchers = "ACESSAR VOUCHERS"
let kKilometers = "km"
let kMeters = "metros"
let kDelivery = "Entrega"
let kMin = "min"
let kItem = "item"
let kItems = "itens"
let kChooseAddress = "Escolher Endereço"
let kSetAddress = "Cadastrar Endereço"
let kScanTable = "Escanear Mesa"
let kVoucher = "Voucher"
let kTakeaway = "Retirar na Loja"
//MARK: - Label Texts

let kLabelVoucher = "VOUCHER"
let kLabelTakeaway = "RETIRAR NA LOJA"
let kLabelMinimumExtra = " - Mínimo:"

//Mark: - Delivery Types

let kDeliveryTypeDelivery = "Entrega em casa / Delivery"
let kDeliveryTypeShortDelivery = "Pedido na Mesa"
let kDeliveryTypeVoucher = "Pedido Voucher"
let kDeliveryTypeTakeaway = "Buscar na Loja"
