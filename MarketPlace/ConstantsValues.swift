//
//  ConstantsValues.swift
//  MarketPlace
//
//  Created by Lucas Bieniek on 17/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

let kDistanceMinPin    :Double = 0
let kDistanceMaxPin    :Double = 50000

extension UIColor {
    
    static func blueGreen4all() -> UIColor {
        return UIColor(red: 12.0/255.0, green: 104.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    }
    
    static func blue4all() -> UIColor {
        return UIColor(red: 18.0/255.0, green: 182.0/255.0, blue: 182.0/255.0, alpha: 1.0)
    }
    
    static func green4all() -> UIColor {
        return UIColor(red: 79.0/255.0, green: 164.0/255.0, blue: 68.0/255.0, alpha: 1.0)
    }
    
    static func gray4all() -> UIColor {
        return UIColor(red: 142.0/255.0, green: 142.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    }
    
    static func lightGray4all() -> UIColor {
        return UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    
    static func darkGray4all() -> UIColor {
        return UIColor(red: 61.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    }
    
    static func redFood4all() -> UIColor {
        return UIColor(red: 182.0/255.0, green: 41.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    }
    
    static func orangeFun4all() -> UIColor {
        return UIColor(red: 242.0/255.0, green: 98.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    }
    
    static func yellowMobility4all() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 216.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
}
