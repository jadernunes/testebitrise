//
//  GeometricUtils.swift
//  Geotify
//
//  Created by Matheus Luz on 8/19/16.
//  Copyright © 2016 Matheus Luz. All rights reserved.
//

import CoreLocation
import UIKit
import MapKit

func isInsideRegion(userLocation: CLLocationCoordinate2D, coordinatesToMakePolygon: [Locations]) -> Bool { // math function to check if point is inside region (polygon)
  var mapPoints: [MKMapPoint] = []
  let mpr: CGMutablePathRef = CGPathCreateMutable()
  var coords: [CLLocationCoordinate2D] = []
  for index in 0..<coordinatesToMakePolygon.count {
    let lat = Double(coordinatesToMakePolygon[index].latitude)
    let long = Double(coordinatesToMakePolygon[index].longitude)
    let coordinatesToAppend = CLLocationCoordinate2D(latitude: lat, longitude: long)
    coords.append(coordinatesToAppend)
  }
  
  for b in coords {
    let c: MKMapPoint = MKMapPointForCoordinate(b)
    mapPoints.append(c)
  }
  
  for p in 0 ..< mapPoints.count {
    if p == 0 {
      CGPathMoveToPoint(mpr, nil, CGFloat(mapPoints[p].x), CGFloat(mapPoints[p].y))
    } else {
      CGPathAddLineToPoint(mpr, nil, CGFloat(mapPoints[p].x), CGFloat(mapPoints[p].y))
    }
  }
  
  let target: MKMapPoint = MKMapPointForCoordinate(userLocation)
  let cgTarget: CGPoint = CGPoint(x: target.x, y: target.y)
  let isWithin = CGPathContainsPoint(mpr, nil, cgTarget, false)
  
  return isWithin
}

func getRegionInfoFromId(currentRegionId: String) -> AnyObject? { // return an array of coordinates to make a polygon based on the current region Id
  var myDict: NSDictionary?
  if let path = NSBundle.mainBundle().pathForResource("Locations", ofType: "plist") {
    myDict = NSDictionary(contentsOfFile: path)
  }
  if let dict = myDict {
    let places = dict.objectForKey("PlacesList") as! [NSDictionary]
    for place in places {
      let placeInfo = place.objectForKey("PlaceInfo") as! NSDictionary
      let name = placeInfo.objectForKey("name") as! NSString
      if name.isEqual(currentRegionId) {
        return placeInfo
      }
    }
    return nil
  }
  return nil
}


func notifyOnLeavingRegion(currentRegionId: String, window: UIWindow) { // sends the user a goodbye message
  if let placeInfo = getRegionInfoFromId(currentRegionId) {
    notifyUser(placeInfo.objectForKey("messageGoodbye") as! String, window: window)
  }
}

func makeAndCheckIfInsidePolygon(placeInfo: NSDictionary, coordinates:CLLocationCoordinate2D, userInside: Bool ) -> Bool { // draws an imaginary polygon and calls the isInsideFunc, returning true if a) user is inside region
  var placePolygon: [Locations] = []                                                                                       // AND inside polygon and b) if user is inside region and there is no polygon to be drawn
  if let vertices = placeInfo.objectForKey("verticesList") as? [NSDictionary] {
    for vertice in vertices {
      let coordpolygon = CLLocationCoordinate2DMake((vertice.objectForKey("latitude")) as! Double,vertice.objectForKey("longitude") as! Double)
      placePolygon.append(Locations(lat:coordpolygon.latitude,long:coordpolygon.longitude))
      if isInsideRegion(coordinates,coordinatesToMakePolygon: placePolygon) && !userInside {
        return true
      }
    }
    return false
  }
  else {
    return true
  }
}

func createGeofencingAreas(locationManager: CLLocationManager, vc: UIViewController) -> [Geotification] { // opens the Locations.plist file and starts monitoring each region described there
  var geotifications = [Geotification]()                                                                  // TO BE DONE: check dinamically the 20 nearest regions to the user and start monitoring them (iOS limitation)
  var myDict: NSDictionary?
  if let path = NSBundle.mainBundle().pathForResource("Locations", ofType: "plist") {
    myDict = NSDictionary(contentsOfFile: path)
  }
  if let dict = myDict {
    let places = dict.objectForKey("PlacesList") as! [NSDictionary]
    for place in places {
      let placeInfo = place.objectForKey("PlaceInfo") as! NSDictionary
      let center = placeInfo.objectForKey("center") as! NSDictionary
      let coord = CLLocationCoordinate2DMake((center.objectForKey("latitude")) as! Double,center.objectForKey("longitude") as! Double)
      let geo = Geotification(coordinate:coord, radius: placeInfo.objectForKey("radius") as! Double, identifier: placeInfo.objectForKey("name") as! String, note: placeInfo.objectForKey("name") as! String, eventType: EventType.OnEntry)
      geotifications.append(geo)
      startMonitoringGeotification(geo, locationManager: locationManager, vc: vc)
    }
  }
  return geotifications
  
}


func regionWithGeotification (geotification:Geotification) -> CLCircularRegion { // creates a circular region with a coordinate, radius and id
  let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
  region.notifyOnEntry = true
  region.notifyOnExit = true
  return region
}



func startMonitoringGeotification(geotification: Geotification, locationManager: CLLocationManager, vc: UIViewController) { // calls the location manager to monitor a specific region
/*  if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
    showSimpleAlertWithTitle("Error", message: "Geofencing is not supported on this device!", viewController: vc)
    return
  }
  if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
    showSimpleAlertWithTitle("Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.", viewController: vc)
  }*/
  let region = regionWithGeotification(geotification)
  locationManager.startMonitoringForRegion(region)
}






