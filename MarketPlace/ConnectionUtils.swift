//
//  ConnectionUtils.swift
//  Geotify
//
//  Created by Matheus Luz on 8/19/16.
//  Copyright © 2016 Ken Toh. All rights reserved.
//

import CoreLocation
import UIKit
import MapKit


func notifyUser(message: String, window: UIWindow) { // displays an alert to the user if app is open or a notification if using another app/device locked
  if UIApplication.sharedApplication().applicationState == .Active {
    if let viewController = window.rootViewController {
      showSimpleAlertWithTitle(nil, message: message, viewController: viewController)
    }
  }
  else {
    let notification = UILocalNotification()
    notification.alertBody = message
    notification.soundName = "Default";
    UIApplication.sharedApplication().presentLocalNotificationNow(notification)
  }
}

func showSimpleAlertWithTitle(title: String!, message: String, viewController: UIViewController) { // generic func to display an alert
  let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
  let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
  alert.addAction(action)
  viewController.presentViewController(alert, animated: true, completion: nil)
}

func zoomToUserLocationInMapView(mapView: MKMapView) {
    if let coordinate = mapView.userLocation.location?.coordinate {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 10000, 10000)
        mapView.setRegion(region, animated: true)
    }
}