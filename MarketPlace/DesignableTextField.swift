//
//  DesignableTextField.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

@IBDesignable
class DesignableUITextField: UITextField {
    
    func addShadow(){
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 0.2
        self.layer.cornerRadius = 10.0
        self.backgroundColor = UIColor.whiteColor()
        self.layer.masksToBounds = false
    }
    
    // Provides left padding for images
    override func leftViewRectForBounds(bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRectForBounds(bounds)
        textRect.origin.x += leftPadding
        self.addShadow()
        return textRect
    }
    
    // Provides left padding for images
    override func rightViewRectForBounds(bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRectForBounds(bounds)
        textRect.origin.x -= rightPadding
        self.addShadow()
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateRightView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 10
    @IBInspectable var rightPadding: CGFloat = 10
    
    @IBInspectable var color: UIColor = UIColor.lightGray4all() {
        didSet {
            updateLeftView()
            updateRightView()
        }
    }
    
    func updateLeftView() {
        
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.Always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.Always
            leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 25))
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSForegroundColorAttributeName: color])
    }
    
    func updateRightView() {
        
        if let image = rightImage {
            rightViewMode = UITextFieldViewMode.Always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            button.setImage(image, forState: UIControlState.Normal)
            button.addTarget(self, action: #selector(buttonClearPressed), forControlEvents: UIControlEvents.TouchUpInside)
            
            rightView = button
        } else {
            leftViewMode = UITextFieldViewMode.Always
            rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSForegroundColorAttributeName: color])
    }
    
    func buttonClearPressed(){
        self.text = ""
        self.resignFirstResponder()
        self.leftImage = UIImage(named: "iconSearchGreen")
        self.delegate?.textFieldDidEndEditing!(self)
    }
}
