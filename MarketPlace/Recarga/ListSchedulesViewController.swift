//
//  ListSchedulesViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 08/12/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class ListSchedulesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {

    @IBOutlet weak var tableView    : UITableView!
    @IBOutlet weak var labelMessage : UILabel!

    var indicator                           = UIActivityIndicatorView(activityIndicatorStyle: .White)
    var listItems = Array<[String : AnyObject]>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfiguration()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadFromApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    func initialConfiguration(){
        
        let barButton = UIBarButtonItem(customView: indicator)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.barTintColor = LayoutControl.primaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: LayoutControl.primaryFontWithSize(18)];
        
        tableView.layer.borderColor = UIColor.lightGrayColor().CGColor
        tableView.layer.borderWidth = 0.5
        tableView.delegate      = self
        tableView.dataSource    = self
        
    }

    func loadFromApi() {
        let client = APIClient()
        
        let sessionToken = Lib4all.sharedInstance().getAccountData()["sessionToken"] as! String
        indicator.startAnimating()
        
        client.listRecurrent(["sessionToken":sessionToken]) { (responseList, error) in
            self.indicator.stopAnimating()
            
            if responseList != nil {
                if responseList?.count > 0 {
                    self.listItems = responseList!
                    self.tableView.hidden    = false
                    self.labelMessage.hidden = true
                }else{
                    self.tableView.hidden    = true
                    self.labelMessage.text   = "Não foram encontradas recargas programadas."
                    self.labelMessage.hidden = false
                }
            }else if error != nil {
                var message = "Não foi possível carregar as recargas programadas.\nPor favor, tente novamente."
                if let errorCode = error!["code"] as? String{
                    message = "\(errorCode) : \(message)"
                    self.labelMessage.text   = "Não foram encontradas recargas programadas."
                }
                
                let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .Alert)
                
                let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                })
                
                alert.addAction(closeAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            self.tableView.reloadData()
        }
    }

    func showActionSheet(schedule : RMSchedule) {
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Escolha uma opção", preferredStyle: .ActionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Editar", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        let saveAction = UIAlertAction(title: "Excluir", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            let client = APIClient()
            
            
            let parameters : [String : AnyObject] = ["recurrentRechargeId": schedule.id,
                
                "customerData":["sessionToken": User.sharedUser().token]]
            
            
            client.deleteRecurrentRecharge(parameters, completionHandler: { (error) in
                
                if error != nil {
                    var message = "Não foi possível excluir a recarga programada. Por favor, tente novamente."
                    if let errorCode = error!["code"] as? String{
                        message = "\(errorCode) : \(message)"
                    }
                    
                    let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .Alert)
                    
                    let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                        alert.dismissViewControllerAnimated(true, completion: nil)
                    })
                    
                    alert.addAction(closeAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }else{
                    let message = "Recarga programada removida com sucesso."
                    
                    let alert = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
                    
                    let closeAction = UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                        alert.dismissViewControllerAnimated(true, completion: nil)
                    })
                    
                    alert.addAction(closeAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    self.loadFromApi()
                }
                
            })

        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: - Actions
    @IBAction func callScheduleDetails (sender : AnyObject) {
        self.performSegueWithIdentifier("segueRecharge", sender: self)
    }
    
    @IBAction func closeListSchedules (sender : AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - TableView delegates/source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellSchedule")!
        let labelDate           = cell.viewWithTag(1) as! UILabel
        let labelOperatorAmount = cell.viewWithTag(2) as! UILabel

        let recharge = RMSchedule(value: listItems[indexPath.row])
        
        labelDate.text = recharge.getNextDateFormatted()
        labelOperatorAmount.text = "\(recharge.productData.operatorData.name) \(recharge.productData.getAmountFormated())"
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let recharge = RMSchedule(value: listItems[indexPath.row])

        showActionSheet(recharge)
    }
}
