//
//  RechargeTableViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 28/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RechargeTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, LoginDelegate {

    @IBOutlet weak var textDdd: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    @IBOutlet weak var textOperators: UITextField!
    @IBOutlet weak var textAmount: UITextField!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    var component               : ComponentViewController!
    var pickerOperatorSource    = Array<RMOperator>()
    var pickerProductsSource    = Array<RMProduct>()
    var pickerOperator          : UIPickerView!
    var pickerProducts          : UIPickerView!
    var selectedOperator        : RMOperator?
    var selectedProduct         : RMProduct?
    var indicator               = UIActivityIndicatorView(activityIndicatorStyle: .White)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfiguration()
        
        configurePaymentComponent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialConfiguration() {
        
        indicator.frame     = CGRectMake(0, 0, 40, 40)
        
        let barButton = UIBarButtonItem(customView: indicator)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.barTintColor = LayoutControl.primaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: LayoutControl.primaryFontWithSize(18)];
        
        let toolBarOperator = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectItemOnPicker(_:)))
        doneButton.tag = 1
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didCancelPicker(_:)))
        
        toolBarOperator.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBarOperator.userInteractionEnabled = true
        
        pickerOperator = UIPickerView(frame: CGRectMake(0, (self.tableView.frame.size.height - 150.0), self.tableView.frame.size.width, 150))
        pickerOperator.delegate = self
        pickerOperator.dataSource = self
        pickerOperator.showsSelectionIndicator = true
        textOperators.inputView = pickerOperator
        textOperators.inputAccessoryView = toolBarOperator
        
        //ToolBAR VALUES
        let toolBarValues = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        
        let doneButton2 = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectItemOnPicker(_:)))
        doneButton2.tag = 2
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton2 = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didCancelPicker(_:)))
        
        toolBarValues.setItems([cancelButton2, spaceButton2, doneButton2], animated: false)
        toolBarValues.userInteractionEnabled = true
        
        pickerProducts = UIPickerView(frame: CGRectMake(0, (self.tableView.frame.size.height - 150.0), self.tableView.frame.size.width, 150))
        pickerProducts.delegate = self
        pickerProducts.dataSource = self
        pickerProducts.showsSelectionIndicator = true
        textAmount.inputView = pickerProducts
        textAmount.inputAccessoryView = toolBarValues
        
        let phoneNumber = Lib4all.sharedInstance().getAccountData()["phoneNumber"] as! String
        let ddd = phoneNumber.substringFromIndex(phoneNumber.startIndex.advancedBy(2)).substringToIndex(phoneNumber.startIndex.advancedBy(2))
        
        textDdd.text = ddd
        textPhone.text = phoneNumber.substringFromIndex(phoneNumber.startIndex.advancedBy(4))
        
        
        let client = APIClient()
        indicator.startAnimating()
        client.listProductsByAreaCode(ddd) { (arrayOperators, error) in
            
            if arrayOperators != nil {
                self.pickerOperatorSource.removeAll()
                for item in arrayOperators! {
                    let oper = RMOperator(value: item)
                    self.pickerOperatorSource.append(oper)
                }
                self.textOperators.enabled = true
                self.pickerOperator.reloadAllComponents()
            }
            
            self.indicator.stopAnimating()
        }
    }
    
    func didSelectItemOnPicker(sender : AnyObject){
        if sender.tag == 1 {
            if textOperators.isFirstResponder() {
                textOperators.resignFirstResponder()
                if selectedOperator != nil {
                    textOperators.text = selectedOperator?.name
                    pickerProductsSource = selectedOperator!.products
                    textAmount.enabled = true
                    pickerProducts.selectRow(0, inComponent: 0, animated: false)
                    pickerProducts.reloadAllComponents()
                    
                    //check products
                    if selectedOperator?.products.count > 0 {
                        textAmount.text = selectedOperator?.products[0].name
                        selectedProduct  = selectedOperator?.products[0]

                    }

                }
            }
        }else if sender.tag == 2 {
            if textAmount.isFirstResponder() {
                textAmount.resignFirstResponder()
                if selectedOperator != nil {
                    textAmount.text = selectedProduct?.name
                }
            }
        }
    }
    
    func didCancelPicker(sender : AnyObject){
        if textOperators.isFirstResponder() {
            textOperators.resignFirstResponder()
        }
    }
    
    func configurePaymentComponent() {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.component = ComponentViewController()
            
            self.component.buttonTitleWhenNotLogged = "ENTRAR"
            self.component.buttonTitleWhenLogged    = "PAGAR"
            self.component.delegate = self
            self.component.view.frame               = self.viewContainer.bounds
            self.viewContainer.addSubview(self.component.view)
            self.addChildViewController(self.component)
            
            self.component.didMoveToParentViewController(self)
        }
    }

    @IBAction func closeController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    // MARK: - Picker view data source
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerOperator {
            return pickerOperatorSource.count
        }else{
            return pickerProductsSource.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerOperator {
            selectedOperator = pickerOperatorSource[row]
            if pickerProductsSource.count > 0 {
                pickerProducts.selectRow(0, inComponent: 0, animated: true)
                pickerProducts.reloadComponent(0)
            }
        }else{
            selectedProduct  = pickerProductsSource[row]
            if selectedProduct != nil {
                labelMessage.text = "Essa regarga expira em \(selectedProduct!.expiry) dias"
            }else{
                labelMessage.text = ""
            }
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerOperator {
            return pickerOperatorSource[row].name
        }else{
            return pickerProductsSource[row].name
        }
    }
    
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        let client = APIClient()
        let loading = LoadingViewController()

        
        let parameters : [String : AnyObject] = ["productId": selectedProduct!.productId,
                                                 "areaCode": String(textDdd.text) != nil ? String(textDdd.text!) : "",
                                                 "areaCodeConfirmation": String(textDdd.text) != nil ? String(textDdd.text!) : "",
                                                 "phoneNumber": textPhone.text != nil ? textPhone.text! : "",
                                                 "phoneNumberConfirmation": textPhone.text != nil ? textPhone.text! : "",
                                                  "customerData":["sessionToken": sessionToken,"cardId": cardId]
        ]

        viewContainer.userInteractionEnabled = false
        
        indicator.startAnimating()
        loading.startLoading(self, title: "Aguarde...")
        client.purchaseProduct(parameters) { (response, error) in
            
            loading.finishLoading({ 
                self.indicator.stopAnimating()
                self.viewContainer.userInteractionEnabled = true
                
                if response != nil {
                    
                    let detailsController = UIStoryboard(name: "RecargaMobile", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! RMReceiptViewController
                    detailsController.purchase = RMPurchase(value: response!)
                    
                    self.navigationController?.pushViewController(detailsController, animated: true)
                    
                }else{
                    var message = "Não foi possível realizar a recarga neste momento. Nenhum débito foi realizado no seu meio de pagamento."
                    
                    if let serverMessage = error!["message"] as? String{
                        message = serverMessage
                    }
                    
                    if let errorCode = error!["code"] as? String{
                        message = "\(errorCode) : \(message)"
                    }
                    
                    let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .Alert)
                    
                    let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                        alert.dismissViewControllerAnimated(true, completion: nil)
                    })
                    
                    alert.addAction(closeAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            })
        }
        
    }

}
