//
//  APIClient.swift
//  RecargaMobile
//
//  Created by MacMini on 23/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit
import Alamofire

class APIClient: NSObject {
    
    var alamoFireManager = Alamofire.Manager.sharedInstance
    
    override init() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 40 // seconds
        alamoFireManager = Alamofire.Manager(configuration: configuration)
        
        if Lib4all.environment() == .Production {
            APIClientConstants.BaseURL = APIClientConstants.EnvironmentURL.Production
        }else{
            APIClientConstants.BaseURL = APIClientConstants.EnvironmentURL.Homologation
        }
        
    }

    //MARK: - List Operators
    func listOperators(state: String, completionHandler:([String: AnyObject]?, [String: AnyObject]?) -> Void) -> Void{
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.ListOperators
        
        alamoFireManager.request(.POST, urlString,parameters: ["state":state], encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(response.result.value as? [String : AnyObject], nil)
                    break
                case .Failure:
                    completionHandler(nil, self.handleError(response))
                }
        }
    }
    
    //MARK: - List Products
    func listProducts(operatorId: String, completionHandler:([String: AnyObject]?, [String: AnyObject]?) -> Void) -> Void{
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.ListProducts
        
        alamoFireManager.request(.POST, urlString,parameters: ["operatorId":operatorId], encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(response.result.value as? [String : AnyObject], nil)
                    break
                case .Failure:
                    completionHandler(nil, self.handleError(response))
                }
        }
    }
    
    //MARK: -  List Products By Area Code
    func listProductsByAreaCode(areaCode: String, completionHandler:(Array<[String: AnyObject]>?, [String: AnyObject]?) -> Void) -> Void{
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.ListProductsByAreaCode
        
        print(urlString)
        
        alamoFireManager.request(.POST, urlString,parameters: ["areaCode":areaCode], encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(response.result.value!["operators"] as? Array<[String: AnyObject]>, nil)
                    break
                case .Failure:
                    completionHandler(nil, self.handleError(response))
                }
        }
    }

    //MARK: -  List Products By Area Code
    func purchaseProduct(parameters: [String : AnyObject], completionHandler:([String: AnyObject]?, [String: AnyObject]?) -> Void) -> Void{
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.PurchaseProduct
        
        alamoFireManager.request(.POST, urlString,parameters: parameters, encoding: .JSON)
            .validate()
            .responseJSON { response in
                if let requestBody = response.request?.HTTPBody {
                    do {
                        let jsonArray = try NSJSONSerialization.JSONObjectWithData(requestBody, options: [])
                        print("Array: \(jsonArray)")
                    }
                    catch {
                        print("Error: \(error)")
                    }
                }
                switch response.result {
                case .Success:
                    completionHandler(response.result.value as? [String : AnyObject], nil)
                    break
                case .Failure:
                    completionHandler(nil, self.handleError(response))
                }
        }
    }
    
    //MARK: -  List Purchases
    func listPurchases(customerData: [String : AnyObject], completionHandler:(Array<[String: AnyObject]>?, [String: AnyObject]?) -> Void) -> Void{
        
        let parameters : [String : AnyObject] = ["itemIndex" : 0,
                                                 "itemCount" : 100000,
                                                 "customerData" : customerData]
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.ListPurchases
        
        alamoFireManager.request(.POST, urlString,parameters: parameters, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(response.result.value!["purchases"] as? Array<[String: AnyObject]>, nil)
                    break
                case .Failure:
                    completionHandler(nil, self.handleError(response))
                }
        }
    }

    //MARK: -  List Purchases
    func listRecurrent(customerData: [String : AnyObject], completionHandler:(responseList : Array<[String: AnyObject]>?, error: [String: AnyObject]?) -> Void) -> Void{
        
        let parameters : [String : AnyObject] = ["customerData" : customerData]
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.ListRecurrentRecharges
        
        alamoFireManager.request(.POST, urlString,parameters: parameters, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(responseList: response.result.value!["recurrentRechargeList"] as? Array<[String: AnyObject]>, error: nil)
                    break
                case .Failure:
                    completionHandler(responseList: nil, error: self.handleError(response))
                }
        }
    }
    
    //MARK: -  List Purchases
    func addRecurrentRecharge(parameters: [String : AnyObject], completionHandler:([String: AnyObject]?) -> Void) -> Void{
        
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.AddRecurrentRecharge
        
        alamoFireManager.request(.POST, urlString,parameters: parameters, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(nil)
                    break
                case .Failure:
                    completionHandler(self.handleError(response))
                }
        }
    }

    //MARK: -  List Purchases
    func deleteRecurrentRecharge(parameters: [String : AnyObject], completionHandler:([String: AnyObject]?) -> Void) -> Void{
                
        let urlString = APIClientConstants.BaseURL + APIClientConstants.Methods.DeleteRecurrentRecharge
        
        alamoFireManager.request(.POST, urlString,parameters: parameters, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    completionHandler(nil)
                    break
                case .Failure:
                    completionHandler(self.handleError(response))
                }
        }
    }

    
    //MARK: - Util
    private func handleError(response : Response<AnyObject, NSError>) -> [String : AnyObject]{
        var errorCode = "-1"
        var errorMessage = "Ocorreu um erro. Por favor, tente novamente."
        var responseObject = Dictionary<String, AnyObject>()
        
        if let data = response.data {
            let responseData = String(data: data, encoding: NSUTF8StringEncoding)
            let responseJSON = JSONParseDict(responseData!)
            if let message: String = responseJSON["message"] as? String {
                if !message.isEmpty {
                    errorMessage = message
                }
            }
            
            if let code: String = responseJSON["code"] as? String {
                if !code.isEmpty {
                    errorCode = code
                }
            }
        }
        
        responseObject["message"]   = errorMessage
        responseObject["code"]      = errorCode
        
        return responseObject
    }
    
    func JSONParseDict(jsonString:String) -> Dictionary<String, AnyObject> {
        
        if let data: NSData = jsonString.dataUsingEncoding(
            NSUTF8StringEncoding){
            
            do{
                if let jsonObj = try NSJSONSerialization.JSONObjectWithData(
                    data,
                    options: NSJSONReadingOptions(rawValue: 0)) as? Dictionary<String, AnyObject>{
                    return jsonObj
                }
            }catch{
                print("Error JSONParseDict")
            }
        }
        return [String: AnyObject]()
    }
}
