//
//  LibRecargaMobile.swift
//  RecargaMobile
//
//  Created by MacMini on 24/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class LibRecargaMobile: NSObject {

    class func callMenuRecargaMobile(referenceViewController : UIViewController) {
        
        let menuController = UIStoryboard(name: "RecargaMobile", bundle: nil).instantiateViewControllerWithIdentifier("MenuVC")
        
        menuController.providesPresentationContextTransitionStyle = true;
        menuController.definesPresentationContext = true;
        menuController.modalPresentationStyle = .OverCurrentContext
        menuController.modalTransitionStyle = .CrossDissolve
        
        referenceViewController.presentViewController(menuController, animated: true, completion: nil)
        
    }
    
}
