//
//  RMReceiptViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 05/12/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RMReceiptViewController: UIViewController {

    var purchase : RMPurchase!
    @IBOutlet weak var labelDetails : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = LayoutControl.primaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: LayoutControl.primaryFontWithSize(18)];
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Fechar", style: .Plain, target: self, action: #selector(self.closeController(_:)))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.whiteColor()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        labelDetails.setContentOffset(CGPointZero, animated: false)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        labelDetails.text = "4all Tecnologia LTDA\n" +
            "CNPJ 23.483.494/0001-08\n" +
            "Porto Alegre / RS\n\n" +
            "Comprovante de Recarga\n" +
            "Terminal: \(purchase.terminal)\n" +
            "Estabelecimento: \(purchase.establishment)\n" +
            "Código identificador: \(purchase.codOnline)\n" +
            "\(purchase.getDateFormatted(true))\n\n" +
            "Operadora: \(purchase.productData.operatorData.name)\n" +
            "Telefone: \(purchase.getFormattedNumber())\n" +
            "Valor: \(purchase.getAmountFormated())\n" +
            "Validade: \(purchase.productData.expiry) dias\n" +
            "NSU: \(purchase.nsu)\n\n" +
            "\(purchase.message)"
    }

    //MARK: - Actions
    @IBAction func closeController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
