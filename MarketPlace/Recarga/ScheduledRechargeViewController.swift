//
//  ScheduledRechargeViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 08/12/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class ScheduledRechargeViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, LoginDelegate {

    @IBOutlet weak var textDdd: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    @IBOutlet weak var textOperators: UITextField!
    @IBOutlet weak var textAmount: UITextField!
    @IBOutlet weak var textDate: UITextField!
    @IBOutlet weak var switchMonthly: UISwitch!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var viewContainer: UIView!

    var component               : ComponentViewController!
    var pickerOperatorSource    = Array<RMOperator>()
    var pickerProductsSource    = Array<RMProduct>()
    var pickerOperator          : UIPickerView!
    var pickerProducts          : UIPickerView!
    var selectedOperator        : RMOperator?
    var selectedProduct         : RMProduct?
    var indicator               = UIActivityIndicatorView(activityIndicatorStyle: .White)
    var selectedDate            = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfiguration()
        
        configurePaymentComponent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialConfiguration() {
        
        indicator.frame     = CGRectMake(0, 0, 40, 40)
        
        //ToolBAR OPERATOR
        let barButton = UIBarButtonItem(customView: indicator)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.barTintColor = LayoutControl.primaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: LayoutControl.primaryFontWithSize(18)];
        
        let toolBarOperator = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectItemOnPicker(_:)))
        doneButton.tag = 1
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didCancelPicker(_:)))
        
        toolBarOperator.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBarOperator.userInteractionEnabled = true
        
        pickerOperator = UIPickerView(frame: CGRectMake(0, (self.tableView.frame.size.height - 150.0), self.tableView.frame.size.width, 150))
        pickerOperator.delegate = self
        pickerOperator.dataSource = self
        pickerOperator.showsSelectionIndicator = true
        textOperators.inputView = pickerOperator
        textOperators.inputAccessoryView = toolBarOperator
        
        //ToolBAR VALUES
        let toolBarValues = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        
        let doneButton2 = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectItemOnPicker(_:)))
        doneButton2.tag = 2
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton2 = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didCancelPicker(_:)))
        
        toolBarValues.setItems([cancelButton2, spaceButton2, doneButton2], animated: false)
        toolBarValues.userInteractionEnabled = true
        
        pickerProducts = UIPickerView(frame: CGRectMake(0, (self.tableView.frame.size.height - 150.0), self.tableView.frame.size.width, 150))
        pickerProducts.delegate = self
        pickerProducts.dataSource = self
        pickerProducts.showsSelectionIndicator = true
        textAmount.inputView = pickerProducts
        textAmount.inputAccessoryView = toolBarValues
        
        //Toolbar date
        let toolBarDate = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        let doneDate = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectDate(_:)))
        doneButton.tag = 1
        let spaceDate = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBarDate.setItems([spaceDate, doneDate], animated: false)
        let datePicker = UIDatePicker()
        datePicker.date = NSDate()
        datePicker.datePickerMode = .Date
        textDate.inputView = datePicker
        textDate.inputAccessoryView = toolBarDate
        
        let phoneNumber = Lib4all.sharedInstance().getAccountData()["phoneNumber"] as! String
        let ddd = phoneNumber.substringFromIndex(phoneNumber.startIndex.advancedBy(2)).substringToIndex(phoneNumber.startIndex.advancedBy(2))
        
        textDdd.text = ddd
        textPhone.text = phoneNumber.substringFromIndex(phoneNumber.startIndex.advancedBy(4))
        
        
        let client = APIClient()
        indicator.startAnimating()
        client.listProductsByAreaCode(ddd) { (arrayOperators, error) in
            
            if arrayOperators != nil {
                self.pickerOperatorSource.removeAll()
                for item in arrayOperators! {
                    let oper = RMOperator(value: item)
                    self.pickerOperatorSource.append(oper)
                }
                self.textOperators.enabled = true
                self.pickerOperator.reloadAllComponents()
            }
            
            self.indicator.stopAnimating()
        }
    }
    
    func didSelectDate(sender : AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        textDate.resignFirstResponder()
        let picker = textDate.inputView as! UIDatePicker
        textDate.text = dateFormatter.stringFromDate(picker.date)

        dateFormatter.dateFormat = "yyyy-MM-dd"
        selectedDate = dateFormatter.stringFromDate(picker.date)

    }
    
    
    func didSelectItemOnPicker(sender : AnyObject){
        if sender.tag == 1 {
            if textOperators.isFirstResponder() {
                textOperators.resignFirstResponder()
                if selectedOperator != nil {
                    textOperators.text = selectedOperator?.name
                    pickerProductsSource = selectedOperator!.products
                    textAmount.enabled = true
                    pickerProducts.reloadAllComponents()
                    pickerProducts.selectRow(0, inComponent: 0, animated: false)
                }
            }
        }else if sender.tag == 2 {
            if textAmount.isFirstResponder() {
                textAmount.resignFirstResponder()
                if selectedOperator != nil {
                    textAmount.text = selectedProduct?.name
                }
            }
        }
    }
    
    func didCancelPicker(sender : AnyObject){
        if textOperators.isFirstResponder() {
            textOperators.resignFirstResponder()
        }
    }
    
    func configurePaymentComponent() {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.component = ComponentViewController()
            
            self.component.buttonTitleWhenNotLogged = "Programar Recarga"
            self.component.buttonTitleWhenLogged    = "Programar Recarga"
            self.component.delegate = self
            self.component.view.frame               = self.viewContainer.bounds
            self.viewContainer.addSubview(self.component.view)
            self.addChildViewController(self.component)
            
            self.component.didMoveToParentViewController(self)
        }
    }
    
    @IBAction func closeController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }
    
    // MARK: - Picker view data source
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerOperator {
            return pickerOperatorSource.count
        }else{
            return pickerProductsSource.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerOperator {
            selectedOperator = pickerOperatorSource[row]
        }else{
            selectedProduct  = pickerProductsSource[row]
            if selectedProduct != nil {
                labelMessage.text = "Essa regarga expira em \(selectedProduct!.expiry) dias"
            }else{
                labelMessage.text = ""
            }
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerOperator {
            return pickerOperatorSource[row].name
        }else{
            return pickerProductsSource[row].name
        }
    }
    
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        let client = APIClient()
        
        let parameters : [String : AnyObject] = ["productId": selectedProduct!.productId,
                                                 "areaCode": textDdd.text != nil ? textDdd.text! : "",
                                                 "areaCodeConfirmation": textDdd.text != nil ? textDdd.text! : "",

                                                 "phoneNumber": textPhone.text != nil ? textPhone.text! : "",
                                                 "phoneNumberConfirmation": textPhone.text != nil ? textPhone.text! : "",

                                                 "totalMonths": switchMonthly.on ? 0 : 1,
                                                 "nextRecharge": selectedDate,
                                                 "customerData":["sessionToken": sessionToken,"cardId": cardId]
        ]
        
        
        viewContainer.userInteractionEnabled = false
        
        indicator.startAnimating()
        client.addRecurrentRecharge(parameters) { (error) in
            
            if error != nil {
                var message = "Não foi possível programar a recarga neste momento. Nenhum débito foi realizado no seu meio de pagamento."
                if let errorCode = error!["code"] as? String{
                    message = "\(errorCode) : \(message)"
                }
                
                let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .Alert)
                
                let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                })
                
                alert.addAction(closeAction)
                self.presentViewController(alert, animated: true, completion: nil)
                
            }else{
                
                let alert = UIAlertController(title: "Atenção", message: "Recarga programada com sucesso.", preferredStyle: .Alert)
                let this = self
                
                let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                    this.dismissViewControllerAnimated(true, completion: {
                        
                    })
                })
                
                alert.addAction(closeAction)
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
         
            self.viewContainer.userInteractionEnabled = true
            self.indicator.stopAnimating()

        }
    }
    
}
