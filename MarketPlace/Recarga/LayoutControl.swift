//
//  LayoutControl.swift
//  RecargaMobile
//
//  Created by MacMini on 19/12/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class LayoutControl: NSObject {

    static let primaryColor = UIColor(red: 95.0/255.0, green:183/255.0, blue:63.0/255.0, alpha:1.0)
    
    
    static func primaryFontWithSize(size: CGFloat) -> UIFont{
        return UIFont(name: "Dosis-Regular", size: size)!
    }
}
