//
//  RMPurchase.swift
//  RecargaMobile
//
//  Created by MacMini on 23/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RMPurchase: NSObject {

    var purchaseId  = ""
    var areaCode    = ""
    var phoneNumber = ""
    var status      = 0
    var dateTime    = ""
    var merchant    = ""
    var codOnline   = ""
    var terminal    = ""
    var establishment    = ""
    var nsu         = ""
    var message     = ""
    var price       = 0
    var operatorName = ""
    var productData = RMProduct()
    
    override init() {
        super.init()
    }
    
    init(value : Dictionary<String, AnyObject>) {
        super.init()
        
        purchaseId      = value["productId"] as? String ?? ""
        areaCode        = value["areaCode"] as? String ??  ""
        phoneNumber     = value["phoneNumber"] as? String ??  ""
        status          = value["status"] as? Int ?? 0
        dateTime        = value["dateTime"] as? String ??  ""
        merchant        = value["merchant"] as? String ??  ""
        codOnline       = value["codOnline"] as? String ??  ""
        nsu             = value["NSU"] as? String ??  ""
        terminal        = value["terminal"] as? String ??  ""
        message         = value["message"] as? String ??  ""
        establishment   = value["estabelecimento"] as? String ?? ""
        price           = value["price"] as? Int ?? 0
        operatorName    = value["operator"] as? String ?? ""

        if let productDict = value["productData"] as? Dictionary<String, AnyObject> {
            productData = RMProduct(value: productDict)
        }
    }
    
    func getFormattedNumber() -> String {
        var numberFormatted = phoneNumber
        
        if phoneNumber.characters.count > 4 {
            numberFormatted.insert(" ", atIndex: numberFormatted.startIndex.advancedBy(4))
        }
        return "(\(areaCode)) \(numberFormatted)"
    }
    
    func getDateFormatted(withTime : Bool = false) -> String {

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let nsDate = dateFormatter.dateFromString(dateTime)
        
        dateFormatter.dateFormat = "dd/MM/yy"
        
        if withTime {
            dateFormatter.dateFormat = "dd/MM/yy HH:mm:ss"
        }
        
        if nsDate != nil {
            return dateFormatter.stringFromDate(nsDate!)
        }else {
            return "---"
        }
    }

    func getAmountFormated() -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt-BR")
        return formatter.stringFromNumber((self.productData.price / 100))!
    }
    
    func getNSDate() -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let nsDate = dateFormatter.dateFromString(dateTime)
        
        return nsDate
    }
}
