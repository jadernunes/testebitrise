//
//  RMProduct.swift
//  RecargaMobile
//
//  Created by MacMini on 24/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RMProduct: NSObject {
    
    var productId   = ""
    var name        = ""
    var price       = 0.0
    var expiry      = 0
    var operatorData = RMOperator()
    
    override init() {
        super.init()
    }
    
    init(value : Dictionary<String, AnyObject>) {
        super.init()
    
        productId   = value["productId"] as? String ?? ""
        name        = value["name"] as? String ??  ""
        price       = value["price"] as? Double ?? 0.0
        expiry      = value["expiry"] as? Int ?? 0
        
        if let operatorDict = value["operatorData"] as? Dictionary<String, AnyObject> {
            operatorData = RMOperator(value: operatorDict)
        }
    }
    
    func getAmountFormated() -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt-BR")
        return formatter.stringFromNumber((self.price / 100.0))!
    }
    
}
