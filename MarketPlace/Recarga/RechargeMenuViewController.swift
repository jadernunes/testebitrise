//
//  RechargeMenuViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 24/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RechargeMenuViewController: UIViewController {

    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var viewContainerMenu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfiguration()
    }

    func initialConfiguration(){
        self.modalPresentationStyle = .OverCurrentContext
        self.modalTransitionStyle   = .CrossDissolve
        
        buttonClose.layoutIfNeeded()
        buttonClose.layer.cornerRadius  = buttonClose.frame.size.width / 2
        buttonClose.clipsToBounds       = true
        buttonClose.backgroundColor     = LayoutControl.primaryColor;
        self.view.bringSubviewToFront(buttonClose)
        viewContainerMenu.layer.cornerRadius = 6
        viewContainerMenu.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeMenu(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        if Lib4all.sharedInstance().hasUserLogged() == false{
            Lib4all.sharedInstance().callLogin(self, completion: { (phone, email, sessionToken) in
            })
            
            return false
        }
        
        return true
    }
    
}
