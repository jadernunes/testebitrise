//
//  RMSchedule.swift
//  RecargaMobile
//
//  Created by MacMini on 08/12/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RMSchedule: NSObject {

    var scheduledRechargeId  = ""
    var areaCode             = ""
    var phoneNumber          = ""
    var monthsRemaining      = 0
    var nextRecharge         = ""
    var firstRecharge        = ""
    var id                   = ""
    var totalMonths          = 0
    var productId            = ""
    var productData          = RMProduct()

    init(value : Dictionary<String, AnyObject>) {
        super.init()
        
        scheduledRechargeId  = value["scheduledRechargeId"] as? String ?? ""
        areaCode    = value["areaCode"] as? String ??  ""
        phoneNumber = value["phoneNumber"] as? String ??  ""
        monthsRemaining      = value["monthsRemaining"] as? Int ?? 0
        nextRecharge    = value["nextRecharge"] as? String ??  ""
        firstRecharge    = value["firstRecharge"] as? String ??  ""
        id   = value["id"] as? String ??  ""
        totalMonths         = value["totalMonths"] as? Int ??  0
        productId    = value["productId"] as? String ??  ""
        
        if let productDict = value["productData"] as? Dictionary<String, AnyObject> {
            productData = RMProduct(value: productDict)
        }
    }

    func getNextDateFormatted(withTime : Bool = false) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let nsDate = dateFormatter.dateFromString(nextRecharge)
        
        dateFormatter.dateFormat = "dd/MM/yy"
        
        if withTime {
            dateFormatter.dateFormat = "dd/MM/yy HH:mm:ss"
        }
        
        if nsDate != nil {
            return dateFormatter.stringFromDate(nsDate!)
        }else {
            return "---"
        }
    }

    
}
