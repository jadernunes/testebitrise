//
//  HistoryRechargesViewController.swift
//  RecargaMobile
//
//  Created by MacMini on 29/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class HistoryRechargesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var textFirstDate        : UITextField!
    @IBOutlet weak var textEndDate          : UITextField!
    @IBOutlet weak var labelNoData          : UILabel!
    @IBOutlet weak var tableView            : UITableView!
    let refreshControl                      = UIRefreshControl()
    var indicator                           = UIActivityIndicatorView(activityIndicatorStyle: .White)
    var listAllPurchases                    = Array<RMPurchase>()
    var listFilteredPurchases               = Array<RMPurchase>()
    var selectedPurchase                    = RMPurchase()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate      = self
        tableView.dataSource    = self
        
        
        //Toolbar firstdate
        let toolBarFirst = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectDate(_:)))
        doneButton.tag = 1
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBarFirst.setItems([spaceButton, doneButton], animated: false)
    
        //Tollbar enddate
        let toolBarLast = UIToolbar(frame: CGRectMake(0, 0, self.tableView.frame.size.width, 44))
        let doneButtonLast = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.didSelectDate(_:)))
        doneButtonLast.tag = 2
        let spaceButtonLast = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBarLast.setItems([spaceButtonLast, doneButtonLast], animated: false)
        
        let datePickerFirst = UIDatePicker()
        datePickerFirst.date = NSDate()
        datePickerFirst.datePickerMode = .Date

        let datePickerLast = UIDatePicker()
        datePickerLast.date = NSDate()
        datePickerLast.datePickerMode = .Date
        
        textFirstDate.inputView = datePickerFirst
        textFirstDate.inputAccessoryView = toolBarFirst
        
        textEndDate.inputView = datePickerLast
        textEndDate.inputAccessoryView = toolBarLast
        
        let barButton = UIBarButtonItem(customView: indicator)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.barTintColor = LayoutControl.primaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: LayoutControl.primaryFontWithSize(18)];

        refreshControl.addTarget(self, action: #selector(self.loadHistoryFromServer), forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
        
        loadHistoryFromServer()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadHistoryFromServer(){
        
        let client = APIClient()
        
        indicator.startAnimating()
        client.listPurchases(["sessionToken":User.sharedUser().token]) { (response, error) in
            if response != nil {
                self.listAllPurchases.removeAll()
                self.listFilteredPurchases.removeAll()
                
                for objectDict in response! {
                    self.listAllPurchases.append(RMPurchase(value: objectDict))
                }
                
                self.listFilteredPurchases.appendContentsOf(self.listAllPurchases)
                self.tableView.reloadData()
                
                if self.listFilteredPurchases.count > 0 {
                    self.tableView.hidden    = false
                    self.labelNoData.hidden = true
                }else{
                    self.tableView.hidden    = true
                    self.labelNoData.text   = "Não foram encontradas recargas."
                    self.labelNoData.hidden = false
                }

            }else if error != nil {
                let alert = UIAlertController(title: "Atenção", message: error!["message"]!.stringValue, preferredStyle: .Alert)
                
                if let errorCode = error!["code"] as? String{
                    let message = "\(errorCode) : Não foram encontradas recargas."
                    self.labelNoData.text   = message
                }

                
                let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                })
                
                alert.addAction(closeAction)
                if error!["code"]?.intValue != -1 {
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            
            self.refreshControl.endRefreshing()
            self.indicator.stopAnimating()
            
            
        }
    }
    
    func filterPurchases(){
        var dateFirst   = NSDate()
        var dateEnd     = NSDate()
        
        let picker  = textFirstDate.inputView as! UIDatePicker
        dateFirst   = picker.date
        
        let pickerEnd = textEndDate.inputView as! UIDatePicker
        dateEnd   = pickerEnd.date
        
        if dateFirst.isLessThanDate(dateEnd) {
        
            listFilteredPurchases.removeAll()
            
            for purchase in listAllPurchases {
                
                if purchase.getNSDate() != nil {
                    if (purchase.getNSDate()!.isGreaterThanDate(dateFirst) || purchase.getNSDate()!.equalToDate(dateFirst)) &&
                        (purchase.getNSDate()!.isLessThanDate(dateEnd) || purchase.getNSDate()!.equalToDate(dateEnd)) {
                            listFilteredPurchases.append(purchase)
                    }
                    
                }
                
            }
            
            tableView.reloadData()
        }else{
            let alert = UIAlertController(title: "Atenção", message: "Data final não pode ser inferior a inicial.", preferredStyle: .Alert)
            
            let closeAction = UIAlertAction(title: "Fechar", style: .Cancel, handler: { (action) in
                alert.dismissViewControllerAnimated(true, completion: nil)
            })
            
            alert.addAction(closeAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func didSelectDate(sender : AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        //        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle

        
        textFirstDate.resignFirstResponder()
        textEndDate.resignFirstResponder()
        
        if sender.tag == 1 {
            let picker = textFirstDate.inputView as! UIDatePicker
            textFirstDate.text = dateFormatter.stringFromDate(picker.date)
        } else {
            let picker = textEndDate.inputView as! UIDatePicker
            textEndDate.text = dateFormatter.stringFromDate(picker.date)
        }
        
    }
    
    //MARK: - Actions
    @IBAction func closeController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func applyFilter(sender: AnyObject) {
        if textFirstDate.text != nil && textEndDate.text != nil {
            if !textFirstDate.text!.isEmpty && !textEndDate.text!.isEmpty {
                filterPurchases()
            }
        }
    }
    
    @IBAction func cleanFilter(sender: AnyObject) {
        listFilteredPurchases.removeAll()
        listFilteredPurchases.appendContentsOf(listAllPurchases)
        textFirstDate.text  = ""
        textEndDate.text    = ""
        
        tableView.reloadData()
    }
    
    //MARK: - Table View Data Source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFilteredPurchases.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellHistory") as! HistoryTableViewCell
        
        let purchase = listFilteredPurchases[indexPath.row]
        
        cell.labelPhone.text = purchase.getFormattedNumber()
        cell.labelDate.text  = purchase.getDateFormatted()
        cell.labelAmount.text = purchase.getAmountFormated()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedPurchase = listFilteredPurchases[indexPath.row]
        
        let detailsController = UIStoryboard(name: "RecargaMobile", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! RMReceiptViewController
        detailsController.purchase = selectedPurchase
        
        let navigationController = UINavigationController(rootViewController: detailsController)
        
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
}
