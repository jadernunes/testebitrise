//
//  FAPClientConstants.swift
//  4all Park
//
//  Created by Cristiano Matte on 22/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class APIClientConstants {
    
    static var BaseURL       = "http://conta.test.4all.com:3010"

    struct EnvironmentURL {
        static let Production   = "https://recargacelular.api.4all.com"
        static let Homologation = "http://recarga-celular.homolog-interna.4all.com:3010"
    }
    
    struct Methods {
        static let ListOperators            = "/listOperators"
        static let ListProducts             = "/listProducts"
        static let ListProductsByAreaCode   = "/listProductsByAreaCode"
        static let PurchaseProduct          = "/purchaseProduct"
        static let ListPurchases            = "/listPurchases"
        static let AddPhoneNumber           = "/addPhoneNumber"
        static let ListPhoneNumbers         = "/listPhoneNumbers"
        static let ListRecurrentRecharges   = "/listRecurrentRecharges"
        static let AddRecurrentRecharge     = "/addRecurrentRecharge"
        static let DeleteRecurrentRecharge  = "/removeRecurrentRecharge"


    }
    
    
    struct JSONKeys {
        static let SessionTokenKey = "sessionToken"
        static let WpsSessionIdKey = "idSessao"
        static let WpsEmailKey = "emailWps"
        static let ErrorCodeKey = "codError"
        static let ErrorMessageKey = "msgError"
        static let EmailConfirmationCodeKey = "codigoConfirmacaoEmail"
        static let NameKey = "nomeRazao"
        static let CpfOrCnpjKey = "cpfCnpj"
        static let CepKey = "cep"
        static let StateKey = "ufEstado"
        static let StreetKey = "logradouro"
        static let NumberKey = "numero"
        static let ComplementKey = "complemento"
        static let CityKey = "cidade"
        static let NeighborhoodKey = "bairro"
        static let HasOpticalReaderKey = "hasOpticalReader"
        static let WpsGarageIdKey = "idWpsGarage"
    }
}
