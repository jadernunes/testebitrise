//
//  RMOperator.swift
//  RecargaMobile
//
//  Created by MacMini on 28/11/16.
//  Copyright © 2016 4all Tecnologia. All rights reserved.
//

import UIKit

class RMOperator: NSObject {

    var operatorId  = ""
    var name        = ""
    var products    = Array<RMProduct>()
    
    override init() {
        super.init()
    }
    
    init(value : Dictionary<String, AnyObject>) {
        super.init()
        
        operatorId  = value["operatorId"] as? String ?? ""
        name        = value["name"] as? String ?? ""
        
        if let arrayDictProducts = value["products"] as? Array<[String : AnyObject]> {
            for dictProduct in arrayDictProducts {
                products.append(RMProduct(value: dictProduct))
            }
        }
    }
    
}
