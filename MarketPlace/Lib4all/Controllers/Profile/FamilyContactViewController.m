//
//  FamilyContactViewController.m
//  Example
//
//  Created by Adriano Soares on 15/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "FamilyContactViewController.h"
#import "BaseNavigationController.h"
#import "LayoutManager.h"
#import "User.h"
#import "ErrorTextField.h"
#import "UIImage+Color.h"
#import "NSStringMask.h"
#import "CardsTableViewController.h"
#import "FamilySetBalanceViewController.h"
#import "FamilyConfirmViewController.h"


@interface FamilyContactViewController ()

@property (weak, nonatomic) IBOutlet ErrorTextField *phoneTextField;

@property (strong, nonatomic) NSString *phoneTextImage;

@property (strong, nonatomic) NSString *phone;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@end

@implementation FamilyContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.phoneTextImage = @"iconPhone";
    
    self.phoneTextField.delegate = self;
    self.phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    
    [self configureLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     self.navigationItem.title = @"Membro";
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (IBAction)selectContact:(id)sender {
    if (NSClassFromString(@"CNContactPickerViewController")) {
        // iOS 9, 10, use CNContactPickerViewController
        CNContactPickerViewController *picker = [[CNContactPickerViewController alloc] init];
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    } else {
        ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
        picker.peoplePickerDelegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    // Get the multivalue number property.
    CFTypeRef multivalue = ABRecordCopyValue(person, property);
    
    // Get the index of the selected number. Remember that the number multi-value property is being returned as an array.
    CFIndex index = ABMultiValueGetIndexForIdentifier(multivalue, identifier);
    
    // Copy the number value into a string.
    NSString *number = (__bridge NSString *)ABMultiValueCopyValueAtIndex(multivalue, index);
    
    number = [self cleanPhoneString:number];
    
    [self setNumber: number];
}


- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
    if ([contactProperty.key isEqualToString:@"phoneNumbers"]) {
        [self setNumber:[[contactProperty valueForKey:@"value"] valueForKey:@"digits"]];
    }


}


- (void) setNumber: (NSString *)number {
    User *user = [User sharedUser];
    NSString *phoneNumber = @"";
    if ([number containsString:@"+55"]) {
        phoneNumber = [number stringByReplacingOccurrencesOfString:@"+" withString:@""];
        if (phoneNumber.length == 12) {
            phoneNumber = [NSString stringWithFormat:@"%@9%@", [phoneNumber substringToIndex:4], [phoneNumber substringFromIndex:4]];
        }
    } else if ([number length] == 11) {
        phoneNumber = [NSString stringWithFormat:@"55%@", number];
    } else if ([number length] == 10) {
        phoneNumber = [NSString stringWithFormat:@"55%@9%@", [number substringToIndex:2], [number substringFromIndex:2]];
    } else if ([number length] == 9) {
        NSString *ddd = [user.phoneNumber substringWithRange:NSMakeRange(2, 2)];
        phoneNumber = [NSString stringWithFormat:@"55%@%@", ddd, number];
    } else if ([number length] == 8) {
        NSString *ddd = [user.phoneNumber substringWithRange:NSMakeRange(2, 2)];
        phoneNumber = [NSString stringWithFormat:@"55%@9%@", ddd, number];
    }
    if (phoneNumber.length < 13) {
        return;
    }
    _phoneTextField.text = (NSString *)[NSStringMask maskString:[phoneNumber substringFromIndex:2] withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
    
}

- (NSString *) cleanPhoneString: (NSString *)phone {
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"[\\(\\)-]"
                                               withString:@""
                                                  options:NSRegularExpressionSearch
                                                    range:NSMakeRange(0, phone.length)];
    
    
    phone = [[phone componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];


    return phone;
}

- (IBAction)continueClick:(id)sender {
    if (![self isDataValid:[self cleanPhoneString:self.phoneTextField.text]]) {
        [_phoneTextField showFieldWithError:NO];
        return;
    }
    
    CardsTableViewController* cardPickerViewController = [[UIStoryboard storyboardWithName: @"Lib4all" bundle: nil]
                                         instantiateViewControllerWithIdentifier:@"CardsTableViewController"];

    
    // Exibe o viewController se foi possível obte-lo da storyboard e se há navigationController
    if (cardPickerViewController && self.navigationController) {
        cardPickerViewController.onSelectCardAction = OnSelectCardShowNextVC;
        cardPickerViewController.didSelectCardBlock = ^(NSString *cardID) {
            [self setBalance: cardID];
        };
        [self.navigationController pushViewController:cardPickerViewController animated:YES];
    }
}

- (void) setBalance:(NSString *)cardID {
    FamilySetBalanceViewController *balanceVC = [[UIStoryboard storyboardWithName: @"Lib4all" bundle: nil]
                                                 instantiateViewControllerWithIdentifier:@"FamilySetBalanceViewController"];
    
    balanceVC.cardID = cardID;
    balanceVC.phoneNumber = [self cleanPhoneString:self.phoneTextField.text];
    
    balanceVC.completion = ^(double amount) {
        FamilyConfirmViewController *vc = [[UIStoryboard storyboardWithName: @"Lib4all" bundle: nil]
                                           instantiateViewControllerWithIdentifier:@"FamilyConfirmViewController"];
        
        vc.cardID = cardID;
        vc.phoneNumber = [self cleanPhoneString:self.phoneTextField.text];
        vc.amount = amount;
        
        [self.navigationController pushViewController:vc animated:YES];
    
    };
    
    [self.navigationController pushViewController:balanceVC animated:YES];

}

- (BOOL)isDataValid:(NSString *)data {
    NSRegularExpression *phoneRegex = [NSRegularExpression regularExpressionWithPattern:@"^[\\d]*$"
                                                                                options:0
                                                                                  error:nil];
    NSString *cleanedPhone = [self cleanPhoneString:data];
    if ([phoneRegex numberOfMatchesInString:data options:0 range:NSMakeRange(0, data.length)] > 0 && cleanedPhone.length == 11) {
        return YES;
    }
    return NO;
}


// MARK: - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    // Permite backspace apenas com cursor no último caractere
    if (range.length == 1 && string.length == 0 && range.location != newString.length) {
        textField.selectedTextRange = [textField textRangeFromPosition:textField.endOfDocument toPosition:textField.endOfDocument];
        return NO;
    }
    
    newString = [self cleanPhoneString:newString];
    textField.text = (NSString *)[NSStringMask maskString:newString withPattern:@"\\((\\d{2})\\) (\\d{5})-(\\d{4})"];
    
    return NO;
}

- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Membro";
    
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    [self.phoneTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    [self.phoneTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.phoneTextField setFont:[[LayoutManager sharedManager] fontWithSize:16]];
    [self.phoneTextField setIconsImages:[UIImage imageNamed:self.phoneTextImage]
                               errorImg:[[UIImage imageNamed:self.phoneTextImage] withColor:[[LayoutManager sharedManager] red]]];
    self.phoneTextField.textColor = [[LayoutManager sharedManager] darkGray];
 
    self.orLabel.font = [[LayoutManager sharedManager] fontWithSize:14];
    self.orLabel.textColor = [[LayoutManager sharedManager] darkGray];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
