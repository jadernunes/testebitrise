//
//  FamilyDetailsViewController.m
//  Example
//
//  Created by Adriano Soares on 20/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "FamilyDetailsViewController.h"
#import "FamilySetBalanceViewController.h"
#import "BaseNavigationController.h"
#import "LoadingViewController.h"
#import "LayoutManager.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "User.h"
#import "FamilyProfileTableViewController.h"

@interface FamilyDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *organizerLabel;
@property (weak, nonatomic) IBOutlet UILabel *organizerValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *recipientLabel;
@property (weak, nonatomic) IBOutlet UILabel *recipientValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *avaliableLabel;
@property (weak, nonatomic) IBOutlet UILabel *avaliableValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *limitLabel;
@property (weak, nonatomic) IBOutlet UILabel *limitValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *limitDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeLimitButton;

@end

@implementation FamilyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
    
    double balance          = [_sharedDetails[@"balance"] doubleValue];;
    double recurringBalance = [_sharedDetails[@"recurringBalance"] doubleValue];
    double spentAmount      = recurringBalance - balance;

    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];

    self.totalNumberLabel.text    = [[formatter stringFromNumber: [NSNumber numberWithFloat:spentAmount/100]] stringByReplacingOccurrencesOfString:@"R$" withString:@"R$ "];
    self.avaliableValueLabel.text = [[formatter stringFromNumber: [NSNumber numberWithFloat:balance/100]] stringByReplacingOccurrencesOfString:@"R$" withString:@"R$ "];
    self.limitValueLabel.text     = [[formatter stringFromNumber: [NSNumber numberWithFloat:recurringBalance/100]] stringByReplacingOccurrencesOfString:@"R$" withString:@"R$ "];
    
    NSString *recurringDate = _sharedDetails[@"nextRecurringDate"];
    if (recurringDate != nil) {
        self.limitDateLabel.text      = [self.limitDateLabel.text stringByReplacingOccurrencesOfString:@"<day>" withString:[recurringDate substringFromIndex:recurringDate.length - 2]];
    } else {
        self.limitDateLabel.hidden = YES;
    }
    
    if ([_sharedDetails[@"provider"] boolValue] == YES) {
        self.organizerValueLabel.text = [User sharedUser].fullName;
        self.recipientValueLabel.text = _sharedDetails[@"identifier"];
    } else {
        [self.changeLimitButton removeFromSuperview];
        
        self.organizerValueLabel.text = _sharedDetails[@"identifier"];
        self.recipientValueLabel.text = [User sharedUser].fullName;
    }
}

- (IBAction)changeLimitButtonTouched {
    FamilySetBalanceViewController *viewController = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                                      instantiateViewControllerWithIdentifier:@"FamilySetBalanceViewController"];
    
    
    viewController.completion = ^(double amount) {
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        NSString *message = [NSString stringWithFormat:@"Confirmar a alteração do limite para %@?", [formatter stringFromNumber: [NSNumber numberWithFloat:amount/100]]] ;
        message = [message stringByReplacingOccurrencesOfString:@"R$" withString:@"R$ "];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Sim"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        Services *service = [[Services alloc] init];
                                        LoadingViewController *loader = [[LoadingViewController alloc] init];
                                        
                                        service.failureCase = ^(NSString *cod, NSString *msg){
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                                                           message:msg
                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                                      style:UIAlertActionStyleDefault
                                                                                    handler:nil]];
                                            
                                            [loader finishLoading:^{
                                                [self presentViewController:alert animated:YES completion:nil];
                                            }];
                                        };
                                        
                                        service.successCase = ^(NSDictionary *response){
                                            [loader finishLoading:^{
                                                for (UIViewController *controller in self.navigationController.viewControllers) {
                                                    if ([controller isKindOfClass:[FamilyProfileTableViewController class]]) {
                                                        [self.navigationController popToViewController:controller animated:YES];
                                                        break;
                                                    }
                                                }
                                            }];
                                        };
                                        
                                        [loader startLoading:self title:@"Aguarde..."];
                                        [service updateSharedCard:_cardID custumerId:_sharedDetails[@"customerId"] withBalance:[[NSNumber alloc] initWithDouble:amount]];
                                        
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Não"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    };
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)excludeShared:(id)sender {
    NSString *message = @"Realmente deseja excluir?";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Sim"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    Services *service = [[Services alloc] init];
                                    LoadingViewController *loader = [[LoadingViewController alloc] init];
                                    
                                    service.failureCase = ^(NSString *cod, NSString *msg) {
                                        [loader finishLoading:nil];
                                    };
                                    
                                    service.successCase = ^(NSDictionary *response) {
                                        [loader finishLoading:nil];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    };
                                    
                                    [service deleteSharedCard:self.cardID custumerId:[self.sharedDetails valueForKey:CustomerIdKey]];
                                    
                                    [loader startLoading:self title:@"Aguarde..."];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Não"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {

                               }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];



}

- (void)configureLayout {
    BaseNavigationController *navigationController = (BaseNavigationController *)self.navigationController;
    [navigationController configureLayout];
    self.navigationItem.title = @"Detalhes";

    LayoutManager *layoutManager = [LayoutManager sharedManager];
    self.view.backgroundColor = [layoutManager backgroundColor];

    self.organizerLabel.font = [layoutManager fontWithSize:18.0];
    self.organizerValueLabel.font = [layoutManager fontWithSize:15.0];
    self.organizerLabel.textColor = [layoutManager darkGray];
    self.organizerValueLabel.textColor = [layoutManager darkGray];
    
    self.recipientLabel.font = [layoutManager fontWithSize:18.0];
    self.recipientValueLabel.font = [layoutManager fontWithSize:15.0];
    self.recipientLabel.textColor = [layoutManager darkGray];
    self.recipientValueLabel.textColor = [layoutManager darkGray];
    
    self.totalLabel.font = [layoutManager fontWithSize:18.0];
    self.totalNumberLabel.font = [layoutManager fontWithSize:15.0];
    self.totalLabel.textColor = [layoutManager darkGray];
    self.totalNumberLabel.textColor = [layoutManager darkGray];
    
    self.avaliableLabel.font = [layoutManager fontWithSize:18.0];
    self.avaliableValueLabel.font = [layoutManager fontWithSize:15.0];
    self.avaliableLabel.textColor = [layoutManager darkGray];
    self.avaliableValueLabel.textColor = [layoutManager darkGray];
    
    self.limitLabel.font = [layoutManager fontWithSize:18.0];
    self.limitValueLabel.font = [layoutManager fontWithSize:15.0];
    self.limitLabel.textColor = [layoutManager darkGray];
    self.limitValueLabel.textColor = [layoutManager darkGray];
    
    self.limitDateLabel.font = [layoutManager fontWithSize:18.0];
    self.limitDateLabel.textColor = [layoutManager darkGray];
}

@end
