//
//  PasswordViewController.m
//  Example
//
//  Created by Cristiano Matte on 14/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "PasswordViewController.h"
#import "ErrorTextField.h"
#import "LayoutManager.h"
#import "UIImage+Color.h"
#import "Services.h"
#import "ServicesConstants.h"
#import "LoadingViewController.h"
#import "ForgotPasswordViewController.h"
#import "BlockedPasswordViewController.h"
#import "Lib4all.h"

@interface PasswordViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLable;;
@property (weak, nonatomic) IBOutlet ErrorTextField *passwordTextField;

@end

@implementation PasswordViewController

// MARK: - View controller life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureLayout];
    
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    [_passwordTextField setKeyboardType:UIKeyboardTypeDefault];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_passwordTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_passwordTextField resignFirstResponder];
}

// MARK: - Actions

- (IBAction)continueButtonTouched {
    Services *service = [[Services alloc] init];
    LoadingViewController *loading = [[LoadingViewController alloc] init];
    
    // Completion comum para os casos em que há algum erro nas requisições de dados do usuário
    void (^failureCase)(NSString *, NSString *) = ^(NSString *cod, NSString *msg) {
        // Informa o usuário sobre uma falha no login
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                       message:@"Falha na conexão. Tente novamente mais tarde."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            // Faz logout para remover todos os dados do usuários já baixados e fecha a tela de challenge
            [[Lib4all sharedInstance] callLogout:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:ok];
        [loading finishLoading:^{
            [self presentViewController:alert animated:YES completion:nil];
        }];
    };
    
    // O último passo para finalizar o login é obter as preferências de usuário
    void (^getAccountPreferences)(void) = ^{
        Services *getAccountPreferenceService = [[Services alloc] init];
        
        getAccountPreferenceService.failureCase = ^(NSString *cod, NSString *msg) {
            failureCase(cod, msg);
        };
        
        getAccountPreferenceService.successCase = ^(NSDictionary *response) {
            [[Lib4all sharedInstance].userStateDelegate userDidLogin];
            
            [loading finishLoading:^{
                [_signFlowController viewControllerDidFinish:self];
            }];
            return;
        };
        
        [getAccountPreferenceService getAccountPreferences:@[ReceivePaymentEmailsKey]];
    };
    
    // O terceiro passo da finalização de login é obter os cartões do usuário
    void (^getAccountCards)(void) = ^{
        Services *getAccountCardsService = [[Services alloc] init];
        
        getAccountCardsService.successCase = ^(NSDictionary *response){
            // Em caso de sucesso, chama o último passo da finalização de login
            getAccountPreferences();
        };
        
        getAccountCardsService.failureCase = ^(NSString *cod, NSString *msg){
            failureCase(cod, msg);
        };
        
        [getAccountCardsService listCards];
    };
    
    // O segundo passo da finalização de login é obter os dados do usuário
    void (^getAccountData)(void) = ^{
        Services *getAccountDataService = [[Services alloc] init];
        
        getAccountDataService.failureCase = ^(NSString *cod, NSString *msg) {
            failureCase(cod, msg);
        };
        
        getAccountDataService.successCase = ^(NSDictionary *response) {
            // Em caso de sucesso, chama o terceiro passo da finalização de login
            getAccountCards();
        };
        
        [getAccountDataService getAccountData:@[CustomerIdKey, PhoneNumberKey, EmailAddressKey, CpfKey, FullNameKey, BirthdateKey, EmployerKey, JobPositionKey]];
    };
    
    // O primeiro passo da finalização de login é verificar a senha
    service.successCase = ^(NSDictionary *data) {
        getAccountData();
    };
    
    service.failureCase = ^(NSString *code, NSString *msg) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Fechar"
                                              otherButtonTitles:nil];
        [loading finishLoading:^{
            if ([code isEqualToString:@"4.33"]) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção!"
                                                                               message:msg
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Entendi" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [self performSegueWithIdentifier:@"segueBlockedPassword" sender:nil];
                }];
                
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            } else {
                [alert show];
                [self.passwordTextField showFieldWithError:NO];
            }
        }];
    };
    
    [loading startLoading:self title:@"Aguarde..."];
    [service completeLoginWithPassword:_passwordTextField.text];
}

- (void)closeButtonTouched {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

// MARK: - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueForgotPassword"]) {
        ForgotPasswordViewController *viewController = segue.destinationViewController;
        viewController.signFlowController = _signFlowController;
    }
    if ([segue.identifier isEqualToString:@"segueBlockedPassword"]) {
        BlockedPasswordViewController *viewController = segue.destinationViewController;
        viewController.signFlowController = _signFlowController;
    }
}

// MARK: - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newString.length > 12) {
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self continueButtonTouched];
    return YES;
}

// MARK: - Layout

- (void)configureLayout {
    // Configura view
    self.view.backgroundColor = [[LayoutManager sharedManager] backgroundColor];
    
    // Configura navigation bar
    UIImageView *imgTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    imgTitle.image = [UIImage imageNamed:@"4allwhite"];
    imgTitle.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = imgTitle;

    self.titleLable.font = [[LayoutManager sharedManager] fontWithSize:24.0];
    self.titleLable.textColor = [[LayoutManager sharedManager] darkGray];
    
    UIColor *redColor = [[LayoutManager sharedManager] red];
    
    // Configura o text field
    [self.passwordTextField roundCustomCornerRadius:5.0 corners:UIRectCornerAllCorners];
    [self.passwordTextField setBorder:[[LayoutManager sharedManager] lightGray] width:1];
    [self.passwordTextField setIconsImages:[UIImage imageNamed:@"iconPassword"] errorImg:[[UIImage imageNamed:@"iconPassword"] withColor:redColor]];
    self.passwordTextField.backgroundColor = [UIColor whiteColor];
    self.passwordTextField.font = [[LayoutManager sharedManager] fontWithSize:16.0];
    self.passwordTextField.textColor = [[LayoutManager sharedManager] darkGray];
    self.passwordTextField.placeholder = @"Senha";
}

@end
