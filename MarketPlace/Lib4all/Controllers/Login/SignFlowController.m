//
//  SignFlowController.m
//  Example
//
//  Created by Cristiano Matte on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "SignFlowController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "PinViewController.h"
#import "PinConfirmationViewController.h"
#import "PasswordViewController.h"
#import "ChallengeViewController.h"
#import "AccountCreatedViewController.h"
#import "CardAdditionFlowController.h"
#import "DataFieldViewController.h"
#import "ChoosePaymentCardViewController.h"
#import "BlockedPasswordViewController.h"
#import "NameDataField.h"
#import "CPFDataField.h"
#import "BirthdateDataField.h"
#import "Lib4allPreferences.h"
#import "User.h"
#import "CreditCardsList.h"
#import "Lib4all.h"

@implementation SignFlowController

@synthesize onLoginOrAccountCreation = _onLoginOrAccountCreation;

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _onLoginOrAccountCreation = YES;
    }
    
    return self;
}

- (void)startFlowWithViewController:(UIViewController *)viewController {

}

- (void)viewControllerDidFinish:(UIViewController *)viewController {
    // O fluxo varia se for login ou cadastro
    if (_isLogin) {
        [self continueSignInFlowWihViewController:viewController];
    } else {
        [self continueSignUpFlowWihViewController:viewController];
    }
}

- (void)continueSignInFlowWihViewController:(UIViewController *)viewController {
    UIViewController *destination;
    User *user = [User sharedUser];
    
    if ([viewController isKindOfClass:[SignInViewController class]] && user.hasPassword && user.isPasswordBlocked) {
        // Redireciona para tela de senha bloqueada
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"BlockedPasswordViewController"];
        ((BlockedPasswordViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[SignInViewController class]] && user.hasPassword) {
        // Redireciona para tela de inserção de senha
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                         instantiateViewControllerWithIdentifier:@"PasswordViewController"];
        ((PasswordViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[SignInViewController class]]) {
        // Redireciona para tela de challenge
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                         instantiateViewControllerWithIdentifier:@"ChallengeViewController"];
        ((ChallengeViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[ChallengeViewController class]]) {
        // Após o challenge, apresenta a tela de senha
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                                         instantiateViewControllerWithIdentifier:@"PinViewController"];
        _enteredPhoneNumber = user.phoneNumber;
        _enteredEmailAddress = user.emailAddress;
        ((PinViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[ChoosePaymentCardViewController class]]) {
        // Finaliza o fluxo de login com pagamento
        [viewController dismissViewControllerAnimated:YES completion:^{
            if (_loginWithPaymentCompletion != nil) _loginWithPaymentCompletion(user.token, _selectedCardId);
        }];
    } else {
        BOOL requireFullName = (user.fullName == nil || [user.fullName isEqualToString:@""]);
        BOOL requireCpf = [[Lib4allPreferences sharedInstance] requireCpfOrCnpj] && (user.cpf == nil || [user.cpf isEqualToString:@""]);
        BOOL requireBirthdate = (user.birthdate == nil || [user.birthdate isEqualToString:@""]);
        
        if (requireFullName || requireCpf || requireBirthdate) {
            destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                           instantiateViewControllerWithIdentifier:@"DataFieldViewController"];
            
            if (requireFullName) {
                // Redireciona para tela de inserção de nome
                ((DataFieldViewController *)destination).dataFieldProtocol = [[NameDataField alloc] init];
            } else if (requireCpf) {
                // Redireciona para tela de inserção de cpf
                ((DataFieldViewController *)destination).dataFieldProtocol = [[CPFDataField alloc] init];
            } else if (requireBirthdate) {
                // Redireciona para tela de inserção de data de nascimento
                ((DataFieldViewController *)destination).dataFieldProtocol = [[BirthdateDataField alloc] init];
            }
            
            ((DataFieldViewController *)destination).flowController = self;
        } else if (_requirePaymentData && [[CreditCardsList sharedList] getDefaultCard] == nil) {
            // Inicia fluxo de adição de cartão
            CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] init];
            flowController.loginWithPaymentCompletion = _loginWithPaymentCompletion;
            flowController.onLoginOrAccountCreation = YES;
            
            [flowController startFlowWithViewController:viewController];
            return;
        } else if (_requirePaymentData) {
            // Redireciona para tela de escolha de cartão
            destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                           instantiateViewControllerWithIdentifier:@"ChoosePaymentCardViewController"];
            ((ChoosePaymentCardViewController *)destination).signFlowController = self;
        } else {
            // Finaliza o fluxo de login
            [viewController dismissViewControllerAnimated:YES completion:^{
                if (_loginCompletion != nil) _loginCompletion(user.phoneNumber, user.emailAddress, user.token);
            }];
        }
    }
    
    if (destination != nil) {
        [viewController.navigationController pushViewController:destination animated:YES];
    }
}

- (void)continueSignUpFlowWihViewController:(UIViewController *)viewController {
    UIViewController *destination;
    
    if ([viewController isKindOfClass:[SignInViewController class]]) {
        // Após a tela de login, apresenta a tela de cadastro
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"SignUpViewController"];
        ((SignUpViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[SignUpViewController class]]) {
        // Após a tela de cadastro, apresenta a tela de senha
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"PinViewController"];
        ((PinViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[PinConfirmationViewController class]]) {
        // Após a tela de senha, apresenta a tela de challenge
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"ChallengeViewController"];
        ((ChallengeViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[ChallengeViewController class]] && _requirePaymentData) {
        // Após a tela de challenge, se exige dados de pagamento, inicia fluxo de adição de cartão
        CardAdditionFlowController *flowController = [[CardAdditionFlowController alloc] init];
        flowController.loginWithPaymentCompletion = _loginWithPaymentCompletion;
        flowController.onLoginOrAccountCreation = YES;
        
        [flowController startFlowWithViewController:viewController];
    } else if ([viewController isKindOfClass:[ChallengeViewController class]]) {
        // Após a tela de challenge, se não exige dados de pagamento, apresenta tela final
        destination = [[UIStoryboard storyboardWithName:@"Lib4all" bundle:nil]
                       instantiateViewControllerWithIdentifier:@"AccountCreatedViewController"];
        ((AccountCreatedViewController *)destination).signFlowController = self;
    } else if ([viewController isKindOfClass:[AccountCreatedViewController class]]) {
        // Após tela final, chama o callback de login
        [viewController dismissViewControllerAnimated:YES completion:^{
            if (_loginCompletion != nil) {
                User *sharedUser = [User sharedUser];
                _loginCompletion(sharedUser.phoneNumber, sharedUser.emailAddress, sharedUser.token);
            }
        }];
    }
    
    if (destination != nil) {
        [viewController.navigationController pushViewController:destination animated:YES];
    }
}

- (void)viewControllerWillClose:(UIViewController *)viewController {
    // Caso o usuário feche uma tela de inserção de dados após o login, o login deve ser desfeito
    if (_isLogin &&
        ([viewController isKindOfClass:[DataFieldViewController class]] ||
         [viewController isKindOfClass:[PinViewController class]])) {
        [[Lib4all sharedInstance] callLogout:nil];
    }
}

@end
