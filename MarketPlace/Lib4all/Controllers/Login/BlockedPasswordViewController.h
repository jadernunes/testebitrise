//
//  BlockedPasswordViewController.h
//  Example
//
//  Created by Cristiano Matte on 15/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignFlowController.h"

@interface BlockedPasswordViewController : UIViewController

@property (strong, nonatomic) SignFlowController *signFlowController;

@end
