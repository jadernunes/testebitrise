//
//  ConfirmScheduleViewController.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 8/31/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import JGProgressHUD
import RealmSwift

class ConfirmScheduleViewController : UIViewController
{
    var dictionaryPostData: NSDictionary!
    
    @IBOutlet weak var labelMessageConfirm: UILabel!
    @IBOutlet weak var viewBackData: UIView!
    @IBOutlet weak var buttonConfirmSchedule: UIButton!
    
    @IBOutlet weak var labelTitleCompany: UILabel!
    @IBOutlet weak var labelNameCompany: UILabel!
    @IBOutlet weak var labelTitleService: UILabel!
    @IBOutlet weak var labelNameService: UILabel!
    @IBOutlet weak var labelTitleProfessional: UILabel!
    @IBOutlet weak var labelNameProfessional: UILabel!
    @IBOutlet weak var labelHourTitle: UILabel!
    @IBOutlet weak var labelHourText: UILabel!
    @IBOutlet weak var labelDateTitle: UILabel!
    @IBOutlet weak var labelDateText: UILabel!
    @IBOutlet weak var labelAddressTitle: UILabel!
    @IBOutlet weak var labelAddressText: UILabel!
    @IBOutlet weak var labelPriceTitle: UILabel!
    @IBOutlet weak var labelPriceValue: UILabel!
    
    var messageSchedule = kMessageSolicitationSchedule
    var titleSchedule   = kTitleSolicitationSchedule
    var titleButton4all = kButtonSolicitationScheduleTitle
    var subTitle        = kSubtitleSolicitationSchedule
    let loading = JGProgressHUD(style: .Dark)
    
    //MARK: - Init methods
    override func viewDidLoad() {
        Util.adjustTableViewInsetsFromTarget(self)

        self.viewBackData.layer.borderColor = SessionManager.sharedInstance.cardColor.CGColor
        self.viewBackData.layer.borderWidth = 1.0
        self.title = kConfirmScheduleTitle
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if SessionManager.sharedInstance.unity.immediateScheduling {
            GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(ScreensName.screenScheduleConfirm, idUnity: String(SessionManager.sharedInstance.idUnitToSchedule))
            messageSchedule = kMessageInstantSchedule
            titleSchedule   = kTitleInstantSchedule
            titleButton4all = kButtonInstantSchedule
            subTitle        = kSubtitleInstantSchedule
        } else {
            GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(ScreensName.screenScheduleRequest, idUnity: String(SessionManager.sharedInstance.idUnitToSchedule))
            labelMessageConfirm.text = kConfirmationSchedule
        }
        self.setLayout()
        self.setDataInFields()
        self.configurationButtonLogin4all()
        self.labelMessageConfirm.textColor = SessionManager.sharedInstance.cardColor
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    //MARK: - Set Colors and Fonys
    
    func setLayout() {
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        
        self.labelMessageConfirm.font = LayoutManager.primaryFontWithSize(21)
        
        self.labelNameCompany.textColor = LayoutManager.labelContrastColor
        self.labelNameCompany.font = LayoutManager.primaryFontWithSize(17)
        
        self.labelNameService.textColor = LayoutManager.labelContrastColor
        self.labelNameService.font = LayoutManager.primaryFontWithSize(17)
        
        self.labelNameProfessional.textColor = LayoutManager.labelContrastColor
        self.labelNameProfessional.font = LayoutManager.primaryFontWithSize(17)

        self.labelHourText.textColor = LayoutManager.labelContrastColor
        self.labelHourText.font = LayoutManager.primaryFontWithSize(17)

        self.labelDateText.textColor = LayoutManager.labelContrastColor
        self.labelDateText.font = LayoutManager.primaryFontWithSize(17)

        self.labelAddressText.textColor = LayoutManager.labelContrastColor
        self.labelAddressText.font = LayoutManager.primaryFontWithSize(17)

        self.labelPriceValue.textColor = LayoutManager.labelContrastColor
        self.labelPriceValue.font = LayoutManager.primaryFontWithSize(17)

    }
    //MARK: - Set data schedule in fields
    
    func setDataInFields()
    {
        let dataShow = dictionaryPostData.objectForKey("dataShow") as! NSDictionary
        
        let dateString = dataShow.objectForKey("dateToShow") as! String
        let hourStartToShow = dataShow.objectForKey("hourStartToShow") as! String
        let hourEndToShow = dataShow.objectForKey("hourEndToShow") as! String
        let dicProfessional = dataShow.objectForKey("professionalSelected") as! NSDictionary
        let firstName = dicProfessional.objectForKey("firstName") as! String
        let lastName = dicProfessional.objectForKey("lastName") as! String
        let service = SessionManager.sharedInstance.unityItem.title
        let address = SessionManager.sharedInstance.unity.address
        let nameCompany = SessionManager.sharedInstance.unity.name
        let valuePrice = SessionManager.sharedInstance.unityItem.price
        
        self.labelTitleCompany.text = kEstablishment
        self.labelNameCompany.text = nameCompany
        
        self.labelTitleService.text = kService
        self.labelNameService.text = service
        
        self.labelDateTitle.text = kDate
        self.labelDateText.text = dateString
        
        self.labelHourTitle.text = kTime
        self.labelHourText.text = "\(hourStartToShow) - \(hourEndToShow)"
        
        self.labelTitleProfessional.text = kProfessional
        self.labelNameProfessional.text = "\(firstName) \(lastName)"
        
        self.labelAddressTitle.text = kAddress
        self.labelAddressText.text = address
        
        self.labelPriceTitle.text = kValue
        self.labelPriceValue.text = "R$ " + String(format: "%.02f", valuePrice/100)
    }
    
    //MARK: - Configuration 4all button
    
    func configurationButtonLogin4all()
    {
        let lib4all = Lib4all()
        
        if lib4all.hasUserLogged()
        {
            buttonConfirmSchedule.setTitle(titleButton4all, forState: UIControlState.Normal)
        }
        else
        {
            buttonConfirmSchedule.setTitle(kEnter, forState: UIControlState.Normal)
        }
    }
    
    //MARK: - Save All Schedules
    
    func getAllSchedulesByServer()
    {
        let api = ApiServices()
        
        api.successCase = {(obj) in
            SchedulePersitence.deleteAllObjects()
            SchedulePersitence.saveManySchedule(obj as! NSArray)
        }
        
        api.failureCase = {(msg) in
        }
        
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    api.getAllScheduleBySessionToken(user.token)
                }
            }
        }
    }
    
    //MARK: - Confirm schedule
    
    @IBAction func buttonConfirmSchedulePressed(sender: AnyObject)
    {
        if Util.checkSchedule(self.dictionaryPostData, isConfirmSchedule: true) {
            
            let lib4all = Lib4all()
            if lib4all.hasUserLogged()
            {
                let api = ApiServices()
                
                let loading = JGProgressHUD(style: .Dark)
                loading.showInView(self.view)
                
                api.successCase = {(obj) in
                    loading.dismiss()
                    
                    self.getAllSchedulesByServer()
                    
                    let dataShow = self.dictionaryPostData.objectForKey("dataShow") as! NSDictionary
                    let dicProfessional = dataShow.objectForKey("professionalSelected") as! NSDictionary
                    let firstName = dicProfessional.objectForKey("firstName") as! String
                    let lastName = dicProfessional.objectForKey("lastName") as! String
                    let fullName = "\(firstName) \(lastName)"
                    
                    let nameCompany = SessionManager.sharedInstance.unity.name!
                    let service = SessionManager.sharedInstance.unityItem.title
                    let valuePrice = SessionManager.sharedInstance.unityItem.price
                    var urlImageUnity = (SessionManager.sharedInstance.unity.getThumb()?.value)
                    if urlImageUnity == nil
                    {
                        urlImageUnity = ""
                    }
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                    
                    (vc.viewControllers[0] as! ReceiptViewController).labels = ["success":self.messageSchedule,
                                                                                "subTitle":self.subTitle,
                                                                                "shop":nameCompany,
                                                                                "service":service,
                                                                                "price": valuePrice,
                                                                                "date":self.labelDateText.text!,
                                                                                "time": self.labelHourText.text!,
                                                                                "type": TypePageSuccess.Schedule.rawValue,
                                                                                "title": self.titleSchedule,
                                                                                "professional":fullName,
                                                                                "imageUrl":urlImageUnity!]
                    
                    (vc.viewControllers[0] as! ReceiptViewController).previousVC = self
                    
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                
                api.failureCase = {(msg) in
                    loading.dismiss()
                }
                
                let dataPost: NSMutableDictionary = NSMutableDictionary.init(dictionary: dictionaryPostData.objectForKey("dataPost") as! NSDictionary)
                
                //Get token by user loged
                let lib = Lib4all.sharedInstance()
                
                if lib.hasUserLogged() {
                    let user = User.sharedUser()
                    if user != nil {
                        if user.token != nil {
                            dataPost.setObject(user.token, forKey: "sessionToken")
                            api.makeSchedule(dataPost)
                        }
                    }
                }
            }
            else
            {
                lib4all.callLogin(self, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                    self.configurationButtonLogin4all()
                    SessionManager.sharedInstance.registerDevice()
                })
            }
        } else {
            let alert = UIAlertView(title: kAlertTitle, message: kErrorSelectingSchedule, delegate: self, cancelButtonTitle: kAlertButtonOk)
            alert.show()
        }
    }
}
