//
//  ListCoupomTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 29/09/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ListCoupomTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var constraintDistanceTopCell: NSLayoutConstraint!
    @IBOutlet weak var constraintDistanceBottomCell: NSLayoutConstraint!
    
    //values to show
    @IBOutlet weak var labelUnityName               : UILabel!
    @IBOutlet weak var labelShortDescriptionCampaign: UILabel!
    @IBOutlet weak var labelPriceTo                 : UILabel!
    @IBOutlet weak var labelPriceFrom               : UILabel!
    @IBOutlet weak var labelTitleFrom               : UILabel!
    @IBOutlet weak var labelTitleTo                 : UILabel!
    @IBOutlet weak var labelQuantityCoupom          : UILabel!
    @IBOutlet weak var labelDate                    : UILabel!
    @IBOutlet weak var imageViewLogoUnity           : UIImageView!
    @IBOutlet weak var labelLast: UILabel!
    @IBOutlet weak var labelCoupom: UILabel!
    @IBOutlet weak var labelTitleDateAllCoupom: UILabel!
    @IBOutlet weak var labelTitleDateOnlyUser: UILabel!
    @IBOutlet weak var labelDateOnlyUser: UILabel!
    @IBOutlet weak var viewShowItem: UIView!
    @IBOutlet weak var labelShowOneItem: UILabel!
    @IBOutlet weak var labelSatusCoupom: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelSatusCoupom.layer.masksToBounds = true
        self.labelSatusCoupom.layer.cornerRadius = 5
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func showDateAllCoupom(type: TypeShowListCoupons!){
        if (type == TypeShowListCoupons.meCoupons){
            self.labelCoupom.hidden = true
            self.labelLast.hidden = true
            self.labelQuantityCoupom.hidden = true
            self.labelDate.hidden = true
            self.labelTitleDateAllCoupom.hidden = true
            self.labelTitleDateOnlyUser.hidden = false
            self.labelDateOnlyUser.hidden = false
            self.labelSatusCoupom.hidden = false
        } else {
            self.labelCoupom.hidden = false
            self.labelLast.hidden = false
            self.labelQuantityCoupom.hidden = false
            self.labelDate.hidden = false
            self.labelTitleDateAllCoupom.hidden = false
            self.labelTitleDateOnlyUser.hidden = true
            self.labelDateOnlyUser.hidden = true
            self.labelSatusCoupom.hidden = true
        }
    }
    
    func showDiscountData(campaign: Campaign){
        
        if campaign.idCampaignDiscountType == TypeDiscountCampaign.price.rawValue {
            self.viewShowItem.hidden = true
            let doubleOriginalPrice = Double(campaign.originalPrice)
            let newPrice = Double(campaign.originalPrice - campaign.discount)
            
            let originalPrice = String(format:"R$ %.02f",(doubleOriginalPrice/100))
            let newValue = String(format:"R$ %.02f",(newPrice/100))
            
            self.labelPriceFrom.text = originalPrice
            self.labelPriceTo.text = String(newValue)
        } else {
            self.viewShowItem.hidden = false
            
            if campaign.discount <= 0 {
                self.labelShowOneItem.text = ""
            } else {
                self.labelShowOneItem.text = String("\(campaign.discount) %")
            }
        }
    }
    
    func populate(campaign: Campaign, type: TypeShowListCoupons!) {
        
        self.showDateAllCoupom(type)
        self.showDiscountData(campaign)
        var unity: Unity?
        
        if campaign.idUnity > 0 {
            unity = UnityPersistence.getUnityByID(String(campaign.idUnity))
        }
        
        self.labelUnityName.text = ""
        if unity != nil {
            if unity!.name != nil {
                self.labelUnityName.text = unity!.name
            }
        }
        
        self.labelShortDescriptionCampaign.text = campaign.shortDesc
        
        let coupom = CoupomPersistence.getCoupomByIdCampaign(campaign.id)
        if coupom != nil {
            if coupom!.redeemed {
                self.labelSatusCoupom.text = kUsed
            } else {
                self.labelSatusCoupom.text = kFree
            }
        }
        
        var urlImage = ""
        
        if unity != nil {
            if let logoUnity = unity?.getLogo() {
                urlImage = (logoUnity.value)!
            }
        }
        
        self.imageViewLogoUnity.addImage(urlImage)
        
        //Count coupons
        let couponAvailable = (campaign.couponLimit - campaign.couponClaimed)
        self.labelQuantityCoupom.text = String(couponAvailable)
        
        //Data end claim
        if (type == TypeShowListCoupons.allCupons){
            self.labelDateOnlyUser.text = String.getDateByServerConvertToShow((campaign.dateEndClaim)!)
        } else {
            self.labelDateOnlyUser.text = String.getDateByServerConvertToShow((campaign.dateEndRedeem)!)
        }
    }
}
