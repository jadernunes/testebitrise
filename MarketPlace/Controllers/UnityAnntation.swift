//
//  UnityAnntation.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 31/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class UnityAnntation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title           : String?
    var subtitle        : String?
    var unity           : Unity
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, unity: Unity) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.unity = unity
    }
}
