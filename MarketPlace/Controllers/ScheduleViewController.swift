//
//  ScheduleViewController.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 8/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ScheduleViewController : UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var collectionViewPerson: UICollectionView!
    @IBOutlet weak var constraintHeightCollectionViewPerson: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewTitleUnity: NSLayoutConstraint!
    @IBOutlet weak var tableViewDay: UITableView!
    @IBOutlet weak var tableViewListHour: UITableView!
    @IBOutlet weak var labelDateSelected: UILabel!
    @IBOutlet weak var labelServiceName: UILabel!
    @IBOutlet weak var viewBaseNameService: UIView!
    @IBOutlet weak var viewLabelDaySelected: UIView!
    
    private let quantityShowDays: Int! = 30
    private var listAgendas: NSArray!
    private var userSelected: NSDictionary!
    private var daysLokedInMonth: NSMutableArray!
    private var daysLokedNextMonth: NSMutableArray!
    private var daySelected: NSDictionary!
    private var listHoursByDay: NSArray! = []
    private var dictionaryPostData: NSDictionary!
    
    enum ActionUserChoused: Int {
        case Refuse = 0, Accepted
    }
    
    override func viewWillAppear(animated: Bool) {
        CartCustomManager.sharedInstance.hideCart()
        GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(ScreensName.screenSchedule, idUnity: String(SessionManager.sharedInstance.idUnitToSchedule))
        self.title = kScheduleTitle
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    //MARK: - Init Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        self.daysLokedInMonth = NSMutableArray()
        self.daysLokedNextMonth = NSMutableArray()
        self.listAgendas = SessionManager.sharedInstance.listCalendar
        self.collectionViewPerson.backgroundColor = LayoutManager.backgroundFirstColor
        self.viewBaseNameService.backgroundColor = LayoutManager.backgroundFirstColor
        self.tableViewDay.backgroundColor = LayoutManager.backgroundFirstColor
        self.viewLabelDaySelected.backgroundColor = SessionManager.sharedInstance.cardColor
        if(listAgendas.count > 0)
        {
            userSelected = self.listAgendas.objectAtIndex(0) as! NSDictionary
            selectUserAndDayAndHour()
        }
        else
        {
            //don't have Professionals data
            constraintHeightCollectionViewPerson.constant = 0
            constraintHeightViewTitleUnity.constant = 0
        }
        
        self.labelServiceName.text = SessionManager.sharedInstance.unityItem.title
        self.labelServiceName.textColor = LayoutManager.labelContrastColor
    }
    
    //MARK: - Utils Functions
    
    func getDurationByUnitItem() -> Double
    {
        let listService = userSelected.objectForKey("services") as! NSArray
        
        for service in  listService
        {
            if service.objectForKey("id")?.integerValue == SessionManager.sharedInstance.idUnitItem
            {
                return NSString.horasMinutosToSeconds(service.objectForKey("duration") as! String)
            }
        }
        
        return 0
    }
    
    func getListUnitBlockedHours(dateString:NSString!) -> NSArray
    {
        let keyLiteralDay = NSString.getLiteralDayFromCalendarUSA(dateString).lowercaseString
        var listUnitBlockedHours = NSArray()
        
        if (SessionManager.sharedInstance.unityShifts.objectForKey(keyLiteralDay) != nil) {
            listUnitBlockedHours = SessionManager.sharedInstance.unityShifts.objectForKey(keyLiteralDay) as! NSArray
        }
        
        return listUnitBlockedHours
    }
    
    func getListHourSelectedDay() -> NSArray?
    {
        let tempoAtendimento: NSTimeInterval = self.getDurationByUnitItem()
        let intervalo: NSTimeInterval = 1800
        var listaHorasDiaSelecionado: [NSDictionary] = []
        let data = daySelected.objectForKey("day") as! NSString
        let listaPeriodosAtivos: NSArray = daySelected.objectForKey("shifts") as! NSArray
        let listaPeriodosBloqueados: NSArray = daySelected.objectForKey("blockedShifts") as! NSArray
        let listUnitBlockedHours = getListUnitBlockedHours(data)
        
        for periodoAtivo in listaPeriodosAtivos
        {
            //08:00
            var horarioInicial = ((data as String) + " " + (periodoAtivo.objectForKey("startAt") as! String))
            horarioInicial = ((data as String) + " " + NSString.getHourByDate(horarioInicial))
            
            //10:00 (08:00 + 2:00)
            var horarioInicialMaisTempoAtendimento = NSDate.nextDateStringByTimeInterval(NSDate.completeDateByStringDateWithFormat(horarioInicial),newTimeInter: tempoAtendimento)
            
            //12:00
            let horarioFinal = ((data as String) + " " + (periodoAtivo.objectForKey("endAt") as! String))
            var difProximoHorarioMenoHorarioFinal: NSTimeInterval = NSDate.secondsByDifferenceDates(horarioInicialMaisTempoAtendimento, dateStringFinish: horarioFinal)
            
            var i = 0
            //se 10:00 <= 12:00
            while difProximoHorarioMenoHorarioFinal < 0
            {
                if i > 0
                {
                    //08:30
                    var proximaHorarioInicial = NSDate.nextDateStringByTimeInterval(NSDate.completeDateByStringDateWithFormat(horarioInicial),newTimeInter: intervalo) as String
                    horarioInicial = proximaHorarioInicial
                    
                    //se 08:30 <= 12:00
                    difProximoHorarioMenoHorarioFinal = NSDate.secondsByDifferenceDates(proximaHorarioInicial, dateStringFinish: horarioFinal)
                    
                    if difProximoHorarioMenoHorarioFinal <= 0
                    {
                        //10:30 (08:30 + 2:00)
                        proximaHorarioInicial = NSDate.nextDateStringByTimeInterval(NSDate.completeDateByStringDateWithFormat(proximaHorarioInicial),newTimeInter: tempoAtendimento) as String
                        horarioInicialMaisTempoAtendimento = proximaHorarioInicial
                        
                        //se 10:30 <= 12:00 === If next time lass than and equal 0 can add hour
                        difProximoHorarioMenoHorarioFinal = NSDate.secondsByDifferenceDates(proximaHorarioInicial, dateStringFinish: horarioFinal)
                    }
                }
                
                if SessionManager.sharedInstance.hourAvaliableInUnit(data, startDateString: horarioInicial, nextDate: horarioInicialMaisTempoAtendimento, listHourUnit: listUnitBlockedHours)
                {
                    let hourStringDate = (NSString.getHourByDateWithHourFormat(horarioInicial) + " - " + NSString.getHourByDateWithHourFormat(horarioInicialMaisTempoAtendimento))
                   
                    let status: Bool  = SessionManager.sharedInstance.hourAvaliable(data, startDateString: horarioInicial, nextDate: horarioInicialMaisTempoAtendimento, listBlocked: listaPeriodosBloqueados)
                    
                    let dicResult = [
                        "dataPost":[
                            "date":NSString.getDateToServerByDateFormat(data),
                            "timeBegin":NSString.getHourByDate(horarioInicial),
                            "timeEnd":NSString.getHourByDate(horarioInicialMaisTempoAtendimento),
                            "hourResult":hourStringDate,
                            "status":status
                            ] as NSDictionary!,
                        "dataShow":[
                            "hourStartToShow":NSString.getHourByDateWithHourFormat(horarioInicial),
                            "hourEndToShow":NSString.getHourByDateWithHourFormat(horarioInicialMaisTempoAtendimento),
                            "dateToShow":data
                            ] as NSDictionary!
                        ] as NSDictionary!
                    listaHorasDiaSelecionado.append(dicResult)
                }
                
                i += 1
            }
        }
        
        return listaHorasDiaSelecionado
    }
    
    func getFirstDayHasUser() -> (day: Int,month: Int)
    {
        let listDays = userSelected.objectForKey("availabilty") as! NSArray
        var lesserDayInMonth = 40
        var lesserDayInNextMonth = 40
        var firstDay = 0
        var firstMonth = 0
        
        for dic in listDays
        {
            let month = NSDate.monthByDateWithFormat(dic.objectForKey("day") as! NSString)
            let day = NSDate.dayByDateWithFormat(dic.objectForKey("day") as! NSString)
            
            if month == NSDate.month()
            {
                if day <= lesserDayInMonth
                {
                    lesserDayInMonth = day
                }
            }
            else if month == NSDate.month()+1
            {
                if day <= lesserDayInNextMonth
                {
                    lesserDayInNextMonth = day
                }
            }
        }
        
        if lesserDayInMonth >= NSDate.day() && lesserDayInMonth != 40
        {
            firstDay = lesserDayInMonth
            firstMonth = NSDate.month()
        }
        else if lesserDayInNextMonth != 40
        {
            firstDay = lesserDayInNextMonth
            firstMonth = NSDate.month()+1
        }
        
        return (firstDay,firstMonth)
    }
    
    func selectUserAndDayAndHour()
    {
        self.tableViewDay.reloadData()
        // update some UI
        self.daySelected = self.userSelected.objectForKey("availabilty")?.objectAtIndex(0) as! NSDictionary
        let monthUser = NSDate.monthByDateWithFormat(self.daySelected.objectForKey("day") as! NSString)
        var sectionDay: Int = 1
        var dayToSelect =  NSDate.dayByDateWithFormat(self.daySelected.objectForKey("day") as! NSString)
        let dayToShow = dayToSelect
        var monthToSelect = 0
        
        dayToSelect = self.getFirstDayHasUser().day
        monthToSelect = self.getFirstDayHasUser().month
        
        if monthToSelect == NSDate.month()
        {
            sectionDay = 0
            dayToSelect = (dayToSelect - NSDate.day())
        }
        else
        {
            dayToSelect -= 1
        }
        
        let indexPath:NSIndexPath = NSIndexPath(forRow: dayToSelect, inSection: sectionDay)
        self.tableViewDay.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)
        
        let customDateString = NSString.dateToCustomFormat(dayToShow, month: monthUser, year: NSDate.year())
        self.labelDateSelected.text = "\(customDateString)"
        
        dispatch_async(dispatch_get_main_queue()) {
            
            if self.getListHourSelectedDay() != nil {
                self.listHoursByDay = self.getListHourSelectedDay()
                self.tableViewListHour.reloadData()
            }
        }
    }
    
    func reloadHours()
    {
        dispatch_async(dispatch_get_main_queue()) {
            
            self.listHoursByDay = self.getListHourSelectedDay()
            self.tableViewListHour.reloadData()
        }
    }
    
    //MARK: - CollectionView Methods
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_PERSON_SCHEDULE, forIndexPath: indexPath) as! PersonCollectionViewCell
        
        let imageName = listAgendas.objectAtIndex(indexPath.row).objectForKey("thumb")
        if imageName!.isKindOfClass(NSString)
        {
            let auxImageName = imageName as! String
            cell.thumbProfile.addImage(auxImageName) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
                if image != nil {
                    cell.thumbProfile.contentMode      = .ScaleAspectFill
                }
            }
        }
        
        let firstName = listAgendas.objectAtIndex(indexPath.row).objectForKey("firstName") as! String
        cell.nameProfile.text = firstName
        cell.nameProfile.textColor = LayoutManager.labelContrastColor
        
        if userSelected != nil
        {
            let idUserSelected = userSelected.objectForKey("idUser") as! Int
            let idUserIndex = listAgendas.objectAtIndex(indexPath.row).objectForKey("idUser") as! Int
            
            if idUserIndex == idUserSelected
            {
                cell.backgroundColor = LayoutManager.professionalSelectedOnSchedule
            }
            else
            {
                cell.backgroundColor = LayoutManager.backgroundFirstColor
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        userSelected = self.listAgendas.objectAtIndex(indexPath.row) as! NSDictionary
        collectionView.reloadData()
        
        dispatch_async(dispatch_get_main_queue()) {
            self.selectUserAndDayAndHour()
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(collectionView == self.collectionViewPerson)
        {
            return listAgendas.count
        }
        else
        {
            return 0
        }
    }
    
    //MARK: - TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if tableView == tableViewDay
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tableViewDay
        {
            return 50
        }
        else
        {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int)
    {
        if let headerView = view as? UITableViewHeaderFooterView
        {
            headerView.textLabel?.font = LayoutManager.primaryFontWithSize(18)
            headerView.textLabel?.textColor = LayoutManager.white
            headerView.backgroundView?.backgroundColor = SessionManager.sharedInstance.cardColor
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let daysMonth = NSDate.getDaysInMonth()
        let daysInMonth = (daysMonth - NSDate.day()) + 1
        let daysNextMonth = (quantityShowDays - daysInMonth)
        
        if tableView == tableViewDay
        {
            if section == 0
            {
                return daysInMonth - self.daysLokedInMonth.count
            }
            else
            {
                return daysNextMonth - self.daysLokedNextMonth.count
            }
        }
        else
        {
            if daySelected == nil
            {
                return 0
            }
            else
            {
                return listHoursByDay.count
            }
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0
        {
            return NSString.monthLiteralByMonth(NSDate.month()) as String
        }
        
        return NSString.monthLiteralByMonth(NSDate.month()+1) as String
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView == tableViewDay
        {
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_DAY_SCHEDULE, forIndexPath: indexPath) as! DayTableViewCell
            let listDays = userSelected.objectForKey("availabilty") as! NSArray
            cell.backgroundColor = LayoutManager.backgroundFirstColor
            cell.descriptionDay.textColor = LayoutManager.labelContrastColor
            cell.numericDay.textColor = LayoutManager.labelContrastColor
            var onceExecute: Bool = true
            for dic in listDays
            {
                if onceExecute == true
                {
                    let monthUser =  NSDate.monthByDateWithFormat(dic.objectForKey("day") as! NSString)
                    
                    if monthUser == NSDate.month() && indexPath.section == 0
                    {
                        let dayUser = NSDate.dayByDateWithFormat(dic.objectForKey("day") as! NSString)
                        let dayCust = (NSDate.day()+indexPath.row)
                        
                        if dayUser == dayCust
                        {
                            onceExecute = false
                        }
                    }
                    else if monthUser == NSDate.month()+1 && indexPath.section == 1
                    {
                        let dayUser = NSDate.dayByDateWithFormat(dic.objectForKey("day") as! NSString)
                        
                        let dayCust = (indexPath.row + 1)
                        if dayUser == dayCust
                        {
                            onceExecute = false
                        }
                    }
                }
            }
            
            if indexPath.section == 0
            {
                cell.numericDay.text = "\(NSDate.day()+indexPath.row)"
                let dayLiteral = NSString.dayLiteralByDayMonthAndYear(NSDate.day()+indexPath.row, month: NSDate.month(), year: NSDate.year())
                cell.descriptionDay.text = "\(dayLiteral)"
            }
            else
            {
                let dayLiteral = NSString.dayLiteralByDayMonthAndYear(indexPath.row+1, month: NSDate.month()+1, year: NSDate.year())
                
                cell.numericDay.text = "\(indexPath.row+1)"
                cell.descriptionDay.text = "\(dayLiteral)"
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_HOUR_SCHEDULE, forIndexPath: indexPath) as! HourTableViewCell
            
            let dicResult = listHoursByDay.objectAtIndex(indexPath.row) as! NSDictionary
            let dataPost = dicResult.objectForKey("dataPost") as! NSDictionary
            
            let status: Bool = (dataPost.objectForKey("status")?.boolValue)!
            
            if(status == true)
            {
                cell.labelHour?.textColor = UIColor.blackColor()
            }
            else
            {
                cell.labelHour?.textColor = UIColor.lightGrayColor()
            }
            
            cell.accessoryType = UITableViewCellAccessoryType.None
            cell.labelHour.text = "\(dataPost.objectForKey("hourResult")!)"
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.tableViewListHour
        {
            let dicHour = listHoursByDay.objectAtIndex(indexPath.row) as! NSDictionary
            let dataPost = dicHour.objectForKey("dataPost") as! NSDictionary
            let dataShow = dicHour.objectForKey("dataShow") as! NSDictionary
            
            let dicResult = [
                "dataPost":[
                    "idProfessional":userSelected.objectForKey("idProfessional")!,
                    "date":dataPost.objectForKey("date")!,
                    "timeBegin":dataPost.objectForKey("timeBegin")!,
                    "timeEnd":dataPost.objectForKey("timeEnd")!,
                    "idUnity":SessionManager.sharedInstance.idUnitToSchedule,
                    "idUnityItem":SessionManager.sharedInstance.idUnitItem,
                    "idSchedulingType":1,
                    "idStatus":1,
                    ] as NSDictionary!,
                "dataShow":[
                    "hourStartToShow":dataShow.objectForKey("hourStartToShow")!,
                    "hourEndToShow":dataShow.objectForKey("hourEndToShow")!,
                    "professionalSelected":userSelected,
                    "dateToShow":dataShow.objectForKey("dateToShow")!
                    ] as NSDictionary!
            ] as NSDictionary
            dictionaryPostData = dicResult
            
            let status: Bool = (dataPost.objectForKey("status")?.boolValue)!
            
            if status == true
            {
                self.performSegueWithIdentifier("segueGotoConfirmSchedule", sender: nil)
            }
        }
        else
        {
            let listDays = userSelected.objectForKey("availabilty") as! NSArray
            
            var onceExecute: Bool = true
            for dic in listDays
            {
                if onceExecute == true
                {
                    let monthUser = NSDate.monthByDateWithFormat(dic.objectForKey("day") as! NSString)
                    var nextMonth = NSDate.month()+1
                    
                    if NSDate.month() == 12 {
                        nextMonth = 1
                    }
                    
                    if monthUser == NSDate.month() && indexPath.section == 0
                    {
                        let dayUser = NSDate.dayByDateWithFormat(dic.objectForKey("day") as! NSString)
                        let dayCust = (NSDate.day()+indexPath.row)
                        
                        if dayUser == dayCust
                        {
                            onceExecute = false
                            daySelected = dic as! NSDictionary
                            self.tableViewListHour.reloadData()
                        }
                    }
                    else if monthUser == nextMonth && indexPath.section == 1
                    {
                        let dayUser = NSDate.dayByDateWithFormat(dic.objectForKey("day") as! NSString)
                        
                        let dayCust = (indexPath.row + 1)
                        if dayUser == dayCust
                        {
                            onceExecute = false
                            daySelected = dic as! NSDictionary
                            self.tableViewListHour.reloadData()
                        }
                    }
                }
            }
            
            if onceExecute == true
            {
                daySelected = nil
                self.tableViewListHour.reloadData()
            }
            else
            {
                self.reloadHours()
            }
            
            if indexPath.section == 0
            {
                let customDateString = NSString.dateToCustomFormat(NSDate.day()+indexPath.row, month: NSDate.month(), year: NSDate.year())
                labelDateSelected.text = "\(customDateString)"
            }
            else
            {
                let customDateString = NSString.dateToCustomFormat(indexPath.row+1, month: NSDate.month()+1, year: NSDate.year())
                labelDateSelected.text = "\(customDateString)"
            }
        }
    }
    
    //MARK: - Confirm Schedule
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let confirmViewController = segue.destinationViewController as! ConfirmScheduleViewController
        confirmViewController.dictionaryPostData = dictionaryPostData
    }
}
