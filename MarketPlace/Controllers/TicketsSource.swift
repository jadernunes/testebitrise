//
//  TicketsSource.swift
//  MarketPlace
//
//  Created by Luciano on 8/23/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketsSource: NSObject, UITableViewDelegate, UITableViewDataSource {

    var movie : Movie!
    var superViewController: MovieDetailsViewController!
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_TICKET_MOVIE) as! TicketsTableViewCell
        
        let session = movie.getAvailableSessions().objectAtIndex(indexPath.row) as! MovieSession
        cell.superViewController = self.superViewController
        cell.movie = self.movie
        cell.session = session
        
        cell.labelHour.text = session.getFormatedDateString("HH:mm")
        
        //Hide price when less than it
        var priceMovie = ""
        if session.price.value > 0 {
            priceMovie = Util.formatCurrency(session.price.value!)
        }
        
        cell.labelPrice.text = priceMovie
        
        if session.subtitled == true {
            cell.labelType.text = kSubtitled
        }else{
            cell.labelType.text = kDubbed
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movie.getAvailableSessions().count
    }
    
}
