//
//  SectionsTableView.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

enum PositionSectionNewHome: Int {
    case Banner = 0
    case Gastronomy = 1
    case Entertainment = 2
}

enum TypeNewSection: Int {
    case Banner = 0
    case List = 1
}

class SectionsTableView: UITableView {
    
    var superViewController: NewHomeViewController?
    var sections:[[String: Any]] = [
        [
            "tintColor":"",
            "icon":"",
            "title":"",
            "actions":nil as [Unity]?,
            "position":0,
            "type":0
        ],
        [
            "tintColor":UIColor.redFood4all(),
            "icon":"iconFoodNew",
            "title":"COMA E BEBA DO SEU JEITO",
            "actions":[
                [
                    "title":"Delivery",
                    "icon":"iconDelivery",
                    "target":"foodEntrega",
                    "isShowBadge":false
                ],
                [
                    "title":"Retirar na loja",
                    "icon":"iconTakeout",
                    "target":"foodRetirada",
                    "isShowBadge":false
                ],
                [
                    "title":"Pagar comanda",
                    "icon":"iconComanda",
                    "target":"foodComanda",
                    "isShowBadge":false
                ]
                ] as [[String: Any]],
            "position":1,
            "type":1
        ],
        [
            "tintColor":UIColor.orangeFun4all(),
            "icon":"iconFunNew",
            "title":"CONECTE O SEU DIA",
            "actions":[
                [
                    "title":"Ingressos e \nvouchers",
                    "icon":"iconEvent",
                    "target":"4allCardDetailVC",
                    "isShowBadge":false
                ],
                [
                    "title":"Cupons de \ndesconto",
                    "icon":"iconCoupom",
                    "target":"allCoupom",
                    "isShowBadge":false
                ],
                [
                    "title":"Recarga de \ncelular",
                    "icon":"iconRecargaCelular",
                    "target":"recargaCelular",
                    "isShowBadge":false
                ],
                [
                    "title":"Mapa de \nestabelecimentos",
                    "icon":"iconNewMap",
                    "target":"newMap",
                    "isShowBadge":false
                ],
                [
                    "title":"Lojas \npróximas",
                    "icon":"iconNewLocation",
                    "target":"LojasProximasNewHomeViewController",
                    "isShowBadge":false
                ],
                [
                    "title":"Recarga de \ntransporte",
                    "icon":"iconShowCard",
                    "target":"MyCards",
                    "isShowBadge":false
                ]
            ] as [[String: Any]],
            "position":2,
            "type":1
        ]
    ]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.registerNib(UINib(nibName: "BannerNewHomeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "BannerNewHomeTableViewCell")
        self.registerNib(UINib(nibName: "ActionNewHomeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ActionNewHomeTableViewCell")
        
        self.dataSource = self
        self.delegate   = self
        self.backgroundColor = UIColor.whiteColor()
    }
}

extension SectionsTableView: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        switch indexPath.row {
        case PositionSectionNewHome.Banner.rawValue:
            return 135
            case PositionSectionNewHome.Entertainment.rawValue:
            return 230
        default:
            return 125
        }
    }
    
}

extension SectionsTableView: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let section = sections[indexPath.row]
        let position = section["position"] as! Int
        let title = section["title"] as! String
        
        switch position {
        case PositionSectionNewHome.Banner.rawValue:
            if let cell = tableView.dequeueReusableCellWithIdentifier("BannerNewHomeTableViewCell", forIndexPath: indexPath) as? BannerNewHomeTableViewCell {
                cell.superViewController = self.superViewController
                
                if let actions = section["actions"] as? [Unity] {
                    cell.populateData(actions)
                }
                
                return cell
            }
            break
        default:
            if let cell = tableView.dequeueReusableCellWithIdentifier("ActionNewHomeTableViewCell", forIndexPath: indexPath) as? ActionNewHomeTableViewCell {
                
                cell.labelTitle.text = title
                cell.imageViewIcon.image = UIImage(named: section["icon"] as! String)
                cell.viewLineSeparator.backgroundColor = UIColor.lightGray4all() //section["tintColor"] as? UIColor
                cell.superViewController = self.superViewController
                cell.viewLineSeparator.hidden = false
                
                if let actions = section["actions"] as? [[String: Any]] {
                    cell.populateData(actions, section: section)
                }
                
                if indexPath.row == sections.count - 1 {
                    cell.viewLineSeparator.hidden = true
                }
                
                
                return cell
            }
            break
        }
        
        let cell = UITableViewCell()
        return cell
    }
}
