//
//  TrackingViewController.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/14/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class TrackingViewController: UIViewController {

    var order = Order()
    var orderStatus = NSArray()
    var loadOrderStatusTask : NSTimer!
    var load: LoadingViewController!

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TrackingViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        
        return refreshControl
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var viewSeparatorHeader: UIView!
    @IBOutlet var viewHeaderItems: UIView!
    @IBOutlet weak var viewSeparatorHeaderItems: UIView!
    @IBOutlet weak var labelHeaderItems: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        load = LoadingViewController.sharedManager() as! LoadingViewController
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.registerCells()
        self.tableView.addSubview(self.refreshControl)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        Util.adjustTableViewInsetsFromTarget(self)
        
        self.load.startLoading(self, title: "Aguarde")
        startPolling()
    }
    override func viewWillDisappear(animated: Bool) {
        loadOrderStatusTask.invalidate()
    }

    func startPolling() -> Void {
        loadOrderStatusTask = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(TrackingViewController.loadTrackingWithTimer), userInfo: nil, repeats: true)
        loadOrderStatusTask.fire()
    }
    
    func refresh() {
        self.load.startLoading(self, title: "Aguarde")
        self.loadTrackingFromServer()
    }
    
    func loadTrackingWithTimer() {
        loadTrackingFromServer()
    }
    
    func loadTrackingFromServer() -> Void {
        
        let api = ApiServices()
        
        api.failureCase = { (msg) in
            self.load.finishLoading(nil)
        }
        
        api.successCase = { (arrayStatus) in
            
            if let arrayOrderStatus = arrayStatus as? NSArray {
                self.updateSatusHistory(arrayOrderStatus)
            }
        }
        
        api.getOrderStatus(String(order.id))
    }
    
    private func updateSatusHistory(arrsyStatus: NSArray){
        self.orderStatus = arrsyStatus
        
        //check if there is status 80 to avoid the polling
        for item in arrsyStatus {
            if let status = item as? Dictionary<String, AnyObject> {
                if status["idOrderStatus"] as! Int > self.order.status {
                    let realm = try! Realm()
                    
                    realm.beginWrite()
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_HOME_CART_OBSERVER, object: nil)
                    })
                    
                    self.order.status = status["idOrderStatus"] as! Int
                    self.order.dthrLastStatus = NSDate().timeIntervalSince1970 * 1000
                    try! realm.commitWrite()
                }
            }
        }
        
        self.load.finishLoading(nil)
        
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func registerCells() {
        self.tableView.registerNib(UINib(nibName: "OrderStatusTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderStatusTableViewCell")
        self.tableView.registerNib(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")
        self.tableView.registerNib(UINib(nibName: "NumberAndDeliveryTableViewCell", bundle: nil), forCellReuseIdentifier: "NumberAndDeliveryTableViewCell")
        self.tableView.registerNib(UINib(nibName: "OrderStatusTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderStatusTableViewCell")
        self.tableView.registerNib(UINib(nibName: "TotalOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "TotalOrderTableViewCell")
        self.tableView.registerNib(UINib(nibName: "ItemsTrackingTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemsTrackingTableViewCell")

    }

    func layoutHeaderView(titleHeader: String) {
        viewSeparatorHeader.backgroundColor = SessionManager.sharedInstance.cardColor
        labelHeader.font = LayoutManager.primaryFontWithSize(21)
        labelHeader.textColor = LayoutManager.darkGreyTracking
        labelHeader.text = titleHeader
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0,UIScreen.mainScreen().bounds.width, 0.5)
        topBorder.backgroundColor = LayoutManager.lightGreyTracking.CGColor
        viewHeader.layer.addSublayer(topBorder)
    }
    
    func layoutHeaderItemsView(titleHeader: String) {
        viewSeparatorHeaderItems.backgroundColor = SessionManager.sharedInstance.cardColor
        labelHeaderItems.font = LayoutManager.primaryFontWithSize(21)
        labelHeaderItems.textColor = LayoutManager.darkGreyTracking
        labelHeaderItems.text = titleHeader
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0,UIScreen.mainScreen().bounds.width, 0.5)
        topBorder.backgroundColor = LayoutManager.lightGreyTracking.CGColor
        viewHeaderItems.layer.addSublayer(topBorder)
    }
    
    private func informationDetail(orderItem:OrderItem) -> (heightText:CGFloat!,text:String){
        
        var heightCell = CGFloat(86)
        var listSubItems = ""
        if orderItem.subItems.count > 0 {
            
            for i in 0...orderItem.subItems.count-1 {
                let item = orderItem.subItems[i]
                if i == 0 {
                    listSubItems = item.unityItem!.title
                } else {
                    listSubItems += ", " + item.unityItem!.title
                }
            }
            
            heightCell += CGFloat(CGFloat(listSubItems.characters.count)/2.0) + 25
            
        } else if orderItem.unityItem != nil {
            if orderItem.unityItem?.desc != nil {
                listSubItems = (orderItem.unityItem?.desc)!
                heightCell += CGFloat(CGFloat((orderItem.unityItem?.desc!.characters.count)!)/2.0) + 15
            }
        }
        
        return (heightCell,listSubItems)
    }
}

extension TrackingViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
            case 0:
                return 90
            case 1:
                return 55
            case 2:
                if order.idOrderType == OrderType.Delivery.rawValue || order.idOrderType == OrderType.TakeAway.rawValue  {
                    return 70
                }
                else {
                    return 0
                }
            case 3:
                if order.idOrderType == OrderType.Delivery.rawValue {
                    return 160
                }
                else {
                    return 100
                }
            case 4:
                return informationDetail((order.orderItems[indexPath.row])).heightText
            case 5:
                if order.idOrderType == OrderType.Delivery.rawValue {
                    return 110
                }
                else {
                    return 70
            }
            default:
                return 0
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            layoutHeaderView("Status")
            return viewHeader
        }
         
        if section == 4 {
            layoutHeaderItemsView("Itens do carrinho")
            return viewHeaderItems
        }
        else {
            let view = UIView()
            view.backgroundColor = viewHeader.backgroundColor
            return view
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
            case 1:
                return 70
            case 2:
                if order.idOrderType == OrderType.Delivery.rawValue || order.idOrderType == OrderType.TakeAway.rawValue  {
                    return 15
                }
                else {
                    return CGFloat.min
                }
            case 3:
                return 15
            case 4:
                return 70
            default:
                return CGFloat.min

        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
}

extension TrackingViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("NumberAndDeliveryTableViewCell") as! NumberAndDeliveryTableViewCell
            cell.labelTitle.text = "Número do pedido"
            cell.labelDescription.text = "#" + String(order.getIdentificator())
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("OrderStatusTableViewCell") as! OrderStatusTableViewCell 

            if indexPath.row == 0 {
                cell.viewLineUp.hidden = true
                cell.labelStatus.text = StatusDescription.Sent.rawValue
                
                if checkStatus(StatusOrder.Sent.rawValue) {
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.waiting = true
                    cell.labelTime.text = getTime(StatusOrder.Sent.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                }
                
                if checkStatus(StatusOrder.Preparing.rawValue) == true || checkStatus(StatusOrder.Finished.rawValue) == true || checkStatus(StatusOrder.Delivered.rawValue) == true || checkStatus(StatusOrder.ToDelivery.rawValue){
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.done = true
                    cell.labelTime.text = getTime(StatusOrder.Sent.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                    
                }
                
                if checkStatus(StatusOrder.Canceled.rawValue) && cell.done == false  && cell.waiting == true {
                    cell.labelStatus.textColor = UIColor.redColor()
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_cancel")
                    cell.imageViewStatus.tintImage(UIColor.redColor())
                    cell.labelStatus.text = StatusDescription.Canceled.rawValue
                    cell.labelTime.text = getTime(StatusOrder.Canceled.rawValue)
                    cell.labelTime.textColor = UIColor.redColor()
                }
            }
            
            if indexPath.row == 1 {
                cell.viewLineUp.hidden = false
                cell.labelStatus.text = StatusDescription.Preparing.rawValue
                
                if checkStatus(StatusOrder.Sent.rawValue) == true && checkStatus(StatusOrder.Preparing.rawValue) == true {
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.waiting = true
                    cell.labelTime.text = getTime(StatusOrder.Preparing.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                    
                }
                if checkStatus(StatusOrder.Finished.rawValue) == true || checkStatus(StatusOrder.Delivered.rawValue) == true || checkStatus(StatusOrder.ToDelivery.rawValue){
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.done = true
                    cell.labelTime.text = getTime(StatusOrder.Preparing.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                }
                if checkStatus(StatusOrder.Canceled.rawValue) && cell.done == false && cell.waiting == true{
                    cell.labelStatus.textColor = UIColor.redColor()
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_cancel")
                    cell.imageViewStatus.tintImage(UIColor.redColor())
                    cell.labelStatus.text = StatusDescription.Canceled.rawValue
                    cell.labelTime.text = getTime(StatusOrder.Canceled.rawValue)
                    cell.labelTime.textColor = UIColor.redColor()
                }
            }
            
            if indexPath.row == 2 {
                if order.idOrderType == OrderType.Delivery.rawValue {
                    cell.labelStatus.text = StatusDescription.ToDelivery.rawValue
                }
                else {
                    cell.labelStatus.text = StatusDescription.Finished.rawValue
                }
                
                if (checkStatus(StatusOrder.Finished.rawValue) == true || checkStatus(StatusOrder.ToDelivery.rawValue)){
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.imageViewStatus.layer.borderWidth  = 0.0
                    cell.waiting = true
                    cell.labelTime.text = getTime(StatusOrder.Finished.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                }
                if checkStatus(StatusOrder.Delivered.rawValue) == true{
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.imageViewStatus.layer.borderWidth  = 0.0
                    cell.done = true
                    cell.labelTime.text = getTime(StatusOrder.Finished.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                }
                if checkStatus(StatusOrder.Canceled.rawValue) && cell.done == false && cell.waiting == true{
                    cell.labelStatus.textColor = UIColor.redColor()
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_cancel")
                    cell.imageViewStatus.tintImage(UIColor.redColor())
                    cell.labelStatus.text = StatusDescription.Canceled.rawValue
                    cell.labelTime.text = getTime(StatusOrder.Canceled.rawValue)
                    cell.labelTime.textColor = UIColor.redColor()
                }
            }
            
            if indexPath.row == 3 {
                cell.viewLineDown.hidden = true
                cell.labelStatus.text = StatusDescription.Delivered.rawValue
                
                if checkStatus(StatusOrder.Delivered.rawValue) {
                    cell.labelStatus.textColor = LayoutManager.green4all
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_done")
                    cell.imageViewStatus.tintImage(LayoutManager.green4all)
                    cell.imageViewStatus.layer.borderWidth  = 0.0
                    cell.done = true
                    cell.labelTime.text = getTime(StatusOrder.Delivered.rawValue)
                    cell.labelTime.textColor = LayoutManager.green4all

                }
                if checkStatus(StatusOrder.Canceled.rawValue) && cell.done == false && cell.waiting == true{
                    cell.labelStatus.textColor = UIColor.redColor()
                    cell.imageViewStatus.image     = UIImage(named: "ic_order_cancel")
                    cell.imageViewStatus.tintImage(UIColor.redColor())
                    cell.labelStatus.text = StatusDescription.Canceled.rawValue
                    cell.labelTime.text = getTime(StatusOrder.Canceled.rawValue)
                    cell.labelTime.textColor = UIColor.redColor()

                }
            }
            return cell

        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("NumberAndDeliveryTableViewCell") as! NumberAndDeliveryTableViewCell
            cell.labelTitle.text = "Previsão de entrega"
            if order.idOrderType == OrderType.Delivery.rawValue {
                let unity = OrderEntityManager.sharedInstance.getOrderUnity(self.order.idUnity)
                let date = Util.timeStampToNSDate(order.timestamp).dateByAddingTimeInterval((unity?.deliveryEstimatedTime)! * 60)
                cell.labelDescription.text = Util.getFormatedDateString(date, pattern: "HH:mm")
            }
            else if order.idOrderType == OrderType.TakeAway.rawValue {
                let unity = OrderEntityManager.sharedInstance.getOrderUnity(self.order.idUnity)
                let date = Util.timeStampToNSDate(order.timestamp).dateByAddingTimeInterval((unity?.takeAwayEstimatedTime)! * 60)
                cell.labelDescription.text = Util.getFormatedDateString(date, pattern: "HH:mm")
            }
            else {
                cell.labelTitle.hidden = true
                cell.labelDescription.hidden = true
                cell.hidden = true
            }
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("AddressTableViewCell") as! AddressTableViewCell
            if order.idOrderType == OrderType.Delivery.rawValue {
                cell.labelTitle.text = "Endereço de entrega"
                let address = order.address
                var textStreet = (address?.street)! + ", " + (address?.number)!
                if address?.complement != "" {
                    textStreet = textStreet + (" / ") + (address?.complement)!
                }
                var textCity = ""
                if let city = address?.city {
                    textCity = city
                    if let state = address?.state {
                        textCity = textCity + " / " + state
                    }
                }
                let textZip = "CEP " + (address?.zip)!
                let textAddress = textStreet + ("\n") + textCity + ("\n") + textZip
                
                cell.labelAddress.text = textAddress

            }
            else {
                cell.labelTitle.text = "Local de entrega"
                cell.labelAddress.text = order.placeLabel

            }
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier("ItemsTrackingTableViewCell") as! ItemsTrackingTableViewCell
            let item = (order.orderItems[indexPath.row])
            let idUnity = order.idUnity
            let unity = OrderEntityManager.sharedInstance.getOrderUnity(idUnity)
            cell.populateCell(item, description: informationDetail(item).text, rootVc: self, unity: unity!)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCellWithIdentifier("TotalOrderTableViewCell") as! TotalOrderTableViewCell
            cell.labelTotalText.text = "Total"
            cell.labelPrice.text = Util.formatCurrency((self.order.total)/100)
            if order.idOrderType == OrderType.Delivery.rawValue {
                cell.labelPriceDelivery.text = Util.formatCurrency((self.order.deliveryFee)/100)
                cell.labelPriceDelivery.hidden = false
                cell.labelDelivery.hidden = false
            }
            else {
                cell.labelPriceDelivery.hidden = true
                cell.labelDelivery.hidden = true
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 4
        }
        else if section == 4 {
            return order.orderItems.count
        }
        else {
            return 1
        }
    }

    func checkStatus(idOrderStatus : Int) -> Bool{
        for item in orderStatus {
            if let status = item as? Dictionary<String, AnyObject> {
                if status["idOrderStatus"]!.integerValue == idOrderStatus {
                    return true
                }
            }
            
        }
        
        return false
    }
    
    func getTime(idOrderStatus : Int) -> String{
        for item in orderStatus {
            if let status = item as? Dictionary<String, AnyObject> {
                if status["idOrderStatus"]!.integerValue == idOrderStatus {
                    let date = Util.timeStampToNSDate(Double(status["timestampCreation"]!.integerValue))
                    return Util.getFormatedDateString(date, pattern: "dd/MM   HH:mm")
                }
            }
            
        }
        return ""
    }
}
