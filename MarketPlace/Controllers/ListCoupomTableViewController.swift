//
//  ListCoupomTableViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 29/09/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ListCoupomTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableViewListCoupom: UITableView!
    
    var listCoupons : [NSDictionary] = []
    var listCampaigns : [Campaign]! = []
    var pullRefresh: UIRefreshControl!
    let lib4all = Lib4all()
    let api = ApiServices()
    var typeShow: TypeShowListCoupons! = TypeShowListCoupons.allCupons
    var listIdsAllCoumpons = ""
    
    var preferredStatusBar = UIStatusBarStyle.Default
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.preferredStatusBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Util.adjustTableViewInsetsFromTarget(self)

        CartCustomManager.sharedInstance.hideCart()
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableViewListCoupom.addSubview(pullRefresh)
        
        if typeShow == TypeShowListCoupons.allCupons {
            self.navigationItem.title = kCouponsTitle
                self.populateCampaigns()
        } else {
            self.navigationItem.title = kMyCouponsTitle
                self.populateCoupons()
        }
        
        self.tableViewListCoupom.registerNib(UINib(nibName: "ListCoupomTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_COUPONS)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        CartCustomManager.sharedInstance.hideCart()
        if typeShow == TypeShowListCoupons.allCupons {
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenCoupons)
        } else {
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMyCoupons)
        }
        
        self.refresh()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.checkWithoutData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Cheking without data
    private func checkWithoutData() {
        if typeShow == TypeShowListCoupons.meCoupons {
            Util.checkWithoutData(self.view, tableView: self.tableViewListCoupom, countData: self.listCoupons.count, message: kMessageWithoutDataCoupons)
        }
    }
    
    func refresh() {
        
        if typeShow == TypeShowListCoupons.allCupons {
            self.getCampaignFromServer()
        } else {
            if lib4all.hasUserLogged() {
                self.getAllCouponsByUser()
            } else {
                self.pullRefresh?.endRefreshing()
            }
        }
    }
    
    private func populateCampaigns() {
        self.listCampaigns.removeAll()
        self.listCampaigns.appendContentsOf(CampaignPersitence.getCampaingsByActiveUnitiesWithIDs(self.listIdsAllCoumpons))
        self.orderCampaign()
    }
    
    //MARK: List only active campaigns
    func orderCampaign() {
        
        var listCampaingOrdered = [Campaign]()
        
        for item in self.listCampaigns {
            
            let campaign = Campaign(value: item)
            let dateBeginClaim = NSString.getDateByServerConvertToShow(campaign.dateBeginClaim!)
            let dateEndClaim = NSString.getDateByServerConvertToShow(campaign.dateEndClaim!)
            let dateToday = NSDate.getDateTodayToFormat()
            let couponAvailable = (campaign.couponLimit - campaign.couponClaimed)
            
            let secondsDifferenceToBegin = NSDate.secondsByDifferenceDatesWithFormat(dateToday, dateStringFinish:dateBeginClaim, format: "dd/MM/yyyy")
            let secondsDifferenceToEnd = NSDate.secondsByDifferenceDatesWithFormat(dateToday, dateStringFinish: dateEndClaim, format: "dd/MM/yyyy")
            
            if (secondsDifferenceToBegin >= 0 && secondsDifferenceToEnd <= 0 && couponAvailable > 0) {
                listCampaingOrdered.append(campaign)
            }
        }
        
        self.listCampaigns = listCampaingOrdered
        self.pullRefresh.endRefreshing()
        self.tableViewListCoupom.reloadData()
    }
    
    //MARK: - Request Coupom
    private func getAllCouponsByUser(){
        
        api.successCase = {(obj) in
            if (obj as? NSArray) != nil {
                self.populateCoupons()
            }
            self.tableViewListCoupom.reloadData()
            self.pullRefresh?.endRefreshing()
            self.checkWithoutData()
        }
        
        api.failureCase = {(msg) in
            self.pullRefresh?.endRefreshing()
        }
        
        //Get token by user loged
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    api.getCoupomBySessionToken(user.token)
                }
            }
        }
    }
    
    //MARK: - Request Campaign
    func getCampaignFromServer() {
        
        api.successCase = {(obj) in
            if obj != nil {
                let listCampaignReceived = obj as! NSArray
                CampaignPersitence.importFromArray(listCampaignReceived)
                
                if listCampaignReceived.count > 0 {
                    var listIdsReceived = "\(String(listCampaignReceived[0].objectForKey("id")!))"
                    for item in listCampaignReceived {
                        if listIdsReceived.characters.count > 0 {
                            listIdsReceived += "," + String(item.objectForKey("id")!)
                        }
                    }
                    self.listIdsAllCoumpons = listIdsReceived
                }
                
                self.populateCampaigns()
                self.pullRefresh?.endRefreshing()
            }
        }
        
        api.failureCase = {(msg) in
            self.pullRefresh.endRefreshing()
        }
        
        api.getAllCampaigns()
    }
    
    //MARK: - Populate coupons
    private func populateCoupons(){
        
        self.listCoupons.removeAll()
        
        let listCoupons = CoupomPersistence.getAllLocalCoupons()
        if listCoupons.count > 0 {
            for coupom in listCoupons {
                
                let campaign = CampaignPersitence.getCampaignById(coupom.idCampaign)
                if campaign != nil {
                    let dictionaryDataCoupom: NSDictionary = ["coupom": coupom,"campaign":campaign!]
                    self.listCoupons.append(dictionaryDataCoupom)
                } else {
                    
                    api.successCase = {(obj) in
                        
                        if obj != nil {
                            do {
                                if let newObj = obj {
                                    if newObj is NSArray {
                                        for objCamp in (newObj as! NSArray) {
                                            let campaignReceived = Campaign(value: objCamp)
                                            
                                            let coupomUpdated = CoupomPersistence.getCoupomByIdCampaign(campaignReceived.id)
                                            let campaign = CampaignPersitence.getCampaignById(campaignReceived.id)
                                            
                                            if coupomUpdated != nil && campaign != nil {
                                                let dictionaryDataCoupom: NSDictionary = ["coupom": coupomUpdated!,"campaign":campaign!]
                                                self.listCoupons.append(dictionaryDataCoupom)
                                            }
                                        }
                                        
                                    } else {
                                        let campaignReceived = Campaign(value: obj!)
                                        let coupomUpdated = CoupomPersistence.getCoupomByIdCampaign(campaignReceived.id)
                                        let campaign = CampaignPersitence.getCampaignById(campaignReceived.id)
                                        
                                        let dictionaryDataCoupom: NSDictionary = ["coupom": coupomUpdated!,"campaign":campaign!]
                                        self.listCoupons.append(dictionaryDataCoupom)
                                    }
                                    self.tableViewListCoupom.reloadData()
                                    self.checkWithoutData()
                                }
                            }
                        }
                        self.pullRefresh?.endRefreshing()
                    }
                    
                    api.failureCase = {(msg) in
                        self.pullRefresh?.endRefreshing()
                    }
                    
                    api.getCampaignByID(0, idCampaign: coupom.idCampaign)
                }
            }
        } else {
            self.getAllCouponsByUser()
        }
    }
    
    //MARK: - TableView methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if typeShow == TypeShowListCoupons.allCupons {
            return self.listCampaigns.count
        } else {
            return self.listCoupons.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let countItens = typeShow == TypeShowListCoupons.allCupons ? self.listCampaigns.count : self.listCoupons.count
        
        if indexPath.row == 0 || indexPath.row == countItens-1 {
            return 220
        } else {
            return 200
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: ListCoupomTableViewCell = tableView.dequeueReusableCellWithIdentifier(CELL_COUPONS, forIndexPath: indexPath) as! ListCoupomTableViewCell
        
        let campaign = (typeShow == TypeShowListCoupons.allCupons ? self.listCampaigns[indexPath.row] : (self.listCoupons[indexPath.row].objectForKey("campaign") as! Campaign))
        cell.populate(campaign, type:typeShow)
        
        if indexPath.row == 0 {
            cell.constraintDistanceTopCell.constant = 40.0
            cell.constraintDistanceBottomCell.constant = 20.0
        } else if indexPath.row == self.listCoupons.count-1 {
            cell.constraintDistanceTopCell.constant = 20.0
            cell.constraintDistanceBottomCell.constant = 40.0
        } else {
            cell.constraintDistanceTopCell.constant = 20.0
            cell.constraintDistanceBottomCell.constant = 20.0
        }
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        LayoutManager.roundBottomRightAndTopRightCornersRadius(cell.viewBase, radius: 10.0)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        let campaign = (typeShow == TypeShowListCoupons.allCupons ? self.listCampaigns[indexPath.row] : (self.listCoupons[indexPath.row].objectForKey("campaign") as! Campaign))
        detailsVc.title = kDiscountCouponTitle
        detailsVc.idObjectReceived = campaign.id
        detailsVc.typeObjet = TypeObjectDetail.Campaign
        
        self.navigationController?.pushViewController(detailsVc, animated: true)
    }
}
