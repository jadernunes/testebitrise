//
//  SectionMenuView.swift
//  MarketPlace
//
//  Created by Luca Schifino on 11/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class SectionMenuView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTitle.textColor = UIColor.darkGray4all()
        self.viewSeparator.backgroundColor = UIColor.green4all()
        self.backgroundColor = UIColor.lightGray4all()
    }
    
    class func instanceFromNib() -> SectionMenuView {
        return UINib(nibName: "SectionMenuView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! SectionMenuView
    }

}
