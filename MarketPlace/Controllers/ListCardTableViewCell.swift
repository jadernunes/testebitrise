//
//  ListCardTableViewCell.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ListCardTableViewCell: UITableViewCell {

    @IBOutlet weak var lbNameCard: UILabel!
    @IBOutlet weak var iconSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func populateData(dic: NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            self.lbNameCard.text =  dic["name"] as? String
        }
    }
    
    func markSelected(){
        
    }
}
