//
//  TableSearchResults.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import MapKit

class TableSearchResults: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var listGeneralUnities: [ItemSearched]?
    var superViewController: UIViewController?
    
    init(listUnities: [ItemSearched]?, superController: UIViewController?) {
        super.init()
        self.listGeneralUnities = listUnities
        self.superViewController = superController
    }
    
    private func callDetails(item: ItemSearched?, group: Group?) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        if (item != nil) {
            detailsVc.title = item?.title
            detailsVc.idObjectReceived = item?.id
            detailsVc.typeObjet = TypeObjectDetail.Store
        } else {
            detailsVc.title = group?.name
            detailsVc.idObjectReceived = group?.id
            detailsVc.typeObjet = TypeObjectDetail.Group
        }
        
        if let superVC = self.superViewController {
            superVC.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
    
    @objc func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 92.0
    }
    
    @objc func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let list = self.listGeneralUnities {
            let unity = list[indexPath.row]
            self.callDetails(unity, group: nil)
        }
    }
    
    @objc func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = listGeneralUnities {
            return list.count
        }
        
        return 0
    }
    
    @objc func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("NewSearchResultTableViewCell", forIndexPath: indexPath) as! NewSearchResultTableViewCell
        
        if let list = self.listGeneralUnities {
            dispatch_async(dispatch_get_main_queue(), {
                let item = list[indexPath.row]
                
                cell.labelNameUnity.text = item.title
                cell.imageViewLogoUnity.addImage(item.logo)
            })
        }
        return cell
    }
}
