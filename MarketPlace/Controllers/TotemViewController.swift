//
//  TotemViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import CoreLocation

class TotemViewController: UIViewController, ARDataSource, CLLocationManagerDelegate
{
    let locationManager = CLLocationManager()
    var latitude = Double()
    var longitude = Double()
    var viewController : UIViewController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

    }
    
    func showARViewController()
    {
        // Check if device has hardware needed for augmented reality
        let result = ARViewController.createCaptureSession()
        if result.error != nil
        {
            let message = result.error?.userInfo["description"] as? String
            let alertView = UIAlertView(title: kAlertTitle, message: message, delegate: nil, cancelButtonTitle: kClose)
            alertView.show()
            return
        }
        
        // Create random annotations around center point    //@TODO
        //FIXME: set your initial position here, this is used to generate random POIs
        //let annotations = self.fetchPlacesNearCoordinate(CLLocationCoordinate2DMake(lat, lon), placeType: placeType)
        //let annotations = self.getDummyAnnotations(centerLatitude: TotemViewController.latitude, centerLongitude:TotemViewController.longitude, delta: 0.05, count: 30)
        
        // Present ARViewController
        let arViewController = ARViewController()
        arViewController.debugEnabled = false
        arViewController.dataSource = self
        arViewController.maxDistance = 3000
        arViewController.maxVisibleAnnotations = 100
        arViewController.maxVerticalLevel = 5
        arViewController.headingSmoothingFactor = 0.05
        arViewController.trackingManager.userDistanceFilter = 2
        arViewController.trackingManager.reloadDistanceFilter = 75
        arViewController.latitude = self.latitude
        arViewController.longitude = self.longitude
        
        let annotations = self.getAnnotationsFromUnitiesLocation()
        arViewController.setAnnotations(annotations)
        arViewController.reloadAnnotations()
        
        let navController = UINavigationController.init(rootViewController: arViewController)
        
        
        if let vc = viewController {
            vc.presentViewController(navController, animated: true, completion: nil)
        }
        else {
            HomeTableViewController.rootVc.presentViewController(navController, animated: true, completion: nil)
        }
        
       //HomeTableViewController.rootVc.navigationController?.presentViewController(arViewController, animated: true,completion: nil)
        //HomeTableViewController.rootVc.presentViewController(arViewController, animated: true, completion: nil)
    }
    
    func createAnnotations(centerLatitude centerLatitude: Double, centerLongitude: Double, delta: Double, count: Int) -> Array<ARAnnotation> {
        var annotations: [ARAnnotation] = []
        var myDict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Spots", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        if let dict = myDict {
            let places = dict.objectForKey("PlacesList") as! [NSDictionary]
            for place in places {
                let annotation = ARAnnotation()
                let coordinates = place.objectForKey("coordinates") as! NSDictionary
                annotation.location = CLLocation(latitude: coordinates.objectForKey("latitude") as! Double, longitude: coordinates.objectForKey("longitude") as! Double)
                annotation.title = place.objectForKey("description") as? String
                annotations.append(annotation)
            }
        }
        
        return annotations
    }
    /// This method is called by ARViewController, make sure to set dataSource property.
    func ar(arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView
    {
        // Annotation views should be lightweight views, try to avoid xibs and autolayout all together.
        let distance = viewForAnnotation.distanceFromUser
        let annotationView = PromotionAnnotationView()
        var constant: Double = 0.0
        if distance <= 200 {
            constant = 1.15
            annotationView.alphaConstant = 0.5
        }
        if distance <= 1000 && distance > 200 {
            constant = 1
            annotationView.alphaConstant = 0.6
        }
        if distance <= 2000 && distance > 1000 {
            constant = 0.9
            annotationView.alphaConstant = 0.7
        }
        if distance <= 3000 && distance > 2000 {
            constant = 0.75
            annotationView.alphaConstant = 0.75
        }
        if distance > 3000 {
            constant = 0.7
            annotationView.alphaConstant = 0.8
        }
        
        annotationView.frame = CGRect(x: 0,y: 0,width: 150*constant,height: 50*constant)
        
        return annotationView;
    }
    
    private func getDummyAnnotations(centerLatitude centerLatitude: Double, centerLongitude: Double, delta: Double, count: Int) -> Array<ARAnnotation>
    {
        var annotations: [ARAnnotation] = []
        
        srand48(3)
        for i in 0.stride(to: count, by: 1)
        {
            let annotation = ARAnnotation()
            annotation.location = self.getRandomLocation(centerLatitude: centerLatitude, centerLongitude: centerLongitude, delta: delta)
            annotation.title = "POI \(i)"
            annotations.append(annotation)
        }
        return annotations
    }
    
    
    private func getAnnotationsFromUnitiesLocation() -> Array<ARAnnotation>
    {
        var annotations: [ARAnnotation] = []
        let unities = FactEntityManager.getUnityIDsWithPromotion()
        for item in unities
        {
            let annotation = ARAnnotation()
            annotation.location = CLLocation(latitude: (item as! Unity).latitude,longitude: (item as! Unity).longitude)
            annotation.title = (item as! Unity).name
            annotation.unityId = (item as! Unity).id
            annotations.append(annotation)
        }
        return annotations
    }
    
    @IBAction func closeVC(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    private func getRandomLocation(centerLatitude centerLatitude: Double, centerLongitude: Double, delta: Double) -> CLLocation
    {
        var lat = centerLatitude
        var lon = centerLongitude
        
        let latDelta = -(delta / 2) + drand48() * delta
        let lonDelta = -(delta / 2) + drand48() * delta
        lat = lat + latDelta
        lon = lon + lonDelta
        return CLLocation(latitude: lat, longitude: lon)
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
