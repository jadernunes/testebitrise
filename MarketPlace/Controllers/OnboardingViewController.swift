//
//  OnboardingViewController.swift
//  Donna
//
//  Created by Rodrigo Kreutz on 2/15/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.pagingEnabled = true
            collectionView.showsVerticalScrollIndicator = false
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    @IBOutlet weak var pageControl: UIPageControl! {
        didSet {
            pageControl.pageIndicatorTintColor = onboardingContentPages[0].colorTitle.colorWithAlphaComponent(0.2)
            pageControl.currentPageIndicatorTintColor = onboardingContentPages[0].colorTitle
            pageControl.numberOfPages = onboardingContentPages.count
            pageControl.userInteractionEnabled = false
        }
    }
    
    var superViewController: NewHomeViewController?

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension OnboardingViewController: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onboardingContentPages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: OnboardingContentCollectionViewCell = { cell in
            cell.configureWithContent(onboardingContentPages[indexPath.item])
            cell.delegate = self
            return cell
        }(collectionView.dequeueReusableCellWithReuseIdentifier(.onboardingContentCollectionViewCellIdentifier, forIndexPath: indexPath) as! OnboardingContentCollectionViewCell)
        
        if indexPath.row == 1 {
            let applicationFrame = UIScreen.mainScreen().applicationFrame
            Util.setViewWithGradient(cell.contentView, frame: applicationFrame)
        }
        
        if Lib4all.sharedInstance().hasUserLogged() && indexPath.row == onboardingContentPages.count - 1 {
            cell.buttonLoginWithoutSession.hidden = false
        }
        
        return cell
    }
}

extension OnboardingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension OnboardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetX = collectionView.contentOffset.x
        
        // Page animation (translation and alpha)
        for cell in collectionView.visibleCells().flatMap({ return ($0 as! OnboardingContentCollectionViewCell) }) {
            var transform: CATransform3D = CATransform3DIdentity
            var alpha: CGFloat = 1.0
            
            if offsetX > 0 && offsetX < collectionView.contentSize.width - collectionView.frame.width {
                let translation = offsetX - cell.frame.minX
                transform = CATransform3DMakeTranslation(translation, 0.0, 0.0)
                
                transform = CATransform3DScale(transform, 1.0 - (abs(translation / cell.frame.width) / 1), 1.0 - (abs(translation / cell.frame.width) / 1), 1.0)
                alpha = max(1 - 1.75 * (abs(translation) / (collectionView.frame.width)), 0.0)
            }
            
            cell.applyTransform(transform)
            cell.applyAlpha(alpha)
        }
        
        // Page control animation (index and tint color)
        
        if let centerIndex = collectionView.indexPathForItemAtPoint(CGPoint(x: collectionView.center.x + offsetX, y: collectionView.center.y)) {
            pageControl.currentPage = centerIndex.item
            pageControl.currentPageIndicatorTintColor = onboardingContentPages[centerIndex.item].colorTitle
            pageControl.pageIndicatorTintColor = onboardingContentPages[centerIndex.item].colorTitle.colorWithAlphaComponent(0.2)
        }
    }
}

extension OnboardingViewController: OnboardingContentCollectionViewCellDelegate {
    func skipOnboarding() {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: .onboardingPresentedUserDefaultsKey)
        self.dismissViewControllerAnimated(true) {
            self.superViewController?.skipOnboarding()
        }
    }
    
    func signup(){
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: .onboardingPresentedUserDefaultsKey)
        self.dismissViewControllerAnimated(true) {
            self.superViewController?.signup()
        }
    }
    
    func signin(){
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: .onboardingPresentedUserDefaultsKey)
        self.dismissViewControllerAnimated(true) {
            self.superViewController?.signin()
        }
    }
}
