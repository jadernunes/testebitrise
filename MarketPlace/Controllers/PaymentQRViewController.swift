//
//  PaymentQRViewController.swift
//  MarketPlace
//
//  Created by Luciano on 8/25/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD

class PaymentQRViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var labelTitle                       : UILabel!
    @IBOutlet weak var labelTitleAmount                 : UILabel!
    @IBOutlet weak var labelMerchantName                : UILabel!
    @IBOutlet weak var labelAmount                      : UILabel!
    @IBOutlet weak var labelServiceName                 : UILabel!
    @IBOutlet weak var labelDate                        : UILabel!
    @IBOutlet weak var imageStore                       : UIImageView!
    @IBOutlet weak var containerPayment                 : UIView!
    @IBOutlet weak var viewCoupom                       : UIView!
    @IBOutlet weak var constraintHeightViewCoupom       : NSLayoutConstraint!
    @IBOutlet weak var labelDiscountCoupom              : UILabel!
    @IBOutlet weak var labelTitleNewPriceToPay          : UILabel!
    @IBOutlet weak var viewNewPriceToPay                : UIView!
    @IBOutlet weak var labelNewPriceToPay               : UILabel!
    @IBOutlet weak var constraintHeightNewPriceToPay    : NSLayoutConstraint!
    @IBOutlet weak var navigationBarCustom              : UINavigationBar!
    @IBOutlet weak var navigationButtonBar              : UIBarButtonItem!
    @IBOutlet weak var buttonConfirmCancelPayment       : Button4all!
    
    //MARK: - Variables
    var previousVC                  : UIViewController?
    var strMerchantName             :String? = ""
    var strAmount                   = ""
    var strTransactionId            = ""
    var merchantCNPJ                = ""
    var typePayment                 = ""
    var idObjectPayment             = ""
    var campaign                    :Campaign!
    var coupom                      :Coupom!
    var useCoupom                   = false
    let loading                     = JGProgressHUD(style: .Dark)
    var component                   : ComponentViewController!
    var isCancelPayment             = false
    private var customAmount        : String!
    
    //MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)
        CartCustomManager.sharedInstance.hideCart()
        
        self.hidesBottomBarWhenPushed = true
        
        if idObjectPayment != "" {
            self.campaign = CampaignPersitence.getCampaignByUUID(idObjectPayment)
            if self.campaign != nil {
                self.coupom = CoupomPersistence.getCoupomByIdCampaign(self.campaign.id)
            }
        }
        
        initialConfiguration()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureLayoutComponents()
        addCoupomToPayment()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom methods
    func initialConfiguration() {
        
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        
        //Label Title
        labelTitle.font         = LayoutManager.primaryFontWithSize(20)
        labelTitle.textColor    = SessionManager.sharedInstance.cardColor
        
        //Label TitleAmount
        labelTitleAmount.font         = LayoutManager.primaryFontWithSize(14)
        labelTitleAmount.textColor    = LayoutManager.labelContrastColor
        
        //Label Amount
        labelAmount.font        = LayoutManager.primaryFontWithSize(65)
        labelAmount.textColor    = SessionManager.sharedInstance.cardColor
        
        //Label Merchant Name
        labelMerchantName.font      = LayoutManager.primaryFontWithSize(14)
        labelMerchantName.textColor = LayoutManager.labelContrastColor
        
        //Label Merchant Name
        labelDate.font        = LayoutManager.primaryFontWithSize(14)
        labelDate.textColor   = SessionManager.sharedInstance.cardColor
        
        //Label Service Name
        labelServiceName.font        = LayoutManager.primaryFontWithSize(12)
        labelServiceName.textColor   = LayoutManager.labelContrastColor
        
        labelMerchantName.text      = strMerchantName!
        
        labelDate.text              = Util.getFormatedDateString(NSDate(), pattern: "dd/MM/yy")
        
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.title = kPaymentQRTitle
        
        if strAmount.characters.count > 0 {
            let newStrAmount = Util.removerCharacters(strAmount)
            customAmount = newStrAmount
        }
        
        var urlImage = ""
        let unity = UnityPersistence.getUnityByName(strMerchantName!)
        if(unity != nil) {
            if unity?.getLogo() != nil {
                urlImage = (unity!.getLogo()!.value)!
            }
        }
        
        self.imageStore.addImage(urlImage) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imageStore.contentMode      = .ScaleAspectFill
            }
        }
    }
    
    /**
     Configure all layout items
     
     */
    func configureLayoutComponents() {
        
        self.containerPayment.hidden = true
        self.buttonConfirmCancelPayment.hidden = true
        
        self.labelTitleNewPriceToPay.textColor = SessionManager.sharedInstance.cardColor
        self.labelNewPriceToPay.textColor = SessionManager.sharedInstance.cardColor
        self.labelTitleNewPriceToPay.backgroundColor = UIColor.clearColor()
        self.labelNewPriceToPay.backgroundColor = UIColor.clearColor()
        self.viewNewPriceToPay.backgroundColor = UIColor.clearColor()
        self.constraintHeightNewPriceToPay.constant = 0
        self.viewCoupom.layer.masksToBounds = false
        self.viewCoupom.layer.cornerRadius = 10.0
        viewCoupom.layer.shadowOffset = CGSizeMake(0, 20);
        viewCoupom.layer.shadowRadius = 15;
        viewCoupom.layer.shadowOpacity = 0.3;
        
        if isCancelPayment {
            labelTitle.text = "CONFIRMA CANCELAMENTO?"
        } else {
            labelTitle.text = "CONFIRMA PAGAMENTO?"
        }
        
        self.constraintHeightViewCoupom.constant = 0
        
        if (typePayment == TypeQRPayment.Campaign.rawValue) {
            if self.coupom != nil {
                
                if self.coupom.redeemed == false {
                    self.constraintHeightViewCoupom.constant = 80
                    self.viewCoupom.layer.cornerRadius = 10.0
                    
                    if campaign.idCampaignDiscountType == TypeDiscountCampaign.price.rawValue {
                        let discount = Util.formatCurrency(Double(campaign.discount))
                        self.labelDiscountCoupom.text = discount
                    } else {
                        self.labelDiscountCoupom.text = String("\(campaign.discount) %")
                    }
                } else {
                    
                }
            }
        }
        
        self.navigationBarCustom.translucent = false
        self.navigationBarCustom.barTintColor = LayoutManager.green4all
        self.navigationButtonBar.tintColor = LayoutManager.white
        
        var isLoggedIn = false
        
        if Lib4all().hasUserLogged() {
            isLoggedIn = true
        }
        
        if !isLoggedIn || (isLoggedIn && !self.isCancelPayment) {
            self.containerPayment.hidden = false
            
            self.component = ComponentViewController()
            self.component.delegate = self
            self.component.buttonTitleWhenNotLogged = kEnter
            self.component.buttonTitleWhenLogged  = kPay
            
            dispatch_async(dispatch_get_main_queue()) {
                UIView.animateWithDuration(0.5, animations: {
                    self.component.view.frame = self.containerPayment.bounds
                    self.containerPayment.addSubview(self.component.view)
                    self.addChildViewController(self.component)
                    self.component.didMoveToParentViewController(self)
                    self.component.view.layoutIfNeeded()
                })
            }
        } else {
            self.buttonConfirmCancelPayment.hidden = false
            self.buttonConfirmCancelPayment.setTitle(kCancelPayment, forState: UIControlState.Normal)
        }
    }
    
    //MARK: - Events handler
    
    @IBAction func buttonCancelPaymentTouchDown(sender: AnyObject) {
        if let sessionToken = Lib4all().getAccountData()["sessionToken"] as? String {
            self.cancelPayment(sessionToken)
        }
    }
    
}

//MARK: - AlertView Delegate

extension PaymentQRViewController: UIAlertViewDelegate{
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        self.dismissViewControllerAnimated(true, completion: {
            SessionManager.sharedInstance.lib4all.generateAndShowOfflineQrCode(self.previousVC, ec: self.strMerchantName, transactionId: self.strTransactionId, amount: Int32(self.customAmount)!)
        })
    }
}

//MARK: - 4all delegate

extension PaymentQRViewController: LoginDelegate {
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        SessionManager.sharedInstance.registerDevice()
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        if isCancelPayment {
            self.cancelPayment(sessionToken)
        } else {
            self.makePayment(sessionToken, cardId:cardId, paymentMode:paymentMode)
        }
    }
    
    //MARK: - Make payment
    /**
     Function to make a payment
     
     */
    func makePayment(sessionToken: String!, cardId: String!, paymentMode: PaymentMode){
        
        let api = ApiServices()
        api.successCase = { (obj) in
            
            let response = obj as! NSDictionary
            let status = response.objectForKey("status") as! Int
            var isShowFidelity = false
            
            if status == PAYMENT_CONFIRMED {
                
                //set coupom to redeemed
                CoupomPersistence.updateStatusCoupom(true, coupom: self.coupom)
                
                if let loyaltyData = response.objectForKey("loyaltyData") as? NSDictionary {
                    MyFidelityPersistence.importFromDictionary(loyaltyData)
                    isShowFidelity = true
                }
                
                var urlImage = ""
                let unity = UnityPersistence.getUnityByName(self.strMerchantName!)
                var idUnity = 0
                if(unity != nil) {
                    if unity?.getLogo() != nil {
                        urlImage = (unity!.getLogo()!.value)!
                    }
                    idUnity = (unity?.id)!
                } 
                
                var valueToShow = Double(self.customAmount)!/100
                if self.coupom != nil && self.useCoupom == true {
                    var newPrice = Double(self.customAmount)!
                    if self.campaign.idCampaignDiscountType == TypeDiscountCampaign.price.rawValue {
                        newPrice -= Double(self.campaign.discount)
                    } else {
                        let strDiscount = ((Double(self.campaign.discount)/100) * newPrice)
                        let discountValue = Double(strDiscount)
                        newPrice = newPrice - discountValue
                    }
                    
                    valueToShow = Double(newPrice/100)
                }
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                (vc.viewControllers[0] as! ReceiptViewController).labels = ["success":"PAGAMENTO REALIZADO!",
                                                                            "shop":self.strMerchantName!,
                                                                            "price": valueToShow,
                                                                            "date": Util.getFormatedDateString(NSDate(), pattern: "dd/MM/yy"),
                                                                            "time": Util.getFormatedDateString(NSDate(), pattern: "HH:mm"),
                                                                            "type": TypePageSuccess.PagamentoQR.rawValue,
                                                                            "title": kPaymentQRConfirmationTitle,
                                                                            "idUnity":idUnity,
                                                                            "isShowFidelity":isShowFidelity,
                                                                            "imageUrl":urlImage]
                
                (vc.viewControllers[0] as! ReceiptViewController).previousVC = self.previousVC
                self.loading.dismiss()
                self.presentViewController(vc, animated: true, completion: nil)
            } else {
                self.loading.dismiss()
                Util.showAlert(self, title: kAlertTitle, message: "Não foi possível efetuar o pagamento.")
            }
        }
        
        api.failureCase = { (msg) in
            
            self.loading.dismiss()
            let alert = UIAlertView.init(title: kAlertNoConnection, message: kMessagePaymentOffline, delegate: self, cancelButtonTitle: kAlertButtonOk)
            alert.show()
        }
        
        loading.showInView(self.view)
        
        if coupom != nil && useCoupom == true {
            api.call4allPayTransaction(sessionToken, transactionId: strTransactionId, cardId: cardId,amount: Int(customAmount)!,cnpj: merchantCNPJ,campaignUUID: String(self.campaign.uuid),couponUUID: String(self.coupom.uuid))
        }else {
            api.call4allPayTransaction(sessionToken, transactionId: strTransactionId, cardId: cardId,amount: Int(customAmount)!,cnpj: merchantCNPJ)
        }
    }
    
    //MARK: - Cancel payment
    /**
     Function to cancel a payment
     
     */
    func cancelPayment(sessionToken: String!) {
        
        let api = ApiServices()
        api.successCase = { (obj) in
            
            var urlImage = ""
            let unity = UnityPersistence.getUnityByName(self.strMerchantName!)
            if(unity != nil) {
                if unity?.getLogo() != nil {
                    urlImage = (unity!.getLogo()!.value)!
                }
            }
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
            (vc.viewControllers[0] as! ReceiptViewController).labels = ["success":"CANCELAMENTO REALIZADO!",
                                                                        "shop":self.strMerchantName!,
                                                                        "price": self.customAmount,
                                                                        "date": Util.getFormatedDateString(NSDate(), pattern: "dd/MM/yy"),
                                                                        "time": Util.getFormatedDateString(NSDate(), pattern: "HH:mm"),
                                                                        "type": TypePageSuccess.PagamentoCancelado.rawValue,
                                                                        "title": "CONFIRMAÇÃO DE CANCELAMENTO",
                                                                        "imageUrl":urlImage]
            
            (vc.viewControllers[0] as! ReceiptViewController).previousVC = self.previousVC
            self.loading.dismiss()
            self.presentViewController(vc, animated: true, completion: nil)
        }
        
        api.failureCase = { (msg) in
            self.loading.dismiss()
            Util.showAlert(self, title: "Atenção", message: msg)
        }
        
        loading.showInView(self.view)
        api.refundTransaction(sessionToken, transactionId: strTransactionId)
    }
    
    //MARK: - Use coupom
    private func addCoupomToPayment() {
        useCoupom = false
        
        if self.campaign != nil {
            if self.campaign.discount > 0 {
                
                useCoupom = true
                var newPrice = Double(customAmount)!
                if self.campaign.idCampaignDiscountType == TypeDiscountCampaign.price.rawValue {
                    
                    let discount = Util.formatCurrency(Double(self.campaign.discount / 100))
                    self.labelDiscountCoupom.text = discount
                    newPrice -= Double(self.campaign.discount)
                } else {
                    self.labelDiscountCoupom.text = String("\(self.campaign.discount) %")
                    
                    let strDiscount = ((Double(self.campaign.discount)/100) * newPrice)
                    let discountValue = Double(strDiscount)
                    
                    newPrice = newPrice - discountValue
                }
                
                self.labelAmount.text = Util.formatCurrency((newPrice/100))
                
                self.constraintHeightNewPriceToPay.constant = 50
                self.viewNewPriceToPay.layoutIfNeeded()
                self.labelAmount.layoutIfNeeded()
            }
        }
        
        if strAmount.characters.count > 0 {
            let newStrAmount = Util.removerCharacters(strAmount)
            let newAmountNumber = Double(newStrAmount)! / 100
            
            if newAmountNumber > 0 {
                if useCoupom == true {
                    labelNewPriceToPay.text = Util.formatCurrency(Double(newAmountNumber))
                } else {
                    labelAmount.text = Util.formatCurrency(Double(newAmountNumber))
                }
            }
        }
    }
    
    //MARK: - Close
    @IBAction func buttonClosePressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
