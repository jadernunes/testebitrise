//
//  ChouseTicketsViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 01/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ChouseTicketsViewController: UIViewController, LoginDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelEventSubdescription: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var componentPayment: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelTitleDate: UILabel!
    @IBOutlet weak var labelTitleEvent: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelEventDescription: UILabel!
    
    var component       : ComponentViewController!
    var movie           : Movie?
    var movieSession    :MovieSession!
    var ticketPrice = 0.0
    var shouldShowPrice = false
    var amountSelected = 0
    var itemsInCollectionView = 0
    var idTicket = 0
    var ticketsAvailable = 0
    var ticketsSold = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)
        self.title = movie?.title
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        configureLabels()
        self.collectionView.registerNib(UINib(nibName: "TicketCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CELL_COLLECTION_TICKET)
        self.collectionView.backgroundColor = UIColor.clearColor()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        layout.itemSize = CGSize(width: UIScreen.mainScreen().bounds.width/4, height: UIScreen.mainScreen().bounds.width/4)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        activityIndicator.color = SessionManager.sharedInstance.cardColor
        activityIndicator.hidden = true
        
        self.ticketPrice = Double(movieSession.price.value!)
        self.idTicket = 123
        self.ticketsAvailable = 30
        self.ticketsSold = 20
        self.updateViews()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        shouldShowPrice = false
    }
    
    func configureLabels() {
        
        labelEventDescription.text = movie?.title
        labelEventSubdescription.text = movie?.sinopsis
        labelDate.text = movieSession.getFormatedDateString("dd/MM/yyyy HH:mm")
        
        labelEventDescription.font = LayoutManager.primaryFontWithSize(15)
        labelEventSubdescription.font = LayoutManager.primaryFontWithSize(15)
        labelDate.font = LayoutManager.primaryFontWithSize(15)
        labelTitle.font = LayoutManager.primaryFontWithSize(20)
        labelTitleEvent.font = LayoutManager.primaryFontWithSize(20)
        labelTitleDate.font = LayoutManager.primaryFontWithSize(20)
        
        labelEventDescription.textColor = LayoutManager.white
        labelEventSubdescription.textColor = LayoutManager.white
        labelDate.textColor = LayoutManager.white
        labelTitle.textColor = LayoutManager.white
        labelTitleEvent.textColor = LayoutManager.white
        labelTitleDate.textColor = LayoutManager.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configurePaymentComponent() {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.component = ComponentViewController()
            self.component.buttonTitleWhenNotLogged = kEnter
            self.component.buttonTitleWhenLogged    = kPay
            self.component.requireFullName = true
            self.component.delegate = self
            self.component.view.frame = self.componentPayment.bounds
            self.component.view.backgroundColor = LayoutManager.backgroundFirstColor
            self.componentPayment.addSubview(self.component.view)
            self.addChildViewController(self.component)
            self.component.didMoveToParentViewController(self)
        }
    }
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        SessionManager.sharedInstance.registerDevice()
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
        let success = self.amountSelected == 1 ? kMessageSuccessBuyOneTicket: kMessageSuccessBuyMultipleTickets
        (vc.viewControllers[0] as! ReceiptViewController).labels = ["success":  success,
                                                                    "date": NSDate.getDateTodayToShortYearFormat(),
                                                                    "time":NSDate.getTimeTodayToFormat(),
                                                                    "title": kPaymentConfirmedTitle,
                                                                    "type":1,
                                                                    "idFact":(self.movie?.id)!,
                                                                    "price":Double(self.amountSelected)*self.ticketPrice]
        vc.loadView()
        (vc.viewControllers[0] as! ReceiptViewController).previousVC = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collectionView.frame.size.width/3.5, collectionView.frame.size.height/3.5)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsInCollectionView;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_TICKET, forIndexPath: indexPath) as! TicketCollectionViewCell
        cell.frame.size.width = UIScreen.mainScreen().bounds.width/4
        cell.frame.size.height = UIScreen.mainScreen().bounds.width/4
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let customCell = cell as! TicketCollectionViewCell
        customCell.labelQuantity.text = "\(indexPath.row+1)"
        customCell.labelPrice.text = Util.formatCurrency(ticketPrice*Double(indexPath.row+1))
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).backgroundColor = SessionManager.sharedInstance.cardColor
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelQuantity.textColor = LayoutManager.white
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelPrice.textColor = LayoutManager.white
        amountSelected = indexPath.row + 1
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).backgroundColor = UIColor.clearColor()
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelQuantity.textColor = SessionManager.sharedInstance.cardColor
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelPrice.textColor = SessionManager.sharedInstance.cardColor
    }
    
    func checkValue() {
        self.ticketPrice = Double(movieSession.price.value!)
        self.shouldShowPrice = true
        self.updateViews()
    }
    
    func updateViews() {
        itemsInCollectionView = (ticketsAvailable-ticketsSold) == 0 ? 0 : (ticketsAvailable-ticketsSold) == 1 ? 1 : (ticketsAvailable-ticketsSold) == 2 ? 2 : MAX_TICKETS_EVENT
        if itemsInCollectionView > 0 {
            configurePaymentComponent()
            collectionView.reloadData()
        }
        else {
            let errorAlert = UIAlertController(title: kAlertTitle, message: kMessageNoTicketsAvailable, preferredStyle: UIAlertControllerStyle.Alert)
            
            errorAlert.addAction(UIAlertAction(title: kAlertButtonOk, style: .Cancel, handler: { (action: UIAlertAction!) in
                self.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popViewControllerAnimated(true)
            }))
            self.presentViewController(errorAlert, animated: true, completion: nil)
        }
    }
}
