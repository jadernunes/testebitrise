//
//  ListUnitiesTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 14/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ListUnitiesTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var imageViewIconUnity: UIImageView!
    @IBOutlet weak var labelNameUnity: UILabel!
    @IBOutlet weak var labelDetailUnity: UILabel!
    var unity :Unity!
    
    //MARK: - Native mathods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Configure cirlce on unity image
        self.imageViewIconUnity.layer.cornerRadius = 40
        self.imageViewIconUnity.layer.masksToBounds = true
        self.backgroundColor = LayoutManager.backgroundFirstColor
        self.labelNameUnity.textColor = LayoutManager.labelThirdColor
        self.labelDetailUnity.textColor = LayoutManager.labelThirdColor
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Custom methods
    
    /// Populate data with unity informations
    ///
    /// - Parameter unity: Object that reference unity
    func populateData(unity:Unity){
        var urlImage = ""
        if let urlImge = unity.getLogo() {
            urlImage = urlImge.value!
        }
        
        self.labelNameUnity.text = unity.name
        self.labelDetailUnity.text = unity.desc
        self.imageViewIconUnity.addImage(urlImage)
    }
}
