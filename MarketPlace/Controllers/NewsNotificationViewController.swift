//
//  NewsNotificationViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class NewsNotificationViewController: UIViewController {

    var urlString :String!
    @IBOutlet weak var webViewNewsNotification: UIWebView!
    @IBOutlet weak var buttonClose :UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webViewNewsNotification.delegate = PageManager.sharedInstance
        webViewNewsNotification.loadRequest(NSURLRequest(URL: NSURL(string:urlString)!))
        self.buttonClose.setTitleColor(LayoutManager.primaryColor, forState: UIControlState.Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Close
    @IBAction func buttonClosePressed(sender: AnyObject) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (viewController: UIViewController!) in
        })
    }
}
