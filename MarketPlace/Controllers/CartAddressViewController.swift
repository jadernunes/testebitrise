//
//  CartAddressViewController.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/27/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift
import JGProgressHUD

class CartAddressViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelEmpty: UILabel!
    @IBOutlet weak var buttonNewAddress: UIButton!
    var pullRefresh: UIRefreshControl!

    let loading = JGProgressHUD(style: .Dark)
    var idUnity = Int()
    var addresses : Results<Address>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "CartAddressTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_CART_ADDRESS)
        Util.adjustTableViewInsetsFromTarget(self)

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        let buttonClose = UIBarButtonItem(title: kClose, style: .Plain, target: self, action: #selector(buttonClosePressed(_:)))
        self.navigationItem.setLeftBarButtonItem(buttonClose, animated: true)

        buttonClose.tintColor = LayoutManager.white
        buttonNewAddress.layer.cornerRadius = 6
        buttonNewAddress.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonNewAddress.layer.borderWidth  = 1.0
        buttonNewAddress.titleLabel?.font   = LayoutManager.primaryFontWithSize(22)
        buttonNewAddress.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonNewAddress.setTitle(kNewAddress, forState: UIControlState.Normal)
        buttonClose.setTitleTextAttributes([ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()], forState: .Normal)
        labelEmpty.text = kMessageWithoutDataAddresses
        labelEmpty.font = LayoutManager.primaryFontWithSize(16)
        labelEmpty.textColor = LayoutManager.labelFirstColor
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(pullRefresh)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = kMyAddressesTitle
        addresses = AddressPersistence.getAllLocalAddresses()
        if addresses!.count == 0 {
            labelEmpty.hidden = false
            tableView.hidden = true
        }
        else {
            labelEmpty.hidden = true
            tableView.hidden = false
        }
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_CART_ADDRESS, forIndexPath: indexPath) as! CartAddressTableViewCell
        cell.labelNickname?.text = "ENDEREÇO"//addresses![indexPath.row].name
        cell.labelStreet?.text = addresses![indexPath.row].street!
        cell.labelNumber?.text = addresses![indexPath.row].number! + " " + addresses![indexPath.row].complement!
        return cell
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // call cep check
        // start jload
        let api = ApiServices()
        self.loading.showInView(self.view)
        // on success
        api.successCase = {(obj) in
            self.loading.dismiss()
            if let fee = obj?.objectForKey("fee") {
                
                if let cart = OrderEntityManager.sharedInstance.getCart(0) {
                    let realm = try! Realm()
                    
                    let listAddress = AddressPersistence.getAllLocalAddresses()
                    for addr in listAddress {
                        try! realm.write {
                            addr.selected = false
                            try! realm.commitWrite()
                        }
                    }
                    
                    try! realm.write {
                        cart.address = self.addresses![indexPath.row]
                        cart.address?.selected = true
                        cart.idOrderType = OrderType.Delivery.rawValue
                        cart.deliveryFee = fee as! Double
                    }
                }

                self.loading.dismiss()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                Util.showAlert(self, title: kAlertTitle, message: kMessageAddressNotAvailable)
                self.loading.dismiss()
            }
        }
        api.failureCase = {(msg) in
            Util.showAlert(self, title: kAlertTitle, message: kMessageAddressNotAvailable)
            self.loading.dismiss()
        }
         
        api.checkDeliveryFee(self.addresses![indexPath.row], idUnity: idUnity)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) && (self.addresses?.count > 1) {
            let realm = try! Realm()
            try! realm.write {
                realm.delete(self.addresses![indexPath.row])
            }
            self.refresh()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    @IBAction func buttonClosePressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func refresh() {
        self.pullRefresh.endRefreshing()
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
        if addresses!.count == 0 {
            labelEmpty.hidden = false
            tableView.hidden = true
        }
        else {
            labelEmpty.hidden = true
            tableView.hidden = false
        }
    }
}
