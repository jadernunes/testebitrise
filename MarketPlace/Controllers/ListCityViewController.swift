//
//  ListCityViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ListCityViewController: MZFormSheetController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var listCities: NSArray?
    var citySelected: NSDictionary?
    var handlerViewController: NewCardViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let dicCatalog = MobilityFilesUtil.loadFromFile("mobilityCatalog")
        self.listCities = dicCatalog["cities"] as? NSArray
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClosePressed(sender: AnyObject) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (presentedFSViewController) in
            
        })
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listCities!.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ListCiyTableViewCell
        cell.iconSelected.image = nil
        
        let dictionaryCity = self.listCities![indexPath.row] as! NSDictionary
        cell.populateData(dictionaryCity)
        
        if(dictionaryCity["city_id"] as? String == citySelected?["city_id"] as? String) {
            cell.markSelected()
        }
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (presentedFSViewController) in
            let dictionaryCity = self.listCities![indexPath.row] as! NSDictionary
            self.handlerViewController!.onSelectCitySuccess(dictionaryCity)
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
