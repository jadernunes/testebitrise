//
//  PaymentTypesTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/6/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift
class PaymentTypesTableViewCell: UITableViewCell, ScannerQrCodeProtocol {

    @IBOutlet weak var imageViewCircle: UIImageView!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var imageViewType: UIImageView!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelFullAddress: UILabel!
    @IBOutlet weak var buttonChange: UIButton!
    @IBOutlet weak var labelTable: UILabel!
    @IBOutlet weak var viewGrayBackground: UIView!
    @IBOutlet weak var imageViewArrow: UIImageView!
    
    var cart : Order!
    var parentVC: CartViewController!
    var arrayQrDecoded = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageViewCircle.tintImage(SessionManager.sharedInstance.cardColor)
        self.labelType.font = LayoutManager.primaryFontWithSize(20)
        self.labelType.textColor = LayoutManager.greyTitleItemCart
        self.imageViewType.tintImage(SessionManager.sharedInstance.cardColor)
        self.labelAddress.font = LayoutManager.primaryFontWithSize(20)
        self.labelAddress.textColor = LayoutManager.greyTitleItemCart
        self.labelFullAddress.font = LayoutManager.primaryFontWithSize(18)
        self.labelFullAddress.textColor = LayoutManager.greyDescItemCart
        self.labelTable.font = LayoutManager.primaryFontWithSize(20)
        self.labelTable.textColor = LayoutManager.greyTitleItemCart
        self.labelAddress.hidden = true
        self.labelFullAddress.hidden = true
        self.labelTable.hidden = true
        self.buttonChange.hidden = true
        self.buttonChange.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        self.buttonChange.setTitle("Alterar >", forState: .Normal)
        self.buttonChange.titleLabel?.font = LayoutManager.primaryFontWithSize(18)
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0,UIScreen.mainScreen().bounds.width, 0.5)
        topBorder.backgroundColor = LayoutManager.greyDescItemCart.CGColor
        viewGrayBackground.layer.addSublayer(topBorder)
        self.imageViewArrow.transform = CGAffineTransformMakeRotation((CGFloat(3/2*M_PI)))
        self.imageViewArrow.tintImage(SessionManager.sharedInstance.cardColor)
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAsChosen() {
        self.imageViewCircle.image = UIImage(named: "radio_button")
        self.imageViewCircle.tintImage(SessionManager.sharedInstance.cardColor)
    }
    
    func unsetAsChosen() {
        self.imageViewCircle.image = UIImage(named: "ic_order_empty")
        self.imageViewCircle.tintImage(LayoutManager.greyTitleItemCart)
    }
    
    func changeAddressAndPlaceLabel() {
        if cart.idOrderType == OrderType.Delivery.rawValue {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("CartAddressViewController") as! UINavigationController
            (vc.viewControllers[0] as! CartAddressViewController).idUnity = (cart?.idUnity)!
            self.parentVC.presentViewController(vc, animated: true, completion: nil)
        }
        
        if cart.idOrderType == OrderType.ShortDelivery.rawValue {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
            (vc.viewControllers[0] as! ScannerViewController).delegate = self
            (vc.viewControllers[0] as! ScannerViewController).titleString = kQRScannerTitle
            self.parentVC.presentViewController(vc, animated: true, completion: nil)
        }

    }

    @IBAction func changeSelected(sender: AnyObject) {
        changeAddressAndPlaceLabel()
    }
    
    func didScanValidCode(value: String) {
        arrayQrDecoded = []
        var qrToBeDecoded = value
        qrToBeDecoded.removeAtIndex(qrToBeDecoded.startIndex.advancedBy(3))
        if let qrDecoded = qrToBeDecoded.fromBase64() {
            arrayQrDecoded = qrDecoded.componentsSeparatedByString("_")
        }
        if arrayQrDecoded.count > 0 {
            if arrayQrDecoded.contains("GRP") {
                if arrayQrDecoded[2] == "FOODCOURT" {
                    
                    if arrayQrDecoded.count == 4 {
                        let realm = try! Realm()
                        try! realm.write() {
                            cart?.placeLabel = arrayQrDecoded[3]
                            cart?.idOrderType = OrderType.ShortDelivery.rawValue
                            self.labelTable.text = cart?.placeLabel
                        }
                    }
                    else {
                        Util.showAlert(self.parentVC, title: kAlertTitle, message: kMessageInvalidQR)
                    }
                }
                    
                else {
                    Util.showAlert(self.parentVC, title: kAlertTitle, message: kMessageInvalidQR)
                }
            }
            else if arrayQrDecoded.contains("UNT") {
                if arrayQrDecoded.contains("TBL") {
                    let realm = try! Realm()
                    try! realm.write() {
                        cart?.placeLabel = arrayQrDecoded[arrayQrDecoded.indexOf("TBL")!+1]
                        cart?.idOrderType = OrderType.ShortDelivery.rawValue
                        self.labelTable.text = cart?.placeLabel

                    }
                }
            }
            else {
                Util.showAlert(self.parentVC, title: kAlertTitle, message: kMessageInvalidQR)
            }
        }
        else {
            Util.showAlert(self.parentVC, title: kAlertTitle, message: kMessageInvalidQR)
        }
    }
    
    func didScanInvalidCode() {
        Util.showAlert(self.parentVC, title: kAlertTitle, message: kMessageInvalidQR)
    }
}
