//
//  NewBannerCollectionViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewBannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewBanner: UIImageView!
    @IBOutlet weak var viewBackgroud: UIView!
    
    @IBOutlet weak var labelNameUnity: UILabel!
    @IBOutlet weak var labelDistanceUnity: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelOrderType: UILabel!
    
    @IBOutlet weak var buttonOpenedStatus: UILabel! {
        didSet {
            buttonOpenedStatus.alpha = 0.0
            buttonOpenedStatus.clipsToBounds = true
            buttonOpenedStatus.layer.cornerRadius = 2.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.lightGray4all()
        viewBackgroud.backgroundColor = UIColor.whiteColor()
        self.viewBackgroud.layer.masksToBounds = true
        self.viewBackgroud.layer.cornerRadius = 6
        
        self.labelMessage.textColor = UIColor.gray4all()
        self.labelDistanceUnity.textColor = UIColor.gray4all()
        self.labelOrderType.textColor = UIColor.gray4all()
    }
    
}
