//
//  ReservaMesaViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ReservaMesaViewController: UIViewController {

    var currentTextField            : UITextField? = nil
    var currentPickerDataSouce      = [String]()
    var arrayOptionLugares          = ["2 lugares", "3 lugares", "4 lugares", "5 lugares", "6 lugares"]
    var arrayOptionDia              = ["Amanhã, 4 de novembro de 2016", "5 de novembro de 2016", "6 de novembro de 2016", "7 de novembro de 2016", "8 de novembro de 2016"]
    var arrayOptionHorario          = ["19h", "19:30", "20:00", "20:30", "21:00"]
    var pickerView                  = UIPickerView()
    let toolBar                     = UIToolbar()
    
    @IBOutlet weak var tableView: UITableView!
    
    enum kReservaCellType: Int {
        case lugares        = 0
        case dia            = 1
        case horario        = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = kBookingTitle
        Util.adjustTableViewInsetsFromTarget(self)

        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]

        
        pickerView.delegate = self
        pickerView.showsSelectionIndicator = true
        
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor.blackColor()
        toolBar.sizeToFit()
        
        let doneButton      = UIBarButtonItem(title: kClose, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.donePicker))
        let spaceButton     = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Util.adjustInsetsFromCart(tableView)
        Util.adjustTableViewInsetsFromTarget(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Custom Methods
    func donePicker() {
        self.currentTextField?.text = currentPickerDataSouce[pickerView.selectedRowInComponent(0)]
        self.currentTextField?.resignFirstResponder()
        self.currentTextField?.endEditing(true)
    }
    
    func createCell(tableview: UITableView, identifier: String, indexRow: Int, pergunta: String) -> AnyObject {
        switch indexRow {
        case 0,1,2:
            let cell : ReservaMesaTableViewCell = tableview.dequeueReusableCellWithIdentifier(identifier, forIndexPath: NSIndexPath(forRow: indexRow, inSection: 0)) as! ReservaMesaTableViewCell
            cell.txtAnswer.inputView = self.pickerView
            cell.txtAnswer.inputAccessoryView = toolBar
            cell.txtAnswer.delegate = self
            cell.txtAnswer.tag = 100+indexRow
            cell.lblQuestion.text = pergunta
            return cell
        default:
            break
        }
        return UITableViewCell()
    }

}
