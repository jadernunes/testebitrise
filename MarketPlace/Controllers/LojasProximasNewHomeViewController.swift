//
//  LojasProximasNewHomeViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 19/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class LojasProximasNewHomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var listGeneralUnities: [Unity]?
    var tableNearby = TableNearby(listUnities: nil, superController: nil)
    var tipoPedido: Int?
    var load: LoadingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        load = LoadingViewController.sharedManager() as! LoadingViewController
        load.startLoading(self, title: "Aguarde")
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableNearby.superViewController = self
        
        self.tableView.registerNib(UINib(nibName: "NewSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "NewSearchTableViewCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self)
        
        let userCoordinate = LocationManager.shared.location?.coordinate
        let api = ApiServices()
        api.getUnitiesNearBy(userCoordinate, tipoPedido: self.tipoPedido, completion: { (listUnities, success) in
            
            var lojas = [Unity]()
            
            if let unities = listUnities as? [[String:AnyObject]] {
                lojas = UnityPersistence.manipulateAnyUnities(unities, tipoPedido: self.tipoPedido)
            } else if let object = listUnities {
                if let unities = object["unities"] as? [[String:AnyObject]] {
                    lojas = UnityPersistence.manipulateAnyUnities(unities, tipoPedido: self.tipoPedido)
                }
            }
            
            self.load.finishLoading(nil)
            self.listGeneralUnities = lojas
            self.tableNearby.listGeneralUnities = self.listGeneralUnities
            self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenLojasPorPerto)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension LojasProximasNewHomeViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableNearby.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        return tableNearby.tableView(tableView, didSelectRowAtIndexPath: indexPath)
    }
}

extension LojasProximasNewHomeViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableNearby.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return tableNearby.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }
}
