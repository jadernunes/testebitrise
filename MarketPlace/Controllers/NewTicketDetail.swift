//
//  NewTicketDetail.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 27/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import EventKit

class NewTicketDetail: UIViewController {
    
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var labelCodeQR: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelDescription: UITextView!
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewSeparatorNameUnity: UIView!
    
    @IBOutlet weak var imageViewFact: UIImageView!
    @IBOutlet weak var buttonAddCalendar: UIButton!
    var voucher: Voucher!
    @IBOutlet weak var imageViewSeparatorQR: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNavigation.backgroundColor = UIColor.green4all()
        self.viewSeparatorNameUnity.backgroundColor = UIColor.lightGray4all()
        self.labelDescription.textColor = UIColor.gray4all()
        self.labelHour.textColor = UIColor.gray4all()
        self.labelDate.textColor = UIColor.gray4all()
        self.buttonAddCalendar.layer.masksToBounds = true
        self.buttonAddCalendar.layer.cornerRadius = 8
        self.buttonAddCalendar.layer.borderWidth = 1.0
        self.buttonAddCalendar.layer.borderColor = UIColor.green4all().CGColor
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.labelDate.text = ""
        self.labelHour.text = ""
        self.labelDescription.text = ""
        self.imageViewFact.addImage(nil)
        
        if voucher.idVoucherStatus == VoucherStatus.Cancelled.rawValue ||
            voucher.idVoucherStatus == VoucherStatus.Expired.rawValue ||
            voucher.idVoucherStatus == VoucherStatus.Used.rawValue {
            self.imageViewQR.hidden = true
            self.buttonAddCalendar.hidden = true
            self.imageViewSeparatorQR.hidden = true
            self.viewNavigation.backgroundColor = UIColor.gray4all()
        } else {
            self.imageViewQR.hidden = false
            self.buttonAddCalendar.hidden = false
            self.imageViewSeparatorQR.hidden = false
            self.viewNavigation.backgroundColor = UIColor.green4all()
        }
        
        self.labelCodeQR.text = "#\(self.voucher.id)"
        
        if let ticket = voucher.ticket {
            
            if let batch = ticket.batch {
                if let fact = FactEntityManager.getFactByID(String(batch.idFact)) {
                    self.labelDate.text = NSString.getDayStringFromTimestamp(fact.timestampBegin) + " de " + NSString.getMonthStringFromTimestamp(fact.timestampBegin).capitalizedString
                    self.labelHour.text = NSString.getHourStringFromTimestamp(fact.timestampBegin)
                    self.labelName.text = fact.title
                    self.labelDescription.text = fact.desc
                    self.imageViewFact.addImage(fact.thumb)
                }
            }
        }
        
        let qrcode : QRCodeUtils = QRCodeUtils()
        if let image = qrcode.generateQRCode(qrString:voucher.uuid! as String, target: self.imageViewQR) {
            //voucher.uuid! as String
            self.imageViewQR.image = image
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenDetalheIngresso)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonAddInCalendar(sender: UIButton) {
        
        if let ticket = voucher.ticket {
            
            if let batch = ticket.batch {
                if let fact = FactEntityManager.getFactByID(String(batch.idFact)) {
                    let store = EKEventStore()
                    let event = EKEvent(eventStore: store)
                    let timestampEnd = Util.timeStampToNSDate(Double(fact.timestampEnd))
                    event.title = fact.title
                    event.startDate = timestampEnd //today
                    
                    store.requestAccessToEntityType(.Event) {(granted, error) in
                        if !granted { return }
                        
                        self.registerEventOnCalendar(event, store: store)
                    }
                }
            }
        }
    }
    
    @IBAction func buttonClosePressed(sender: UIButton) {
        self.formSheetController?.dismissAnimated(true, completionHandler: nil)
    }
    
    //MARK: - Register event
    private func registerEventOnCalendar(event: EKEvent!, store: EKEventStore!) {
        
        event.endDate = event.startDate.dateByAddingTimeInterval(60*60) //1 hour long meeting
        event.calendar = store.defaultCalendarForNewEvents
        
        do {
            try store.saveEvent(event, span: .ThisEvent, commit: true)
            Util.showAlert(self, title: kAlertCalendarTitle, message: kMessageEventAddedToCalendar)
            //self.savedEventId = event.eventIdentifier //save event id to access this particular event later
        } catch {
            // Display error to user
            Util.showAlert(self, title: kAlertTitle, message: kMessageUnableToExecuteAction)
        }
    }
}
