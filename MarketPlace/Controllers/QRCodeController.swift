//
//  QRCodeController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 27/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeController : UIViewController, AVCaptureMetadataOutputObjectsDelegate, ScannerQrCodeProtocol {
    
    //IBOutlets
    @IBOutlet weak var qrFrame  : UIImageView!
    @IBOutlet weak var labelInfo: UILabel!
    
    //Variables
    var captureSession      :AVCaptureSession?
    var videoDevice         :AVCaptureDevice?
    var videoPreviewLayer   :AVCaptureVideoPreviewLayer?
    var videoInput          :AVCaptureDeviceInput?
    var metadataOutput      :AVCaptureMetadataOutput?
    var qrCodeFrameView     :UIView?
    var didReadCode         :Bool! = false
    var didFoundTransaction : Bool = false
    var barCode             : String = ""
    var isRunning           : Bool = false
    var delegate            : ScannerQrCodeProtocol!
    var titleString         : String = "Leitor de comanda"
    var arrayQrDecoded      = [String]()
    var toastView           = UILabel()
    var isAnimating         = false
    
    // Added to support different barcodes
    var allowedBarcodeTypes = NSMutableArray()
    
    func showNavBar(navBarTintColor:UIColor?=nil) {
        if let barColor:UIColor = navBarTintColor {
            self.navigationController?.navigationBar.barTintColor = barColor
        }
        else {
            self.navigationController?.navigationBar.barTintColor = LayoutManager.green4all
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes( [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Normal)
        labelInfo.font = LayoutManager.primaryFontWithSize(16)
        labelInfo.textColor = UIColor.whiteColor()
        allowedBarcodeTypes.addObject("org.iso.QRCode")
        allowedBarcodeTypes.addObject("org.iso.PDF417")
        allowedBarcodeTypes.addObject("org.gs1.UPC-E")
        allowedBarcodeTypes.addObject("org.iso.Aztec")
        allowedBarcodeTypes.addObject("org.iso.Code39")
        allowedBarcodeTypes.addObject("org.iso.Code39Mod43")
        allowedBarcodeTypes.addObject("org.gs1.EAN-13")
        allowedBarcodeTypes.addObject("org.gs1.EAN-8")
        allowedBarcodeTypes.addObject("com.intermec.Code93")
        allowedBarcodeTypes.addObject("org.iso.Code128")
        allowedBarcodeTypes.addObject("org.ansi.Interleaved2of5")
        
        self.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.hideNavBar()
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenQRAndTicketReader)
        CartCustomManager.sharedInstance.hideCart()
        
        let origImage = self.qrFrame.image
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        qrFrame.image = tintedImage
        qrFrame.tintColor = LayoutManager.green4all
        
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = LayoutManager.green4all
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
//        self.navigationController?.navigationBarHidden = true
        self.setupCaptureSession()
        if videoPreviewLayer != nil {
            videoPreviewLayer!.frame = self.view.bounds
            self.startRunning()
            self.view.layer.addSublayer(self.videoPreviewLayer!)
        }else{
            //TODO : Tratar quando câmera indisponível
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        didReadCode = false
        createOverlay()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopRunning()
    }
    
    @IBAction func closeVc(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createOverlay()
    {
        
        self.view.setNeedsLayout()
        
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
        
        let overlayView = UIView()
        overlayView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        overlayView.alpha = 0.5
        overlayView.backgroundColor = UIColor.blackColor()
        overlayView.tag = 100
        self.view.addSubview(overlayView)
        
        let maskLayer = CAShapeLayer()
        
        // Create a path with the rectangle in it.
        let path = CGPathCreateMutable()
        qrFrame.layoutIfNeeded()
        let rect = CGRectMake(qrFrame.frame.origin.x+2, qrFrame.frame.origin.y+2, qrFrame.frame.width-3, qrFrame.frame.height-4)
        
        let dontknow =  UIBezierPath(roundedRect: rect, cornerRadius: 28)
        dontknow.stroke()
        CGPathAddPath(path, nil, dontknow.CGPath)
        CGPathAddRect(path, nil, CGRectMake(0, 0, overlayView.frame.width, overlayView.frame.height))
        maskLayer.backgroundColor = UIColor.blackColor().CGColor
        
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        // Release the path since it's not covered by ARC.
        overlayView.layer.mask = maskLayer
        overlayView.clipsToBounds = true
        qrFrame.alpha = 0
        overlayView.alpha = 0
        self.view.bringSubviewToFront(qrFrame.superview!)
        self.view.bringSubviewToFront(qrFrame)
        UIView.animateWithDuration(0.3) { () -> Void in
            overlayView.alpha = 0.5
            self.qrFrame.alpha = 1.0
        }
        self.view.bringSubviewToFront(labelInfo)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCaptureSession(){
        if (captureSession != nil){
            return
        }
        
        videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        if videoDevice == nil{
            NSLog("Não foi localizado camera")
            return
        }
        do{
            
            captureSession = AVCaptureSession()
            videoInput = try AVCaptureDeviceInput(device: videoDevice)
            
            if captureSession?.canAddInput(videoInput) == true{
                captureSession?.addInput(videoInput)
            }
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            
            //Here the metada is capture and processed
            metadataOutput = AVCaptureMetadataOutput()
            let metadataQueue : dispatch_queue_t = dispatch_queue_create("com.4all.client.metadata", nil)
            
            metadataOutput?.setMetadataObjectsDelegate(self, queue: metadataQueue)
            
            if captureSession?.canAddOutput(metadataOutput) == true{
                captureSession?.addOutput(metadataOutput)
            }
            
        }catch{
            
        }
    }
    
    func startRunning(){
        if isRunning == true{
            return
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.captureSession?.startRunning()
            self.metadataOutput?.metadataObjectTypes = self.metadataOutput?.availableMetadataObjectTypes
            self.isRunning = true
        }
    }
    
    func stopRunning(){
        if isRunning == false{
            return
        }
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.captureSession?.stopRunning()
            self.videoPreviewLayer?.removeFromSuperlayer()
            self.isRunning = false
        }
        
    }
    
    //Mark: AV Delegate functions
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
        
        //videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
        if metadataObj != nil {
            let barCode = Barcode.processMetadataObject(metadataObj)
            
            for str in allowedBarcodeTypes{
                if barCode.getBarcodeType() == str as! String && !didReadCode{
                    didReadCode = true
                    self.validBarcodeFound(barCode)
                    return
                }
                
                self.didScanInvalidCode()
                
            }
            return
        }
    }
    
    func validBarcodeFound(barCode : Barcode){
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.barCode = barCode.getBarcodeData()
            self.didScanValidCode(self.barCode)
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if didReadCode == true {
            return false
        }else{
            didReadCode = true
            return true
        }
    }
    
    //Scanner Delegate
    func didScanValidCode(value: String) {
        
        var storyboard = UIStoryboard(name:"Main", bundle: nil)
        arrayQrDecoded = []
        var qrToBeDecoded = value
        qrToBeDecoded.removeAtIndex(qrToBeDecoded.startIndex.advancedBy(3))
        if let qrDecoded = qrToBeDecoded.fromBase64() {
            arrayQrDecoded = qrDecoded.componentsSeparatedByString("_")
        }
        
        //IF IS A PAYMENT, DOESN'T METTER WHICH MARKETPLACE IT IS.. JUST DO IT!
        if arrayQrDecoded.count > 0 &&  !arrayQrDecoded.contains("PAY") && arrayQrDecoded[0] == ID_MARKETPLACE {
            
            if arrayQrDecoded.contains("GRP") {
                didScanGroup(arrayQrDecoded)
            }else if arrayQrDecoded.contains("UNT") {
                didScanUnity(arrayQrDecoded)
            }
            else if arrayQrDecoded.contains("PRK") {
                didScanParking(arrayQrDecoded)
            } else if arrayQrDecoded.contains("TOT") {
                didScanTotem(arrayQrDecoded)
            } else {
                
                if Lib4all().qrCodeIsSupported(value) {
                    //Se válido, chamar o método que irá mostrar a tela de pagamento e informações da transação
                    Lib4all().handleQrCode(value, inViewController: self)
                } else {
                    self.didScanInvalidCode()
                }
            }
        } else if arrayQrDecoded.contains("PAY") {
            
            let destVc = storyboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
            
            if arrayQrDecoded.count == 8 {
                destVc.idObjectPayment = String(arrayQrDecoded[7]) //Id do objeto a ser pago
                destVc.typePayment = String(arrayQrDecoded[6]) //tipo de pagamento Ex.: CPN é Campaign
                destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
            } else if arrayQrDecoded.count == 7 {
                destVc.typePayment = String(arrayQrDecoded[6]) //tipo de pagamento Ex.: CPN é Campaign
                destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
            } else if arrayQrDecoded.count == 6 {
                destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
            } else if arrayQrDecoded.count == 5 {
                destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
            }
            
            destVc.hidesBottomBarWhenPushed = true
            destVc.previousVC = self
            self.presentViewController(destVc, animated: true, completion: nil)
        } else if arrayQrDecoded.contains("CNC") {//Cancelamento de um pedido
            
            let destVc = storyboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
            
            destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
            destVc.strAmount = String(arrayQrDecoded[3])//price * 100
            destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
            destVc.isCancelPayment = true
            
            destVc.previousVC = self
            self.presentViewController(destVc, animated: true, completion: nil)
            
        } else {
            //validate is ticket
            if(Int(value) > 0 && value.characters.count >= 8) {
                storyboard = UIStoryboard(name:"Parking Storyboard", bundle: nil)
                let viewController = storyboard.instantiateViewControllerWithIdentifier("parkingVC") as! PaymentViewController
                viewController.valueFromHomeQRReader = value
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                if Lib4all().qrCodeIsSupported(value) {
                    //Se válido, chamar o método que irá mostrar a tela de pagamento e informações da transação
                    Lib4all().handleQrCode(value, inViewController: self)
                } else {
                    self.didScanInvalidCode()
                }
            }
        }
    }
    
    func didScanInvalidCode() {
        Util.showToast("QR Code inválido!", timeDuration: 2, targetView: view, type: kToastType.warning)
    }
    
    func didScanGroup(arrayContent : [String]) {
        
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        
        //FOODCOURT
        if arrayContent[2] == "FOODCOURT" {
            
            if arrayContent.count == 4 {
                SessionManager.sharedInstance.tableEnabledByQr = arrayContent[3]
            }
            
            listVc.group = GroupEntityManager.getGroupCustom("idGroupTreeType = 20")
            listVc.subType = .Foodcourt
            
            //GROUPS
        }else{
            listVc.group = GroupEntityManager.getGroupCustom("id = \(arrayContent[2])")
            listVc.title = "ITENS"
        }
        
        listVc.preferredStatusStyle = .LightContent
        listVc.hidesBottomBarWhenPushed = true
        self.showNavBar()
        self.navigationController?.pushViewController(listVc, animated: true)
    }
    
    func didScanParking(arrayContent : [String]) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC") as! OndePareiViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if arrayContent.count > 3 {
            let dict = ["building":"\(arrayContent[2])",
                        "floor":"\(arrayContent[3])",
                        "region":"\(arrayContent[4])"]
            
            defaults.setObject(dict, forKey: "dicWherePark")
            
        }
        
        defaults.synchronize()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didScanUnity(arrayContent : [String]) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        if let unity = UnityPersistence.getUnityByID(arrayContent[2]) {
            detailsVc.title = unity.name
            detailsVc.idObjectReceived = unity.id
            detailsVc.typeObjet = TypeObjectDetail.Store
            detailsVc.hidesBottomBarWhenPushed = true
            SessionManager.sharedInstance.searchOrQrEnabled = true
            if arrayContent.contains("TBL") {
                let activeCart = OrderEntityManager.sharedInstance.hasActiveCart()
                if (activeCart.active == false || activeCart.cart.orderItems.count == 0) {
                    if unity.orderShortDeliveryEnabled == true {
                        SessionManager.sharedInstance.tableEnabledByQr = arrayContent[4]
                        SessionManager.sharedInstance.tableChosen = true
                    }
                    else {
                        Util.showAlert(self, title: kAlertTitle, message: kMessageShortDeliveryUnavailable)
                    }
                }
                else {
                    if activeCart.cart.idUnity == unity.id {
                        if unity.orderShortDeliveryEnabled == true {
                            SessionManager.sharedInstance.tableEnabledByQr = arrayContent[4]
                            SessionManager.sharedInstance.tableChosen = true
                        }
                        else {
                            Util.showAlert(self, title: kAlertTitle, message: kMessageShortDeliveryUnavailable)
                        }
                    }
                    else {
                        Util.showAlert(self, title: kAlertTitle, message: kMessageOtherUnityActive)
                    }
                }
            }
            self.showNavBar()
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
        else {
            Util.showAlert(self, title: kAlertTitle, message: kMessageUnityNotFound)
        }
    }
    
    func didScanTotem(arrayContent : [String]) {
        let vc = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("TotemVC") as! TotemViewController
        vc.latitude = Double(arrayContent[2])!
        vc.longitude = Double(arrayContent[3])!
        vc.viewController = self
        vc.showARViewController()
    }
}
