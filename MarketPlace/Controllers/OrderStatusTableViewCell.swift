//
//  OrderStatusTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/14/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class OrderStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewStatus: UIImageView!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var viewLineUp: UIView!
    @IBOutlet weak var viewLineDown: UIView!
    var done = false
    var waiting = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelStatus.font       = LayoutManager.primaryFontWithSize(20)
        labelStatus.textColor  = LayoutManager.mediumGreyTracking
        
        labelTime.font       = LayoutManager.primaryFontWithSize(20)
        labelTime.textColor  = LayoutManager.mediumGreyTracking
        labelTime.text       = ""
        
        self.imageViewStatus.tintImage(LayoutManager.statusImageGreyTracking)
        self.imageViewStatus.contentMode = .ScaleAspectFit
        self.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
