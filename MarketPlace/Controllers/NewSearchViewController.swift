//
//  NewSearchViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 05/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class NewSearchViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: DesignableUITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewBackgroudTextField: UIView!
    @IBOutlet weak var viewSeparatorResult: UIView!
    @IBOutlet weak var labelTitleResult: UILabel!
    
    var listGeneralUnities: [Unity]?
    var listItensSearched: [ItemSearched]? = [ItemSearched]()
    var tableNearby = TableNearby(listUnities: nil, superController: nil)
    var tableResult = TableSearchResults(listUnities: nil, superController: nil)
    var isUseNearby = true
    
    var timeSearch          : NSTimeInterval!
    var canRequest          : Bool!
    var lastText            : String!
    var loader: UIActivityIndicatorView!
    var load: LoadingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        load = LoadingViewController.sharedManager() as! LoadingViewController
        
        loader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        let frame = UIScreen.mainScreen().applicationFrame
        loader.frame = CGRect(x: (frame.size.width/2), y: (frame.size.height/2)-100, width: 20, height: 20)
        self.tableView.addSubview(self.loader)
        self.loader.startAnimating()
        
        self.viewBackgroudTextField.backgroundColor = UIColor.lightGray4all()
        self.textField.leftImage = UIImage(named: "iconSearchGreen")
        self.title = "Busca"
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.textField.delegate = self
        tableNearby.superViewController = self
        tableResult.superViewController = self
        
        self.labelTitleResult.text = "Estabelecimentos próximos"
        self.viewSeparatorResult.backgroundColor = UIColor.green4all()
        
        self.tableView.registerNib(UINib(nibName: "NewSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "NewSearchTableViewCell")
        self.tableView.registerNib(UINib(nibName: "NewSearchResultTableViewCell", bundle: nil), forCellReuseIdentifier: "NewSearchResultTableViewCell")
        
        dispatch_async(dispatch_get_main_queue()){
            if let userCoordinate = LocationManager.shared.location?.coordinate {
                let api = ApiServices()
                api.getUnitiesNearBy(userCoordinate, completion: { (listUnities, success) in
                    
                    var lojas = [Unity]()
                    
                    if let unities = listUnities as? [[String:AnyObject]] {
                        lojas = UnityPersistence.manipulateAnyUnities(unities)
                    } else if let object = listUnities {
                        if let unities = object["unities"] as? [[String:AnyObject]] {
                            lojas = UnityPersistence.manipulateAnyUnities(unities)
                        }
                    }
                    
                    self.loader.stopAnimating()
                    self.loader.removeFromSuperview()
                    self.listGeneralUnities = lojas
                    self.tableNearby.listGeneralUnities = self.listGeneralUnities
                    self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
                })
            } else {
                self.loader.stopAnimating()
                self.loader.removeFromSuperview()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenBusca)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.text?.isEmpty == true {
            self.textField.leftImage = nil
            self.textField.rightImage = UIImage(named: "iconClearText")
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.text?.isEmpty == true {
            self.textField.leftImage = UIImage(named: "iconSearchGreen")
            self.textField.rightImage = nil
            self.isUseNearby = true
            self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        timeSearch = 0.0
        canRequest = true
        lastText = self.textField.text
        self.verifyRequest(lastText)
        
        if textField.text!.characters.count < 3 {
            if isUseNearby == false {
                self.isUseNearby = true
                self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
            }
        }
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.textField.resignFirstResponder()
        super.touchesBegan(touches, withEvent: event)
    }
    
    func verifyRequest(text: String) {
        if text == lastText && text.characters.count >= 3 {
            if timeSearch >= SectionsManager.timeToSearch && canRequest == true {
                if self.textField.text?.characters.count >= 3 {
                    load.startLoading(self, title: "Aguarde")
                    self.searchFromAPI(text)
                    canRequest = false
                }
            } else if timeSearch < SectionsManager.timeToSearch {
                timeSearch = timeSearch.advancedBy(0.1)
                self.performSelector(#selector(verifyRequest), withObject: text, afterDelay: 0.1)
            }
        } else {
            return
        }
    }
    
    //MARK: - Request search from API
    
    func searchFromAPI(searchText: String) {
        let api = ApiServices()
        
        api.successCase = {(obj) in
            self.listItensSearched?.removeAll()
            self.load.finishLoading(nil)
            
            if let arrayContent = obj as? NSArray {
                
                //Convert dictionary to ItemSearched
                for objectSearched in arrayContent {
                    let objectConverted = ItemSearched()
                    
                    objectConverted.id      = objectSearched.objectForKey("id") as! Int
                    objectConverted.logo    = objectSearched.objectForKey("logo") as? String
                    objectConverted.subType = objectSearched.objectForKey("subType") as? Int
                    objectConverted.title   = objectSearched.objectForKey("title") as? String
                    objectConverted.type    = objectSearched.objectForKey("type") as? String
                    
                    self.listItensSearched?.append(objectConverted)
                }
            }
            
            self.isUseNearby = false
            self.tableResult = TableSearchResults(listUnities: self.listItensSearched, superController: self)
            self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
        }
        
        api.failureCase = {(msg) in
            self.load.finishLoading(nil)
        }
        
        //Get session Token
        let lib4all = Lib4all.sharedInstance()
        var sessiontoken = ""
        if lib4all.hasUserLogged() {
            let account = lib4all.getAccountData() as NSDictionary
            sessiontoken = String((account.objectForKey("sessionToken"))!)
        }
        
        api.searchAnythingOnServer(searchText,sessionToken: sessiontoken)
    }
}

extension NewSearchViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.textField.resignFirstResponder()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if isUseNearby {
            return tableNearby.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
        
        return tableResult.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isUseNearby {
            return tableNearby.tableView(tableView, didSelectRowAtIndexPath: indexPath)
        }
        
        return tableResult.tableView(tableView, didSelectRowAtIndexPath: indexPath)
    }
}

extension NewSearchViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUseNearby {
            return tableNearby.tableView(tableView, numberOfRowsInSection: section)
        }
        
        return tableResult.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if isUseNearby {
            return tableNearby.tableView(tableView, cellForRowAtIndexPath: indexPath)
        }
        
        return tableResult.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }
}
