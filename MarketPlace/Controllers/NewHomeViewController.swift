//
//  NewHomeViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 27/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class NewHomeViewController: UIViewController, LoginDelegate {
    
    @IBOutlet weak var tableViewSections: SectionsTableView!
    @IBOutlet weak var buttonPayment4all: Button4allPayment!
    var listaLojasProximas: [Unity]!
    
    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PageManager.sharedInstance.rootViewController = self
        
        self.tableViewSections.superViewController = self
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        
        /* --- adaptação para quando é passada outra home --- */
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        self.tableViewSections.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.buttonPayment4all.frame.height + 30, right: 0)
        
        presentOnboarding(self)
        self.view.backgroundColor = UIColor.lightGray4all()
        
        self.navigationController?.navigationBar.translucent = true
        let imgView = UIImageView(frame: CGRectMake(0, 0, 45, 20))
        imgView.image = UIImage(named: "logoNaviBar")
        imgView.contentMode = .ScaleAspectFit
        self.navigationItem.titleView = imgView
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        PageManager.sharedInstance.getNewNotifications()
        Util.setNavigationBarWithGradient((self.navigationController?.navigationBar)!)
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.postNotificationName("statusLogin", object: nil)
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: true, viewController: self)
        
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        
        if let userCoordinate = LocationManager.shared.location?.coordinate {
            
            if let cell = self.tableViewSections.cellForRowAtIndexPath(indexPath) as? BannerNewHomeTableViewCell {
                cell.hideWarning()
                
                if let listStores = self.listaLojasProximas where listStores.count == 0 {
                    cell.showWarningWithoutStores()
                }
                
                if listaLojasProximas == nil {
                    cell.startLoading()
                }
            }
            
            let api = ApiServices()
            api.getUnitiesNearByToHome(userCoordinate, completion: { (listUnities, success) in
                
                var lojas = [Unity]()
                
                if let unities = listUnities as? [[String:AnyObject]] {
                    lojas = unities.map { Unity.init(value: $0) }
                } else if let list = listUnities {
                    if let unities = list["unities"] as? [[String:AnyObject]] {
                        lojas = unities.map { Unity.init(value: $0) }
                    }
                }
                
                if self.listaLojasProximas == nil {
                    if let cell = self.tableViewSections.cellForRowAtIndexPath(indexPath) as? BannerNewHomeTableViewCell {
                        cell.showWarningWithoutStores()
                    }
                }
                
                self.configureTableWith(lojas, withListIndexPath: indexPath)
            })
        } else {
            self.tableViewSections.reloadRowsAtIndexPaths([indexPath], withRowAnimation:.Fade)
            
            if let cell = self.tableViewSections.cellForRowAtIndexPath(indexPath) as? BannerNewHomeTableViewCell {
                cell.showWarningWithoutLocation()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenHome)
    }
    
    func configureTableWith(unities: [Unity], withListIndexPath indexPath: NSIndexPath) {
        let sortedUnities = unities
        
        if let banners = self.listaLojasProximas {
            let hasAddedUnities = sortedUnities.map { !banners.contains($0) }.reduce(false) { $0 || $1 }
            let hasRemovedUnities = banners.map { !sortedUnities.contains($0) }.reduce(false) { $0 || $1}
            
            if hasAddedUnities || hasRemovedUnities {
                dispatch_async(dispatch_get_main_queue()){
                    self.listaLojasProximas = sortedUnities
                    self.tableViewSections.sections[0]["actions"] = self.listaLojasProximas
                    self.tableViewSections.reloadRowsAtIndexPaths([indexPath], withRowAnimation:.Fade)
                }
            }
        } else {
            dispatch_async(dispatch_get_main_queue()){
                self.listaLojasProximas = sortedUnities
                self.tableViewSections.sections[0]["actions"] = self.listaLojasProximas
                self.tableViewSections.reloadRowsAtIndexPaths([indexPath], withRowAnimation:.Fade)
            }
        }
        
        if let cell = self.tableViewSections.cellForRowAtIndexPath(indexPath) as? BannerNewHomeTableViewCell {
            cell.stopLoading()
        }
    }
    
    @IBAction func buttonPayment4allPressed(sender: Button4allPayment) {
        if Lib4all().hasUserLogged() {
            //Make payment
            
            let qrViewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
            self.presentViewController(qrViewController, animated: true, completion: nil)
        }
        else{
            Lib4all().callLogin((self.sidePanelController.centerPanel as! UINavigationController).viewControllers[0] as! NewHomeViewController, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                self.didLogin()
            })
        }
    }
    
    //MARK: - Onboarding delegate
    func skipOnboarding() {
    }
    
    func signup(){
        if Lib4all().hasUserLogged() == false {
            Lib4all().callSignUp(self) { (phoneNumber, emailAddress, sessionToken) in
                SessionManager.sharedInstance.registerDevice()
            }
        }
    }
    
    func signin(){
        if Lib4all().hasUserLogged() == false {
            Lib4all().callLogin((self.sidePanelController.centerPanel as! UINavigationController).viewControllers[0] as! NewHomeViewController, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                self.didLogin()
            })
        }
    }
}
