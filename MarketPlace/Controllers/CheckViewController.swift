//
//  CheckViewController.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD
import RealmSwift

class CheckViewController: UIViewController, LoginDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var componentPayment: UIView!
    @IBOutlet weak var tableViewConstraintHeight: NSLayoutConstraint!
    
    var placeLabel = ""
    var idUnity = Int()
    var component4all : ComponentViewController!
    var check = Order()
    let api = ApiServices()
    var footerView = CheckFooter()
    var wasPaid = false
    var serverResponseReceived = false
    var rootViewController : UIViewController!
    var willLogin : Bool = false
    var paymentSuccess = false
    var jaDeletou = false
    var load: LoadingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        load = LoadingViewController.sharedManager() as! LoadingViewController
        
        self.paymentSuccess = false
        
        self.tableView.registerNib(UINib(nibName: "CheckTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_CHECK)
        self.tableView.registerNib(UINib(nibName: "CheckTotalTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_CHECK_TOTAL)
        Util.adjustTableViewInsetsFromTarget(self)
        
        self.title = kPayCheckTitle
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        self.load.startLoading(self, title: "Aguarde")
        self.api.getChecksFromServer(self.placeLabel,idUnity: self.idUnity)
        
        api.successCase = {(obj) in
            self.load.finishLoading({
                if let checkServer = obj as? NSDictionary
                {
                    var heightWrapper : CGFloat
                    if self.check.total <= 0{
                        heightWrapper = 280
                    } else {
                        heightWrapper = 60
                    }
                    self.serverResponseReceived = true
                    self.configurePaymentComponent()
                    OrderEntityManager.importFromDictionary(checkServer)
                    
                    self.check = OrderEntityManager.sharedInstance.getOrderById(checkServer.objectForKey("id") as! Int, idUnity: checkServer.objectForKey("idUnity") as! Int)!
                    self.tableView.reloadData()
                    
                    self.tableView.registerNib(UINib(nibName: "CartFooter", bundle: NSBundle.mainBundle()), forHeaderFooterViewReuseIdentifier: "CartFooter")
                    let fixWrapper = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, heightWrapper)) // dont remove
                    fixWrapper.clipsToBounds = true
                    self.configurePaymentComponent()
                    self.footerView = (NSBundle.mainBundle().loadNibNamed("CheckFooter", owner: self, options: nil)![0] as? CheckFooter)!
                    self.footerView.labelPrice?.text = Util.formatCurrency(Double(self.check.total/100))
                    self.footerView.labelPrice?.textColor = LayoutManager.white
                    self.footerView.labelTextPrice?.textColor = LayoutManager.white
                    self.footerView.imgViewCheck.tintImage(Util.hexStringToUIColor("#4FA444"))
                    self.footerView.frame = CGRect.init(x: self.footerView.frame.origin.x,
                        y: self.footerView.frame.origin.y,
                        width: self.tableView.frame.width,
                        height: self.footerView.frame.height)
                    fixWrapper.addSubview(self.footerView)
                    self.tableView.tableFooterView = fixWrapper
                    if self.check.total > 0 {
                        self.componentPayment.hidden = false
                        self.tableViewConstraintHeight.constant = 100
                        self.footerView.labelTextSuccess.hidden = true
                        self.footerView.labelTitleSuccess.hidden = true
                        self.footerView.imgViewCheck.hidden = true
                    } else {
                        self.componentPayment.hidden = true
                        self.tableViewConstraintHeight.constant = 0
                        self.view.layoutIfNeeded()
                        self.footerView.labelTextSuccess.hidden = false
                        self.footerView.labelTitleSuccess.hidden = false
                        self.footerView.imgViewCheck.hidden = false
                    }
                }
            })
        }
        
        api.failureCase = {(msg) in
            
            if self.check.id > 0 {
                let realm = try! Realm()
                realm.beginWrite()
                realm.delete(self.check)
                try! realm.commitWrite()
            }
            self.load.finishLoading({
                let alert = UIAlertController(title: kAlertTitle, message: kMessageFailCheckValue, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: kAlertButtonOk, style: .Cancel, handler: { (action: UIAlertAction!) in
                    self.dismissViewControllerAnimated(true, completion: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            })
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        Util.adjustInsetsFromCart(tableView)
        self.willLogin = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (paymentSuccess == false && Lib4all.sharedInstance().hasUserLogged() && self.check.id > 0) || (self.check.id > 0 && self.check.total == 0.0) {
            let realm = try! Realm()
            realm.beginWrite()
            realm.delete(self.check)
            try! realm.commitWrite()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configurePaymentComponent() {
        dispatch_async(dispatch_get_main_queue()) {
            self.component4all = ComponentViewController()
            self.component4all.buttonTitleWhenNotLogged = kEnter
            self.component4all.buttonTitleWhenLogged    = kPay
            self.component4all.requireFullName = true
            self.component4all.delegate = self
            self.component4all.view.frame = self.componentPayment.bounds
            self.componentPayment.addSubview(self.component4all.view)
            self.addChildViewController(self.component4all)
            self.component4all.didMoveToParentViewController(self)
        }
    }
    
    //MARK: - TableView Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if check.orderItems.count > 0 {
            return 2
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_CHECK,forIndexPath:indexPath) as! CheckTableViewCell
            
            let orderItem = check.orderItems[indexPath.row]
            cell.labelProduct?.text = orderItem.title != nil ? orderItem.title : ""
            cell.labelPrice?.text = Util.formatCurrency(Double(orderItem.total/100))
            cell.labelQuantity?.text = String(Int(orderItem.quantity))
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_CHECK_TOTAL,forIndexPath:indexPath) as! CheckTotalTableViewCell
            
            if indexPath.row == 0 {
                cell.labelTotal?.text = "Total da comanda"
                cell.labelPrice?.text = Util.formatCurrency(Double(self.check.total/100))
            } else {
                cell.labelTotal?.text = "Total dos descontos"
                cell.labelPrice?.text = Util.formatCurrency(Double(self.check.discount/100), prefixMessage: "- ")
            }
            
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if self.check.discount <= 0 {
                return 1
            } else {
                return 2
            }
        }
        return check.orderItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(56)
    }
    
    func callbackShouldPerformButtonAction() -> Bool {
        self.willLogin = true
        return true
    }
    
    
    //MARK: - Lib4all Delegate
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        SessionManager.sharedInstance.registerDevice()
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        let api = ApiServices()
        
        api.successCase = { (obj) in
            self.load.finishLoading({
                self.paymentSuccess = true
                
                let realm = try! Realm()
                realm.beginWrite()
                self.check.status = StatusOrder.Delivered.rawValue
                self.check.dthrLastStatus = NSDate.timeIntervalNow()
                self.check.timestamp = NSDate.timeIntervalNow()
                try! realm.commitWrite()
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let sucessoVc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                
                var logoUnity = ""
                var nameUnity = ""
                if let unity = UnityPersistence.getUnityByID(String(self.idUnity)) {
                    if let logo = unity.getLogo() {
                        logoUnity = logo.value!
                    }
                    
                    if let name = unity.name {
                        nameUnity = name
                    }
                }
                
                let labelParam = ["success": kMessageSuccessPayCheck,
                    "date": NSDate.getDateTodayToShortYearFormat(),
                    "time":NSDate.getTimeTodayToFormat(),
                    "title": kCheckPaymentSuccessTitle,
                    "type":TypePageSuccess.PagamentoPedido.rawValue,
                    "price":(self.check.total),
                    "idUnity":(self.check.idUnity),
                    "shop":nameUnity,
                    "imageUrl": logoUnity
                ]
                
                sucessoVc.loadView()
                (sucessoVc.viewControllers[0] as! ReceiptViewController).labels = labelParam
                (sucessoVc.viewControllers[0] as! ReceiptViewController).previousVC = self
                
                self.navigationController?.presentViewController(sucessoVc, animated: true){
                    let realm = try! Realm()
                    realm.beginWrite()
                    realm.delete(self.check)
                    try! realm.commitWrite()
                }
            })
        }
        
        api.failureCase = { (msg) in
            self.load.finishLoading({
                Util.showAlert(self, title: kAlertTitle, message: msg)
            })
        }
        
        let customerInfo : Dictionary<String, AnyObject> = ["sessionToken":sessionToken,"cardId":cardId,"paymentMode":paymentMode.rawValue]
        
        self.load.startLoading(self, title: "Aguarde")
        api.sendCheck(check, customerInfo: customerInfo)
    }
    
}
