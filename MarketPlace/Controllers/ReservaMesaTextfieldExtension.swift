//
//  ReservaMesaTextfieldExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension ReservaMesaViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.currentTextField = textField
        
        switch currentTextField!.tag {
        case 100:
            self.currentPickerDataSouce = arrayOptionLugares
            pickerView.reloadAllComponents()
        case 101:
            self.currentPickerDataSouce = arrayOptionDia
            pickerView.reloadAllComponents()
        case 102:
            self.currentPickerDataSouce = arrayOptionHorario
            pickerView.reloadAllComponents()
        default:
            break
        }
    }
}
