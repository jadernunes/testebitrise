//
//  SectionProfile.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 01/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class SectionProfile: UIView {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTitle.textColor = UIColor.darkGray4all()
        self.viewSeparator.backgroundColor = UIColor.green4all()
    }
    
    class func instanceFromNib() -> SectionProfile {
        return UINib(nibName: "SectionProfile", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! SectionProfile
    }
}
