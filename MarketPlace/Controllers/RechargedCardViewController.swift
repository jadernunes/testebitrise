//
//  RechargedCardViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 19/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class RechargedCardViewController: UIViewController {

    @IBOutlet weak var rechargePeriod: UILabel!
    @IBOutlet weak var valueRecharged: UILabel!
    @IBOutlet weak var cardRecharded: UILabel!
    @IBOutlet weak var rechargeInformationView: UIView!
    @IBOutlet weak var rechargePeriodInfoView: UIView!
    
    var rechargeValue: Int?
    var issuerNr: String?
    var period: String?
    var rootViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Recarga do cartão"
        
        let buttonClose = UIBarButtonItem(title: "Fechar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RechargedCardViewController.didPressCloseButton))
        self.navigationItem.setLeftBarButtonItem(buttonClose, animated: true)
        buttonClose.tintColor = UIColor.blackColor()
        
        self.rechargePeriod.text = self.period! + "h úteis"
        self.valueRecharged.text = "R$ \(self.rechargeValue!),00"
        self.cardRecharded.text = self.issuerNr
        
        self.rechargeInformationView.layer.cornerRadius = 5.0
        self.rechargeInformationView.layer.masksToBounds = true
        
        self.rechargePeriodInfoView.layer.cornerRadius = 5.0
        self.rechargePeriodInfoView.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didPressCloseButton(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
