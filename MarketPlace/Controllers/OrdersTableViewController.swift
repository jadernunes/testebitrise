 //
//  OrdersTableViewController.swift
//  MarketPlace
//
//  Created by Luciano on 8/18/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class OrdersTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrayOrders         = NSArray()
    var selectedOrder       : Order!
    var pullRefresh         : UIRefreshControl!
    let lib4all             = Lib4all()
    
    @IBOutlet weak var labelEmpty: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        arrayOrders = OrderEntityManager.sharedInstance.getOrders()

        tableView.registerNib(UINib(nibName: CELL_LIST_MY_ORDERS, bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_LIST_ORDERS)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = kOrdersTitle
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMyOrders)
        Util.adjustInsetsFromCart(tableView)
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setLayoutData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    private func setLayoutData() {
        self.title = kOrdersTitle
        
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(pullRefresh)
        
        tableView.tableFooterView = UIView(frame: CGRectZero)
        
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        self.tableView.backgroundColor = LayoutManager.backgroundFirstColor
        
        CartCustomManager.sharedInstance.hideCart()
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        Util.checkWithoutData(self.view, tableView: self.tableView, countData: self.arrayOrders.count, message:kMessageWithoutDataOrders)
    }
    
    @objc private func refresh() {
       // self.getOrdersFromServer()
        self.tableView.reloadData()
        self.pullRefresh.endRefreshing()
    }
    
    //MARK: - Get orders fomr server
    func getOrdersFromServer(){
        let api = ApiServices()
        
        api.successCase = {(arrayItems) in
            self.arrayOrders = OrderEntityManager.sharedInstance.getOrders()
            self.tableView.reloadData()
            self.pullRefresh.endRefreshing()
        }
        
        api.failureCase = { (message) in
            self.pullRefresh.endRefreshing()
        }
        
        if lib4all.hasUserLogged() {
//            api.getOrdersHistory()
        } else {
            self.pullRefresh.endRefreshing()
        }
    }
    
    // MARK: - Table view methods 
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOrders.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_ORDERS, forIndexPath: indexPath) as! OrderTableViewCell
        
        if let order = arrayOrders.objectAtIndex(indexPath.row) as? Order {
            if let unity = OrderEntityManager.sharedInstance.getOrderUnity(order.idUnity) {
                cell.labelPlaceName.text = unity.name
                cell.imageViewProduct.addImage(unity.getLogo()?.value) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
                    if image != nil {
                        cell.imageViewProduct.contentMode = .ScaleAspectFill
                    }
                }
                
            }else{
                cell.labelPlaceName.text = "#\(order.getIdentificator())"
            }
            
           
            
            var textToLabelItems = ""
            for orderItem in order.orderItems {
                if textToLabelItems == ""{
                    if let title = orderItem.unityItem?.title as String! {
                        textToLabelItems.appendContentsOf("\(title)")
                    }
                }
                else if orderItem.unityItem != nil {
                    textToLabelItems.appendContentsOf(" + \(orderItem.unityItem!.title)")
                }
            }
            cell.labelItems.text = textToLabelItems
            let totalCart = order.total / 100
            cell.labelPrice.text = "R$ " + String(format: "%.02f", totalCart)
        }
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let order = arrayOrders.objectAtIndex(indexPath.row) as! Order
        
        selectedOrder = order
        
        self.performSegueWithIdentifier("segueTrackOrder", sender: self)
    }

    //MARK: - Prepare for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueTrackOrder" {
            let destVc = segue.destinationViewController as! TrackingViewController
            destVc.title = UnityPersistence.getUnityByID(String(selectedOrder.idUnity))!.name
            destVc.order = selectedOrder
        }
    }

}
