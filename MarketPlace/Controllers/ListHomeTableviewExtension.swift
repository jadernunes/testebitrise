//
//  ListHomeTableviewExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension ListHomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.listItensSearched.count > 0
        {
            let dataObjectSearched = self.listItensSearched[indexPath.row]
            SessionManager.sharedInstance.searchOrQrEnabled = true
            SessionManager.sharedInstance.cardColor = LayoutManager.green4all
            
            if dataObjectSearched.type == "unity"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
                detailsVc.title = self.listItensSearched[indexPath.row].title
                detailsVc.idObjectReceived = dataObjectSearched.id
                detailsVc.typeObjet = TypeObjectDetail.Store
                detailsVc.hidesBottomBarWhenPushed = true
                self.showNavBar()
                detailsVc.title = dataObjectSearched.title
                self.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "movie"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MovieDetailsVC") as! MovieDetailsViewController
                
                let movie = MovieEntityManager.getMovieByID("\(dataObjectSearched.id)")
                
                detailsVc.hidesBottomBarWhenPushed = true
                detailsVc.movie = movie
                detailsVc.title = movie?.title
                detailsVc.viewDidLayoutSubviews()
                
                self.showNavBar()
                detailsVc.title = dataObjectSearched.title
                self.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "fact"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
                detailsVc.hidesBottomBarWhenPushed = true
                detailsVc.viewDidLayoutSubviews()
                detailsVc.title = self.listItensSearched[indexPath.row].title
                
                detailsVc.idObjectReceived = dataObjectSearched.id
                
                //sub tipo == promo
                if dataObjectSearched.subType == SubTypeFact.Event.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Event
                }
                else if dataObjectSearched.subType == SubTypeFact.Promotion.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Promotion
                }
                else if dataObjectSearched.subType == SubTypeFact.Warning.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Warning
                }
                self.showNavBar()
                detailsVc.title = dataObjectSearched.title
                self.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "group" {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
                viewController.hidesBottomBarWhenPushed = true
                viewController.titleToUse = dataObjectSearched.title
                viewController.viewDidLayoutSubviews()
                viewController.group = GroupEntityManager.getGroupCustom("id = \(dataObjectSearched.id)")
                viewController.subType = .Store
                self.showNavBar()
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        /* --- caso não haja resultados --- */
        if noResults {
            let cell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "noResults")
            cell.textLabel?.text = "Sem resultados."
            cell.textLabel?.font = LayoutManager.primaryFontWithSize(16)
            return cell
        }
        
        return self.generateSearchView(indexPath.row)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listItensSearched.count > 0 {
            return self.listItensSearched.count
        }
        else if noResults {
            return 1
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let heightCell : CGFloat = 90
        return heightCell
    }
    
    private func generateSearchView(positionListSearch: Int) -> SearchTableViewCell{
        
        let cell = self.tbView.dequeueReusableCellWithIdentifier("cellSearch") as! SearchTableViewCell
        
        let dataObjectSearched = self.listItensSearched[positionListSearch]
        cell.pupulateData(dataObjectSearched)
        
        return cell
    }
}
