//
//  DetailsTableViewController.swift
//  MarketPlace
//
//  Created by Luciano on 7/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD
import RealmSwift

class DetailsTableViewController: UIViewController {

    var localObject       : AnyObject!
    var idObjectReceived : Int!
    var typeObjet        : TypeObjectDetail!
    var rootVc           : UIViewController!
    var api              : ApiServices! = ApiServices()
    let loading = JGProgressHUD(style: .Dark)
    var isLoadingData    = true
    
    @IBOutlet weak var tableView: UITableView!
    
    //TableView Delegate and Source
    var source : AnyObject!

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        if isOneMarketplace {
            self.tableView.backgroundColor = LayoutManager.backgroundDark
        } else {
            self.tableView.backgroundColor = LayoutManager.white
        }
        
        self.rootVc = self.navigationController!.viewControllers.first
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self)
        
        self.loadData()
        if (self.localObject != nil) {
            self.showDetail()
        }
        
        if typeObjet == TypeObjectDetail.Store{
            CartCustomManager.sharedInstance.showCart(self)
        }
        
        Util.adjustInsetsFromCart(tableView)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.validadeWithoutdata()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //TODO: linkar UX na ação de listar Unities filhas
    func loadChildrenUnities() {
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        listVc.subType = SubContentType.Store
        self.navigationController?.pushViewController(listVc, animated: true)
    }
    
    //MARK: - Start load
    func startLoad() {
        self.loading.showInView(self.view)
    }

    //MARK: - Stop laod
    func stopLoad() {
        self.loading.dismiss()
    }
    
    //MAR: - Set local data and request to server
    private func loadData() {
        
        if (typeObjet != nil) {
            switch typeObjet! {
                
            case TypeObjectDetail.Service:
                self.localObject = UnityPersistence.getUnityByID("\(self.idObjectReceived)")
                self.getUnityByID()
                break
                
            case TypeObjectDetail.Store:
                self.localObject = UnityPersistence.getUnityByID("\(self.idObjectReceived)")
                self.getUnityByID()
                break
                
            case TypeObjectDetail.Event:
                self.localObject = FactEntityManager.getFactByID("\(self.idObjectReceived)")
                self.getFactByID()
                self.navigationController?.navigationItem.title = kDetailEventTitle
                break
                
            case TypeObjectDetail.Promotion:
                self.localObject = FactEntityManager.getFactByID("\(self.idObjectReceived)")
                self.getFactByID()
                self.navigationController?.navigationItem.title = kDetailPromotionTitle
                break
            
            case TypeObjectDetail.Warning:
                self.localObject = FactEntityManager.getFactByID("\(self.idObjectReceived)")
                self.getFactByID()
                self.navigationController?.navigationItem.title = kDetailWarningTitle
                break
                
            case TypeObjectDetail.Group:
                self.localObject = GroupEntityManager.getGroupById(self.idObjectReceived)
                self.getGroupByID()
                break
                
            case TypeObjectDetail.Campaign:
                self.localObject = CampaignPersitence.getCampaignById(self.idObjectReceived)
                self.getCampaignByID()
                self.navigationController?.navigationItem.title = kDetailCampaignTitle
                break
            }
        }
        self.tableView.reloadData()
    }
    
    //MARK: - Validade without data
    
    private func validadeWithoutdata(){
        if (typeObjet != nil) {
            switch typeObjet! {
                
            case TypeObjectDetail.Group:
                if isLoadingData == true {
                    isLoadingData = false
                    if (localObject as! Group).children.count > 0 ||
                        (localObject as! Group).unities.count > 0 {
                        checkWithoutData(1)
                    } else {
                        checkWithoutData(0)
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    //MARK: - Show Detail
    
    private func showDetail(){
        
        if (typeObjet != nil) {
            if localObject != nil {
                switch typeObjet! {
                    
                case TypeObjectDetail.Service:
                    
                    source = StoreDetailsSource(tableView: tableView, unity: localObject as! Unity,referenceToSuperViewController: self)
                    break
                    
                case TypeObjectDetail.Store:
                    
                    source = NewStoreDetailsSource(tableView: tableView, unity: localObject as! Unity, referenceToSuperViewController: self)
                    break
                    
                case TypeObjectDetail.Event:
                    CartCustomManager.sharedInstance.hideCart()
                    source = EventDetailsSource(tableView: tableView, fact: localObject as! Fact, rootVc: self)
                    break
                    
                case TypeObjectDetail.Promotion:
                    source = PromoDetailsSource(tableView: tableView, fact: localObject as! Fact)
                    break
                    
                case TypeObjectDetail.Warning:
                    break
                    
                case TypeObjectDetail.Group:
                    break
                    
                case TypeObjectDetail.Campaign:
                    source = CampaignDetailSource(tableView: tableView, campaign: localObject as! Campaign,referenceToSuperViewController: self)
                    break
                }
            }
        }
        
        trackerGoogleAnalytics()
        self.tableView.reloadData()
    }
    
    //MARK: - Cheking without data
    private func checkWithoutData(countItems:Int) {
        Util.checkWithoutData(self.view, tableView: self.tableView, countData: countItems, message: kMessageWithoutDataComercial)
    }
    
    //MARK: - Get object in Server
    
    //MARK: Unity
    private func getUnityByID() {
        api.successCase = {(obj) in
            do {
                let realm = try Realm()
                if realm.inWriteTransaction == false {
                    self.localObject = UnityPersistence.getUnityByID("\(self.idObjectReceived)")
                }
            } catch {
                print("########### Error when save in realm")
            }
            
            self.showDetail()
            self.validadeWithoutdata()
        }
        
        api.failureCase = {(msg) in
        }
        
        api.getUnityByIDService(idObjectReceived)
    }
    
    //MARK: - Fact
    private func getFactByID() {
        api.successCase = {(obj) in
            self.localObject = FactEntityManager.getFactByID("\(self.idObjectReceived)")
            self.showDetail()
            self.validadeWithoutdata()
        }
        
        api.failureCase = {(msg) in
        }
        
        api.getFactByID(idObjectReceived)
    }
    
    //MARK: - Group
    private func getGroupByID() {
        api.successCase = {(obj) in
            self.localObject = GroupEntityManager.getGroupById(self.idObjectReceived)
            self.showDetail()
            self.validadeWithoutdata()
        }
        
        api.failureCase = {(msg) in
        }
        
        api.getGroupByID(idObjectReceived)
    }
    
    //MARK: - Campaign
    func getCampaignByID() {
        api.successCase = {(obj) in
            self.localObject = CampaignPersitence.getCampaignById(self.idObjectReceived)
            self.showDetail()
            self.validadeWithoutdata()
        }
        
        api.failureCase = {(msg) in
        }
        
        let idUnity = (self.localObject as! Campaign).idUnity
        api.getCampaignByID(idUnity, idCampaign: idObjectReceived)
    }
    
    //MARK: - Google Analytics
    private func trackerGoogleAnalytics() {
        if self.localObject != nil {
            if (typeObjet != nil) {
                switch typeObjet! {
                case TypeObjectDetail.Service,TypeObjectDetail.Store:
                    
                    //Unity
                    let object = self.localObject as! Unity
                    GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(object.name, idUnity: String(object.id))
                    break
                    
                case TypeObjectDetail.Event,TypeObjectDetail.Promotion,TypeObjectDetail.Warning:
                    
                    //Fact
                    let object = self.localObject as! Fact
                    if object.idUnity > 0 {
                        GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(object.title, idUnity: String(object.idUnity))
                    } else {
                        GoogleAnalytics.sharedInstance.trakingScreenWithName(object.title)
                    }
                    break
                    
                case TypeObjectDetail.Group:
                    //Grupo
                    break
                    
                case TypeObjectDetail.Campaign:
                    let object = self.localObject as! Campaign
                    if object.idUnity > 0 {
                        GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(ScreensName.screenCoupons + " - " + object.shortDesc, idUnity: String(object.idUnity))
                    } else {
                        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenCoupons + " - " + object.shortDesc)
                    }
                    break
                default:
                    break
                }
            }
        }
    }
}
