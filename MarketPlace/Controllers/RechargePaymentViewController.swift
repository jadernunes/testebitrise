//
//  RechargePaymentViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 19/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class RechargePaymentViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, LoginDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var viewSelectedCard: UIView!
    @IBOutlet weak var lbCardNr: UILabel!
    @IBOutlet weak var lbCardName: UILabel!
    @IBOutlet weak var btAgreeTax: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var viewTax: UIView!
    @IBOutlet weak var lbFee: UILabel!
    @IBOutlet weak var lbCardBalance: UILabel!
    @IBOutlet weak var imCardThumb: UIImageView!
    @IBOutlet weak var lbCardBalanceDate: UILabel!
    @IBOutlet weak var spaceInfoCard: NSLayoutConstraint!
    @IBOutlet weak var viewInfoCard: UIView!
    @IBOutlet weak var lastUpdated: UILabel!
    @IBOutlet weak var lbCardBalanceCents: UILabel!
    @IBOutlet weak var imArrow: UIImageView!
    
    var selectedIndexPath: NSIndexPath?
    let offsetButton4all: CGFloat = 10.0
    var arrayValues = NSArray()
    var issuerId: Int?
    var productId: Int?
    var issuerNr: String?
    var issuerName: String?
    var fee:Double = 0.0
    var idRecharge: Int?
    var cardBalance: Int?
    var cardBalanceDate: Int64?
    var cardThumb: String?
    var cardBalanceCents: Int?
    
    /// 4All lib
    var vc: ComponentViewController?
    var vcLoading: LoadingViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(RechargePaymentViewController.goBack))
        self.navigationItem.title = "Recarga do cartão"
        
        
        self.lbCardNr.text = self.issuerNr
        self.lbCardName.text = self.issuerName
        
        self.viewInfoCard.hidden = true
        self.imCardThumb.sd_setImageWithURL(NSURL(string: cardThumb!))
        self.lbCardBalance.text = String(cardBalance!)
        self.lbCardBalance.font = LayoutManager.primaryFontWithSize(30)
        if (cardBalanceCents == 0) {
            self.lbCardBalanceCents.text = ",00"
        } else {
            self.lbCardBalanceCents.text = ",\(cardBalanceCents!)"
        }
        if (self.cardBalanceDate == 0 || self.cardBalanceDate == -1) {
            self.lbCardBalanceDate.hidden = true
            self.lastUpdated.hidden = true
        } else {
            let balanceDate = NSDate(timeIntervalSince1970: Double(self.cardBalanceDate!) / 1000.0)
            let objDateFormatter = NSDateFormatter()
            objDateFormatter.dateFormat = "dd/MM/yyyy - hh:mm"
            self.lbCardBalanceDate.text = "\(objDateFormatter.stringFromDate(balanceDate))"
        }
        
        vc = nil
        
        
        
        //Add tap gesture to toggleBt
        let oneTouch: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RechargePaymentViewController.toggleAgreeWithTax))
        oneTouch.numberOfTouchesRequired = 1
        self.viewTax.addGestureRecognizer(oneTouch)
        
        self.clear()
        
        //Add tap gesture to lbFee
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RechargePaymentViewController.openTerms))
        tap.numberOfTouchesRequired = 1
        self.lbFee.addGestureRecognizer(tap)
        //self.lbFee.userInteractionEnabled = true
        
        //Add tap gesture to viewSelectedCard
        let tapCard:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RechargePaymentViewController.openViewSelectedCard))
        self.viewSelectedCard.addGestureRecognizer(tapCard)
 
        let api = ApiServicesMobility()
        
        api.successCase = { (obj) in
            if let listValues = obj as? NSArray {
                self.arrayValues = NSArray(array: listValues)
                self.collectionView.reloadData()
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
        }
        
        api.failureCase = { (cod, msg) in
            //[MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        api.buscaValores(NSString.init(format: "%d", issuerId!) as String, productId: NSString.init(format: "%d", productId!) as String)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openViewSelectedCard(){
        if (self.viewInfoCard.hidden == true) {
            UIView.animateWithDuration(0.5, animations: {
                //unhide
                self.spaceInfoCard.constant += self.viewInfoCard.frame.height
                self.viewInfoCard.hidden = false
                self.imArrow.image = UIImage(named: "iconArrowUp")
            })
        } else {
            UIView.animateWithDuration(0.5, animations: {
                //hide
                self.spaceInfoCard.constant -= self.viewInfoCard.frame.height
                self.viewInfoCard.hidden = true
                self.imArrow.image = UIImage(named: "iconArrowDown")
            })
        }
    }
    
    @IBAction func lbFeePressed(sender: AnyObject) {
        self.openTerms()
    }
    
    @IBAction func agreeTaxPressed(sender: AnyObject) {
        self.toggleAgreeWithTax()
    }
    
    func openTerms(){
        let termsVC = self.storyboard?.instantiateViewControllerWithIdentifier("TERMS")
        self.navigationController?.presentViewController(termsVC!, animated: true, completion: nil)
        
    }
    
    func toggleAgreeWithTax() {
        self.btAgreeTax.highlighted = !self.btAgreeTax.highlighted
        
        if (btAgreeTax.highlighted) {
            self.show4allPayment()
        } else {
            self.hide4allPayment()
        }
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func show4allPayment() {
        
        if (vc == nil) {
            vc = ComponentViewController()
        }
        
        self.vc!.view.frame = CGRectMake(self.offsetButton4all, self.view.frame.size.height - 100, self.view.frame.size.width-(2*self.offsetButton4all), 70)
        
        //Set delegate para callbacks pre e pós venda
        vc!.delegate = self
        
        //Define o titulo do botão do componente
        vc!.buttonTitleWhenNotLogged = "CRIAR CONTA";
        
        //Define o titulo do botão após estar logado
        vc!.buttonTitleWhenLogged = "FAZER RECARGA";
        
        
        
        //Adiciona view do component ao controller
        self.view.addSubview(vc!.view)
        let contraint = NSLayoutConstraint(item: self.vc!.view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.viewTax, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 20)
        
        
        
        //Adiciona a parte funcional ao container
        self.addChildViewController(vc!)
        vc!.didMoveToParentViewController(self)
        
        UIView.animateWithDuration(0.5, animations: {
                self.vc!.view.frame = CGRectMake(self.offsetButton4all, self.view.frame.size.height - 100, self.view.frame.size.width-(2*self.offsetButton4all), 70)
                self.view.addConstraint(contraint)
        })
    }
    
    func hide4allPayment() {
        
        UIView.animateWithDuration(0.5, animations: {
            self.vc!.view.frame = CGRectMake(self.offsetButton4all, self.view.frame.size.height, self.view.frame.size.width-(2*self.offsetButton4all), 140)
            let contraint = NSLayoutConstraint(item: self.vc!.view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.viewTax, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 20)
            self.view.removeConstraint(contraint)
        })
    }
    
    func showTaxView(){
        
        let paymentValue = arrayValues[self.selectedIndexPath!.row] as! NSDictionary
        
        self.fee = paymentValue["feeValue"] as! Double / 100.0;
        
        let attributedString = NSMutableAttributedString.init(string: "Concordo com os ")
        let attributedString2 = NSMutableAttributedString.init(string: "termos de uso", attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue, NSBackgroundColorAttributeName: UIColor.clearColor()])
        attributedString.appendAttributedString(attributedString2)
        let attributedString3 = NSMutableAttributedString.init(string: String(format: " e com a taxa de conveniência de R$ %1.2f", fee))
        attributedString.appendAttributedString(attributedString3)
        
        lbFee.attributedText = attributedString
        
        if (self.viewTax.hidden && fee > 0) {
            
            /*To unhide*/
            let rect: CGRect = self.viewTax.frame
            viewTax.frame =  CGRectMake(rect.origin.x, rect.origin.y + 100, 0, 0)
            viewTax.hidden = false
            viewTax.alpha = 0.0
            
            self.toggleAgreeWithTax()
            
            UIView.animateWithDuration(0.25, animations: {
                self.viewTax.frame =  rect
                self.viewTax.alpha = 1.0
                })
        }
    }
    
    func hideTaxView() {
        
        let rect: CGRect = self.viewTax.frame
        UIView.animateWithDuration(0.3, animations: {
            self.viewTax.alpha = 0.0
            self.btAgreeTax.highlighted = false
            }, completion: { (finished) in
                self.viewTax.hidden = true
                self.viewTax.alpha = 1.0
                self.viewTax.frame =  rect
            })
    }
    
    func clear() {
        selectedIndexPath = nil
        self.viewTax.hidden = true
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayValues.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let paymentValue = self.arrayValues[indexPath.row] as! NSDictionary
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PaymentCell", forIndexPath: indexPath)
        
        let lbValue = cell.viewWithTag(100) as! UILabel
        let lbCents = cell.viewWithTag(101) as! UILabel
        let lbCurrency = cell.viewWithTag(102) as! UILabel
        
        lbValue.font = LayoutManager.primaryFontWithSize(30)
        //lbCents.font = LayoutManager.primaryFontWithSize(12)
        
        lbValue.text = String(format: "%1.0f", Double(paymentValue["value"] as! NSNumber))

        if (selectedIndexPath != nil && indexPath.compare(self.selectedIndexPath!) == .OrderedSame) {
            lbValue.textColor = UIColor.whiteColor()
            cell.backgroundColor = SessionManagerMobility.sharedInstance.mainColor
            //[cell setBackgroundColor:[C4ColorUtil colorFromHexString:@"#ddbb00"]];
        } else {
            cell.backgroundColor = UIColor.whiteColor()
            lbValue.textColor = UIColor.darkGrayColor()
        }
        
        lbCents.textColor = lbValue.textColor
        lbCurrency.textColor = lbValue.textColor
        
        cell.layer.cornerRadius = 5.0
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let indexPaths = NSMutableArray.init(objects: indexPath)
        
        if (self.selectedIndexPath != nil) {
            if (indexPath.compare(self.selectedIndexPath!) == .OrderedSame) {
                self.selectedIndexPath = nil;
            } else {
                indexPaths.addObject(self.selectedIndexPath!)
                self.selectedIndexPath = indexPath
            }
        } else {
            self.selectedIndexPath = indexPath
        }
        
        let returnIndexPaths:[NSIndexPath] = NSArray.init(array: indexPaths) as! [NSIndexPath]
        
        collectionView.reloadItemsAtIndexPaths(returnIndexPaths)
        
        if (self.selectedIndexPath != nil) {
            self.showTaxView()
        } else {
            self.hideTaxView()
            self.hide4allPayment()
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenRect: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenRect.size.width
        let cellWidth = screenWidth / 3.6; //Replace the divisor with the column count requirement
        let size = CGSizeMake(cellWidth, cellWidth)
        
        return size;
    }
    
    func callbackLogin(sessionToken: String, email: String, phone: String) {
        //[MBProgressHUD showHUDAddedTo:self.view animated:true];
        SessionManagerMobility.sharedInstance.syncCardsWithServer({
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        })
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        self.vcLoading = LoadingViewController()
        self.vcLoading?.startLoading(self, title: "Aguarde...")
        
        let dicUser = NSDictionary(dictionary: ["sessionToken": sessionToken, "cardId": cardId, "paymentMode": paymentMode.rawValue])
        
        self.processPayment(dicUser)
    }
    
    func processPayment(dicUser: NSDictionary) {
        
        let paymentValue: NSDictionary = arrayValues[selectedIndexPath!.row] as! NSDictionary
        let paymentType = dicUser["paymentMode"] as! Int
        
        let rechargeParameters:NSDictionary = ["sessionToken": dicUser["sessionToken"]!,
            "cardId": dicUser["cardId"]!,
            "paymentMode": dicUser["paymentMode"]!,
            "amount": NSNumber.init(long:paymentValue["value"] as! Int * 100),
            "issuerId": NSNumber.init(long: self.issuerId!),
            "productId": NSNumber.init(long: self.productId!),
            "issuerNr": self.issuerNr!,
            "fee": paymentValue["feeValue"]!] as NSDictionary
        
        //TODO:
        let api = ApiServicesMobility()
        
        api.successCase = { (obj) in
            
            if let response = obj as? NSDictionary {
                
                if (paymentType == PaymentMode.Credit.rawValue) {
                    
                    if let message = response["message"] as? String {
                        let alert = UIAlertView.init(title: "Atenção", message: message, delegate: self, cancelButtonTitle: "OK")
                        alert.show()
                        return
                    }
                   
                    self.rechargeSuccess()
                    
                } else if (paymentType == PaymentMode.Debit.rawValue) {
                    if let debitTransactionUrl = response["debitTransactionURL"] as? String {
                        self.processDebitPayment(NSURL.init(string: debitTransactionUrl)!)
                    } else {
                        var message = "Não foi possível completar a recarga. Por favor, tente novamente."
                        self.vcLoading!.finishLoading(nil)
                        if let msg = response["message"] as? String {
                            message = msg
                        }
                        
                        let alert = UIAlertView.init(title: "Atenção", message: message, delegate: self, cancelButtonTitle: "OK")
                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                        alert.show()
                    }
                }
            }
        };
        
        api.failureCase =  { (cod, msg) in
            
            self.vcLoading!.finishLoading(nil)
            
            let codInt = Int(cod)
            var msgAlert = ""
            
            switch (codInt!) {
            case 2:
                msgAlert = "Não foi possível completar sua recarga. Pagamento não autorizado."
                break;
            case 4:
                msgAlert = "Desculpe, transação de recarga indisponível no momento. Tente novamente em alguns minutos."
                break;
            case 5:
                msgAlert = "Desculpe, seu limite de recargas foi excedido."
                break;
            case 6:
                msgAlert = "Desculpe, seu limite de recargas foi excedido."
                break;
            case 7:
                msgAlert = "Desculpe, seu limite de recargas foi excedido. Tente novamente dentro algumas horas."
                break;
            case 8:
                msgAlert = "Desculpe, seu limite de recargas foi excedido."
                break;
            case 14:
                msgAlert = "Você já possui um cartão desta operadora vinculado à sua conta 4all. Para carregar o cartão de outra pessoa, acesse o Menu, clique em Perfil e ative o Perfil Família."
                break;
            case 17:
                msgAlert = "Este cartão não pode ser recarregado. Em caso de dúvidas, entre em contato com a sua operadora de transporte."
                break;
            default:
                msgAlert = "Desculpe, ocorreu um erro inesperado. Entre em contato com o nosso atendimento."
                break;
            }
            let alert = UIAlertView.init(title: "Atenção", message: msgAlert, delegate: self, cancelButtonTitle: "OK")
            alert.show()
            
        }
        api.recharge(rechargeParameters as [NSObject : AnyObject])
    }
    
    func rechargeSuccess(){
  
        let paymentValue = arrayValues[selectedIndexPath!.row]

        //Saving Recharge History
        let card = CardModelPersistence.getProductIfExists(self.issuerNr!)
        let recharge = CardRechargeModel()
        recharge.valuePaid = (paymentValue["value"] as? Double)!
        recharge.date = NSDate()
        recharge.issuerId = self.issuerId!
        recharge.productId = self.productId!
        recharge.issuerNr = self.issuerNr!
        recharge.issuerThumb = card.thumb;
        recharge.productName = card.productName;
        
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(recharge, update: true)
        try! realm.commitWrite()
        
        let navigation = self.storyboard!.instantiateViewControllerWithIdentifier("RECHARGE_SUCCESS") as! UINavigationController
        let rechargedVc = navigation.viewControllers[0] as! RechargedCardViewController
        rechargedVc.rechargeValue = (paymentValue["value"] as? Int)!
        rechargedVc.issuerNr = self.issuerNr!
        
        if(self.issuerId == 5 || self.issuerId == 9) { //Cartão LEGAL e TEM
            rechargedVc.period = "72"
        } else { //Outros Cartões
            rechargedVc.period = "48"
        }
        rechargedVc.rootViewController = self
        
        self.vcLoading!.finishLoading({
            self.navigationController!.presentViewController(navigation, animated: true, completion: {
                self.navigationController!.popToRootViewControllerAnimated(true)
        })
        })
    }
    
    func processDebitPayment(urlPayment: NSURL){
        
        //Use lib 4all to payment debit mode
        let lib = Lib4all.sharedInstance()
        
        self.vcLoading!.finishLoading(nil)
        
        lib.openDebitWebViewInViewController(self, withUrl: urlPayment, completionBlock: { (success) in
            
            
            let api = ApiServicesMobility()
            
            api.successCase = { (obj) in

                //Execute processes payment flow
                self.rechargeSuccess()
            }
            
            api.failureCase =  { (cod,msg) in
                self.vcLoading!.finishLoading(nil)
                let alert = UIAlertView.init(title: "Atenção", message: "Não foi possível completar a transação, por favor tente novamente ou revise sua forma de pagamento.", delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
            let numberIdRecharge = NSNumber(long: self.idRecharge!)
            var statusWebView = "false"
            
            if(success) {
                statusWebView = "true"
            
                self.vcLoading = LoadingViewController()
                self.vcLoading!.startLoading(self, title:"Aguarde...")
            
                let dicCompRechager = NSDictionary(dictionary: ["idRecharge": numberIdRecharge, "success": statusWebView])
                api.completeDebitRecharge(dicCompRechager as [NSObject : AnyObject])
            
            } else {
                self.vcLoading = LoadingViewController()
                self.vcLoading!.startLoading(self, title: "Aguarde...")
            
                //Finish loading
                self.vcLoading!.finishLoading(nil)
            
                let messagePaymentStatus = "Ocorreu um problema em seu pagamento. \nTente novamente!";
            
                let alertPaymentStatus = UIAlertView.init(title: "Status Pagamento", message: messagePaymentStatus, delegate: self, cancelButtonTitle: "OK")
                alertPaymentStatus.show()
            }
        })
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
