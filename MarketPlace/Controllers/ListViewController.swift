//
//  ListFactsTableViewController.swift
//  MarketPlace
//
//  Created by Luciano on 7/7/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ListViewController: UIViewController {

    @IBOutlet weak var tableView                    : UITableView!
    @IBOutlet weak var segmentedControl             : UISegmentedControl!
    @IBOutlet weak var constraintHeightSegmented    : NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewHeight    : NSLayoutConstraint!
    
    var source                      : NSObject!
    var listUnities                 : List<Unity>?
    var group                       : Group?
    var subType                     : SubContentType!
    var segmentedChanged            : Bool = false
    var isListAllItems              : Bool = false
    var preferredStatusStyle        : UIStatusBarStyle = .Default
    var titleToUse                  : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)
        
        self.view.backgroundColor   = LayoutManager.backgroundFirstColor
        tableView.backgroundColor   = LayoutManager.backgroundFirstColor
        segmentedControl.tintColor  = SessionManager.sharedInstance.cardColor
        constraintHeightSegmented.constant = 28
        segmentedControl.hidden            = false
        constraintTableViewHeight.constant = 52
        let attr = NSDictionary(object: LayoutManager.primaryFontWithSize(13), forKey: NSFontAttributeName)
        segmentedControl.setTitleTextAttributes(attr as [NSObject : AnyObject] , forState: .Normal)
        
        group?.loadDataFromRealm()
        
        if group?.groupTreeTypeName != nil && group?.groupTreeTypeName?.characters.count > 0 {
            subType = SubContentType(rawValue: group!.groupTreeTypeName!)
        }
        
        loadContent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        CartCustomManager.sharedInstance.showCart(self)
        self.trakScreenOnGoogleAnalytics()
        Util.adjustInsetsFromCart(tableView)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.preferredStatusStyle
    }
    
    //MARK: Google Analytics
    private func trakScreenOnGoogleAnalytics() {
        if let titleString = self.title {
            if !titleString.containsString(ScreensName.screenStores) {
                self.title = ScreensName.screenStores + " \(titleString)"
            }
        } else {
            self.title = ScreensName.screenStores
        }
        GoogleAnalytics.sharedInstance.trakingScreenWithName(self.title)
        
        if subType != nil && group == nil{
            switch subType!{
            case .Promo:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenPromotions)
                break
                
            case .Event:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenEvents)
                break
                
            case .Movie:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMovies)
                break

            case .Service:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenServices)
                break

            case .Store:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenStores)
                break

            case .Foodcourt:
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenFoodCort)
                break
                
            default:
                break
            }
        }else{
            if subType != nil {
                if subType == .Service {
                    GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenServices)
                }else if subType == .Store {
                    GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenStores)
                }else if subType == .Foodcourt {
                    GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenFoodCort)
                }
            } else if group != nil {
                GoogleAnalytics.sharedInstance.trakingScreenWithName(group?.name)
            }
        }
    }
    
    func loadContent() -> Void {
        
        if subType != nil && group == nil{
            switch subType!{
            case .Promo:
                source = PromoListSource(tableView: tableView, showUpcoming: !segmentedChanged, referenceViewController: self)
                segmentedControl.setTitle(kActive, forSegmentAtIndex: 0)
                segmentedControl.setTitle(kFinished, forSegmentAtIndex: 1)
                tableView.dataSource = source as! PromoListSource
                tableView.delegate   = source as! PromoListSource
                self.navigationItem.title = kPromotions
                break
                
            case .Event:
                source = EventListSource(tableView: tableView, showUpcoming: !segmentedChanged, referenceViewController: self)
                segmentedControl.setTitle(kActive, forSegmentAtIndex: 0)
                segmentedControl.setTitle(kFinished, forSegmentAtIndex: 1)
                tableView.dataSource = source as! EventListSource
                tableView.delegate   = source as! EventListSource
                self.navigationItem.title = kEvents
                break
                
            case .Movie:
                segmentedControl.hidden = true
                constraintHeightSegmented.constant = 0
                constraintTableViewHeight.constant = 0
                self.navigationItem.title = kMovies
                source = MovieListSource(tableView: tableView, rootVc: self)
                tableView.dataSource = source as! MovieListSource
                tableView.delegate   = source as! MovieListSource
                break
                
            case .Store:
                segmentedControl.hidden = true
                constraintHeightSegmented.constant = 0
                constraintTableViewHeight.constant = 0
                self.navigationItem.title = kStores
                break
            
            case .Service:
                segmentedControl.hidden = true
                constraintHeightSegmented.constant = 0
                constraintTableViewHeight.constant = 0
                self.navigationItem.title = kServices
                break
                
            default:
                break
            }
            
            tableView.reloadData()
        }else{
            //Check the need of showing the selector
            if group?.children.count == 0 || group?.unities.count == 0 {
                segmentedControl.hidden = true
                constraintHeightSegmented.constant = 0
                constraintTableViewHeight.constant = 0
                segmentedChanged = group?.children.count > 0 ? false : true
            }
            
            segmentedControl.setTitle(kCategories, forSegmentAtIndex: 0)
            segmentedControl.setTitle(kFullList, forSegmentAtIndex: 1)
            
            self.navigationItem.title = ""
            
            if subType != nil {
                if subType == .Service {
                    self.navigationItem.title = kServices
                }else if subType == .Store {
                    self.navigationItem.title = kStores
                }else if subType == .Foodcourt {
                    self.navigationItem.title = kFoodcourt
                }
            }
            
            //Check the need of showing the selector
            if group?.children.count == 0 || group?.unities.count == 0 {
                segmentedControl.hidden = true
                constraintHeightSegmented.constant = 0
                constraintTableViewHeight.constant = 0
                segmentedChanged = group?.children.count > 0 ? false : true
            }
            
            if segmentedControl.hidden == true {
                isListAllItems = segmentedControl.hidden
            }
            
            if group != nil {
                let src = UnityListSource(tableView: tableView, rootVc: self, group: group!, showChildren: !segmentedChanged, listAllItems:isListAllItems)
                source = src
                
                tableView.dataSource = source as! UnityListSource
                tableView.delegate   = source as! UnityListSource
            }
            
            tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - SegmentedControl
    @IBAction func segmentedValueChanged(sender: AnyObject) {
        if (sender as! UISegmentedControl).selectedSegmentIndex == 0 {
            segmentedChanged = false
        } else{
            segmentedChanged = true
        }
        
        loadContent()
    }
    
    
}
