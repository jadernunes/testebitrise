//
//  StoreDetailsSource_new.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 27/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

protocol ConfigurableWithUnity {
    func configureWithUnity(unity: Unity, withReference: UIViewController?)
}

class NewStoreDetailsSource: NSObject {

    private var heightNewUnityDetailsItemsTableViewCell:    CGFloat = 0.0
    private var heightCellListFactsCarousel:                CGFloat = 0.0
    private var heightNewUnityDetailsShiftsTableViewCell:   CGFloat = 0.0
    private var heightNewUnityDetailsContactTableViewCell:  CGFloat = 0.0
    
    var tableView:  UITableView!
    
    private var referenceToSuperViewController: UIViewController!
    
    private var unity:      Unity!
    
    private var loadUnityOrderIsFinished = false
    private var loadSchedulesIsFinished = false
    
    private var categories = [[String: AnyObject]]()
    
    init(tableView:                         UITableView,
         unity:                             Unity!,
         referenceToSuperViewController:    UIViewController!) {
        super.init()
        
        self.tableView  = tableView
        self.unity      = unity
        
        self.tableView.dataSource       = self
        self.tableView.delegate         = self
        self.tableView.backgroundColor  = LayoutManager.backgroundLight

        self.registerNibs()

        self.referenceToSuperViewController = referenceToSuperViewController
        self.referenceToSuperViewController.title = self.unity.name ?? ""
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.loadCategories()
            self.loadSchedules()
        }
    }
    
    private func registerNibs() {
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_BANNER_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier:NEW_CELL_UNITY_DETAILS_BANNER_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_ITEMS_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_ITEMS_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: CELL_LIST_FACTS_IN_UNITY,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier: CELL_LIST_FACTS_IN_UNITY_CELL_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_SHIFTS_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_SHIFTS_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_CONTACT_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_CONTACT_IDENTIFIER)
    }
    
    private func loadSchedules() {
        let api = ApiServices()
        
        api.successCase = { ( content ) in
            if let content = content as! [String: AnyObject]? {
                SessionManager.sharedInstance.unity = self.unity
                SessionManager.sharedInstance.idUnitToSchedule = self.unity.id
                
                if !self.categories.isEmpty {
                    
                    NSNotificationCenter.defaultCenter()
                        .postNotificationName(UNITY_SCHEDULE_OBSERVER, object: nil, userInfo: ["categories" : self.categories])
                    
                    if let professionals = content["professionals"] as?
                        [AnyObject] {
                        SessionManager.sharedInstance.listCalendar = professionals
                    }
                    
                    if let unityShifts = content["unityShifts"] as?
                        [String: AnyObject] {
                        SessionManager.sharedInstance.unityShifts = unityShifts
                        
                    }
                }
            }
            self.loadSchedulesIsFinished = true
        }
        
        api.failureCase = { ( error ) in
            self.loadSchedulesIsFinished = true
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            if let _ = self.unity {
                api.calendarByUnit(self.unity.id)
            }
        }
    }
    
    private func loadCategories() {
        let api = ApiServices()
        
        api.successCase = { ( content ) in
            
            self.loadUnityOrderIsFinished = true

            if let content = content as? [[String: AnyObject]] where !content.isEmpty {
                
                self.categories = content
                
                NSNotificationCenter.defaultCenter()
                    .postNotificationName(UNITY_ITEMS_OBSERVER, object: nil, userInfo: ["categories" : content])
            }
        }
        
        api.failureCase = { ( error ) in
            self.loadUnityOrderIsFinished = true
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            api.listUnityCategories(self.unity.id)
        }
    }
}

extension NewStoreDetailsSource : UITableViewDataSource {
    func tableView(tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier (NEW_CELL_UNITY_DETAILS_BANNER_IDENTIFIER) as! NewUnityDetailsBannerTableViewCell
            cell.configureWithUnity(self.unity, withReference: nil)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier (NEW_CELL_UNITY_DETAILS_ITEMS_IDENTIFIER) as! NewUnityDetailsItemsTableViewCell
            cell.configureWithUnity(self.unity,
                    withReference: self.referenceToSuperViewController)
            self.heightNewUnityDetailsItemsTableViewCell = cell.getHeight()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_FACTS_IN_UNITY_CELL_IDENTIFIER) as! CellListFactsCarousel
            cell.configureWithUnity(self.unity,
                    withReference: self.referenceToSuperViewController)
            self.heightCellListFactsCarousel = cell.getHeight()
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier (NEW_CELL_UNITY_DETAILS_SHIFTS_IDENTIFIER) as! NewUnityDetailsShiftsTableViewCell
            cell.configureWithUnity(self.unity, withReference: nil)
            self.heightNewUnityDetailsShiftsTableViewCell = cell.getHeight()
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier(NEW_CELL_UNITY_DETAILS_CONTACT_IDENTIFIER) as! NewUnityDetailsContactTableViewCell
            cell.configureWithUnity(self.unity,
                    withReference: self.referenceToSuperViewController)
            self.heightNewUnityDetailsContactTableViewCell = cell.getHeight()
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension NewStoreDetailsSource : UITableViewDelegate {
    func tableView(tableView: UITableView,
                   heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return NewUnityDetailsBannerTableViewCell.getHeight()
        case 1:
            return self.heightNewUnityDetailsItemsTableViewCell
        case 2:
            return self.heightCellListFactsCarousel
        case 3:
            return self.heightNewUnityDetailsShiftsTableViewCell
        case 4:
            return self.heightNewUnityDetailsContactTableViewCell
        default:
            return 0.0
        }
    }
}
