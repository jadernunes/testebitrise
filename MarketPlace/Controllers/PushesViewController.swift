//
//  PushesViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift
class PushesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelNotifications: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var pullRefresh: UIRefreshControl!
    var listPushes : Results<Push>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        self.tableView.backgroundColor = UIColor.clearColor()
        let px = 1 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, 0, self.tableView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.tableView.tableHeaderView = line
        line.backgroundColor = self.tableView.separatorColor
        
        CartCustomManager.sharedInstance.hideCart()
        self.navigationItem.title = kMyPushesTitle
        self.tableView.delegate = self
        self.tableView.dataSource = self
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(pullRefresh)
        self.tableView.registerNib(UINib(nibName: "PushTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_LIST_PUSHES)
        self.tableView.reloadData()
        labelUsername.font = LayoutManager.primaryFontWithSize(26)
        labelNotifications.font = LayoutManager.primaryFontWithSize(26)
        labelUsername.textColor = LayoutManager.backgroundDark
        labelNotifications.textColor = LayoutManager.backgroundDark
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(PushesViewController.refresh),
            name: NOTIF_PUSHES_UPDATE,
            object: nil)
        
        //Get token by user loged
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.fullName != nil {
                    if user.fullName.characters.count > 0 {
                        self.refresh()
                        self.labelNotifications.hidden = false
                        self.labelUsername.text = (user.fullName.componentsSeparatedByString(" ").first!) + ","
                    }
                }
            }
        } else {
            self.labelUsername.text = kMessageWithoutDataPushes
            self.labelNotifications.hidden = true
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenNotifications)
    }
    
    func refresh() {
        // Code to refresh table view
        if Lib4all().hasUserLogged()  {
            if let _ = User.sharedUser().fullName {
                if User.sharedUser().fullName?.characters.count > 0 {
                    self.listPushes = PushPersistence.getAllPushes().sorted("id", ascending: false)
                    self.pullRefresh.endRefreshing()
                    self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
                    if self.listPushes!.count != 1 {
                        labelNotifications.text = "você tem " + String(self.listPushes!.count) + " notificações."
                    }
                    else {
                        labelNotifications.text = "você tem " + String(self.listPushes!.count) + " notificação."
                    }
                }
            }
        }
        self.pullRefresh.endRefreshing()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listPushes != nil {
            return  self.listPushes!.count
        }
        return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellPush", forIndexPath: indexPath) as! PushTableViewCell
        cell.configureCell(listPushes![indexPath.row])
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120.0
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let realm = try! Realm()
            try! realm.write {
                realm.delete(listPushes![indexPath.row])
            }
            self.refresh()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
}
