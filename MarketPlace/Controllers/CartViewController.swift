//
//  CartViewController.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/3/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class CartViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var viewSeparatorHeader: UIView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelUnityName    : UILabel!
    @IBOutlet weak var labelQuantity     : UILabel!
    @IBOutlet weak var viewUnity         : UIView!
    @IBOutlet weak var imageViewCart: UIImageView!
    @IBOutlet weak var imageViewArrow: UIImageView!
    
    var cart               : Order?
    var foodAddress        : Address?
    var cartToTrack        : Order?
    
    var types = [OrderType]()
    var selectedType = OrderType.None
    var arrayQrDecoded = [String]()
    var rootViewController : UIViewController!
    var component          : ComponentViewController!
    var load: LoadingViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        load = LoadingViewController.sharedManager() as! LoadingViewController
        
        if let address = AddressPersistence.getAddressBySelected() {
            let realm = try! Realm()
            try! realm.write {
                cart?.address = address
            }
        }
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.registerCells()
        self.loadCart()
        self.layoutCartView()
        self.edgesForExtendedLayout = UIRectEdge.None
        UIView.animateWithDuration(1.0, animations: {
            self.imageViewArrow.transform = CGAffineTransformMakeRotation((180.0 * CGFloat(M_PI)))
        })
        
        setTypes()
        layoutHeaderView()
        self.component = ComponentViewController()
        self.addChildViewController(component)
        Lib4all.setButtonColor(SessionManager.sharedInstance.cardColor)
        self.component.buttonTitleWhenNotLogged = kEnter
        self.component.buttonTitleWhenLogged    = kPay
        self.component.requireFullName = true
        self.view.addSubview(component.view)
        self.view.sendSubviewToBack(component.view)
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let address = AddressPersistence.getAddressBySelected() {
            let realm = try! Realm()
            try! realm.write {
                cart?.address = address
            }
        }
        
        if self.cart != nil {
            self.selectedType = OrderType(rawValue: self.cart!.idOrderType)!
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(CartViewController.loadCart),
            name: NOTIF_CART_OBSERVER,
            object: nil)
        self.tableView.reloadData()
        if let carrinho = self.cart {
            if carrinho.idOrderType == OrderType.Delivery.rawValue {
                self.checkDeliveryFee()
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().applicationFrame.width, height: self.viewUnity.frame.height)
        Util.setViewWithGradient(self.viewUnity, frame: rect)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let paymentIndexPath = NSIndexPath(forRow: 0, inSection: 2)
        
        if let cell = self.tableView.cellForRowAtIndexPath(paymentIndexPath) as? Payment4allTableViewCell {
            cell.configurePaymentComponent(self.component)
            cell.updateLabels()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NOTIF_CART_OBSERVER, object: nil)
    }

    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
        super.shouldAutomaticallyForwardAppearanceMethods()
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        super.shouldAutomaticallyForwardRotationMethods()
        return true
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func adjustQuantity() {
        if OrderEntityManager.sharedInstance.getCart(0) != nil {
            let countItensInCart = CartCustomManager.sharedInstance.checkNumberOfItemsInCart()
            if countItensInCart == 1 {
                labelQuantity.text      = "\(countItensInCart) " + kItem
            }
            else {
                labelQuantity.text      = "\(countItensInCart) " + kItems
            }
        }
        else {
            labelQuantity.text      = "0 " + kItems
        }
    }
    
    func setTypes() {
        if cart != nil && cart?.orderItems.count > 0 && cart!.idUnity > 0 {
            let idUnity = cart!.idUnity
            let unity = OrderEntityManager.sharedInstance.getOrderUnity(idUnity)

            if ((unity?.orderDeliveryEnabled) == true) {
                types.append(.Delivery)
            }
            if ((unity?.orderTakeAwayEnabled) == true) {
                types.append(.TakeAway)
            }
            if ((unity?.orderVoucherEnabled) == true) {
                types.append(.Voucher)
            }
            if ((unity?.orderShortDeliveryEnabled) == true) {
                types.append(.ShortDelivery)
            }
        }
    }
    
    
    func layoutCartView() {
        labelQuantity.font              = LayoutManager.primaryFontWithSize(18)
        labelQuantity.textColor         = LayoutManager.white
        labelUnityName.font             = LayoutManager.primaryFontWithSize(18)
        labelUnityName.textColor        = LayoutManager.white
        viewUnity.backgroundColor       = UIColor.green4all()
        imageViewCart.tintImage(LayoutManager.white)
        imageViewArrow.tintImage(LayoutManager.white)
        self.imageViewArrow.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CartViewController.handleTap(_:)))
        tap.delegate = self
        viewUnity.addGestureRecognizer(tap)
        adjustQuantity()
        if cart != nil && cart?.orderItems.count > 0 && cart!.idUnity > 0 {
            let idUnity = cart!.idUnity
            let unity = OrderEntityManager.sharedInstance.getOrderUnity(idUnity)
            
            var nameUnity = ""
            if let loja = unity {
                if let name = loja.name {
                    nameUnity = name
                }
            }
            
            labelUnityName.text = nameUnity
        }
        
    }
    
    func layoutHeaderView() {
        viewSeparatorHeader.backgroundColor = SessionManager.sharedInstance.cardColor
        labelHeader.font = LayoutManager.primaryFontWithSize(18)
        labelHeader.textColor = LayoutManager.greyTitleItemCart
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0,UIScreen.mainScreen().bounds.width, 0.5)
        topBorder.backgroundColor = LayoutManager.greyDescItemCart.CGColor
        viewHeader.layer.addSublayer(topBorder)
        
    }
    
    private func registerCells() {
        self.tableView.registerNib(UINib(nibName: "CartItemTableViewCell", bundle: nil), forCellReuseIdentifier: "CartItemTableViewCell")
        self.tableView.registerNib(UINib(nibName: "PaymentTypesTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentTypesTableViewCell")
        self.tableView.registerNib(UINib(nibName: "Payment4allTableViewCell", bundle: nil), forCellReuseIdentifier: "Payment4allTableViewCell")
    }
    
    func loadCart() {
        self.cart = OrderEntityManager.sharedInstance.getCart(0)
        adjustQuantity()
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
    func stopLoad(){
        print("######################### -----  Stop Load  ----- #########################")
        dispatch_async(dispatch_get_main_queue()) { 
            self.load.finishLoading(nil)
        }
    }
    
    func startLoad(){
        print("@@@@@@@@@@@@@@@@@@@@@@@@@ -----  Start Load  ----- @@@@@@@@@@@@@@@@@@@@@@@@@")
        self.load.startLoading(self, title: "Aguarde")
    }
    
    private func informationDetail(orderItem:OrderItem) -> (heightText:CGFloat!,text:String){
        
        var heightCell = CGFloat(86)
        var listSubItems = ""
        if orderItem.subItems.count > 0 {
            
            for i in 0...orderItem.subItems.count-1 {
                let item = orderItem.subItems[i]
                if i == 0 {
                    listSubItems = item.unityItem!.title
                } else {
                    listSubItems += ", " + item.unityItem!.title
                }
            }
            
            heightCell += CGFloat(CGFloat(listSubItems.characters.count)/2.0) + 25
            
        } else if orderItem.unityItem != nil {
            if orderItem.unityItem?.desc != nil {
                listSubItems = (orderItem.unityItem?.desc)!
                heightCell += CGFloat(CGFloat((orderItem.unityItem?.desc!.characters.count)!)/2.0) + 25
            }
        }
        
        return (heightCell,listSubItems)
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func checkDeliveryFee() {
        let api = ApiServices()
        // on success
        api.successCase = {(obj) in
            if let fee = obj?.objectForKey("fee") {
                
                if let cart = OrderEntityManager.sharedInstance.getCart(0) {
                    let realm = try! Realm()
                    try! realm.write {
                        cart.address = AddressPersistence.getAddressBySelected()
                        cart.idOrderType = OrderType.Delivery.rawValue
                        cart.deliveryFee = fee as! Double
                        
                        OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart)
                        
                        let paymentIndexPath = NSIndexPath(forRow: 0, inSection: 2)
                        
                        if let cell = self.tableView.cellForRowAtIndexPath(paymentIndexPath) {
                            if cell.isKindOfClass(Payment4allTableViewCell) {
                                self.tableView.beginUpdates()
                                (cell as! Payment4allTableViewCell).updateLabels()
                                self.tableView.endUpdates()
                            }
                        }
                        
                        self.load.finishLoading(nil)
                    }
                }
            }
            else {
                self.load.finishLoading({ 
                    Util.showAlert(self, title: kAlertTitle, message: kMessageAddressNotAvailable)
                })
            }
        }
        api.failureCase = {(msg) in
            self.load.finishLoading({
                Util.showAlert(self, title: kAlertTitle, message: kMessageAddressNotAvailable)
            })
        }
        
        if let addrss = AddressPersistence.getAddressBySelected() {//self.cart?.address {
            if let idUnity =  cart?.idUnity{
                api.checkDeliveryFee(addrss, idUnity: idUnity)
            } else {
                self.stopLoad()
            }
        } else {
            self.stopLoad()
        }
    }
}


extension CartViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if cart != nil {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("CartItemTableViewCell") as! CartItemTableViewCell
                let item = (cart?.orderItems[indexPath.row])!
                let idUnity = cart!.idUnity
                let unity = OrderEntityManager.sharedInstance.getOrderUnity(idUnity)
                if let loja = unity {
                    cell.populateCell(item, description: informationDetail(item).text, rootVc: self, unity: loja)
                }
                return cell
            }
            else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("PaymentTypesTableViewCell") as! PaymentTypesTableViewCell
                switch types[indexPath.row] {
                case .Delivery:
                    cell.labelType.text = kDeliveryTypeDelivery
                    cell.imageViewType.image = UIImage(named: "icone_delivery")
                    cell.imageViewType.tintImage(SessionManager.sharedInstance.cardColor)
                    
                    break
                case .ShortDelivery:
                    cell.labelType.text = kDeliveryTypeShortDelivery
                    cell.imageViewType.image = UIImage(named: "icone_pedido_na_mesa")
                    cell.imageViewType.tintImage(SessionManager.sharedInstance.cardColor)
                    break
                case .Voucher:
                    cell.labelType.text = kDeliveryTypeVoucher
                    cell.imageViewType.image = UIImage(named: "icone_voucher")
                    cell.imageViewType.tintImage(SessionManager.sharedInstance.cardColor)
                    break
                case .TakeAway:
                    cell.labelType.text = kDeliveryTypeTakeaway
                    cell.imageViewType.image = UIImage(named: "icone_naLoja")
                    cell.imageViewType.tintImage(SessionManager.sharedInstance.cardColor)
                    break
                default:
                    break
                }
                cell.unsetAsChosen()
                cell.labelFullAddress.hidden = true
                cell.labelAddress.hidden = true
                cell.buttonChange.hidden = true
                cell.labelTable.hidden = true
                cell.imageViewArrow.hidden = true
                cell.parentVC = self
                cell.cart = self.cart
                cell.viewGrayBackground.backgroundColor = LayoutManager.white
                
                if types[indexPath.row] == selectedType {
                    cell.setAsChosen()
                    
                    if selectedType == OrderType.Delivery {
                        
                        if let address = cart?.address {
                            cell.labelAddress.hidden = false
                            cell.buttonChange.hidden = false
                            cell.imageViewArrow.hidden = true
                            var addressText = address.street! + ", " + address.number!
                            if address.complement != "" {
                                addressText = addressText + ", " + address.complement!
                            }
                            if let neighborhood = address.neighborhood {
                                addressText = addressText + ", " + neighborhood
                            }
                            if let city = address.city {
                                addressText = addressText + ", " + city
                            }
                            cell.labelFullAddress?.text =  addressText
                            cell.labelAddress.text = kAddress
                            cell.labelFullAddress.hidden = false
                            
                        } else  if let address = AddressPersistence.getAddressBySelected() {
                            let realm = try! Realm()
                            try! realm.write {
                                cart?.address = address
                            }
                            cell.labelAddress.hidden = false
                            cell.buttonChange.hidden = false
                            cell.imageViewArrow.hidden = true
                            var addressText = address.street! + ", " + address.number!
                            if address.complement != "" {
                                addressText = addressText + ", " + address.complement!
                            }
                            if let neighborhood = address.neighborhood {
                                addressText = addressText + ", " + neighborhood
                            }
                            if let city = address.city {
                                addressText = addressText + ", " + city
                            }
                            cell.labelFullAddress?.text =  addressText
                            cell.labelAddress.text = kAddress
                            cell.labelFullAddress.hidden = false
                        }
                        else {
                            cell.labelTable.hidden = false
                            cell.imageViewArrow.userInteractionEnabled = true
                            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: cell, action: #selector(cell.changeAddressAndPlaceLabel))
                            cell.imageViewArrow.addGestureRecognizer(tap)
                            if AddressPersistence.getAllLocalAddresses().count == 0 {
                                cell.labelTable.text = kSetAddress
                                cell.buttonChange.hidden = true
                                cell.imageViewArrow.hidden = false
                            }
                            else {
                                cell.labelTable.text = kChooseAddress
                                cell.buttonChange.hidden = true
                                cell.imageViewArrow.hidden = false
                                
                            }
                        }
                        
                    }
                    else if selectedType == OrderType.ShortDelivery {
                        cell.labelTable.hidden = false
                        if cart!.placeLabel == "" {
                            cell.imageViewArrow.userInteractionEnabled = true
                            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: cell, action: #selector(cell.changeAddressAndPlaceLabel))
                            cell.imageViewArrow.addGestureRecognizer(tap)
                            cell.buttonChange.hidden = true
                            cell.imageViewArrow.hidden = false
                            cell.labelTable.text = kScanTable
                        }
                        else {
                            cell.labelTable.text = cart?.placeLabel
                            cell.buttonChange.hidden = false
                            cell.imageViewArrow.hidden = true
                        }
                    }
                    else {
                        cell.labelFullAddress.hidden = true
                        cell.labelAddress.hidden = true
                        cell.buttonChange.hidden = true
                        cell.labelTable.hidden = true
                    }
                }
                return cell
            }
            else {
                if let cell = tableView.dequeueReusableCellWithIdentifier("Payment4allTableViewCell") as? Payment4allTableViewCell {
                    cell.configurePaymentComponent(self.component)
                    cell.cart = cart
                    if cell.labelTotal != nil {
                        cell.updateLabels()
                    }
                    cell.parentVC = self
                    return cell
                }
                return UITableViewCell()
            }
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let numberOfRows = cart?.orderItems.count {
                return numberOfRows
            }
            return 0
        }
        if section == 1 {
            return types.count
        }
        else {
            return 1
        }
    }
}

extension CartViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if cart == nil {
            return 0
        }
        
        if indexPath.section == 0 {
            return informationDetail((cart?.orderItems[indexPath.row])!).heightText
        }
        if indexPath.section == 1 {
            if types[indexPath.row] == OrderType.Delivery {
                if selectedType == OrderType.Delivery {
                    if cart?.address != nil {
                        return 178
                    }
                    else {
                        return 120
                    }
                }
            }
            if types[indexPath.row] == OrderType.ShortDelivery {
                if selectedType == OrderType.ShortDelivery {
                    return 120
                }
            }
            return 60
        }
        else {
            if selectedType == OrderType.Delivery || selectedType == OrderType.TakeAway {
                return 195
            }
            else {
                return 170
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            let oldArrayIndex = types.indexOf(selectedType)

            if types[indexPath.row] != selectedType {
                eraseTypes()
            }
            selectedType = types[indexPath.row]
            
            let realm = try! Realm()
            try! realm.write {
                cart?.idOrderType = selectedType.rawValue
                
                if selectedType != .Delivery {
                    cart?.deliveryFee = 0.0
                }
                
                OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart!)
            }
            
            if selectedType == .Delivery {
                self.startLoad()
                self.checkDeliveryFee()
            }
            
            let paymentIndexPath = NSIndexPath(forRow: 0, inSection: 2)
              
            if oldArrayIndex != nil {
                let oldIndexPath = NSIndexPath(forRow: oldArrayIndex!, inSection: 1)
                self.tableView.reloadRowsAtIndexPaths([oldIndexPath,indexPath], withRowAnimation: .Fade)
                if let cell = self.tableView.cellForRowAtIndexPath(paymentIndexPath) {
                    if cell.isKindOfClass(Payment4allTableViewCell) {
                        tableView.beginUpdates()
                        (cell as! Payment4allTableViewCell).updateLabels()
                        tableView.endUpdates()
                        print("old Array end")
                    }
                }
            }
            else {
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                if let cell = self.tableView.cellForRowAtIndexPath(paymentIndexPath) {
                    if cell.isKindOfClass(Payment4allTableViewCell) {
                        tableView.beginUpdates()
                        (cell as! Payment4allTableViewCell).updateLabels()
                        tableView.endUpdates()
                        
                        print("else old array")
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            
            return viewHeader
        }
        
        else {
            let view = UIView()
            view.backgroundColor = viewHeader.backgroundColor
            return view
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 60
        }
        if section == 2 {
            return 30
        }
        else {
            return CGFloat.min
        }
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    func eraseTypes() {
        let realm = try! Realm()
        try! realm.write {
            cart?.placeLabel = ""
            cart?.idOrderType = OrderType.None.rawValue
        }
    }
}

extension CartViewController: LoginDelegate {
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        self.tableView.reloadSections(NSIndexSet(index: 2), withRowAnimation: .Fade)
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        let api = ApiServices()
        
        if cart != nil {
            
            let customerInfo : Dictionary<String, AnyObject> = ["sessionToken":sessionToken,"cardId":cardId,"paymentMode":paymentMode.rawValue]
            
            dispatch_async(dispatch_get_main_queue(), {
                if self.unityIsOpened() {
                    if self.cart!.idOrderType != OrderType.None.rawValue {
                        
                        let realm = try! Realm()
                        try! realm.write {
                            if self.cart!.idOrderType != OrderType.Delivery.rawValue {
                                self.cart?.deliveryFee = 0.0
                                self.cart?.address = nil
                            }
                        }
                        switch self.cart!.idOrderType {
                        case OrderType.Delivery.rawValue:
                            if self.cart!.address != nil {
                                let realm = try! Realm()
                                try! realm.write() {
                                    self.cart!.placeLabel = "Delivery"
                                    self.cart!.address = AddressPersistence.getAddressBySelected()
                                }
                                self.startLoad()
                                api.sendOrder(self.cart!, customerInfo: customerInfo)
                            }
                            else {
                                Util.showAlert(self, title: kAlertTitle, message: kMessageChooseAddress)
                            }
                            break
                        case OrderType.ShortDelivery.rawValue:
                            if self.cart!.placeLabel != "" {
                                self.startLoad()
                                api.sendOrder(self.cart!, customerInfo: customerInfo)
                            }
                            else {
                                Util.showAlert(self, title: kAlertTitle, message: kMessageChooseTable)
                            }
                            break
                        case OrderType.Voucher.rawValue:
                            let realm = try! Realm()
                            try! realm.write() {
                                self.cart!.placeLabel = kVoucher
                            }
                            self.startLoad()
                            api.sendOrder(self.cart!, customerInfo: customerInfo)
                            break
                        case OrderType.TakeAway.rawValue:
                            let realm = try! Realm()
                            try! realm.write() {
                                self.cart!.placeLabel = kTakeaway
                            }
                            self.startLoad()
                            api.sendOrder(self.cart!, customerInfo: customerInfo)
                            break
                        default: break
                        }
                    }
                    else {
                        Util.showAlert(self, title: kAlertTitle, message: kMessageSelectDeliveryMode)
                    }
                } else{
                    Util.showAlert(self, title: kAlertTitle, message: kMessageStoreClosed)
                }
            })
        }
        
        api.successCase = { (response) in
            let responseToOrder = response as! NSDictionary
            let order = responseToOrder.objectForKey("order")
            
            self.load.finishLoading({
                
                self.cartToTrack = OrderEntityManager.sharedInstance.getOrderById(Order(value:order!).id, idUnity: Order(value:order!).idUnity)
                self.cart = nil
                CartCustomManager.sharedInstance.hideCart()
                
                if self.cartToTrack!.idOrderType != OrderType.Voucher.rawValue {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let trackingViewController = mainStoryboard.instantiateViewControllerWithIdentifier("TrackingViewController") as! TrackingViewController
                    
                    trackingViewController.order = self.cartToTrack!
                    
                    let sucessoVc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                    var logoUnity = ""
                    if let logo = (UnityPersistence.getUnityByID(String(self.cartToTrack!.idUnity))?.getLogo()) {
                        logoUnity = logo.value!
                    }
                    (sucessoVc.viewControllers[0] as! ReceiptViewController).labels = ["success": "Pedido realizado com sucesso.",
                        "date": NSDate.getDateTodayToShortYearFormat(),
                        "time":NSDate.getTimeTodayToFormat(),
                        "title": kOrderDoneTitle,
                        "type":TypePageSuccess.PagamentoPedido.rawValue,
                        "price":(self.cartToTrack?.total)!,
                        "idUnity":(self.cartToTrack?.idUnity)!,
                        "shop":
                            (UnityPersistence.getUnityByID(String(self.cartToTrack!.idUnity))?.name)!,
                        "imageUrl":
                        logoUnity
                    ]
                    sucessoVc.loadView()
                    (sucessoVc.viewControllers[0] as! ReceiptViewController).previousVC = self
                    
                    self.dismissViewControllerAnimated(true, completion: {
                        PageManager.sharedInstance.rootViewController.presentViewController(sucessoVc,animated: true, completion: nil)
                    })
                }
                    
                else {
                    
                    let voucher = responseToOrder.objectForKey("voucher") as! NSDictionary
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let sucessoVc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                    var logoUnity = ""
                    if let logo = (UnityPersistence.getUnityByID(String(self.cartToTrack!.idUnity))?.getLogo()) {
                        logoUnity = logo.value!
                    }
                    (sucessoVc.viewControllers[0] as! ReceiptViewController).labels = ["success": "Pedido realizado com sucesso.",
                        "date": NSDate.getDateTodayToShortYearFormat(),
                        "time":NSDate.getTimeTodayToFormat(),
                        "title": kOrderDoneTitle,
                        "type":TypePageSuccess.PagamentoVoucher.rawValue,
                        "price":(self.cartToTrack?.total)!,
                        "idUnity":(self.cartToTrack?.idUnity)!,
                        "shop":
                            (UnityPersistence.getUnityByID(String(self.cartToTrack!.idUnity))?.name)!,
                        "imageUrl":
                        logoUnity,
                        "idVoucher":voucher.objectForKey("id")!
                    ]
                    sucessoVc.loadView()
                    (sucessoVc.viewControllers[0] as! ReceiptViewController).previousVC = self
                    
                    self.dismissViewControllerAnimated(true, completion: {
                        PageManager.sharedInstance.rootViewController.presentViewController(sucessoVc,animated: true, completion: nil)
                    })
                }
            })
        }
        
        api.failureCase = { (msg) in
            self.load.finishLoading({
                Util.showAlert(self, title: kAlertTitle, message: msg)
            })
        }
        
    }
    
    func unityIsOpened() -> Bool {
        
        if let cart = cart {
            let idUnity = String(cart.idUnity)
            let unity = UnityPersistence.getUnityByID(idUnity) //realm.objectForPrimaryKey(Unity.self, key: cart?.idUnity)
            
            if unity != nil {
                return unity!.opened
            }
        }
        return false
    }

}
