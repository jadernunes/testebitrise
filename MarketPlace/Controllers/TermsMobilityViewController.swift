//
//  TermsMobilityViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 05/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class TermsMobilityViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(TermsMobilityViewController.goBack))
        self.navigationItem.title = "Termos de uso"
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor() 
        
        // Start the throbber to check if the user exists
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityView.center = CGPointMake(self.view.frame.size.width / 2.0, 370.0)
        activityView.startAnimating()
        activityView.tag = 100
        self.view.addSubview(activityView)
        self.webView.scalesPageToFit = true
        self.webView.delegate = self
        self.webView.loadRequest(NSURLRequest(URL: NSURL.init(string: "https://recargaagora.4all.com/termos.pdf")!))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.view.viewWithTag(100)?.hidden = true
    }
    
    func didPressCloseButton(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
