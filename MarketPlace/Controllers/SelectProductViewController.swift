//
//  SelectProductViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class SelectProductViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableViewListProducts: UITableView!
    var modifier: UnityItemModifier!
    var positionModifier :Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)
        self.title = modifier.name
        
        self.view.backgroundColor = LayoutManager.lightGray4all
        self.tableViewListProducts.backgroundColor = LayoutManager.lightGray4all
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Util.adjustInsetsFromCart(tableViewListProducts)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - TableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modifier.unityItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_SELECT_EXTRA_PRODUCT, forIndexPath: indexPath) as! SelectProductTableViewCell
        let unityItem = modifier.unityItems[indexPath.row]
        cell.populateData(unityItem,modifier: modifier)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var listUniitems = self.getListUnityItemSelectedByCurrentList()
        listUniitems.append(modifier.unityItems[indexPath.row])
        SessionManager.sharedInstance.listUnityItemsSeleted[positionModifier].listUnityItem = listUniitems
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    private func getListUnityItemSelectedByCurrentList() -> [UnityItem]! {
        if SessionManager.sharedInstance.listUnityItemsSeleted.count > 0 {
            for i in 0...SessionManager.sharedInstance.listUnityItemsSeleted.count - 1 {
                let itemSelected = SessionManager.sharedInstance.listUnityItemsSeleted[i]
                if i == positionModifier {
                    if itemSelected.listUnityItem != nil {
                        return itemSelected.listUnityItem
                    }
                }
            }
        }
        return []
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Util.adjustInsetsFromCart(tableViewListProducts)
    }
}
