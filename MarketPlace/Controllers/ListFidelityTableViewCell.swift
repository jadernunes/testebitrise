//
//  ListFidelityTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 17/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ListFidelityTableViewCell: UITableViewCell {

    @IBOutlet weak var viewShowStamp: UIView!
    var maxItems                                : Int!
    var countItems                              : Int!
    var urlImage                                : String!
    var descriptionTerms                        : String!
    var shortDesc                               : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populateData(maxItems:Int,countItems:Int,urlImage:String,descriptionTerms:String,shortDesc:String) {
        
        self.backgroundColor = UIColor.clearColor()
        self.viewShowStamp.layer.masksToBounds = true
        self.viewShowStamp.layer.cornerRadius = 10
        
        self.maxItems           = maxItems
        self.countItems         = countItems
        self.urlImage           = urlImage
        self.descriptionTerms   = descriptionTerms
        self.shortDesc          = shortDesc
        
        let viewStamp = FidelityView.instanceFromNib()
        viewStamp.maxItems = self.maxItems
        viewStamp.countItems = self.countItems
        viewStamp.urlImage = self.urlImage
        viewStamp.descriptionTerms = self.descriptionTerms
        viewStamp.labelDesc.text = self.shortDesc
        
        let deviceBounds = UIScreen.mainScreen().bounds
        viewStamp.frame = CGRectMake(0, 0, deviceBounds.width-20, self.viewShowStamp.frame.size.height)
        self.viewShowStamp.addSubview(viewStamp)
        
        self.viewShowStamp.setNeedsLayout()
        self.viewShowStamp.layoutIfNeeded()
        viewStamp.setNeedsLayout()
        viewStamp.layoutIfNeeded()
    }
    
}
