//
//  ActionCellProfile.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 01/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class SwiftDisclosureIndicator: UIView {
    @IBInspectable var color = UIColor.green4all() {
        didSet {
            setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        var x = CGRectGetMaxX(self.bounds)
        x = x - 3
        let y = CGRectGetMidY(self.bounds)
        let R = CGFloat(4.5)
        CGContextMoveToPoint(context!, x - R, y - R)
        CGContextAddLineToPoint(context!, x, y)
        CGContextAddLineToPoint(context!, x - R, y + R)
        CGContextSetLineCap(context!, CGLineCap.Square)
        CGContextSetLineJoin(context!, CGLineJoin.Miter)
        CGContextSetLineWidth(context!, 2)
        color.setStroke()
        CGContextStrokePath(context!)
    }
}

class ActionCellProfile: UITableViewCell {

    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTitle.textColor = UIColor.darkGray4all()
        self.viewSeparator.backgroundColor = UIColor.lightGray4all()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
