//
//  ListUnityItemsViewController.swift
//  MarketPlace
//
//  Created by Luciano on 10/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import PageMenu
import RealmSwift

class ListUnityItemsViewController: UIViewController {
    
    var pageMenu        : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var contentArray    : NSArray!
    var unity           : Unity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        self.navigationController!.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        let buttonClose = UIButton()
        buttonClose.setTitle("Fechar", forState: .Normal)
        buttonClose.frame = CGRectMake(0, 0, 70, 40)
        buttonClose.addTarget(self, action: #selector(ListUnityItemsViewController.closeViewController), forControlEvents: .TouchUpInside)
        buttonClose.tintColor = UIColor.whiteColor()
        //.... Set Right/Left Bar Button item
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = buttonClose
        self.navigationController!.navigationItem.leftBarButtonItem = leftBarButton
        
        self.title = unity.name!
        
       // configurePages()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self)
        
        configurePages()
        GoogleAnalytics.sharedInstance.trakingScreenWithNameAndIdUnity(ScreensName.screenProductsOrServices, idUnity: String(unity.id))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configurePages() -> Void {
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        
        for item in contentArray {
            if let dict = item as? NSDictionary {
                let controller   = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListProdsVC") as! ListProductsViewController
                
                controller.referenceToSuperViewController = self
                SessionManager.sharedInstance.categoryName = dict.valueForKey("name") as? String
                controller.title = dict.valueForKey("name") as? String
                controller.loadView()
                
                
                if (dict.objectForKey("productCategoryTypeId") as! Int) != ProductCategoryType.extra.rawValue {
                    if let prods = dict.valueForKey("unityItems") as? NSArray{
                        if prods.count > 0 {
                            controller.listProducts = prods
                            controller.unity        = unity
                            controllerArray.append(controller)
                        }
                    }
                }
            }
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(Util.hexStringToUIColor("FB0B40")),
            .ViewBackgroundColor(UIColor.whiteColor()),
            .MenuItemMargin(6.0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.05),
            .SelectionIndicatorColor(UIColor.whiteColor()),
            .SelectionIndicatorHeight(1.0),
            .MenuItemFont(LayoutManager.primaryFontWithSize(16)),
            .MenuHeight(40.0),
            .UnselectedMenuItemLabelColor(LayoutManager.regularFontColor),
            .CenterMenuItems(false),
            .MenuItemWidthBasedOnTitleTextWidth(true)
        ]
        
        if controllerArray.count > 0 && parameters.count > 0 {
            
            // Initialize page menu with controller array, frame, and optional parameters
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
            pageMenu?.menuHeight = 60
            pageMenu?.scrollMenuBackgroundColor = SessionManager.sharedInstance.cardColor
        }
        
        if controllerArray.count > 0 {
            // Lastly add page menu as subview of base view controller view
            // or use pageMenu controller in you view hierachy as desired
            self.view.addSubview(pageMenu!.view)
            let cont = (self.pageMenu?.childViewControllers[0] as! ListProductsViewController)
            cont.tableView.reloadData()
            cont.tableView.scrollEnabled    = true
        }else{
            //add label warning that there's no item/section
            let labelMessage            = UILabel(frame: CGRectMake(0, 0, self.view.frame.size.width, 21))
            labelMessage.center         = CGPointMake(UIScreen.mainScreen().bounds.size.width / 2, UIScreen.mainScreen().bounds.size.height / 2 - 50)
            labelMessage.textAlignment  = .Center
            labelMessage.text           = kMessageNoItemsOrSchedulesFound
            labelMessage.textColor      = LayoutManager.regularFontColor
            labelMessage.font           = LayoutManager.primaryFontWithSize(15)
            self.view.addSubview(labelMessage)
        }
    }
    
    @IBAction func closeViewController(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "segueGotoExtra") {
            let unityItem = (sender as! UnityItem)
            let unityItemModifiers = unityItem.unityItemModifiers
            let viewController = segue.destinationViewController as! ExtraProductsViewController
            ExtraProductsViewController.sharedInstance
            viewController.listUnityItemModifiers = unityItemModifiers
            viewController.unityItem = unityItem
            viewController.unity = self.unity
        }
    }
}
