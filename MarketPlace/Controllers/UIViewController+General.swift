//
//  UIViewController+General.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 02/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension String {
    static public let onboardingStoryboard = "Onboarding"
}

extension UIViewController {
    func presentOnboarding(viewController: NewHomeViewController) {
        if NSUserDefaults.standardUserDefaults().boolForKey(.onboardingPresentedUserDefaultsKey) == false {
            let storyboard = UIStoryboard(name: String.onboardingStoryboard, bundle: nil)
            guard let initialView = storyboard.instantiateInitialViewController() as? OnboardingViewController else {
                return
            }
            initialView.superViewController = viewController
            self.presentViewController(initialView, animated: true, completion: nil)
        }
    }
}
