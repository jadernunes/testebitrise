//
//  HomeTabBarViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

enum TabType: Int {
    case Home           = 0
    case Notifications  = 1
    case QRCodeReader   = 2
    case Map            = 3
    case Profile        = 4
    case Invalid        = 5
}

class Card: NSObject {
    var imageNamed  : String
    var title       : String
    var descript    : String
    var target      : String
    var color       : String
    
    init(initialData:NSDictionary?=nil) {
        if let dictInitData = initialData {
            self.imageNamed         = dictInitData.objectForKey("image") as! String
            self.title              = dictInitData.objectForKey("title") as! String
            self.descript           = dictInitData.objectForKey("description") as! String
            self.target             = dictInitData.objectForKey("target") as! String
            self.color              = dictInitData.objectForKey("color") as! String
        }
        else {
            self.imageNamed         = ""
            self.title              = ""
            self.descript           = ""
            self.target             = ""
            self.color              = ""
        }
    }
    
}

class TabBarItem: NSObject {
    var name        : String
    var iconNamed   : String
    var type        : TabType
    var cards       = [Card]()
    
    init(initialData:NSDictionary?=nil) {
        if let dictInitData = initialData {
            self.name               = dictInitData.objectForKey("name") as! String
            self.iconNamed          = dictInitData.objectForKey("icon") as! String
            if let typeDict = dictInitData["type"] as? NSDictionary {
                switch typeDict["code"] as! Int {
                case 0:
                    self.type = TabType.Home
                case 1:
                    self.type = TabType.Notifications
                case 2:
                    self.type = TabType.QRCodeReader
                case 3:
                    self.type = TabType.Map
                case 4:
                    self.type = TabType.Profile
                default:
                    self.type = TabType.Invalid
                }
            }
            else {
                self.type           = TabType.Invalid
            }
            if let cards = dictInitData["cards"] as? NSArray {
                for i in 0...cards.count-1 {
                    if let dictCard = cards[i] as? NSDictionary {
                        let card = Card.init(initialData: dictCard)
                        self.cards.append(card)
                    }
                }
            }
        }
        else {
            self.name           = ""
            self.iconNamed      = ""
            self.type           = TabType.Invalid
        }
    }
}

class HomeTabBarViewController: UITabBarController {
    var tabBarItems             = [TabBarItem]()
    var tabBarControllers       = [UIViewController]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.tintColor = UIColor().hexStringToUIColor("009933")
        
        /* --- configuração inicial de tabs --- */
        for i in 0...LIST_TAB_BAR_HOME.count-1 {
            if let dict = LIST_TAB_BAR_HOME.objectAtIndex(i) as? NSDictionary {
                let tabItem = TabBarItem.init(initialData: dict)
                tabBarItems.append(tabItem)
                switch tabItem.type {
                case TabType.Home:
                    let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("carouselViewController") as! ListHomeViewController
                    viewController.cards = tabItem.cards
                    let navVC = UINavigationController(rootViewController: viewController)
                    navVC.navigationBarHidden = true
                    self.tabBarControllers.append(navVC)
                case TabType.Notifications:
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
                    viewController.typeShow = TypeShowListCoupons.allCupons
                    viewController.preferredStatusBar = UIStatusBarStyle.LightContent
                    let navVC = UINavigationController(rootViewController: viewController)
                    navVC.navigationBar.barTintColor = LayoutManager.green4all
                    navVC.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
                    navVC.navigationBarHidden = false
                    self.tabBarControllers.append(navVC)
                case TabType.QRCodeReader:
                    let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
                    self.tabBarControllers.append(viewController)
                case TabType.Map:
                    let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("mapView") as! UnityMapViewController
                    let navVC = UINavigationController(rootViewController: viewController)
                    navVC.navigationBar.barTintColor = LayoutManager.green4all
                    navVC.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
                    navVC.navigationBarHidden = true
                    self.tabBarControllers.append(navVC)
                case TabType.Profile:
                    let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("perfilNVController") as! UINavigationController
                    self.tabBarControllers.append(viewController)
                default:
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
                    self.tabBarControllers.append(viewController)
                }
            }
        }
        
        /* --- Após inserir as controllers, seta os nomes e imagens --- */
        self.viewControllers = self.tabBarControllers
        for i in 0...self.tabBarItems.count-1 {
            let tabItem = tabBarItems[i]
            let iconImage = UIImage(named: tabItem.iconNamed)
            self.tabBar.items?[i].image = iconImage
            self.tabBar.items?[i].title = tabItem.name
            self.tabBar.items?[i].imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        PageManager.sharedInstance.getNewNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        for i in 0...self.tabBarItems.count-1 {
            let tabItem = tabBarItems[i]
            let iconImage = UIImage(named: tabItem.iconNamed)
            self.tabBar.items?[i].image = iconImage
            self.tabBar.items?[i].title = tabItem.name
            self.tabBar.items?[i].imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
    }
    
    
}
