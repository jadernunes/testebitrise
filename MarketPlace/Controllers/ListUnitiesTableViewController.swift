//
//  ListUnitiesTableViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 14/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ListUnitiesTableViewController: UIView {

    //MARK: - Outlets
    @IBOutlet weak var tableViewListUnities: UITableView!
    
    //MARK: - Constants
    let heightCellUnity = CGFloat(100)
    let lineNumbersUnityDescription = 2
    let idendifierCelll = "cell"
    
    //MARK: - variables
    var listUnities = List<Unity>()
    let heightSectionHeader = 50 as CGFloat
    var referenceSuperViewController : UIViewController!
    
    //MARK: - Init methods
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    class func instanceFromNib() -> ListUnitiesTableViewController {
        return UINib(nibName: "ListUnitiesTableViewController", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! ListUnitiesTableViewController
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Register cell in table view
        self.tableViewListUnities.registerNib(UINib.init(nibName: "ListUnitiesTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: idendifierCelll)
        self.tableViewListUnities.delegate = self
        self.tableViewListUnities.dataSource = self
        self.backgroundColor = LayoutManager.backgroundFirstColor
        self.tableViewListUnities.backgroundColor = LayoutManager.backgroundFirstColor
    }
    
    //MARK: - Show detail Unity
    
    /// Show unity detail
    ///
    /// - Parameter unity: Object that reference unity
    func callDetails(unity: Unity!) -> Void{
        
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        detailsVc.title = unity.name
        detailsVc.idObjectReceived = unity.id
        detailsVc.typeObjet = TypeObjectDetail.Store
        self.referenceSuperViewController.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
}

//MARK: - TableView Delegate

extension ListUnitiesTableViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return heightCellUnity
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightSectionHeader
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let titleFrame = CGRect(x: 5, y: 2, width: referenceSuperViewController.view.frame.size.width - 5, height: heightSectionHeader)
        let headerFrame = CGRect(x: 0, y: 0, width: referenceSuperViewController.view.frame.size.width, height: heightSectionHeader)
        
        let customTitle = UILabel(frame: titleFrame)
        customTitle.numberOfLines = lineNumbersUnityDescription
        customTitle.textColor = SessionManager.sharedInstance.cardColor
        customTitle.text = kTitleListUnities
        customTitle.textAlignment = NSTextAlignment.Center
        
        let customView = UIView(frame: headerFrame)
        customView.backgroundColor = LayoutManager.lightGray4all
        customView.addSubview(customTitle)
        
        return customView
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let unity = listUnities[indexPath.row]
        self.callDetails(unity)
    }
}

//MARK: - TableView Data Source

extension ListUnitiesTableViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listUnities.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(idendifierCelll, forIndexPath: indexPath) as! ListUnitiesTableViewCell
        let unity = listUnities[indexPath.row]
        cell.populateData(unity)
        
        return cell
    }
}


