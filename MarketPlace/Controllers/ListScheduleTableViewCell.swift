//
//  ListScheduleTableViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class ListScheduleTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelUnityName: UILabel!
    @IBOutlet weak var labelServiceName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelStatus.backgroundColor = UIColor.clearColor()

        self.labelStatus.textColor = LayoutManager.green4all
        self.labelStatus.font = LayoutManager.primaryFontWithSize(16)
        self.labelUnityName.textColor = LayoutManager.labelContrastColor
        self.labelUnityName.font = LayoutManager.primaryFontWithSize(19)
        self.labelDate.textColor = LayoutManager.labelContrastColor
        self.labelDate.font = LayoutManager.primaryFontWithSize(16)

        self.labelHour.textColor = LayoutManager.labelContrastColor
        self.labelHour.font = LayoutManager.primaryFontWithSize(16)

        self.labelServiceName.textColor = LayoutManager.labelContrastColor
        self.labelServiceName.font = LayoutManager.primaryFontWithSize(16)

        imageViewThumb.layoutIfNeeded()
        imageViewThumb.layer.cornerRadius = imageViewThumb.frame.height/2.0
        imageViewThumb.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        imageViewThumb.layer.borderWidth  = 1
        imageViewThumb.clipsToBounds      = true

        self.backgroundColor = LayoutManager.backgroundFirstColor
    }
    
    func populateData(scheduleData: Schedule)
    {
        let service = scheduleData.unityItemTitle as String!
        
        let fullDate = "\(NSString.getHourByServerConvertToShow(scheduleData.timeBegin)) - \(NSString.getHourByServerConvertToShow(scheduleData.timeEnd))" as String!
        let unity = UnityPersistence.getUnityByID(scheduleData.idUnity!) as Unity!
        
        if unity != nil
        {
            self.labelUnityName.text = unity.name
            var stringImage = ""
            
            if(unity.getLogo() != nil) {
                stringImage = (unity.getLogo()?.value)!
            }
            
            self.imageViewThumb.addImage(stringImage)
        }
        
        switch StatusSchedule(rawValue: scheduleData.idStatus)! {
        case .Sent:
            self.labelStatus.text = String(StatusScheduleDescription.Sent.rawValue)
            self.labelStatus.textColor = Util.hexStringToUIColor("#02D254")
            break
        case .Confirmed:
            self.labelStatus.text = String(StatusScheduleDescription.Confirmed.rawValue)
            self.labelStatus.textColor = Util.hexStringToUIColor("#02D254")
            break
        case .Canceled:
            self.labelStatus.text = String(StatusScheduleDescription.Canceled.rawValue)
            self.labelStatus.textColor = Util.hexStringToUIColor("#ED1B26")
            break
        }
        
        self.labelDate.text = NSString.getDateByServerConvertToShow(scheduleData.dateString as String!)
        self.labelHour.text = fullDate
        self.labelServiceName.text = service
    }
}
