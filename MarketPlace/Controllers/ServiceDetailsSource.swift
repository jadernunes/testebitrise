//
//  ServiceDetailsSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import EventKit

class ServiceDetailsSource: NSObject, UITableViewDataSource, UITableViewDelegate{

    let arrContent = NSMutableArray()
    var tableView  : UITableView!
    var unity       : Unity!
    var referenceToSuperViewController  : UIViewController!
    
    //MARK: - Init methods
    
    init(tableView : UITableView, unity : Unity!, referenceToSuperViewController : UIViewController!) {
        super.init()
        
        self.tableView = tableView
        
        self.tableView.registerNib(UINib(nibName: "UnityDetailsBannerTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: STORE_BANNER_DETAILS_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "MapTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: MAP_IDENTIFIER)
        
        self.unity                          = unity
        self.referenceToSuperViewController = referenceToSuperViewController
        self.tableView.dataSource           = self
        self.tableView.delegate             = self
        
        referenceToSuperViewController.title = unity.name!
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .ImageBanner:
            return UnityDetailsBannerTableViewCell.getConfiguredCell(tableView, unity: unity, referenceViewController: referenceToSuperViewController)
        case .Map:
            return MapTableViewCell.getConfiguredCell(self.tableView, luc: self.unity.indoorMapLuc?["luc"] as! String)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .ImageBanner:
            return 370
        case .Map:
            return 350
        }
    }

    
    //MARK: - Enums
    enum ContentTypeEvent: Int {
        case ImageBanner    = 0
        case Map            = 1
    }

    
}
