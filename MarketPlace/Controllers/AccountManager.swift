//
//  AccountManager.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol LoginDelegate: CallbacksDelegate {
    func didLogin()
}

extension LoginDelegate {
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        self.didLogin()
    }
    
    func didLogin(){
        SessionManager.sharedInstance.registerDevice()
        
        let lib = Lib4all.sharedInstance()
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if let token = user.token {
                    self.getCouponsFromServer(token)
                    self.getVochersAndTicketsFromServer(token)
                }
            }
        }
    }
    
    private func getCouponsFromServer(token: String){
        
        let api = ApiServices()
        api.getCoupomBySessionToken(token)
        
        api.successCase = {(obj) in
            dispatch_async(dispatch_get_main_queue()){
                MenuViewController.sharedInstance.reloadAllData()
            }
        }
        
        api.failureCase = {(msg) in
        }
    }
    
    private func getVochersAndTicketsFromServer(token: String){
        
        let api = ApiServices()
        api.getVouchersBySessionToken(token)
        
        api.successCase = {(obj) in
            dispatch_async(dispatch_get_main_queue()){
                MenuViewController.sharedInstance.reloadAllData()
            }
        }
        
        api.failureCase = {(msg) in
        }
    }
}
