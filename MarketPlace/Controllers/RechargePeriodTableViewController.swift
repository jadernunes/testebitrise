//
//  RechargePeriodTableViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 17/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class RechargePeriodTableViewController: UITableViewController {

    @IBOutlet weak var explanationText: UITextView!
    @IBOutlet weak var explanationLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(RechargePeriodTableViewController.goBack))
        self.navigationItem.title = "Prazo das Recargas"
        
        explanationText.font = LayoutManager.primaryFontWithSize(14)
        explanationLbl.font = LayoutManager.primaryFontWithSize(14)
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
