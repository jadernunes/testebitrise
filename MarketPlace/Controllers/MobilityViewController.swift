//
//  MobilityViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 27/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class MobilityViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView          : UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var urladdress                      : String = URL_WEB_RECARGA + "?sessionId="
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activityIndicator.startAnimating()
    }
    
    override func viewDidAppear(animated:Bool) {
        super.viewDidAppear(animated)
        let URL = self.prepareURL(self.urladdress)
        let request = NSURLRequest(URL: URL)
        self.webView.loadRequest(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - Custom Methods
    func prepareURL(urlString:String) -> NSURL {
        if Lib4all.sharedInstance().hasUserLogged() {
            if let user = User.sharedUser() {
                let fullURL = urlString + user.token
                let URL = NSURL(string: fullURL)
                return URL!
            }
        }
        return NSURL(string: urlString)!
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        
        self.activityIndicator.stopAnimating()
        self.view.sendSubviewToBack(self.activityIndicator)
    }
    
}
