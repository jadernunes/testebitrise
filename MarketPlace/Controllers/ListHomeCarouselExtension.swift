//
//  ListHomeCarouselExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation


extension ListHomeViewController : iCarouselDataSource, iCarouselDelegate {
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        return cards.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var itemView        : UIView
        var itemImage       : UIImageView = UIImageView()
        var itemViewText    : UIView
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            
            /* --- Config das Imgs --- */
            //don't do anything specific to the index within
            //this `if (view == nil) {...}` statement because the view will be
            //recycled and used with other index values later
            itemView = UIImageView(frame:kFrameCarouselCardFrame)
            itemImage.contentMode = UIViewContentMode.ScaleAspectFill
            itemImage.sizeToFit()
            itemImage.frame = kFrameCarouselCardImageFrame
            itemImage.tag = 1
            LayoutManager.roundCustomCornersRadius(itemImage, radius: 6.0, corners: [.TopLeft, .TopRight])
            itemImage.layer.masksToBounds = true
            itemView.addSubview(itemImage)
            
            /* --- Config dos Labels --- */
            kFrameCarouselCardTextView.size.width = itemImage.bounds.width
            itemViewText = UIView(frame:kFrameCarouselCardTextView)
            itemViewText.frame = kFrameCarouselCardTextView
            itemViewText.backgroundColor = UIColor.whiteColor()
            itemViewText.tag = 2
            let lblTitulo1 : UILabel? = UILabel(frame:CGRect(x:15, y:0, width:kFrameCarouselCardTextView.size.width - 30, height:kFrameCarouselCardTextView.size.height))
            lblTitulo1?.font = LayoutManager.primaryFontWithSize(16, bold: false)
            lblTitulo1?.textAlignment = .Center
            lblTitulo1?.numberOfLines = 2
            lblTitulo1?.backgroundColor = UIColor.clearColor()
            lblTitulo1!.tag = 11
            itemViewText.addSubview(lblTitulo1!)
            LayoutManager.roundCustomCornersRadius(itemViewText, radius: 6.0, corners: [.BottomLeft, .BottomRight])
            itemViewText.layer.masksToBounds = true
            
            
            itemView.addSubview(itemViewText)
            
        }
        else
        {
            //get a reference to the label in the recycled view
            itemView = view! as UIView;
            itemImage = itemView.viewWithTag(1) as! UIImageView
            itemViewText = itemView.viewWithTag(2)! as UIView
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        /* --- Configura a imagem pelo indice da view --- */
        let card = cards[index] as Card
        itemImage.image = UIImage(named: card.imageNamed)
        
        /* --- Seta os labels  --- */
        if let lblTitulo1 = itemViewText.viewWithTag(11) as? UILabel {
            lblTitulo1.text = card.descript
        }
        
        /* --- Efeito sombra --- */
        itemView.layer.masksToBounds = false;
        itemView.layer.shadowOffset = CGSizeMake(0, 20);
        itemView.layer.shadowRadius = 15;
        itemView.layer.shadowOpacity = 0.3;
        
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .Spacing)
        {
            return value * 1.1
        }
        return value
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        let card = cards[index] as Card
        
        switch card.target {
        case "4Mobility":
            if let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("mobilityVC") as? MobilityViewController {
                self.navigationController?.title = card.target
                viewController.hidesBottomBarWhenPushed = true
                viewController.title = card.title
                SessionManager.sharedInstance.cardColor = Util.hexStringToUIColor(card.color)
                self.navigationController?.pushViewController(viewController, animated: true)
                self.showNavBar(Util.hexStringToUIColor(card.color))
            }
            break
        case "4food":
            SessionManager.sharedInstance.cardColor = Util.hexStringToUIColor(card.color)
            self.performSegueWithIdentifier("segueGastronomia", sender: self)
            self.showNavBar(Util.hexStringToUIColor(card.color))
            break
        case "4beauty", "4fun":
            if let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("4allCardDetailVC") as? HomeTableViewController {
                viewController.targetURLString = card.target
                viewController.hidesBottomBarWhenPushed = true
                viewController.title = card.title
                self.navigationController?.title = card.target
                SessionManager.sharedInstance.cardColor = Util.hexStringToUIColor(card.color)
                self.navigationController?.pushViewController(viewController, animated: true)
                self.showNavBar(Util.hexStringToUIColor(card.color))
            }
            break
        case "4Park":
            SessionManager.sharedInstance.cardColor = Util.hexStringToUIColor(card.color)
            self.performSegueWithIdentifier("segue4park", sender: self)
            self.showNavBar(Util.hexStringToUIColor(card.color))
            break
        default:
            break
        }
    }
    
    func carouselDidScroll(carousel: iCarousel) {
        let card = cards[carousel.currentItemIndex] as Card
        self.textWelcome.textColor = Util.hexStringToUIColor(card.color)
        self.orderView.backgroundColor = Util.hexStringToUIColor(card.color)
    }
    
}
