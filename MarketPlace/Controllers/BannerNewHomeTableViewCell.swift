//
//  BannerNewHomeTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift
import MapKit

class BannerNewHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewBanners: UICollectionView!
    var listBanner: [Unity]? {
        didSet {
            self.collectionViewBanners.reloadData()
        }
    }
    @IBOutlet weak var pageControllBanners: UIPageControl!
    var superViewController: NewHomeViewController?
    var loader: UIActivityIndicatorView!
    let countItems: Int = 5
    
    @IBOutlet weak var viewWarning: UIView!
    @IBOutlet weak var imageViewWarning: UIImageView!
    @IBOutlet weak var labelTitleWarning: UILabel!
    @IBOutlet weak var labelMessageWarning: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionViewBanners.registerNib(UINib(nibName: "NewBannerCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "NewBannerCollectionViewCell")
        
        self.collectionViewBanners.delegate = self
        self.collectionViewBanners.dataSource = self
        
        self.pageControllBanners.currentPageIndicatorTintColor = UIColor.green4all()
        self.pageControllBanners.pageIndicatorTintColor = UIColor.gray4all().colorWithAlphaComponent(0.2)
        self.backgroundColor = UIColor.lightGray4all()
        self.collectionViewBanners.backgroundColor = UIColor.lightGray4all()
        
        loader = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        let frame = UIScreen.mainScreen().applicationFrame
        loader.frame = CGRect(x: (frame.size.width/2)-10, y: 60, width: 20, height: 20)
        self.collectionViewBanners.addSubview(self.loader)
        self.loader.startAnimating()
        self.pageControllBanners.numberOfPages = 0
        
        self.labelTitleWarning.textColor = UIColor.darkGray4all()
        self.labelMessageWarning.textColor = UIColor.gray4all()
        
        self.hideWarning()
        
        let tapOnViewWarning = UITapGestureRecognizer(target: self, action: #selector(tapOnViewWarningPressed))
        self.viewWarning.addGestureRecognizer(tapOnViewWarning)
    }
    
    func tapOnViewWarningPressed() {
        UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populateData(listaLojasProximas: [Unity]?){
        
        
        dispatch_async(dispatch_get_main_queue()){
            
            self.loader.stopAnimating()
            self.loader.removeFromSuperview()
            
            if let lista = listaLojasProximas {
                
                self.listBanner = lista
                
                if let location = LocationManager.shared.location {
                    self.listBanner = lista.sort { CLLocation(latitude: $0.0.latitude, longitude: $0.0.longitude).distanceFromLocation(location) < CLLocation(latitude: $0.1.latitude, longitude: $0.1.longitude).distanceFromLocation(location)
                    }
                }
                
                self.pageControllBanners.numberOfPages = lista.count
                
                if lista.count > self.countItems {
                    self.pageControllBanners.numberOfPages = self.countItems
                }
            }
            
            self.hideWarning()
//            self.collectionViewBanners.reloadData()
        }
    }

    private func callDetails(unity: Unity?, group: Group?) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        if (unity != nil) {
            detailsVc.title = unity?.name
            detailsVc.idObjectReceived = unity?.id
            detailsVc.typeObjet = TypeObjectDetail.Store
        } else {
            detailsVc.title = group?.name
            detailsVc.idObjectReceived = group?.id
            detailsVc.typeObjet = TypeObjectDetail.Group
        }
        
        if let superVC = self.superViewController {
            superVC.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
    
    func showWarningWithoutStores(){
        self.loader.stopAnimating()
        self.loader.removeFromSuperview()
        self.imageViewWarning.image = UIImage(named: "iconWithoutStores")
        self.labelTitleWarning.text = "Não econtramos lojas próximas…"
        self.labelMessageWarning.text = "Neste espaço mostraremos estabelecimentos que estiverem perto de você!"
        self.viewWarning.hidden = false
        self.collectionViewBanners.hidden = true
        self.pageControllBanners.hidden = true
        self.bringSubviewToFront(self.viewWarning)
    }
    
    func showWarningWithoutLocation(){
        self.loader.stopAnimating()
        self.loader.removeFromSuperview()
        self.imageViewWarning.image = UIImage(named: "iconWithoutLocation")
        self.labelTitleWarning.text = "Não sabemos onde você está..."
        self.labelMessageWarning.text = "Ative a geolocalização para mostrarmos os estabelecimentos próximos você."
        self.viewWarning.hidden = false
        self.collectionViewBanners.hidden = true
        self.pageControllBanners.hidden = true
        self.bringSubviewToFront(self.viewWarning)
    }
    
    func hideWarning(){
        self.sendSubviewToBack(self.viewWarning)
        self.viewWarning.hidden = true
        self.collectionViewBanners.hidden = false
        self.pageControllBanners.hidden = false
        self.bringSubviewToFront(self.collectionViewBanners)
        
        self.collectionViewBanners.alpha = 1.0
    }
    
    
    func startLoading(){
        self.collectionViewBanners.addSubview(self.loader)
        self.loader.startAnimating()
        self.collectionViewBanners.alpha = 0.2
    }
    
    func stopLoading(){
        self.loader.stopAnimating()
        self.loader.removeFromSuperview()
        self.bringSubviewToFront(self.collectionViewBanners)
        self.collectionViewBanners.alpha = 1.0
    }
}

extension BannerNewHomeTableViewCell: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = self.collectionViewBanners.frame.size
        
        return size
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let list = self.listBanner {
            let unity = list[indexPath.row]
            self.callDetails(unity, group: nil)
        }
    }
    
}

extension BannerNewHomeTableViewCell: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let indexPath = self.collectionViewBanners.indexPathsForVisibleItems()[0]
        pageControllBanners.currentPage = indexPath.row
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let countItens = listBanner {
            if countItens.count > countItems {
                return countItems
            }
            
            return countItens.count
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("NewBannerCollectionViewCell", forIndexPath: indexPath) as? NewBannerCollectionViewCell {
            cell.labelNameUnity.text = ""
            cell.imageViewBanner.addImage(nil)
            cell.labelDistanceUnity.text = ""
            
            cell.buttonOpenedStatus.alpha = 0.0
            
            if indexPath.row <= (self.listBanner?.count)! - 1 {
                let loja = self.listBanner![indexPath.row]
                cell.labelNameUnity.text = ""
                
                if let logo = loja.getLogo() {
                    cell.imageViewBanner.addImage(logo.value)
                }
                
                if let name = loja.name {
                    cell.labelNameUnity.text = name
                }
                
                if loja.opened {
                    cell.buttonOpenedStatus.backgroundColor = UIColor(colorLiteralRed: 79.0 / 255, green: 164.0 / 255.0, blue: 68.0 / 255, alpha: 1.0)
                    cell.buttonOpenedStatus.text = "ABERTO"
                    cell.buttonOpenedStatus.alpha = 1.0
                } else {
                    cell.buttonOpenedStatus.backgroundColor = UIColor(colorLiteralRed: 195.0 / 255, green: 35.0 / 255.0, blue: 47.0 / 255, alpha: 1.0)
                    cell.buttonOpenedStatus.text = "FECHADO"
                    cell.buttonOpenedStatus.alpha = 1.0
                }
                
                if let userLocation = LocationManager.shared.location {
                    
                    let distanceFormatter = MKDistanceFormatter()
                    distanceFormatter.units = .Metric
                    distanceFormatter.unitStyle = .Abbreviated
                    distanceFormatter.locale = NSLocale.currentLocale()
                    
                    cell.labelDistanceUnity.text = distanceFormatter.stringFromDistance(userLocation.distanceFromLocation(CLLocation(latitude: loja.latitude, longitude: loja.longitude)))
                }
                
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
}
