//
//  MapWebViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD

class MapWebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    var urlString: String!
    let loading = JGProgressHUD(style: .Dark)
    
    //MARK: - Init Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        let url = NSURL(string: urlString);
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        NSURLCache.sharedURLCache().diskCapacity = 0
        NSURLCache.sharedURLCache().memoryCapacity = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loading.showInView(self.view)
        CartCustomManager.sharedInstance.hideCart()
        
        self.title = kMapTitle
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    //MARK: - Actions
    
    @IBAction func closeVc(sender : UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - WebView Delegate
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.loading.dismiss()
    }
}
