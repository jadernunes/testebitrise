//
//  ListProductsViewController.swift
//  MarketPlace
//
//  Created by Luciano on 8/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD

class ListProductsViewController: UITableViewController{
    
    var listProducts = NSArray()
    var unity : Unity!
    var referenceToSuperViewController : UIViewController!
    var listCalendar: [AnyObject]!

    //MARK: - Init method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.bounces    = true
        tableView.alwaysBounceVertical = true
        tableView.backgroundColor = UIColor.redColor()
        
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self.referenceToSuperViewController)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(ListProductsViewController.adjustTableView),
            name: NOTIF_CART_CHANGED,
            object: nil)
        tableView.reloadData()
        Util.adjustInsetsFromCart(tableView)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        Util.adjustTableViewInsetsFromTarget(self)
        Util.adjustInsetsFromCart(tableView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func adjustTableView() {
         Util.adjustInsetsFromCart(tableView)
    }
    
    //MARK: - Get duration by Unity type
    func getDurationServiceByUnitItemID(unityItemID: Int) -> String
    {
        for dicCalendar in SessionManager.sharedInstance.listCalendar
        {
            let listServices = dicCalendar.objectForKey("services") as! NSArray
            for dicService in listServices
            {
                if dicService.objectForKey("id")!.integerValue == unityItemID
                {
                    var duration = dicService.objectForKey("duration") as! String
                    duration = NSString.getHourByServerConvertToShow(duration)
                    
                    return duration
                }
            }
        }
        
        return ""
    }
    
    //MARK: - Get calendar by unity
    
    func getCalendarByIdService(idService: Int) -> [AnyObject]
    {
        var newListCalendar: [AnyObject]! = []
        
        for dicCalendar in SessionManager.sharedInstance.listCalendar
        {
            let newDicCalendar = dicCalendar as! NSDictionary
            let listServices = dicCalendar.objectForKey("services") as! NSArray
            
            for dicService in listServices
            {
                if dicService.objectForKey("id")!.integerValue == idService
                {
                    if (newListCalendar as NSArray).containsObject(newDicCalendar) == false
                    {
                        newListCalendar.append(newDicCalendar)
                    }
                }
            }
        }
        
        //Replace list calendar with professional that excute by service type
        return newListCalendar
    }
    
    //MARK: - Set thumb product in cell
    
    func setThumbProductInCell(cell: ProductTableViewCell,urlStringImage: String!)
    {
        cell.imageViewThumb.addImage(urlStringImage)
    }
    
    //MARK: - TableView Delegate methods
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listProducts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_PRODUCT_ITEM) as! ProductTableViewCell
        
        self.setInitialCartView()
        
        let item = UnityItem(value: listProducts.objectAtIndex(indexPath.row))
        var isShowIconCart = true
        
        for modifier in item.unityItemModifiers {
            if modifier.minQuantity > 0 {
                isShowIconCart = false
            }
        }
        
        cell.rootVc = self.referenceToSuperViewController
        cell.unity = unity
        cell.product = item
        
        cell.labelName.text = item.title
        cell.constraintHeightLabelTitle.constant = CGFloat(CGFloat(item.title.characters.count)/2.0) + 30
        
        cell.labelQuantity.text = "\(OrderEntityManager.sharedInstance.getItemQuantity(item, unity: unity))"
        
        cell.labelDescription.text = item.desc
        
        var heightLabelDescription: CGFloat = 25
        if item.desc != nil {
            heightLabelDescription += CGFloat(CGFloat(item.desc!.characters.count)/2.0)
        }
        
        cell.constraintHeightlabelDescription.constant = heightLabelDescription

        cell.labelPrice.text = String(format: "R$ %.2f", item.price/100)
        self.setThumbProductInCell(cell, urlStringImage: item.thumb)
        
        if item.idUnityItemType == UnitItemType.Service.rawValue
        {
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.labelQuantity.hidden = true
            cell.viewPicker.hidden = true
            cell.buttonAddCart.hidden = true
            
            cell.iconHourGlass.hidden = false
            cell.labelTime.text = self.getDurationServiceByUnitItemID(item.id)
        }
        else if item.idUnityItemType == UnitItemType.Pizza.rawValue || item.idUnityItemType == UnitItemType.Combo.rawValue || isShowIconCart == false
        {
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.iconHourGlass.hidden = true
            cell.labelTime.text = ""
            cell.labelQuantity.hidden = true
            cell.viewPicker.hidden = true
            cell.buttonAddCart.hidden = true
            cell.labelPrice.hidden = true
            
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryType.None
            cell.iconHourGlass.hidden = true
            cell.labelTime.text = ""
            
            if cell.labelQuantity.text != "0" {
                cell.viewPicker.hidden    = false
                cell.buttonAddCart.hidden = true
            }else{
                cell.viewPicker.hidden    = true
                cell.buttonAddCart.hidden = false
            }
        }
    
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let item = UnityItem(value: listProducts.objectAtIndex(indexPath.row))
        
        let heightTitle = CGFloat(CGFloat(item.title.characters.count)/2.0) + 25
        
        var heightDescription = CGFloat(30)
        if item.desc != nil {
            heightDescription += CGFloat(CGFloat(item.desc!.characters.count)/2.0)
        }
        
        return 35 + heightTitle + heightDescription
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = UnityItem(value: listProducts.objectAtIndex(indexPath.row))
        
        if item.idUnityItemType == UnitItemType.Service.rawValue
        {
            SessionManager.sharedInstance.listCalendar = self.getCalendarByIdService(item.id)
            
            if SessionManager.sharedInstance.listCalendar.count > 0 && unity != nil
            {
                SessionManager.sharedInstance.idUnitItem = item.id
                SessionManager.sharedInstance.unityItem = item
                referenceToSuperViewController.performSegueWithIdentifier("segueGotoSchedule", sender: nil);
            }
        } else if item.idUnityItemType == UnitItemType.Pizza.rawValue || item.idUnityItemType == UnitItemType.Combo.rawValue || item.unityItemModifiers.count > 0 {
            if item.unityItemModifiers.count > 0 {
                referenceToSuperViewController.performSegueWithIdentifier("segueGotoExtra", sender: item);
            }
        } else {
            referenceToSuperViewController.performSegueWithIdentifier("segueGotoExtra", sender: item);
        }
    }
    
    func setInitialCartView() {
        CartCustomManager.sharedInstance.showCart(referenceToSuperViewController)
        if CartCustomManager.sharedInstance.shouldShowCart() {
            Util.adjustInsetsFromCart(tableView)
        }
    }
}
