//
//  HelpMobilityViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 11/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class HelpMobilityViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var activityLoad: UIActivityIndicatorView!
    @IBOutlet weak var buttonChat: UIButton!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Ajuda"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(HelpMobilityViewController.goBack))
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        self.buttonChat.layer.borderColor = SessionManagerMobility.sharedInstance.mainColor.CGColor
        self.buttonChat.layer.borderWidth = 1.0
        self.buttonChat.titleLabel?.font = LayoutManager.primaryFontWithSize(15)
        
        self.startLoad()
        self.loadFileHelp()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startLoad(){
        self.activityLoad.hidden = false
        self.activityLoad.startAnimating()
        self.view.bringSubviewToFront(self.activityLoad)
    }
    
    func stopLoad() {
        self.activityLoad.stopAnimating()
        self.activityLoad.hidden = true
        self.view.sendSubviewToBack(self.activityLoad)
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func callChat(sender: AnyObject) {
        Lib4all.sharedInstance().showChat()
    }

    func loadFileHelp(){
        let htmlFile = NSBundle.mainBundle().pathForResource("mobilityHelpFile", ofType: "html")
        let htmlString = try! String(contentsOfFile: htmlFile!, encoding: NSUTF8StringEncoding)
        self.webView.loadHTMLString(htmlString, baseURL: NSBundle.mainBundle().bundleURL)
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        /* --- Captura o clique e abre no Safari --- */
        if (navigationType == .LinkClicked) {
            UIApplication.sharedApplication().openURL(request.URL!)
            return false
        }
        return true

    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.stopLoad()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
