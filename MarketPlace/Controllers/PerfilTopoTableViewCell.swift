//
//  PerfilTopoTableViewCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 26/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PerfilTopoTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfilePicture        : UIImageView!
    @IBOutlet weak var txtProfileName           : UILabel!
    @IBOutlet weak var txtProfileDetails        : UILabel!
    
    let kMessageGuest                           = "Olá, seja bem-vindo."
    let kDetailsGuest                           = "Por favor faça seu login para entrar na conta 4all."
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.refreshData()
        self.txtProfileName.font = LayoutManager.primaryFontWithSize(18, bold: false)
        self.txtProfileDetails.font = LayoutManager.primaryFontWithSize(14, bold: false)
    }

    func refreshData() {
        self.imgProfilePicture.image = self.maskRoundedImage(self.imgProfilePicture.image!, radius: self.imgProfilePicture.frame.size.width)
        
        if Lib4all.sharedInstance().hasUserLogged() {
            if let user = User.sharedUser() {
                if user.fullName != nil {
                    if let username = user.fullName {
                        self.txtProfileName.text = username
                    }
                    else {
                        self.txtProfileName.text = kMessageGuest
                    }
                }
                
                self.txtProfileDetails.text = user.emailAddress + "\n" + user.phoneNumber + "\nV " + SystemInfo.getVersionApp()
            }
        }
        else {
            self.txtProfileName?.text = kMessageGuest
            self.txtProfileDetails?.text = kDetailsGuest + "\nV " + SystemInfo.getVersionApp()
        }
    }
    
    func maskRoundedImage(image: UIImage, radius: CGFloat) -> UIImage {
        let imageView: UIImageView = UIImageView(image: image)
        var layer: CALayer = CALayer()
        layer = imageView.layer
        
        layer.masksToBounds = true
        layer.cornerRadius = CGFloat(radius)
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
