//
//  OrderTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 8/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelPlaceName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelItems: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = LayoutManager.backgroundFirstColor
        labelPlaceName.font       = LayoutManager.primaryFontWithSize(20)
        labelPrice.font           = LayoutManager.primaryFontWithSize(20)
        labelItems.font           = LayoutManager.primaryFontWithSize(17)

        labelPlaceName.textColor             = LayoutManager.labelFirstColor
        labelItems.textColor             = LayoutManager.labelFirstColor

        labelPrice.textColor                 = LayoutManager.labelFirstColor
        imageViewProduct.layoutIfNeeded()
        imageViewProduct.layer.cornerRadius = imageViewProduct.frame.height/2.0
        imageViewProduct.layer.borderColor  = LayoutManager.labelFirstColor.CGColor
        imageViewProduct.layer.borderWidth  = 1
        imageViewProduct.clipsToBounds      = true
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
