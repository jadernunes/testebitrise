//
//  NewProfileViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 01/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewProfileViewController: UIViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelProfile: UILabel!
    @IBOutlet weak var tableViewListActions: UITableView!
    @IBOutlet weak var buttonHelp: UIButton!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelHelp: UILabel!
    @IBOutlet weak var labelLogout: UILabel!
    
    let sections:[[String: Any]] = [
//        [
//            "title":"MINHAS AQUISIÇÕES",
//            "actions":[
//                [
//                    "title":"Meus ingressos",
//                    "icon":"iconIngressos",
//                    "isShowBadge":true,
//                    "target":"meusIngressos"
//                ] as [String: Any],
//                [
//                    "title":"Meus vouchers",
//                    "icon":"iconTickets",
//                    "isShowBadge":true,
//                    "target":"meusVouchers"
//                ] as [String: Any],
//                [
//                    "title":"Meus cupons",
//                    "icon":"iconCoupom",
//                    "isShowBadge":true,
//                    "target":"meusCoupons"
//                ] as [String: Any],
//                [
//                    "title":"Meus Pedidos",
//                    "icon":"iconPedidos",
//                    "isShowBadge":false,
//                    "target":"OrdersTableViewController"
//                ] as [String: Any],
//                [
//                    "title":"Fidelidades",
//                    "icon":"iconFidelity",
//                    "isShowBadge":false,
//                    "target":"fidelity"
//                    ] as [String: Any]
//            ] as [[String: Any]]
//        ] as [String: Any],
        [
            "title":"INFORMAÇÕES DA CONTA",
            "actions":[
                [
                    "title":"Extrato",
                    "icon":"iconExtract",
                    "isShowBadge":false,
                    "target":ProfileOption.Receipt.rawValue
                ] as [String: Any],
                [
                    "title":"Assinaturas",
                    "icon":"iconAssinaturas",
                    "isShowBadge":false,
                    "target":ProfileOption.Subscriptions.rawValue
                ] as [String: Any],
                [
                    "title":"Dados Pessoais",
                    "icon":"iconPersonalData",
                    "isShowBadge":false,
                    "target":ProfileOption.UserData.rawValue
                ] as [String: Any],
                [
                    "title":"Meus Cartões",
                    "icon":"iconCards",
                    "isShowBadge":false,
                    "target":ProfileOption.UserCards.rawValue
                ] as [String: Any],
                [
                    "title":"Familia",
                    "icon":"iconFamily",
                    "isShowBadge":false,
                    "target":ProfileOption.Family.rawValue
                ] as [String: Any],
                [
                    "title":"Configurações",
                    "icon":"iconConfig",
                    "isShowBadge":false,
                    "target":ProfileOption.Settings.rawValue
                ] as [String: Any],
                [
                    "title":"Sobre o App 4all",
                    "icon":"group483",
                    "isShowBadge":false,
                    "target":ProfileOption.About.rawValue
                ] as [String: Any]
            ] as [[String: Any]]
        ] as [String: Any]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewListActions.delegate = self
        self.tableViewListActions.dataSource = self
        
        self.labelHelp.textColor = UIColor.gray4all()
        self.labelLogout.textColor = UIColor.gray4all()
        
        self.tableViewListActions.registerNib(UINib(nibName: "ActionCellProfile", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ActionCellProfile")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if Lib4all.sharedInstance().hasUserLogged() {
            let user = User.sharedUser()
            self.labelName.text = user.fullName
            self.labelEmail.text = user.emailAddress
            self.labelProfile.text = user.phoneNumber
        } else {
            self.labelName.text = ""
            self.labelEmail.text = ""
            self.labelProfile.text = ""
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func showMenuAccount4all(profileOption:ProfileOption){
        if let account = Lib4all.sharedInstance() {
            if account.hasUserLogged() {
                account.openAccountScreen(profileOption, inViewController: self)
            }
        }
    }
    
    @IBAction func buttonHelpPressed(sender: UIButton) {
        self.showMenuAccount4all(ProfileOption.Help)
    }
    
    @IBAction func buttonLogoutPressed(sender: UIButton) {
        let alert = UIAlertView(title: "Atenção", message: "Você tem deseja realmente sair da conta?", delegate: self, cancelButtonTitle: "Não", otherButtonTitles: "Sim")
        alert.show()
    }
}

extension NewProfileViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let rect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let view = SectionProfile.instanceFromNib()
        view.frame = rect
        let sectionObject = self.sections[section]
        if let title = sectionObject["title"] as? String {
            view.labelTitle.text = title
        }
        
        return view
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let section = self.sections[indexPath.section]
        if let actions = section["actions"] as? [[String: Any]] where actions.count > 0 {
            
            let action = actions[indexPath.row]
            if let target = action["target"] as? String where target.characters.count > 0 {
                
//                case 2://Ingressos (Cinema, Evento)
//                let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
//                viewController.isListVouchers = false
//                (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
//                self.sidePanelController.showCenterPanelAnimated(true)
//                break
//                case 3://Vouchers (Pedido, Fidelidade)
//                let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
//                viewController.isListVouchers = true
//                (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
//                self.sidePanelController.showCenterPanelAnimated(true)
//                break
//                case 4://Cupons (Coupom)
//                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
//                viewController.typeShow = TypeShowListCoupons.meCoupons
//                viewController.title = "Meus cupons"
//                (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
//                self.sidePanelController.showCenterPanelAnimated(true)
//                break
                
//                [
//                    "title":"Meus Ingressos",
//                    "icon":"iconIngressos",
//                    "isShowBadge":true,
//                    "target":"meusIngressos"
//                    ] as [String: Any],
//                [
//                "title":"Meus Tickets",
//                "icon":"iconTickets",
//                "isShowBadge":true,
//                "target":"meusVouchers"
//                ] as [String: Any],
//                [
//                "title":"Meus Cupons",
//                "icon":"iconCoupom",
//                "isShowBadge":true,
//                "target":"meusCoupons"
//                ] as [String: Any],
                
                if target == "meusIngressos" {
                    let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
                    viewController.isListVouchers = false
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else if target == "meusVouchers" {
                    let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
                    viewController.isListVouchers = true
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else if target == "meusCoupons" {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
                    viewController.typeShow = TypeShowListCoupons.meCoupons
                    viewController.title = "Meus cupons"
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else if target == "fidelity" {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListFidelityTableViewController") as! ListFidelityTableViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(target)
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            } else {
                if let target = action["target"] {
                    self.showMenuAccount4all(ProfileOption(rawValue: target as! Int)!)
                }
            }
        }
    }
}

extension NewProfileViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionObject = self.sections[section]
        if let actions = sectionObject["actions"] as? [[String: Any]] {
            return actions.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ActionCellProfile", forIndexPath: indexPath) as! ActionCellProfile
        cell.labelTitle.text = ""
        Util.removeBadgeInMenu(cell.imageViewIcon)
        
        let sectionObject = self.sections[indexPath.section]
        if let actions = sectionObject["actions"] as? [[String: Any]] {
            let action = actions[indexPath.row]
            if let title = action["title"] as? String {
                cell.labelTitle.text = title
            }
            
            if let icon = action["icon"] as? String {
                cell.imageViewIcon.image = UIImage(named: icon)
            }
            
            if let target = action["target"] as? String where target.characters.count > 0 {
                if target == "meusIngressos" {
                    if let isShowBadger = action["isShowBadge"] as? Bool where isShowBadger == true {
                        let count = VoucherPersistence.getAllLocalVouchers([.Cinema,.Event]).count
                        Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                    }
                } else if target == "meusVouchers" {
                    if let isShowBadger = action["isShowBadge"] as? Bool where isShowBadger == true {
                        let count = VoucherPersistence.getAllLocalVouchers([.Order,.Fidelity]).count
                        Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                    }
                } else if target == "meusCoupons" {
                    if let isShowBadger = action["isShowBadge"] as? Bool where isShowBadger == true {
                        let count = CoupomPersistence.getAllLocalCoupons().count
                        Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                    }
                }
            }
        }
        
        return cell
    }
    
}


extension NewProfileViewController: UIAlertViewDelegate {
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            let load = LoadingViewController.sharedManager() as! LoadingViewController
            load.startLoading(self, title: "Aguarde")
            Lib4all.sharedInstance().callLogout { (status) in
                
                load.finishLoading({
                    let notificationCenter = NSNotificationCenter.defaultCenter()
                    notificationCenter.postNotificationName("statusLogin", object: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                })
            }
        }
    }
    
}
