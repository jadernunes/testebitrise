//
//  CupomDetailSource.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

//MARK: - Enums
enum TypeCell: Int {
    case CellHeader             = 0
    case CellActions            = 1
    case CellDescription        = 2
    case CellTerms              = 3
}

enum HeightCellCampaign: CGFloat {
    case CellHeader             = 250
    case CellActions            = 200
    case CellTerms              = 130
    case CellDescription        = 135
}

class CampaignDetailSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableViewCupomInformation   = UITableView()
    var campaign                    : Campaign!
    var superViewController         = DetailsTableViewController()
    let countCells                  = 4
    var api                         : ApiServices! = ApiServices()
    var coupom                      : Coupom!
    
    init(tableView : UITableView, campaign : Campaign, referenceToSuperViewController : UIViewController) {
        super.init()
        
        self.campaign                                  = campaign
        self.tableViewCupomInformation                  = tableView
        self.superViewController                        = (referenceToSuperViewController as! DetailsTableViewController)
        self.tableViewCupomInformation.dataSource       = self
        self.tableViewCupomInformation.delegate         = self
        self.tableViewCupomInformation.backgroundColor  = LayoutManager.backgroundFirstColor
        self.coupom = CoupomPersistence.getCoupomByIdCampaign(self.campaign.id)
        
        self.tableViewCupomInformation.registerNib(UINib(nibName: "CampaignHeaderTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_HEADER_CAMPAING)
        self.tableViewCupomInformation.registerNib(UINib(nibName: "CampaignActionsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_ACTIONS_CAMPAING)
        self.tableViewCupomInformation.registerNib(UINib(nibName: "CampaignTermsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_TERMS_CAMPAING)
        self.tableViewCupomInformation.registerNib(UINib(nibName: "CampaignDescriptionTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_DESCRIPTION_CAMPAING)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countCells
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch TypeCell(rawValue: indexPath.row)! {
        case .CellHeader:
            return CampaignHeaderTableViewCell.getConfiguredCell(tableView, campaign: self.campaign)
            
        case .CellActions:
            return CampaignActionsTableViewCell.getConfiguredCell(tableView, campaign: self.campaign,superViewController: self)
            
        case .CellTerms:
            return CampaignTermsTableViewCell.getConfiguredCell(tableView, campaign: self.campaign)
        
        case .CellDescription:
            return CampaignDescriptionTableViewCell.getConfiguredCell(tableView, campaign: self.campaign)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch TypeCell(rawValue: indexPath.row)! {
        case .CellHeader:
            return HeightCellCampaign.CellHeader.rawValue
        case .CellActions:
            return HeightCellCampaign.CellActions.rawValue
        case .CellTerms:
            return HeightCellCampaign.CellTerms.rawValue + (CGFloat((campaign?.terms.characters.count)!)/2.0)
        case .CellDescription:
            return HeightCellCampaign.CellDescription.rawValue + (CGFloat((campaign?.longDesc.characters.count)!)/2.0)
        }
    }
    
    //MARK: - Button Action pressed
    
    func buttonActionsOfferPressed() {
        
        let lib4all = Lib4all()
        
        if lib4all.hasUserLogged() {
            
            if (self.coupom == nil) { //Claim
                self.superViewController.startLoad()
                self.claimRemoteCoupom()
            }
        } else {
            lib4all.callLogin(self.superViewController, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                self.superViewController.startLoad()
                self.claimRemoteCoupom()
                SessionManager.sharedInstance.registerDevice()
            })
        }
    }
    
    //MARK: - Claim coupom
    
    private func claimRemoteCoupom() {
        api.successCase = {(obj) in
            self.superViewController.stopLoad()
            
            //Atualizar texto do botão para: "Cupom Baixado"
            self.coupom = CoupomPersistence.getCoupomByIdCampaign(self.campaign.id)
            self.getCampaignByID()
        }
        
        api.failureCase = {(msg) in
            self.superViewController.stopLoad()
        }
        
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    let user = User.sharedUser()
                    api.claimCoupom(self.campaign.id, sessionToken: user.token)
                }
            }
        }
    }
    
    //MARK: Campaign
    func getCampaignByID() {
        api.successCase = {(obj) in
            self.campaign = CampaignPersitence.getCampaignById(self.coupom.idCampaign)
            self.tableViewCupomInformation.reloadData()
        }
        
        api.failureCase = {(msg) in
        }
        
        api.getCampaignByID(0, idCampaign: self.coupom.idCampaign)
    }
}
