//
//  ReservaMesaTableExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension ReservaMesaViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        

        switch indexPath.row {
        case 0:
            let cell = self.createCell(tableView, identifier: CELL_QUESTION_BOOKING, indexRow: indexPath.row, pergunta: kMessageTableSizeBooking)
            return cell as! UITableViewCell
        case 1:
            let cell = self.createCell(tableView, identifier: CELL_QUESTION_BOOKING, indexRow: indexPath.row, pergunta: kMessageDayBooking)
            return cell as! UITableViewCell
        case 2:
            let cell = self.createCell(tableView, identifier: CELL_QUESTION_BOOKING, indexRow: indexPath.row, pergunta: kMessageDateBooking)
            return cell as! UITableViewCell
        case 3 :
            let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(CELL_BANNER_BOOKING, forIndexPath: indexPath)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_BUTTON_BOOKING, forIndexPath: indexPath) as! ReservaMesaConfirmarCell
            cell.rootViewController = self
            return cell
        default:
            break
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
}
