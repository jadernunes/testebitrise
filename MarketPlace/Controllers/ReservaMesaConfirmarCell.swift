//
//  ReservaMesaConfirmarCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 03/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ReservaMesaConfirmarCell: UITableViewCell {
    @IBOutlet weak var btnConfirmaReserva       : UIButton!
    var rootViewController                      : UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buttonConfirmSchedulePressed(sender: AnyObject)
    {
        
            let lib4all = Lib4all()
            if lib4all.hasUserLogged()
            {
                
            }
            else
            {
                lib4all.callLogin(rootViewController, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                    self.configurationButtonLogin4all()
                    SessionManager.sharedInstance.registerDevice()
                })
            }
    }
    //MARK: - Configuration 4all button
    func configurationButtonLogin4all()
    {
        let lib4all = Lib4all()
        
        if lib4all.hasUserLogged()
        {
            
        }
        else
        {
            btnConfirmaReserva.setTitle(kEnter, forState: UIControlState.Normal)
        }
    }
    
    
}
