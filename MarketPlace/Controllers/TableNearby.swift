//
//  TableNearby.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import MapKit

class TableNearby: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var listGeneralUnities: [Unity]?
    var superViewController: UIViewController?
    
    init(listUnities: [Unity]?, superController: UIViewController?) {
        super.init()
        self.listGeneralUnities = listUnities
        self.superViewController = superController
    }
    
    private func callDetails(unity: Unity?, group: Group?) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        if (unity != nil) {
            detailsVc.title = unity?.name
            detailsVc.idObjectReceived = unity?.id
            detailsVc.typeObjet = TypeObjectDetail.Store
        } else {
            detailsVc.title = group?.name
            detailsVc.idObjectReceived = group?.id
            detailsVc.typeObjet = TypeObjectDetail.Group
        }
        
        if let superVC = self.superViewController {
            superVC.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
    
    @objc func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 132.0
    }
    
    @objc func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let list = self.listGeneralUnities {
            let unity = list[indexPath.row]
            self.callDetails(unity, group: nil)
        }
    }
    
    @objc func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = listGeneralUnities {
            return list.count
        }
        
        return 0
    }
    
    @objc func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("NewSearchTableViewCell", forIndexPath: indexPath) as! NewSearchTableViewCell
        if let list = self.listGeneralUnities {
            dispatch_async(dispatch_get_main_queue(), {
                
                let unity = list[indexPath.row]
                cell.labelNameUnity.text = unity.name
                cell.imageViewLogoUnity.layer.cornerRadius = 4
                cell.imageViewLogoUnity.layer.masksToBounds = true
                
                if let logo = unity.getLogo() where (logo.value != nil) {
                    cell.imageViewLogoUnity.addImage(logo.value)
                } else {
                    cell.imageViewLogoUnity.addImage(nil)
                }
                
                if let userLocation = LocationManager.shared.location {
                    
                    let distanceFormatter = MKDistanceFormatter()
                    distanceFormatter.units = .Metric
                    distanceFormatter.unitStyle = .Abbreviated
                    distanceFormatter.locale = NSLocale.currentLocale()
                    
                    cell.labelDistance.text = distanceFormatter.stringFromDistance(userLocation.distanceFromLocation(CLLocation(latitude: unity.latitude, longitude: unity.longitude)))
                }
            })
        }
        return cell
    }
}
