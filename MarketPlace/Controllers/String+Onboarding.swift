//
//  String+Onboarding.swift
//  Donna
//
//  Created by Rodrigo Kreutz on 2/15/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

extension String {
    
    // MARK: - Cell identifiers
    static let onboardingContentCollectionViewCellIdentifier = "OnboardingContentCollectionViewCell"
    
    // MARK: - UserDefault keys
    static let onboardingPresentedUserDefaultsKey = "onboardingPresented"
    
    // MARK: - Images identifiers
    static let onboardingFirstPageIcon = "onboarding1"
    static let onboardingSecondPageIcon = "onboarding2"
    static let onboardingThirdPageIcon = "onboarding3"
    static let onboardingFourthPageIcon = "price-icon"
    
    // MARK: - Onboarding text
    static let onboardingFirstPageTitle = "PEÇA COMIDA, COMPRE INGRESSOS, FUJA DAS FILAS E NÃO PERCA MAIS TEMPO"
    static let onboardingFirstPageContent = "Você pode usar sua Conta 4all em restaurantes, festas, shoppings, transporte, entre outros."
    
    static let onboardingSecondPageTitle = "PAGAR COM 4ALL É TÃO FÁCIL QUANTO TIRAR UMA FOTO"
    static let onboardingSecondPageContent = "É só ler o código de barras ou o QR code com o smartphone e pronto!"
    
    static let onboardingThirdPageTitle = "CRIE SUA CONTA 4ALL E DEIXE A CARTEIRA EM CASA"
    static let onboardingThirdPageContent = "Somos muito mais que pagamento. \nSomos pagamentos e serviços que conectam o seu dia. Vamos lá?!"
}
