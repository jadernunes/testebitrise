//
//  VoucherCollectionCell.swift
//  TestePassbook
//
//  Created by Leandro on 10/11/16.
//  Copyright © 2016 Leandro. All rights reserved.
//

import UIKit

class VoucherCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBackground    : UIImageView!
    @IBOutlet weak var imgLogo          : UIImageView!
    @IBOutlet weak var imgQRCode        : UIImageView!
    @IBOutlet weak var lblNome          : UILabel!
    @IBOutlet weak var lblData          : UILabel!
    @IBOutlet weak var lblQRTitulo      : UILabel!
    @IBOutlet weak var lblQRDescricao   : UILabel!
    @IBOutlet weak var lblWinningDesc   : UILabel!
    @IBOutlet weak var activityVerify   : UIActivityIndicatorView!
    @IBOutlet weak var labelActivityVerify: UILabel!
    
    var lblsTextColor                   : UIColor = UIColor.whiteColor()
    
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    override func awakeFromNib() {
        configInicial()
    }
    
    override func didTransitionFromLayout(oldLayout: UICollectionViewLayout, toLayout newLayout: UICollectionViewLayout) {
    }
    
    //MARK - Custom Methods
    func configInicial() {
        /* --- Arredondar os cantos --- */
        imgLogo.layer.cornerRadius = 12
        self.imgLogo.clipsToBounds = true
        
        self.lblNome.text = ""
        self.lblData.text = ""
        self.lblQRTitulo.text = ""
        self.lblQRDescricao.text = ""
        
        self.imgBackground.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.imgBackground.layer.cornerRadius = 15.0
        self.imgBackground.layer.borderWidth = 0.5
        self.imgBackground.layer.masksToBounds = true
        
        self.lblNome.font = LayoutManager.primaryFontWithSize(14, bold: true)
        self.lblNome.textColor = self.lblsTextColor
        self.lblData.font = LayoutManager.primaryFontWithSize(12, bold: false)
        self.lblData.textColor = self.lblsTextColor
        self.lblQRTitulo.font = LayoutManager.primaryFontWithSize(14, bold: true)
        self.lblQRTitulo.textColor = self.lblsTextColor
        self.lblQRDescricao.font = LayoutManager.primaryFontWithSize(12, bold: false)
        self.lblQRDescricao.textColor = self.lblsTextColor
        self.lblWinningDesc.font = LayoutManager.primaryFontWithSize(12, bold: false)
        self.lblWinningDesc.textColor = self.lblsTextColor
        
        self.activityVerify.tintColor = UIColor.whiteColor()
    }
    
}
