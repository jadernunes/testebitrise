//
//  CartItemTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/3/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift
class CartItemTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var buttonRemove: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var constraintImageWidth: NSLayoutConstraint!
    
    var product             : UnityItem!
    var unity               : Unity!
    var orderItem           : OrderItem!
    var rootViewController  : UIViewController!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelPrice.font = LayoutManager.primaryFontWithSize(20)
        self.labelPrice.textColor = LayoutManager.greyTitleItemCart
        
        self.labelQuantity.font = LayoutManager.primaryFontWithSize(20)
        self.labelQuantity.textColor = LayoutManager.greyTitleItemCart
        
        self.labelName.font = LayoutManager.primaryFontWithSize(20)
        self.labelName.textColor = LayoutManager.greyTitleItemCart
        
        self.labelDescription.font = LayoutManager.primaryFontWithSize(15)
        self.labelDescription.textColor = LayoutManager.greyDescItemCart
        
        self.buttonAdd.tintButton(SessionManager.sharedInstance.cardColor, image: UIImage(named: "ic_add")!)
        self.buttonRemove.tintButton(SessionManager.sharedInstance.cardColor, image: UIImage(named: "ic_remove")!)
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func populateCell(item: OrderItem, description: String, rootVc: UIViewController, unity: Unity) {
        
        self.orderItem = item
        self.product = orderItem.unityItem
        self.rootViewController = rootVc
        self.unity = unity
        self.labelPrice.text = Util.formatCurrency(item.total/100)
        self.labelDescription.text = description
        self.labelName.text = item.unityItem?.title
        self.labelQuantity.text = String(Int(item.quantity))
        var url = ""
        if item.unityItem?.thumb != nil {
            url = (item.unityItem?.thumb!)!
            self.imageViewProduct.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
                if image != nil {
                    self.imageViewProduct.contentMode      = .ScaleAspectFill
                }
            }
        }
            
        else {
            self.constraintImageWidth.constant = 0
        }
        
    }

    
    @IBAction func removeItem(sender: AnyObject) {
        if orderItem.quantity == 1 {
            
            let removeAlert = UIAlertController(title: self.product.title, message: kMessageRemoveItemFromCart, preferredStyle: UIAlertControllerStyle.Alert)
            
            removeAlert.addAction(UIAlertAction(title: kAlertButtonYes, style: .Default, handler: { (action: UIAlertAction!) in
                if OrderEntityManager.sharedInstance.removeOrderItemInCart(self.orderItem) == false {
                    Util.showAlert(self.rootViewController, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
                } else {
                    let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(self.product, unity: self.unity)
                    self.labelQuantity.text = "\(quantityInCart)"
                    CartCustomManager.sharedInstance.updateCart()
                    if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                        CartCustomManager.sharedInstance.hideCart()
                        let realm = try! Realm()
                        try! realm.write() {
                            let cart = OrderEntityManager.sharedInstance.getCart(0)
                            cart?.placeLabel = ""
                            cart?.address = nil
                            cart?.idOrderType = OrderType.None.rawValue
                            cart?.total = 0.0
                            cart?.productTotal = 0.0
                            cart?.deliveryFee = 0.0
                            
                        }
                        if let tableview = self.superview?.superview as? UITableView {
                            tableview.reloadData()
                        }
                        self.rootViewController.dismissViewControllerAnimated(true, completion: nil)
                    }
                    else {
                        if let tableview = self.superview?.superview as? UITableView {
                            tableview.reloadData()
                        }
                        self.callNotification()
                    }
                }
            }))
            
            removeAlert.addAction(UIAlertAction(title: kAlertButtonNo, style: .Cancel, handler: { (action: UIAlertAction!) in
            }))
            if let tableview = self.superview?.superview as? UITableView {
                tableview.reloadData()
            }
            rootViewController.presentViewController(removeAlert, animated: true, completion: nil)
            
        } else {
            
            if orderItem.quantity == 1 {
                let removeAlert = UIAlertController(title: self.product.title, message: kMessageRemoveItemFromCart, preferredStyle: UIAlertControllerStyle.Alert)
                
                removeAlert.addAction(UIAlertAction(title: kAlertButtonYes, style: .Default, handler: { (action: UIAlertAction!) in
                    if self.product.idUnityItemType == UnitItemType.Pizza.rawValue ||
                        self.product.idUnityItemType == UnitItemType.Combo.rawValue {
                        if OrderEntityManager.sharedInstance.removeOrderItemInCart(self.orderItem) == false {
                            Util.showAlert(self.rootViewController, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
                        } else {
                            let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(self.product, unity: self.unity)
                            self.labelQuantity.text = "\(quantityInCart)"
                            CartCustomManager.sharedInstance.updateCart()
                            if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                                CartCustomManager.sharedInstance.hideCart()
                                let realm = try! Realm()
                                try! realm.write() {
                                    let cart = OrderEntityManager.sharedInstance.getCart(0)
                                    cart?.placeLabel = ""
                                    cart?.address = nil
                                    cart?.idOrderType = OrderType.None.rawValue
                                    cart?.total = 0.0
                                    cart?.productTotal = 0.0
                                    cart?.deliveryFee = 0.0
                                    
                                }
                                if let tableview = self.superview?.superview as? UITableView {
                                    tableview.reloadData()
                                }
                                self.rootViewController.dismissViewControllerAnimated(true, completion: nil)
                            }
                            else {
                                if let tableview = self.superview?.superview as? UITableView {
                                    tableview.reloadData()
                                }
                                self.callNotification()
                            }
                        }
                    } else {
                        if OrderEntityManager.sharedInstance.removeItemFromCart(self.product, unity: self.unity) == false {
                            Util.showAlert(self.rootViewController, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
                        }
                        else{
                            let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(self.product, unity: self.unity)
                            self.labelQuantity.text = "\(quantityInCart)"
                            CartCustomManager.sharedInstance.updateCart()
                            if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                                CartCustomManager.sharedInstance.hideCart()
                                let realm = try! Realm()
                                try! realm.write() {
                                    let cart = OrderEntityManager.sharedInstance.getCart(0)
                                    cart?.placeLabel = ""
                                    cart?.address = nil
                                    cart?.idOrderType = OrderType.None.rawValue
                                    cart?.total = 0.0
                                    cart?.productTotal = 0.0
                                    cart?.deliveryFee = 0.0
                                    
                                }
                                if let tableview = self.superview?.superview as? UITableView {
                                    tableview.reloadData()
                                }
                                self.rootViewController.dismissViewControllerAnimated(true, completion: nil)
                            }
                            else {
                                self.callNotification()
                            }
                        }
                    }
                }))
                
                removeAlert.addAction(UIAlertAction(title: kAlertButtonNo, style: .Cancel, handler: { (action: UIAlertAction!) in
                }))
                if let tableview = self.superview?.superview as? UITableView {
                    tableview.reloadData()
                }
                rootViewController.presentViewController(removeAlert, animated: true, completion: nil)
            }
                
            else {
                if self.product.idUnityItemType == UnitItemType.Pizza.rawValue ||
                    self.product.idUnityItemType == UnitItemType.Combo.rawValue {
                    
                    if OrderEntityManager.sharedInstance.removeOrderItemInCart(orderItem) == false {
                        Util.showAlert(self.rootViewController, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
                    } else {
                        let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(self.product, unity: self.unity)
                        self.labelQuantity.text = "\(quantityInCart)"
                        CartCustomManager.sharedInstance.updateCart()
                        if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                            CartCustomManager.sharedInstance.hideCart()
                            let realm = try! Realm()
                            try! realm.write() {
                                let cart = OrderEntityManager.sharedInstance.getCart(0)
                                cart?.placeLabel = ""
                                cart?.address = nil
                                cart?.idOrderType = OrderType.None.rawValue
                                cart?.total = 0.0
                                cart?.productTotal = 0.0
                                cart?.deliveryFee = 0.0
                                
                            }
                            if let tableview = self.superview?.superview as? UITableView {
                                tableview.reloadData()
                            }
                            rootViewController.dismissViewControllerAnimated(true, completion: nil)
                        }
                        else {
                            self.callNotification()
                        }
                    }
                    
                } else {
                    //if OrderEntityManager.sharedInstance.removeItemFromCart(self.product, unity: self.unity) == false {
                    if OrderEntityManager.sharedInstance.removeOrderItemInCart(orderItem) == false {
                        Util.showAlert(self.rootViewController, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
                    }
                    else{
                        let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(self.product, unity: self.unity)
                        self.labelQuantity.text = "\(quantityInCart)"
                        CartCustomManager.sharedInstance.updateCart()
                        if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                            CartCustomManager.sharedInstance.hideCart()
                            let realm = try! Realm()
                            try! realm.write() {
                                let cart = OrderEntityManager.sharedInstance.getCart(0)
                                cart?.placeLabel = ""
                                cart?.address = nil
                                cart?.idOrderType = OrderType.None.rawValue
                                cart?.total = 0.0
                                cart?.productTotal = 0.0
                                cart?.deliveryFee = 0.0
                                
                            }
                            if let tableview = self.superview?.superview as? UITableView {
                                tableview.reloadData()
                            }
                            self.rootViewController.dismissViewControllerAnimated(true, completion: nil)
                        }
                        else {
                            self.callNotification()
                        }
                    }
                }
            }
        }
        if let tableview = self.superview?.superview as? UITableView {
            tableview.reloadData()
        }
    }

    
    @IBAction func addItem(sender: AnyObject) {
        do {
            try OrderEntityManager.sharedInstance.somarItem(orderItem)
            let quantityInCart = OrderEntityManager.sharedInstance.getItemQuantity(product, unity: unity)
            labelQuantity.text = "\(quantityInCart)"
            CartCustomManager.sharedInstance.updateCart()
            callNotification()
        }catch _ as NSError {
        }
        if let tableview = self.superview?.superview as? UITableView {
            tableview.reloadData()
        }
    }
    
    func callNotification() {
        dispatch_async(dispatch_get_main_queue(), {
            NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_CART_OBSERVER, object: nil)
        })
    }


}
