//
//  OnboardingContent.swift
//  Donna
//
//  Created by Jáder Borba Nunes on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

struct OnboardingContent {
    let textTitle: String
    let colorTitle: UIColor
    
    let textSubtitle: String
    let colorSubtitle: UIColor
    
    let colorTextButtonSkip: UIColor
    
    let colorTextButtonSignin: UIColor
    let colorBackgroudButtonSignin: UIColor
    let textButtonSignin: String
    
    let colorTextButtonSignup: UIColor
    let colorBackgroudButtonSignup: UIColor
    let textButtonSignup: String
    
    let colorBackground: UIColor
    let imageIcon: UIImage?
    
    init(textTitle: String, colorTitle: UIColor, textSubtitle: String, colorSubtitle: UIColor, colorTextButtonSkip: UIColor, colorTextButtonSignin: UIColor, colorBackgroudButtonSignin: UIColor, colorBackground: UIColor,  imageName: String, textButtonSignin: String, textButtonSignup: String, colorTextButtonSignup: UIColor, colorBackgroudButtonSignup: UIColor) {
        self.textTitle = textTitle
        self.colorTitle = colorTitle
        self.textSubtitle = textSubtitle
        self.colorSubtitle = colorSubtitle
        self.colorTextButtonSkip = colorTextButtonSkip
        self.colorTextButtonSignin = colorTextButtonSignin
        self.colorBackgroudButtonSignin = colorBackgroudButtonSignin
        self.colorBackground = colorBackground
        self.imageIcon = UIImage(named: imageName)
        self.textButtonSignin = textButtonSignin
        self.colorTextButtonSignup = colorTextButtonSignup
        self.colorBackgroudButtonSignup = colorBackgroudButtonSignup
        self.textButtonSignup = textButtonSignup
    }
}

let onboardingContentPages = [
    OnboardingContent(
        textTitle: .onboardingFirstPageTitle,
        colorTitle: UIColor.green4all(),
        textSubtitle: .onboardingFirstPageContent,
        colorSubtitle: UIColor.gray4all(),
        colorTextButtonSkip: UIColor.green4all(),
        colorTextButtonSignin: UIColor.whiteColor(),
        colorBackgroudButtonSignin: UIColor.green4all(),
        colorBackground: UIColor.whiteColor(),
        imageName: .onboardingFirstPageIcon,
        textButtonSignin: "Entrar",
        textButtonSignup: "Cadastre-se",
        colorTextButtonSignup: UIColor.whiteColor(),
        colorBackgroudButtonSignup: UIColor.green4all()),
    
    OnboardingContent(
        textTitle: .onboardingSecondPageTitle,
        colorTitle: UIColor.whiteColor(),
        textSubtitle: .onboardingSecondPageContent,
        colorSubtitle: UIColor.whiteColor(),
        colorTextButtonSkip: UIColor.whiteColor(),
        colorTextButtonSignin: UIColor.whiteColor(),
        colorBackgroudButtonSignin: UIColor.blueGreen4all(),
        colorBackground: UIColor.green4all(),
        imageName: .onboardingSecondPageIcon,
        textButtonSignin: "Entrar",
        textButtonSignup: "Cadastre-se",
        colorTextButtonSignup: UIColor.whiteColor(),
        colorBackgroudButtonSignup: UIColor.blueGreen4all()),
    
    OnboardingContent(
        textTitle: .onboardingThirdPageTitle,
        colorTitle: UIColor.green4all(),
        textSubtitle: .onboardingThirdPageContent,
        colorSubtitle: UIColor.gray4all(),
        colorTextButtonSkip: UIColor.green4all(),
        colorTextButtonSignin: UIColor.whiteColor(),
        colorBackgroudButtonSignin: UIColor.green4all(),
        colorBackground: UIColor.whiteColor(),
        imageName: .onboardingThirdPageIcon,
        textButtonSignin: "Entrar",
        textButtonSignup: "Cadastre-se",
        colorTextButtonSignup: UIColor.whiteColor(),
        colorBackgroudButtonSignup: UIColor.green4all())
]
