//
//  MapViewViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 26/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RealmSwift
/*
 
MapView Delegation na Extension dessa classe (VCMapView.swift)
 
 */

class UnityMapViewController: UIViewController {
    
    @IBOutlet var mapView                   : MKMapView!
    var uLocation                           = CLLocation()
    var nearestUnity                        = NearestUnity()
    let locationManager                     = CLLocationManager()
    var unitiesList                         : Results<Unity>?

    var imgLogoUnity                        = UIImageView()
    var lblTituloUnity                      = UILabel()
    var lblSubtituloUnity                   = UILabel()
    var viewNearestUnity                    = UIView()
    var preferredStatusBar                  : UIStatusBarStyle = .Default
    var firstLoad                           = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            mapView.showsUserLocation = true
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        self.createNearestView()
        self.getLocalUnities()
        
        NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: #selector(self.searchNearestUnity), userInfo: nil, repeats: true)

    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.preferredStatusBar
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.configInicial()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.trackUser(self)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Custom Methods
    func createNearestView() {
        let viewSize:CGFloat = 80.0
        viewNearestUnity = UIView.init(frame: CGRectMake(0, -viewSize, self.view.frame.width, viewSize))
        viewNearestUnity.backgroundColor = Util.hexStringToUIColor("3D3D3D").colorWithAlphaComponent(0.9)
        imgLogoUnity = UIImageView.init(frame: CGRectMake(10, viewNearestUnity.frame.height*0.5-20, 50, 50))
        imgLogoUnity.contentMode = .ScaleAspectFill
        imgLogoUnity.backgroundColor = UIColor.clearColor()
        imgLogoUnity.layer.cornerRadius = imgLogoUnity.frame.size.height * 0.5
        imgLogoUnity.layer.masksToBounds = true
        imgLogoUnity.layer.borderWidth = 0
        viewNearestUnity.addSubview(imgLogoUnity)
        lblTituloUnity = UILabel.init(frame: CGRectMake(30+imgLogoUnity.frame.size.width, 7, viewNearestUnity.frame.width*0.5, 50))
        lblTituloUnity.textColor = UIColor.whiteColor()
        lblTituloUnity.font = LayoutManager.primaryFontWithSize(20)
        viewNearestUnity.addSubview(lblTituloUnity)
        lblSubtituloUnity = UILabel.init(frame: CGRectMake(30+imgLogoUnity.frame.size.width, lblTituloUnity.frame.origin.y + 22, viewNearestUnity.frame.width*0.5, 50))
        lblSubtituloUnity.numberOfLines = 2
        lblSubtituloUnity.textColor = UIColor.whiteColor()
        lblSubtituloUnity.font = LayoutManager.primaryFontWithSize(10)
        viewNearestUnity.addSubview(lblSubtituloUnity)
        let imgDisclousure = UIImageView.init(frame: CGRectMake(self.view.frame.width - 60, viewNearestUnity.frame.height*0.5-10, 40, 40))
        imgDisclousure.contentMode = .ScaleAspectFit
        imgDisclousure.image = UIImage.init(named: "iconDisclosure")
        imgDisclousure.tintImage(LayoutManager.green4all)
        viewNearestUnity.addSubview(imgDisclousure)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.nearestUnityViewTap(_:)))
        self.viewNearestUnity.addGestureRecognizer(tapGesture)
        
        
        self.view.addSubview(viewNearestUnity)
        
    }
    
    func nearestUnityViewTap(sender:UITapGestureRecognizer){
        let object = nearestUnity.unity!
        SessionManager.sharedInstance.cardColor = LayoutManager.green4all
        
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        detailsVc.title = object.name
        detailsVc.idObjectReceived = object.id
        detailsVc.typeObjet = TypeObjectDetail.Store
//        detailsVc.hidesBottomBarWhenPushed = true
//        self.showNavBar()
        detailsVc.title = object.name
        self.navigationController?.pushViewController(detailsVc, animated: true)
        
        //remove o recognizer para que ao tocar diretamente no pin, não vá para o detalhe
        for recognizer in view.gestureRecognizers ?? [] {
            view.removeGestureRecognizer(recognizer)
        }
    }
    
    func updateNearestUnity(unity: Unity) {
        if unity.latitude != 0 && unity.longitude != 0 {
            let newDistance:Double = uLocation.distanceFromLocation(CLLocation.init(latitude: unity.latitude, longitude: unity.longitude))
            if newDistance < nearestUnity.minimumDistance {
                nearestUnity.unity = unity
                nearestUnity.distance = newDistance
                self.showNearestUnity(0.3)
            } else if nearestUnity.distance == 0 {
                if newDistance/1000 < nearestUnity.minimumDistance { //divide para ter a medida em KM
                    nearestUnity.unity = unity
                    nearestUnity.distance = newDistance
                    self.showNearestUnity(0.3)
                }
            }
        }
    }
    
    func searchNearestUnity() {
        nearestUnity.unity = nil
        for unity in self.unitiesList! {
            self.updateNearestUnity(unity)
        }
        if nearestUnity.unity == nil {
            self.hideNearestUnity(0.3)
        }
    }
    
    func showNearestUnity(duration: NSTimeInterval) {
        /* --- Se a view ja esta aparecendo, não anima novamente --- */
        if self.viewNearestUnity.frame.origin.y <= 0 {
            self.preferredStatusBar = .LightContent
            self.setNeedsStatusBarAppearanceUpdate()
            lblTituloUnity.text = nearestUnity.unity?.name
            lblSubtituloUnity.text = nearestUnity.unity?.address
            if let unity = nearestUnity.unity {
                imgLogoUnity.addImage(unity.getLogo()?.value)
            }
            UIView.animateWithDuration(duration) {
                self.viewNearestUnity.frame = CGRectMake(0,
                                                         0,
                                                         self.viewNearestUnity.frame.width,
                                                         self.viewNearestUnity.frame.height)
            }
        }
    }
    
    func hideNearestUnity(duration: NSTimeInterval) {
        if self.viewNearestUnity.frame.origin.y >= 0 {
            self.preferredStatusBar = .Default
            self.setNeedsStatusBarAppearanceUpdate()
            UIView.animateWithDuration(duration) {
                self.viewNearestUnity.frame = CGRectMake(0,
                                                         -self.viewNearestUnity.frame.height,
                                                         self.viewNearestUnity.frame.width,
                                                         self.viewNearestUnity.frame.height)
            }
        }
    }
    
    func configInicial() {
        self.hideNavBar()
        self.mapView.delegate = self
        
        /* --- Efeito sombra --- */
        self.viewNearestUnity.layer.masksToBounds = false;
        self.viewNearestUnity.layer.shadowOffset = CGSizeMake(0, 10);
        self.viewNearestUnity.layer.shadowRadius = 10;
        self.viewNearestUnity.layer.shadowOpacity = 0.3;
    }
    
    func getLocationByAddress(address: String, unity :Unity) {
        
        let location = address;
        let geocoder:CLGeocoder = CLGeocoder();
        geocoder.geocodeAddressString(location) { (placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            if placemarks?.count > 0 {
                let topResult:CLPlacemark = placemarks![0];
                let placemark: MKPlacemark = MKPlacemark(placemark: topResult);
                let annotation = UnityAnntation(coordinate:(placemark.location?.coordinate)!, title:unity.name!, subtitle: unity.desc!, unity: unity)
                self.mapView.addAnnotation(annotation);
                //Atualizar latitude longitude
                let realm = try! Realm()
                try! realm.write {
                    unity.latitude = annotation.coordinate.latitude
                    unity.longitude = annotation.coordinate.longitude
                }
            }
        }
        
    }
    
    ///define pins with unities retrieved
    func prepareAnnotations(results : Results<Unity>) {
        for unity in results {
            let location        = CLLocationCoordinate2D(latitude: unity.latitude, longitude: unity.longitude)
            var title           : String = ""
            var subtitle        : String = ""
            if let name = unity.name  {
                title = name
            }
            if let desc = unity.desc {
                subtitle = desc
            }
            if unity.latitude == 0 || unity.longitude == 0 {
                /* --- Vai voltar, favor Jáder, ao ler isto, compreender. Ass. Lelê --- */
                //self.getLocationByAddress(unity.address!, unity: unity)
            } else {
                let annotation = UnityAnntation(coordinate:location, title:title, subtitle: subtitle,unity: unity)
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    func getAllUnities()
    {
        let api = ApiServices()
        
        /* --- Caso de sucesso busca os vouchers --- */
        api.successCase = {(obj) in
            
            if let objAux = obj {
                if let listUnities = objAux.valueForKey("unities") as? [[String:AnyObject]] {
                    
                    var listIDs = ""
                    
                    for object in listUnities {
                        if let unity = object as? Unity {
                            for objectShift in unity.shifts {
                                if let shift = objectShift as? Shifts {
                                    listIDs += "," + String(shift.id)
                                }
                            }
                        }
                    }
                    
                    if listUnities.count > 0 {
                        let listIdsReceived = UnityPersistence.importFromArray(listUnities)
                        Util.syncObject(Unity.className(), stringIDs: listIdsReceived)
                        Util.syncObject(Shifts.className(), stringIDs: listIDs)
                    }
                }
            }
            
            self.getLocalUnities()
        }
        
        api.failureCase = {(msg) in
        }
        api.listUnities()
    }
    
    func getLocalUnities() {
        self.unitiesList = UnityPersistence.getAllUnities()
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.prepareAnnotations(self.unitiesList!)
    }
    
    func showNavBar(navBarTintColor:UIColor?=nil) {
        if let barColor:UIColor = navBarTintColor {
            self.navigationController?.navigationBar.barTintColor = barColor
        }
        else {
            self.navigationController?.navigationBar.barTintColor = LayoutManager.green4all
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideNavBar() {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func trackUser(sender: AnyObject) {
        let latitude:CLLocationDegrees = uLocation.coordinate.latitude
        let longitude:CLLocationDegrees = uLocation.coordinate.longitude
        let latDelta:CLLocationDegrees = 0.02
        let lonDelta:CLLocationDegrees = 0.02
        let span = MKCoordinateSpanMake(latDelta, lonDelta)
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        let region = MKCoordinateRegionMake(location, span)
        self.searchNearestUnity()
        mapView.setRegion(region, animated: true)
    }

}
