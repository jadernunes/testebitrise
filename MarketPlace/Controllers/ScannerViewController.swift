//
//  ScannerViewController.swift
//  MarketPlace
//
//  Created by Luciano on 7/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    //IBOutlets
    @IBOutlet weak var qrFrame  : UIImageView!
    @IBOutlet weak var labelInfo: UILabel!
    
    //Variables
    var captureSession      :AVCaptureSession?
    var videoDevice         :AVCaptureDevice?
    var videoPreviewLayer   :AVCaptureVideoPreviewLayer?
    var videoInput          :AVCaptureDeviceInput?
    var metadataOutput      :AVCaptureMetadataOutput?
    var qrCodeFrameView     :UIView?
    var didReadCode         :Bool! = false
    var didFoundTransaction : Bool = false
    var barCode             : String = ""
    var isRunning           : Bool = false
    var delegate            : ScannerQrCodeProtocol!
    var titleString         : String = ""
    
    // Added to support different barcodes
    var allowedBarcodeTypes = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem?.setTitleTextAttributes( [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Normal)
        Util.setNavigationBarWithGradient((self.navigationController?.navigationBar)!)
        labelInfo.font = LayoutManager.primaryFontWithSize(16)
        labelInfo.textColor = UIColor.whiteColor()
        allowedBarcodeTypes.addObject("org.iso.QRCode")
        allowedBarcodeTypes.addObject("org.iso.PDF417")
        allowedBarcodeTypes.addObject("org.gs1.UPC-E")
        allowedBarcodeTypes.addObject("org.iso.Aztec")
        allowedBarcodeTypes.addObject("org.iso.Code39")
        allowedBarcodeTypes.addObject("org.iso.Code39Mod43")
        allowedBarcodeTypes.addObject("org.gs1.EAN-13")
        allowedBarcodeTypes.addObject("org.gs1.EAN-8")
        allowedBarcodeTypes.addObject("com.intermec.Code93")
        allowedBarcodeTypes.addObject("org.iso.Code128")
        allowedBarcodeTypes.addObject("org.ansi.Interleaved2of5")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenQRAndTicketReader)
        CartCustomManager.sharedInstance.hideCart()
        qrFrame.tintImage(SessionManager.sharedInstance.cardColor)

        //tint navigationbar back button
        navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        self.setupCaptureSession()
        if videoPreviewLayer != nil {
            videoPreviewLayer!.frame = self.view.bounds
            self.startRunning()
            self.view.layer.addSublayer(self.videoPreviewLayer!)
        }else{
            //TODO : Tratar quando câmera indisponível
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        didReadCode = false
        createOverlay()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopRunning()
    }
    
    @IBAction func closeVc(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createOverlay()
    {
        
        self.view.setNeedsLayout()
        
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
        
        let overlayView = UIView()
        overlayView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        overlayView.alpha = 0.5
        overlayView.backgroundColor = UIColor.blackColor()
        overlayView.tag = 100
        self.view.addSubview(overlayView)
        
        let maskLayer = CAShapeLayer()
        
        // Create a path with the rectangle in it.
        let path = CGPathCreateMutable()
        qrFrame.layoutIfNeeded()
        let rect = CGRectMake(qrFrame.frame.origin.x+2, qrFrame.frame.origin.y+2, qrFrame.frame.width-3, qrFrame.frame.height-4)
        
        let dontknow =  UIBezierPath(roundedRect: rect, cornerRadius: 28)
        dontknow.stroke()
        CGPathAddPath(path, nil, dontknow.CGPath)
        CGPathAddRect(path, nil, CGRectMake(0, 0, overlayView.frame.width, overlayView.frame.height))
        maskLayer.backgroundColor = UIColor.blackColor().CGColor
        
        maskLayer.path = path;
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        // Release the path since it's not covered by ARC.
        overlayView.layer.mask = maskLayer
        overlayView.clipsToBounds = true
        qrFrame.alpha = 0
        qrFrame.tintImage(SessionManager.sharedInstance.cardColor)
        overlayView.alpha = 0
        self.view.bringSubviewToFront(qrFrame.superview!)
        self.view.bringSubviewToFront(qrFrame)
        UIView.animateWithDuration(0.3) { () -> Void in
            overlayView.alpha = 0.5
            self.qrFrame.alpha = 1.0
        }
        self.view.bringSubviewToFront(labelInfo)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCaptureSession(){
        if (captureSession != nil){
            return
        }
        
        videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        if videoDevice == nil{
            NSLog("Não foi localizado camera")
            return
        }
        do{
            
            captureSession = AVCaptureSession()
            videoInput = try AVCaptureDeviceInput(device: videoDevice)
            
            if captureSession?.canAddInput(videoInput) == true{
                captureSession?.addInput(videoInput)
            }
            
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            
            //Here the metada is capture and processed
            metadataOutput = AVCaptureMetadataOutput()
            let metadataQueue : dispatch_queue_t = dispatch_queue_create("com.4all.client.metadata", nil)
            
            metadataOutput?.setMetadataObjectsDelegate(self, queue: metadataQueue)
            
            if captureSession?.canAddOutput(metadataOutput) == true{
                captureSession?.addOutput(metadataOutput)
            }
            
        }catch{
            
        }
    }
    
    func startRunning(){
        if isRunning == true{
            return
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.captureSession?.startRunning()
            self.metadataOutput?.metadataObjectTypes = self.metadataOutput?.availableMetadataObjectTypes
            self.isRunning = true
        }
    }
    
    func stopRunning(){
        if isRunning == false{
            return
        }
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.captureSession?.stopRunning()
            self.videoPreviewLayer?.removeFromSuperlayer()
            self.isRunning = false
        }
        
    }
    
    //Mark: AV Delegate functions
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            //qrCodeFrameView?.frame = CGRectZero
            //updateBox()
            //messageLabel.text = "Nenhum QR Code detectado"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
        
        //videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
        if metadataObj != nil {
            let barCode = Barcode.processMetadataObject(metadataObj)
            
            for str in allowedBarcodeTypes{
                if barCode.getBarcodeType() == str as! String && !didReadCode{
                        didReadCode = true
                        self.validBarcodeFound(barCode)
                        return
                }
            }
            
            self.invalidBarcodeFound()
            return
        }
    }
    
    func invalidBarcodeFound() {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.stopRunning()
            self.dismissViewControllerAnimated(true, completion: {
                self.delegate.didScanInvalidCode()
            })
        }

    }
    func validBarcodeFound(barCode : Barcode){
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.stopRunning()
            self.barCode = barCode.getBarcodeData()
            self.dismissViewControllerAnimated(true, completion: { 
                self.delegate.didScanValidCode(self.barCode)
            })
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if didReadCode == true {
            return false
        }else{
            didReadCode = true
            return true
        }
    }
}
