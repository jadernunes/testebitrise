//
//  NewVoucherDetail.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewVoucherDetail: UIViewController {

    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var labelCodeQR: UILabel!
    @IBOutlet weak var imageViewUnity: UIImageView!
    @IBOutlet weak var labelNameUnity: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelTitleDescription: UILabel!
    @IBOutlet weak var labelDescription: UITextView!
    @IBOutlet weak var labelTitleQR: UILabel!
    @IBOutlet weak var imageViewQR: UIImageView!
    @IBOutlet weak var viewSeparatorNameUnity: UIView!
    @IBOutlet weak var imageViewSeparatorQR: UIImageView!
    
    var voucher: Voucher!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNavigation.backgroundColor = UIColor.green4all()
        self.viewSeparatorNameUnity.backgroundColor = UIColor.lightGray4all()
        self.labelTitleDescription.textColor = UIColor.blueGreen4all()
        self.labelDescription.textColor = UIColor.gray4all()
        self.labelHour.textColor = UIColor.gray4all()
        self.labelDate.textColor = UIColor.gray4all()
        self.imageViewUnity.layer.cornerRadius = 8
        self.imageViewUnity.layer.masksToBounds = true
        self.labelTitleQR.textColor = UIColor.gray4all()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.imageViewUnity.addImage(nil)
        self.labelDate.text = NSString.getDateStringFromTimestamp(voucher.timestamp)
        self.labelHour.text = NSString.getHourStringFromTimestamp(voucher.timestamp)
        self.labelDescription.text = ""
        
        if voucher.idVoucherStatus == VoucherStatus.Cancelled.rawValue ||
            voucher.idVoucherStatus == VoucherStatus.Expired.rawValue ||
            voucher.idVoucherStatus == VoucherStatus.Used.rawValue {
            self.imageViewQR.hidden = true
            self.labelTitleQR.hidden = true
            self.imageViewSeparatorQR.hidden = true
            self.viewNavigation.backgroundColor = UIColor.gray4all()
        } else {
            self.imageViewQR.hidden = false
            self.labelTitleQR.hidden = false
            self.imageViewSeparatorQR.hidden = false
            self.viewNavigation.backgroundColor = UIColor.green4all()
        }
        
        if let order = self.voucher.order {
            self.labelCodeQR.text = "#\(order.id)"
            if let unity = UnityPersistence.getUnityByID("\(order.idUnity)") {
                if let logo = unity.getLogo() {
                    self.imageViewUnity.addImage(logo.value)
                }
                
                self.labelNameUnity.text = unity.name
            }
            
            var text = "Total: \(Util.formatCurrency(Double(order.total / 100)))\n"
            for item in order.orderItems {
                if let unityItem = item.unityItem {
                    let qtd:String? = String(Int(item.quantity))
                    text += "- " + qtd! + " x " + unityItem.title + "\n"
                }
            }
            self.labelDescription.text = text
        }
        
        if let fidelity = voucher.fidelity {
            self.labelCodeQR.text = "#\(fidelity.id)"
            if let unity = UnityPersistence.getUnityByID("\(fidelity.idUnity)") {
                if let logo = unity.getLogo() {
                    self.imageViewUnity.addImage(logo.value)
                }
                
                self.labelNameUnity.text = unity.name
            }
            
            self.labelDescription.text = fidelity.desc
        }
        
        let qrcode : QRCodeUtils = QRCodeUtils()
        if let image = qrcode.generateQRCode(qrString:voucher.uuid! as String, target: self.imageViewQR) {
            //voucher.uuid! as String
            self.imageViewQR.image = image
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenDetalheVoucher)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    @IBAction func buttonClosePressed(sender: UIButton) {
        self.formSheetController?.dismissAnimated(true, completionHandler: nil)
    }
}
