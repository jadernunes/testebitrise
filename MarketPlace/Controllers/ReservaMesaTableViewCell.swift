//
//  ReservaMesaTableViewCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ReservaMesaTableViewCell: UITableViewCell {

    @IBOutlet weak var txtAnswer        : UITextField!
    @IBOutlet weak var lblQuestion      : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
