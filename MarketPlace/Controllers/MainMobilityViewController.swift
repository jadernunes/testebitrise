//
//  MainMobilityViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 09/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class MainMobilityViewController: UIViewController, UITabBarDelegate, UITabBarControllerDelegate {

    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
