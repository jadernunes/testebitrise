//
//  ListHomeSearchBarExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension ListHomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        tbView.reloadData()
        self.showTableView()
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        tbView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        timeSearch = 0.0
        canRequest = true
        lastText = searchText
        self.verifyRequest(lastText)
        
        if searchText.characters.count < 3 {
            noResults = false
            self.listItensSearched.removeAll()
            self.tbView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.stopSearch()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.stopSearch()
        self.hideTableView()
    }
}
