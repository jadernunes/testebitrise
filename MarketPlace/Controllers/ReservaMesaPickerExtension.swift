//
//  ReservaMesaPickerExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension ReservaMesaViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currentPickerDataSouce.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currentPickerDataSouce[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let textfield = self.currentTextField {
            textfield.text = currentPickerDataSouce[row]
        }
    }
}
