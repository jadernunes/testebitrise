//
//  AddressTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/14/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelTitle.font = LayoutManager.primaryFontWithSize(20)
        self.labelTitle.textColor = LayoutManager.lightGreyTracking
        self.labelAddress.font = LayoutManager.primaryFontWithSize(19)
        self.labelAddress.textColor = LayoutManager.darkGreyTracking
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
