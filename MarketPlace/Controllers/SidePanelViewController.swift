//
//  SidePanelViewController.swift
//  MarketPlace
//
//  Created by Luciano on 8/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class SidePanelViewController: JASidePanelController {

    var rightButtonsMenu: [UIBarButtonItem]?
    
    static let sharedInstance = SidePanelViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.leftPanel = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVC")
        self.centerPanel = self.storyboard?.instantiateViewControllerWithIdentifier("NewHomeVC")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
