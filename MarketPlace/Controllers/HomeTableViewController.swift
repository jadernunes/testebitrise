//
//  HomeTableViewController.swift
//  Shopping Total
//
//  Created by 4all on 6/2/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import JGProgressHUD

class HomeTableViewController: UIViewController,CLLocationManagerDelegate, ScannerQrCodeProtocol,UISearchBarDelegate,UISearchControllerDelegate,UIWebViewDelegate {
    
    static let sharedInstance = HomeTableViewController()
    @IBOutlet var tbView: SectionsManager!
    @IBOutlet var buttonMenu: UIBarButtonItem!
    @IBOutlet weak var searchBarCustom: UISearchBar?
    
    let loading = JGProgressHUD(style: .Dark)
    var pullRefresh: UIRefreshControl!
    let locationManager = CLLocationManager()
    var arrayQrDecoded = [String]()
    var searchActivate      = false
    var targetURLString     = NSString()
    var newsViewController  :NewsNotificationViewController!
    var formSheet: MZFormSheetController!
    var localNews: NewsNotification?
    var indexPathsGrid = [NSIndexPath]()
    
    static var rootVc: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        
        self.navigationController?.navigationBar.translucent = false
        
            self.locationManager.delegate = self
            self.locationManager.requestAlwaysAuthorization()
            _ = createGeofencingAreas(self.locationManager,vc: self)
        
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        tbView.addSubview(pullRefresh)
        
        /* --- adaptação para quando é passada outra home --- */
        if self.targetURLString.length <= 0 {
            navigationController?.navigationBar.tintColor = UIColor.whiteColor()
            self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
            self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
            
            buttonMenu.image = Ionicons.Navicon.image(40, color: UIColor.whiteColor())
            buttonMenu.imageInsets = UIEdgeInsetsMake(0, 5, 0, 5)
            buttonMenu.title = ""
            
            self.navigationController?.navigationBar.translucent = true
            let imgView = UIImageView(frame: CGRectMake(0, 0, 100, 42))
            imgView.image = UIImage(named: "logo_total")
            imgView.contentMode = .ScaleAspectFit
            self.navigationItem.titleView = imgView
        }
        
        self.tbView.rootViewController  = self
        
        //if is not the first loading, executes the following code in background and show what is stored already
        // not blocking the screen with a loading indicator
        if GroupEntityManager.getAllGroups().count > 0 {
            self.loadSectionsFromAPI()
        }else{
            self.startLoad()
        }
        
        self.loadUnitiesAndGroups {
            self.loadSectionsFromAPI()
            self.stopLoad()
        }
        
        self.addObserverToCheckOrdersStatus()
        HomeTableViewController.rootVc = self
        
        //Clear all data
        self.tbView.setTableViewSorted([AnyObject]())
        self.tbView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        CartCustomManager.sharedInstance.showCart(self)
        Util.adjustInsetsFromCart(tbView)
        Util.adjustTableViewInsetsFromTarget(self)
        
        PageManager.sharedInstance.getNewNotifications()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenDiversaoEventos)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom methods
    
    func startLoad(){
        self.loading.showInView(self.navigationController!.view)
        self.view.sendSubviewToBack(self.tbView)
    }
    
    func stopLoad(){
        self.loading.dismiss()
    }
    
    func stopSearch() {
        self.searchBarCustom?.text = ""
        tbView.stopSearch()
        self.stopLoad()
    }
    
    //MARK: - SearchBar delegate
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActivate = true
        tbView.reloadData()
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        tbView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        tbView.searchBar(searchBar, textDidChange: searchText)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        tbView.stopSearch()
    }
    
    //MARK: Methods
    func insertOrdersCellManually(arrayOfSections : NSMutableArray) -> NSArray {
        let section = ["position":-1,
                       "sectionType":"orders"]
        
        arrayOfSections.addObject(section)
        
        return NSArray(array: arrayOfSections)
    }
    
    func refresh() {
        // Code to refresh table view
        self.loadSectionsFromAPI()
    }
    
    func loadUnitiesAndGroups(completion: () -> ()) {
        
        let apiGroups = ApiServices()
        
        //Before loading the sections, make sure unities and groups were loaded from server
        apiGroups.failureCase = { (msg) in
            completion()
        }
        
        apiGroups.successCase = { (obj) in
            
            if let grupos = obj {
                if let listaGrupos = grupos["groups"] as? NSArray {
                    if listaGrupos.count > 0 {
                        Util.print("Data: \(listaGrupos)")
                        GroupEntityManager.importFromArray(listaGrupos)
                    }
                }
            }
            
            completion()
        }
        
        apiGroups.listGroups(nil)
        
    }
    
    func loadSectionsFromAPI()
    {
        
        //Before request to server, try to load locally
        self.tbView.loadArrayLocally()
        self.tbView.reloadData()
        
        let api = ApiServices()
        api.successCase = {(obj) in
            if var arrContent = obj as? NSArray {
                self.pullRefresh?.endRefreshing()
                arrContent = self.insertOrdersCellManually(arrContent.mutableCopy() as! NSMutableArray)
                self.tbView.setTableViewSorted(arrContent)
                self.tbView.reloadData()
                self.stopLoad()
            }
        }
        
        api.failureCase = {(msg) in
            //TODO: Load from local json file
            
            self.stopLoad()
            self.tbView.loadArrayLocally()
            self.tbView.reloadData()
            self.pullRefresh?.endRefreshing()
        }
        
        
        api.listSections(targetURLString as String)
    }
    
    private func addObserverToCheckOrdersStatus(){
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(HomeTableViewController.reloadHeightOrders),
            name: NOTIF_HOME_CART_OBSERVER,
            object: nil)
    }
    
    //In case of there's none active order, hide the cell
    @objc private func reloadHeightOrders(){
        let arrayOrders = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
        
        if arrayOrders.count == 0 {
            tbView.reloadData()
        }
    }
    
    //Scanner Delegate
    func didScanValidCode(value: String) {
        
        var storyboard = UIStoryboard(name:"Main", bundle: nil)
        arrayQrDecoded = []
        var qrToBeDecoded = value
        if qrToBeDecoded.characters.count > 3 {
            qrToBeDecoded.removeAtIndex(qrToBeDecoded.startIndex.advancedBy(3))
            
            if let qrDecoded = qrToBeDecoded.fromBase64() {
                arrayQrDecoded = qrDecoded.componentsSeparatedByString("_")
            }
            
            if arrayQrDecoded.count > 0 &&  !arrayQrDecoded.contains("PAY") && arrayQrDecoded[0] == ID_MARKETPLACE {
                
                if arrayQrDecoded.contains("GRP") {
                    didScanGroup(arrayQrDecoded)
                }else if arrayQrDecoded.contains("UNT") {
                    didScanUnity(arrayQrDecoded)
                }
                else if arrayQrDecoded.contains("PRK") {
                    didScanParking(arrayQrDecoded)
                } else if arrayQrDecoded.contains("TOT") {
                    didScanTotem(arrayQrDecoded)
                } else {
                    Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
                }
            } else if arrayQrDecoded.contains("PAY") {
                
                let destVc = storyboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
                
                if arrayQrDecoded.count == 8 {
                    destVc.idObjectPayment = String(arrayQrDecoded[7]) //Id do objeto a ser pago
                    destVc.typePayment = String(arrayQrDecoded[6]) //tipo de pagamento Ex.: CPN é Campaign
                    destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                    destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                    destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                    destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
                } else if arrayQrDecoded.count == 7 {
                    destVc.typePayment = String(arrayQrDecoded[6]) //tipo de pagamento Ex.: CPN é Campaign
                    destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                    destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                    destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                    destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
                } else if arrayQrDecoded.count == 6 {
                    destVc.merchantCNPJ = String(arrayQrDecoded[5]) //merchantCNPJ
                    destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                    destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                    destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
                } else if arrayQrDecoded.count == 5 {
                    destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                    destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                    destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
                }
                
                destVc.previousVC = self
                self.presentViewController(destVc, animated: true, completion: nil)
                
            } else if arrayQrDecoded.contains("CNC") {//Cancelamento de um pedido
                
                let destVc = storyboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
                
                destVc.strMerchantName = String(arrayQrDecoded[4]) //name EC
                destVc.strAmount = String(arrayQrDecoded[3])//price * 100
                destVc.strTransactionId = String(arrayQrDecoded[2]) // Transaction ID
                destVc.isCancelPayment = true
                
                destVc.previousVC = self
                self.presentViewController(destVc, animated: true, completion: nil)
            }
            else if arrayQrDecoded.contains("CHK") {
                didScanChecks(arrayQrDecoded)
            } else {
                //validate is ticket
                if(Int(value) > 0 && value.characters.count >= 8) {
                    storyboard = UIStoryboard(name:"Parking Storyboard", bundle: nil)
                    let viewController = storyboard.instantiateViewControllerWithIdentifier("parkingVC") as! PaymentViewController
                    viewController.valueFromHomeQRReader = value
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else {
                    Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
                }
            }
        } else {
            Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
        }
    }
    
    func didScanInvalidCode() {
        Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
    }
    
    func didScanGroup(arrayContent : [String]) {
        
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        
        //FOODCOURT
        if arrayContent[2] == "FOODCOURT" {
            
            if arrayContent.count == 4 {
                SessionManager.sharedInstance.tableEnabledByQr = arrayContent[3]
                SessionManager.sharedInstance.tableChosen = true
                SessionManager.sharedInstance.deliveryChosen = false
                SessionManager.sharedInstance.takeawayChosen = false
                SessionManager.sharedInstance.idAddressSelected = 0
                SessionManager.sharedInstance.deliveryFee = 0.0
            }
            
            listVc.group = GroupEntityManager.getGroupCustom("idGroupTreeType = 20")
            listVc.subType = .Foodcourt
            
            //GROUPS
        }else{
            listVc.group = GroupEntityManager.getGroupCustom("id = \(arrayContent[2])")
            listVc.title = "ITENS"
        }
        
        (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(listVc, animated: true)
    }
    
    func didScanParking(arrayContent : [String]) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC") as! OndePareiViewController
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if arrayContent.count > 3 {
            let dict = ["building":"\(arrayContent[2])",
                        "floor":"\(arrayContent[3])",
                        "region":"\(arrayContent[4])"]
            
            defaults.setObject(dict, forKey: "dicWherePark")
            
        }
        
        defaults.synchronize()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didScanTotem(arrayContent : [String]) {
        
        let vc = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("TotemVC") as! TotemViewController
        vc.latitude = Double(arrayContent[2])!
        vc.longitude = Double(arrayContent[3])!
        vc.showARViewController()
    }
    
    func didScanChecks(arrayContent : [String]){
        let vc = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("CheckViewController") as! CheckViewController
        vc.placeLabel = arrayContent[1]
        vc.idUnity = Int(arrayContent[2])!
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func didScanUnity(arrayContent : [String]) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        if let unity = UnityPersistence.getUnityByID(arrayContent[2]) {
            detailsVc.title = unity.name
            detailsVc.idObjectReceived = unity.id
            detailsVc.typeObjet = TypeObjectDetail.Store
            detailsVc.hidesBottomBarWhenPushed = true
            SessionManager.sharedInstance.searchOrQrEnabled = true
            if arrayContent.contains("TBL") {
                let activeCart = OrderEntityManager.sharedInstance.hasActiveCart()
                if (activeCart.active == false || activeCart.cart.orderItems.count == 0) {
                    if unity.orderShortDeliveryEnabled == true {
                        SessionManager.sharedInstance.tableEnabledByQr = arrayContent[4]
                        SessionManager.sharedInstance.tableChosen = true
                    }
                    else {
                        Util.showAlert(self, title: kAlertTitle, message: kMessageShortDeliveryUnavailable)
                    }
                }
                else {
                    if activeCart.cart.idUnity == unity.id {
                        if unity.orderShortDeliveryEnabled == true {
                            SessionManager.sharedInstance.tableEnabledByQr = arrayContent[4]
                            SessionManager.sharedInstance.tableChosen = true
                        }
                        else {
                            Util.showAlert(self, title: kAlertTitle, message: kMessageShortDeliveryUnavailable)
                        }
                    }
                    else {
                        Util.showAlert(self, title: kAlertTitle, message: kMessageOtherUnityActive)
                    }
                }
            }
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
        else {
            Util.showAlert(self, title: kAlertTitle, message: kMessageUnityNotFound)
        }
    }

    //MARK: - IBActions
    @IBAction func openScanner(sender: UIBarButtonItem) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
        (vc.viewControllers[0] as! ScannerViewController).delegate = self
        (vc.viewControllers[0] as! ScannerViewController).titleString = kQRScannerTitle
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func openMenu(sender: UIBarButtonItem) {
        self.stopSearch()
        self.sidePanelController.showLeftPanelAnimated(true)
    }    
}
