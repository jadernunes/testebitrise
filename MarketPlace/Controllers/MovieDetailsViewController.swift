//
//  CoordinatorBasedController.swift
//  MarketPlace
//
//  Created by Luciano on 7/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class MovieDetailsViewController: UITableViewController{
   
    @IBOutlet weak var tableViewTickets     : UITableView!
    @IBOutlet weak var labelDate            : UILabel!
    @IBOutlet weak var buttonNext           : UIButton!
    @IBOutlet weak var buttonPrevious       : UIButton!
    @IBOutlet weak var ytPlayer             : YTPlayerView!
    @IBOutlet weak var labelTitle           : UILabel!
    @IBOutlet weak var labelClassification  : UILabel!
    @IBOutlet weak var labelClassDesc       : UILabel!
    @IBOutlet weak var labelDuration        : UILabel!
    @IBOutlet weak var labelSessionsTitle   : UILabel!
    @IBOutlet weak var labelBuyTicketsTitle : UILabel!
    @IBOutlet weak var imagePoster          : UIImageView!
    @IBOutlet weak var labelTitleSinopsis : UILabel!
    @IBOutlet weak var textSinopsis       : UITextView!
    @IBOutlet weak var labelTitleCast     : UILabel!
    @IBOutlet weak var labelCast          : UITextView!
    @IBOutlet weak var labelTitleDirector : UILabel!
    @IBOutlet weak var labelDirector      : UITextView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLabelCast: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLabelDirector: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewCellFooter: NSLayoutConstraint!
    
    @IBOutlet weak var viewSeparator: UIView!
    
    var movie       : Movie!
    var currentDate = NSDate()
    var arrSessions = NSMutableArray()
    var ticketsSource = TicketsSource()
    var newHeightSinopsis: CGFloat!
    var newHeightCast: CGFloat!
    var newHeightDirector: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        initialConfiguration()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMovies + " - " + movie.title)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if SessionManager.sharedInstance.cartActive == true {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 64, right: 0)
        }
        self.constraintHeight.constant = newHeightSinopsis
        self.constraintHeightLabelCast.constant = newHeightCast
        self.constraintHeightLabelDirector.constant = newHeightDirector
        self.constraintHeightViewCellFooter.constant = newHeightSinopsis + newHeightCast + newHeightDirector
    }
    
    //MARK: - METHODS
    func initialConfiguration(){
        
        if self.movie != nil {
            if self.movie.sinopsis != nil {
                newHeightSinopsis = CGFloat(self.movie.sinopsis!.characters.count)/2.0
            }
        }
        
        newHeightCast = (CGFloat(self.movie.cast!.characters.count)/2.0) + 20
        newHeightDirector = (CGFloat(self.movie.director!.characters.count)/2.0) + 20
        
        self.tableView.backgroundColor  = LayoutManager.backgroundFirstColor
        
        labelDate.font      = LayoutManager.primaryFontWithSize(16)
        labelDate.textColor = UIColor.whiteColor()
        
        buttonNext.setImage(Ionicons.IosArrowRight.image(buttonNext.frame.height), forState: .Normal)
        buttonPrevious.setImage(Ionicons.IosArrowLeft.image(buttonNext.frame.height), forState: .Normal)
        
        buttonNext.tintColor     = LayoutManager.labelContrastColor
        buttonPrevious.tintColor = UIColor.whiteColor()
        
        // -- HEADER CELL -- //
        labelTitle.font      = LayoutManager.primaryFontWithSize(22)
        labelTitle.textColor = UIColor.whiteColor()
        labelDuration.font      = LayoutManager.primaryFontWithSize(16)
        labelDuration.textColor = UIColor.lightGrayColor()
        
        labelClassDesc.font      = LayoutManager.primaryFontWithSize(16)
        labelClassDesc.textColor = UIColor.lightGrayColor()
        
        labelClassification.font       = LayoutManager.primaryFontWithSize(27)
        labelClassification.textColor = UIColor.whiteColor()
        
        labelClassification.layer.cornerRadius = 6
        labelClassification.clipsToBounds = true
        imagePoster.layer.cornerRadius = 6
        imagePoster.clipsToBounds = true

        viewSeparator.tintColor = SessionManager.sharedInstance.cardColor
        labelSessionsTitle.font = LayoutManager.primaryFontWithSize(16)
        labelSessionsTitle.textColor = UIColor.lightGrayColor()

        labelBuyTicketsTitle.font = LayoutManager.primaryFontWithSize(16)
        labelBuyTicketsTitle.textColor = UIColor.lightGrayColor()

        labelTitle.text           = movie.title
        let duration = movie.duration!.componentsSeparatedByString(":")
        let durationFormatted = duration[0] + ":" + duration[1]
        labelDuration.text        = durationFormatted
        
        if movie.ageClassification > 0 {
            
            labelClassification.text  = "\(movie.ageClassification)"
            
            if(movie.ageClassification == 10) {
                labelClassification.backgroundColor = UIColor().hexStringToUIColor("0f7dc2")
            } else if(movie.ageClassification == 12) {
                labelClassification.backgroundColor = UIColor().hexStringToUIColor("f8c411")
            } else if(movie.ageClassification == 14) {
                labelClassification.backgroundColor = UIColor().hexStringToUIColor("e67824")
            } else if(movie.ageClassification == 16) {
                labelClassification.backgroundColor = UIColor().hexStringToUIColor("db2827")
            } else if(movie.ageClassification == 18) {
                labelClassification.backgroundColor = UIColor().hexStringToUIColor("1d1815")
            }
        }else{
            labelClassification.text  = "L"
            labelClassification.backgroundColor = UIColor().hexStringToUIColor("0c9447")
        }
        
        //get the youtube video ID
        if movie.youtube != nil {
            var videoUrl = movie.youtube
            videoUrl = videoUrl?.stringByReplacingOccurrencesOfString("v=", withString: "")
            let index2 = videoUrl!.rangeOfString("?", options: .BackwardsSearch)?.startIndex
            if index2 != nil {
                let videoId = videoUrl!.substringFromIndex(index2!).stringByReplacingOccurrencesOfString("?", withString: "")
                ytPlayer.loadWithVideoId(videoId)
            }
        }
    
        var url = ""
        //Check if it should download thumb or externalThumb
        if movie.thumb != nil{
            if movie.thumb!.characters.count > 0{
                url = movie.thumb!
            }
        }
        
        imagePoster.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imagePoster.contentMode      = .ScaleToFill
            }
        }
        
        arrSessions = movie.getAvailableSessions()
        showCurrentDate()
        tableViewTickets.reloadData()
        
        // -- FOOTER CELL -- //
        labelTitleCast.font             = LayoutManager.primaryFontWithSize(16)
        labelTitleCast.textColor        = UIColor.lightGrayColor()
        labelTitleSinopsis.font         = LayoutManager.primaryFontWithSize(16)
        labelTitleSinopsis.textColor    = UIColor.lightGrayColor()
        labelCast.font                  = LayoutManager.primaryFontWithSize(16)
        labelCast.textColor             = UIColor.whiteColor()
        textSinopsis.font               = LayoutManager.primaryFontWithSize(14)
        textSinopsis.tintColor          = UIColor.whiteColor()
        textSinopsis.textColor          = UIColor.whiteColor()
        textSinopsis.backgroundColor    = UIColor.clearColor()
        labelTitleDirector.font         = LayoutManager.primaryFontWithSize(16)
        labelTitleDirector.textColor    = UIColor.lightGrayColor()
        labelDirector.font              = LayoutManager.primaryFontWithSize(16)
        labelDirector.textColor         = UIColor.whiteColor()

        ticketsSource.movie = self.movie
        ticketsSource.superViewController = self
        tableViewTickets.dataSource = ticketsSource
        tableViewTickets.delegate = ticketsSource
        tableViewTickets.reloadData()
        
        self.navigationItem.title = movie.title
        self.textSinopsis.text = self.movie.sinopsis
        self.labelCast.text = self.movie.cast
        self.labelDirector.text = self.movie.director
        
    }
    
    func showCurrentDate(){
        
        labelDate.text = "hoje, \(NSDate.day()) de \(Util.getStringMonth(String(NSDate.month()))) de \(NSDate.year())".uppercaseString
    }
    
        //MARK: - IBActions
    
    @IBAction func previousDay(sender : UIButton) {

        let previous = Util.getFormatedDateString(currentDate, pattern: "ddMMyyyy")
        let current  = Util.getFormatedDateString(NSDate(), pattern: "ddMMyyyy")
        
        //go back if is not the current date
        if previous != current {

        
            let previousDate = NSCalendar.currentCalendar()
                .dateByAddingUnit(
                    .Day,
                    value: -1,
                    toDate: currentDate,
                    options: []
            )
            
            if previousDate != nil {
                
                currentDate = previousDate!
                arrSessions = movie.getAvailableSessions(previousDate!)
                tableView.reloadData()
                showCurrentDate()
            }
        }
    }
    
    @IBAction func nextDay(sender : UIButton) {
        let nextDate = NSCalendar.currentCalendar()
            .dateByAddingUnit(
                .Day,
                value: 1,
                toDate: currentDate,
                options: []
        )
        
        if nextDate != nil {
            currentDate = nextDate!
            arrSessions = movie.getAvailableSessions(nextDate!)
            tableView.reloadData()
            showCurrentDate()
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 390 // Header
        }else if indexPath.row == 1 {
            return CGFloat(60 * arrSessions.count) + 80// Tickets + navigation dos dias
        }else if indexPath.row == 2 {
            return 160 + newHeightSinopsis + newHeightCast + newHeightDirector //Footer
        }
        
        return 0
    }
    
    func textViewDidChange(textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "segueGotoChouseTickets"){
            
            let dicSenderChouse = sender as! NSDictionary
            
            let viewController = segue.destinationViewController as! ChouseTicketsViewController
            viewController.movie = dicSenderChouse.objectForKey("movie") as? Movie
            viewController.movieSession = dicSenderChouse.objectForKey("session") as! MovieSession
            
        }
    }

}
