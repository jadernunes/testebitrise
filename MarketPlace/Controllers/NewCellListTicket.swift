//
//  NewCellListTicket.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 27/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class NewCellListTicket: UITableViewCell {
    
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var imageViewDiscloserIndicator: UIImageView!
    
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewClock: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBase.layer.cornerRadius = 3
        self.viewBase.layer.masksToBounds = true
        self.viewBase.layer.borderWidth = 1.0
        self.viewBase.layer.borderColor = UIColor.lightGray4all().CGColor
        
        self.labelDay.textColor = UIColor.gray4all()
        self.labelMonth.textColor = UIColor.gray4all()
        self.labelTitle.textColor = UIColor.darkGray4all()
        self.labelHour.textColor = UIColor.gray4all().colorWithAlphaComponent(0.5)
        
        self.labelDay.text = ""
        self.labelMonth.text = ""
        self.labelTitle.text = ""
        self.labelHour.text = ""
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
