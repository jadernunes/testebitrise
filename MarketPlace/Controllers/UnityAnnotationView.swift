//
//  UnityAnnotationView.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 31/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import MapKit

class UnityAnnotationView: MKAnnotationView {

    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}
