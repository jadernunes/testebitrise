//
//  ActionBaseCollectionViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ActionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAction: UIImageView!
    @IBOutlet weak var labelTitleAction: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.backgroundColor = UIColor.lightGray4all()
        self.labelTitleAction.textColor = UIColor.darkGray4all()
    }
}
