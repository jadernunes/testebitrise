//
//  Perfil4allViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 26/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class Perfil4allViewController: UIViewController {
    
    @IBOutlet weak var imgTopoBG: UIImageView!
    @IBOutlet weak var tableProfile: UITableView!
    
    let kSectionProfile                         : Int = 0
    let kSectionData                            : Int = 1
    var kTopoIsVisible                          = true
    let myInset                                 : CGFloat = 66
    
    let kCellIdentifier_minhasCredenciais       = "cellCredenciais"
    let kCellIdentifier_minhasFidelidades       = "cellFidelidades"
    let kCellIdentifier_meusCupons              = "cellCupons"
    let kCellIdentifier_logout                  = "cellLogout"
    let kCellIdentifier_Extratos                = "cellExtratos"
    let kCellIdentifier_Signatures              = "cellSignatures"
    let kCellIdentifier_PersonalData            = "cellPersonalData"
    let kCellIdentifier_Cards                   = "cellCards"
    let kCellIdentifier_ProfileFamily           = "cellProfileFamily"
    let kCellIdentifier_Config                  = "cellConfig"
    let kCellIdentifier_Help                    = "cellHelp"
    let kCellIdentifier_About                   = "cellAbout"
    let kCellIdentifier_Logout                  = "cellLogout"
    
    let arrayMenuProfile                        : [Int] = [kCellProfileIdentifier.kPedidos.rawValue,
                                                           kCellProfileIdentifier.kCredenciais.rawValue,
                                                           kCellProfileIdentifier.kFidelidades.rawValue,
                                                           kCellProfileIdentifier.kCupons.rawValue,
                                                           kCellProfileIdentifier.kExtratos.rawValue,
                                                           kCellProfileIdentifier.kSignatures.rawValue,
                                                           kCellProfileIdentifier.kPersonalData.rawValue,
                                                           kCellProfileIdentifier.kCards.rawValue,
                                                           kCellProfileIdentifier.kProfileFamily.rawValue,
                                                           kCellProfileIdentifier.kConfig.rawValue,
                                                           kCellProfileIdentifier.kHelp.rawValue,
                                                           kCellProfileIdentifier.kAbout.rawValue,
                                                           kCellProfileIdentifier.kLogout.rawValue
                                                           ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableProfile.reloadData()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Custom Methods
    
    func createProfileCell(indexPath: NSIndexPath, reuseIdentifier:String) -> UITableViewCell {
        let cell = tableProfile.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuCellTableViewCell
        let menuType : kCellProfileIdentifier = kCellProfileIdentifier(rawValue:arrayMenuProfile[indexPath.row])!
        cell.title.text = menuType.title()
        cell.backgroundColor = UIColor.whiteColor()
        let origImage = UIImage.init(named:(menuType.icon()))
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        cell.icon.image = tintedImage
        cell.icon.tintColor = LayoutManager.green4all
        return cell
    }
    
    func createLogin4allCell(indexPath: NSIndexPath, reuseIdentifier:String) -> UITableViewCell {
        let cell = tableProfile.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuCellTableViewCell
        let menuType : kCellProfileIdentifier = kCellProfileIdentifier.kLogin
        cell.title.text = menuType.title()
        cell.backgroundColor = UIColor.whiteColor()
        let origImage = UIImage.init(named:(menuType.icon()))
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        cell.icon.image = tintedImage
        cell.icon.tintColor = LayoutManager.green4all
        
        return cell
    }
    
    func maskCell(cell: UITableViewCell, margin: Float) {
        cell.layer.mask = visibilityMaskForCell(cell, location: (margin / Float(cell.frame.size.height) ))
        cell.layer.masksToBounds = true
    }
    
    func visibilityMaskForCell(cell: UITableViewCell, location: Float) -> CAGradientLayer {
        let mask = CAGradientLayer()
        mask.frame = cell.bounds
        mask.colors = [UIColor(white: 1, alpha: 0).CGColor, UIColor(white: 1, alpha: 1).CGColor]
        mask.locations = [NSNumber(float: location), NSNumber(float: location)]
        return mask;
    }
    
    //MARK: - Actions 4all account
    
    private func showMenuAccount4all(profileOption:ProfileOption){
        if let account = Lib4all.sharedInstance() {
            if account.hasUserLogged() {
                account.openAccountScreen(profileOption, inViewController: self)
            }
        }
    }
    
    private  func profile4all() {
        if Lib4all.sharedInstance().hasUserLogged() {
            Lib4all.sharedInstance().showProfileController(self)
        }
    }
    
    private  func logout4all() {
        
        if let account = Lib4all.sharedInstance() {
            if account.hasUserLogged() {
                
                let loader = LoadingViewController();
                loader.startLoading(self, title: "Aguarde...")
                
                dispatch_async(dispatch_get_main_queue(), {
                    account.callLogout({ (success) in
                        loader.finishLoading({
                            self.tableProfile.reloadData()
                            self.tableProfile.setContentOffset(CGPointZero, animated: true)
                        })
                    })
                })
            }
        }
    }
    
    private func login4all() {
        Lib4all.sharedInstance().callLogin(self, requireFullName: true, requireCpfOrCnpj: false) { (phoneNumber, email, sessionToken) in
            SessionManager.sharedInstance.registerDevice()
            self.tableProfile.reloadData()
            self.tableProfile.setContentOffset(CGPointZero, animated: true)
        }
    }
    
}

//MARK: - UITableViewDataSource

extension Perfil4allViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == kSectionProfile {
            return nil
        }
        else {
            let cell = self.tableProfile.dequeueReusableCellWithIdentifier("cellMinhaConta")! as UITableViewCell
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == kSectionProfile {
            return 0
        }
        else {
            return 46
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == kSectionProfile {
            kTopoIsVisible = true
            return 210
        } else if indexPath.section == kSectionData { /* --- Se há usuario logado --- */
            if Lib4all.sharedInstance().hasUserLogged() {
                return 60
            } else {
                return 60
            }
        } else {
            return 10
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == kSectionData {
            if !Lib4all.sharedInstance().hasUserLogged() {
                self.login4all()
            } else {
                switch indexPath.row {
                case kCellProfileIdentifier.kPedidos.rawValue:
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OrdersTableViewController") as! OrdersTableViewController
                    viewController.hidesBottomBarWhenPushed = true
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    break
                case kCellProfileIdentifier.kCredenciais.rawValue://vouchers
//                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("VouchersViewController") as! VouchersViewController
//                    viewController.hidesBottomBarWhenPushed = true
//                    self.navigationController?.setNavigationBarHidden(false, animated: true)
//                    self.navigationController?.pushViewController(viewController, animated: true)
                    break
                case kCellProfileIdentifier.kFidelidades.rawValue:
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListFidelityTableViewController") as! ListFidelityTableViewController
                    viewController.hidesBottomBarWhenPushed = true
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    break
                case kCellProfileIdentifier.kCupons.rawValue:
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
                    viewController.hidesBottomBarWhenPushed = true
                    viewController.typeShow = TypeShowListCoupons.meCoupons
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    break
                case kCellProfileIdentifier.kExtratos.rawValue:
                    self.showMenuAccount4all(ProfileOption.Receipt)
                    break
                case kCellProfileIdentifier.kSignatures.rawValue:
                    self.showMenuAccount4all(ProfileOption.Subscriptions)
                    break
                case kCellProfileIdentifier.kPersonalData.rawValue:
                    self.showMenuAccount4all(ProfileOption.UserData)
                    break
                case kCellProfileIdentifier.kCards.rawValue:
                    self.showMenuAccount4all(ProfileOption.UserCards)
                    break
                case kCellProfileIdentifier.kProfileFamily.rawValue:
                    self.showMenuAccount4all(ProfileOption.Family)
                    break
                case kCellProfileIdentifier.kConfig.rawValue:
                    self.showMenuAccount4all(ProfileOption.Settings)
                    break
                case kCellProfileIdentifier.kHelp.rawValue:
                    self.showMenuAccount4all(ProfileOption.Help)
                    break
                case kCellProfileIdentifier.kAbout.rawValue:
                    self.showMenuAccount4all(ProfileOption.About)
                    break
                case kCellProfileIdentifier.kLogout.rawValue:
                    self.logout4all()
                    break
                default:
                    break
                }
            }
        }
    }
}

//MARK: - UITableViewDataSource

extension Perfil4allViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == kSectionProfile {
            return 1
        }
        else {
            if Lib4all.sharedInstance().hasUserLogged() {
                return arrayMenuProfile.count
            }
            else {
                return 1
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == kSectionProfile { /* --- Cell do topo, em uma secao separada --- */
            let cell = self.tableProfile.dequeueReusableCellWithIdentifier("cellTopo")! as! PerfilTopoTableViewCell
            cell.refreshData()
            return cell
        } else if !Lib4all.sharedInstance().hasUserLogged() { /* --- cell login conta 4all --- */
            return self.createLogin4allCell(indexPath, reuseIdentifier: (kCellProfileIdentifier.kMenuGeral.simpleDescription()))
        } else if Lib4all.sharedInstance().hasUserLogged() { /* --- usuario logado, mostra "minhas celulas" --- */
            return self.createProfileCell(indexPath, reuseIdentifier: (kCellProfileIdentifier.kMenuGeral.simpleDescription()))
        } else {
            return UITableViewCell()
        }
    }
}

//MARK: - UIScrollViewDelegate
extension Perfil4allViewController: UIScrollViewDelegate {
    
    func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        self.tableProfile.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        return true
    }
    
    /* --- esconder celulas acima da section --- */
    func scrollViewDidScroll(scrollView: UIScrollView) {
        for cell in self.tableProfile.visibleCells {
            if cell.reuseIdentifier != "cellTopo" {
                let hiddenFrameHeight = scrollView.contentOffset.y + self.myInset + (cell.frame.size.height*0.5) - cell.frame.origin.y
                if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
                    maskCell(cell, margin: Float(hiddenFrameHeight))
                }
            }
        }
    }
}
