//
//  MenuCellTableViewCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 28/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class MenuCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.title.font = LayoutManager.primaryFontWithSize(14, bold: false)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
