//
//  PushTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PushTableViewCell: UITableViewCell {

    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStore: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.whiteColor()
        labelDate.font = LayoutManager.primaryFontWithSize(15)
        labelDescription.font = LayoutManager.primaryFontWithSize(16)
        labelStore.font = LayoutManager.primaryFontWithSize(16)
        labelDate.textColor = LayoutManager.backgroundDark
        labelDescription.textColor = LayoutManager.backgroundDark
        labelStore.textColor = LayoutManager.backgroundDark
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configureCell(push: Push) {
        
        labelDescription.textColor = LayoutManager.grayGeneralText
        labelDescription.text = push.body!
        labelDate.text = push.firstCustomField == nil ? "" : push.firstCustomField!
        labelDate.textColor = LayoutManager.grayGeneralText
        
        switch push.type {
        case PushesType.BeautyStatus.rawValue:
            labelStore.textColor = LayoutManager.purple4Beauty
            labelStore.text = "4Beauty"
            imageLogo.image = UIImage(named: "beauty_icon")
            break
        case PushesType.Generic.rawValue:
            labelStore.textColor = LayoutManager.green4all
            imageLogo.image = UIImage(named: "4all")
            labelStore.text = "Geral"
            break
        case PushesType.LineStatus.rawValue:
            labelStore.textColor = LayoutManager.red4Food
            labelStore.text = "4Food"
            imageLogo.image = UIImage(named: "food_icon")
            break
        case PushesType.OrderStatus.rawValue:
            labelStore.textColor = LayoutManager.red4Food
            labelStore.text = "4Food"
            imageLogo.image = UIImage(named: "food_icon")
            break
        case PushesType.PaymentStatus.rawValue:
            labelStore.textColor = LayoutManager.green4all
            imageLogo.image = UIImage(named: "4all")
            labelStore.text = kMessageSuccessPayment
            break
        case PushesType.PromoStatus.rawValue:
            labelStore.textColor = LayoutManager.green4all
            imageLogo.image = UIImage(named: "4all")
            labelStore.text = kPromotion
            break
        case PushesType.MakePayment.rawValue:
            labelStore.textColor = LayoutManager.green4all
            imageLogo.image = UIImage(named: "4all")
            labelStore.text = "4Pay"
            break
        default: break
        }
        
    }
   
}
