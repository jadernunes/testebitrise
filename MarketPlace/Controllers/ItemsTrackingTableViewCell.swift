//
//  ItemsTrackingTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/18/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ItemsTrackingTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelPrice.font = LayoutManager.primaryFontWithSize(20)
        self.labelPrice.textColor = LayoutManager.greyTitleItemCart
        
        self.labelQuantity.font = LayoutManager.primaryFontWithSize(20)
        self.labelQuantity.textColor = LayoutManager.greyTitleItemCart
        
        self.labelName.font = LayoutManager.primaryFontWithSize(20)
        self.labelName.textColor = LayoutManager.greyTitleItemCart
        
        self.labelDescription.font = LayoutManager.primaryFontWithSize(15)
        self.labelDescription.textColor = LayoutManager.greyDescItemCart
        
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func populateCell(item: OrderItem, description: String, rootVc: UIViewController, unity: Unity) {
        self.labelPrice.text = Util.formatCurrency(item.total/100)
        self.labelDescription.text = description
        self.labelName.text = item.unityItem?.title
        self.labelQuantity.text = String(Int(item.quantity))
    }

}
