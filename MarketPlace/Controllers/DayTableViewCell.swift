//
//  DayCollectionViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 8/16/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class DayTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var numericDay: UILabel!
    @IBOutlet weak var descriptionDay: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}