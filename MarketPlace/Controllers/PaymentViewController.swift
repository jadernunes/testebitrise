//
//  PaymentViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 8/25/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController,UITextFieldDelegate, ScannerQrCodeProtocol, LoginDelegate {
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var ticketNumberLabel: UILabel!
    @IBOutlet weak var ticketNumberTextField: UITextField!
    @IBOutlet weak var containerPayment: UIView!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    @IBOutlet weak var valueNumberLabel: UILabel!
    @IBOutlet weak var valueTextLabel: UILabel!
   
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var component               : ComponentViewController!
    var receivedValue           : NSString = ""
    var ticketPrice             : Double = 0.0
    var valueFromHomeQRReader   : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        configurePaymentComponent()
        self.hidesBottomBarWhenPushed = true
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        scanButton.layer.cornerRadius = 6
        scanButton.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        scanButton.layer.borderWidth  = 1.0
        scanButton.titleLabel?.font   = LayoutManager.primaryFontWithSize(19)
        scanButton.setTitleColor(LayoutManager.labelContrastColor, forState: .Normal)
        scanButton.setIconWithCoordinates(UIImage(named: "iconQr"))
        
        ticketNumberLabel.font = LayoutManager.primaryFontWithSize(16)
        ticketNumberLabel.textColor = UIColor().hexStringToUIColor("919194")
        
        ticketNumberTextField.delegate = self
        ticketNumberTextField.addTarget(self, action: #selector(PaymentViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        ticketNumberTextField.attributedPlaceholder = NSAttributedString(string:"0000000000-00",
                                                               attributes:[NSForegroundColorAttributeName: UIColor().hexStringToUIColor("919194")])
        ticketNumberTextField.textColor = UIColor().hexStringToUIColor("919194")
        ticketNumberTextField.font = LayoutManager.primaryFontWithSize(19)
        ticketNumberTextField.layer.cornerRadius = 6
        ticketNumberTextField.layer.borderColor = UIColor().hexStringToUIColor("919194").CGColor
        ticketNumberTextField.layer.borderWidth = CGFloat(1.0)
        ticketNumberTextField.tintColor = UIColor().hexStringToUIColor("919194")
    
        valueTextLabel.textColor = LayoutManager.labelContrastColor
        valueTextLabel.font = LayoutManager.primaryFontWithSize(19)
        
        valueNumberLabel.textColor = SessionManager.sharedInstance.cardColor
        valueNumberLabel.font = LayoutManager.primaryFontWithSize(54)
        
        activityIndicator.color = SessionManager.sharedInstance.cardColor
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PaymentViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
       // containerPayment.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenPayCarPlace)
        CartCustomManager.sharedInstance.hideCart()
        self.title = kPaymentParkTitle
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        hideLabels()
        if ticketNumberTextField.text?.characters.count == 12 {
            valueTextLabel.hidden = false
            valueNumberLabel.hidden = false
            containerPayment.hidden = false
        }
        
        if valueFromHomeQRReader != nil {
            if valueFromHomeQRReader.characters.count > 0 {
                self.didScanValidCode(valueFromHomeQRReader)
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.attributedPlaceholder = NSAttributedString(string:"0000000000-00",attributes:[NSForegroundColorAttributeName: UIColor().hexStringToUIColor("919194")])
        if ticketNumberTextField.text?.characters.count == 12 {
            receivedValue = ticketNumberTextField.text!
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.placeholder = ""
        if textField.text?.characters.count == 12 {
            textField.text = ""
            hideLabels()
            receivedValue = ""
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
            if textField.text?.characters.count > 11 {
                
                textField.resignFirstResponder()
                return false
            }
            else {
                return true
            }
    }
    
    func textFieldDidChange(textField: UITextField) {
        if textField.text?.characters.count == 12 {
                textField.resignFirstResponder()
                showLabels()
                checkValue()
            }
    }
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func callScan(sender: UIButton) {
            ticketNumberTextField.text = ""
            receivedValue = ""
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
        (vc.viewControllers[0] as! ScannerViewController).delegate = self
        (vc.viewControllers[0] as! ScannerViewController).titleString = kPaymentParkTitle
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func didScanInvalidCode() {
        Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
    }

    func didScanValidCode(value: String) {
        if (value.characters.count == 12) {
            receivedValue = value
            ticketNumberTextField.text = value
            showLabels()
            checkValue()
        }
        else {
            let alert = UIAlertController(title: kAlertTitle, message: kMessageInvalidTicket, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: kAlertButtonOk, style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func showLabels() {
        valueTextLabel.hidden = false
    }
    
    func hideLabels() {
        activityIndicator.hidden = true
        containerPayment.hidden = true
        valueTextLabel.hidden = true
        valueNumberLabel.hidden = true
    }
    
    private func showFailMessage(){
        
    }
    
    func checkValue() {
        
        let api = ApiServices()
        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        api.successCase = {(obj) in
            if let tckPrice = obj as? NSDictionary
            {
                self.activityIndicator.stopAnimating()
                self.ticketPrice = Double(tckPrice.objectForKey("price") as! Int)/100
                self.updateViews()
            }
        }
        
        api.failureCase = {(msg) in
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidden = true
            self.hideLabels()
            self.ticketNumberTextField.text = ""
            let alert = UIAlertController(title: kAlertTitle, message: kMessageFailTicketParkValue, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: kAlertButtonOk, style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

        api.valueForTicket((receivedValue as String),UUID: SystemInfo.getUserUuid())
    }
    
    func updateViews() {
        activityIndicator.hidden = true
        containerPayment.hidden = false
        valueNumberLabel.hidden = false
        valueNumberLabel.text = "R$ " + String(format: "%.02f",ticketPrice)
    }
    
    func configurePaymentComponent() {
        
        dispatch_async(dispatch_get_main_queue()) {
            self.component = ComponentViewController()
            self.component.buttonTitleWhenNotLogged = kEnter
            self.component.buttonTitleWhenLogged    = kPay
            self.component.requireFullName = true
            self.component.delegate = self
            self.component.view.frame = self.containerPayment.bounds
            self.component.view.backgroundColor = LayoutManager.backgroundFirstColor
            self.containerPayment.addSubview(self.component.view)
            self.addChildViewController(self.component)
            self.component.didMoveToParentViewController(self)
        }
    }
    
    func dismissViewController(alert: UIAlertAction!) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        SessionManager.sharedInstance.registerDevice()
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        
        self.containerPayment.userInteractionEnabled = false
        let api = ApiServices()
        let loadingVc = LoadingViewController()
        api.successCase = {(obj) in
            if let tckPaid = obj as? NSDictionary
            {
                loadingVc.finishLoading(nil)
                if (tckPaid.objectForKey("paid") as! Bool) {
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                    
                    (vc.viewControllers[0] as! ReceiptViewController).labels = ["success": kMessageSuccessPayment,
                                                                                "date": NSDate.getDateTodayToShortYearFormat(),
                                                                                "time":NSDate.getTimeTodayToFormat(),
                                                                                "title": kPaymentConfirmedTitle,
                                                                                "type":TypePageSuccess.PagamentoTicket.rawValue,
                                                                                "price":self.ticketPrice]
                    vc.loadView()
                    (vc.viewControllers[0] as! ReceiptViewController).previousVC = self
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else {
                    Util.print ("PAGAMENTO FALHOU (NAO DEVERIA CAIR AQUI, E SIM NO FAILURE)")
                }
            }
        }
        
        api.failureCase = {(msg) in
            Util.print("Payment fail: \(msg)")
            self.containerPayment.userInteractionEnabled = true
            loadingVc.finishLoading(nil)
            let alert = UIAlertController(title: kAlertTitle, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: kAlertButtonOk, style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        let customerInfo : Dictionary<String, AnyObject> = ["sessionToken":sessionToken,"cardId":cardId,"paymentMode":paymentMode.rawValue]
        
        let parameters = ["amount":self.ticketPrice*100,"deviceUuid":SystemInfo.getUserUuid(),"customerInfo":customerInfo]
        api.ticketPayment((receivedValue as String),dictionaryParameters:parameters)
        loadingVc.startLoading(self,title: kWait)

    }

}


