//
//  ListCiyTableViewCell.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ListCiyTableViewCell: UITableViewCell {

    @IBOutlet weak var lbNameCity: UILabel!
    @IBOutlet weak var iconSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func populateData(dic: NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            self.lbNameCity.text =  dic["name"] as? String
            }
    }
    
    func markSelected(){
        
    }
}
