//
//  ViewController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 19/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ListHomeViewController: UIViewController {

    var items                           : [Int] = []
    var listItensSearched               = [ItemSearched]()
    var cards                           = [Card]()
    @IBOutlet var carousel              : iCarousel!
    let searchBar                       = UISearchBar()
    var tbView                          = UITableView()
    let textWelcome                     = UITextView()
    let kTextMessageNotLogged           = "Bem-vindo!"
    let blurEffectView                  = UIVisualEffectView(effect: UIBlurEffect(style: .ExtraLight))
    
    var searchActivate                  = false
    var timeSearch                      : NSTimeInterval!
    var canRequest                      : Bool!
    var lastText                        : String!
    var offsetCartView                  = CGSize() //Este valor vai ser somado sempre a todos frames
    var percentCardImage                : CGFloat = 0.8
    var percentCardText                 : CGFloat = 0.2
    var noResults                       = false
    var statusBarStyle                  : UIStatusBarStyle = .Default
    var orderView                       = OrderHomeTableViewCell()
    static let timeToSearch             = 0.7 as Double
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.initialConfig()//ajusta as configurações do iCarousel primeiro, já valida se precisa de um offset para mostrar pedidos
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return statusBarStyle
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        carousel.type = .Rotary
        carousel.scrollSpeed = 4
        carousel.pagingEnabled = true
        carousel.contentOffset = kFrameCardOffset
        
        initialSettings(true)
        createOrderView()
        orderView.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.generateOrdersView),
            name: "UpdateInOrderStatus",
            object: nil)
        
        self.tbView.registerNib(UINib(nibName: "SearchTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "cellSearch")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        /* --- Validate if heve orders to show --- */
        generateOrdersView()
        CartCustomManager.sharedInstance.hideCart()
        self.hideNavBar()
        SessionManager.sharedInstance.searchOrQrEnabled = false
        SessionManager.sharedInstance.cardColor = LayoutManager.green4all

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segue4park" {
            segue.destinationViewController.title = "4Park"
        } else if segue.identifier == "segueGastronomia" {
            segue.destinationViewController.title = "Gastronomia"
            segue.destinationViewController.hidesBottomBarWhenPushed = true
        }
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        stopSearch()
    }
    
    //MARK: - Custom Methods
    private func reloadScreen(orderViewHidden:Bool, hasOrders:Bool) {
        /* --- Sets the status bar to light and refresh --- */
        self.statusBarStyle = .Default
        self.setNeedsStatusBarAppearanceUpdate()
        /* --- Apply the offset needed to position the cards --- */
        self.adjustInsetsFromOrder(hasOrders)
        self.orderView.hidden = orderViewHidden
    }
    
    private func initialSettings(first:Bool) {
        /* --- Ajusta o tamanho do card e empurra o carousel para baixo --- */
        carousel.contentOffset.height = (kFrameCardOffset.height + offsetCartView.height)
        
        /* --- customize search bar --- */
        searchBar.delegate = self
        searchBar.tintColor = UIColor().hexStringToUIColor("009933")
        searchBar.placeholder = "Busca"
        
        /* --- posicao X = diferenca do width para a view, dividido pela metade para lado iguais --- */
        kFrameSearchBar = CGRect(x: (UIScreen.mainScreen().bounds.width-kSizeSearchBar.width)*0.5,
                                 y: 90 + offsetCartView.height,
                                 width: kSizeSearchBar.width,
                                 height: kSizeSearchBar.height)
        searchBar.frame = kFrameSearchBar
        searchBar.searchBarStyle = .Minimal
        
        /* --- customize welcome text --- */
        if Lib4all.sharedInstance().hasUserLogged() {
            if let user = User.sharedUser() {
                let firstName = Util.getFirstNameFromFullName(user.fullName)
                self.textWelcome.text = "Bem-vindo, " + firstName + "."
                self.textWelcome.font = LayoutManager.primaryFontWithSize(kSizeTextWelcome.height-CGFloat(firstName.characters.count) , bold: false)
            }
        } else {
            self.textWelcome.text = kTextMessageNotLogged
            self.textWelcome.font = LayoutManager.primaryFontWithSize(kSizeTextWelcome.height, bold: false)
        }
        self.textWelcome.backgroundColor = UIColor.clearColor()
        self.textWelcome.textAlignment = .Center
        let card : Card = cards[self.carousel.currentItemIndex]
        self.textWelcome.textColor = Util.hexStringToUIColor(card.color)
        kFrameTextWelcome = CGRect(x: 0,
                                   y: kFrameSearchBar.origin.y - kSizeTextWelcome.height - 20,
                                   width: kSizeTextWelcome.width,
                                   height: kSizeTextWelcome.height*2)
        self.textWelcome.frame = kFrameTextWelcome
        self.textWelcome.userInteractionEnabled = false
        
        /* --- customize tableview --- */
        self.tbView.alpha = 0;
        self.tbView.delegate = self
        self.tbView.dataSource = self
        self.tbView.separatorStyle = .None
        self.tbView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.75)
        let posY = kFrameSearchBar.origin.y+kSizeSearchBar.height
        kFrameTableViewResults = CGRect(x: 0,
                                        y: posY,
                                        width: UIScreen.mainScreen().bounds.width,
                                        height: UIScreen.mainScreen().bounds.height-posY-(self.tabBarController?.tabBar.frame.height)!)
        let tmpTable = self.tbView
        tmpTable.frame = kFrameTableViewResults
        
        if !first {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ListHomeViewController.touchTableView(_:)))
            tmpTable.addGestureRecognizer(tapGesture)
            self.tbView = tmpTable
            
            self.view.addSubview(self.textWelcome)
            self.view.addSubview(self.searchBar)
            self.view.addSubview(self.tbView)
        }
    }
    
    private func createOrderView() {
        orderView = NSBundle.mainBundle().loadNibNamed("OrderHomeTableViewCell", owner: self, options: nil)!.first as! OrderHomeTableViewCell
        orderView.viewController = self
        orderView.frame = CGRectMake(0, 0, self.view.frame.width, 65)
        orderView.bounds = CGRectMake(0, -12, orderView.frame.width, 65)
        self.view.addSubview(orderView)
    }
    
    @objc private func generateOrdersView(){
        let arrayOrders = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
        if arrayOrders.count > 0 {
            orderView.arrayOrders = arrayOrders
            orderView.collectionView.reloadData()
            let card = cards[carousel.currentItemIndex] as Card
            orderView.backgroundColor = Util.hexStringToUIColor(card.color)
            orderView.cartTintColor = (UIColor.whiteColor())
            if self.isBeingPresented() {
                UIView.animateWithDuration(0.3, animations: {
                    self.reloadScreen(false, hasOrders: true)
                })
            } else {
                self.reloadScreen(false, hasOrders: true)
            }
        }
        else {
            if self.isBeingPresented() {
                UIView.animateWithDuration(0.3, animations: {
                    self.reloadScreen(true, hasOrders: false)
                })
            } else {
                self.reloadScreen(true, hasOrders: false)
            }
        }
    }
    
    /* --- Valisa se há necessidade de um offset de carrinho --- */
    func adjustInsetsFromOrder(hasOrder:Bool) {
        if hasOrder {
            offsetCartView = CGSizeMake(0, 24)
            percentCardImage = 0.6
        }
        else {
            offsetCartView = CGSizeMake(0, 0)
        }
        UIView.animateWithDuration(0.3) { 
            self.initialSettings(false)
            self.view.setNeedsLayout()
        }
    }
    
    @IBAction func touchTableView(touch: UITapGestureRecognizer) {
        if let indexRow : NSIndexPath = tbView.indexPathForRowAtPoint(touch.locationInView(touch.view)) {
            self.tableView(self.tbView,
                           didSelectRowAtIndexPath: indexRow)
        }
        else {
            self.hideTableView()
        }
    }
    
    func showNavBar(navBarTintColor:UIColor?=nil) {
        if let barColor:UIColor = navBarTintColor {
            self.navigationController?.navigationBar.barTintColor = barColor
        }
        else {
            self.navigationController?.navigationBar.barTintColor = LayoutManager.green4all
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func hideTableView() {
        self.stopSearch()
        self.searchBar.text = ""
        self.searchBar(self.searchBar, textDidChange: "")
        self.searchBar.setShowsCancelButton(false, animated: false)
        UIView.animateWithDuration(0.3, animations: {
            self.tbView.alpha = 0
            }, completion: { (completed) in
        })
    }
    
    func showTableView() {
        UIView.animateWithDuration(0.3, animations: {
            self.tbView.alpha = 1
            }, completion: { (completed) in
        })
    }
    
    func stopSearch() {
        self.searchBar.resignFirstResponder()
    }
    
    func verifyRequest(text: String)
    {
        if text == lastText && text.characters.count >= 3
        {
            if timeSearch >= SectionsManager.timeToSearch && canRequest == true
            {
                if self.searchBar.text?.characters.count >= 3
                {
                    self.searchFromAPI(self.searchBar.text!)
                    canRequest = false
                }
            }
            else if timeSearch < SectionsManager.timeToSearch
            {
                timeSearch = timeSearch.advancedBy(0.1)
                self.performSelector(#selector(verifyRequest), withObject: text, afterDelay: 0.1)
            }
        }
        else
        {
            return
        }
    }
    
    func initialConfig() {
        kSizeCardImage                          = CGSize(width: kSizeCard.width,
                                                         height: kSizeCard.height * percentCardImage)
        kSizeCardTextView                       = CGSize(width: kSizeCard.width,
                                                         height: kSizeCard.height * percentCardText)
        kFrameCarouselCardFrame                 = CGRect(x: 0,
                                                         y: 100 + offsetCartView.height,
                                                         width: kSizeCard.width,
                                                         height: kSizeCard.height)
        kFrameCarouselCardImageFrame            = CGRect (x: 0,
                                                          y: 0,
                                                          width: kSizeCardImage.width,
                                                          height: kSizeCardImage.height)
        kFrameCarouselCardTextView              = CGRect (x: 0,
                                                          y: kSizeCardImage.height,
                                                          width: kSizeCardTextView.width,
                                                          height: kSizeCardTextView.height)
    }
    
    //MARK: - Request search from API
    
    func searchFromAPI(searchText: String)
    {
        let api = ApiServices()
        
        api.successCase = {(obj) in
            if let arrayContent = obj as? NSArray
            {
                self.listItensSearched.removeAll()
                
                //Convert dictionary to ItemSearched
                for objectSearched in arrayContent
                {
                    let objectConverted = ItemSearched()
                    
                    objectConverted.id      = objectSearched.objectForKey("id") as! Int
                    objectConverted.logo    = objectSearched.objectForKey("logo") as? String
                    objectConverted.subType = objectSearched.objectForKey("subType") as? Int
                    objectConverted.title   = objectSearched.objectForKey("title") as? String
                    objectConverted.type    = objectSearched.objectForKey("type") as? String
                    
                    self.listItensSearched.append(objectConverted)
                }
                
                self.tbView.reloadData()
            } else if self.listItensSearched.count <= 0 {
                self.noResults = true
                self.tbView.reloadData()
            }
        }
        
        api.failureCase = {(msg) in
            self.noResults = true
        }
        
        //Get session Token
        let lib4all = Lib4all.sharedInstance()
        var sessiontoken = ""
        if lib4all.hasUserLogged() {
            let account = lib4all.getAccountData() as NSDictionary
            sessiontoken = String((account.objectForKey("sessionToken"))!)
        }
        
        api.searchAnythingOnServer(searchText,sessionToken: sessiontoken)
    }
}
