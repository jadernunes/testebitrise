//
//  ListScheduleViewController.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import JGProgressHUD

class ListScheduleViewController : UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tableViewListSchedule: UITableView!
    @IBOutlet weak var labelEmpty: UILabel!
    
    var pullRefresh: UIRefreshControl!
    var listSchedule: Results<Schedule>?
    let loading = JGProgressHUD(style: .Dark)
    let lib4all = Lib4all()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        self.setLayoutData()
        
        self.addObserverChangeScheduleStatus()
        self.listSchedule = SchedulePersitence.getAllLocalSchedules()
        self.checkEmpty()
        self.loading.showInView(self.tableViewListSchedule)
        self.refresh()
    }
    
    override func viewWillAppear(animated: Bool) {
        CartCustomManager.sharedInstance.hideCart()
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMySchedules)
    }
    
    func checkEmpty() {
        if self.listSchedule!.count == 0 {
            self.tableViewListSchedule.hidden = true
            self.labelEmpty.hidden = false
        }
        else {
            self.tableViewListSchedule.hidden = false
            self.labelEmpty.hidden = true
        }
    }
    
    private func setLayoutData() {
        self.navigationItem.title = kMySchedulesTitle
        self.labelEmpty?.text = kMessageWithoutDataSchedules
        self.labelEmpty?.font = LayoutManager.primaryFontWithSize(16)
        self.labelEmpty?.textColor = LayoutManager.labelFirstColor
        self.labelEmpty?.hidden = true
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableViewListSchedule.addSubview(pullRefresh)
        self.tableViewListSchedule.backgroundColor = LayoutManager.backgroundFirstColor
    }
    
    //MARK: - Get all schedule on server
    @objc private func refresh() {
        self.getAllSchedulesByServer()
    }
    
    func getAllSchedulesByServer()
    {
        let api = ApiServices()
        
        api.successCase = {(obj) in

            SchedulePersitence.deleteAllObjects()
            SchedulePersitence.saveManySchedule(obj as! NSArray)
            
            self.listSchedule = SchedulePersitence.getAllLocalSchedules()
            self.tableViewListSchedule.reloadData()
            self.pullRefresh?.endRefreshing()
            self.loading.dismiss()
            self.checkEmpty()
        }
        
        api.failureCase = {(msg) in
            self.loading.dismiss()
            self.pullRefresh?.endRefreshing()
        }
        
        //Get token by user loged
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    api.getAllScheduleBySessionToken(user.token)
                } else {
                    self.pullRefresh.endRefreshing()
                }
            } else {
                self.pullRefresh.endRefreshing()
            }
        } else {
            self.pullRefresh.endRefreshing()
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.listSchedule != nil
        {
            return (self.listSchedule?.count)!
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: ListScheduleTableViewCell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_SCHEDULE, forIndexPath: indexPath) as! ListScheduleTableViewCell
        
        let scheduleObject = listSchedule![indexPath.row]
        cell.populateData(scheduleObject)
        
        return cell
    }
    
    func addObserverChangeScheduleStatus() {
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.didChangeStatusSchedule),
            name: NOTIF_SCHEDULE_UPDATE,
            object: nil)
    }
    
    func didChangeStatusSchedule() {
        self.listSchedule = SchedulePersitence.getAllLocalSchedules()
        self.tableViewListSchedule.reloadData()
    }
}
