//
//  ExtraProductsViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ExtraProductsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate, UIScrollViewDelegate, IterativeMessageDelegate {

    let heightSectionHeader = 54 as CGFloat
    static let sharedInstance = ExtraProductsViewController()
    var listUnityItemModifiers  :List<UnityItemModifier>!
    var objectWillSelect: (position:Int,modifier:UnityItemModifier?) = (position:0,modifier:nil)
    var unityItem: UnityItem!
    var unity: Unity?
    
    /* --- viewHeader --- */
    var viewHeader : UIView!
    private let kTableHeaderHeight : CGFloat = 180
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var viewBottomInformations: UIView!
    @IBOutlet weak var tableViewListModifiers: UITableView!
    @IBOutlet weak var labelTotalPrice: UILabel!
    @IBOutlet weak var buttonAddInCart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        SessionManager.sharedInstance.listUnityItemsSeleted.removeAll()
        var i = 0
        for modifier in listUnityItemModifiers {
            SessionManager.sharedInstance.listUnityItemsSeleted.append((index:i,modifier:modifier,listUnityItem:nil))
            i += 1
        }
                
        viewBottomInformations.backgroundColor = LayoutManager.backgroundFirstColor
        labelTotalPrice.textColor = SessionManager.sharedInstance.cardColor
        buttonAddInCart.tintColor = SessionManager.sharedInstance.cardColor
        self.title = unityItem.title
        
        self.tableViewListModifiers.backgroundColor = LayoutManager.lightGray4all
        self.view.backgroundColor = LayoutManager.lightGray4all
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableViewListModifiers.reloadData()
        
        updateUIItemsValue()
        CartCustomManager.sharedInstance.hideCart()
        Util.adjustInsetsFromCart(tableViewListModifiers)
        
        self.createViewHeader()
    }
    
    override func viewDidAppear(animated: Bool) {
        updateViewHeader()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    //MARK: -
    
    func handleTapInSection(gestureRecognizer: UIGestureRecognizer)
    {
        let index = gestureRecognizer.view?.tag
        let modifier = listUnityItemModifiers[index!]
        objectWillSelect = (position:index!,modifier:modifier)
        
        if canAddMoreItens(modifier, listUnityItem: SessionManager.sharedInstance.listUnityItemsSeleted[index!].listUnityItem) {
            self.performSegueWithIdentifier("segueGotoListProductsSelected", sender: nil)
        } else {
            let alert = UIAlertView(title: kAlertTitle, message: kMessageAllExtrasChosen, delegate: self, cancelButtonTitle: kAlertButtonOk)
            alert.show()
        }
    }
    
    func updateViewHeader() {
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableViewListModifiers.bounds.width, height: kTableHeaderHeight)
        if tableViewListModifiers.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableViewListModifiers.contentOffset.y
            headerRect.size.height = -tableViewListModifiers.contentOffset.y
        }
        self.viewHeader.frame = headerRect
    }
    
    func createViewHeader() {
        
        /* --- Header View --- */
        if viewHeader == nil {
            
            viewHeader = UIView.init()
            viewHeader = tableViewListModifiers.tableHeaderView
            tableViewListModifiers.tableHeaderView = nil
            tableViewListModifiers.addSubview(viewHeader)
            tableViewListModifiers.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
            tableViewListModifiers.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
            
            for viewIndex in 0..<viewHeader.subviews.count {
                if viewHeader.subviews[viewIndex].isMemberOfClass(UIImageView) {
                    if let imgView = viewHeader.subviews[viewIndex] as? UIImageView {
                        if unityItem.thumb != nil {
                            imgView.addImage(unityItem.thumb!)
                        }
                    }
                }
                
                if viewHeader.subviews[viewIndex].isMemberOfClass(UITextView) {
                    if let txtView = viewHeader.subviews[viewIndex] as? UITextView {
                        txtView.editable = false
                        if let strDesc = unityItem.desc {
                            txtView.text = strDesc
                            txtView.textContainerInset = UIEdgeInsetsMake(5, 10, 5, 10);
                        }
                    }
                }
            }
            
            updateViewHeader()
        } else {
            tableViewListModifiers.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
            tableViewListModifiers.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
            updateViewHeader()
        }
    }
    
    //MARK: - Table View methods
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let headerFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 10.0)
        let customView = UIView(frame: headerFrame)
        customView.backgroundColor = LayoutManager.lightGray4all
        return customView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightSectionHeader
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let imageFrame = CGRect(x: self.view.frame.size.width - 35, y: 10, width: 20, height: 30)
        let titleFrame = CGRect(x: 5, y: 2, width: self.view.frame.size.width - 30, height: heightSectionHeader)
        let headerFrame = CGRect(x: 5, y: 0, width: self.view.frame.size.width-5, height: heightSectionHeader)
        
        let iconDisclosure = UIImageView(frame: imageFrame)
        iconDisclosure.image = UIImage(named: "iconDisclosure")
        iconDisclosure.tintImage(SessionManager.sharedInstance.cardColor)
        
        let customTitle = UILabel(frame: titleFrame)
        customTitle.numberOfLines = 2
        customTitle.textColor = SessionManager.sharedInstance.cardColor
        
        let itemSection = SessionManager.sharedInstance.listUnityItemsSeleted[section]
        customTitle.text = itemSection.modifier.name
        
        let customView = UIView(frame: headerFrame)
        customView.backgroundColor = LayoutManager.backgroundFirstColor
        customView.addSubview(customTitle)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapInSection))
        tapRecognizer.delegate = self
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        customView.addGestureRecognizer(tapRecognizer)
        customView.tag = section
        customView.addSubview(iconDisclosure)
        
        return customView
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return SessionManager.sharedInstance.listUnityItemsSeleted.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if SessionManager.sharedInstance.listUnityItemsSeleted[section].listUnityItem != nil {
            return SessionManager.sharedInstance.listUnityItemsSeleted[section].listUnityItem.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell : UITableViewCell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "unityDescription")
            let textField : UITextView = UITextView.init(frame: cell.bounds)
            textField.userInteractionEnabled = false
            cell .addSubview(textField)
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_SELECT_EXTRA_PRODUCT, forIndexPath: indexPath) as! SelectProductTableViewCell
        let listItems = SessionManager.sharedInstance.listUnityItemsSeleted[indexPath.section].listUnityItem
        let unityItem = listItems[indexPath.row]
        cell.populateData(unityItem,modifier: SessionManager.sharedInstance.listUnityItemsSeleted[indexPath.section].modifier)
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            SessionManager.sharedInstance.listUnityItemsSeleted[indexPath.section].listUnityItem.removeAtIndex(indexPath.row)
            updateUIItemsValue()
            
            self.tableViewListModifiers.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    //MARK: - ScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
         updateViewHeader()
    }
    
    //MARK: - Get title section
    
    private func getSectionTitle(itemSection:(index:Int!,modifier:UnityItemModifier,listUnityItem:[UnityItem]!)) -> String {
        var strMinCount = ""
        
        if itemSection.modifier.minQuantity > 0 {
            strMinCount = kLabelMinimumExtra + String(itemSection.modifier.minQuantity)
        }
        
        var countSelected = 0
        if itemSection.listUnityItem != nil {
            countSelected = itemSection.listUnityItem.count
        }
        
        let strMaxCount = String(countSelected) + "/" + String(itemSection.modifier.maxQuantity) + strMinCount
        let strTitle = String(itemSection.modifier.name) + "\n" + strMaxCount
        
        return strTitle
    }
    
    //MARK: - Check add item
    
    private func canAddMoreItens(modifier:UnityItemModifier,listUnityItem:[UnityItem]!) -> Bool {
        
        if listUnityItem != nil {
            if listUnityItem.count >= modifier.maxQuantity {
                return false
            }
        }
        
        updateUIItemsValue()
        
        return true
    }
    
    //MARK: - Check items to cart - not in use
    
    private func checkMinItensToUseInCart() -> (isValid:Bool,message:String) {
        
        var canAddItemsInCart = true
        var message = ""
        
        for itemSelected in SessionManager.sharedInstance.listUnityItemsSeleted {
            if itemSelected.listUnityItem != nil {
                if itemSelected.listUnityItem.count < itemSelected.modifier.minQuantity {
                    
                    message += "\n" + String(itemSelected.modifier.minQuantity) + " - " + String(itemSelected.modifier.name)
                    canAddItemsInCart = false
                }
            } else {
                if itemSelected.modifier.minQuantity > 0 {
                    canAddItemsInCart = false
                    message += "\n" + String(itemSelected.modifier.minQuantity) + " - " + String(itemSelected.modifier.name)
                }
            }
        }
        
        return (canAddItemsInCart,message)
    }
    
    //MARK: - Update UI values
    
    private func updateUIItemsValue() {
        
        var totalPrice = self.unityItem.price
        var canAddItemsInCart = true
        
        for itemSelected in SessionManager.sharedInstance.listUnityItemsSeleted {
            if itemSelected.listUnityItem != nil {
                if itemSelected.listUnityItem.count < itemSelected.modifier.minQuantity {
                    canAddItemsInCart = false
                }
                
                for subItem in itemSelected.listUnityItem {
                    //Se for pizza dividir valor do item pela quantidade do seu grupo
                    if itemSelected.modifier.idUnityItemModifierPriceType == UnityItemModifierPriceType.sum_total.rawValue &&
                        subItem.idUnityItemType == UnitItemType.Pizza.rawValue {
                        totalPrice += (subItem.price / Double(itemSelected.listUnityItem.count))
                    } else {
                        totalPrice += subItem.price
                    }
                }
            } else {
                if itemSelected.modifier.minQuantity > 0 {
                    canAddItemsInCart = false
                }
            }
        }
        
        self.buttonAddInCart.enabled = canAddItemsInCart
        
        if canAddItemsInCart {
            self.buttonAddInCart.tintColor = SessionManager.sharedInstance.cardColor
        } else {
            self.buttonAddInCart.tintColor = LayoutManager.grayGeneralText
        }
        
        self.labelTotalPrice.text = Util.formatCurrency(totalPrice/100)
    }
    
    //MARK: - Add in Cart
    
    @IBAction func addInCart(sender: AnyObject?) {
        
        let checkProduct = checkMinItensToUseInCart()
        
        if checkProduct.isValid {
            
            if SessionManager.sharedInstance.listUnityItemsSeleted.count <= 0 {
                do {
                    let realm = try! Realm()
                    try OrderEntityManager.sharedInstance.addItemToCard(unityItem, unity: unity!)
                    if let cart = realm.objects(Order.self).filter("status = \(StatusOrder.Open.rawValue)").first {
                        if cart.orderItems.count == 1{
                            NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_CART_CHANGED, object: nil)
                        }
                    }
                }catch _ as NSError {
                    if OrderEntityManager.sharedInstance.hasActiveOrder() {
                        Util.showIterativeMessage(self, typeActions: ActionIterativeMessage.OneOption, tag: 1, message: "Você possui um pedido ativo!\nAguarde para fazer novos.", title: kAlertTitle)
                    } else {
                        Util.showIterativeMessage(self, typeActions: ActionIterativeMessage.TwoOptions, tag: 0, message: "Já existe produtos no carrinho. Gostaria de remover?", title: kAlertTitle)
                    }
                }
                CartCustomManager.sharedInstance.checkNumberOfItemsInCart()
                self.navigationController?.popViewControllerAnimated(true)
                return
            }
            
            let realm = try! Realm()
            realm.beginWrite()
            
            let orderItem = OrderItem()
            orderItem.id = OrderEntityManager.sharedInstance.getNextKey(realm, aClass: OrderItem.self)
            orderItem.unityItem = unityItem
            orderItem.quantity = 1
            orderItem.itemPrice = self.unityItem.price
            let listOrderItems = List<OrderItem>()
            var idIndex = orderItem.id
            for grupos in SessionManager.sharedInstance.listUnityItemsSeleted {
                if grupos.listUnityItem != nil {
                    for objUnityItem in grupos.listUnityItem {
                        idIndex += 1
                        let countItems = grupos.listUnityItem.count
                        let subOrderItem = OrderItem()
                        subOrderItem.id = idIndex
                        subOrderItem.unityItem = objUnityItem
                        subOrderItem.quantity = 1
                        subOrderItem.itemPrice = objUnityItem.price
                        subOrderItem.parentId = orderItem.id
                        
                        if grupos.modifier.idUnityItemModifierPriceType == UnityItemModifierPriceType.sum_total.rawValue &&
                            objUnityItem.idUnityItemType == UnitItemType.Pizza.rawValue {
                            // é pizza
                            subOrderItem.quantity = 1 / Double(countItems)
                            subOrderItem.total = (subOrderItem.itemPrice * subOrderItem.quantity)
                        } else {
                            subOrderItem.total = objUnityItem.price
                        }
                        
                        orderItem.itemPrice += subOrderItem.total
                        realm.add(subOrderItem, update: true)
                        
                        listOrderItems.append(subOrderItem)
                    }
                }
            }
            
            
            orderItem.total = orderItem.itemPrice // * quantity
            orderItem.subItems = listOrderItems
            realm.add(orderItem, update: true)
            try! realm.commitWrite()
            
            do {
                let status = try OrderEntityManager.sharedInstance.addOrderItemToCart(orderItem,isFirstAdd: true)
                
                if status == true {
                    CartCustomManager.sharedInstance.checkNumberOfItemsInCart()
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }catch _ as NSError {}
        } else {
            let message = kMessageChooseAtLeast + checkProduct.message
            let alert = UIAlertView(title: kAlertTitle, message: message, delegate: self, cancelButtonTitle: kAlertButtonOk)
            alert.show()
        }
    }
    
    //MARK: - Iterative message
    func buttonRefusePressed(sender:IterativeMessageViewController){
        
    }
    
    func buttonConfirmPressed(sender:IterativeMessageViewController){
        if sender.tag == 0 {
            OrderEntityManager.sharedInstance.deleteCart()
            self.addInCart(nil)
            CartCustomManager.sharedInstance.updateCart()
        }
    }
    
    //MARK: - Prepare for Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if ((segue.identifier?.compare("segueGotoListProductsSelected")) != nil) {
            let modifier = objectWillSelect.modifier
            let viewController = segue.destinationViewController as! SelectProductViewController
            viewController.modifier = modifier
            viewController.positionModifier = objectWillSelect.position
        }
    }
}
