//
//  TotalOrderTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/14/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class TotalOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTotalText: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDelivery: UILabel!
    @IBOutlet weak var labelPriceDelivery: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelPrice.font = LayoutManager.primaryFontWithSize(20)
        self.labelPrice.textColor = LayoutManager.darkGreyTracking
        self.labelTotalText.font = LayoutManager.primaryFontWithSize(20)
        self.labelTotalText.textColor = LayoutManager.darkGreyTracking
        self.labelDelivery.font = LayoutManager.primaryFontWithSize(17)
        self.labelDelivery.textColor = LayoutManager.mediumGreyTracking
        self.labelPriceDelivery.font = LayoutManager.primaryFontWithSize(17)
        self.labelPriceDelivery.textColor = LayoutManager.mediumGreyTracking
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
