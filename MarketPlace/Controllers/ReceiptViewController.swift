//
//  ReceiptViewController.swift
//  MarketPlace
//
//  Created by Luciano on 8/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ReceiptViewController: UIViewController {
    
    @IBOutlet weak var labelThanks: UILabel!
    @IBOutlet weak var labelSuccess: UILabel!
    @IBOutlet weak var labelShop: UILabel!
    @IBOutlet weak var labelService: UILabel!
    @IBOutlet weak var labelProfessional: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var buttonFinish: UIButton!
    @IBOutlet weak var imageShop: UIImageView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelSmallPrice: UILabel!
    @IBOutlet weak var viewShowStamp: UIView!
    @IBOutlet weak var constraintHeightButtonAccess: NSLayoutConstraint!
    @IBOutlet weak var buttonAccess: UIButton!
    @IBOutlet weak var iconSuccess: UIImageView!
    
    var previousVC      : UIViewController?
    var labels          : NSDictionary?
    var viewStamp       : FidelityView!
    var idUnity         : Int!
    var idFact          : Int?
    var idVoucher       : Int?
    var isShowFidelity  : Bool?    
    //MARK: - Native Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Util.adjustTableViewInsetsFromTarget(self)

        viewStamp = FidelityView.instanceFromNib()
        initialConfigs()
        setLabels()
        self.hidesBottomBarWhenPushed = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        labelPrice.hidden = true
        labelSmallPrice.hidden = true
        imageShop.hidden = false
        labelShop.hidden = false
        labelProfessional.hidden = false
        labelService.hidden = false
        
        if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoTicket.rawValue || (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoVoucher.rawValue {
            labelPrice.hidden = false
            labelShop.hidden = true
            labelProfessional.hidden = true
            labelService.hidden = true
            imageShop.hidden = true
        } else if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoQR.rawValue {
            labelProfessional.hidden = true
            labelService.hidden = true
            labelSmallPrice.hidden = false
        }else if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoPedido.rawValue {
            labelSmallPrice.hidden = false
            labelProfessional.hidden = true
            labelService.hidden = true
        }else if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoCancelado.rawValue {
            labelShop.hidden = false
            labelSmallPrice.hidden = false
            labelService.hidden = true
            labelProfessional.hidden = true
        }
        
        self.buttonAccess.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Initialize itens
    
    func initialConfigs() {
        labelThanks.font = LayoutManager.primaryFontWithSize(25)
        labelThanks.textColor = SessionManager.sharedInstance.cardColor
        
        labelSuccess.font = LayoutManager.primaryFontWithSize(18)
        labelSuccess.textColor = LayoutManager.labelFirstColor
        
        labelShop.font = LayoutManager.primaryFontWithSize(20)
        labelShop.textColor = LayoutManager.labelFirstColor
        
        labelService.font = LayoutManager.primaryFontWithSize(18)
        labelService.textColor = LayoutManager.labelFirstColor
        
        labelProfessional.font = LayoutManager.primaryFontWithSize(18)
        labelProfessional.textColor = LayoutManager.labelFirstColor
        
        labelDate.font = LayoutManager.primaryFontWithSize(18)
        labelDate.textColor = SessionManager.sharedInstance.cardColor
        
        labelTime.font = LayoutManager.primaryFontWithSize(18)
        labelTime.textColor = LayoutManager.labelFirstColor
        
        labelPrice.font = LayoutManager.primaryFontWithSize(28)
        labelPrice.textColor = LayoutManager.labelFirstColor
        
        labelSmallPrice.font = LayoutManager.primaryFontWithSize(18)
        labelSmallPrice.textColor = LayoutManager.labelFirstColor
        
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        
        buttonFinish.layer.cornerRadius = 6
        buttonFinish.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonFinish.layer.borderWidth  = 1.0
        buttonFinish.titleLabel?.font   = LayoutManager.primaryFontWithSize(22)
        buttonFinish.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.buttonAccess.backgroundColor = SessionManager.sharedInstance.cardColor
        self.buttonAccess.setTitleColor(LayoutManager.grayGeneralText, forState: UIControlState.Normal)
    }
    
    //MARK: - CLose page
    
    @IBAction func finishAction(sender: UIButton) {
        if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoPedido.rawValue {
            if previousVC is CheckViewController {
                previousVC?.navigationController?.popToRootViewControllerAnimated(false)
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                if let navigation = previousVC?.navigationController {
                    navigation.popToRootViewControllerAnimated(false)
                    self.dismissViewControllerAnimated(true, completion: nil)
                } else if (previousVC is CartViewController) {
                    if let cart = previousVC as? CartViewController {
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                } else {
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        } else if (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoTicket.rawValue || (labels?.objectForKey("type"))! as! Int == TypePageSuccess.PagamentoVoucher.rawValue {
            
            self.previousVC!.navigationController?.popToRootViewControllerAnimated(false)
            self.dismissViewControllerAnimated(true, completion: {
            })
        } else {
            
            let child = previousVC!.navigationController?.childViewControllers.filter({ (viewController) -> Bool in
                if viewController.isKindOfClass(DetailsTableViewController) {
                    return true
                }
                return false
            }).first
            
            //Goto Unity Detail
            if child != nil {
                previousVC!.dismissViewControllerAnimated(true, completion: {
                    self.previousVC!.navigationController?.popToViewController(child!, animated: false)
                })
            } else {
                previousVC!.dismissViewControllerAnimated(false, completion: {
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
            }
        }
    }
    
    private func checkShowFidelity(){
        
        if isShowFidelity == true {
            let unityFidelity = UnityFidelityPersistence.getUnityFidelityByIdUnity(idUnity)
            
            if unityFidelity != nil {
                if unityFidelity?.active == true {
                    self.viewShowStamp.hidden = false
                    var myFidelity: MyFidelity!
                    
                    if self.idUnity != nil {
                        myFidelity = MyFidelityPersistence.getMyFidelityByIdUnity(self.idUnity)
                    }
                    
                    if myFidelity == nil {
                        viewStamp.maxItems = unityFidelity?.totalStamp
                        viewStamp.countItems = 1
                        viewStamp.urlImage = unityFidelity?.thumb
                        viewStamp.descriptionTerms = unityFidelity?.terms
                        viewStamp.labelDesc.text = unityFidelity?.winningDesc
                    } else {
                        viewStamp.maxItems = myFidelity?.maxStamp
                        viewStamp.countItems = myFidelity?.stampCount
                        viewStamp.urlImage = myFidelity?.stampThumb
                        viewStamp.descriptionTerms = myFidelity?.terms
                        viewStamp.labelDesc.text = unityFidelity?.winningDesc
                    }
                    
                    self.presentFidelity(unityFidelity?.totalStamp)
                }
            }
        }
    }
    
    //MARK: - Set value in labels
    
    func setLabels() {
        
        self.viewShowStamp.hidden = true
        self.constraintHeightButtonAccess.constant = 0
        self.buttonAccess.hidden = true
        let type = (labels?.objectForKey("type"))! as! Int
        self.idVoucher = labels?.objectForKey("idVoucher") as? Int
        
        if let isShow = labels?.objectForKey("isShowFidelity") as? Bool{
            isShowFidelity = isShow
        }
        
        if type == TypePageSuccess.PagamentoTicket.rawValue || type == TypePageSuccess.PagamentoVoucher.rawValue {
            
            if type == TypePageSuccess.PagamentoVoucher.rawValue {
                self.idFact = labels?.objectForKey("idFact") as? Int
                labelPrice.text = "R$ " + String(format: "%.02f",((labels?.objectForKey("price") as? Double)!)/100)
                self.buttonAccess.setTitle(kAccessVouchers, forState: UIControlState.Normal)
            }
            else {
                labelPrice.text = "R$ " + String(format: "%.02f",((labels?.objectForKey("price") as? Double)!))
                self.buttonAccess.setTitle(kAccessTickets, forState: UIControlState.Normal)
            }

            self.buttonAccess.hidden = false
            self.constraintHeightButtonAccess.constant = 44
            
        } else if type == TypePageSuccess.PagamentoQR.rawValue {
            
            self.idUnity = (labels?.objectForKey("idUnity"))! as! Int
            labelShop.text = labels?.objectForKey("shop") as? String
            labelSmallPrice.text = "R$ " + String(format: "%.02f",(labels?.objectForKey("price") as? Double)!)
            
            let url = labels?.objectForKey("imageUrl") as! String
            imageShop.addImage(url, completionHandler: { (image, error, cacheType, url) in
                self.imageShop.contentMode = .ScaleAspectFill
            })
            
            self.checkShowFidelity()
            
        } else if type == TypePageSuccess.PagamentoPedido.rawValue {
            self.idUnity = (labels?.objectForKey("idUnity"))! as! Int
            self.labelShop.text = (labels?.objectForKey("shop") as? String)
            self.labelSmallPrice.text = "R$ " + String(format: "%.02f",((labels?.objectForKey("price") as? Double)!)/100)
            self.labelService.hidden = true
            self.labelProfessional.hidden = true
            let url = labels?.objectForKey("imageUrl") as! String
            imageShop.sd_setImageWithURL( NSURL(string: "\(BASE_URL_IMAGES)\(url)"), placeholderImage: UIImage.init(named: "placeholder")) { (image, error, cacheType, url) in
                
                if image != nil{
                    self.imageShop.contentMode = .ScaleAspectFill
                }
            }
            
            self.checkShowFidelity()
            
        } else if type == TypePageSuccess.PagamentoCancelado.rawValue {
            self.labelShop.text = (labels?.objectForKey("shop") as? String)
            let price = (labels?.objectForKey("price"))!
            let priceCleaned = Util.removerCharacters(String(price))
            self.labelSmallPrice.text = Util.formatCurrency(Double(priceCleaned)! / 100)
            
            let url = labels?.objectForKey("imageUrl") as! String
            imageShop.addImage(url, completionHandler: { (image, error, cacheType, url) in
                self.imageShop.contentMode = .ScaleAspectFill
            })
        } else {
            //Schedule
            labelThanks.text = labels?.objectForKey("subTitle") as? String
            labelShop.text = labels?.objectForKey("shop") as? String
            labelService.text = labels?.objectForKey("service") as? String
            labelProfessional.text = labels?.objectForKey("professional") as? String
            labelSmallPrice.text = "R$ " + String(format: "%.02f",(labels?.objectForKey("price") as? Double)!)
            
            let url = labels?.objectForKey("imageUrl") as! String
            imageShop.addImage(url, completionHandler: { (image, error, cacheType, url) in
                self.imageShop.contentMode = .ScaleAspectFill
            })
        }
        
        labelSuccess.text = labels?.objectForKey("success") as? String
        labelDate.text = labels?.objectForKey("date") as? String
        labelTime.text = labels?.objectForKey("time") as? String
        self.title = labels?.objectForKey("title") as? String
        
        //Google Analytics configuring
        trackingGoogleAnalytics()
        
        self.iconSuccess.tintImage(SessionManager.sharedInstance.cardColor)
    }
    
    //MARK: Google Analytics
    private func trackingGoogleAnalytics() {
        
        switch TypePageSuccess(rawValue: (labels?.objectForKey("type")! as! Int))! {
        case .PagamentoTicket:
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmTicketPayment)
            break
            
        case .PagamentoQR:
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmQRPayment)
            break
            
        case .PagamentoPedido:
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmOrderPayment)
            break
            
        case .Schedule:
            if SessionManager.sharedInstance.unity.immediateScheduling {
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmSchedule)
            } else {
                GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenRequesSchedule)
            }
            break
        case .PagamentoCancelado:
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmCancelPayment)
            break
        case .PagamentoVoucher:
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenConfirmVoucherPayment)
            break
        }
    }
    
    //MARK: - Present Fidelity
    
    private func presentFidelity(countItens: Int!) {
        self.viewShowStamp.layer.masksToBounds = true
        self.viewShowStamp.layer.cornerRadius = 10
        
        self.viewShowStamp.layer.borderWidth = 0.5
        self.viewShowStamp.layer.borderColor = SessionManager.sharedInstance.cardColor.CGColor
        
        let deviceBounds = UIScreen.mainScreen().bounds
        viewStamp.frame = CGRectMake(0, 0, deviceBounds.width-20, Util.getHeightViewFidelityByCountItems(countItens) - 25)
        viewShowStamp.addSubview(viewStamp)
        
        var newFrame = viewShowStamp.frame
        newFrame.size.height -= 30
        viewShowStamp.frame = newFrame
        
        self.viewShowStamp.setNeedsLayout()
        self.viewShowStamp.layoutIfNeeded()
        viewStamp.setNeedsLayout()
        viewStamp.layoutIfNeeded()
    }
    
    @IBAction func buttonAccessPressed(sender: AnyObject) {
        
//        if SessionManager.sharedInstance.searchOrQrEnabled == false {
//            HomeTableViewController.rootVc.navigationController?.popToRootViewControllerAnimated(false)
//        }
//        else {
//            self.previousVC!.navigationController?.popToRootViewControllerAnimated(false)
//        }
        let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
        viewController.isListVouchers = true
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

