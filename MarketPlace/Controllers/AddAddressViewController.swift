//
//  AddAddressViewController.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class AddAddressViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldIdentifier      : UITextField!
    @IBOutlet weak var textFieldCEP             : UITextField!
    @IBOutlet weak var textFieldNumber          : UITextField!
    @IBOutlet weak var textFieldComplement      : UITextField!
    @IBOutlet weak var buttonAdd                : UIButton!
    @IBOutlet weak var activityIndicator        : UIActivityIndicatorView!
    @IBOutlet weak var textFieldStreet          : UITextField!
    @IBOutlet weak var textFieldCity            : UITextField!
    @IBOutlet weak var textFieldNeighborhood    : UITextField!
    @IBOutlet weak var scrollAddress: UIScrollView!
    
    
    var cep         = ""
    var address     = Address()
    var isCepCity   = true

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        textFieldCEP.addTarget(self, action: #selector(AddAddressViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddAddressViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        buttonAdd.layer.cornerRadius = 6
        buttonAdd.layer.borderWidth  = 1.0
        buttonAdd.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonAdd.titleLabel?.font   = LayoutManager.primaryFontWithSize(22)
        buttonAdd.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonAdd.setTitle(kAdd, forState: UIControlState.Normal)
        
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: LayoutManager.primaryFontWithSize(18), NSForegroundColorAttributeName : UIColor.whiteColor()]
        activityIndicator.hidden = true
        self.title = kAddAddressTitle
        
        self.textFieldStreet.userInteractionEnabled = false
        self.textFieldCity.userInteractionEnabled = false
        self.textFieldNeighborhood.userInteractionEnabled = false
        
        
        /* --- Fit content when keyboard appears --- */
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillChangeFrameNotification, object: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        textFieldCEP.text = ""
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textFieldDidEndEditing(textField: UITextField) {
        if textField == textFieldCEP {
            textField.attributedPlaceholder = NSAttributedString(string:kInsertZipCode,attributes:[NSForegroundColorAttributeName: UIColor().hexStringToUIColor("919194")])
            if textFieldCEP.text?.characters.count >= 8 {
                cep = textFieldCEP.text!
            } else {
                textFieldStreet.text = ""
                textFieldStreet.borderStyle = .RoundedRect
                textFieldCity.text = ""
                textFieldCity.borderStyle = .RoundedRect
                textFieldNeighborhood.text = ""
                textFieldNeighborhood.borderStyle = .RoundedRect
                textFieldNumber.text = ""
                textFieldComplement.text = ""
            }
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == textFieldCEP {
            textField.placeholder = ""
            if textField.text?.characters.count == 8 {
                textField.text = ""
                cep = ""
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldCEP {
            //removing characters
            if string == "" {
                return true
            }
            if textField.text?.characters.count > 7 {
                
                textField.resignFirstResponder()
                return false
            }
            else {
                return true
            }
        }
        return true
    }
    
    func textFieldDidChange(textField: UITextField) {
        if textField == textFieldCEP {
 
            if textField.text?.characters.count >= 8 {
                textField.resignFirstResponder()
                cep = textField.text!
                checkAddress()
            }
        }
    }
    
    func showLabels() {
        textFieldStreet.hidden = false
        textFieldCity.hidden = false
        textFieldNeighborhood.hidden = false
        textFieldNumber.hidden = false
        textFieldComplement.hidden = false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollAddress.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollAddress.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollAddress.contentInset = contentInset
    }
    
    func filterDigits(str:String) -> String {
        
        let aSet = NSCharacterSet(charactersInString:"0123456789").invertedSet
        let compSepByCharInSet = str.componentsSeparatedByCharactersInSet(aSet)
        let numberFiltered = compSepByCharInSet.joinWithSeparator("")
        return numberFiltered
        
    }
    
    func checkAddress() {
        let api = ApiServices()
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        api.successCase = {(obj) in
            if let address = obj as? NSDictionary {
                self.address.name = self.textFieldIdentifier.text
                self.address.zip = self.filterDigits(self.textFieldCEP.text!)
                
                let street = address.objectForKey("address") as? String
                if street?.characters.count > 0 {
                    self.isCepCity = false
                    self.textFieldNeighborhood.hidden = false
                } else {
                    self.isCepCity = true
                    self.textFieldNeighborhood.hidden = true
                }
                
                self.textFieldCEP.text = self.cep
                self.address.street = street
                self.address.neighborhood = address.objectForKey("neighborhood") as? String
                self.address.city = address.objectForKey("city") as? String
                self.address.state = address.objectForKey("uf") as? String
                self.textFieldStreet.text? = self.address.street!
                self.textFieldCity.text? = self.address.city! + ", " + self.address.state!
                self.textFieldNeighborhood.text? = self.address.neighborhood!
                self.activityIndicator.hidden = true
                self.activityIndicator.stopAnimating()
                self.textFieldStreet.userInteractionEnabled = false
                self.textFieldStreet.borderStyle = .None
                self.textFieldCity.userInteractionEnabled = false
                self.textFieldCity.borderStyle = .None
                self.textFieldNeighborhood.userInteractionEnabled = false
                self.textFieldNeighborhood.borderStyle = .None
                
                if self.address.street == "" {
                    self.textFieldStreet.userInteractionEnabled = true
                    self.textFieldStreet.borderStyle = .RoundedRect
                }
                
                if self.address.neighborhood == "" {
                    self.textFieldNeighborhood.userInteractionEnabled = true
                    self.textFieldNeighborhood.borderStyle = .RoundedRect
                }

                self.showLabels()
            }
        }
        api.failureCase = { (msg) in
            self.activityIndicator.hidden = true
            self.activityIndicator.stopAnimating()
            
            self.textFieldStreet.text = ""
            self.textFieldCity.text = ""
            self.textFieldNeighborhood.text = ""
            self.textFieldNumber.text = ""
            self.textFieldComplement.text = ""
            
            Util.showAlert(self, title: kAlertTitle, message: msg)
        }
        cep = self.filterDigits(cep)
        api.checkCep(cep)
    }
    
    @IBAction func addAddress(sender: AnyObject) {

        if textFieldCEP.text?.characters.count > 0 {
            if textFieldIdentifier.text?.characters.count > 0 {
                if textFieldNumber.text?.characters.count > 0 {
                    if textFieldStreet.text?.characters.count > 0 {
                        if textFieldNeighborhood.text?.characters.count > 0  || self.isCepCity == true {
                            
                            let realm = try! Realm()
                            address.id = AddressPersistence.getNextKey(realm, aClass: Address.self)
                            address.number = self.textFieldNumber.text
                            address.complement = self.textFieldComplement.text
                            address.street = self.textFieldStreet.text
                            address.neighborhood = self.textFieldNeighborhood.text
                            try! realm.write {
                                realm.create(Address.self, value: address, update: true)
                            }
                            self.navigationController?.popViewControllerAnimated(true)
                            
                        } else {
                            Util.showAlert(self, title:kAlertTitle, message: kMessageInsertNeighborhood)
                        }
                    }
                    else {
                        Util.showAlert(self, title:kAlertTitle, message: kMessageInsertStreet)
                    }
                }
                else {
                    Util.showAlert(self, title:kAlertTitle, message: kMessageInsertHouseNumber)
                }
            }
            else {
                Util.showAlert(self, title:kAlertTitle, message: kMessageInsertAddressIdentifier)
            }
        }
        else {
            Util.showAlert(self, title:kAlertTitle, message: kMessageInsertZip)
        }
    }

}
