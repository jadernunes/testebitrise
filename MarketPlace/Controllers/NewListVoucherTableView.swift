//
//  NewListVoucherTableView.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewListVoucherTableView: UIViewController {
    
    @IBOutlet weak var tableViewVoucher: UITableView!
    var isListUsed: Bool = false
    var listVouchers: [Voucher]! = [Voucher]()
    var superViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewVoucher.delegate = self
        tableViewVoucher.dataSource = self
        
        self.tableViewVoucher.registerNib(UINib(nibName: "NewCellListVoucher", bundle: nil), forCellReuseIdentifier: "NewCellListVoucher")
        
        if isListUsed {
            listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Order,.Fidelity], listStatus: [.Used])
        } else {
            listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Order,.Fidelity], listStatus: [.Created,.Cancelled,.Expired])
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension NewListVoucherTableView: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listVouchers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("NewCellListVoucher", forIndexPath: indexPath) as? NewCellListVoucher {
            cell.imageViewLogo.addImage(nil)
            cell.labelNameUnity.text = ""
            cell.labelTipoPedido.text = ""
            
            if isListUsed {
                cell.imageViewDiscloserIndicator.image = UIImage(named: "iconDiscloserIndicatorGray")
            } else {
                cell.imageViewDiscloserIndicator.image = UIImage(named: "iconDiscloserIndicatorGreen")
            }
            
            let voucher = self.listVouchers[indexPath.row]
            
            if let order = voucher.order {
                if let unity = UnityPersistence.getUnityByID("\(order.idUnity)") {
                    if let logo = unity.getLogo() {
                        cell.imageViewLogo.addImage(logo.value)
                    }
                    cell.labelNameUnity.text = unity.name
                }
                cell.imageViewTipoPedido.image = UIImage(named: "iconPedidoNew")
                cell.labelTipoPedido.text = "Pedido"
            }
            
            if let fidelity = voucher.fidelity {
                if let unity = UnityPersistence.getUnityByID("\(fidelity.idUnity)") {
                    if let logo = unity.getLogo() {
                        cell.imageViewLogo.addImage(logo.value)
                    }
                    
                    cell.labelNameUnity.text = unity.name
                    
                }
                cell.imageViewTipoPedido.image = UIImage(named: "iconFidelidadeNew")
                cell.labelTipoPedido.text = "Fidelidade"
            }
            
            
            if self.isListUsed {
                cell.viewLeft.backgroundColor = UIColor.gray4all()
            } else {
                cell.viewLeft.backgroundColor = UIColor.green4all()
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension NewListVoucherTableView: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let controller = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherDetail") as! NewVoucherDetail
        
        let voucher = self.listVouchers[indexPath.row]
        controller.voucher = voucher
        
        let deviceBounds = UIScreen.mainScreen().bounds
        let formSheet = MZFormSheetController.init(viewController: controller)
        formSheet.shouldDismissOnBackgroundViewTap = false;
        formSheet.cornerRadius = 8.0;
        formSheet.portraitTopInset = 6.0;
        formSheet.landscapeTopInset = 6.0;
        formSheet.presentedFormSheetSize = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height - 40);
        formSheet.formSheetWindow.transparentTouchEnabled = false;
        formSheet.transitionStyle = .Fade
        
        formSheet.presentAnimated(true) { (viewController: UIViewController!) in
            
        }
    }
}
