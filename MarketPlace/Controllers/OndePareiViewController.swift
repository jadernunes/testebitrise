//
//  OndePareiViewController.swift
//  MarketPlace
//
//  Created by Luciano on 7/27/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class OndePareiViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ScannerQrCodeProtocol, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var buttonSelect         : UIButton!
    @IBOutlet weak var buttonScan           : UIButton!
    @IBOutlet weak var buttonPayOrScan      : UIButton!
    @IBOutlet weak var imagePhoto           : UIImageView!
    @IBOutlet weak var labelTitleBuilding   : UILabel!
    @IBOutlet weak var labelBuilding        : UILabel!
    @IBOutlet weak var labelTitleFloor      : UILabel!
    @IBOutlet weak var labelFloor           : UILabel!
    @IBOutlet weak var labelTitleSpot       : UILabel!
    @IBOutlet weak var labelSpot            : UILabel!
    @IBOutlet weak var labelTitlePlace      : UILabel!
    @IBOutlet weak var pickerBuilding       : UIPickerView!
    @IBOutlet weak var pickerFloor          : UIPickerView!
    @IBOutlet weak var pickerRegion         : UIPickerView!
    @IBOutlet weak var constraintViewPhoto           : NSLayoutConstraint!
    @IBOutlet weak var constraintHeightSelectedPlace : NSLayoutConstraint!
    @IBOutlet weak var constraintBottomViewPicker    : NSLayoutConstraint!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    var selectedBuilding : Building?
    var selectedFloor    : Floor?
    var selectedRegion   : Region?
    var listFloor  = [Floor]()
    var listRegion = [Region]()
    var arrayQrDecoded = [String]()
    let picker = UIImagePickerController()
    var pickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kOndePareiTitle
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        Util.adjustTableViewInsetsFromTarget(self)
        buttonSelect.layer.cornerRadius = 6
        buttonSelect.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonSelect.layer.borderWidth  = 1.0
        buttonSelect.titleLabel?.font   = LayoutManager.primaryFontWithSize(14)
        buttonSelect.setTitleColor(LayoutManager.labelContrastColor, forState: .Normal)
        buttonSelect.setIcon(UIImage(named: "iconFile")!, inLeft: true, margin: 5)
        buttonScan.layer.cornerRadius = 6
        buttonScan.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonScan.layer.borderWidth  = 1.0
        buttonScan.titleLabel?.font   = LayoutManager.primaryFontWithSize(14)
        buttonScan.setTitleColor(LayoutManager.labelContrastColor, forState: .Normal)
        buttonScan.setIcon(UIImage(named: "iconQr")!, inLeft: true, margin: 5)
        
        labelTitlePlace.font = LayoutManager.primaryFontWithSize(14)
        labelTitlePlace.textColor = LayoutManager.labelContrastColor
        
        labelTitleBuilding.font = LayoutManager.primaryFontWithSize(12)
        labelTitleBuilding.textColor = UIColor.lightGrayColor()
        
        labelTitleFloor.font = LayoutManager.primaryFontWithSize(12)
        labelTitleFloor.textColor = UIColor.lightGrayColor()

        labelTitleSpot.font = LayoutManager.primaryFontWithSize(12)
        labelTitleSpot.textColor = UIColor.lightGrayColor()
        
        labelBuilding.font = LayoutManager.primaryFontWithSize(48)
        labelBuilding.textColor = LayoutManager.labelContrastColor
        labelFloor.font = LayoutManager.primaryFontWithSize(48)
        labelFloor.textColor = LayoutManager.labelContrastColor
        labelSpot.font = LayoutManager.primaryFontWithSize(48)
        labelSpot.textColor = LayoutManager.labelContrastColor
        
        buttonPayOrScan.layer.cornerRadius = 6
        buttonPayOrScan.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonPayOrScan.layer.borderWidth  = 1.0
        buttonPayOrScan.titleLabel?.font   = LayoutManager.primaryFontWithSize(14)
        buttonPayOrScan.setTitleColor(LayoutManager.labelContrastColor, forState: .Normal)
        buttonPayOrScan.setIcon(UIImage(named: "iconCamera")!, inLeft: true, margin: 5)
        
        viewPicker.backgroundColor = LayoutManager.backgroundFirstColor
        
        loadCachedImage()
        loadWherePark()
        
        loadPickers()
        
        pickerData = ["---"]
        pickerBuilding.dataSource = self
        pickerBuilding.delegate   = self
        
        pickerFloor.dataSource  = self
        pickerFloor.delegate    = self
        
        pickerRegion.dataSource = self
        pickerRegion.delegate   = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        CartCustomManager.sharedInstance.hideCart()
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenLocationStopCar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func scanOrPay(sender: UIButton) {
        takePhoto()
    }
    
    @IBAction func deletePhoto(sender: UIButton) {
        let fileManager = NSFileManager.defaultManager()
        let tempFilePath = getDocumentsDirectory().stringByAppendingPathComponent("cached.png")
        do {
            try fileManager.removeItemAtPath(tempFilePath)
            
            UIView.animateWithDuration(0.5, animations: {
                self.constraintViewPhoto.constant = 0
                self.scrollView.layoutIfNeeded()
            })
            
            buttonPayOrScan.setTitle(kButtonTitlePhotoOndeParei, forState: .Normal)
            buttonPayOrScan.setIcon(UIImage(named: "iconCamera")!, inLeft: true)
            
            NSUserDefaults.standardUserDefaults().removeObjectForKey("dicWherePark")
            UIView.animateWithDuration(0.5) {
                self.constraintHeightSelectedPlace.constant = 0.0
                self.scrollView.layoutIfNeeded()
            }
            
        } catch {
        }
    }
    
    @IBAction func selectPlace(sender: AnyObject) {
        UIView.animateWithDuration(0.5) { 
            self.constraintBottomViewPicker.constant = 0
            self.scrollView.layoutIfNeeded()
        }
        
    }
    
    @IBAction func confirmPicker(sender: AnyObject) {
        UIView.animateWithDuration(0.5) {
            self.constraintBottomViewPicker.constant = -160
            self.scrollView.layoutIfNeeded()
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let place = NSMutableDictionary()
        if ParkingEntityManager.getBuildings().count > 0 {
            place.setValue(ParkingEntityManager.getBuildings()[pickerBuilding.selectedRowInComponent(0)].name, forKey: "building")
        }else{
            place.setValue("-", forKey: "building")
        }
        
        if listFloor.count > 0 {
            place.setValue(listFloor[pickerFloor.selectedRowInComponent(0)].name, forKey: "floor")
        }else{
            place.setValue("-", forKey: "floor")
        }
        
        if listRegion.count > 0 {
            place.setValue(listRegion[pickerRegion.selectedRowInComponent(0)].name, forKey: "region")
        }else{
            place.setValue("-", forKey: "region")
        }
        
        defaults.setObject(place, forKey: "dicWherePark")
        
        loadWherePark()
    }
    
    @IBAction func cancelPicker(sender: AnyObject) {
        UIView.animateWithDuration(0.5) {
            self.constraintBottomViewPicker.constant = -160
            self.view.layoutIfNeeded()
        }
    }

    @IBAction func scanNewPhoto(sender: UIButton) {
        takePhoto()
    }
    
    @IBAction func scanBarcode(sender: UIButton) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
        (vc.viewControllers[0] as! ScannerViewController).delegate = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    //MARK: - Methods
    func loadPickers(){
        if selectedBuilding == nil && ParkingEntityManager.getBuildings().count > 0 {
            selectedBuilding = ParkingEntityManager.getBuildings()[0]
            listFloor = ParkingEntityManager.getFloors(selectedBuilding!.id)
        }
        
        if selectedFloor == nil && selectedBuilding != nil &&  listFloor.count > 0{
            selectedFloor = listFloor[0]
            listRegion = ParkingEntityManager.getRegions(selectedFloor!.id)
        }
        
        if selectedRegion == nil && selectedFloor != nil && listRegion.count > 0 {
            selectedRegion = listRegion[0]
        }
    }
    
    func takePhoto() -> Void{
        picker.delegate = self;
        picker.allowsEditing = true
        picker.sourceType = .Camera
        picker.modalPresentationStyle = .FullScreen
        self.navigationController!.presentViewController(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        picker.dismissViewControllerAnimated(true) { 
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                self.imagePhoto.image = image
                
                self.saveImage(image)
                UIView.animateWithDuration(0.5, animations: {
                    self.constraintViewPhoto.constant = 190
                    self.scrollView.layoutIfNeeded()
                    self.buttonPayOrScan.setTitle(kButtonTitlePayParkOndeParei, forState: .Normal)
                    self.buttonPayOrScan.setIcon(UIImage(named: "iconCash")!, inLeft: true)
                })
            }
        }
    }
    
    func loadCachedImage(){
        let filename = getDocumentsDirectory().stringByAppendingPathComponent("cached.png")
        
        if let image = UIImage(contentsOfFile: filename) {
            self.imagePhoto.image = image
            self.constraintViewPhoto.constant = 190
            buttonPayOrScan.setTitle(kButtonTitlePayParkOndeParei, forState: .Normal)
            buttonPayOrScan.setIcon(UIImage(named: "iconCash")!, inLeft: true)
        }
    }
    
    func saveImage (image: UIImage) -> Bool{
        var result = false
        
        if let data = UIImagePNGRepresentation(image) {
            let filename = getDocumentsDirectory().stringByAppendingPathComponent("cached.png")
            result = data.writeToFile(filename, atomically: true)
            
        }
        
        return result
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func didScanInvalidCode() {
        Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
    }

    func didScanValidCode(value: String) {
        arrayQrDecoded = []
        let defaults = NSUserDefaults.standardUserDefaults()
        var qrToBeDecoded = value
        qrToBeDecoded.removeAtIndex(qrToBeDecoded.startIndex.advancedBy(3))
        if let qrDecoded = qrToBeDecoded.fromBase64() {
            arrayQrDecoded = qrDecoded.componentsSeparatedByString("_")
        }
        
        if arrayQrDecoded.count > 3 && arrayQrDecoded.contains("PRK") && arrayQrDecoded[0] == ID_MARKETPLACE {
            let dict = ["building":"\(arrayQrDecoded[2])",
                        "floor":"\(arrayQrDecoded[3])",
                        "region":"\(arrayQrDecoded[4])"]
            
            defaults.setObject(dict, forKey: "dicWherePark")
            defaults.synchronize()
            loadWherePark()
        }
        
        else {
            Util.showAlert(self, title: kAlertTitle, message: kMessageInvalidQR)
        }

    }
    
    func loadWherePark() {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let arrItems = defaults.objectForKey("dicWherePark") as? Dictionary <String, String>
        if arrItems != nil && arrItems?.keys.count > 2 {
            labelBuilding.text = arrItems!["building"]
            labelFloor.text    = arrItems!["floor"]
            labelSpot.text     = arrItems!["region"]
            
            UIView.animateWithDuration(0.5) {
                self.constraintHeightSelectedPlace.constant = 158
            }
        }else{
            UIView.animateWithDuration(0.5) {
                self.constraintHeightSelectedPlace.constant = 0.0
            }
        }
        
        self.scrollView.layoutIfNeeded()

    }
    
    
    //MARK: UIPickerViewDelegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerBuilding {
            
            return ParkingEntityManager.getBuildings().count
            
        }else if pickerView == pickerFloor {
            return listFloor.count
        }else if pickerView == pickerRegion {
            return listRegion.count
        }
        
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBuilding {
            if ParkingEntityManager.getBuildings().count > 0 {
                selectedBuilding = ParkingEntityManager.getBuildings()[row]
                
                //update lists
                listFloor   = ParkingEntityManager.getFloors(selectedBuilding!.id)
                
                if listFloor.count > 0 {
                    listRegion  = ParkingEntityManager.getRegions(listFloor[0].id)
                }else{
                    listRegion  = [Region]()
                }
                
                pickerFloor.reloadAllComponents()
                pickerRegion.reloadAllComponents()
            }else{
                
            }
        }
        
        if pickerView == pickerFloor {
            
            listFloor = ParkingEntityManager.getFloors(selectedBuilding!.id)
            
            if listFloor.count > 0 {
                selectedFloor = listFloor[row]
                
                listRegion = ParkingEntityManager.getRegions(selectedFloor!.id)
                
                if listRegion.count > 0 {
                    selectedRegion = listRegion[0]
                    //Update spaces on region changes
                    
                }else{
                    selectedRegion = nil
                }

            }
          
            pickerRegion.reloadAllComponents()
        }
        
        if pickerView == pickerRegion {
            listRegion = ParkingEntityManager.getRegions(selectedFloor!.id)
            
            if listRegion.count > 0 {
                if row < listRegion.count {
                    selectedRegion = listRegion[row]
                }else{
                    selectedRegion = listRegion[0]
                }
                //Update spaces on region changes
                
            }else{
                selectedRegion = nil
            }
            
        }
    }

    
    // The data to return for the row and component (column) that's being passed in
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        var label = view as? UILabel
        
        if (label == nil){
            label = UILabel(frame: CGRectMake(0, 0, pickerView.bounds.size.width, 30))
            label?.minimumScaleFactor = 0.5
            label?.textColor = LayoutManager.labelContrastColor
            label?.font = LayoutManager.primaryFontWithSize(12)
            label?.textAlignment = .Center
        }
        
        var string = ""
        
        if pickerView == pickerBuilding {
        
            string = ParkingEntityManager.getBuildings()[row].name
            
        }else if pickerView == pickerFloor {
            
            string = listFloor[row].name
            
        }else if pickerView == pickerRegion {
            
            if selectedFloor != nil && listRegion.count > 0 {
                string = listRegion[row].name
            }else {
                string = "---"
            }
        }
        
        label!.text = string
        
        return label!
    }
    
}

//MARK: - Protocols
protocol ScannerQrCodeProtocol {
    func didScanValidCode(value : String) -> Void
    func didScanInvalidCode() -> Void
}
