//
//  BuyTicketsEventViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 10/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class BuyTicketsEventViewController : UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var labelEventSubdescription:    UILabel!
    @IBOutlet weak var labelEventDescription:       UILabel!
    @IBOutlet weak var labelDate:                   UILabel!
    @IBOutlet weak var labelTitle:                  UILabel!
    @IBOutlet weak var labelTitleDate:              UILabel!
    @IBOutlet weak var labelTitleEvent:             UILabel!
    
    @IBOutlet weak var componentPayment:    UIView!
    private var component:                  ComponentViewController!
    
    private var ticketPrice             = 0.0
    private var shouldShowPrice         = false
    private var amountSelected          = 0
    private var itemsInCollectionView   = 0
    private var idTicket                = 0
    private var ticketsAvailable        = 0
    private var ticketsSold             = 0
    
    var fact:       Fact!
    var ticketId:   CLong?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Util.adjustTableViewInsetsFromTarget(self)
        configureUI()
        
        collectionView.registerNib(UINib(nibName: "TicketCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CELL_COLLECTION_TICKET)
        
        dispatch_async(dispatch_get_main_queue(), {
            self.loadTicket(byId: self.ticketId!)
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        shouldShowPrice = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureUI() {
        title = fact?.title
        view.backgroundColor = LayoutManager.backgroundFirstColor
        
        collectionView.backgroundColor = UIColor.clearColor()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: UIScreen.mainScreen()
            .bounds.width/4, height: UIScreen.mainScreen().bounds.width/4)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        
        labelEventDescription.text      = fact!.title
        labelEventDescription.font      = LayoutManager.primaryFontWithSize(15)
        labelEventDescription.textColor = LayoutManager.labelFirstColor
        
        labelEventSubdescription.text       = fact!.desc
        labelEventSubdescription.font       = LayoutManager.primaryFontWithSize(15)
        labelEventSubdescription.textColor  = LayoutManager.labelFirstColor
        
        labelDate.text = fact!.getFormatedDateString(true, pattern: "dd/MM/YYYY") +
            " às \(fact!.getFormatedDateString(true, pattern: "HH:mm"))hs".uppercaseString
        labelDate.font = LayoutManager.primaryFontWithSize(15)
        labelDate.textColor = LayoutManager.labelFirstColor
        
        labelTitle.font         = LayoutManager.primaryFontWithSize(20)
        labelTitle.textColor    = LayoutManager.labelFirstColor
        
        labelTitleEvent.font        = LayoutManager.primaryFontWithSize(20)
        labelTitleEvent.textColor   = LayoutManager.labelFirstColor
        
        labelTitleDate.font         = LayoutManager.primaryFontWithSize(20)
        labelTitleDate.textColor    = LayoutManager.labelFirstColor
    }
    
    func preShowWithFact(byId id: String) -> Bool {
        if let fact = FactEntityManager.getFactByID(id) {
            self.fact = fact
            return true
        }
        return false
    }
    
    func loadTicket(byId id: CLong) {
        if let ticketId = self.ticketId {
            TicketEntityManager.getTicketBy(itsId: self.ticketId!) { (ticket) in
                self.ticketPrice = Double(ticket!.price) / 100.0
                self.ticketId = CLong(ticketId)
                self.ticketsAvailable = ticket!.ticketLimit
                self.ticketsSold = ticket!.ticketSold
                self.updateViews()
            }
        }
    }
    
    func configurePaymentComponent() {
        dispatch_async(dispatch_get_main_queue()) {
            self.component = ComponentViewController()
            self.component.delegate = self
            
            self.component.buttonTitleWhenNotLogged = kEnter
            self.component.buttonTitleWhenLogged    = kPay
            self.component.view.frame = self.componentPayment.bounds
            self.component.view.backgroundColor = LayoutManager.backgroundFirstColor
            
            self.componentPayment.addSubview(self.component.view)
            self.addChildViewController(self.component)
            self.component.didMoveToParentViewController(self)
        }
    }
    
    func updateViews() {
        itemsInCollectionView = (ticketsAvailable-ticketsSold) == 0 ? 0 : (ticketsAvailable-ticketsSold) == 1 ? 1 : (ticketsAvailable-ticketsSold) == 2 ? 2 : MAX_TICKETS_EVENT
        if itemsInCollectionView > 0 {
            configurePaymentComponent()
            collectionView.reloadData()
        }
        else {
            let errorAlert = UIAlertController(title: kAlertTitle, message: kMessageNoTicketsAvailable, preferredStyle: UIAlertControllerStyle.Alert)
            
            errorAlert.addAction(UIAlertAction(title: kAlertButtonOk, style: .Cancel, handler: { (action: UIAlertAction!) in
                self.dismissViewControllerAnimated(true, completion: nil)
                self.navigationController?.popViewControllerAnimated(true)
            }))
            self.presentViewController(errorAlert, animated: true, completion: nil)
        }
    }
}

extension BuyTicketsEventViewController : UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsInCollectionView;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_TICKET, forIndexPath: indexPath) as! TicketCollectionViewCell
        cell.frame.size.width = UIScreen.mainScreen().bounds.width/4
        cell.frame.size.height = UIScreen.mainScreen().bounds.width/4
        return cell
    }
}

extension BuyTicketsEventViewController : UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collectionView.frame.size.width/3.5, collectionView.frame.size.height/3.5)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let customCell                          = cell as! TicketCollectionViewCell
        customCell.labelQuantity.text           = "\(indexPath.row + 1)"
        
        let price                               = ticketPrice * Double(indexPath.row  + 1)
        customCell.labelPrice.text              = Util.formatCurrency(price)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).backgroundColor = SessionManager.sharedInstance.cardColor
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelQuantity.textColor = LayoutManager.white
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelPrice.textColor = LayoutManager.white
        amountSelected = indexPath.row + 1
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).backgroundColor = UIColor.clearColor()
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelQuantity.textColor = SessionManager.sharedInstance.cardColor
        (collectionView.cellForItemAtIndexPath(indexPath) as! TicketCollectionViewCell).labelPrice.textColor = SessionManager.sharedInstance.cardColor
    }
}

extension BuyTicketsEventViewController : LoginDelegate {
    
    func callbackLogin(sessionToken: String!, email: String!, phone: String!) {
        
    }
    
    func callbackPreVenda(sessionToken: String!, cardId: String!, paymentMode: PaymentMode) {
        if amountSelected != 0 {
            
            let loadingVc = LoadingViewController()
            loadingVc.startLoading(self,title: kWait)
            
            let api = ApiServices()
            
            api.successCase = { (obj) in
                if (obj as? NSArray) != nil {
                    loadingVc.finishLoading(nil)
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc = mainStoryboard.instantiateViewControllerWithIdentifier("comprovante") as! UINavigationController
                    let success = self.amountSelected == 1 ? kMessageSuccessBuyOneTicket : kMessageSuccessBuyMultipleTickets
                    (vc.viewControllers[0] as! ReceiptViewController).labels = ["success":  success,
                                                                                "date":     NSDate.getDateTodayToShortYearFormat(),
                                                                                "time":     NSDate.getTimeTodayToFormat(),
                                                                                "title":    kPaymentConfirmedTitle,
                                                                                "type":     1,
                                                                                "idFact":   (self.fact?.id)!,
                                                                                "price":    Double(self.amountSelected) * self.ticketPrice]
                    vc.loadView()
                    (vc.viewControllers[0] as! ReceiptViewController).previousVC = self
                    self.presentViewController(vc, animated: true, completion: nil)
                }
                else {
                    /// Can't do the payment
                }
            }
            
            api.failureCase = { (msg) in
                loadingVc.finishLoading(nil)
                let alert = UIAlertController(title: "Erro", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            
            let customerInfo : Dictionary<String, AnyObject> = ["sessionToken": sessionToken,"cardId": cardId,"paymentMode": paymentMode.rawValue]
            let parameters = ["numberOfTickets": amountSelected, "amount": Double(amountSelected) * ticketPrice * 100, "customerInfo": customerInfo]
            
            api.eventTicketPayment(Int(ticketId!), dictionaryParameters:parameters)
        }
        else {
            Util.showAlert(self, title: kAlertTitle, message: kMessageChooseNumberOfTickets)
        }
    }
}
