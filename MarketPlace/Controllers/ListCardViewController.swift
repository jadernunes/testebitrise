//
//  ListCardViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ListCardViewController: MZFormSheetController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var listCards: NSMutableArray?
    var cardSelected: NSDictionary?
    var handlerViewController: NewCardViewController?
    var citySelected: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.listCards = NSMutableArray()
        let catalogDictionary = MobilityFilesUtil.loadFromFile("mobilityCatalog")
        let issuers = catalogDictionary["issuers"] as! NSArray
        let issuersId = self.citySelected!["issuers"]
        
        for issuer in issuers {
            if (issuersId!.containsObject(issuer["issuer_id"]!)) {
                self.listCards?.addObject(issuer)
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //UIViewController *viewControllerDelegate = (UIViewController *)self.handlerViewController;
        //[MBProgressHUD hideHUDForView:viewControllerDelegate.view animated:YES];
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCards!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath) as! ListCardTableViewCell
        cell.iconSelected.image = nil
        
        let dictionaryCard = self.listCards![indexPath.row] as! NSDictionary
        cell.populateData(dictionaryCard)
        
        if(dictionaryCard["issuer_id"] as? String == self.cardSelected?["issuer_id"] as? String) {
            cell.markSelected()
        }
        return cell;

    }
    
    @IBAction func buttonClosePressed(sender: AnyObject) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (presentedFSViewController) in
            
        })
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (presentedFSViewController) in
            let dictionaryCity = self.listCards![indexPath.row] as! NSDictionary
            self.handlerViewController!.onSelectCardSuccess(dictionaryCity)
        })

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
