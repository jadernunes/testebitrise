//
//  NewCardViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewCardViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var txCard: UITextField!
    @IBOutlet weak var txCity: UITextField!
    @IBOutlet weak var txNumber: UITextField!
    
    
    @IBOutlet weak var viewCard: UIView!
    
    var activeField: UITextField?
    let refreshControl: UIRefreshControl = UIRefreshControl()
    var arrayCities: NSMutableArray?
    var selectedCity: NSDictionary?
    var arrayIssuers: NSMutableArray?
    var selectedIssuer: NSDictionary?
    var nrCardDigits: Int = 0
    var cityIdSelected: Int = -1
    let keyIssuerId = "issuerId"
    //var fieldMask: String?
    
    //4all lib
    var vcLoading: LoadingViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SessionManagerMobility.sharedInstance.customizeViewWithTitle("Novo cartão", vc: self)
        
        let buttonClose = UIBarButtonItem(title: "Fechar", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(NewCardViewController.didPressCloseButton))
        self.navigationItem.setLeftBarButtonItem(buttonClose, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        self.scroll.scrollEnabled = true
        self.selectedCity = nil
        self.selectedIssuer = nil
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "OK", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(NewCardViewController.saveIssuer(_:)))
        
        self.txCity.rightViewMode = UITextFieldViewMode.Always
        self.txCity.rightView = UIImageView.init(image: UIImage.init(named: "iconArrowDown"))
        self.txCity.layer.cornerRadius = 5.0
        self.txCity.layer.masksToBounds = true
        let padding = CGRect(x: 0, y: 0, width: 10, height: 0)
        self.txCity.leftView = UIView(frame: padding)
        self.txCity.leftViewMode = .Always
        
        self.txCard.rightViewMode = UITextFieldViewMode.Always
        self.txCard.rightView = UIImageView.init(image: UIImage.init(named: "iconArrowDown"))
        //self.txCard.rightView!.frame.origin.x = (self.txCard.rightView?.frame.origin.x)! - 20
        self.txCard.layer.cornerRadius = 5.0
        self.txCard.layer.masksToBounds = true
        self.txCard.leftView = UIView(frame: padding)
        self.txCard.leftViewMode = .Always
        self.txCard.enabled = false
        
        self.txNumber.delegate = self
        self.txNumber.layer.cornerRadius = 5.0
        self.txNumber.layer.masksToBounds = true
        self.txNumber.leftView = UIView(frame: padding)
        self.txNumber.leftViewMode = .Always
        self.txNumber.enabled = false
        self.txNumber.textAlignment = .Left
        
        let tapGestureInScroll = UITapGestureRecognizer.init(target: self, action: #selector(NewCardViewController.hideKeyboard))
        self.scroll.addGestureRecognizer(tapGestureInScroll)
        
        self.populateIssuers()
        
        self.startLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewCardViewController.keyboardWasShown), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewCardViewController.keyboardWillBeHidden), name: UIKeyboardDidHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewCardViewController.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didPressCloseButton(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func keyboardWasShown(aNotification: NSNotification){
        
        let info = aNotification.userInfo
        let kbSize: CGSize = (info![UIKeyboardFrameBeginUserInfoKey]!.CGRectValue).size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        self.scroll.contentInset = contentInsets
        self.scroll.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize.height
        if let activeTxtField = self.activeField {
            if (!CGRectContainsPoint(aRect, activeTxtField.frame.origin) ) {
                let scrollPoint: CGPoint = CGPointMake(0.0, activeTxtField.frame.origin.y-kbSize.height);
                self.scroll.setContentOffset(scrollPoint, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(){
        self.scroll.contentInset = UIEdgeInsetsZero
        self.scroll.scrollIndicatorInsets = UIEdgeInsetsZero;
        self.scroll.setContentOffset(CGPointMake(0.0, 0.0), animated: true)
    }
    
    func startLoad(){
        self.refreshControl.beginRefreshing()
    }
    
    func stopLoad(){
        self.refreshControl.endRefreshing()
    }
    
    func populateIssuers() {
        
        let dicCatalog = MobilityFilesUtil.loadFromFile("mobilityCatalog")
        
        if (dicCatalog.count != 0) {
            self.arrayCities = dicCatalog["cities"] as? NSMutableArray
        }
    }
    
    func hideKeyboard(){
        self.hideKeyboardByView(self.view)
    }
    
    func hideKeyboardByView(view: UIView) {
        for subView in view.subviews {
            if (subView.isKindOfClass(UITextField)) {
                subView.resignFirstResponder()
            } else {
                self.hideKeyboardByView(subView)
            }
        }
    }
    
    func keyboardWillShow(){
        
        let mobilityStoryboard = UIStoryboard.init(name: "Mobility", bundle: nil)
        var formSheet: MZFormSheetController?
        let deviceBounds = UIScreen.mainScreen().bounds
        let sizeScreenOptions = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height - 40.0)
        
        if (self.txCard.editing || self.txCity.editing) {
            if(self.txCity.editing) {
                let controller = mobilityStoryboard.instantiateViewControllerWithIdentifier("ListCityTableViewController") as! ListCityViewController
                controller.handlerViewController = self
                controller.citySelected = selectedCity
                
                formSheet = MZFormSheetController.init(viewController: controller)
            } else if(self.txCard.editing) {
                let controller = mobilityStoryboard.instantiateViewControllerWithIdentifier("ListCardsTableViewController") as! ListCardViewController
                controller.handlerViewController = self
                controller.cardSelected = selectedIssuer
                controller.citySelected = selectedCity;
                formSheet = MZFormSheetController.init(viewController: controller)
            }
            
            self.hideKeyboard()
            
            //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            formSheet!.shouldDismissOnBackgroundViewTap = false;
            formSheet!.cornerRadius = 8.0
            formSheet!.portraitTopInset = 6.0
            formSheet!.landscapeTopInset = 6.0
            formSheet!.presentedFormSheetSize = sizeScreenOptions
            formSheet!.formSheetWindow.transparentTouchEnabled = false
            formSheet!.transitionStyle = .Fade
            
            dispatch_async(dispatch_get_main_queue(), { 
                formSheet!.presentAnimated(true, completionHandler: { (presentedViewController) in
                    
                })
            })
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.activeField = nil
    }
    
    @IBAction func saveIssuer(sender: AnyObject){
        
        // validações
        var msg = ""
        var field = UITextField()
        
        if (selectedIssuer == nil) {
            msg = "Por favor, escolha qual o seu cartão :)"
            field = self.txCard
            
        } else  {
            if (self.txNumber.text!.characters.count != nrCardDigits) {
                msg = "Por favor, revise o número do seu cartão :)"
                field = self.txNumber
            }
        }
        
        if (msg != "") {
            let alert = UIAlertView.init(title: "Novo Cartão", message: msg, delegate: self, cancelButtonTitle: "OK")
            alert.show()
            field.becomeFirstResponder()
            return
        }
        
        //let dicIssuer = ["issuer_nr": self.txNumber.text!, "issuer_id": selectedIssuer![keyIssuerId]!] as NSDictionary
        
        //TODO:
        let api = ApiServicesMobility()
        
        api.successCase = { (obj) in
            if let response = obj as? NSDictionary {
                self.receiveCheckFromServer(response)
                SessionManagerMobility.sharedInstance.needUpdateList = true
            }
        }
        
        api.failureCase = { (cod, msg) in
            //[MBProgressHUD hideHUDForView:self.view animated:YES];
            self.vcLoading?.finishLoading(nil)
            let alert = UIAlertView.init(title: "Novo Cartão", message: "Não foi possível completar a operação. Por favor, tente novamente.", delegate: self, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
        var sessionToken = ""
        if (Lib4all.sharedInstance().hasUserLogged()) {
            sessionToken = (Lib4all.sharedInstance().getAccountData()["sessionToken"] as? String)!
        }
        
        self.vcLoading = LoadingViewController()
        self.vcLoading?.startLoading(self, title: "Aguarde...")
        
        api.consultaSaldoECartaoValido(self.txNumber.text, andIssuerId: "\(selectedIssuer!["issuer_id"] as! Int)", andCityID: "\(selectedCity!["cityId"] as! Int)", andSessionToken: sessionToken)
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    func receiveCheckFromServer(response: NSDictionary){
        
        let isValid = response["valid"] as? Bool
        
        if ((isValid) != nil && isValid!) {
            
            if let cardBalances = response["cardBalances"] as? NSArray {
                if (cardBalances.count > 0) {
                    let cardbal: NSDictionary = cardBalances.firstObject as! NSDictionary
                    //cardbal.setValue(1, forKey: "checkBalance")
                    //cardbal.setValue(selectedCity!["cityId"] as! Int, forKey: "cityId")
                    let card = CardModel(value: cardbal)
                    card.checkBalance = true
                    card.thumb = "http://i.imgur.com/\((selectedIssuer!["thumb"])!).png"
                    print(card.thumb)
                    card.cityId = selectedCity!["cityId"] as! Int
                    let stored:Bool = CardModelPersistence.storeCardIfNotExists(card)
                    
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    if (!stored) {
                        vcLoading?.finishLoading(nil)
                        let alert = UIAlertView.init(title: "Verificação do cartão", message: "Você já cadastrou esse cartão.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    }else{
                        self.vcLoading!.finishLoading({
                            let alert = UIAlertView.init(title: "Sucesso", message: "Cartão adicionado com sucesso!", delegate: nil, cancelButtonTitle: "OK")
                            alert.show()
                            self.dismissViewControllerAnimated(true, completion:nil)
                        })

                        
                    }
                }
            } else {
                
                let api = ApiServicesMobility()
                
                api.successCase = { (obj) in
                    if let response = obj as? NSArray {
                        if (response.count > 0) {
                            let cardInfo = response[0] as! NSDictionary
                            let card = CardModel()
                            card.issuerNr = self.txNumber.text!
                            card.issuerId = self.selectedIssuer!["issuer_id"] as! Int
                            card.cityId = self.selectedCity!["cityId"] as!Int
                            card.productId = cardInfo["id"] as! Int
                            card.productName = cardInfo["name"] as! String
                            card.thumb = cardInfo["thumb"] as! String
                            print(card.thumb)
                            card.rechargeable = cardInfo["rechargeable"] as! Bool
                            
                            let stored:Bool = CardModelPersistence.storeCardIfNotExists(card)
                            
                            if (!stored) {
                                self.vcLoading?.finishLoading({
                                    let alert = UIAlertView.init(title: "Verificação do cartão", message: "Você já cadastrou esse cartão.", delegate: nil, cancelButtonTitle: "OK")
                                    alert.show()
                                })
                            }else{
                                self.vcLoading?.finishLoading({
                                    let alert = UIAlertView.init(title: "Sucesso", message: "Cartão adicionado com sucesso!", delegate: nil, cancelButtonTitle: "OK")
                                    alert.show()
                                    self.dismissViewControllerAnimated(true, completion:nil)
                                })
                                
                            }
                        }
                    }
                    
                }
                
                api.failureCase = { (cod, msg) in
                    // [MBProgressHUD hideHUDForView:self.view animated:YES];
                    self.vcLoading?.finishLoading({
                        let alert = UIAlertView.init(title: "Atenção", message: "Houve uma falha na comunicação.\nTente novamente.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    })
                }
                
                //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                api.buscaSemCheckBalance(selectedIssuer!["issuer_id"] as? String)
                
            }
            
        } else {
            //[MBProgressHUD hideHUDForView:self.view animated:YES];
            self.vcLoading?.finishLoading({
                let alert = UIAlertView.init(title: "Atenção", message: "Cartão inválido.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            })
        }
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    func onSelectCitySuccess(dic: NSDictionary){
        self.selectedCity = dic
        self.txCity.text = self.selectedCity!["name"] as? String
        
        //Reset fields
        selectedIssuer = nil
        self.txCard.enabled = true
        self.txCard.text = "Selecione o cartão"
        nrCardDigits = 0
        txNumber.text = nil
        self.txNumber.placeholder = "Digite o número do cartão"
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    func onSelectCardSuccess(dic: NSDictionary){
        self.selectedIssuer = dic
        self.txCard.text = self.selectedIssuer!["name"] as? String
        self.txNumber.enabled = true
        txNumber.text = nil
        nrCardDigits = (self.selectedIssuer!["nr_digits"] as? Int)!
        
        if let fieldMask = self.selectedIssuer!["mask"] as? String {
            //self.txNumber.setMask("{dddd}-{DDDD}-{WaWa}-{aaaa}", withMaskTemplate: fieldMask.stringByReplacingOccurrencesOfString("X", withString: "d"))
            self.txNumber.placeholder = fieldMask
            self.txNumber.delegate = self
        }
        
        
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
