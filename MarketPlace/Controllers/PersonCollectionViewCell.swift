//
//  ScheduleCollectionViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 8/16/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class PersonCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var thumbProfile: UIImageView!
    @IBOutlet weak var nameProfile: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.thumbProfile.layer.masksToBounds = true
        self.thumbProfile.layer.cornerRadius = 30
    }
    
    override func preferredLayoutAttributesFittingAttributes(layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes
    {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFittingSize(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        newFrame.size.width = CGFloat(ceilf(Float(size.width)))
        layoutAttributes.frame = newFrame
        
        return layoutAttributes
    }
}