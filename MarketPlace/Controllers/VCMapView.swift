//
//  VCMapView.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 31/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import MapKit

extension UnityMapViewController : MKMapViewDelegate, CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        uLocation = userLocation
        
        if firstLoad {
            self.trackUser(self)
            firstLoad = false
        }
        
    }
    
    func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            manager.startUpdatingLocation()
        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation.isKindOfClass(UnityAnntation)) {
            mapView.translatesAutoresizingMaskIntoConstraints = false
            let reuseId = "UnityAnntation"
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as MKAnnotationView!
            if (annotationView == nil) {
                annotationView = UnityAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                // Resize image
                annotationView.image = Util.resizeImageNamed("iconLocation", size: kSizePinAnnotationView)
                // calloutview
                let calloutImage = Util.resizeImageNamed("iconDisclosure", size: CGSizeMake(15, 20))
                let btnDisclousure = UIButton.init(type: .Custom)
                btnDisclousure.tintButton(LayoutManager.green4all, image: calloutImage)
                btnDisclousure.frame = CGRectMake(0, 0, 32, 32);
                annotationView.rightCalloutAccessoryView = btnDisclousure
                annotationView.canShowCallout = true
            } else {
                annotationView.annotation = annotation;
            }
            let annot = annotation as! UnityAnntation
            self.updateNearestUnity(annot.unity)
            return annotationView
        } else {
            return nil
        }
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {        
        if let unityAnnotation = view.annotation as? UnityAnntation {
            let object = unityAnnotation.unity
            SessionManager.sharedInstance.cardColor = LayoutManager.green4all

            let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
            detailsVc.title = object.name
            detailsVc.idObjectReceived = object.id
            detailsVc.typeObjet = TypeObjectDetail.Store
            detailsVc.hidesBottomBarWhenPushed = true
            self.showNavBar()
            detailsVc.title = object.name
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.calloutTapped(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    func calloutTapped(sender:UITapGestureRecognizer) {
        guard let annotation = (sender.view as? UnityAnnotationView)?.annotation as? UnityAnntation else { return }
        let object = annotation.unity
        SessionManager.sharedInstance.cardColor = LayoutManager.green4all
        SessionManager.sharedInstance.searchOrQrEnabled = true
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        detailsVc.title = object.name
        detailsVc.idObjectReceived = object.id
        detailsVc.typeObjet = TypeObjectDetail.Store
        detailsVc.hidesBottomBarWhenPushed = true
        self.showNavBar()
        detailsVc.title = object.name
        self.navigationController?.pushViewController(detailsVc, animated: true)
        
        //remove o recognizer para que ao tocar diretamente no pin, não vá para o detalhe
        for recognizer in view.gestureRecognizers ?? [] {
            view.removeGestureRecognizer(recognizer)
        }
    }
    
}
