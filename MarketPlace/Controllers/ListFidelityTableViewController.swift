//
//  ListFidelityTableViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 17/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ListFidelityTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableViewListFidelity: UITableView!
    
    let api = ApiServices()
    var pullRefresh: UIRefreshControl!
    var listFidelities: Results<MyFidelity>?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = kMyFidelitiesTitle
        self.view.backgroundColor = LayoutManager.backgroundFirstColor
        self.tableViewListFidelity.backgroundColor = LayoutManager.backgroundFirstColor
        Util.adjustTableViewInsetsFromTarget(self)
        
        pullRefresh = UIRefreshControl()
        pullRefresh.attributedTitle = NSAttributedString(string: kPullToRefresh)
        pullRefresh.addTarget(self, action: #selector(self.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableViewListFidelity.addSubview(pullRefresh)
        
        //get local data
        self.listFidelities = MyFidelityPersistence.getAllLocalFidelitys()
        self.refresh()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMyFidelities)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.checkWithoutData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Check data
    private func checkWithoutData(){
        Util.checkWithoutData(self.view, tableView: self.tableViewListFidelity, countData: listFidelities?.count, message: kMessageWithoutDataFidelities)
    }
    
    //MARK: - Refresh to get all data from server
    func refresh() {
        self.getMyFidelitiesFormServer()
    }
    
    //MARK: - get all data from server
    private func getMyFidelitiesFormServer(){
        
        api.successCase = {(obj) in
    
            self.listFidelities = MyFidelityPersistence.getAllLocalFidelitys()
            self.tableViewListFidelity.reloadData()
            self.pullRefresh?.endRefreshing()
            
            self.checkWithoutData()
        }
        
        api.failureCase = {(msg) in
            self.pullRefresh?.endRefreshing()
        }
        
        //Get token by user loged
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    api.getMyFidelities(user.token)
                }
            }
        }
    }
    
    //MARK: - TableView methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listFidelities!.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let fidelity = self.listFidelities![indexPath.row]
        let countItems = fidelity.maxStamp
        
        var valueToAddToPadding : CGFloat = 00.0
        
        let deviceModel = UIDevice().getDeviceModel()
        
        if deviceModel == .iPhone5      ||
            deviceModel == .iPhone5s    ||
            deviceModel == .iPhone5c    ||
            deviceModel == .iPhoneSE
        {
            valueToAddToPadding = 20.00
        }
        
        let padding      = UIScreen.mainScreen().bounds.size.width / 6 + valueToAddToPadding
        let numberOfRows = ceil(Double(countItems) / 6)
        
        //calculate estimated cellRow
        //Get screen width less estimated margins
        let estimatedRow = ((UIScreen.mainScreen().bounds.size.width - 23) / 6 + 13)
        
        let sectionHeight =  CGFloat(numberOfRows * Double(estimatedRow))
        let newHeight = (sectionHeight + CGFloat(padding))
        
        return newHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: ListFidelityTableViewCell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_FIDELITIES, forIndexPath: indexPath) as! ListFidelityTableViewCell
        
        let fidelity = self.listFidelities![indexPath.row]
        var stamp = ""
        if fidelity.stampThumb != nil {
            stamp = fidelity.stampThumb!
        }
        
        cell.populateData(fidelity.maxStamp, countItems: fidelity.stampCount, urlImage: stamp, descriptionTerms: fidelity.terms!, shortDesc: fidelity.descriptionFidelity!)
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
}
