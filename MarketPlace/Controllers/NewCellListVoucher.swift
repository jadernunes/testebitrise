//
//  NewCellListVoucer.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewCellListVoucher: UITableViewCell {

    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelNameUnity: UILabel!
    @IBOutlet weak var labelTipoPedido: UILabel!
    @IBOutlet weak var imageViewTipoPedido: UIImageView!
    @IBOutlet weak var imageViewDiscloserIndicator: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBase.layer.cornerRadius = 3
        self.viewBase.layer.masksToBounds = true
        self.viewBase.layer.borderWidth = 1.0
        self.viewBase.layer.borderColor = UIColor.lightGray4all().CGColor
        self.labelTipoPedido.textColor = UIColor.gray4all().colorWithAlphaComponent(0.5)
        self.imageViewLogo.layer.cornerRadius = 8
        self.imageViewLogo.layer.masksToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
