//
//  NewListTicketTableView.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 27/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewListTicketTableView: UIViewController {
    
    @IBOutlet weak var tableViewVoucher: UITableView!
    var isListUsed: Bool = false
    var listVouchers: [Voucher]! = [Voucher]()
    var superViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewVoucher.delegate = self
        tableViewVoucher.dataSource = self
        
        self.tableViewVoucher.registerNib(UINib(nibName: "NewCellListTicket", bundle: nil), forCellReuseIdentifier: "NewCellListTicket")
        
        if isListUsed {
            listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Cinema,.Event], listStatus: [.Used])
        } else {
            listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Cinema,.Event], listStatus: [.Created,.Cancelled,.Expired])
        }
        
        print(listVouchers)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension NewListTicketTableView: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listVouchers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("NewCellListTicket", forIndexPath: indexPath) as? NewCellListTicket {
            
            if isListUsed {
                cell.imageViewDiscloserIndicator.image = UIImage(named: "iconDiscloserIndicatorGray")
            } else {
                cell.imageViewDiscloserIndicator.image = UIImage(named: "iconDiscloserIndicatorGreen")
            }
            
            let voucher = self.listVouchers[indexPath.row]
            if let ticket = voucher.ticket {
                
                if let batch = ticket.batch {
                    if let fact = FactEntityManager.getFactByID(String(batch.idFact)) {
                        cell.labelDay.text = NSString.getDayStringFromTimestamp(fact.timestampBegin).uppercaseString
                        cell.labelMonth.text = NSString.getMonthStringFromTimestamp(fact.timestampBegin).uppercaseString
                        cell.labelTitle.text = fact.title
                        cell.labelHour.text = NSString.getHourStringFromTimestamp(fact.timestampBegin)
                    }
                }
            }
            if self.isListUsed {
                cell.viewLeft.backgroundColor = UIColor.gray4all()
            } else {
                cell.viewLeft.backgroundColor = UIColor.green4all()
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension NewListTicketTableView: UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let controller = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewTicketDetail") as! NewTicketDetail
        
        let voucher = self.listVouchers[indexPath.row]
        controller.voucher = voucher
        
        let deviceBounds = UIScreen.mainScreen().bounds
        let formSheet = MZFormSheetController.init(viewController: controller)
        formSheet.shouldDismissOnBackgroundViewTap = false;
        formSheet.cornerRadius = 8.0;
        formSheet.portraitTopInset = 6.0;
        formSheet.landscapeTopInset = 6.0;
        formSheet.presentedFormSheetSize = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height - 40);
        formSheet.formSheetWindow.transparentTouchEnabled = false;
        formSheet.transitionStyle = .Fade
        
        formSheet.presentAnimated(true) { (viewController: UIViewController!) in
            
        }
    }
}
