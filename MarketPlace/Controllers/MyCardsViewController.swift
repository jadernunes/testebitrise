//
//  MyCardsViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 17/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class MyCardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var arrayData = [CardModel]()
    var arrayIssuer : NSArray?
    var refreshControl : UIRefreshControl?
    var selectedCard : CardModel?
    var balanceInt: Int?
    var balanceCents: String?
    var menuTableView :UITableView?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(MyCardsViewController.goBack))
        SessionManagerMobility.sharedInstance.customizeViewWithTitle("Recarga de transporte", vc: self)
        let menuButton = UIBarButtonItem(image: UIImage(named: "iconSubmenu"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(menuButtonPressed))
        self.navigationItem.setRightBarButtonItem(menuButton, animated: true)
        
        self.tableView.tableFooterView = UIView.init(frame: CGRect(x: 0, y: 0, width: 320, height: 64))
        self.tableView.registerNib(UINib.init(nibName: "MyCardsFooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier:"mobilityCellFooter")
        
        self.refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(MyCardsViewController.loadData(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData(UIRefreshControl())

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addShadow(view: UIView){
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        view.layer.shadowOpacity = 0.4
        view.layer.masksToBounds = false
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func menuButtonPressed(){
        if self.view.viewWithTag(15) != nil {
            self.closeMenu()
        } else {
            let screenWidth = UIScreen.mainScreen().bounds.size.width
            let screenHeight = UIScreen.mainScreen().bounds.size.height
            let clearView = UIView(frame: CGRectMake(0, 0, screenWidth, screenHeight))
            clearView.backgroundColor = UIColor.clearColor()
            clearView.tag = 15
            self.view.addSubview(clearView)
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MyCardsViewController.closeMenu))
            clearView.addGestureRecognizer(gestureRecognizer)
            
            self.createMenuTableView()
            self.view.addSubview(self.menuTableView!)
        }
    }
    
    func closeMenu(){
        self.view.viewWithTag(15)!.removeFromSuperview()
        self.menuTableView!.removeFromSuperview()
    }
    
    func loadData(refreshControl: UIRefreshControl){
        let realm = try! Realm()
        
        let catalogDictionary: [NSObject: AnyObject]? = MobilityFilesUtil.loadFromFile("mobilityCatalog")
        
        if let catalog = catalogDictionary {
            if let issuers = catalog["issuers"] as? NSArray {
                arrayIssuer = issuers
                arrayData = realm.objects(CardModel).sort({_,_ in
                    return true
                })
            }
        }
        
        if ((SessionManagerMobility.sharedInstance.needUpdateList || refreshControl != self.refreshControl)  && Lib4all.sharedInstance().hasUserLogged()) {
            self.refreshStoredCards()
        }
        
        self.tableView.reloadData()
        
        self.refreshControl!.endRefreshing()
        SessionManagerMobility.sharedInstance.needUpdateList = false
    }
    
    func createMenuTableView(){
        let x = UIScreen.mainScreen().bounds.size.width
        let y = CGFloat(0)
        
        self.menuTableView = UITableView(frame: CGRectMake(x, y, -197, 176))
        self.menuTableView!.registerNib(UINib.init(nibName: "MobilityMenuTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier:"mobilityMenuCell")
        
        self.menuTableView!.separatorStyle = .None
        self.menuTableView!.scrollEnabled = false
        
        self.menuTableView!.delegate = self
        self.menuTableView!.dataSource = self
        self.menuTableView!.reloadData()
        
        self.addShadow(self.menuTableView!)
    }
    
    func getIssuer(issuerId:Int) -> CardIssuerModel? {
        for provIssuer in arrayIssuer! {
            if let issuer = provIssuer as? NSDictionary {
                if issuer["issuer_id"] as! Int == issuerId {
                    return CardIssuerModel(value: issuer)
                }
            }
        }
        return nil
    }
    
    func isValidCardForAppId(issuerId:Int) -> Bool {
        for provIssuer in arrayIssuer! {
            if let issuer = provIssuer as? CardIssuerModel {
                if issuer.issuer_id == issuerId {
                    return true
                }
            }
        }
        return false
    }
    
    func addCardInStorage(issuerId:Int, issuerNr:String) {
        
        
        let api = ApiServicesMobility()
        
        api.successCase = {(obj) in
            
            if let cardBalancesModel = obj as? NSDictionary {
                
                let issuer = self.getIssuer(issuerId)
                
                if(issuer!.checkBalance) {
                    let cardBalances = NSArray.init(array: cardBalancesModel["cardBalances"] as! NSArray)
                    
                    if(cardBalances.count != 0) {
                        for tempDictionary in cardBalances as! [NSDictionary] {
                            let cardModelDictionary = tempDictionary.mutableCopy()
                            if (cardModelDictionary["checkBalance"] == nil) {
                                cardModelDictionary.setValue(true, forKey:"checkBalance")
                            }
                            let card = CardModel()
                            card.issuerId = (issuer?.issuer_id)!
                            card.productId = Int(cardModelDictionary["productId"] as! String)!
                            card.issuerNr = issuerNr
                            card.productName = cardModelDictionary["productName"] as! String
                            card.incomingBalance = Double(cardModelDictionary["incomingBalance"] as! String)!
                            card.rechargeable = (cardModelDictionary["rechargeable"] as! NSString).boolValue
                            card.balance = Double(cardModelDictionary["balance"] as! String)!
                            card.thumb = "http://i.imgur.com/\((issuer?.thumb)!).png"
                            print(card.thumb)
                            card.checkBalance = true;
                            if (cardModelDictionary["balanceDate"] != nil) {
                                card.balanceDate = Int64(cardModelDictionary["balanceDate"] as! String)!
                            }
                            else {
                                card.balanceDate = -1
                            }
                            
                            let realm = try! Realm()
                            realm.beginWrite()
                            realm.add(card, update: true)
                            try! realm.commitWrite()
                            self.tableView.reloadData()
                        }
                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    }
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                }
                else { //treat cards without check balance
                    
                    let noBalanceService = ApiServicesMobility()
                    
                    noBalanceService.successCase = {(obj) in
                        if let productInfoModels = obj as? NSArray {
                            if (productInfoModels.count > 0) {
                                let productInfo:NSDictionary = productInfoModels[0] as! NSDictionary
                                let card = CardModel()
                                card.issuerId = issuerId
                                card.issuerNr = issuerNr
                                card.rechargeable = (productInfo["rechargeable"] as! NSString).boolValue
                                card.productName = productInfo["name"] as! String
                                card.thumb = productInfo["thumb"] as! String
                                print(card.thumb)
                                card.productId = Int(productInfo["id"] as! String)!
                                
                                let realm = try! Realm()
                                realm.beginWrite()
                                realm.add(card, update: true)
                                try! realm.commitWrite()
                                
                                self.tableView.reloadData()
                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                            }
                        }
                    }
                    noBalanceService.failureCase = {(cod, msg) in
                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    }
                    noBalanceService.buscaSemCheckBalance("\(issuerId)")
                }
                self.tableView.reloadData()
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
            }
        }
        api.failureCase = {(cod, msg) in
            //[MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        api.consultaSaldoECartaoValido(issuerNr, andIssuerId:"\(issuerId)", andCityID:nil, andSessionToken: User.sharedUser().token)
    }
    
    func refreshStoredCards() {
        
        if (Lib4all.sharedInstance().hasUserLogged()) {
            
            if (User.sharedUser().token == nil || User.sharedUser().token == "") {
                let realm = try! Realm()
                realm.beginWrite()
                realm.deleteAll()
                try! realm.commitWrite()
            }
            
            let sessionToken = User.sharedUser().token
            
            let api = ApiServicesMobility()
            
            api.successCase = {(obj) in
                
                if let cards = obj as? NSArray {
                    
                    for cardDict in cards as! [NSDictionary] {
                        var foundCard = false
                        var cardBalaceDateIsZero = false
                        var card = CardModelPersistence.getProductIfExists(cardDict["issuerNr"] as! String)
                        if (card.issuerId != 0) {
                            foundCard = true
                        }
                        card = CardModelPersistence.getProductIfDateBalanceEqualsZero(cardDict["issuerNr"] as! String)
                        if (card.issuerId != 0) {
                            cardBalaceDateIsZero = true
                        }
                        
                        if (self.isValidCardForAppId(Int(cardDict["issuerId"] as! NSNumber))) {
                            if (!foundCard || cardBalaceDateIsZero) {
                                self.addCardInStorage(Int(cardDict["issuerId"] as! NSNumber), issuerNr: cardDict["issuerNr"] as! String)
                            }
                            else {
                                CardModelPersistence.removeCardFromStorageIfHave(cardDict["issuerNr"] as! String)
                            }
                        }
                    }
                }
                
                self.tableView.reloadData()
                //MBProgressHUD.hideHUDForView(self.view, animated:true)
            }
            
            
            api.failureCase = {(cod, msg) in
                
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
            //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            api.meusCartoes(sessionToken)
        }
        
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if (tableView == self.menuTableView) {
            return 1
        } else {
            if (arrayData.count > 0) {
                self.tableView.backgroundView = nil
                return 1
            } else {
                // Display a message when the table is empty
                let messageLabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
                messageLabel.font = LayoutManager.primaryFontWithSize(15)
                messageLabel.text = "Toque no botão '+' para incluir seu cartão."
                messageLabel.textColor = UIColor.darkGrayColor()
                messageLabel.numberOfLines = 0
                messageLabel.textAlignment = .Center
                messageLabel.sizeToFit()
                
                self.tableView.backgroundView = messageLabel
                self.tableView.separatorStyle = .None
                return 0
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.menuTableView) {
            return 4
        } else {
            return (arrayData.count) + 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (tableView == self.menuTableView) {
            let cell = tableView.dequeueReusableCellWithIdentifier("mobilityMenuCell", forIndexPath : indexPath) as! MobilityMenuTableViewCell
            cell.selectionStyle = .None
            let menuOptionIcon = cell.viewWithTag(999) as! UIImageView
            let menuOptionLabel = cell.viewWithTag(998) as! UILabel
            
            switch (indexPath.row) {
            case 0:
                menuOptionIcon.image = UIImage(named: "iconHistory")
                menuOptionLabel.text = "HISTÓRICO DE RECARGA"
                break
            case 1:
                menuOptionIcon.image = UIImage(named: "iconPrazo")
                menuOptionLabel.text = "PRAZO DE RECARGAS"
                break
            case 2:
                menuOptionIcon.image = UIImage(named: "iconTerms")
                menuOptionLabel.text = "TERMOS DE USO"
                break
            case 3:
                menuOptionIcon.image = UIImage(named: "iconHelp")
                menuOptionLabel.text = "AJUDA"
                break
            default:
                break
            }
            return cell
        } else {
            var cell = UITableViewCell()
            if (indexPath.row == arrayData.count) {
                cell = tableView.dequeueReusableCellWithIdentifier("mobilityCellFooter", forIndexPath : indexPath) as! MyCardsFooterTableViewCell
            } else {
                cell = tableView.dequeueReusableCellWithIdentifier("mobilityCardCell", forIndexPath : indexPath) as! MyCardsTableViewCell
                let card : CardModel = arrayData[indexPath.row]
                
                
                let lbBalance = cell.viewWithTag(100) as! UILabel
                let lbBalanceCents = cell.viewWithTag(102) as! UILabel
                let lbNumber = cell.viewWithTag(104) as! UILabel
                let lbNick = cell.viewWithTag(103) as! UILabel
                let lbMensagemServidor = cell.viewWithTag(333) as! UILabel
                let lblBalanceDate = cell.viewWithTag(500) as! UILabel
                let lblBalanceDateUpdated = cell.viewWithTag(501) as! UILabel
                let im = cell.viewWithTag(110) as! UIImageView
                let viewRecharge = cell.viewWithTag(111)
                let viewBase = cell.viewWithTag(120)
                
                lbMensagemServidor.backgroundColor = UIColor.clearColor()
                
                lbBalance.font = LayoutManager.primaryFontWithSize(30)
                lbBalanceCents.font = LayoutManager.primaryFontWithSize(15)
                lblBalanceDate.font = LayoutManager.primaryFontWithSize(13)
                
                // balance parte inteira
                var balance = card.balance / 100.0
                balanceInt = Int(balance)
                lbBalance.text = "\(balanceInt!),"
                
                // calcula cents
                balance = balance - Double(balanceInt!)
                balanceCents = "\(Int(balance*100))"
                if(balance == 0) {
                    balanceCents = "00"
                }
                
                lbNumber.text = card.issuerNr
                lbNick.text = card.productName
                lbBalanceCents.text = balanceCents
                
                if (card.balanceDate != 0 && card.balanceDate != -1) {
                    let balanceDate = NSDate(timeIntervalSince1970: Double(card.balanceDate) / 1000.0)
                    let objDateFormatter = NSDateFormatter()
                    objDateFormatter.dateFormat = "dd/MM/yyyy - HH:mm"
                    lblBalanceDate.text = "\(objDateFormatter.stringFromDate(balanceDate))"
                    lblBalanceDateUpdated.text = "Saldo atualizado em"
                }
                
                im.sd_setImageWithURL(NSURL(string: card.thumb))
                lbMensagemServidor.text = ""
                
                if let cardIssuer : CardIssuerModel = self.getIssuer(card.issuerId) {
                    if (!cardIssuer.checkBalance) {
                        lbMensagemServidor.backgroundColor = UIColor.whiteColor()
                        lbMensagemServidor.text = card.issuerId == 4 ? "Não disponibilizado pela operadora" : "Consulta indisponível no momento."
                        lbMensagemServidor.hidden = false
                    } else{
                        lbMensagemServidor.hidden = true
                    }
                } else {
                    lbMensagemServidor.backgroundColor = UIColor.whiteColor()
                    lbMensagemServidor.text = "Consulta indisponível no momento."
                    lbMensagemServidor.hidden = false
                }
                if (card.rechargeable) {
                    viewRecharge!.hidden = false
                    
                } else {
                    viewRecharge!.hidden = true
                }
                
                viewBase!.backgroundColor = UIColor.whiteColor()
                viewBase!.layer.cornerRadius = 5.0
                viewBase!.layer.masksToBounds = true
            }
          return cell  
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (tableView == self.menuTableView){
            return 44
        } else {
            if(indexPath.row == arrayData.count) {
                return 130
            }
            return 130
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (tableView == self.menuTableView) {
          switch (indexPath.row){
                case 0: //HISTORICO
                    self.closeMenu()
                    performSegueWithIdentifier("MOBILITY_HISTORY", sender: self)
                    break
                case 1: //PRAZO
                    self.closeMenu()
                    performSegueWithIdentifier("MOBILITY_PRAZO", sender: self)
                    break
                case 2: //TERMOS
                    self.closeMenu()
                    performSegueWithIdentifier("MOBILITY_TERMS", sender: self)
                    break
                case 3: //SOBRE
                    self.closeMenu()
                    performSegueWithIdentifier("MOBILITY_HELP", sender: self)
                    break
                default:
                    break
            }
        } else {
            self.selectedCard = arrayData[indexPath.row]
            if (selectedCard!.rechargeable) {
                performSegueWithIdentifier("paymentSegue", sender: nil)
            }
        }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if (tableView == self.tableView) {
            if let card = arrayData[indexPath.row] as? CardModel{
                return card.rechargeable
            }
            return false
        }
        return true
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if (tableView == self.menuTableView) {
            return false
        }
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == .Delete) {
            // Delete the row from the data source
            let deleteService = ApiServicesMobility()
            
            deleteService.successCase = { (obj) in
                
                let realm = try! Realm()
                try! realm.write() {
                    realm.delete(self.arrayData[indexPath.row])
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.loadData(UIRefreshControl())
                    SessionManagerMobility.sharedInstance.needUpdateList = true
                }
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
            deleteService.failureCase = { (cod, msg) in
                let alert = UIAlertView(title: "Atenção", message: "Não foi possível excluir o cartão, por favor, tente novamente.", delegate: nil, cancelButtonTitle: "OK")
                
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                
                alert.show()
            };
            
            if (Lib4all.sharedInstance().hasUserLogged()) {
                //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                deleteService.deletarCartaoUsuario(arrayData[indexPath.row].issuerNr, andSessionToken: User.sharedUser().token)
                
            }else{
                let product = arrayData[indexPath.row]
                CardModelPersistence.removeCardFromStorageIfHave(product.issuerNr)
                self.loadData(UIRefreshControl())
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let paymentController = segue.destinationViewController as? RechargePaymentViewController {
            
            paymentController.productId = selectedCard!.productId
            paymentController.issuerId  = selectedCard!.issuerId
            paymentController.issuerNr  = selectedCard!.issuerNr
            paymentController.issuerName = selectedCard!.productName
            paymentController.cardThumb = selectedCard!.thumb
            let cardBalanceInt = Int(selectedCard!.balance/100)
            //calculate the cents
            let cardBalanceCents = selectedCard!.balance/100 - Double(cardBalanceInt)
            paymentController.cardBalance = cardBalanceInt
            paymentController.cardBalanceCents = Int(cardBalanceCents*100)
            paymentController.cardBalanceDate = selectedCard!.balanceDate
            
            
        }
    }
    
    @IBAction func addCard(sender: AnyObject) {
        
        let storyboard = UIStoryboard.init(name:"Mobility", bundle:nil)
        let newCardVC = storyboard.instantiateViewControllerWithIdentifier("NEW_ISSUER")
        newCardVC.modalPresentationStyle = .FullScreen
        
        self.navigationController?.presentViewController(newCardVC, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
