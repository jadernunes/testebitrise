//
//  SelectProductTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 25/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class SelectProductTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewIconItem: UIImageView!
    @IBOutlet weak var labelTitleProduct: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var constraintHeightlabelDescription: NSLayoutConstraint!
    @IBOutlet weak var labeDescripionProduct: UILabel!
    @IBOutlet weak var viewSeparatorCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewSeparatorCell.backgroundColor = LayoutManager.lightGray4all
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func populateData(unityItem: UnityItem, modifier:UnityItemModifier) {
        
        self.labelPrice.text = ""
        if modifier.idUnityItemModifierPriceType == UnityItemModifierPriceType.sum_total.rawValue &&
            unityItem.idUnityItemType != UnitItemType.Pizza.rawValue {
            self.labelPrice.text = Util.formatCurrency(unityItem.price/100)
        }
        
        var urlImage = ""
        if(unityItem.thumb != nil) {
            urlImage = unityItem.thumb!
        }
        
        self.labelTitleProduct.text = unityItem.title
        self.imageViewIconItem.addImage(urlImage)
        
        if unityItem.desc != nil {
            if unityItem.desc!.characters.count == 0 {
                self.labeDescripionProduct.text = ""
                self.constraintHeightlabelDescription.constant = 20
            } else {
                self.labeDescripionProduct.text = unityItem.desc!
                self.constraintHeightlabelDescription.constant = 50
            }
        }
    }
    
}
