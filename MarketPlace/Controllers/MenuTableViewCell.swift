//
//  MenuTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var viewLineSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.font = LayoutManager.primaryFontWithSize(15)
        viewLineSeparator.backgroundColor = UIColor.lightGray4all()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }    
}
