//
//  MenuViewController.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LoginDelegate {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelHelp: UILabel!
    @IBOutlet weak var labelLogout: UILabel!
    
    @IBOutlet weak var viewBaseUserUnLoged: UIView!
    @IBOutlet weak var labelMessageMakeLogin: UILabel!
    @IBOutlet weak var labelPossuiConta: UILabel!
    @IBOutlet weak var buttonSignup: UIButton!
    
    @IBOutlet weak var buttonSignin: UIButton!
    @IBOutlet weak var labelVersion: UILabel!
    
    static let sharedInstance = MenuViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor.clearColor()
        self.labelName.textColor = UIColor.whiteColor()
        self.labelDesc.textColor = UIColor.whiteColor()
        self.labelVersion.textColor = UIColor.whiteColor()
        self.labelName.font = LayoutManager.primaryFontWithSize(20)
        self.labelDesc.font = LayoutManager.primaryFontWithSize(14)
        self.labelVersion.font = LayoutManager.primaryFontWithSize(12)
        
        self.labelVersion.text = SystemInfo.getVersionApp()
        
        self.labelMessageMakeLogin.textColor = UIColor.darkGray4all()
        self.labelPossuiConta.textColor = UIColor.darkGray4all()
        self.buttonSignin.setTitleColor(UIColor.green4all(), forState: UIControlState.Normal)
        
        self.buttonSignup.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        self.buttonSignup.setBackgroundImage(Util.imageLayerForGradientBackgroundInView(self.buttonSignup), forState: UIControlState.Normal)
        self.buttonSignup.layer.masksToBounds = true
        self.buttonSignup.layer.cornerRadius = 8
        
        labelHelp.textColor = UIColor.darkGray4all()
        labelLogout.textColor = UIColor.darkGray4all()
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(reloadAllData), name: "statusLogin", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadAllData()
    }
    
    @IBAction func buttonSignupPressed(sender: UIButton) {
        self.sidePanelController.showCenterPanelAnimated(true)
        if Lib4all().hasUserLogged() == false {
            Lib4all().callSignUp(self) { (phoneNumber, emailAddress, sessionToken) in
                self.reloadAllData()
                self.didLogin()
            }
        }
    }
    
    @IBAction func buttonSigninPressed(sender: UIButton) {
        self.sidePanelController.showCenterPanelAnimated(true)
        if Lib4all().hasUserLogged() == false {
            Lib4all().callLogin((self.sidePanelController.centerPanel as! UINavigationController).viewControllers[0] as! NewHomeViewController, requireFullName: true, requireCpfOrCnpj: false, completion: { (phoneNumber, email, sessionToken) in
                self.reloadAllData()
                self.didLogin()
            })
        }
    }
    
    func reloadAllData(){
        
        if Lib4all.sharedInstance().hasUserLogged() {
            let user = User.sharedUser()
            if self.labelName != nil && self.labelDesc != nil {
                self.labelName.text = user.fullName
                self.labelDesc.text = user.emailAddress
                self.tableView.reloadData()
            }
            
            if self.tableView != nil {
                self.tableView.hidden = false
            }
            
            if self.viewBaseUserUnLoged != nil {
                self.viewBaseUserUnLoged.hidden = true
            }
            
        } else {
            if self.labelName != nil && self.labelDesc != nil {
                self.labelName.text = "Oi! Seja bem vindo!"
                self.labelDesc.text = ""
                self.tableView.reloadData()
            }
            
            if self.tableView != nil {
                self.tableView.hidden = true
            }
            
            if self.viewBaseUserUnLoged != nil {
                self.viewBaseUserUnLoged.hidden = false
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return LIST_SECTIONS_SIDE_MENU.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dicMenu = LIST_SECTIONS_SIDE_MENU.objectAtIndex(section) as! NSDictionary
        let sectionMenu = SectionMenu(titleSection: dicMenu.objectForKey("titleSection") as! String, listItems: dicMenu.objectForKey("listItems") as! Array<NSDictionary>)
        
        return sectionMenu.listItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let dicMenu = LIST_SECTIONS_SIDE_MENU.objectAtIndex(indexPath.section) as! NSDictionary
        let sectionMenu = SectionMenu(titleSection: dicMenu.objectForKey("titleSection") as! String, listItems: dicMenu.objectForKey("listItems") as! Array<NSDictionary>)
        let cellMenu = sectionMenu.listItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("menuCell") as! MenuTableViewCell
        cell.labelName.text = cellMenu.objectForKey("titleCell") as? String
        cell.labelName.textColor = UIColor.darkGray4all()
        
        cell.imageViewIcon.addImage(nil)
        Util.removeBadgeInMenu(cell.imageViewIcon)
        
        if let icon = cellMenu["icon"] as? String {
            cell.imageViewIcon.image = UIImage(named: icon)
        }
        
        if let type = cellMenu["type"] as? Int {
            if type == 6 {//Ingresso
                if let icon = cellMenu["isShowBadge"] as? Bool where icon == true  {
                    let count = VoucherPersistence.getAllLocalNewVouchers([.Cinema,.Event], listStatus: [.Created]).count
                    Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                }
            } else if type == 7 {//Vouchers
                if let icon = cellMenu["isShowBadge"] as? Bool where icon == true  {
                    let count = VoucherPersistence.getAllLocalNewVouchers([.Order,.Fidelity], listStatus: [.Created]).count
                    Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                }
            } else if type == 8 {//Cupons
                if let icon = cellMenu["isShowBadge"] as? Bool where icon == true  {
                    let count = CoupomPersistence.getAllLocalCoupons().count
                    Util.showBadgeInMenu(cell.imageViewIcon, count: count)
                }
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.section == 0 {
            switch indexPath.row {
                case 0://Ingressos (Cinema, Evento)
                    let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
                    viewController.isListVouchers = false
                    (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
                    self.sidePanelController.showCenterPanelAnimated(true)
                    break
                case 1://Vouchers (Pedido, Fidelidade)
                    let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
                    viewController.isListVouchers = true
                    (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
                    self.sidePanelController.showCenterPanelAnimated(true)
                    break
                case 2://Cupons (Coupom)
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
                    viewController.typeShow = TypeShowListCoupons.meCoupons
                    viewController.title = "Meus cupons"
                    (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
                    self.sidePanelController.showCenterPanelAnimated(true)
                    break
                case 3: //Pedidos
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OrdersTableViewController") as! OrdersTableViewController
                    (self.sidePanelController.centerPanel as! UINavigationController).pushViewController(viewController, animated: true)
                    self.sidePanelController.showCenterPanelAnimated(true)
                    break
                default:
                    break
            }
            
        } else {
            switch indexPath.row {
                case 0: //Extratos
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.Receipt)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 1: //Assinaturas
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.Subscriptions)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 2: //Dados Pessoais
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.UserData)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 3://Cartões
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.UserCards)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 4: //Família
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.Family)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 5: //Configurações
                    if Lib4all.sharedInstance().hasUserLogged() {
                        self.showMenuAccount4all(ProfileOption.Settings)
                        self.sidePanelController.showCenterPanelAnimated(true)
                    }
                    break
                case 6: //Sobre
                    self.showMenuAccount4all(ProfileOption.About)
                    self.sidePanelController.showCenterPanelAnimated(true)
                    break
                default:
                    break
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let rect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let view = SectionMenuView.instanceFromNib()
        view.frame = rect
        let dicMenu = LIST_SECTIONS_SIDE_MENU.objectAtIndex(section) as! NSDictionary
        let sectionMenu = SectionMenu(titleSection: dicMenu.objectForKey("titleSection") as! String, listItems: dicMenu.objectForKey("listItems") as! Array<NSDictionary>)
        if let title = sectionMenu.titleSection {
            view.labelTitle.text = title
        }
        
        return view
    }
    
    func alertClose(gesture: UITapGestureRecognizer) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func showMenuAccount4all(profileOption:ProfileOption){
        if let account = Lib4all.sharedInstance() {
            if account.hasUserLogged() {
                account.openAccountScreen(profileOption, inViewController: self.sidePanelController.centerPanel)
            }
        }
    }
    
    @IBAction func buttonHelpPressed(sender: UIButton) {
        self.showMenuAccount4all(ProfileOption.Help)
        self.sidePanelController.showCenterPanelAnimated(true)
    }
    
    @IBAction func buttonLogoutPressed(sender: UIButton) {
        let alert = UIAlertView(title: "Atenção", message: "Você tem deseja realmente sair da conta?", delegate: self, cancelButtonTitle: "Não", otherButtonTitles: "Sim")
        alert.show()
    }
}

extension MenuViewController: UIAlertViewDelegate {
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            let load = LoadingViewController.sharedManager() as! LoadingViewController
            load.startLoading(self, title: "Aguarde")
            Lib4all.sharedInstance().callLogout { (status) in
                
                load.finishLoading({
                    let notificationCenter = NSNotificationCenter.defaultCenter()
                    notificationCenter.postNotificationName("statusLogin", object: nil)
                    self.navigationController?.popViewControllerAnimated(true)
                })
            }
            self.sidePanelController.showCenterPanelAnimated(true)
        }
    }
    
}
