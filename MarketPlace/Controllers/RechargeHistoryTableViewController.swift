//
//  RechargeHistoryTableViewController.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 08/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class RechargeHistoryTableViewController: UITableViewController {

    var arrayHistory = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Histórico de recargas"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "backButton"), style: .Plain, target: self, action: #selector(RechargeHistoryTableViewController.goBack))
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(RechargeHistoryTableViewController.populateHistory), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.populateHistory()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if (self.arrayHistory.count > 0) {
            self.tableView.backgroundView = nil
            return 1
        } else {
            // Display a message when the table is empty
            let messageLabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            messageLabel.font = LayoutManager.primaryFontWithSize(15)
            messageLabel.text = "Não há transações recentes."
            messageLabel.textColor = UIColor.darkGrayColor()
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .Center
            messageLabel.sizeToFit()
            
            self.tableView.backgroundView = messageLabel
            self.tableView.separatorStyle = .None
            return 0
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayHistory.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let recharge = arrayHistory[indexPath.row] as! CardRechargeModel
        
        let cell = tableView.dequeueReusableCellWithIdentifier("RechargeCell", forIndexPath:indexPath)
        
        // Configure the cell...
        let lbAmount = cell.viewWithTag(100) as! UILabel
        let lbAmountCents = cell.viewWithTag(101) as! UILabel
        let lbTime = cell.viewWithTag(102) as! UILabel
        let lbDate = cell.viewWithTag(103) as! UILabel
        let lbNumberCard = cell.viewWithTag(222) as! UILabel
        let im = cell.viewWithTag(110) as! UIImageView
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        lbAmount.font = LayoutManager.primaryFontWithSize(30)
        lbAmountCents.font = LayoutManager.primaryFontWithSize(15)
        
        lbNumberCard.text = recharge.issuerNr
        im.sd_setImageWithURL(NSURL(string: recharge.issuerThumb))
        lbAmount.text = String(format: "%1.0f,", recharge.valuePaid)
        lbTime.text = timeFormatter.stringFromDate(recharge.date)
        lbDate.text = dateFormatter.stringFromDate(recharge.date)
        
        return cell

    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func populateHistory(){

        self.refreshControl!.beginRefreshing()
        let realm = try! Realm()
        let history = realm.objects(CardRechargeModel).sorted("date", ascending: false)
        
        self.arrayHistory = NSMutableArray()
        
        for recharge in history {
            self.arrayHistory.addObject(recharge)
        }
            
        dispatch_async(dispatch_get_main_queue()) {
            self.refreshControl!.endRefreshing()
            self.tableView.reloadData()
        }
    }
    

}
