//
//  NumberAndDeliveryTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/14/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NumberAndDeliveryTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelTitle.font = LayoutManager.primaryFontWithSize(20)
        self.labelTitle.textColor = LayoutManager.lightGreyTracking
        self.labelDescription.font = LayoutManager.primaryFontWithSize(20)
        self.labelDescription.textColor = LayoutManager.darkGreyTracking
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
