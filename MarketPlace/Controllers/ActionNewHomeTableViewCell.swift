//
//  ActionNewHomeTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ActionNewHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var collectionViewActions: UICollectionView!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var viewLineSeparator: UIView!
    @IBOutlet weak var viewSeparatorLabel: UIView!
    
    var listActions: [[String: Any]]?
    var section: [String: Any]?
    var superViewController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTitle.textColor = UIColor.darkGray4all()
        self.viewSeparatorLabel.backgroundColor = UIColor.lightGray4all()
        
        self.collectionViewActions.registerNib(UINib(nibName: "ActionCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "ActionCollectionViewCell")
        
        self.collectionViewActions.delegate = self
        self.collectionViewActions.dataSource = self
        self.backgroundColor = UIColor.whiteColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func populateData(actions: [[String: Any]], section:[String: Any]){
        self.listActions = actions
        self.section = section
        self.collectionViewActions.reloadData()
    }
    
}

extension ActionNewHomeTableViewCell: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let widthDevice = UIScreen.mainScreen().bounds.width
        let width = Double(widthDevice)/Double(3.2)
        let height = 85.0
        let size = CGSizeMake(CGFloat(width), CGFloat(height))
        
        return size
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let action = self.listActions![indexPath.row]
        if let target = action["target"] as? String where target.characters.count > 0 {
            
            if target == "recarga" {
                LibRecargaMobile.callMenuRecargaMobile(self.superViewController)
            } else if target == "allCoupom" {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListCoupomTableViewController") as! ListCoupomTableViewController
                viewController.typeShow = TypeShowListCoupons.allCupons
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "newParking" {
                self.superViewController.performSegueWithIdentifier("segue4park", sender: nil)
            } else if target == "newMap" {
                let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("mapView") as! UnityMapViewController
                viewController.title = "Mapa"
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "MyCards" {
                let viewController = UIStoryboard(name: "Mobility", bundle: nil).instantiateViewControllerWithIdentifier("mobilityInitialVC") as! MyCardsViewController
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "Recharge" {
                if let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("mobilityVC") as? MobilityViewController {
                    self.superViewController.navigationController?.pushViewController(viewController, animated: true)
                }
            } else if target == "addCard" {
                if let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("mobilityVC") as? MobilityViewController {
                    self.superViewController.navigationController?.pushViewController(viewController, animated: true)
                }
            } else if target == "4allCardDetailVC" {
                if let viewController = UIStoryboard(name: "4all", bundle: nil).instantiateViewControllerWithIdentifier("4allCardDetailVC") as? HomeTableViewController {
                    viewController.targetURLString = "4fun"
                    viewController.title = "Ingressos e vouchers"
                    self.superViewController.navigationController?.pushViewController(viewController, animated: true)
                }
            } else if target == "ListVC" {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
                viewController.subType = .Event
                viewController.title = "Ingressos e vouchers"
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "LojasProximasNewHomeViewController" {
                let viewController = UIStoryboard(name: "LojasPerto", bundle: nil).instantiateViewControllerWithIdentifier("LojasProximasNewHomeViewController") as! LojasProximasNewHomeViewController
                viewController.title = "Lojas próximas"
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "recargaCelular" {
                if Lib4all.sharedInstance().hasUserLogged() {
                    LibRecargaMobile.callMenuRecargaMobile(self.superViewController)
                } else {
                    Lib4all.sharedInstance().callLogin(self.superViewController, completion: { (phone, email, sessionToken) in
                    })
                }
            }
            else if target == "foodEntrega" {
                let viewController = UIStoryboard(name: "Gastronomia", bundle: nil).instantiateViewControllerWithIdentifier("FoodInitialVC") as! FoodInitialVC
                viewController.initialOrderType = .entrega
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
                
            } else if target == "foodRetirada" {
                let viewController = UIStoryboard(name: "Gastronomia", bundle: nil).instantiateViewControllerWithIdentifier("FoodInitialVC") as! FoodInitialVC
                viewController.initialOrderType = .retirada
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else if target == "foodComanda" {
                let viewController = UIStoryboard(name: "Gastronomia", bundle: nil).instantiateViewControllerWithIdentifier("FoodInitialVC") as! FoodInitialVC
                viewController.initialOrderType = .comanda
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(target)
                self.superViewController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
}

extension ActionNewHomeTableViewCell: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let countItens = listActions {
            return countItens.count
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ActionCollectionViewCell", forIndexPath: indexPath) as? ActionCollectionViewCell {
            
            let action = self.listActions![indexPath.row]
            cell.imageViewAction.image = UIImage(named: action["icon"] as! String)
            cell.labelTitleAction.text = action["title"] as? String
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
}
