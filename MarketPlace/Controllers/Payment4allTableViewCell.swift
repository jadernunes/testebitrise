//
//  Payment4allTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 1/8/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Payment4allTableViewCell: UITableViewCell, LoginDelegate {
    @IBOutlet weak var containerPayment: UIView!
    @IBOutlet weak var labelTimeDelivery: UILabel!
    @IBOutlet weak var labelPriceDelivery: UILabel!
    @IBOutlet weak var labelTextTotal: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    
    var cart                : Order!
    var parentVC            : CartViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelTimeDelivery.font = LayoutManager.primaryFontWithSize(15)
        self.labelTimeDelivery.textColor = LayoutManager.greyDescItemCart
        self.labelPriceDelivery.font = LayoutManager.primaryFontWithSize(15)
        self.labelPriceDelivery.textColor = LayoutManager.greyDescItemCart
        self.labelTextTotal.font = LayoutManager.primaryFontWithSize(24)
        self.labelTextTotal.textColor = LayoutManager.greyTitleItemCart
        self.labelTotal.font = LayoutManager.primaryFontWithSize(24)
        self.labelTotal.textColor = SessionManager.sharedInstance.cardColor
        
        self.labelTimeDelivery.alpha = 0
        self.labelPriceDelivery.alpha = 0
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurePaymentComponent(component: ComponentViewController) {
        dispatch_async(dispatch_get_main_queue()) {
            component.buttonTitleWhenNotLogged = kEnter
            component.buttonTitleWhenLogged    = kPay
            component.requireFullName = true
            component.delegate = self.parentVC
            component.view.frame = self.containerPayment.bounds
            component.buttonTitleWhenNotLogged = kEnter
            component.buttonTitleWhenLogged = kPay
            self.containerPayment.addSubview(component.view)
        }
    }

    func updateLabels() {
        if self.cart != nil {
            self.labelTotal.text = Util.formatCurrency(self.cart.total/100)
            
            if cart.idOrderType == OrderType.Delivery.rawValue || cart.idOrderType == OrderType.TakeAway.rawValue {
                let unity = OrderEntityManager.sharedInstance.getOrderUnity(self.cart!.idUnity)
                
                if cart.idOrderType == OrderType.Delivery.rawValue {
                    self.labelTimeDelivery.text = kDelivery + " (\(Int(unity!.deliveryEstimatedTime))" + kMin + ")"
                    if cart.address != nil {
                        self.labelPriceDelivery.text = Util.formatCurrency((self.cart?.deliveryFee)!/100)
                        self.labelPriceDelivery.alpha = 1
                    }
                }
                if cart.idOrderType == OrderType.TakeAway.rawValue {
                    if let unityObject = unity {
                        self.labelTimeDelivery.text = kTakeaway + " (\(Int(unityObject.takeAwayEstimatedTime))" + kMin + ")"
                        self.labelPriceDelivery.alpha = 0
                    }
                }
                self.labelTimeDelivery.alpha = 1
            } else {
                self.labelTimeDelivery.alpha = 0
                self.labelPriceDelivery.alpha = 0
            }
        }
    }

}
