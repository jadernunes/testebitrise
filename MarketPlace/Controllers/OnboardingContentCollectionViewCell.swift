//
//  OnboardingContentCell.swift
//  Donna
//
//  Created by Rodrigo Kreutz on 2/15/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

protocol OnboardingContentCollectionViewCellDelegate {
    func skipOnboarding()
    func signup()
    func signin()
}

class OnboardingContentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var buttonSkip: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonSubscribe: UIButton!
    @IBOutlet weak var buttonLoginWithoutSession: UIButton!
    
    private var animated: Bool = false
    
    var delegate: OnboardingContentCollectionViewCellDelegate?
    
    func configureWithContent(onboardingContent: OnboardingContent) {
        layer.removeAllAnimations()
        backgroundColor = onboardingContent.colorBackground
        imageViewIcon.image = onboardingContent.imageIcon
        labelTitle.text = onboardingContent.textTitle
        labelTitle.textColor = onboardingContent.colorTitle
        labelContent.text = onboardingContent.textSubtitle
        labelContent.textColor = onboardingContent.colorSubtitle
        buttonSkip.setTitleColor(onboardingContent.colorTextButtonSkip, forState: .Normal)
        buttonSkip.addTarget(self, action: #selector(OnboardingContentCollectionViewCell.skipOnboarding), forControlEvents: .TouchUpInside)
        
        //Button SingUp
        buttonSubscribe.backgroundColor = onboardingContent.colorBackgroudButtonSignup
        buttonSubscribe.setTitleColor(onboardingContent.colorTextButtonSignup, forState: UIControlState.Normal)
        buttonSubscribe.addTarget(self, action: #selector(OnboardingContentCollectionViewCell.signup), forControlEvents: .TouchUpInside)
        buttonSubscribe.setTitle(onboardingContent.textButtonSignup, forState: UIControlState.Normal)
        
        //Button Login
        buttonLogin.backgroundColor = onboardingContent.colorBackgroudButtonSignin
        buttonLogin.setTitleColor(onboardingContent.colorTextButtonSignin, forState: UIControlState.Normal)
        buttonLogin.addTarget(self, action: #selector(OnboardingContentCollectionViewCell.signin), forControlEvents: .TouchUpInside)
        buttonLogin.setTitle(onboardingContent.textButtonSignin, forState: UIControlState.Normal)
        
        buttonLoginWithoutSession.backgroundColor = onboardingContent.colorBackgroudButtonSignin
        buttonLoginWithoutSession.setTitleColor(onboardingContent.colorTextButtonSignin, forState: UIControlState.Normal)
        buttonLoginWithoutSession.addTarget(self, action: #selector(OnboardingContentCollectionViewCell.skipOnboarding), forControlEvents: .TouchUpInside)
        buttonLoginWithoutSession.layer.masksToBounds = true
        buttonLoginWithoutSession.layer.cornerRadius = 8
        
        self.buttonLoginWithoutSession.hidden = true
        
        if Lib4all.sharedInstance().hasUserLogged() {
            self.buttonLogin.hidden = true
            self.buttonSubscribe.hidden = true
        } else {
            self.buttonLogin.hidden = false
            self.buttonSubscribe.hidden = false
        }
    }
    
    func applyTransform(transform: CATransform3D) {
        labelTitle.layer.transform = transform
        labelContent.layer.transform = transform
        imageViewIcon.layer.transform = transform
        buttonSkip.layer.transform = transform
        buttonSubscribe.layer.transform = transform
        buttonLogin.layer.transform = transform
        buttonLoginWithoutSession.layer.transform = transform
    }
    
    func applyAlpha(alpha: CGFloat) {
        labelTitle.alpha = alpha
        labelContent.alpha = alpha
        imageViewIcon.alpha = alpha
        buttonSkip.alpha = alpha
        buttonSubscribe.alpha = alpha
        buttonLogin.alpha = alpha
        buttonLoginWithoutSession.alpha = alpha
    }
    
    func skipOnboarding() {
        delegate?.skipOnboarding()
    }

    func signup(){
        delegate?.signup()
    }
    
    func signin(){
        delegate?.signin()
    }
}
