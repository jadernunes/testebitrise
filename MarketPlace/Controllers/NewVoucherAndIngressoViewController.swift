//
//  NewVoucherAndIngressoViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import PageMenu

class NewVoucherAndIngressoViewController: UIViewController {

    var pageMenu: CAPSPageMenu!
    var listPages : [UIViewController] = []
    var contentArray    : [AnyObject] = []
    var isListVouchers: Bool = false
    var load: LoadingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isListVouchers == true {
            self.title = "Vouchers"
            
            //Em aberto
            let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewListVoucherTableView") as! NewListVoucherTableView
            viewController.title = "MEUS VOUCHERS"
            viewController.isListUsed = false
            listPages.append(viewController)
            
            //Utilizados
            let vcUsados = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewListVoucherTableView") as! NewListVoucherTableView
            vcUsados.title = "VOUCHERS UTILIZADOS"
            vcUsados.isListUsed = true
            listPages.append(vcUsados)
        } else {
            self.title = "Ingressos"
            
            //Em aberto
            let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewListTicketTableView") as! NewListTicketTableView
            viewController.title = "MEUS INGRESSOS"
            viewController.isListUsed = false
            listPages.append(viewController)
            
            //Utilizados
            let vcUsados = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewListTicketTableView") as! NewListTicketTableView
            vcUsados.title = "INGRESSOS UTILIZADOS"
            vcUsados.isListUsed = true
            listPages.append(vcUsados)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(UIColor.lightGray4all()),
            .ViewBackgroundColor(UIColor.whiteColor()),
            .MenuItemMargin(6.0),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.05),
            .SelectionIndicatorColor(UIColor.green4all()),
            .SelectionIndicatorHeight(2.0),
            .MenuItemFont(LayoutManager.primaryFontWithSize(16)),
            .MenuHeight(48.0),
            .UnselectedMenuItemLabelColor(UIColor.darkGray4all().colorWithAlphaComponent(0.5)),
            .SelectedMenuItemLabelColor(UIColor.darkGray4all()),
            .CenterMenuItems(false),
            .MenuItemWidthBasedOnTitleTextWidth(true)
        ]
        
        if listPages.count > 0 && parameters.count > 0 {
            
            // Initialize page menu with controller array, frame, and optional parameters
            pageMenu = CAPSPageMenu(viewControllers: listPages, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
            pageMenu?.menuHeight = 60
            pageMenu?.scrollMenuBackgroundColor = UIColor.lightGray4all()
        }
        
        if listPages.count > 0 {
            // Lastly add page menu as subview of base view controller view
            // or use pageMenu controller in you view hierachy as desired
            self.view.addSubview(pageMenu!.view)
            
            if isListVouchers == true {
                let cont = ((self.pageMenu?.childViewControllers[0])! as! NewListVoucherTableView)
                cont.tableViewVoucher?.backgroundColor = UIColor.whiteColor()
                cont.view.backgroundColor = UIColor.whiteColor()
                cont.superViewController = self
            } else {
                let cont = ((self.pageMenu?.childViewControllers[0])! as! NewListTicketTableView)
                cont.tableViewVoucher?.backgroundColor = UIColor.whiteColor()
                cont.view.backgroundColor = UIColor.whiteColor()
                cont.superViewController = self
            }
        }
        
        load = LoadingViewController.sharedManager() as! LoadingViewController
        load.startLoading(self, title: "Aguarde")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        getAllMyVouchersByServer()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if isListVouchers == true {
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMeusVouchers)
        } else {
            GoogleAnalytics.sharedInstance.trakingScreenWithName(ScreensName.screenMeusIngressos)
        }
    }
    
    func getAllMyVouchersByServer()
    {
        let api = ApiServices()
        
        /* --- Caso de sucesso busca os vouchers --- */
        api.successCase = {(obj) in
            self.load.finishLoading(nil)
            
            if self.isListVouchers == true {
                let cont = ((self.pageMenu?.childViewControllers[0])! as! NewListVoucherTableView)
                if cont.isListUsed {
                    cont.listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Order,.Fidelity], listStatus: [.Used])
                } else {
                    cont.listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Order,.Fidelity], listStatus: [.Created,.Cancelled,.Expired])
                }
                cont.tableViewVoucher?.reloadData()
            } else {
                let cont = ((self.pageMenu?.childViewControllers[0])! as! NewListTicketTableView)
                if cont.isListUsed {
                    cont.listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Cinema,.Event], listStatus: [.Used])
                } else {
                    cont.listVouchers = VoucherPersistence.getAllLocalNewVouchers([.Cinema,.Event], listStatus: [.Created,.Cancelled,.Expired])
                }
                
                cont.tableViewVoucher?.reloadData()
            }
        }
        
        api.failureCase = {(msg) in
            self.load.finishLoading(nil)
        }
        
        //Get token by user loged
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    api.getVouchersBySessionToken(user.token)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
