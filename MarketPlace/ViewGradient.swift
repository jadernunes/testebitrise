//
//  ViewGradient.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 27/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

@IBDesignable class ViewGradient : UIView {
    
    @IBInspectable var startColor: UIColor = UIColor.clearColor() {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var endColor: UIColor = UIColor.clearColor() {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var isHorizontal: Bool = false {
        didSet {
            setupView()
        }
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    override class func layerClass() -> AnyClass {
        return CAGradientLayer.self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setupView()
    }
    
    private func setupView() {
        let colors:Array = [startColor.CGColor, endColor.CGColor]
        gradientLayer.colors = colors
        
        if isHorizontal {
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        } else {
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        }
        
        self.setNeedsDisplay()
    }
}
