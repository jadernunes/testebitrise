//
//  ConstantsSize.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 01/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

/* --- MapView Unities --- */
var kSizePinAnnotationView      : CGSize = CGSizeMake(30, 30)
var kSizePinCalloutImage        : CGSize = CGSizeMake(15, 20)

/* --- Home 4all --- */
var kFrameTableViewResults          = CGRect()
var kFrameTextWelcome               = CGRect() //Texto vai se posicionar com base na posicao da Search Bar
let kSizeTextWelcome                = CGSize(width: UIScreen.mainScreen().bounds.width,
                                             height: 30)
let kPercentImage                   : Float = 75
let kPercentViewDescription         : Float = 25
var kFrameSearchBar                 = CGRect()
let kSizeSearchBar                  : CGSize = CGSize(width: UIScreen.mainScreen().bounds.width*0.9,
                                                      height: 44)

let kSizeCard                       : CGSize = CGSize(width: UIScreen.mainScreen().bounds.width*0.9,
                                                      height: UIScreen.mainScreen().bounds.height*0.6)

let kFrameCardOffset                : CGSize = CGSize(width: 0,
                                                      height: 50)

var kSizeCardImage                  : CGSize = CGSize()
var kSizeCardTextView               : CGSize = CGSize()

var kFrameCarouselCardFrame         : CGRect = CGRect()
var kFrameCarouselCardImageFrame    : CGRect = CGRect()
var kFrameCarouselCardTextView      : CGRect = CGRect()
