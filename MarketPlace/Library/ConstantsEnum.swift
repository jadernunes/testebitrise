//
//  EnumConstants.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 14/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

enum UnitItemType: Int {
    case Product = 1
    case Service = 2
    case Pizza   = 3
    case Combo   = 4
}

enum UnitType: Int {
    case Service = 1
    case Store = 2
    case HotSite = 3
}

enum StatusOrder: Int {
    case Open               = 0
    case Sent               = 20
    case PaymentConfirmed   = 27
    case Confirmed          = 30
    case Preparing          = 50
    case Finished           = 60
    case ToDelivery         = 70
    case Delivered          = 80
    case Canceled           = 200
}

enum StatusSchedule: Int {
    case Sent       = 20
    case Confirmed  = 30
    case Canceled   = 200
}

enum StatusScheduleDescription: String {
    case Sent       = "Agendamento Recebido"
    case Confirmed  = "Agendamento Confirmado"
    case Canceled   = "Agendamento Cancelado"
}

enum StatusDescription: String {
    case Open       = "Aberto"
    case Sent       = "Pedido Recebido"
    case Confirmed  = "Confirmado"
    case Preparing  = "Em Produção"
    case Finished   = "Pronto para Entrega"
    case ToDelivery = "Enviado para entrega"
    case Delivered  = "Entregue"
    case Canceled   = "Cancelado"
}

enum StatusTracking: String {
    case Open       = "Aberto"
    case Sent       = "Pedido Recebido"
    case Confirmed  = "Confirmado"
    case Preparing  = "Preparando Delícias"
    case Finished   = "Pronto para Entrega"
    case ToDelivery = "Pedido a Caminho"
    case Delivered  = "Entregue"
    case Canceled   = "Cancelado"
}
enum ContentType: String {
    case Order      = "orders"
    case Banner     = "banner"
    case Parking    = "estacionamento"
    case Movie      = "cinema"
    case Grid       = "grid"
    case Info       = "info"
    case Map        = "map"
}

enum SubTypeFact: Int {
    case Event      = 1
    case Promotion  = 2
    case Warning    = 3
    case Cupom      = 4
}

enum TypeObjectDetail: Int {
    case Service    = 1
    case Store      = 2
    case Event      = 3
    case Promotion  = 4
    case Warning    = 5
    case Group      = 6
    case Campaign   = 7
}

enum SectionsMenu: Int {
    case Promos         = 0
    case Stores         = 1
    case Foodcourt      = 2
    case Movies         = 3
    case Services       = 4
    case Events         = 5
    case Directions     = 6
    case OndeParei      = 7
    case PayParking     = 8
    case Orders         = 9
    case Schedules      = 10
    case Profile        = 11
    case Campaign       = 12
    case Notifications  = 13
    case Coupom         = 14
    case Vouchers       = 15
    case Fidelity       = 16
}

enum TypeShowListCoupons: Int {
    case meCoupons      = 0
    case allCupons      = 1
}

enum TypeDiscountCampaign: Int {
    case price      = 1
    case percentage = 2
}

enum PushesType: Int {
    case BeautyStatus     = 1
    case Generic          = 2
    case LineStatus       = 3
    case OrderStatus      = 4
    case PaymentStatus    = 5
    case PromoStatus      = 6
    case MakePayment      = 7
}

enum DeviceModel : Int {
    case notSupported   = 0
    case iPhone4        = 1
    case iPhone4s       = 2
    case iPhone5        = 3
    case iPhone5c       = 4
    case iPhone5s       = 5
    case iPhoneSE       = 6
    case iPhone6        = 7
    case iPhone6Plus    = 8
    case iPhone6s       = 9
    case iPhone6sPlus   = 10
    case iPhone7        = 11
    case iPhone7plus    = 12
    case iPodTouch5     = 13
    case iPodTouch6     = 14
}

enum ShortcutItem: String {
    case ShortcutScanQR                 = "com.fourall.mktplace.stotal.scanQR"
    case ShortcutOndeParei              = "com.fourall.mktplace.stotal.ondeParei"
    case ShortcutPagarEstacionamento    = "com.fourall.mktplace.stotal.PagarEstacionamento"
}

enum TypePageSuccess : Int {
    case PagamentoTicket    = 1
    case PagamentoQR        = 2
    case PagamentoPedido    = 3
    case Schedule           = 4
    case PagamentoCancelado = 5
    case PagamentoVoucher   = 6
}

enum SubContentType : String {
    case Promo      = "promo"
    case Event      = "event"
    case Warning    = "warning"
    case Store      = "store"
    case Movie      = "movie"
    case Gastronomy = "gastronomy"
    case Foodcourt  = "foodcourt"
    case Service    = "service"
    case Campaign   = "campaign"
}

enum ContentTypeEvent: Int {
    case ImageBanner    = 0
    case Button         = 1
    case Description    = 2
    case TicketAccess   = 3
    case Ticket         = 4
}

enum DimensionCustom: UInt {
    case Account4all    = 1
    case Latitude       = 2
    case Longitude      = 3
    case Commercial     = 4
    case Product        = 5
}

enum VoucherStatus : Int {
    case Created    = 1
    case Used       = 2
    case Cancelled  = 3
    case Expired    = 4
}

enum VoucherType : Int {
    case Cinema     = 1
    case Event      = 2
    case Coupon     = 3
    case Order      = 4
    case Fidelity   = 5
}

enum ProductCategoryType: Int {
    case invalid        = 0
    case defaultType    = 1
    case extra          = 2
}

enum UnityItemModifierPriceType: Int {
    case sum_total      = 1
    case not_sum_total  = 2
}

enum TypeQRPayment : String {
    case Campaign = "CPN"
}

enum OrderType :Int {
    case None           = 0
    case TakeAway       = 1
    case Delivery       = 2
    case ShortDelivery  = 3
    case Voucher        = 4
    case Check          = 5
    case InStore        = 6
}

struct NearestUnity {
    var minimumDistance: Double     = 1000 //1km
    var unity                       : Unity? = nil
    var distance                    : Double = 0
    
    mutating func reset() {
        distance = 0.0
        unity = nil
        
    }
}

enum kToastType: Int {
    case success                       = 0
    case warning                       = 1
    case error                         = 2
}

enum kCellProfileIdentifier: Int {
    
    case kPedidos                      = 0
    case kCredenciais                  = 1//Vouchers
    case kFidelidades                  = 2
    case kCupons                       = 3
    case kExtratos                     = 4
    case kSignatures                   = 5
    case kPersonalData                 = 6
    case kCards                        = 7
    case kProfileFamily                = 8
    case kConfig                       = 9
    case kHelp                         = 10
    case kAbout                        = 11
    case kLogout                       = 12
    case kLogin                        = 13
    case kMenuGeral                    = 14
    
    func simpleDescription() -> String {
        switch self {
        case .kCredenciais:
            return "cellCredenciais"
        case .kFidelidades:
            return "cellFidelidades"
        case .kCupons:
            return "cellCupons"
        case .kExtratos:
            return "cellExtratos"
        case .kSignatures:
            return "cellSignatures"
        case .kPersonalData:
            return "cellPersonalData"
        case .kCards:
            return "cellCards"
        case .kProfileFamily:
            return "cellProfileFamily"
        case .kConfig:
            return "cellConfig"
        case .kHelp:
            return "cellHelp"
        case .kAbout:
            return "cellAbout"
        case .kLogout:
            return "cellLogout"
        case .kLogin:
            return "cellLogin"
        case .kMenuGeral:
            return "cellMenu"
        case .kPedidos:
            return "cellPedido"
        }
        
    }
    
    func title() -> String {
        switch self {
        case .kPedidos:
            return "Meus Pedidos"
        case .kCredenciais:
            return "Meus Vouchers"
        case .kFidelidades:
            return "Minhas Fidelidades"
        case .kCupons:
            return "Meus Cupons"
        case .kExtratos:
            return "Extratos"
        case .kSignatures:
            return "Assinaturas"
        case .kPersonalData:
            return "Dados pessoais"
        case .kCards:
            return "Cartões"
        case .kProfileFamily:
            return "Perfil família"
        case .kConfig:
            return "Configurações"
        case .kHelp:
            return "Ajuda"
        case .kAbout:
            return "Sobre"
        case .kLogout:
            return "Logout"
        case .kLogin:
            return "Fazer login na Conta 4all"
        case .kMenuGeral:
            return ""
        }
    }
    
    func icon() -> String {
        switch self {
        case .kPedidos:
            return "meusPedidos"
        case .kCredenciais:
            return "conta4all-icone-minhasCredenciais"
        case .kFidelidades:
            return "conta4all-icone-meusCartoesFidelidade"
        case .kCupons:
            return "conta4all-icone-promocoesCadastradas"
        case .kExtratos:
            return "iconChartProfile"
        case .kSignatures:
            return "iconSubscription"
        case .kPersonalData:
            return "iconUserProfile"
        case .kCards:
            return "iconCardProfile"
        case .kProfileFamily:
            return "iconFamilyProfile"
        case .kConfig:
            return "iconSettingsProfile"
        case .kHelp:
            return "iconHelp"
        case .kAbout:
            return "iconInfoProfile"
        case .kLogout:
            return "iconLogoutAccount4all"
        case .kLogin:
            return "123-id-card"
        case .kMenuGeral:
            return ""
        }
    }
}

enum ActionIterativeMessage: Int {
    case TwoOptions = 0, OneOption
}

enum kAppType: Int {
    case food           = 0
    case fun            = 1
    case shoppingTotal  = 2
    case fourAll        = 3
}

