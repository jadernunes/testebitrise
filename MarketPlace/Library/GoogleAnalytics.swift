//
//  GoogleAnalytics.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 13/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class GoogleAnalytics {
    
    static let sharedInstance = GoogleAnalytics()
    
    init() {
        startConfiguration()
    }
    
    private func startConfiguration() {
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
        gai.dispatchInterval = 5
        gai.logger.logLevel = GAILogLevel.Error  // remove before app release
        gai.trackerWithTrackingId(KEY_GOOGLE_ANALYTICS)
    }
    
    private func trakingWithDimensionCustomByTracker(tracker: GAITracker, id4all: String = "", idComercial: String = "", idProduct: String = "") {
        
        if id4all.characters.count > 0 {
            tracker.set(GAIFields.customDimensionForIndex(DimensionCustom.Account4all.rawValue), value: id4all)
        }
        
        if idComercial.characters.count > 0 {
            tracker.set(GAIFields.customDimensionForIndex(DimensionCustom.Commercial.rawValue), value: idComercial)
        }
        
        if idProduct.characters.count > 0 {
            tracker.set(GAIFields.customDimensionForIndex(DimensionCustom.Product.rawValue), value: idProduct)
        }
        
        SystemInfo.getLocationInfo { (latitude, longitude, timestamp) in
            tracker.set(GAIFields.customDimensionForIndex(DimensionCustom.Latitude.rawValue), value: latitude)
            tracker.set(GAIFields.customDimensionForIndex(DimensionCustom.Longitude.rawValue), value: longitude)
        }
    }
    
    private func baseTracking(screenName:String = "", idComercial: String = "", idProduct: String = "") {
        
        let lib4all = Lib4all.sharedInstance()
        let tracker = GAI.sharedInstance().defaultTracker
        var userID = "Guest"
        
        if lib4all.hasUserLogged() {
            let userData = lib4all.getAccountData() as NSDictionary
            if userData.objectForKey("customerId") != nil {
                userID = userData.objectForKey("customerId") as! String
            }
        }
        
        tracker.set(kGAIScreenName, value: screenName)
        tracker.set(kGAIUserId, value: userID)
        
        //Hit a dimension custom
        self.trakingWithDimensionCustomByTracker(tracker, id4all: userID, idComercial: idComercial, idProduct: idProduct)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func trakingScreenWithName(name:String!) {
        self.baseTracking(name)
    }
    
    func trakingScreenWithNameAndIdUnity(name:String!, idUnity:String!) {
        self.baseTracking(name, idComercial: idUnity)
    }
}
