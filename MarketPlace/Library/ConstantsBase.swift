//
//  Constants.swift
//  MarketPlace
//
//  Created by Luciano on 6/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

//Layout
let pathLayout = NSBundle.mainBundle().pathForResource("Config", ofType: "plist")
let dicInfoLayout = NSDictionary(contentsOfFile: pathLayout!)
let dicLayout = dicInfoLayout?.objectForKey("Layout")
let baseColor = dicLayout?.objectForKey("baseColor") as! String

//General Info
let path        = NSBundle.mainBundle().pathForResource("Info", ofType: "plist")
let dicInfo     = NSDictionary(contentsOfFile: path!)
let dicConfig   = dicInfo?.objectForKey("CONFIG")

//Locations
let pathLocation    = NSBundle.mainBundle().pathForResource("Locations", ofType: "plist")
let dicLocation     = NSDictionary(contentsOfFile: pathLocation!)
let dicPlacesList   = dicLocation?.objectForKey("PlacesList") as! [[String:AnyObject]]

let dicPlaceInfo    = dicPlacesList.first!["PlaceInfo"] as? [String:AnyObject]
let dicCenter       = dicPlaceInfo?["center"]

let latitude        = dicCenter?["latitude"]  as! Double
let longitude       = dicCenter?["longitude"] as! Double
let nameLocation    = dicPlaceInfo?["name"]   as! String

//Config Menu
let pathConfigMenu          = NSBundle.mainBundle().pathForResource("CONFIG_MENU", ofType: "plist")
let pathConfigTabBarHome    = NSBundle.mainBundle().pathForResource("CONFIG_HOME", ofType: "plist")
let dicInfoConfigMenu       = NSDictionary(contentsOfFile: pathConfigMenu!)
let dicInfoConfigTabBarHome = NSDictionary(contentsOfFile: pathConfigTabBarHome!)
let LIST_SECTIONS_SIDE_MENU = dicInfoConfigMenu?.objectForKey("Cells") as! NSArray
let LIST_COLOR_SIDE_MENU    = dicInfoConfigMenu?.objectForKey("View") as! NSDictionary
let LIST_TAB_BAR_HOME       = dicInfoConfigTabBarHome?.objectForKey("Tabs") as! NSArray
let dicAccount4All          = dicConfig?.objectForKey("ACCOUNT_4ALL") as! NSDictionary
let dicMovieConfig          = dicConfig?.objectForKey("CONFIG_MOVIE") as! NSDictionary
let isOneMarketplace        = dicConfig?.objectForKey("isOneMarketplace") as! Bool

let BASE_URL                = dicConfig?.objectForKey("URL_BASE_API") as! String
let BASE_URL_IMAGES         = dicConfig?.objectForKey("URL_BASE_IMAGE") as! String
let TOKEN_AUTHORIZATION     = dicConfig?.objectForKey("authorization") as! String
let BASE_URL_MAP            = dicConfig?.objectForKey("URL_BASE_MAP") as! String
let KEY_GOOGLE_ANALYTICS    = dicConfig?.objectForKey("KEY_GOOGLE_ANALYTICS") as! String
let KEY_GOOGLE_MAPS         = dicConfig?.objectForKey("KEY_GOOGLE_MAPS") as! String
let URL_MOVIE               = dicMovieConfig.objectForKey("URL_MOVIE") as? String
let CAN_BYE_TIKET           = dicMovieConfig.objectForKey("CAN_BYE_TIKET") as! Bool

//Account 4all
let C4ALL_BASE_URL          = dicAccount4All.objectForKey("URL_BASE_4ALL") as! String
let ENVIROMENT_4ALL         = dicAccount4All.objectForKey("ENVIROMENT_4ALL") as! Int
let APPLICATION_ID          = dicAccount4All.objectForKey("APPLICATION_ID") as! String

//APP RECARGA
let URL_WEB_RECARGA         = dicConfig?.objectForKey("URL_WEB_RECARGA") as! String

//4Park
let keyAPIServices          = "AIzaSyCSIUBw87E8G7gGnVmgQSq5iOzw7YhmkjU"
let keyAPIPlacesClient      = "AIzaSyCSIUBw87E8G7gGnVmgQSq5iOzw7YhmkjU"

let isDesenvMode            = dicConfig?.objectForKey("IS_DESENV_MODE") as! Bool

//FACT TYPE
let FACT_TYPE_EVENT = 1
let FACT_TYPE_PROMO = 2

//ID
let ID_MARKETPLACE = "2"

//ORDER STATUS
let STATUS_SENT             = 20
let STATUS_CONFIRMED        = 30
let STATUS_IN_PRODUCTION    = 50
let STATUS_TO_DELIVERY      = 70
let STATUS_DELIVERED        = 80
let STATUS_CANCELED         = 200

//HTTP RESPONSE
let HTTP_RESPONSE_CREATED = 201

//NOTIFICATION OBSERVER
let NOTIF_UPDATE_LABELS = "UpdateCartLabels"
let NOTIF_CART_OBSERVER = "UpdateCartControllerContent"
let NOTIF_HOME_CART_OBSERVER = "UpdateInOrderStatus"
let UNITY_ITEMS_OBSERVER = "UpdatedUnityItemsInUnity"
let UNITY_FIDELITY_OBSERVER = "UpdatedUnityFidelity"
let CELL_BUTTON_ITEMS_CLICKED = "CellUnityItemsClicked"
let UNITY_SCHEDULE_OBSERVER = "UpdateUnitySchedule"
let NOTIF_CART_CHANGED = "UpdatedCartInListController"
let NOTIF_SCHEDULE_UPDATE = "UpdateInScheduleStatus"
let NOTIF_PUSHES_UPDATE = "UpdatePushesContent"

// PAYMENT STATUS
let PAYMENT_CONFIRMED = 3

// TICKET EVENT
let MAX_TICKETS_EVENT = 3
