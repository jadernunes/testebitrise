//
//  Extensions.swift
//  Shopping Total
//
//  Created by Luciano on 6/21/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Extensions: NSObject {

}

extension UIButton {
    
    func setIcon(image : UIImage, inLeft : Bool, margin : Int = 0) -> Void {
        
        self.viewWithTag(7)?.removeFromSuperview()
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let imgIcon             = UIImageView(frame: CGRectMake(0, 0, self.frame.size.height - 5, self.frame.size.height - 10))
        imgIcon.image           = image
        imgIcon.tag             = 7
        imgIcon.contentMode     = .ScaleAspectFill
        
        if inLeft {
            imgIcon.center = CGPointMake(self.titleLabel!.frame.origin.x - ((imgIcon.frame.size.width / 2) + CGFloat(margin)), self.frame.size.height / 2);
        } else {
            imgIcon.center = CGPointMake((self.frame.origin.x + self.frame.size.width) - imgIcon.frame.size.width, self.frame.size.height / 2);
        }
        
        self.tintColor = SessionManager.sharedInstance.cardColor
        
        self.addSubview(imgIcon)
    }
    
    func addActivityIndicator(inLeft : Bool) -> Void {
    
        self.viewWithTag(8)?.removeFromSuperview()
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, self.frame.size.height - 5, self.frame.size.height - 10))
        activityIndicator.tag             = 8
        activityIndicator.startAnimating()
        
        if inLeft {
            activityIndicator.center = CGPointMake(self.titleLabel!.frame.origin.x - ((activityIndicator.frame.size.width / 2)), self.frame.size.height / 2);
        } else {
            activityIndicator.center = CGPointMake((self.frame.origin.x + self.frame.size.width) - activityIndicator.frame.size.width, self.frame.size.height / 2);
        }
        
        self.tintColor = SessionManager.sharedInstance.cardColor
        self.addSubview(activityIndicator)
    }
    
    func tintButton(color: UIColor, image: UIImage) {
        let origImage = image
        let tintedImage = origImage.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.setImage(tintedImage, forState: .Normal)
        self.tintColor = color
    }
    
    func setIconWithCoordinates(image : UIImage?) -> Void {
        
        if let _ = image {
            self.viewWithTag(7)?.removeFromSuperview()
            
            let imgIcon             = UIImageView(frame: CGRectMake(0, 0, self.frame.width - 1, self.frame.width - 1))
            imgIcon.image           = image
            imgIcon.tag             = 7
            imgIcon.contentMode     = .ScaleAspectFill
            imgIcon.center = CGPointMake((self.titleLabel!.frame.origin.x - self.frame.origin.x)/2+imgIcon.frame.size.width*2/3, 4/5*self.frame.size.height);
            
            self.tintColor = SessionManager.sharedInstance.cardColor
            self.addSubview(imgIcon)
        }
    }
    
}

extension Object {
    func toDictionary() -> NSDictionary {
        let properties = self.objectSchema.properties.map { $0.name }
        let dictionary = self.dictionaryWithValuesForKeys(properties)
        
        let mutabledic = NSMutableDictionary()
        mutabledic.setValuesForKeysWithDictionary(dictionary)
        
        for prop in self.objectSchema.properties as [Property]! {
            // find lists
            if let nestedObject = self[prop.name] as? Object {
                mutabledic.setValue(nestedObject.toDictionary(), forKey: prop.name)
            } else if let nestedListObject = self[prop.name] as? ListBase {
                var objects = [AnyObject]()
                for index in 0..<nestedListObject._rlmArray.count  {
                    let object = nestedListObject._rlmArray[index] as AnyObject
                    objects.append((object as! NSDictionary))
                }
                mutabledic.setObject(objects, forKey: prop.name)
            }
            
        }
        return mutabledic
    }
    
}

extension Results {
    
    func get <T:Object> (offset: Int, limit: Int ) -> Array<T>{
        //create variables
        var lim = 0 // how much to take
        var off = 0 // start from
        var l: Array<T> = Array<T>() // results list
        
        //check indexes
        if off<=offset && offset<self.count - 1 {
            off = offset
        }
        if limit > self.count {
            lim = self.count
        }else{
            lim = limit
        }
        
        //do slicing
        for i in off..<lim{
            let dog = self[i] as! T
            l.append(dog)
        }
        
        //results
        return l
    }
}
