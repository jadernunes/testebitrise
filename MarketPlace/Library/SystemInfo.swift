//
//  SystemInfo.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import Locksmith
import CoreLocation

class SystemInfo {
    
    static func getUserUuid() -> String{
        let uuid: String?
        let dic = Locksmith.loadDataForUserAccount("com.4all")
        
        if dic != nil {
            if (dic! as NSDictionary).objectForKey("uuid") == nil {
                let strApplicationUUID = UIDevice.currentDevice().identifierForVendor!.UUIDString
                try! Locksmith.saveData(["uuid": strApplicationUUID], forUserAccount: "com.4all")
            }
        } else {
            
            if UIDevice.currentDevice().identifierForVendor != nil {
                let strApplicationUUID = UIDevice.currentDevice().identifierForVendor!.UUIDString
                try! Locksmith.saveData(["uuid": strApplicationUUID], forUserAccount: "com.4all")
            } else {
                let strApplicationUUID = NSUUID().UUIDString
                try! Locksmith.saveData(["uuid": strApplicationUUID], forUserAccount: "com.4all")
            }
        }
        
        let newDic = Locksmith.loadDataForUserAccount("com.4all")
        uuid = (newDic! as NSDictionary).objectForKey("uuid") as? String
        return uuid!;
    }
    
    static func getDevice() -> String
    {
        return UIDevice.currentDevice().name
    }
    
    static func getSystemName() -> String
    {
        return UIDevice.currentDevice().systemName
    }
    
    static func getSystemVersion() -> String
    {
        return UIDevice.currentDevice().systemVersion
    }
    
    static func getDeviceUUID() -> String
    {
        return UIDevice.currentDevice().identifierForVendor!.UUIDString
    }
    
    static func getLocationStringInfo(completionHandler: (lCity:String, lState:String) -> Void){
        dispatch_async(dispatch_get_main_queue()) {
            if CLLocationManager.locationServicesEnabled() {
                if CLLocationManager.authorizationStatus() == .AuthorizedAlways {
                    let locationManager = CLLocationManager()
                    if let location = locationManager.location {
                        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                            
                            if error != nil {
                                return
                            }
                            
                            if placemarks!.count > 0 {
                                let pm = placemarks![0]
                                if let city = pm.locality {
                                    if let state = pm.administrativeArea {
                                        completionHandler(lCity: city, lState: state)
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    static func getLocationInfo(completion: (latitude: String, longitude: String, timestamp: String) -> ()) -> () {
        var latitude    = ""
        var longitude   = ""
        var timestamp   = ""
        
        dispatch_async(dispatch_get_main_queue()) {
            if CLLocationManager.locationServicesEnabled() {
                if CLLocationManager.authorizationStatus() == .AuthorizedAlways {
                    if let location = CLLocationManager().location {
                        latitude = String(location.coordinate.latitude)
                        longitude = String(location.coordinate.longitude)
                        timestamp = String(location.timestamp)
                    }
                }
            }
            completion(latitude: latitude, longitude: longitude, timestamp: timestamp)
        }
    }
    
    static func getBuild() -> String
    {
        let dictionary = NSBundle.mainBundle().infoDictionary!
        let build = dictionary["CFBundleVersion"] as! String
        
        return build
    }
    
    static func getVersionApp() -> String {
        let dictionary = NSBundle.mainBundle().infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        return version
    }
    
    static func getSystemInfo() -> String
    {
        let dictionary = NSBundle.mainBundle().infoDictionary!
        
        var targeName = dictionary["CFBundleName"] as! String
        targeName = NSString.removeSpecialCharacters(targeName)
        
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        let systemName = SystemInfo.getSystemName() as String
        let systemVersion = SystemInfo.getSystemVersion() as String
        let uuid = SystemInfo.getUserUuid() as String
        
        var latitude = "0"
        var longitude = "0"
        var timestamp = "0"
        
        self.getLocationInfo { (latitude_, longitude_, timestamp_) in
            latitude = latitude_
            longitude = longitude_
            timestamp = timestamp_
        }

        let systemInfo = targeName + "/" + version + "(" + "\(build);\(uuid);\(systemName);\(systemVersion);\(latitude);\(longitude);\(timestamp)" + ")"
        
        return systemInfo
    }
}
