//
//  EventDetailsSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import EventKit

class EventDetailsSource : NSObject {

    var tableView:                      UITableView!
    var fact:                           Fact!
    var rootVc:                         UIViewController!
    var heightForTicketList:            CGFloat!
    
    init(tableView: UITableView, fact: Fact!, rootVc: UIViewController) {
        super.init()
        
        self.fact      = fact
        self.tableView = tableView
        self.rootVc    = rootVc
        
        self.tableView.dataSource       = self
        self.tableView.delegate         = self
        self.tableView.backgroundColor  = LayoutManager.backgroundFirstColor
        
        self.tableView.registerNib(UINib(nibName: "BannerImageTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BANNER_IMAGE_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "DescriptionTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: DESCRIPTION_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "ButtonTableViewCell",      bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BUTTON_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "TicketButtonTableViewCell",      bundle: NSBundle.mainBundle()), forCellReuseIdentifier: ACCESS_TICKET_BUTTON)
        self.tableView.registerNib(UINib(nibName: "TicketsListTableViewCell_",
            bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_LIST_TICKETS)
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: CELL_LIST_UNITIES_IN_FACT)
    }
    
    //MARK: - Register event
    private func registerEventOnCalendar(event: EKEvent!, store: EKEventStore!) {
        
        event.endDate = event.startDate.dateByAddingTimeInterval(60*60) //1 hour long meeting
        event.calendar = store.defaultCalendarForNewEvents
        
        do {
            try store.saveEvent(event, span: .ThisEvent, commit: true)
            Util.showAlert(self.rootVc, title: kAlertCalendarTitle, message: kMessageEventAddedToCalendar)
            //self.savedEventId = event.eventIdentifier //save event id to access this particular event later
        } catch {
            // Display error to user
            Util.showAlert(self.rootVc, title: kAlertTitle, message: kMessageUnableToExecuteAction)
        }
    }
    
    func configureCellListUnities(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellListUnities = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_UNITIES_IN_FACT, forIndexPath: indexPath)
        cellListUnities.backgroundColor = UIColor.clearColor()
        
        let tableViewListUnities = ListUnitiesTableViewController.instanceFromNib()
        let heightHeader = CGFloat(self.fact.unities.count) > 0 ? CGFloat(self.fact.unities.count * 100 + 60) : 0
        let frameHeader = CGRectMake(0, 0, self.rootVc.view.frame.size.width, heightHeader)
        tableViewListUnities.frame = frameHeader
        
        tableViewListUnities.referenceSuperViewController = self.rootVc
        tableViewListUnities.listUnities = self.fact.unities
        
        dispatch_async(dispatch_get_main_queue(), {
            cellListUnities.addSubview(tableViewListUnities)
        })
        
        return cellListUnities
    }
}

// MARK: - UITableViewDataSource Methods
extension EventDetailsSource : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fact.hasVoucher {
            return 6
        }
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let typeCell = ContentTypeEvent(rawValue: indexPath.row)?.rawValue
        
        if typeCell == ContentTypeEvent.ImageBanner.rawValue {
            return BannerImageTableViewCell.getConfiguredCell(self.tableView, fact: fact, showDetailsBanner: false)
        } else if typeCell == ContentTypeEvent.Description.rawValue {
            let cellDescription = DescriptionTableViewCell.getConfiguredCell(self.tableView, fact: fact,styleDark: true)
            cellDescription.marginLeft.constant = 70
            return cellDescription
        } else if typeCell == ContentTypeEvent.Button.rawValue {
            let cellButton = ButtonTableViewCell.getConfiguredCell(self.tableView, fact: fact)
            cellButton.constraintLeftMargin.constant = 70
            return cellButton
        }else if typeCell == ContentTypeEvent.Ticket.rawValue {
            let cellTicketsList = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_TICKETS) as! TicketsListTableViewCell_
            cellTicketsList.configureWithFact(fact, rootViewController: self.rootVc)
            heightForTicketList = cellTicketsList.superCellHeight
            return cellTicketsList
        } else if typeCell == ContentTypeEvent.TicketAccess.rawValue {
            if fact.hasVoucher {
                let cellTicketButton = TicketButtonTableViewCell.getConfiguredCell(self.tableView, fact: fact, superViewController: self.rootVc)
                cellTicketButton.marginLeft.constant = 70
                return cellTicketButton
            } else {
                return self.configureCellListUnities(tableView, cellForRowAtIndexPath: indexPath)
            }
        } else {
            return self.configureCellListUnities(tableView, cellForRowAtIndexPath: indexPath)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let typeCell = ContentTypeEvent(rawValue: indexPath.row)?.rawValue {
            if typeCell == ContentTypeEvent.ImageBanner.rawValue {
                return 350
            } else if typeCell == ContentTypeEvent.Description.rawValue {
                return 100 + (CGFloat(fact.desc.characters.count)/2.0)
            } else if typeCell == ContentTypeEvent.Button.rawValue {
                return 60
            } else if typeCell == ContentTypeEvent.TicketAccess.rawValue {
                if fact.hasVoucher {
                    return 80
                } else {
                    let newHeight = CGFloat(self.fact.unities.count) > 0 ? CGFloat(self.fact.unities.count * 100 + 60) : 0
                    return newHeight
                }
            } else if typeCell == ContentTypeEvent.Ticket.rawValue {
                return CGFloat(heightForTicketList ?? 0.0)
            } else {
                return 0
            }
        } else {
            let newHeight = CGFloat(self.fact.unities.count) > 0 ? CGFloat(self.fact.unities.count * 100 + 60) : 0
            return newHeight
        }
    }
}

// MARK: - UITableViewDelegate Methods
extension EventDetailsSource : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .Button:
            
            let store = EKEventStore()
            let event = EKEvent(eventStore: store)
            let timestampEnd = Util.timeStampToNSDate(Double(fact.timestampEnd))
            event.title = fact.title
            event.startDate = timestampEnd //today
            
            store.requestAccessToEntityType(.Event) {(granted, error) in
                if !granted { return }
                
                self.registerEventOnCalendar(event, store: store)
            }
        default:
            break
        }
    }
}

extension DetailsTableViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        print("Scrolling")
    }
}
