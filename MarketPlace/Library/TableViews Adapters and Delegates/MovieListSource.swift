//
//  MovieListSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class MovieListSource: NSObject , UITableViewDataSource, UITableViewDelegate{
    
    var tableView    : UITableView!
    var rootVc       : UIViewController!
    
    init(tableView : UITableView, rootVc : UIViewController) {
        super.init()
        
        self.tableView      = tableView
        self.rootVc         = rootVc

        self.tableView.registerNib(UINib(nibName: "AllMoviesTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: ALL_MOVIES_IDENTIFIER)
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier(ALL_MOVIES_IDENTIFIER)! as! AllMoviesTableViewCell
        cell.rootVc = rootVc
        
        cell.collectView.reloadData()
        cell.collectView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    //MARK: - Navigation
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rootVc.view.frame.height
    }
    
    func callList(group: Group) -> Void{
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        
        listVc.group = group
        listVc.title = group.name
        if group.groupTreeTypeName != nil {
            listVc.subType = SubContentType(rawValue: group.groupTreeTypeName!)
        }
        
        rootVc.navigationController?.pushViewController(listVc, animated: true)
    }
    
    func callDetails(unity: Unity?, group: Group?) -> Void{
        
    }
}
