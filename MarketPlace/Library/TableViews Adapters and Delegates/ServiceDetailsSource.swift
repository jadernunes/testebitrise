//
//  EventDetailsSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import EventKit

class EventDetailsSource: NSObject, UITableViewDataSource, UITableViewDelegate{

    let arrContent = NSMutableArray()
    var tableView  : UITableView!
    var fact       : Fact!
    var rootVc     : UIViewController!
    
    init(tableView : UITableView, fact : Fact!, rootVc : UIViewController) {
        super.init()
        
        self.fact      = fact
        self.tableView = tableView
        self.rootVc    = rootVc
        self.tableView.dataSource       = self
        self.tableView.delegate         = self
        self.tableView.backgroundColor  = LayoutManager.backgroundDark
        self.tableView.registerNib(UINib(nibName: "BannerImageTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BANNER_IMAGE_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "DescriptionTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: DESCRIPTION_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "ButtonTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BUTTON_IDENTIFIER)

    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .ImageBanner:
            return BannerImageTableViewCell.getConfiguredCell(self.tableView, fact: fact, showDetailsBanner: false)
        case .Description:
            return DescriptionTableViewCell.getConfiguredCell(self.tableView, fact: fact,styleDark: true)
        case.Button:
            return ButtonTableViewCell.getConfiguredCell(self.tableView, fact: fact)
        }    
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .Button:
            
            let timestampEnd = Util.timeStampToNSDate(Double(fact.timestampEnd))

            
            let store = EKEventStore()
            store.requestAccessToEntityType(.Event) {(granted, error) in
                if !granted { return }
                let event = EKEvent(eventStore: store)
                event.title = self.fact.title
                event.startDate = timestampEnd //today
                event.endDate = event.startDate.dateByAddingTimeInterval(60*60) //1 hour long meeting
                event.calendar = store.defaultCalendarForNewEvents
                do {
                    try store.saveEvent(event, span: .ThisEvent, commit: true)
                    Util.showAlert(self.rootVc, title: "Calendário", message: "Evento adicionado com sucesso.")
                    //self.savedEventId = event.eventIdentifier //save event id to access this particular event later
                } catch {
                    // Display error to user
                    Util.showAlert(self.rootVc, title: "Atenção", message: "Não foi possível completar a ação.")
                }
            }
        default:
            break
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch ContentTypeEvent(rawValue: indexPath.row)! {
        case .ImageBanner:
            return 370
        case .Button:
            return 60
        case .Description:
            return 160
        }
    }

    
    //MARK: - Enums
    enum ContentTypeEvent: Int {
        case ImageBanner    = 0
        case Button         = 1
        case Description    = 2
    }

    
}
