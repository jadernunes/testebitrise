//
//  UnityListSource.swft
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import CoreLocation
import RealmSwift

class UnityListSource: NSObject, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    var tableView               : UITableView!
    var group                   : Group!
    var showChildren            = true
    var rootVc                  : UIViewController!
    var listUnitiesToSort       = [Unity]()
    var isListAllItems          : Bool!
    let listUnitiesSorted : NSMutableArray = []
    let locationManager = CLLocationManager()

    init(tableView : UITableView, rootVc : UIViewController, group: Group, showChildren : Bool, listAllItems: Bool) {
        super.init()
        
        self.tableView      = tableView
        self.rootVc         = rootVc
        self.showChildren   = showChildren
        self.group          = group
        self.isListAllItems = listAllItems
        
        self.tableView.registerNib(UINib(nibName: "StoreBannerTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: STORE_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "StandardTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: STANDARD_IDENTIFIER)
        let sortDescriptors = [NSSortDescriptor(key: "soon", ascending: true),
                               NSSortDescriptor(key: "opened", ascending: false),
                               NSSortDescriptor(key: "fourPayEnabled", ascending: false),
                               NSSortDescriptor(key: "orderEnabled", ascending: false),
                               NSSortDescriptor(key: "schedulingEnabled", ascending: false),
                               NSSortDescriptor(key: "name", ascending: true),
                               NSSortDescriptor(key: "distanceFromUser", ascending: true)]
        
        let unitiesWithProducts = group.unities.filter(String(format: "orderEnabled == true || orderDeliveryEnabled == true || orderShortDeliveryEnabled == true || orderTakeAwayEnabled == true || orderVoucherEnabled == true || orderCheckEnabled == true"))

        if !isOneMarketplace { //4all
            self.listUnitiesToSort.appendContentsOf(unitiesWithProducts)
        } else {
            self.listUnitiesToSort.appendContentsOf(group.unities)
        }
        
        self.listUnitiesSorted.addObjectsFromArray(listUnitiesToSort)
        self.listUnitiesSorted.sortUsingDescriptors(sortDescriptors)
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.distanceFilter  = CLLocationDistance(100)
            locationManager.startUpdatingLocation()
        }
    }
    
    //Cheking without data
    private func checkWithoutData(countItems: Int) {
        Util.checkWithoutData(rootVc.view, tableView: self.tableView, countData: countItems, message: kMessageWithoutDataComercial)
    }
    
    private func indexToAnimated (index:NSIndexPath, tableview: UITableView) -> Bool {
        
    
        return true
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if showChildren == true {
            return StandardTableViewCell.getConfiguredCell(tableView, group: group.children[indexPath.row])
        }else{
            
            /*
             if group.idGroupTreeType == 20 {
                return StoreBannerTableViewCell.getConfiguredCell(tableView, unity: listUnitiesSorted[indexPath.row])
             }
             */
            return StoreBannerTableViewCell.getConfiguredCell(tableView, unity: listUnitiesSorted[indexPath.row] as! Unity)
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        Util.animateCell(cell, indexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if showChildren == true {
            
            if group.children[indexPath.row].children.count > 0 || group.children[indexPath.row].unities.count > 0{
                callList(group.children[indexPath.row])
            }else{
                callDetails(nil, group: group.children[indexPath.row])
            }
        }
        else
        {
            var unity = listUnitiesSorted[indexPath.row] as! Unity
            
            if(unity.idUnityType == UnitType.HotSite.rawValue && unity.siteUrl?.characters.count > 0)
            {
                callHotSite(unity)
            }
            else
            {
                if isListAllItems == true {
                    unity = listUnitiesSorted[indexPath.row] as! Unity
                }
                
                callDetails(unity, group: nil)
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var countItems = listUnitiesSorted.count
        if showChildren == true {
            countItems = group.children.count
        }
        
        return countItems
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if showChildren == true {
            return 46
        }else{
            return 200
        }
    }
    
    //MARK: - Navigation
    func callList(group: Group) -> Void{
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        
        listVc.group = group
        listVc.title = group.name
        listVc.titleToUse = group.name
        if group.groupTreeTypeName != nil {
            listVc.subType = SubContentType(rawValue: group.groupTreeTypeName!)
        }
        
        rootVc.navigationController?.pushViewController(listVc, animated: true)
    }
    
    func callDetails(unity: Unity?, group: Group?) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        if (unity != nil) {
            detailsVc.title = unity?.name
            detailsVc.idObjectReceived = unity?.id
            detailsVc.typeObjet = TypeObjectDetail.Store
        } else {
            detailsVc.title = group?.name
            detailsVc.idObjectReceived = group?.id
            detailsVc.typeObjet = TypeObjectDetail.Group
        }
        
        rootVc.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    func callHotSite(unity: Unity?)
    {
        let hotSiteWebViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("mapShopping") as! UINavigationController
        (hotSiteWebViewController.viewControllers[0] as! MapWebViewController).urlString = unity?.siteUrl
        
        rootVc.navigationController?.presentViewController(hotSiteWebViewController, animated: true, completion: nil)
    }
    
    //MARK: - Location Delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocation = manager.location!
        UnityPersistence.updateLocationFromUser(listUnitiesToSort, location: locValue)
        
        let sortDescriptors = [NSSortDescriptor(key: "soon", ascending: true),NSSortDescriptor(key: "fourPayEnabled", ascending: false),NSSortDescriptor(key: "orderEnabled", ascending: false),NSSortDescriptor(key: "schedulingEnabled", ascending: false),NSSortDescriptor(key: "distanceFromUser", ascending: true)]
        self.listUnitiesSorted.removeAllObjects()
        self.listUnitiesSorted.addObjectsFromArray(listUnitiesToSort)
        self.listUnitiesSorted.sortUsingDescriptors(sortDescriptors)
        self.tableView.reloadData()
    }
    
}
