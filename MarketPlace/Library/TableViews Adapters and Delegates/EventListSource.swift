//
//  EventListSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/14/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class EventListSource: NSObject , UITableViewDataSource, UITableViewDelegate{
    
    var tableView           : UITableView!
    var listEvents          = NSMutableArray()
    var showUpcomingEvents  = true
    var rootVc              : UIViewController!

    init(tableView : UITableView, showUpcoming : Bool, referenceViewController : UIViewController) {
        super.init()
        
        self.tableView          = tableView
        self.listEvents         = FactEntityManager.getFacts(FACT_TYPE_EVENT)
        self.showUpcomingEvents = showUpcoming
        self.rootVc             = referenceViewController
        
        self.tableView.registerNib(UINib(nibName: "BannerImageTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BANNER_IMAGE_IDENTIFIER)

        loadContentTableView()
    }
    
    func loadContentTableView(){
        
        let finalList = NSMutableArray()
        self.listEvents         = FactEntityManager.getFacts(FACT_TYPE_EVENT)
        
        if showUpcomingEvents == true { //List current events
            
            for item in self.listEvents {
                let fact = Fact(value: item)
                
                let dateTime = Util.timeStampToNSDate(Double(fact.timestampEnd))
                
                if  NSDate().compare(dateTime) == .OrderedAscending ||
                    NSDate().compare(dateTime) == .OrderedSame{
                    
                    finalList.addObject(fact)
                }
            }
        }else{
            for item in self.listEvents {
                let fact = Fact(value: item)
                
                let dateTime = Util.timeStampToNSDate(Double(fact.timestampEnd))

                
                if NSDate().compare(dateTime) == .OrderedDescending{
                    finalList.addObject(fact)
                }
            }
        }
        
        self.listEvents = finalList
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let fact = Fact(value:listEvents.objectAtIndex(indexPath.row))
        
        let cell = BannerImageTableViewCell.getConfiguredCell(self.tableView, fact: fact, showDetailsBanner: false)
        
        return cell

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        let fact = listEvents.objectAtIndex(indexPath.row) as! Fact
        
        detailsVc.title = fact.title
        detailsVc.idObjectReceived = fact.id
        detailsVc.typeObjet = TypeObjectDetail.Event
        
        rootVc.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listEvents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 355
    }
}
