//
//  SectionsManager.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class SectionsManager: UITableView, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate,UISearchControllerDelegate {
    
    //MARK: Variables
    var arrSections         : NSMutableArray = []
    var listItensSearched   : NSMutableArray = []
    var cellHeight          = 100.0
    var rootViewController  : HomeTableViewController!
    var timeSearch          : NSTimeInterval!
    var canRequest          : Bool!
    var lastText            : String!
    var listGridSections    = [UITableViewCell]()
    var indexPaths          = [NSIndexPath]()
    
    static let timeToSearch = 0.7 as Double
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.registerNib(UINib(nibName: "OrderHomeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_ORDER_HOME)
        self.registerNib(UINib(nibName: "BannerTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_BANNER_HOME)
        self.registerNib(UINib(nibName: "ParkingTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_PARKING_HOME)
        self.registerNib(UINib(nibName: "MovieTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_MOVIE_HOME)
        self.registerNib(UINib(nibName: "GridTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_GRID_HOME)
        self.registerNib(UINib(nibName: "FooterTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_FOOTER_HOME)
        self.registerNib(UINib(nibName: "MapHomeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_MAP_HOME)
        self.registerNib(UINib(nibName: "SearchTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_SEARCH_HOME)
        
        self.dataSource = self
        self.delegate   = self
    }
    
    //MARK: - Custom Methods
    
    func setTableViewSorted (arraySections : NSArray) -> Void{
        
        //Sort asc the array
        let descriptor = NSSortDescriptor(key: "position", ascending: true)
        let sortedArray = arraySections.sortedArrayUsingDescriptors([descriptor])
        
        
        self.arrSections = NSMutableArray(array: sortedArray)
        
        /* --- se houver alguma section desconhecida remove (shopping total e 4all) --- */
        let cleanArray:NSMutableArray = NSMutableArray()
        for obj in arrSections {
            if let contentType = ContentType(rawValue: obj["sectionType"] as! String) {
                cleanArray.addObject(obj)
                if !isOneMarketplace && contentType == .Order {
                    cleanArray.removeObject(obj)
                }
            }
        }
        arrSections = cleanArray
        
        saveArrayLocally()
        generateGridList()
    }
    
    private func saveArrayLocally() -> Void {
        
        let file = "sections.json" //this is the file. we will write to and read from it
        
        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
            
            //writing
            do {
                let jsonData = try! NSJSONSerialization.dataWithJSONObject(arrSections, options: .PrettyPrinted)
                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)
                
                try jsonString?.writeToURL(path!, atomically: false, encoding: NSUTF8StringEncoding)
                
            }catch {/* error handling here */
            }
        }
    }
    
    func loadArrayLocally() -> Void {
        
        let file = "sections.json" //this is the file. we will write to and read from it
        
        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
            
            //writing
            do {
                let jsonString = try String(contentsOfURL: path!, encoding: NSUTF8StringEncoding)
                let jsonData   = jsonString.dataUsingEncoding(NSUTF8StringEncoding)
                
                let array = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: .MutableContainers) as! NSMutableArray
                
                setTableViewSorted(array)          
                
            }catch {/* error handling here */
            }
        }
    }
    
    func generateGridList() {
        
        //clear grid list
        self.listGridSections.removeAll()
        
        for item in self.arrSections {
            if let section = item as? Dictionary<String, AnyObject> {
                if ContentType(rawValue: section["sectionType"] as! String) == .Grid {
                    self.listGridSections.append(self.generateGridView(section))
                }
            }
        }
    }
    
    func getGridById(id : Int,screenConfig:ScreenConfig) -> UITableViewCell?{
    
        for cell in self.listGridSections {
            if (cell as! GridTableViewCell).id == id {
                (cell as! GridTableViewCell).screenConfig = screenConfig
                return cell
            }
        }
        
        return GridTableViewCell()
    }
    
    func stopSearch(){
        
        rootViewController.searchBarCustom?.resignFirstResponder()
        
        if rootViewController.searchActivate
        {
            if rootViewController.searchBarCustom?.text?.characters.count < 3 {
                rootViewController.searchActivate = false
                self.listItensSearched.removeAllObjects()
                self.reloadData()
            }
        }
    }
    
    //MARK: - Timer to execute request
    
    func verifyRequest(text: String)
    {
        if text == lastText && text.characters.count >= 3
        {
            if timeSearch >= SectionsManager.timeToSearch && canRequest == true
            {
                if self.rootViewController.searchBarCustom?.text?.characters.count >= 3
                {
                    self.searchFromAPI((self.rootViewController?.searchBarCustom!.text)!)
                    canRequest = false
                }
            }
            else if timeSearch < SectionsManager.timeToSearch
            {
                timeSearch = timeSearch.advancedBy(0.1)
                self.performSelector(#selector(verifyRequest), withObject: text, afterDelay: 0.1)
            }
        }
        else
        {
            return
        }
    }
    
    //MARK: - Request search from API
    
    func searchFromAPI(searchText: String)
    {
        let api = ApiServices()
        
        api.successCase = {(obj) in
            self.rootViewController.stopLoad()
            if let arrayContent = obj as? NSArray
            {
                self.listItensSearched.removeAllObjects()
                
                //Convert dictionary to ItemSearched
                for objectSearched in arrayContent
                {
                    let objectConverted = ItemSearched()
                    
                    objectConverted.id      = objectSearched.objectForKey("id") as! Int
                    objectConverted.logo    = objectSearched.objectForKey("logo") as? String
                    objectConverted.subType = objectSearched.objectForKey("subType") as? Int
                    objectConverted.title   = objectSearched.objectForKey("title") as? String
                    objectConverted.type    = objectSearched.objectForKey("type") as? String
                    
                    self.listItensSearched.addObject(objectConverted)
                }
                
                self.reloadData()
            }
        }
        
        api.failureCase = {(msg) in
            self.rootViewController.stopSearch()
        }
        
        //Get session Token
        let lib4all = Lib4all.sharedInstance()
        var sessiontoken = ""
        if lib4all.hasUserLogged() {
            let account = lib4all.getAccountData() as NSDictionary
            sessiontoken = String((account.objectForKey("sessionToken"))!)
        }
        
        api.searchAnythingOnServer(searchText,sessionToken: sessiontoken)
    }
    
    //MARK: - Search delegates
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        timeSearch = 0.0
        canRequest = true
        lastText = searchText
        self.verifyRequest(lastText)
        
        if searchText.characters.count < 3
        {
            rootViewController.stopLoad()
            self.listItensSearched.removeAllObjects()
            self.reloadData()
        }
        else
        {
            rootViewController.startLoad()
        }
    }
    
    //MARK: - TableView Methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.listItensSearched.count > 0
        {
            let dataObjectSearched = self.listItensSearched.objectAtIndex(indexPath.row) as! ItemSearched
            
            if dataObjectSearched.type == "unity"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
                detailsVc.title = self.listItensSearched[indexPath.row].title

                detailsVc.idObjectReceived = dataObjectSearched.id
                detailsVc.typeObjet = TypeObjectDetail.Store
                
                rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "movie"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MovieDetailsVC") as! MovieDetailsViewController
                
                let movie = MovieEntityManager.getMovieByID("\(dataObjectSearched.id)")
                detailsVc.title = self.listItensSearched[indexPath.row].title
                detailsVc.movie = movie
                detailsVc.title = movie?.title
                rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "fact"
            {
                let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
                detailsVc.idObjectReceived = dataObjectSearched.id
                detailsVc.title = self.listItensSearched[indexPath.row].title
                //sub tipo == promo
                if dataObjectSearched.subType == SubTypeFact.Event.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Event
                }
                else if dataObjectSearched.subType == SubTypeFact.Promotion.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Promotion
                }
                else if dataObjectSearched.subType == SubTypeFact.Warning.rawValue
                {
                    detailsVc.typeObjet = TypeObjectDetail.Warning
                }
                
                rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
            }
            else if dataObjectSearched.type == "group" {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
                viewController.titleToUse = dataObjectSearched.title
                viewController.group = GroupEntityManager.getGroupCustom("id = \(dataObjectSearched.id)")
                viewController.subType = .Store
                rootViewController.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if rootViewController.searchActivate == false
        {
            let obj = arrSections.objectAtIndex(indexPath.row) as! [String : AnyObject]
            if let contentType = ContentType(rawValue: obj["sectionType"] as! String){
                
                switch contentType {
                case .Order:
                    return self.generateOrdersView(obj)
                case .Banner:
                    return self.generateBannerView(obj,indexPath: indexPath)
                case .Parking:
                    return self.generateParkingView(obj)
                case .Movie:
                    return self.generateMovieView(obj)
                case .Grid:
                    let screenConfig = ScreenConfig(value:obj["screenConfig"]!)
                    return self.getGridById(obj["id"] as! Int,screenConfig:screenConfig)!
                case .Info:
                    return self.generateFooterView(obj)
                case .Map:
                    return self.generateMapView(obj)
                }
            }
        }
        else
        {
            return self.generateSearchView(indexPath.row)
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cellHome")!
        
        return cell;
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if !indexPaths.contains(indexPath) {
            indexPaths.append(indexPath)
            Util.animateCell(cell, indexPath: indexPath)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if rootViewController.searchActivate == false {
            return arrSections.count
        } else {
            return self.listItensSearched.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        var heightCell : CGFloat = 0
        
        if rootViewController.searchActivate == false {
            
            let obj = arrSections.objectAtIndex(indexPath.row) as! Dictionary <String, AnyObject>
            
            if let contentType = ContentType(rawValue: obj["sectionType"] as! String){
                switch contentType {
                case .Order:
                    if  OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)").count > 0 {
                        heightCell  = 60
                    }else{
                        heightCell  = 0
                    }
                case .Banner:
                    
                    var useServerHeight = false
                    if obj["screenConfig"] != nil {
                        let screenConfig = ScreenConfig(value: obj["screenConfig"]!)
                        if screenConfig.height > 0 {
                            heightCell += CGFloat(screenConfig.height)
                            useServerHeight = true
                        }
                    }
                    
                    if useServerHeight == false {
                        heightCell += 300
                    }
                    break
                case .Parking :
                    heightCell  = 60
                    break
                case .Movie   :
                    heightCell += 460
                    break
                case .Grid    :
                    
                    heightCell += GridTableViewCell.getHeightForSection(self.arrSections.objectAtIndex(indexPath.row) as! Dictionary<String, AnyObject>)
                    
                    break
                case .Info    :
                    heightCell += 300
                    break
                case .Map     :
                    heightCell  = 120
                    break
                }
                
            }
        } else {
            heightCell = 100
        }
        
        return heightCell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        stopSearch()
    }
    
    //MARK: - Tap Gesture
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches where (touch.view is SectionsManager) {
            stopSearch()
            return
        }
        
        super.touchesBegan(touches, withEvent: event)
    }
    
    //MARK: - Generate cells
    
    //MARK: Orders
    private func generateOrdersView(itemObject : Dictionary <String, AnyObject>) -> OrderHomeTableViewCell{
        
        let cell = self.dequeueReusableCellWithIdentifier(CELL_ORDER_HOME) as! OrderHomeTableViewCell
        
        cell.arrayOrders = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
        cell.viewController = HomeTableViewController.rootVc
        cell.collectionView.reloadData()
        
        //Since the cell is reused, its necessary to reload everytime it shows up to not get any other trash content
        //Also, for being reused, its necessary to set the index to start
        
        return cell;
    }
    
    //MARK: Banner
    private func generateBannerView(itemObject : Dictionary <String, AnyObject>,indexPath:NSIndexPath) -> BannerTableViewCell{
        
        let cell = self.dequeueReusableCellWithIdentifier(CELL_BANNER_HOME,forIndexPath: indexPath) as! BannerTableViewCell
        
        //Since the cell is reused, its necessary to reload everytime it shows up to not get any other trash content
        //Also, for being reused, its necessary to set the index to start
        cell.collectView.reloadData()
        cell.collectView.setContentOffset(CGPointZero, animated: false)
        cell.initialConfiguration(itemObject, rootVc: rootViewController)
        
        return cell;
    }
    
    //MARK: Parking
    private func generateParkingView(itemObject : Dictionary <String, AnyObject>) -> ParkingTableViewCell{
        
        let cell = self.dequeueReusableCellWithIdentifier(CELL_PARKING_HOME) as! ParkingTableViewCell
        cell.rootVc = self.rootViewController
        return cell
        
    }
    
    //MARK: Movie
    private func generateMovieView(itemObject : Dictionary <String, AnyObject>) -> MovieTableViewCell{
        let cell = self.dequeueReusableCellWithIdentifier(CELL_MOVIE_HOME) as! MovieTableViewCell
        
        cell.configureLayout(itemObject, rootVc: self.rootViewController)
        return cell
    }
    
    //MARK: Grid
    private func generateGridView(itemObject : Dictionary <String, AnyObject>) -> UITableViewCell{
        let cell = self.dequeueReusableCellWithIdentifier(CELL_GRID_HOME) as! GridTableViewCell
        
        cell.rootViewController = self.rootViewController
        cell.configureLayout(itemObject)
        
        return cell
    }
    
    //MARK: Footer
    private func generateFooterView(itemObject : Dictionary <String, AnyObject>) -> UITableViewCell{
        
        let cell = self.dequeueReusableCellWithIdentifier(CELL_FOOTER_HOME) as! FooterTableViewCell
        
        return cell
    }
    
    //MARK: Map
    private func generateMapView(itemObject : Dictionary <String, AnyObject>) -> UITableViewCell{
        let cell = self.dequeueReusableCellWithIdentifier(CELL_MAP_HOME) as! MapHomeTableViewCell
        cell.rootViewController = self.rootViewController
        return cell
    }
    
    //MARK: Search
    private func generateSearchView(positionListSearch: Int) -> SearchTableViewCell{
        
        let cell = self.dequeueReusableCellWithIdentifier(CELL_SEARCH_HOME) as! SearchTableViewCell
        
        let dataObjectSearched = self.listItensSearched.objectAtIndex(positionListSearch) as! ItemSearched
        cell.pupulateData(dataObjectSearched)
        
        return cell
    }
}

