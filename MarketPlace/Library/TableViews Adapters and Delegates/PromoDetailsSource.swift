//
//  PromoDetailsSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PromoDetailsSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    let arrContent = NSMutableArray()
    var tableView  : UITableView!
    var fact       : Fact!
    
    init(tableView : UITableView, fact : Fact!) {
        super.init()
        
        self.fact                   = fact
        self.tableView              = tableView
        self.tableView.dataSource   = self
        self.tableView.delegate     = self
        
        self.tableView.registerNib(UINib(nibName: "BannerImageTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BANNER_IMAGE_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "ScanCodeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: SCAN_CODE_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "DescriptionTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: DESCRIPTION_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "MapTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: MAP_IDENTIFIER)
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch ContentTypePromo(rawValue: indexPath.row)! {
        case .ImageBanner:
            return BannerImageTableViewCell.getConfiguredCell(self.tableView, fact: fact, showDetailsBanner: true)
        case .QrReader:
            let cell = ScanCodeTableViewCell.getConfiguredCell(self.tableView, fact: fact)
            cell.contentView.hidden = !fact.qrRead
            
            return cell
            
        case .Description:
            return DescriptionTableViewCell.getConfiguredCell(self.tableView, fact: fact, styleDark: true)
        case.Map:
            return MapTableViewCell.getConfiguredCell(self.tableView)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch ContentTypePromo(rawValue: indexPath.row)! {
        case .ImageBanner:
            return 226
        case .QrReader:
            if fact.qrRead == true{
                return 60
            }else{
                return 0
            }
        case .Description:
            return 160
        case.Map:
            return 350
        }
    }
    
    
    //MARK: - Enums
    enum ContentTypePromo: Int {
        case ImageBanner    = 0
        case QrReader       = 1
        case Description    = 2
        case Map            = 3
    }

    
    
}
