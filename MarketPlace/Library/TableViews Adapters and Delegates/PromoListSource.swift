//
//  PromoListSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/13/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PromoListSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    var tableView           : UITableView!
    var listPromos          = NSMutableArray()
    var showUpcomingEvents  = true
    var rootViewController  : UIViewController!

    init(tableView : UITableView, showUpcoming : Bool, referenceViewController : UIViewController) {
        super.init()
        
        self.tableView          = tableView
        self.showUpcomingEvents = showUpcoming
        self.listPromos         = FactEntityManager.getFacts(FACT_TYPE_PROMO)
        self.rootViewController = referenceViewController

        self.tableView.registerNib(UINib(nibName: "BannerImageTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BANNER_IMAGE_IDENTIFIER)
        
        loadContentTableView()
    }
    
    func loadContentTableView(){
        
        let finalList = NSMutableArray()
        if showUpcomingEvents == true { //List current promos
            
            for item in self.listPromos {
                
                let fact = Fact(value: item)
                let dateTime = Util.timeStampToNSDate(Double(fact.timestampEnd))

                if  NSDate().compare(dateTime) == .OrderedAscending ||
                    NSDate().compare(dateTime) == .OrderedSame{
                    
                    finalList.addObject(fact)
                }
            }
        }else{
            for item in self.listPromos {
                
                let fact = Fact(value: item)
                let dateTime = Util.timeStampToNSDate(Double(fact.timestampEnd))
                
                if NSDate().compare(dateTime) == .OrderedDescending{
                    finalList.addObject(fact)
                }
            }
        }
        
        self.listPromos = finalList
    }
    
    //MARK: - DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let fact = Fact(value: listPromos.objectAtIndex(indexPath.row))
        return BannerImageTableViewCell.getConfiguredCell(self.tableView, fact: fact, showDetailsBanner: true)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        let fact = listPromos.objectAtIndex(indexPath.row) as! Fact
        detailsVc.title = fact.title
        detailsVc.idObjectReceived = fact.id
        detailsVc.typeObjet = TypeObjectDetail.Promotion
        
        rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPromos.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 235
    }
}
