//
//  StoreDetailsSource.swift
//  MarketPlace
//
//  Created by Luciano on 7/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import JGProgressHUD
import RealmSwift

class StoreDetailsSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    var arrContent                      = NSArray()
    var tableView                       : UITableView!
    var store                           : Unity!
    var referenceToSuperViewController  : UIViewController!
    let loading                         = JGProgressHUD(style: .Dark)
    var fidelityEnable                  : Bool! = false
    var lib4all                         = Lib4all.sharedInstance()
    var storeId                         = Int()
    var loadUnityItemsFinished          = false
    var loadSchedulesFinished           = false
    var contentToShow                   = [ContentTypeStore]()
    var listFacts                       : Results<Fact>!
    
    //MARK: - Enums
    enum ContentTypeStore: Int {
        case ImageBanner    = 0
        case ListFacts      = 1
        case UnityItems     = 2
        case PayCheck       = 3
        case Reserve        = 4
        case Fidelity       = 5
    }
    
    //MARK: - Init methods
    
    init(tableView : UITableView, unity : Unity!, referenceToSuperViewController : UIViewController!) {
        super.init()
        
        self.tableView = tableView
        self.loadUnityItemsFinished = false
        self.loadSchedulesFinished = false
        self.tableView.registerNib(UINib(nibName: "UnityDetailsBannerTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: STORE_BANNER_DETAILS_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: "UnityItemsTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: STORE_UNITY_ITEMS)
        self.tableView.registerNib(UINib(nibName: "FidelityTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_FIDELITY)
        self.tableView.registerNib(UINib(nibName: "PayCheckTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: PAY_CHECK)
        self.tableView.registerNib(UINib(nibName: "ReserveViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_RESERVE_BUTTON)
        self.tableView.registerNib(UINib(nibName: "CellListFactsCarousel", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CELL_LIST_FACTS_IN_UNITY)
        
        self.store                          = unity
        self.storeId                        = store.id
        self.referenceToSuperViewController = referenceToSuperViewController
        self.tableView.dataSource           = self
        self.tableView.delegate             = self
        referenceToSuperViewController.title = unity.name!
        
        self.listFacts = FactEntityManager.getFactsByUnityId(storeId)
        self.verifyFidelityEnable()
        
        contentToShow.append(.ImageBanner)
        contentToShow.append(.ListFacts)
        if store.orderEnabled == true || unity.schedulingEnabled == true  {
            contentToShow.append(.UnityItems)
        }
        if store.orderCheckEnabled == true {
            contentToShow.append(.PayCheck)
        }
        if fidelityEnable == true {
            contentToShow.append(.Fidelity)
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            self.loadCategories()
            self.loadSchecules()
        })
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            self.loadMyFidelities()
        })
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.openItemsListClicked),
            name: CELL_BUTTON_ITEMS_CLICKED,
            object: nil)
    }
    
    //MARK: Verify fidelity enable
    
    private func verifyFidelityEnable() {
        
        if lib4all.hasUserLogged() {
            let fidelity = self.store.fidelity
            if fidelity != nil {
                fidelityEnable = fidelity?.active
            }
        }
    }
    
    //MARK: - Load Schedules
    
    func loadSchecules()  -> Void {
        let api = ApiServices()
        api.successCase = {(obj) in
            if  obj != nil
            {
                NSNotificationCenter.defaultCenter().postNotificationName(UNITY_SCHEDULE_OBSERVER, object: nil)
                let arrContent = obj as! NSDictionary
                SessionManager.sharedInstance.listCalendar = arrContent.objectForKey("professionals") as! [AnyObject]
                SessionManager.sharedInstance.idUnitToSchedule = self.store.id
                SessionManager.sharedInstance.unityShifts = arrContent.objectForKey("unityShifts") as! NSDictionary
                SessionManager.sharedInstance.unity = self.store
            }
            self.loadSchedulesFinished = true
        }
        
        api.failureCase = {(msg) in
            self.loadSchedulesFinished = true
        }
        
        if store != nil
        {
            api.calendarByUnit(storeId)
        }
    }
    
    //MARK: - Load Categories
    
    func loadCategories() -> Void{
        let api = ApiServices()
        
        api.successCase = { (obj) in
            self.arrContent = obj as! NSArray
            NSNotificationCenter.defaultCenter().postNotificationName(UNITY_ITEMS_OBSERVER, object: nil)
            self.loadUnityItemsFinished = true
        }
        
        api.failureCase = { (msg) in
            self.loadUnityItemsFinished = true
        }
        
        api.listUnityCategories(storeId)
    }
    
    //MARK: - Load Fidelities
    
    func loadMyFidelities() -> Void{
        let api = ApiServices()
        
        api.successCase = { (obj) in
            NSNotificationCenter.defaultCenter().postNotificationName(UNITY_FIDELITY_OBSERVER, object: nil)
        }
        
        api.failureCase = { (msg) in
        }
        
        let lib = Lib4all.sharedInstance()
        
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    if String(user.token).characters.count > 0 {
                        api.getMyFidelitiesByUnityAndSessionToken(storeId, sessionToken: String(user.token))
                    }
                }
            }
        }
    }
    
    //MARK: - TableView Methods
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch contentToShow[indexPath.row] {
        case .ImageBanner:
            let cell = UnityDetailsBannerTableViewCell.getConfiguredCell(self.tableView, unity: store, referenceViewController: referenceToSuperViewController)
            
            cell.labelInfo.userInteractionEnabled = true
            let tap = UITapGestureRecognizer.init(target:cell, action: #selector(cell.callPhoneNumber))
            cell.labelInfo.addGestureRecognizer(tap)
            
            return cell
        /*case .ListFacts:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_LIST_FACTS_IN_UNITY, forIndexPath: indexPath) as! CellListFactsCarousel
            
            cell.superViewController = referenceToSuperViewController
            cell.listFacts = self.listFacts
            cell.pageControlListFacts.numberOfPages = self.listFacts.count
            cell.carousel.delegate = cell
            cell.carousel.dataSource = cell
            cell.carousel.scrollEnabled = self.listFacts.count > 1 ? true : false
            
            return cell*/
        case .UnityItems:
            let cell = tableView.dequeueReusableCellWithIdentifier(STORE_UNITY_ITEMS) as! UnityItemsTableViewCell
            cell.unity = store
            cell.rootViewController = referenceToSuperViewController
            cell.setLabelText(store)            
            return cell
        case .PayCheck:
            let cell = tableView.dequeueReusableCellWithIdentifier(PAY_CHECK) as! PayCheckTableViewCell
            cell.rootViewController = referenceToSuperViewController
            cell.unity = store
            return cell
        case .Reserve:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_RESERVE_BUTTON) as! ReserveViewCell!
            cell.rootViewController = referenceToSuperViewController
            return cell
        case .Fidelity:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL_FIDELITY) as! FidelityTableViewCell
            cell.rootViewController = referenceToSuperViewController
            cell.unity = store
            cell.updateCell()
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentToShow.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch contentToShow[indexPath.row] {
        case .ImageBanner:
            return 350
        case .ListFacts:
            if self.listFacts.count > 0 {
                return 200
            } else {
                return 0
            }
        case .UnityItems:
            return 70
        case .PayCheck:
            return 70
        case .Reserve:
            return 70
        case .Fidelity:
            let unityFidelity = UnityFidelityPersistence.getUnityFidelityByIdUnity(store.id)
            return Util.getHeightViewFidelityByCountItems((unityFidelity?.totalStamp)!) + 30
        }
    }
    
    func openItemsListClicked() {
        if  arrContent.count > 0 {
            if loadSchedulesFinished == true && loadUnityItemsFinished == true {
                let listUnityItemsViewController                = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListUnityItemsVC") as! ListUnityItemsViewController
                listUnityItemsViewController.contentArray       = arrContent
                listUnityItemsViewController.unity              = store
            
            referenceToSuperViewController.navigationController?.pushViewController(listUnityItemsViewController, animated: true)
            }
            else {
                if (store.orderEnabled == true && store.schedulingEnabled == false) {
                    Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageLoadingItems)
                }
                else if (store.orderEnabled == false && store.schedulingEnabled == true) {
                    Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageLoadingSchedules)
                }
                else if (store.orderEnabled == true && store.schedulingEnabled == true) {
                    Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageLoadingItemsAndSchedules)
                }
            }
        }
        else {
            if (store.orderEnabled == true && store.schedulingEnabled == false) {
                Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageNoItemsAvailable)
            }
            else if (store.orderEnabled == false && store.schedulingEnabled == true) {
                Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageNoSchedulesAvailable)
            }
            else if (store.orderEnabled == true && store.schedulingEnabled == true) {
                Util.showAlert(self.referenceToSuperViewController, title: kAlertTitle, message: kMessageNoItemsAndSchedulesAvailable)
            }
        }
    }
}
