//
//  LayoutManager.swift
//  Shopping Total
//
//  Created by 4all on 6/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class LayoutManager: NSObject {
    
    //Colors
    static var primaryColor                     : UIColor!
    static var regularFontColor                 : UIColor!
    static var backgroundLight                  : UIColor!
    static var backgroundDark                   : UIColor!
    static var white                            : UIColor!
    static var black                            : UIColor!
    static var professionalSelectedOnSchedule   : UIColor!
    static var backgroundFirstColor             : UIColor!
    static var backgroundSecondColor            : UIColor!
    static var backgroundThirdColor             : UIColor!
    static var labelFirstColor                  : UIColor!
    static var labelSecondColor                 : UIColor!
    static var labelThirdColor                  : UIColor!
    static var labelFourthColor                 : UIColor!
    static var labelContrastColor               : UIColor!
    
    static var scarletRed                       : UIColor!
    static var openGreen                        : UIColor!
    
    static var activeUnityOpenColor             : UIColor!
    static var activeUnityClosedColor           : UIColor!

    
    //Pushes
    static var purple4Beauty                  : UIColor!
    static var red4Food                       : UIColor!
    static var grayGeneralText                : UIColor!
    static var green4all                      : UIColor!
    static var lightGray4all                  : UIColor!
    static var greyCart                       : UIColor!
    static var greyTitleItemCart              : UIColor!
    static var greyDescItemCart               : UIColor!
    static var lightGreyTracking              : UIColor!
    static var mediumGreyTracking             : UIColor!
    static var darkGreyTracking               : UIColor!
    static var greenStatusTracking            : UIColor!
    static var statusImageGreyTracking        : UIColor!


    static func loadConfig(){
        var myDict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Config", ofType: "plist") {
            myDict = NSDictionary(contentsOfFile: path)
        }
        if let dict = myDict {
            
            if let layoutDict = dict.objectForKey("Layout") as? NSDictionary {
                LayoutManager.primaryColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("primaryColor") as! String)
                LayoutManager.regularFontColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("regularFontColor") as! String)
                LayoutManager.backgroundLight = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("backgroundLight") as! String)
                LayoutManager.backgroundDark = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("backgroundDark") as! String)
                LayoutManager.professionalSelectedOnSchedule = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("professionalSelectedOnSchedule") as! String)
                
                LayoutManager.backgroundFirstColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("backgroundFirstColor") as! String)
                LayoutManager.backgroundSecondColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("backgroundSecondColor") as! String)
                LayoutManager.backgroundThirdColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("backgroundThirdColor") as! String)
                LayoutManager.labelFirstColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("labelFirstColor") as! String)
                LayoutManager.labelSecondColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("labelSecondColor") as! String)
                LayoutManager.labelThirdColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("labelThirdColor") as! String)
                LayoutManager.labelContrastColor = UIColor()
                    .hexStringToUIColor(layoutDict
                        .valueForKey("labelContrastColor") as! String)
                
                LayoutManager.purple4Beauty     = UIColor().hexStringToUIColor("923598")
                LayoutManager.red4Food          = UIColor().hexStringToUIColor("C2252E")
                LayoutManager.grayGeneralText   = UIColor().hexStringToUIColor("3D3D3D")
                LayoutManager.green4all         = UIColor().hexStringToUIColor("4FA444")
                LayoutManager.lightGray4all     = UIColor().hexStringToUIColor("F2F2F2")
                LayoutManager.purple4Beauty       = UIColor().hexStringToUIColor("923598")
                LayoutManager.red4Food            = UIColor().hexStringToUIColor("C2252E")
                LayoutManager.grayGeneralText     = UIColor().hexStringToUIColor("3D3D3D")
                LayoutManager.green4all           = UIColor().hexStringToUIColor("4FA444")
                LayoutManager.lightGray4all       = UIColor().hexStringToUIColor("F2F2F2")
                LayoutManager.greyCart            = UIColor().hexStringToUIColor("2E2E2F")
                LayoutManager.greyTitleItemCart   = UIColor().hexStringToUIColor("323233")
                LayoutManager.greyDescItemCart    = UIColor().hexStringToUIColor("666768")
                LayoutManager.lightGreyTracking   = UIColor().hexStringToUIColor("B5B5B5")
                LayoutManager.mediumGreyTracking  = UIColor().hexStringToUIColor("6C6C6C")
                LayoutManager.darkGreyTracking    = UIColor().hexStringToUIColor("0A0A0A")
                LayoutManager.greenStatusTracking = UIColor().hexStringToUIColor("95C38F")
                LayoutManager.statusImageGreyTracking = UIColor().hexStringToUIColor("AAAAAA")
                
                LayoutManager.scarletRed = UIColor(red: 0.761, green: 0.145, blue: 0.180, alpha: 1.00)
                LayoutManager.openGreen = UIColor(red: 0.000, green: 0.808, blue: 0.004, alpha: 1.00)
                LayoutManager.labelFourthColor = UIColor(red: 0.506, green: 0.506, blue: 0.506, alpha: 1.00)
                
                LayoutManager.activeUnityOpenColor      = LayoutManager.openGreen
                LayoutManager.activeUnityClosedColor    = LayoutManager.scarletRed
                LayoutManager.purple4Beauty             = UIColor().hexStringToUIColor("923598")
                LayoutManager.red4Food                  = UIColor().hexStringToUIColor("C2252E")
                LayoutManager.grayGeneralText           = UIColor().hexStringToUIColor("3D3D3D")
                LayoutManager.green4all                 = UIColor().hexStringToUIColor("4FA444")
                LayoutManager.lightGray4all             = UIColor().hexStringToUIColor("F2F2F2")
                LayoutManager.greyCart                  = UIColor().hexStringToUIColor("2E2E2F")
                LayoutManager.greyTitleItemCart         = UIColor().hexStringToUIColor("323233")
                LayoutManager.greyDescItemCart          = UIColor().hexStringToUIColor("666768")
                LayoutManager.lightGreyTracking         = UIColor().hexStringToUIColor("B5B5B5")
                LayoutManager.mediumGreyTracking        = UIColor().hexStringToUIColor("6C6C6C")
                LayoutManager.darkGreyTracking          = UIColor().hexStringToUIColor("0A0A0A")
                LayoutManager.greenStatusTracking       = UIColor().hexStringToUIColor("95C38F")
                LayoutManager.statusImageGreyTracking   = UIColor().hexStringToUIColor("AAAAAA")
            }
        }
        
        LayoutManager.white = UIColor.whiteColor()
        LayoutManager.black = UIColor.blackColor()
    }
    
    //Font
    static func primaryFontWithSize(size: CGFloat, bold : Bool = false) -> UIFont{
        if !bold {
            return UIFont(name: "Dosis-Regular", size: size)!
        }else {
            return UIFont(name: "Dosis-Bold", size: size)!
        }
    }
    
    private static func roundCorners(view : UIView, corners : UIRectCorner, radius: CGFloat){
        
        let bounds      = view.bounds
        let maskPath    = UIBezierPath.init(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(radius, radius))
        
        let maskLayer   = CAShapeLayer()
        maskLayer.frame = bounds;
        maskLayer.path  = maskPath.CGPath
        
        view.layer.mask = maskLayer
        
        let frameLayer       = CAShapeLayer()
        frameLayer.frame     = bounds
        frameLayer.path      = maskPath.CGPath
        frameLayer.fillColor = nil
        
        view.layer.addSublayer(frameLayer)
    }
    
    static func roundTopCornersRadius(view: UIView, radius: CGFloat){
        self.roundCorners(view, corners: [.TopLeft, .TopRight], radius: radius)
    }
    
    static func roundBottomCornersRadius(view: UIView, radius: CGFloat){
        self.roundCorners(view, corners: [.BottomLeft, .BottomRight], radius: radius)
    }
    
    static func roundBottomRightAndTopRightCornersRadius(view: UIView, radius: CGFloat){
        self.roundCorners(view, corners: [.TopRight, .BottomRight], radius: radius)
    }

    static func roundCustomCornersRadius(view: UIView, radius: CGFloat, corners : UIRectCorner){
        self.roundCorners(view, corners: corners, radius: radius)
    }
}

extension UIColor {
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
