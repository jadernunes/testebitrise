//
//  ScreensName.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 13/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class ScreensName: NSObject {
    
    static let screenHome                   = "Home"
    static let screenOrderMonitoring        = "Detalhe do pedido"
    static let screenPromotions             = "Promoções"
    static let screenEvents                 = "Eventos"
    static let screenMovies                 = "Filmes"
    static let screenServices               = "Serviços"
    static let screenFoodCort               = "Praça de alimentação"
    static let screenStores                 = "Lojas"
    static let screenNotifications          = "Notificações"
    static let screenCoupons                = "Cupons"
    static let screenLocationStopCar        = "Onde Parei"
    static let screenPayCarPlace            = "Pagar estacionamento"
    static let screenMyOrders               = "Meus pedidos"
    static let screenMySchedules            = "Meus agendamentos"
    static let screenMyCoupons              = "Meus cupons"
    static let screenMyFidelities           = "Minhas fidelidades"
    static let screenMyVouchers             = "Meus vouchers"
    static let screenSchedule               = "Agendamento"
    static let screenScheduleConfirm        = "Confirmação do agendamento"
    static let screenScheduleRequest        = "Solicitação do agendamento"
    static let screenQRAndTicketReader      = "Leitor QR e Ticket"
    static let screenProductsOrServices     = "Produtos ou serviços"
    static let screenCart                   = "Carrinho"
    static let screenConfirmTicketPayment   = "Confirmação de pagamento de ticket"
    static let screenConfirmQRPayment       = "Confirmação de pagamento por QR"
    static let screenConfirmOrderPayment    = "Confirmação de pagamento de pedido"
    static let screenConfirmSchedule        = "Confirmação agendamento"
    static let screenRequesSchedule         = "Solicitação agendamento"
    static let screenListHome               = "Lista de Homes"
    static let screenConfirmCancelPayment   = "Confirmação de cancelamento de pagamento"
    static let screenConfirmVoucherPayment  = "Confirmação de pagamento de pedido com voucher"
    static let screenDiversaoEventos        = "Diversão e eventos"
    static let screenBusca                  = "Busca"
    static let screenLojasPorPerto          = "Lojas por perto"
    
    static let screenDetalheIngresso        = "Detalhe do ingresso"
    static let screenDetalheVoucher         = "Detalhe do voucher"
    static let screenMeusVouchers           = "Meus vouchers"
    static let screenMeusIngressos          = "Meus ingressos"
}
