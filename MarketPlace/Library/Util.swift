//
//  Util.swift
//  MarketPlace
//
//  Created by Luciano on 7/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit
import RealmSwift

class Util: NSObject {
    
    enum Days: String {
        case Sunday , Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    }
    
    static let cache = NSCache()
    static var toastView = UILabel()
    static var isAnimating = false

    class func showAlert(view: UIViewController, title : String, message : String) -> Void {
        dispatch_async(dispatch_get_main_queue()) { 
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: kClose, style: .Cancel) { (action) in
                
            }
            
            alertController.addAction(cancelAction)
            
            view.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    /**
     Function to call an alertview like Toast in Android
     
     * Uses mainthread to update the animation
     
    */
    class func showToast(message:String, timeDuration:Double, targetView: UIView, type: kToastType) {
        if !isAnimating {
            self.isAnimating        = true
            let heightToast         : CGFloat = 50
            
            /* --- se o toast não está na view, configura --- */
            if !toastView.isDescendantOfView(targetView) {
                toastView = UILabel.init(frame: CGRect.init(x: 0, y: -heightToast,width:targetView.frame.size.width ,height: heightToast))
                toastView.numberOfLines = 2
                toastView.textAlignment = .Center
                toastView.contentMode = .Bottom
                toastView.font = LayoutManager.primaryFontWithSize(18)
                toastView.textColor = UIColor.whiteColor()
                targetView.addSubview(toastView)
            }
            
            /* --- Configura o tipo de toast --- */
            switch type {
            case kToastType.success:
                toastView.backgroundColor = LayoutManager.green4all
            case kToastType.warning:
                toastView.backgroundColor = Util.hexStringToUIColor("#99850B")
            case kToastType.error:
                toastView.backgroundColor = Util.hexStringToUIColor("#DC4E41")
            }
            
            /* --- dispara as açoes usando a main thread, sem ela a animation não acessa os parametros --- */
            dispatch_async(dispatch_get_main_queue(),{
                self.isAnimating = true
                self.toastView.text = "\n\(message)"
                UIView.animateWithDuration(0.3, animations: {
                    self.toastView.frame.origin.y = 0
                    }, completion: { (completed) in
                        UIView.animateWithDuration(0.3, delay: timeDuration, options: UIViewAnimationOptions.CurveEaseInOut, animations: { 
                            self.toastView.frame = CGRect.init(x: 0, y: -heightToast, width:targetView.frame.size.width ,height: heightToast)
                            }, completion: { (completed) in
                                self.isAnimating = false
                        })
                })
            })
        }
    }
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func setIconButton(button: UIButton, image : UIImage, inLeft : Bool) -> Void {
        
        let imgIcon             = UIImageView(frame: CGRectMake(0, 0, button.frame.size.height - 45, button.frame.size.height - 45))
        imgIcon.center          = button.titleLabel!.center
        imgIcon.image           = image
        imgIcon.contentMode     = .ScaleAspectFit
        UIGraphicsBeginImageContext(imgIcon.bounds.size);
        imgIcon.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        let img = image
        
        button.setImage(img, forState: .Normal)
        button.tintColor = SessionManager.sharedInstance.cardColor
        if inLeft == true {
            button.transform = CGAffineTransformMakeScale(1.0, -1.0);
            button.titleLabel!.transform = CGAffineTransformMakeScale(1.0, -1.0);
            button.imageView!.transform = CGAffineTransformMakeScale(1.0, -1.0);
        }else{
            button.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            button.titleLabel!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            button.imageView!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        }
    }
    
    class func getFormatedDateString(date: NSDate, pattern : String) -> String{
        
        let dateFormatter  = NSDateFormatter()
        
        dateFormatter.dateFormat = pattern
        dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        
        return dateFormatter.stringFromDate(date)
    }
    
    
    class func timeStampToNSDate(timestamp : Double) -> NSDate {
        
        // get the NSDate
        let dateTime    = NSDate(timeIntervalSince1970: timestamp/1000)
        
        return dateTime
    }
    
    class func getStringMonth(month : String) -> String {
        
        switch month {
        case "01":
            return "Janeiro"
        case "02":
            return "Fevereiro"
        case "03":
            return "Março"
        case "04":
            return "Abril"
        case "05":
            return "Maio"
        case "06":
            return "Junho"
        case "07":
            return "Julho"
        case "08":
            return "Agosto"
        case "09":
            return "Setembro"
        case "10":
            return "Outubro"
        case "11":
            return "Novembro"
        case "12":
            return "Dezembro"
        default:
            return ""
        }
    }

    
    class func callMaps(coordinate: CLLocationCoordinate2D,
                        placeName: String,
                        alertControllerStyle: UIAlertControllerStyle,
                        viewController: UIViewController) -> UIAlertController?
    {
        let alert = UIAlertController(title: "Escolha como abrir", message: "", preferredStyle: alertControllerStyle)
        
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"http://maps.apple.com/")!)) {
            let maps = UIAlertAction(title: "Maps", style: .Default) { (alert: UIAlertAction!) -> Void in
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = placeName
                mapItem.openInMapsWithLaunchOptions([MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
            }
            alert.addAction(maps)
        }
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
             let googleMaps = UIAlertAction(title: "Google Maps", style: .Default) { (alert: UIAlertAction!) -> Void in
                

                let urlStr:NSString = String(format: "comgooglemaps://?daddr=%f,%f&directionsmode=driving",coordinate.latitude, coordinate.longitude)
                 UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
                
            }
            alert.addAction(googleMaps)
        }
       
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "waze://")!) {

            let waze = UIAlertAction(title: "Waze", style: .Default) { (alert: UIAlertAction!) -> Void in
                
                let urlStr:NSString = String(format: "waze://?ll=%f,%f&navigate=yes",coordinate.latitude, coordinate.longitude)
                UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
            }
            alert.addAction(waze)
        }
        if (alert.actions.count) > 0 {
            
            if alertControllerStyle == .ActionSheet {
                alert.addAction(UIAlertAction(title: .actionSheetCancel, style: UIAlertActionStyle.Cancel, handler: { action in
                    viewController.dismissViewControllerAnimated(true, completion: nil)
                }))
            }
            
            return alert
        }
        else {
            return nil
        }
    }
    
    class func openMapDefault(coordinate: CLLocationCoordinate2D,placeName: String, viewController: UIViewController) {
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"http://maps.apple.com/")!)) {
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = placeName
            mapItem.openInMapsWithLaunchOptions([MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }
        
        else if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
            let urlStr:NSString = String(format: "comgooglemaps://?daddr=%f,%f&directionsmode=driving",coordinate.latitude, coordinate.longitude)
            UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
        }
        
        else if UIApplication.sharedApplication().canOpenURL(NSURL(string: "waze://")!) {
            let urlStr:NSString = String(format: "waze://?ll=%f,%f&navigate=yes",coordinate.latitude, coordinate.longitude)
            UIApplication.sharedApplication().openURL(NSURL(string: urlStr as String)!)
        }

        
    }
    
    class func formatCurrency(currencyValue:Double, prefixMessage:String?=nil, sufixMessage:String?=nil) -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale.currentLocale()
        
        var fullMessage : String = formatter.stringFromNumber(currencyValue)!
        if let prefix : String = prefixMessage {
            fullMessage = prefix + fullMessage
        }
        if let sufix : String = sufixMessage {
            fullMessage = fullMessage + sufix
        }
        return fullMessage
    }
    
    class func getFirstNameFromFullName(fullName:String) -> (String) {
        return fullName.componentsSeparatedByString(" ").first!
    }
    
    class func resizeImageNamed(imageName: String, size: CGSize) -> UIImage {
        let pinImage = UIImage(named: imageName)
        UIGraphicsBeginImageContext(size)
        pinImage!.drawInRect(CGRectMake(0, 0, size.width, size.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    
    /// Create a UIImage from the target UIView
    ///
    /// - Parameter view: the target view to create a print
    /// - Returns: returns a UIImage from the UIView
    class func printView(view:UIView) -> UIImage {
        UIGraphicsBeginImageContext(view.bounds.size);
        let context = UIGraphicsGetCurrentContext();
        view.layer.renderInContext(context!)
        let screenShot : UIImage! = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return screenShot
    }
    
    /// Method to animate cell, please note that the animations
    /// will run and the layer WILL NOT keep the changes after the end
    /// of the animation
    ///
    /// - Parameters:
    ///   - cell: cell to animate
    ///   - indexPath: indexPath of the cell, in case you want to adjust something based on index
    class func animateCell(cell :UITableViewCell, indexPath :NSIndexPath) {
        /// Path will make the mask to reveal the cell
        let path = UIBezierPath(rect: CGRectMake(cell.layer.frame.width*0.5, cell.layer.frame.size.height*0.5, 0, 0))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        cell.layer.mask = mask // apply the mask
        
        /// revealPath is the final frame of the mask, will be applied on the animations
        let revealPath = UIBezierPath(rect: CGRectMake(0, 0, cell.layer.frame.size.width, cell.layer.frame.size.height))
        
        /// anim is one animation, animations is a list of animations, that will run by the order
        let anim = CABasicAnimation(keyPath: "path")
        anim.fromValue              = path.CGPath
        anim.toValue                = revealPath.CGPath
        anim.duration               = 0.20
        anim.fillMode               = kCAFillModeBoth
        anim.removedOnCompletion    = false
        
        /// animOpacity will change the opacity(obviusly) of the layer
        let animOpacity = CABasicAnimation(keyPath: "opacity")
        animOpacity.fromValue = 0
        animOpacity.toValue = 1
        animOpacity.duration = 0.3
        
        /// animScale change the scale of the layer
        let animScale = CABasicAnimation(keyPath: "transform.scale")
        animScale.fromValue             = 1
        animScale.toValue               = 1.2
        animScale.duration              = 0.4
        animScale.timingFunction        = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseIn)
        animScale.removedOnCompletion   = false
        
        /// animScale2 will return to the original size of the layer
        let animScale2 = CABasicAnimation(keyPath: "transform.scale")
        animScale2.fromValue             = 1.2
        animScale2.toValue               = 1
        animScale2.duration              = 0.4
        animScale.timingFunction        = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseOut)
        animScale2.removedOnCompletion   = false
        
        /* --- Add the animations, on the order that must execute --- */
        cell.layer.mask!.addAnimation(anim, forKey: "anim")
        cell.layer.mask!.addAnimation(animOpacity, forKey: "animOpacity")
        cell.layer.addAnimation(animScale, forKey: "animScale")
        cell.layer.addAnimation(animScale2, forKey: "animScale2")
    }
    
    
    /// Method to animate grid cell, please note that the animations
    /// will run and the layer WILL keep the changes after the end
    /// of the animation
    ///
    /// - Parameters:
    ///   - cell: cell to animate
    ///   - indexPath: indexPath of the cell, in case you want to adjust something based on index
    class func animateGridCell(cell :UICollectionViewCell, indexPath :NSIndexPath, bounceSize:CGFloat) {
        /* --- reduce the size of the cell --- */
        cell.layer.transform = CATransform3DMakeScale(0.2,0.2,1)
        
        /* --- create the timer to make the bubble effect, one after another --- */
        let seconds : Double = Double(indexPath.row*5)/100
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
        /* --- run the animations --- */
        dispatch_after(dispatchTime, dispatch_get_main_queue(), {
            UIView.animateWithDuration(0.3, animations: {
                cell.layer.transform = CATransform3DMakeScale(bounceSize,bounceSize,1)
                },completion: { finished in
                    UIView.animateWithDuration(0.2, animations: {
                        cell.layer.transform = CATransform3DMakeScale(1,1,1)
                    })
            })
        })
    }
    
    class func tintImageView(imageView imageView:UIImageView, color: UIColor) -> UIImageView {
        let origImage = imageView.image
        let tintedImage = origImage!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imageView.image = tintedImage
        imageView.tintColor = color
        return imageView
    }
    
    //MARK: - Sync objects in Realm
    
    static func syncObject(classNameRealm: String,stringIDs:String) {
        
        let realm = try! Realm()
        let result = realm.dynamicObjects(classNameRealm).filter("NOT id IN {\(stringIDs)}")
        
        try! realm.write {
            realm.delete(result)
        }
    }
    
    //MARK: - Check schedule
    static func checkSchedule(dataSchedule: NSDictionary!, isConfirmSchedule: Bool) -> Bool {
        
        let dataPost = dataSchedule.objectForKey("dataShow") as! NSDictionary
        let date = (dataPost.objectForKey("dateToShow"))!
        var timeBegin = (dataPost.objectForKey("hourStartToShow"))!
        
        //add seconds to send to server
        if isConfirmSchedule {
            timeBegin = timeBegin.stringByAppendingString(":00")
        }
        
        let completeDateToSchedule = String(format: "\(date) \(timeBegin)")
        
        let actualDate = NSDate()
        let shortActualDate = NSDate.toShortTimeString(actualDate)
        
        let differenceDate = NSDate.secondsByDifferenceDatesWithFormat(completeDateToSchedule, dateStringFinish: shortActualDate, format: "dd/MM/yyyy HH:mm:ss")
        if differenceDate >= 0 {
            return true
        }
        
        return false
    }
    
    //MARK - showMessageWithoutData
    static func checkWithoutData(superView:UIView, tableView: UIView, countData:Int!, message:String!) -> Void {
        
        var labelEmpty = superView.viewWithTag(123456) as? UILabel
        var isNew = false
        if labelEmpty == nil {
            isNew = true
            labelEmpty = UILabel(frame: CGRectMake(0, 0, 200, 80))
            labelEmpty?.tag = 123456
            labelEmpty!.center = tableView.center
            labelEmpty!.textAlignment = NSTextAlignment.Center
            labelEmpty!.text = message
            labelEmpty!.font = LayoutManager.primaryFontWithSize(16)
            labelEmpty!.textColor = LayoutManager.primaryColor
            labelEmpty!.numberOfLines = 3
        }
        
        if countData == 0 {
            if isNew {
                superView.addSubview(labelEmpty!)
            }
            
            superView.bringSubviewToFront(labelEmpty!)
            tableView.hidden = true
            labelEmpty!.hidden = false
        }
        else {
            if labelEmpty!.isDescendantOfView(superView) {
                labelEmpty!.removeFromSuperview()
            }
            
            superView.sendSubviewToBack(labelEmpty!)
            labelEmpty!.hidden = true
            tableView.hidden = false
        }
    }
    
    static func removerCharacters(value: String!) -> String! {
        var newValue = NSString.removeSpecialCharacters(value) as NSString
        newValue = newValue.removeCharacters(".")
        newValue = newValue.removeCharacters(",")
        return newValue as String
    }
    
    static func removeCharacterInString(value: String!, character: String) -> String! {
        var newValue = NSString.removeSpecialCharacters(value) as NSString
        newValue = newValue.removeCharacters(character)
        return newValue as String
    }
    
    static func adjustTableViewInsetsFromTarget(viewController: UIViewController) {
        if isOneMarketplace {
            viewController.automaticallyAdjustsScrollViewInsets = true
        }
        else {
            viewController.automaticallyAdjustsScrollViewInsets = false
        }
    }
    static func adjustInsetsFromCart(tableView: UITableView) {
        if SessionManager.sharedInstance.cartActive == true {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 64, right: 0)
        }
        else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    //MARK: - Height view Fidelity
    static func getHeightViewFidelityByCountItems(countItems: Int) -> CGFloat {
        
        var valueToAddToPadding : CGFloat = 00.0
        
        let deviceModel = UIDevice().getDeviceModel()
        
        if deviceModel == .iPhone5      ||
            deviceModel == .iPhone5s    ||
            deviceModel == .iPhone5c    ||
            deviceModel == .iPhoneSE
        {
            valueToAddToPadding = 20.00
        }
        
        let padding      = UIScreen.mainScreen().bounds.size.width / 6 + valueToAddToPadding
        let numberOfRows = ceil(Double(countItems) / 6)
        
        //calculate estimated cellRow
        //Get screen width less estimated margins
        let estimatedRow = ((UIScreen.mainScreen().bounds.size.width - 23) / 6 + 13)
        
        let sectionHeight =  CGFloat(numberOfRows * Double(estimatedRow))
        let heightFidelityView = (sectionHeight + CGFloat(padding))
        
        return heightFidelityView
    }
    
    //MARK: - Day of week in English
    static func dayOfWeek(day: Int) -> String {
        switch day {
        case 1:
            return Days.Monday.rawValue
        case 2:
            return Days.Tuesday.rawValue
        case 3:
            return Days.Wednesday.rawValue
        case 4:
            return Days.Thursday.rawValue
        case 5:
            return Days.Friday.rawValue
        case 6:
            return Days.Saturday.rawValue
        case 7:
            return Days.Sunday.rawValue
        default:
            return ""
        }
    }
    
    //MARK: - Day of week in portuguese
    static func dayOfWeekInPortuguese(day: String) -> String {
        switch day {
        case Days.Monday.rawValue:
            return "Segunda-feira"
        case Days.Tuesday.rawValue:
            return "Terça-feira"
        case Days.Wednesday.rawValue:
            return "Quarta-feira"
        case Days.Thursday.rawValue:
            return "Quinta-feira"
        case Days.Friday.rawValue:
            return "Sexta-feira"
        case Days.Saturday.rawValue:
            return "Sábado"
        case Days.Sunday.rawValue:
            return "Domingo"
        default:
            return ""
        }
    }
    
    //MARK: - Current day of week in String format
    static func currentDayOfWeek() -> String {
        
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Weekday], fromDate: date)
        let day = components.weekday

        return dayOfWeek(day)
    }
    
    //MARK: - Custom print
    static func print(object: Any){
        if isDesenvMode {
            Swift.print(object)
        }
    }
    
    //MARK: - Show Generic Message
    
    static func showIterativeMessage(delegateReference:IterativeMessageDelegate? = nil,typeActions:ActionIterativeMessage!,tag:Int! = 0,message:String = "",title:String = ""){
        
        let controller = UIStoryboard(name: "GeneralStoryboard", bundle: nil).instantiateViewControllerWithIdentifier("IterativeMessageViewController") as! IterativeMessageViewController
        
        controller.delegate = delegateReference
        controller.typeActions = typeActions
        controller.tag = tag
        controller.titleMessage = title
        controller.message = message
        
        let deviceBounds = UIScreen.mainScreen().bounds
        let formSheet = MZFormSheetController.init(viewController: controller)
        formSheet.shouldDismissOnBackgroundViewTap = true;
        formSheet.cornerRadius = 8.0;
        formSheet.portraitTopInset = 6.0;
        formSheet.landscapeTopInset = 6.0;
        formSheet.presentedFormSheetSize = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height/3);
        formSheet.shouldCenterVertically = true
        formSheet.formSheetWindow.transparentTouchEnabled = false;
        
        formSheet.presentAnimated(true) { (viewController: UIViewController!) in
            
        }
    }
    
    static func showBadge(view: UIView, count: Int){
        
        let customButton = UIButton(type: .Custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        customButton.tintColor = UIColor.whiteColor()
        view.addSubview(customButton)
        
        let badge = BBBadgeBarButtonItem(customUIButton: customButton)
        badge.badgeValue = "\(count)"
        badge.shouldHideBadgeAtZero = true;
        badge.badgePadding = 0.0;
        badge.badgeOriginX = 20.0;
        badge.badgeOriginY = 18.0;
        badge.badgeBGColor = UIColor.orangeFun4all()
    }
    
    static func showBadgeInMenu(view: UIView, count: Int){
        
        let customButton = UIButton(type: .Custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        customButton.tintColor = UIColor.whiteColor()
        customButton.tag = 1999
        view.addSubview(customButton)
        
        let badge = BBBadgeBarButtonItem(customUIButton: customButton)
        badge.badgeValue = "\(count)"
        badge.shouldHideBadgeAtZero = true;
        badge.badgePadding = 0.0;
        badge.badgeOriginX = 13.0;
        badge.badgeOriginY = 10.0;
        badge.badgeBGColor = UIColor.orangeFun4all()
    }
    
    static func removeBadgeInMenu(view: UIView){
        let bagde = view.viewWithTag(1999)
        bagde?.removeFromSuperview()
    }
    
    static func setNavigationBarWithGradient(navigationBar: UINavigationBar){
        navigationBar.translucent = false
        navigationBar.tintColor = UIColor.whiteColor()
        let fontDictionary = [ NSForegroundColorAttributeName:UIColor.whiteColor() ]
        navigationBar.titleTextAttributes = fontDictionary
        navigationBar.setBackgroundImage(Util.imageLayerForGradientBackground(navigationBar), forBarMetrics: UIBarMetrics.Default)
    }
    
    static func setViewWithGradient(view: UIView, frame: CGRect){
        
        let imageView = UIImageView(frame: frame)
        imageView.image = Util.imageLayerForGradientBackgroundInView(view)
        view.addSubview(imageView)
        view.sendSubviewToBack(imageView)
    }
    
    static func imageLayerForGradientBackground(navigationBar: UINavigationBar) -> UIImage {
        
        var updatedFrame = navigationBar.bounds
        // take into account the status bar
        updatedFrame.size.height += 20
        let layer = CAGradientLayer.gradient4allLayerForBoundsForNavigation(updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    static func imageLayerForGradientBackgroundInView(view: UIView) -> UIImage {
        
        let updatedFrame = view.bounds
        let layer = CAGradientLayer.gradient4allLayerForBounds(updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    static func putsInfoInDeviceSettings(){
        
        let versionApp = SystemInfo.getVersionApp() + " (\(SystemInfo.getBuild()))"
        let versionAccount = Lib4all.sharedInstance().versionAccount()
        
        NSUserDefaults.standardUserDefaults().setObject(versionApp, forKey: "version_app")
        NSUserDefaults.standardUserDefaults().setObject(versionAccount, forKey: "version_account")
    }
}
