//
//  UIDeviceExtension.swift
//  MarketPlace
//
//  Created by Luciano on 10/7/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class UIDeviceExtension: NSObject {
    
}

public extension UIDevice {
    
    internal func getDeviceModel() -> DeviceModel {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return DeviceModel.iPodTouch5
        case "iPod7,1":                                 return DeviceModel.iPodTouch6
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return DeviceModel.notSupported //"iPhone 4"
        case "iPhone4,1":                               return DeviceModel.iPhone4s
        case "iPhone5,1", "iPhone5,2":                  return DeviceModel.iPhone5
        case "iPhone5,3", "iPhone5,4":                  return DeviceModel.iPhone5c
        case "iPhone6,1", "iPhone6,2":                  return DeviceModel.iPhone5s
        case "iPhone7,2":                               return DeviceModel.iPhone6
        case "iPhone7,1":                               return DeviceModel.iPhone6Plus
        case "iPhone8,1":                               return DeviceModel.iPhone6s
        case "iPhone8,2":                               return DeviceModel.iPhone6sPlus
        case "iPhone9,1", "iPhone9,3":                  return DeviceModel.iPhone7
        case "iPhone9,2", "iPhone9,4":                  return DeviceModel.iPhone7plus
        case "iPhone8,4":                               return DeviceModel.iPhoneSE
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return DeviceModel.notSupported //"iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return DeviceModel.notSupported //"iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return DeviceModel.notSupported //"iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return DeviceModel.notSupported //"iPad Air"
        case "iPad5,3", "iPad5,4":                      return DeviceModel.notSupported //"iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return DeviceModel.notSupported //"iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return DeviceModel.notSupported //"iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return DeviceModel.notSupported //"iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return DeviceModel.notSupported //"iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return DeviceModel.notSupported //"iPad Pro"
        case "AppleTV5,3":                              return DeviceModel.notSupported //"Apple TV"
        case "i386", "x86_64":                          return DeviceModel.notSupported //"Simulator"
        default:                                        return DeviceModel.notSupported //identifier
        }
    }
    
}
