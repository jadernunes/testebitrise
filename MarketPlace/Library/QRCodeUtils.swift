//
//  QRCodeUtils.swift
//  MarketPlace
//
//  Created by Leandro on 10/13/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

public class QRCodeUtils: NSObject {
    
    var qrcodeImage: CIImage!
    
    // MARK: IBAction method implementation
    
    func generateQRCode(qrString qrString: String, target: UIImageView) -> UIImage? {
        if qrcodeImage == nil {
            if qrString == "" {
                return nil
            }
            
            let data = qrString.dataUsingEncoding(NSISOLatin1StringEncoding, allowLossyConversion: false)// data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter?.setValue(data, forKey: "inputMessage")
            filter!.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter!.outputImage
            
            let scaleX = target.frame.size.width / qrcodeImage.extent.size.width
            let scaleY = target.frame.size.height / qrcodeImage.extent.size.height
            
            let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
            
            return UIImage(CIImage: transformedImage)
        }
        else {
            qrcodeImage = nil
            return nil
        }
        
    }
    
    // MARK: Custom method implementation
    func displayQRCodeImage() -> UIImage {
        let scaleY : CGFloat = 1
        let scaleX : CGFloat = 1
        
        let transformedImage = qrcodeImage.imageByApplyingTransform(CGAffineTransformMakeScale(scaleX, scaleY))
        
        return UIImage(CIImage: transformedImage)
    }
    
}
