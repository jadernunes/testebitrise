//
//  UIImageViewExtension.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

enum SDImageCacheType : Int {
    case None
    case Disk
    case Memory
}

typealias CUstomWebImageCompletionBlock = (UIImage!, NSError!, SDImageCacheType, NSURL!) -> Void

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if let imageData = data as NSData? {
                    self.image = UIImage(data: imageData)
                }
            }
        }
    }
    
    private func addImageInImageView(urlStringImage: String?, completionHandler completedBlock: CUstomWebImageCompletionBlock!) {
        var completeURL = BASE_URL_IMAGES
        
        if urlStringImage != "null" && urlStringImage != nil && urlStringImage != "<null>" && urlStringImage != "" {
            
            completeURL += urlStringImage!
            
            if !(urlStringImage?.containsString(".jpeg"))! &&
                !(urlStringImage?.containsString(".JPEG"))! &&
                !(urlStringImage?.containsString(".jpeg"))! &&
                !(urlStringImage?.containsString(".jpg"))! &&
                !(urlStringImage?.containsString(".png"))! &&
                !(urlStringImage?.containsString(".PNG"))!
            {
                completeURL += ".jpeg"
            }
        }
        
        self.sd_setImageWithURL(NSURL(string: completeURL), placeholderImage: UIImage.init(named: "placeholder")) { (completedBlock) in
        }
    }
    
    func addImage(urlStringImage: String?, completionHandler completedBlock: CUstomWebImageCompletionBlock!) {
        self.addImageInImageView(urlStringImage, completionHandler: completedBlock)
    }
    
    func addImage(urlStringImage: String?) {
        self.addImageInImageView(urlStringImage) { (image, error, cacheType, url) in
        
        }
    }
    
    func tintImage(color: UIColor) {
        let origImage = self.image
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.image = tintedImage
        self.tintColor = color
    }
}

