//
//  CAGradientLayer+Extension.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 08/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
extension CAGradientLayer {
    class func gradient4allLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.startPoint = CGPointMake(0.0,0.0)
        layer.endPoint = CGPointMake(1.0, 1.0)
        layer.colors = [UIColor.green4all().CGColor, UIColor.blue4all().CGColor]
        return layer
    }
    
    class func gradient4allLayerForBoundsForNavigation(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.startPoint = CGPointMake(0.2,0.4)
        layer.endPoint = CGPointMake(0.5, 1.0)
        layer.colors = [UIColor.green4all().CGColor, UIColor.blue4all().CGColor]
        return layer
    }
}
