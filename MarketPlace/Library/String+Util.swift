//
//  File.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 24/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension String {
    
    /// Util cancel in UIAlertViewController
    public static let actionSheetCancel = "Cencelar"
}
