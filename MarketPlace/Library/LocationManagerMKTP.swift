//
//  LocationManagerMKTP.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import CoreLocation
import CoreLocation

class LocationManagerMKTP: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationManagerMKTP()
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(locations[0], completionHandler: {
            placemarks, error in
            
            if error == nil && placemarks!.count > 0 {
                let placeMark = placemarks!.last! as CLPlacemark
                
                if SessionManager.sharedInstance.isShowMessageLocality == false {
                    SessionManager.sharedInstance.isShowMessageLocality = true
                    
                    let api = ApiServices()
                    api.successCase = { (result) in
                        if let jSon = result as? NSDictionary {
                            if jSon.objectForKey("citySupported")!.boolValue == false {
                                if placeMark.locality != nil && placeMark.administrativeArea != nil {
                                    let alert = UIAlertView.init(title: kAlertTitle, message: String.init(format: kMessageCityNotAvailable, placeMark.locality!), delegate: self, cancelButtonTitle: kAlertButtonOk)
                                    alert.show()
                                }
                            }
                        }
                    }
                    
                    api.failureCase = { (msg) in
                    }
                    if placeMark.locality != nil && placeMark.administrativeArea != nil {
                        api.verifyAppLocation(placeMark.locality!, state: placeMark.administrativeArea!)
                    }
                    else {
                        api.verifyAppLocation("", state: "")
                    }
                }
            }
        })
    }
}
