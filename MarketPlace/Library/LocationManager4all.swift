//
//  LocationManager4all.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 09/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: CLLocationManager {
    static let shared = LocationManager()
    
    override init() {
        super.init()
        
        requestWhenInUseAuthorization()
    }
    
    override func startUpdatingLocation() {
        desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        distanceFilter = 100.0
        super.startUpdatingLocation()
    }
}
