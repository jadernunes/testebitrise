//
//  StringExtension.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension NSString
{
    class func removeSpecialCharacters(completeString: String) -> String
    {
        var resultString = completeString.stringByReplacingOccurrencesOfString(" ", withString: "")
        resultString = resultString.stringByReplacingOccurrencesOfString("-", withString: "")
        resultString = resultString.stringByReplacingOccurrencesOfString("<", withString: "")
        resultString = resultString.stringByReplacingOccurrencesOfString(">", withString: "")
        
        return resultString
    }
    
    class func convertHourFromServer(stringHourFromServer: String) -> String
    {
        let subString: [String] = stringHourFromServer.componentsSeparatedByString(":00.000")
        return subString[0]
    }
    
    class func dateToCustomFormat(day: Int,month: Int,year: Int) -> NSString!
    {
        let customDate = NSDate.dateByDayMonthAndYear(day, month: month, year: year)
        
        //date format
        let formatter = NSString.formatDate("dd/MMMM - EEE")
        var dateString = formatter.stringFromDate(customDate!)
        dateString = dateString.uppercaseString
        
        return dateString
    }
    
    class func formatDate(format: NSString!) -> NSDateFormatter
    {
        //date format
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "pt_BR")
        formatter.dateFormat = format as String
        formatter.timeZone = NSTimeZone.localTimeZone()
        
        return formatter
    }
    
    class func dayLiteralByDayMonthAndYear(day: Int,month: Int,year: Int) -> NSString!
    {
        let customDate = NSDate.dateByDayMonthAndYear(day, month: month, year: year)
        
        //date format
        let formatter = self.formatDate("EEE")
        var dateString = formatter.stringFromDate(customDate)
        dateString = dateString.uppercaseString
        
        return dateString
    }
    
    class func monthLiteralByMonth(month: Int) -> NSString!
    {
        let customDate = NSDate.dateByMonth(month)
        
        //date format
        let formatter = self.formatDate("MMM")
        var dateString = formatter.stringFromDate(customDate)
        dateString = dateString.uppercaseString
        
        return dateString
    }
    
    class func getLiteralDayFromCalendarUSA(dateString:NSString!) -> String
    {
        // Set date format
        let dateFormateStart = NSString.formatDate("dd/MM/yyyy")
        let dateStart = dateFormateStart.dateFromString(dateString as String)!
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func getDateStringFromTimestamp(timestamp:Double) -> String{
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(timestamp)/1000)  //UTC time
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone() //Edit
        dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        dateFormatter.dateFormat = "dd/MM"
//        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
//        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        
        let strDateSelect = dateFormatter.stringFromDate(dateTimeStamp)
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.timeZone = NSTimeZone.localTimeZone()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        
        return strDateSelect
    }
    
    class func getDayStringFromTimestamp(timestamp:Double) -> String{
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(timestamp)/1000)  //UTC time
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone() //Edit
        dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        dateFormatter.dateFormat = "dd"
        //        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        //        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        
        let strDateSelect = dateFormatter.stringFromDate(dateTimeStamp)
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.timeZone = NSTimeZone.localTimeZone()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        
        return strDateSelect
    }
    
    class func getMonthStringFromTimestamp(timestamp:Double) -> String{
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(timestamp)/1000)  //UTC time
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone() //Edit
        dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        dateFormatter.dateFormat = "MMM"
        //        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        //        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        
        let strDateSelect = dateFormatter.stringFromDate(dateTimeStamp)
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.timeZone = NSTimeZone.localTimeZone()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        
        return strDateSelect
    }
    
    class func getHourStringFromTimestamp(timestamp:Double) -> String{
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(timestamp)/1000)  //UTC time
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone() //Edit
        dateFormatter.locale = NSLocale(localeIdentifier: "pt_BR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        
        let strDateSelect = dateFormatter.stringFromDate(dateTimeStamp)
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.timeZone = NSTimeZone.localTimeZone()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        
        return strDateSelect
    }
    
    class func getHourByDate(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("dd/MM/yyyy HH:mm:ss")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "HH:mm:ss"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func getHourByDateWithHourFormat(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("dd/MM/yyyy HH:mm:ss")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func getDateToServerByDateFormat(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("dd/MM/yyyy")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "yyyy-MM-dd"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func getDateByServerConvertToShow(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("yyyy-MM-dd")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "dd/MM/yyyy"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func getHourByServerConvertToShow(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("HH:mm:ss")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    class func horasMinutosToSeconds (HoraMinutos:String) -> Double {
        
        let formatar = NSDateFormatter()
        let calendar = NSCalendar.currentCalendar()
        formatar.locale = NSLocale.currentLocale()
        formatar.timeZone = NSTimeZone.localTimeZone()
        formatar.dateFormat = "HH:mm:ss"
        
        let Inicio = formatar.dateFromString(HoraMinutos)
        let comp = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: Inicio!)
        
        let hora = comp.hour
        let minute = comp.minute
        
        let hours = hora*3600
        let minuts = minute*60
        
        let totseconds: Double = Double.init(hours+minuts)
        
        return totseconds
    }
    
    class func getNumericDayByLiteral(dateStringStart:NSString!) -> String
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate("EEEE")
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "yyyy-MM-dd"
        let timeString = formatter.stringFromDate(dateStart)
        
        return timeString
    }
    
    func fromBase64() -> String?
    {
        if let data = NSData(base64EncodedString: self as String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters) {
            return String(data: data, encoding: NSUTF8StringEncoding)
        }
        return nil
    }
    
    func removeCharacters(characters: String) -> String
    {
        let characterSet = NSCharacterSet(charactersInString: characters)
        let components = self.componentsSeparatedByCharactersInSet(characterSet)
        let result = components.joinWithSeparator("")
        return result
    }
}

extension String {
    var isNumeric: Bool {
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self.characters).isSubsetOf(nums)
    }
    
    /// "Doesn't work"
    func dateFromString(format: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        dateFormatter.dateFormat = format
        return dateFormatter.dateFromString(self)
    }
}
