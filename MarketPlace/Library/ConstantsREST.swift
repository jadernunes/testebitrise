//
//  ConstantsREST.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 14/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

let ACTION_HOMES                = "homes"
let ACTION_FACTS                = "m/facts/"
let ACTION_GROUPS               = "groupTrees/"
let ACTION_MOVIES               = "movies/"
let ACTION_BUILDINGS            = "parkingBuildings/"
let ACTION_PROD_CAT             = "productCategories?unityId="
let ACTION_ORDER                = "orders"
let ACTION_GET_UNIT             = "unities"
let ACTION_GET_TICKET           = "wps/ticket/"
let ACTION_PAYMENT_TICKET       = "payments"
let ACTION_CHECKIN              = "checkin"
let ACTION_SCHEDULES            = "schedules"
let ACTION_DEVICES              = "devices"
let ACTION_SEARCH               = "search"
let ACTION_CAMPAIGNS            = "campaigns"
let ACTION_COUPONS              = "coupons"
let ACTION_FIDELITY             = "fidelityStamps"
let ACTION_EVENT_TICKETS        = "tickets"
let ACTION_VOUCHERS             = "vouchers"
let ACTION_FEES                 = "/fees"
let ACTION_PAY_TRANSACTION      = "/customer/payTransaction"
let ACTION_REFUND_TRANSACTION   = "/customer/refundTransaction"
let ACTION_SUPPORT_CITY         = "supportedCities"
let ACTION_NEWS_NOTIFICATIONS   = "m/newsNotifications"
let ACTION_GET_CEP              = "address?cep="
let ACTION_GET_NEIGHBORHOOD_CITY    = "neighborhood?city="
let ACTION_GET_NEIGHBORHOOD_STATE   = "&state="
//Gastronomia
let ACTION_CEP                  = "unities?cep="
