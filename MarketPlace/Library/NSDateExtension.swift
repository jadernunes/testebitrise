//
//  NSDateExtension.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

extension NSDate
{
    class func dateByMonth(month: Int) -> NSDate!
    {
        //Date components by TimZone
        let dateComponents = NSDateComponents()
        dateComponents.month = month
        dateComponents.timeZone = NSTimeZone.localTimeZone() // user's own time zone is used if not specified
        
        // Create date from components
        let userCalendar = NSCalendar.currentCalendar() // user calendar
        let someDateTime = userCalendar.dateFromComponents(dateComponents)
        
        return someDateTime
    }
    
    class func dateByDayMonthAndYear(day: Int,month: Int,year: Int,hour:Int,minute:Int) -> NSDate!
    {
        //Date components by TimZone
        let dateComponents = NSDateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.timeZone = NSTimeZone.localTimeZone() // user's own time zone is used if not specified
        
        // Create date from components
        let userCalendar = NSCalendar.currentCalendar() // user calendar
        let someDateTime = userCalendar.dateFromComponents(dateComponents)
        
        return someDateTime
    }
    
    class func dateByDayMonthAndYear(day: Int,month: Int,year: Int) -> NSDate!
    {
        //Date components by TimZone
        let dateComponents = NSDateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.timeZone = NSTimeZone.localTimeZone() // user's own time zone is used if not specified
        
        // Create date from components
        let userCalendar = NSCalendar.currentCalendar() // user calendar
        let someDateTime = userCalendar.dateFromComponents(dateComponents)
        
        return someDateTime
    }
    
    class func day() -> Int
    {
        let components = self.dateComponetsFromThisMoment(.Day)
        let day = components.day
        
        //Return day
        return day
    }
    
    class func month() -> Int
    {
        let components = self.dateComponetsFromThisMoment(.Month)
        let day = components.month
        
        //Return day
        return day
    }
    
    class func year() -> Int
    {
        let components = self.dateComponetsFromThisMoment(.Year)
        let day = components.year
        
        //Return day
        return day
    }
    
    class func getDaysInMonth() -> Int
    {
        let date = NSDate()
        let cal = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian)!
        let range = cal.rangeOfUnit(.Day, inUnit: .Month, forDate: date)
        
        return range.length
    }
    
    class func getDaysByMonth(month: Int) -> Int
    {
        let date = NSDate.dateByDayMonthAndYear(1, month: month, year: NSDate.year())
        let cal = NSCalendar(calendarIdentifier:NSCalendarIdentifierGregorian)!
        let range = cal.rangeOfUnit(.Day, inUnit: .Month, forDate: date)
        
        return range.length
    }
    
    func hour() -> Int
    {
        //Get Hour
        let components = NSDate.dateComponetsFromThisMoment(.Hour)
        
        let hour = components.hour
        
        //Return Hour
        return hour
    }
    
    func minute() -> Int
    {
        //Get Minute
        let components = NSDate.dateComponetsFromThisMoment(.Minute)
        let minute = components.minute
        
        //Return Minute
        return minute
    }
    
    class func toShortTimeString(dateC: NSDate) -> String
    {
        //Get Short Time String
        let formatter = NSDateFormatter()
        //        formatter.timeStyle = .ShortStyle
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let timeString = formatter.stringFromDate(dateC)
        
        //Return Short Time String
        return timeString
    }
    
    class func getDateTodayToFormat() -> String
    {
        let date = NSDate()
        
        //Get Short Time String
        let formatter = NSDateFormatter()
        //        formatter.timeStyle = .ShortStyle
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "dd/MM/yyyy"
        let timeString = formatter.stringFromDate(date)
        
        //Return Short Time String
        return timeString
    }
    
    class func getDateTodayToShortYearFormat() -> String
    {
        let date = NSDate()
        
        //Get Short Time String
        let formatter = NSDateFormatter()
        //        formatter.timeStyle = .ShortStyle
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "dd/MM/yy"
        let timeString = formatter.stringFromDate(date)
        
        //Return Short Time String
        return timeString
    }
    
    class func getTimeTodayToFormat() -> String
    {
        let date = NSDate()
        
        //Get Short Time String
        let formatter = NSDateFormatter()
        //        formatter.timeStyle = .ShortStyle
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.stringFromDate(date)
        
        //Return Short Time String
        return timeString
    }
    
    class func dateComponetsFromThisMoment(calendarUnit: NSCalendarUnit) -> NSDateComponents!
    {
        let currentDate = NSDate()
        
        //Get day
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(calendarUnit, fromDate: currentDate)
        
        return components
    }
    
    class func dateComponetsToEEE(calendarUnit: NSCalendarUnit,dateString:NSString!) -> NSDateComponents!
    {
        let dateString = NSString.convertHourFromServer(dateString as String)
        
        let dateFormate = NSString.formatDate("dd/MM/yyyy")
        let date = dateFormate.dateFromString(dateString as String)!
        
        //Get day
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(calendarUnit, fromDate: date)
        
        return components
    }
    
    class func dateComponetsFromDateWithFormat(calendarUnit: NSCalendarUnit,dateString:NSString!) -> NSDateComponents!
    {
        let dateStringMerged = NSString.convertHourFromServer(dateString as String) as String?
        
        let dateFormate = NSString.formatDate("dd/MM/yyyy")
        
        var date = NSDate()
        if dateFormate.dateFormat != nil {
            if dateFormate.dateFromString((dateStringMerged)!) != nil {
                date = dateFormate.dateFromString((dateStringMerged)!)!
            }
        }
        
        //Get day
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(calendarUnit, fromDate: date)
        
        return components
    }
    
    class func timeIntervalNow() -> NSTimeInterval
    {
        let currentDate = NSDate()
        
        return currentDate.timeIntervalSince1970*1000
    }
    
    class func secondsByDifferenceDates(dateStringStart:NSString!, dateStringFinish:NSString!) -> NSTimeInterval
    {
        return self.secondsByDifferenceDatesWithFormat(dateStringStart, dateStringFinish: dateStringFinish, format: "dd/MM/yyyy HH:mm:ss")
    }
    
    class func secondsByDifferenceDatesWithFormat(dateStringStart:NSString!, dateStringFinish:NSString!, format:NSString!) -> NSTimeInterval
    {
        let dateStringStart = NSString.convertHourFromServer(dateStringStart as String)
        let dateStringFinish = NSString.convertHourFromServer(dateStringFinish as String)
        
        // Set date format
        let dateFormateStart = NSString.formatDate(format)
        let dateStart = dateFormateStart.dateFromString(dateStringStart as String)!
        
        let dateFormateFinish = NSString.formatDate(format)
        let dateFinsh = dateFormateFinish.dateFromString(dateStringFinish as String)!
        
        return dateStart.timeIntervalSinceDate(dateFinsh)
    }
    
    class func nextDateStringByTimeInterval(startDate: NSDate, newTimeInter: NSTimeInterval) -> NSString
    {
        let dateFormateStart = NSString.formatDate("dd/MM/yyyy HH:mm:ss")
        
        let newDate = startDate.dateByAddingTimeInterval(newTimeInter)
        return dateFormateStart.stringFromDate(newDate)
    }
    
    class func dayByDateWithFormat(dateString:NSString!) -> Int
    {
        let dateString = NSString.convertHourFromServer(dateString as String)
        
        let components = self.dateComponetsFromDateWithFormat(.Day, dateString: dateString)
        let day = components.day
        
        //Return day
        return day
    }
    
    class func monthByDateWithFormat(dateString:NSString!) -> Int
    {
        let dateString = NSString.convertHourFromServer(dateString as String)
        
        let components = self.dateComponetsFromDateWithFormat(.Month, dateString: dateString)
        let month = components.month
        
        //Return month
        return month
    }
    
    class func yearByDateWithFormat(dateString:NSString!) -> Int
    {
        let dateString = NSString.convertHourFromServer(dateString as String)
        
        let components = self.dateComponetsFromDateWithFormat(.Year, dateString: dateString)
        let year = components.year
        
        //Return year
        return year
    }
    
    class func completeDateByStringDateWithFormat(dateString:NSString!) -> NSDate
    {
        let dateString = NSString.convertHourFromServer(dateString as String)
        
        let dateFormate = NSString.formatDate("dd/MM/yyyy HH:mm:ss")
        return dateFormate.dateFromString(dateString as String)!
    }
    
//    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
//        return self.compare(dateToCompare) == NSComparisonResult.OrderedAscending
//    }
//    
//    func isLessThanDate(dateToCompare: NSDate) -> Bool {
//        return self.compare(dateToCompare) == NSComparisonResult.OrderedDescending
//    }
    
    class func currentDateInFormat(format: String = "HH:mm:ss") -> String {
        let currentDate = NSDate()
        let dateFormatter = NSDateFormatter()
        //dateFormatter.locale = NSLocale.currentLocale()
        //dateFormatter.timeZone = NSTimeZone(abbreviation: "BRST")
        dateFormatter.dateFormat = format
        return dateFormatter.stringFromDate(currentDate)
    }
}
