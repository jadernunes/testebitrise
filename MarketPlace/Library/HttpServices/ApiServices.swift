//
//  ApiServices.swift
//  MarketPlace
//
//  Created by Luciano on 6/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import CoreLocation

class ApiServices: NSObject {

    var successCase: ((AnyObject?) -> (Void)) = { (obj) in}
    var failureCase: ((String) -> (Void)) = {(msg) in}
    var headers : [String : String] = [:]
    var alamoFireManager = Alamofire.Manager.sharedInstance
    
    override init() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 30 // seconds
        alamoFireManager = Alamofire.Manager(configuration: configuration)
        
        headers = [
            "Authorization": TOKEN_AUTHORIZATION,
            "Content-Type": "application/json",
            "User-Agent":SystemInfo.getSystemInfo()
        ]
    }
    
    func updateHader(){
        headers = [
            "Authorization": TOKEN_AUTHORIZATION,
            "Content-Type": "application/json",
            "User-Agent":SystemInfo.getSystemInfo()
        ]
        
        Util.print("Headers: \n\(headers)")
    }
    
    //MARK: - List Sections
    func listSections(targetURLRoute:String?=nil) -> Void{
        self.updateHader()
        
        var urlString = BASE_URL + ACTION_HOMES + "?inside=true"
        
        if let route :String = targetURLRoute {
            if route.characters.count > 0 {
                urlString.appendContentsOf("&homeTypeName=" + route)
            }
        }
        
        alamoFireManager.request(.GET, urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let contentResponse = response.result.value as? [String: AnyObject] {
                        Util.print("Data: \(contentResponse)")
                        if let content = contentResponse["sections"] as? NSArray where content.count>0 {
                            self.successCase(content)
                        }else{
                            self.successCase(NSArray())
                        }
                    }else{
                        self.successCase(NSArray())
                    }
                    
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - List Facts
    func listActiveFacts() -> Void{
        self.updateHader()
        
        let urlString = BASE_URL + ACTION_FACTS + "?active=true"
        
        alamoFireManager.request(.GET, urlString, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }

    //MARK: - List Groups
    func listGroups(byTypeId : Int?) -> Void{
        self.updateHader()
        
        let urlString = BASE_URL + ACTION_GROUPS
        
        
        alamoFireManager.request(.GET, urlString, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - List Movies
    func listMovie(byTypeId : Int?) -> Void{
        self.updateHader()
        
        let urlString = BASE_URL + ACTION_MOVIES
        
        alamoFireManager.request(.GET, BASE_URL + ACTION_MOVIES, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success: 
                    self.successCase(nil)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - List Parking Buildings
    func listParkingBuildings() -> Void{
        self.updateHader()

        let urlString = BASE_URL + ACTION_BUILDINGS
        
        alamoFireManager.request(.GET, urlString, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let building = response.result.value! as? NSArray where building.count>0{
                        Util.print("Data: \(building)")
                        
                        let listIdsReceived = ParkingEntityManager.importFromArray(building)
                        Util.syncObject(Building.className(), stringIDs: listIdsReceived)
                    }
                    
                    self.successCase(NSArray())
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }

    //MARK: - List Unity Categories
    func listUnityCategories(unityId : CLong) -> Void{
        self.updateHader()
        
        let urlString = BASE_URL + "\(ACTION_PROD_CAT)\(unityId)&style=extended"
        
        alamoFireManager.request(.GET,  urlString, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let cats = response.result.value! as? NSArray where cats.count>0{
//                        ProductCategoryPersistence.deleteAll()
//                        UnityItemPersistence.deleteAll()
                        Util.print("Data: \(cats)")
                        
                        self.successCase(cats)
                    }else{
                        self.successCase(NSArray())
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }

    //MARK: - Send Order
    func sendOrder(order : Order, customerInfo : Dictionary<String, AnyObject>) ->Void {
        self.updateHader()
        
        var jsonObj : [String : AnyObject] = [:]
        var orderItems : [NSDictionary] = []
        
        jsonObj["idUnity"]          = order.idUnity
        jsonObj["status"]           = STATUS_SENT
        jsonObj["paymentStatus"]    = 1
        jsonObj["type"]             = 1
        jsonObj["productTotal"]     = order.productTotal
        jsonObj["discount"]         = order.discount
        jsonObj["deliveryFee"]      = order.deliveryFee
        jsonObj["placeLabel"]       = order.placeLabel
        jsonObj["observation"]      = order.observation
        jsonObj["customerInfo"]     = customerInfo
        jsonObj["idOrderType"]      = order.idOrderType
        jsonObj["total"] = order.total
        
        if (order.address != nil) {
            var address : [String: AnyObject] = [:]
            address["street"]           = order.address?.street
            address["neighborhood"]     = order.address?.neighborhood
            address["number"]           = order.address?.number
            address["complement"]       = order.address?.complement
            address["reference"]        = ""
            address["cep"]              = Util.removeCharacterInString(order.address?.zip, character: "-")
            address["uf"]               = order.address?.state
            address["city"]             = order.address?.city
            jsonObj["orderAddress"]     = address
        }
        
        for item in order.orderItems {
            var orderItem : [String: AnyObject] = [:]
            
            orderItem["idUnityItem"]    = item.unityItem?.id
            orderItem["quantity"]       = item.quantity
            orderItem["discount"]       = item.discount
            orderItem["observation"]    = item.observation
            orderItem["total"] = item.total
            orderItem["itemPrice"] = item.itemPrice
            
            var subOrderItems : [NSDictionary] = []
            
            for subItem in item.subItems {
                var subOrderItem : [String: AnyObject] = [:]
                
                subOrderItem["idUnityItem"]    = subItem.unityItem?.id
                subOrderItem["quantity"]       = subItem.quantity
                subOrderItem["discount"]       = subItem.discount
                subOrderItem["total"]          = subItem.total
                subOrderItem["itemPrice"]      = subItem.itemPrice
                subOrderItem["observation"]    = subItem.observation
                subOrderItems.append(subOrderItem)
            }
            
            orderItem["subItems"] = subOrderItems
            orderItems.append(orderItem)
        }
        
        jsonObj["orderItems"] = orderItems
        
        let orderCopy = Order(value: order)
        let urlString = BASE_URL + ACTION_ORDER
        
        alamoFireManager.request(.POST, urlString, parameters: jsonObj, headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let orderResponse = response.result.value! as? Dictionary<String, AnyObject> {
                        
                        Util.print("Data: \(orderResponse)")
                        
                        let realm = try! Realm()
                        
                        realm.beginWrite()
                        if orderResponse["id"] != nil {
                            orderCopy.id = orderResponse["id"] as! Int
                            orderCopy.idOrderType = order.idOrderType
                        }
                        
                        if orderResponse["idTokPdv"] != nil {
                            orderCopy.idTokPdv = orderResponse["idTokPdv"] as! Int
                        }
                        
                        if response.response?.statusCode == HTTP_RESPONSE_CREATED {
                            order.status = STATUS_SENT
                            orderCopy.status = STATUS_SENT
                            orderCopy.timestamp = NSDate().timeIntervalSince1970 * 1000
                        }
                        
                        realm.delete(order)
                        realm.add(orderCopy)
                        try! realm.commitWrite()
                        
                        let responseToOrder = ["order":orderCopy] as NSMutableDictionary
                        
                        let voucherObject = orderResponse["voucher"] as? NSDictionary
                        if voucherObject != nil {
                            responseToOrder.setObject(voucherObject!, forKey: "voucher")
                        }
                        
                        self.successCase(responseToOrder)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }

    }
    
    func sendCheck(check : Order, customerInfo : Dictionary<String, AnyObject>) ->Void {
        self.updateHader()
        
        var jsonObj : [String : AnyObject] = [:]
        var orderItems : [NSDictionary] = []
        
        jsonObj["id"]            = check.id
        jsonObj["idUnity"]       = check.idUnity
        jsonObj["status"]        = STATUS_DELIVERED
        jsonObj["paymentStatus"] = 1
        jsonObj["type"]          = 1
        jsonObj["productTotal"]  = check.productTotal
        jsonObj["discount"]      = check.discount
        jsonObj["deliveryFee"]   = check.deliveryFee
        jsonObj["total"]         = check.total
        jsonObj["placeLabel"]    = check.placeLabel
        jsonObj["observation"]   = "obs"
        jsonObj["customerInfo"]  = customerInfo
        
        //Address
        var address : [String: AnyObject] = [:]
        address["street"]           = "object - A"
        address["neighborhood"]     = "object - B"
        address["number"]           = "object - C"
        address["complement"]       = "object - D"
        address["reference"]        = "object - E"
        address["cep"]              = ""
        address["uf"]               = "RS"
        address["city"]             = "Porto Alegre"
        
        jsonObj["orderAddress"]     = address
        
        for item in check.orderItems {
            var orderItem : [String: AnyObject] = [:]
            
            orderItem["idUnityItem"]    = item.unityItem?.id
            orderItem["quantity"]       = item.quantity
            orderItem["discount"]       = item.discount
            orderItem["total"]          = item.total
            orderItem["observation"]    = item.observation
            
            var subOrderItems : [NSDictionary] = []
            
            for subItem in item.subItems {
                var subOrderItem : [String: AnyObject] = [:]
                
                subOrderItem["idUnityItem"]    = subItem.unityItem?.id
                subOrderItem["quantity"]       = subItem.quantity
                subOrderItem["discount"]       = subItem.discount
                subOrderItem["total"]          = subItem.total
                subOrderItem["observation"]    = subItem.observation
                subOrderItems.append(subOrderItem)
            }
            
            orderItem["subItems"] = subOrderItems
            
            orderItems.append(orderItem)
        }
        
        jsonObj["orderItems"] = orderItems
        
        let urlString = BASE_URL + ACTION_ORDER + "/\(check.id)/" + ACTION_PAYMENT_TICKET
        let checkCopy = Order(value: check)
        
        alamoFireManager.request(.POST, urlString, parameters: jsonObj, headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let orderResponse = response.result.value! as? Dictionary<String, AnyObject> {
                        
                        Util.print("Data: \(orderResponse)")
                        
                        let realm = try! Realm()
                        
                        realm.beginWrite()
                        if orderResponse["id"] != nil {
                            checkCopy.id = orderResponse["id"] as! Int
                        }
                        
                        if response.response?.statusCode == HTTP_RESPONSE_CREATED {
                            checkCopy.status = STATUS_DELIVERED
                            checkCopy.timestamp = NSDate().timeIntervalSince1970 * 1000
                        }
                        realm.delete(check)
                        realm.add(checkCopy)
                        try! realm.commitWrite()
                        
                        self.successCase(nil)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
        
    }
    
    //MARK: - List Unity Categories
    func getOrderStatus(externalId : String) -> Void{
        self.updateHader()
        
        if(externalId.characters.count > 0) {
            let urlString = BASE_URL + "orders/\(externalId)/history"
            
            alamoFireManager.request(.GET, urlString, parameters: [:], headers: headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .Success:
                        if let status = response.result.value! as? NSArray where status.count>0{
                            Util.print("Data: \(status)")
                            
                            self.successCase(status)
                            
                        }else{
                            self.successCase(NSArray())
                        }
                        break
                        
                    case .Failure:
                        self.handleError(response)
                        break
                    }
            }
        }
    }
    
    //MARK: - Payment on 4all account
    func call4allPayTransaction(sessionToken : String, transactionId: String, cardId: String, amount:Int,cnpj:String,campaignUUID:String? = nil,couponUUID:String? = nil) -> Void{
        self.updateHader()

        var parameters = ["sessionToken": sessionToken,
                          "transactionId": transactionId,
                          "cardId": cardId,
                          "waitForTransaction": true,
                          "amount":amount,
                          "cnpj":cnpj
            ] as NSMutableDictionary!
        
        if campaignUUID != nil && couponUUID != nil {
            parameters = ["sessionToken": sessionToken,
                          "transactionId": transactionId,
                          "cardId": cardId,
                          "waitForTransaction": true,
                          "amount":amount,
                          "cnpj":cnpj,
                          "campaignUUID":campaignUUID!,
                          "couponUUID":couponUUID!
                ] as NSMutableDictionary!
        }
        
        if cnpj.characters.count == 0 {
            parameters.removeObjectForKey("cnpj")
        }
        
        let urlString = C4ALL_BASE_URL + ACTION_PAY_TRANSACTION
        
        alamoFireManager.request(.POST, urlString, parameters: parameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseData = response.result.value! as? NSDictionary {
                        
                        Util.print("Data: \(responseData)")
                        
                        self.successCase(responseData)
                        
                    }else{
                        self.successCase(Int())
                    }
                    
                    break
                    
                case .Failure:
                    var errorMessage = "Ocorreu um erro. Por favor, tente novamente."

                    if let data = response.data {
                        let responseData = String(data: data, encoding: NSUTF8StringEncoding)
                        
                        let responseJSON = self.JSONParseDict(responseData!)
                        if let error = responseJSON["error"] as? Dictionary<String, AnyObject> {
                            if let msg = error["message"] as? String {
                                errorMessage = msg
                            }
                        }
                    }
                    self.failureCase(errorMessage)
                    break
                }
        }
    }
    
    func refundTransaction(sessionToken : String, transactionId: String){
        self.updateHader()
        
        let parameters = ["sessionToken": sessionToken,
                          "transactionId": transactionId
            ] as NSDictionary!
        
        let urlString = C4ALL_BASE_URL + ACTION_REFUND_TRANSACTION
        
        alamoFireManager.request(.POST, urlString, parameters: parameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let status = (response.result.value! as? NSDictionary)?.objectForKey("status") {
                        Util.print("Data: \(status)")

                        self.successCase(status)
                    }else{
                        self.successCase(nil)
                    }
                    
                    break
                    
                case .Failure:
                    var errorMessage = "Ocorreu um erro. Por favor, tente novamente."
                    
                    if let data = response.data {
                        let responseData = String(data: data, encoding: NSUTF8StringEncoding)
                        
                        let responseJSON = self.JSONParseDict(responseData!)
                        if let error = responseJSON["error"] as? Dictionary<String, AnyObject> {
                            if let msg = error["message"] as? String {
                                errorMessage = msg
                            }
                        }
                    }
                    self.failureCase(errorMessage)
                    break
                }
        }
    }
    
    //MARK: - Get calendar by Unit
    func calendarByUnit(idUnit: Int) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT + "/\(idUnit)/schedules?mode=mobile"
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let contentResponse = response.result.value as? NSDictionary {
                        Util.print("Data: \(contentResponse)")

                        self.successCase(contentResponse)
                    }else{
                        self.successCase(nil)
                    }
                    
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    func valueForTicket(ticketNumber: String,UUID: String) -> Void{
        self.updateHader()
        let urlString: String = BASE_URL + ACTION_GET_TICKET + "\(ticketNumber)?deviceUuid=\(UUID)"
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \(response.result.value)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    func checkCep(cep: String) -> Void {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_CEP + cep
        print("Sent GET checkCep: \(urlString)")
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    print("Received Success GET: \(urlString)")
                    Util.print("Data: \(response.result.value)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    print("Received Fail GET - getCep: \(urlString)\n\(response)")
                    self.handleError(response)
                    break
                }
        }

    }
    
    func getNeighborhoods(city: String, state: String) -> Void {
        let urlString: String = BASE_URL + ACTION_GET_NEIGHBORHOOD_CITY + city + ACTION_GET_NEIGHBORHOOD_STATE + state
        print("Sent POST getNeighborhoods: \(urlString)")
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    print("Received Success GET: \(urlString)")
                    Util.print("Data: \(response.result.value)")
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    print("Received Fail GET - getNeighborhoods: \(urlString)\n\(response)")
                    self.handleError(response)
                    break
                }
        }
    }
    
    func getAllTickets(id : Int) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_FACTS + "\(id)/" + ACTION_EVENT_TICKETS
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    print("Received Success GET: \(urlString)")
                    Util.print("Data: \(response.result.value)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    print("Received Fail GET - getAllTickets: \(urlString)\n\(response)")
                    self.handleError(response)
                    break
                }
        }
        
    }
    
    func ticketPayment(ticketNumber: String, dictionaryParameters: NSDictionary) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_TICKET + "\(ticketNumber)/" + ACTION_PAYMENT_TICKET
        
        alamoFireManager.request(.POST,urlString,parameters: dictionaryParameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \(response.result.value)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
        
    }
    
    func eventTicketPayment(id: Int, dictionaryParameters: NSDictionary) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_EVENT_TICKETS + "/\(id)/" + ACTION_VOUCHERS
        
        alamoFireManager.request(.POST,urlString,parameters: dictionaryParameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseSuccess = (response.result.value as? NSArray) {
                        Util.print("Data: \(response.result.value)")
                        
                        VoucherPersistence.importFromArray(responseSuccess)
                        self.successCase(responseSuccess)
                    } else {
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
        
    }
    
    //MARK: - Schedules
    func makeSchedule(dicSchedule : NSDictionary) ->Void {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT + "/\(dicSchedule.objectForKey("idUnity") as! Int)/schedules"
        
        alamoFireManager.request(.POST, urlString , parameters: dicSchedule as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let orderResponse = response.result.value! as? Dictionary<String, AnyObject> {
                        Util.print("Data: \(orderResponse)")
                        
                        self.successCase(orderResponse)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    func getAllScheduleBySessionToken(sessionToken: String) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_SCHEDULES + "?sessionToken=\(sessionToken)"
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \(response.result.value)")

                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    func getScheduleById(idSchedule: String) -> Void{
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_SCHEDULES + "/\(idSchedule)"
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    self.successCase((response.result.value)!)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //List Schedules history
    func getScheduleHistory(idSchedule : String) -> Void{
        self.updateHader()

        let urlString: String = BASE_URL + ACTION_SCHEDULES + "/" + String(idSchedule) + "/history"
        
        alamoFireManager.request(.GET, urlString, parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let status = response.result.value! as? NSArray where status.count>0{
                        Util.print("Data: \(status)")
                        
                        self.successCase(status)
                        
                    }else{
                        self.successCase(NSArray())
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Geofence
    
    func enteredLocation(parameters: NSDictionary) {
        self.updateHader()

        let urlString = BASE_URL + ACTION_CHECKIN
        
        alamoFireManager.request(.POST, urlString, parameters: (parameters as! [String : AnyObject]), encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                Util.print("Data: \(JSON)")
            case .Failure:
                break
            }
        }
    }
    
    //MARK: - Register Device Info
    
    func registerDeviceInfo(dicRegister : NSDictionary) ->Void {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_DEVICES
        
        alamoFireManager.request(.POST, urlString , parameters: dicRegister as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let orderResponse = response.result.value! as? Dictionary<String, AnyObject> {
                        Util.print("Data: \(orderResponse)")
                        
                        self.successCase(orderResponse)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    ////MARK: - Util
    func handleError(response : Response<AnyObject, NSError>){
        var errorMessage = "Ocorreu um erro. Por favor, tente novamente."
        
        if let data = response.data {
            let responseData = String(data: data, encoding: NSUTF8StringEncoding)
            let responseJSON = JSONParseDict(responseData!)
            if let message: String = responseJSON["message"] as? String {
                if !message.isEmpty {
                    errorMessage = message
                }
            }
        }
        
        failureCase(errorMessage)
    }
    
    func JSONParseDict(jsonString:String) -> Dictionary<String, AnyObject> {
        
        if let data: NSData = jsonString.dataUsingEncoding(
            NSUTF8StringEncoding){
            
            do{
                if let jsonObj = try NSJSONSerialization.JSONObjectWithData(
                    data,
                    options: NSJSONReadingOptions(rawValue: 0)) as? Dictionary<String, AnyObject>{
                    return jsonObj
                }
            }catch{
            }
        }
        return [String: AnyObject]()
    }

    
    //MARK: - Search anything
    
    func searchAnythingOnServer(searchText: String = "",sessionToken: String = "")
    {
        self.updateHader()

        let urlString: String = BASE_URL + ACTION_SEARCH
        
        let dicParameters = [
            "query":searchText,
            "sessionToken":sessionToken
            ] as NSDictionary!
        
        alamoFireManager.request(.POST, urlString , parameters: dicParameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let orderResponse = response.result.value! as? NSArray {
                        if orderResponse.count > 0 {
                            Util.print("Data: \(orderResponse)")
                            self.successCase(orderResponse)
                        } else {
                            self.successCase(nil)
                        }
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get all Unities
    
    func listUnities(){
        
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get Unity By ID
    
    func getUnityByIDService(idUnity: Int){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT + "/" + String(idUnity)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    if let unity = response.result.value! as? NSDictionary {
                        UnityPersistence.importFromDictionary(unity)
                        self.successCase(unity)
                    } else {
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get Unity By CEP
    
    func getUnitiesByCEP(address: Address){
        self.updateHader()
        if let zip = address.zip {
            let zipFilter = String(zip.characters.filter { "01234567890".characters.contains($0) })
            var urlString: String = BASE_URL + ACTION_CEP + zipFilter
            let lat = NSString(format: "&lat=%.15f", latitude)
            let lon = NSString(format: "&lon=%.15f", longitude)
            urlString = "\(urlString)\(lat)\(lon)"
            
            alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .Success:
                        self.successCase(response.result.value)
                        break
                    case .Failure:
                        self.handleError(response)
                        break
                    }
            }
        }
    }
    
    //MARK: - Get Fact by ID
    
    func getFactByID(idFact: Int){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_FACTS + String(idFact)
        
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")

                    if let fact = response.result.value! as? NSDictionary {
                        FactEntityManager.importFromDictionary(fact)
                        self.successCase(fact)
                    } else {
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get Group by ID
    
    func getGroupByID(idGroup: Int){
        self.updateHader()

        let urlString: String = BASE_URL + ACTION_GROUPS + String(idGroup)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    if let fact = response.result.value! as? NSDictionary {
                        self.successCase(fact)
                    } else {
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Campaign

    //Get Campaign by ID in Unity
    func getCampaignByID(idUnity: Int,idCampaign: Int){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT + "/" + String(idUnity) + "/" + ACTION_CAMPAIGNS + "/" + String(idCampaign)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    if let object = response.result.value! as? NSDictionary {
                        CampaignPersitence.importFromDictionary(object)
                        self.successCase(object)
                    } else {
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get Order History
    
    func getOrdersHistory(listProducts: Bool = true){
        self.updateHader()

        let lib = Lib4all.sharedInstance()
        if lib.hasUserLogged() {
            let user = User.sharedUser()
            if user != nil {
                if user.token != nil {
                    var urlString: String = BASE_URL + ACTION_ORDER + "?sessionToken=" + user.token
                    
                    if listProducts == true {
                        urlString += "&style=extended"
                    }
                    
                    alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
                        .validate()
                        .responseJSON { response in
                            switch response.result {
                            case .Success:
                                Util.print("Data: \((response.result.value)!)")
                                
                                if let arrayOrders = response.result.value! as? NSArray where arrayOrders.count>0 {
                                    OrderEntityManager.sharedInstance.importFromArray(arrayOrders)
                                    self.successCase(arrayOrders)
                                    break
                                } else {
                                    self.successCase(NSArray())
                                }
                                break
                                
                            case .Failure:
                                
                                self.handleError(response)
                                break
                            }
                    }
                }
            }
        }
    }

    //Get all Campaigns By Unity
    func getAllCampaignsByIdUnity(idUnity: Int){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT + "/" + String(idUnity) + ACTION_CAMPAIGNS
    
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //Get all Campaigns By Marketplace
    func getAllCampaigns() {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_CAMPAIGNS
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Coupom
    
    //MARK: - Claim
    func claimCoupom(idCampaign:Int, sessionToken:String) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_CAMPAIGNS + "/" + String(idCampaign) + "/" + ACTION_COUPONS
        
        let dicParameters = [
            "idCampaign":idCampaign,
            "sessionToken":sessionToken
            ] as NSDictionary!
        
        
        alamoFireManager.request(.POST, urlString , parameters: dicParameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSDictionary {
                        Util.print("Data: \(responseReceived)")
                        CoupomPersistence.importFromDictionary(responseReceived)
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get coupom by id
    func getCoupomByID(idCampaign:Int, idCoupom:Int) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_CAMPAIGNS + "/" + String(idCampaign) + "/" + ACTION_COUPONS + "/" + String(idCoupom)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSDictionary {
                        Util.print("Data: \(responseReceived)")

                        CoupomPersistence.importFromDictionary(responseReceived)
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:

                    self.handleError(response)
                    break
                }
        }
    }

    //MARK: - Get coupom by id
    func getCoupomBySessionToken(sessionToken:String) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_COUPONS + "?sessionToken=" + String(sessionToken)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSArray where responseReceived.count>0 {
                        Util.print("Data: \(responseReceived)")

                        CoupomPersistence.deleteAllObjects()
                        CoupomPersistence.importFromArray(responseReceived)
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get my vouchers
    func getVouchersBySessionToken(sessionToken:String) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_VOUCHERS + "?sessionToken=" + String(sessionToken)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    let responseReceived = response.result.value as? NSArray
                    if  responseReceived?.count > 0  {
                        Util.print("Data: \(responseReceived)")
                        
                        VoucherPersistence.deleteAllObjects()
                        VoucherPersistence.importFromArray(responseReceived!)
                        self.successCase(responseReceived!)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get coupom by id
    func getMyFidelitiesByUnityAndSessionToken(idUnity:Int, sessionToken:String) {
        self.updateHader()
        
        let urlString: String = BASE_URL + "me/" + ACTION_GET_UNIT + "/\(idUnity)/" + ACTION_FIDELITY + "?sessionToken=" + String(sessionToken)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSDictionary {
                        Util.print("Data: \(responseReceived)")
                        
                        MyFidelityPersistence.importFromDictionary(responseReceived)
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get my fidelities
    func getMyFidelities(sessionToken:String) {
        self.updateHader()
        
        let urlString: String = BASE_URL + "me/" + ACTION_FIDELITY + "?sessionToken=" + String(sessionToken)
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSArray where responseReceived.count>0 {
                        Util.print("Data: \(responseReceived)")

                        MyFidelityPersistence.deleteAllObjects()
                        MyFidelityPersistence.importFromArray(responseReceived)
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - Get checks
    func getChecksFromServer(placeLabel:String,idUnity: Int) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_ORDER + "?placeLabel=" + String(placeLabel) + "&idUnity=" + String(idUnity)
        
        let safeURL = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        alamoFireManager.request(.GET,safeURL,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSDictionary {
                        Util.print("Data: \(responseReceived)")
                        
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    func checkDeliveryFee(address: Address, idUnity: Int) {
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_SEARCH + ACTION_FEES
        
        var jsonObj: [String : AnyObject] = [:]
        
        let zip = String(address.zip!.characters.filter { "01234567890".characters.contains($0) })
        jsonObj["cep"]                  = zip
        jsonObj["uf"]                   = address.state
        jsonObj["city"]                 = address.city
        jsonObj["neighborhood"]         = address.neighborhood
        jsonObj["idUnity"]              = idUnity
        
        alamoFireManager.request(.POST,urlString,parameters: jsonObj, encoding: .JSON)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    self.successCase(response.result.value)
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }

    }

    func verifyAppLocation(city:String, state: String){
        self.updateHader()
        
        var urlString: String = BASE_URL + ACTION_SUPPORT_CITY
        var newCity = ""

        if let str = city.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet()) {
            newCity = str
        }
        
        urlString += "?city=" + newCity + "&state=" + state
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSDictionary {
                        Util.print("Data: \(responseReceived)")
                        
                        self.successCase(responseReceived)
                    }else{
                        self.successCase(nil)
                    }
                    break
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    //MARK: - New Notification
    func getNewsNotifications(city:String, state:String) {
        self.updateHader()
        
        let strCity = city.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let strState = state.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let urlString: String = BASE_URL + ACTION_NEWS_NOTIFICATIONS + "?city=\(strCity!)&state=\(strState!)&limit=2"
        
        alamoFireManager.request(.GET,urlString,parameters: [:], headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let responseReceived = response.result.value! as? NSArray {
                        if responseReceived.count > 0 {
                            NewsNotificationPersistence.save(responseReceived[0] as! NSDictionary)
                            self.successCase(responseReceived[0] as! NSDictionary)
                        }
                    }else{
                        self.successCase(nil)
                    }
                    break
                    
                case .Failure:
                    self.handleError(response)
                    break
                }
        }
    }
    
    typealias Completion = ((listUnities: [Unity]?, success: Bool) -> Void)
    
    func getUnitiesNearBy(coordinate: CLLocationCoordinate2D? = nil, tipoPedido: Int? = 0, completion: ((listUnities: AnyObject?, success: Bool) -> Void)){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT
        
        var parameters = [String:AnyObject]()
        if let coord = coordinate {
            parameters = [
                "distance": 20000,
                "lat": coord.latitude,
                "lon": coord.longitude
            ]
        }
        
        Util.print("Send getUnitiesNearBy GET: \(urlString)\n data: \(parameters)")
        alamoFireManager.request(.GET,urlString,parameters: parameters, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    completion(listUnities: response.result.value, success: true)
                    self.successCase(response.result.value)
                    
                    break
                    
                case .Failure:
                    Util.print("Receive failure GET: \(urlString)\n Data: \((response.result.value))")
                    completion(listUnities: nil, success: false)
                    self.handleError(response)
                    break
                }
        }
    }
    
    func getUnitiesNearByToHome(coordinate: CLLocationCoordinate2D? = nil, completion: ((listUnities: AnyObject?, success: Bool) -> Void)){
        self.updateHader()
        
        let urlString: String = BASE_URL + ACTION_GET_UNIT
        
        var parameters = [String:AnyObject]()
        if let coord = coordinate {
            parameters = [
                "distance": 20000,
                "lat": coord.latitude,
                "lon": coord.longitude
            ]
        }
        
        Util.print("Send getUnitiesNearByToHome GET: \(urlString)\n data: \(parameters)")
        alamoFireManager.request(.GET,urlString,parameters: parameters, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    Util.print("Data: \((response.result.value)!)")
                    
                    completion(listUnities: response.result.value, success: true)
                    self.successCase(response.result.value)
                    
                    break
                    
                case .Failure:
                    Util.print("Receive failure GET: \(urlString)\n Data: \((response.result.value))")
                    completion(listUnities: nil, success: false)
                    self.handleError(response)
                    break
                }
        }
    }
}
