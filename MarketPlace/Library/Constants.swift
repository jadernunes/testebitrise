//
//  Constants.swift
//  MarketPlace
//
//  Created by Luciano on 6/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

//General Info
let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist")
let dicInfo = NSDictionary(contentsOfFile: path!)
let dicConfig = dicInfo?.objectForKey("CONFIG")

//Config Menu
let pathConfigMenu = NSBundle.mainBundle().pathForResource("CONFIG_MENU", ofType: "plist")
let dicInfoConfigMenu = NSDictionary(contentsOfFile: pathConfigMenu!)
let LIST_SECTIONS_SIDE_MENU = dicInfoConfigMenu?.objectForKey("Cells") as! NSArray
let LIST_COLOR_SIDE_MENU = dicInfoConfigMenu?.objectForKey("View") as! NSDictionary

let BASE_URL            = dicConfig?.objectForKey("URL_BASE_API") as! String
let C4ALL_BASE_URL      = dicConfig?.objectForKey("URL_BASE_4ALL") as! String
let BASE_URL_IMAGES     = dicConfig?.objectForKey("URL_BASE_IMAGE") as! String
let TOKEN_AUTHORIZATION = dicConfig?.objectForKey("authorization") as! String
let BASE_URL_MAP        = dicConfig?.objectForKey("URL_BASE_MAP") as! String

let ACTION_HOMES                = "homes"
let ACTION_FACTS                = "facts/"
let ACTION_GROUPS               = "groupTrees/"
let ACTION_MOVIES               = "movies/"
let ACTION_BUILDINGS            = "parkingBuildings/"
let ACTION_PROD_CAT             = "productCategories?unityId="
let ACTION_ORDER                = "orders"
let ACTION_GET_UNIT             = "unities"
let ACTION_GET_TICKET           = "wps/ticket/"
let ACTION_PAYMENT_TICKET       = "payments"
let ACTION_CHECKIN              = "checkin"
let ACTION_SCHEDULES            = "schedules"
let ACTION_DEVICES              = "devices"
let ACTION_SEARCH               = "search"

let ACTION_CAMPAIGNS            = "campaigns"
let ACTION_COUPONS              = "coupons"
let ACTION_VOUCHERS             = "vouchers"
let ACTION_FIDELITY             = "fidelityStamps"

//IDENTIFIERS
let BANNER_IMAGE_IDENTIFIER         = "cellBannerImage"
let SCAN_CODE_IDENTIFIER            = "cellScanCode"
let DESCRIPTION_IDENTIFIER          = "cellDescription"
let MAP_IDENTIFIER                  = "cellMap"
let DETAILS_IDENTIFIER              = "cellDetails"
let BUTTON_IDENTIFIER               = "cellButton"
let CARDS_IDENTIFIER                = "cellMap"
let CARD_COL_IDENTIFIER             = "cellCardCollection"
let STANDARD_IDENTIFIER             = "cellStandard"
let STORE_IDENTIFIER                = "cellStoreBanner"
let CARD_IDENTIFIER                 = "cellCard"
let ALL_MOVIES_IDENTIFIER           = "cellAllMovies"
let STORE_BANNER_DETAILS_IDENTIFIER = "cellStoreBannerDetailsIdentifier"
let PAGES_IDENTIFIER                = "cellPages"
let STORE_UNITY_ITEMS               = "cellUnityItems"
let UNITY_RESERVE_AND_LINE          = "cellReserveAndLine"
let UNITY_CONTACT                   = "cellUnityContact"
let TIME_RANGE                      = "cellTimeRange"

//Campaign detail
let CELL_HEADER_CAMPAING            = "cellHeaderCampaign"
let CELL_ACTIONS_CAMPAING           = "cellActionsCampaign"
let CELL_TERMS_CAMPAING             = "cellTermsCampaign"
let CELL_DESCRIPTION_CAMPAING       = "cellDescriptionCampaign"
//Campaign List
let CELL_CAMPAING                   = "cellCampaign"
//Fidelity
let CELL_FIDELITY                   = "cellFidelity"

//FACT TYPE
let FACT_TYPE_EVENT = 1
let FACT_TYPE_PROMO = 2

//ID
let ID_MARKETPLACE = "2"

//ORDER STATUS
let STATUS_SENT             = 20
let STATUS_CONFIRMED        = 30
let STATUS_IN_PRODUCTION    = 50
let STATUS_TO_DELIVERY      = 70
let STATUS_DELIVERED        = 80
let STATUS_CANCELED         = 200

//HTTP RESPONSE
let HTTP_RESPONSE_CREATED = 201

//NOTIFICATION OBSERVER
let NOTIF_CART_OBSERVER = "UpdateCartControllerContent"
let HOME_CART_OBSERVER = "UpdateInOrderStatus"

// PAYMENT STATUS
let PAYMENT_CONFIRMED = 3

enum UnitItemType: Int {
    case Product = 1
    case Service = 2
}

enum UnitType: Int {
    case Service = 1
    case Store = 2
    case HotSite = 3
}

enum StatusOrder: Int {
    case Open               = 0
    case Sent               = 20
    case PaymentConfirmed   = 27
    case Confirmed          = 30
    case Preparing          = 50
    case Finished           = 60
    case ToDelivery         = 70
    case Delivered          = 80
    case Canceled           = 200
}

enum StatusSchedule: Int {
    case Sent       = 20
    case Confirmed  = 30
    case Canceled   = 200
}

enum StatusDescription: String {
    case Open       = "Aberto"
    case Sent       = "Enviado"
    case Confirmed  = "Confirmado"
    case Preparing  = "Em produção"
    case Finished   = "Finalizado"
    case ToDelivery = "Enviado para entrega"
    case Delivered  = "Entregue"
    case Canceled   = "Cancelado"
}

enum ContentType: String {
    case Order      = "orders"
    case Banner     = "banner"
    case Parking    = "estacionamento"
    case Movie      = "cinema"
    case Grid       = "grid"
    case Info       = "info"
    case Map        = "map"
}

enum SubTypeFact: Int {
    case Event      = 1
    case Promotion  = 2
    case Warning    = 3
    case Cupom      = 4
}

enum TypeObjectDetail: Int {
    case Service    = 1
    case Store      = 2
    case Event      = 3
    case Promotion  = 4
    case Warning    = 5
    case Group      = 6
    case Campaign   = 7
}

enum SectionsMenu: Int {
    case Promos         = 0
    case Stores         = 1
    case Foodcourt      = 2
    case Movies         = 3
    case Services       = 4
    case Events         = 5
    case Directions     = 6
    case OndeParei      = 7
    case PayParking     = 8
    case Orders         = 9
    case Schedules      = 10
    case Profile        = 11
    case Campaign       = 12
    case Notifications  = 13
    case Coupom         = 14
    case Vouchers       = 15
}

enum TypeShowListCoupons: Int {
    case meCoupons      = 0
    case allCupons      = 1
}

enum TypeDiscountCampaign: Int {
    case price      = 1
    case percentage = 2
}

enum PushesType: Int {
    case BeautyStatus     = 1
    case TableStatus      = 2
    case LineStatus       = 3
    case OrderStatus      = 4
    case PaymentStatus    = 5
    case PromoStatus      = 6
    case MakePayment      = 7
}

enum DeviceModel : Int {
    case notSupported   = 0
    case iPhone4        = 1
    case iPhone4s       = 2
    case iPhone5        = 3
    case iPhone5c       = 4
    case iPhone5s       = 5
    case iPhoneSE       = 6
    case iPhone6        = 7
    case iPhone6Plus    = 8
    case iPhone6s       = 9
    case iPhone6sPlus   = 10
    case iPhone7        = 11
    case iPhone7plus    = 12
    case iPodTouch5     = 13
    case iPodTouch6     = 14
}

enum ShortcutItem: String {
    case ShortcutScanQR                 = "com.fourall.mktplace.stotal.scanQR"
    case ShortcutOndeParei              = "com.fourall.mktplace.stotal.ondeParei"
    case ShortcutPagarEstacionamento    = "com.fourall.mktplace.stotal.PagarEstacionamento"
}

enum VoucherStatus : Int {
    case Created    = 1
    case Used       = 2
    case Cancelled  = 3
    case Expired    = 4
}

enum VoucherType : Int {
    case Cinema     = 1
    case Event      = 2
    case Coupon     = 3
    case Order      = 4
    case Fidelity   = 5
}

enum TypePageSuccess : Int {
    case PagamentoTicket    = 1
    case PagamentoQR        = 2
    case PagamentoPedido    = 3
    case Schedule           = 4
}
