//
//  PageManager.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PageManager: NSObject,UIWebViewDelegate {
    
    static let sharedInstance   = PageManager()
    var window                  : UIWindow!
    var newsViewController      :NewsNotificationViewController!
    var formSheet               : MZFormSheetController!
    var localNews               : NewsNotification?
    var rootViewController      : NewHomeViewController!
    
    override init() {
        self.window = UIApplication.sharedApplication().windows[0]
    }
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.window?.rootViewController {
            return self.getVisibleViewControllerFrom(rootViewController)
        }
        return nil
    }
    
    func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        
        if vc.isKindOfClass(UINavigationController.self) {
            
            let navigationController = vc as! UINavigationController
            return self.getVisibleViewControllerFrom( navigationController.visibleViewController!)
            
        } else if vc.isKindOfClass(UITabBarController.self) {
            
            let tabBarController = vc as! UITabBarController
            return self.getVisibleViewControllerFrom(tabBarController.selectedViewController!)
            
        } else {
            
            if let presentedViewController = vc.presentedViewController {
                
                return self.getVisibleViewControllerFrom(presentedViewController)
                
            } else {
                
                return vc;
            }
        }
    }
    
    func openQRScan() {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let activeViewController = self.visibleViewController() as! SidePanelViewController
        
        let homeViewController = activeViewController.centerPanel as! UINavigationController
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
        (vc.viewControllers[0] as! ScannerViewController).delegate = homeViewController.viewControllers[0] as! HomeTableViewController
        (vc.viewControllers[0] as! ScannerViewController).titleString = "LEITOR DE CÓDIGO QR"
        (activeViewController.centerPanel as! UINavigationController).presentViewController(vc, animated: true, completion: nil)
    }
    
    func openOndeParei() {
        var activeViewController = self.visibleViewController()
        var navigationHomeViewController: UINavigationController!
        
        if activeViewController!.isKindOfClass(SidePanelViewController) {
            navigationHomeViewController = (activeViewController as! SidePanelViewController).centerPanel as! UINavigationController
            
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC")
            
            var alreadyShow = false
            for children in navigationHomeViewController.childViewControllers {
                if children.isKindOfClass(OndePareiViewController) {
                    alreadyShow = true
                }
            }
            
            if(alreadyShow == false && (activeViewController?.isKindOfClass(OndePareiViewController))! == false) {
                
                navigationHomeViewController.pushViewController(viewController, animated: true)
                self.window?.rootViewController = activeViewController
            }
        } else if activeViewController!.isKindOfClass(ScannerViewController) {
            activeViewController?.dismissViewControllerAnimated(false, completion: {
                activeViewController = self.visibleViewController()
                navigationHomeViewController = (activeViewController as! SidePanelViewController).centerPanel as! UINavigationController
                
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC")
                
                var alreadyShow = false
                for children in navigationHomeViewController.childViewControllers {
                    if children.isKindOfClass(OndePareiViewController) {
                        alreadyShow = true
                    }
                }
                
                if(alreadyShow == false && (activeViewController?.isKindOfClass(OndePareiViewController))! == false) {
                    
                    navigationHomeViewController.pushViewController(viewController, animated: true)
                    self.window?.rootViewController = activeViewController
                }
            })
        } else {
            
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC")
            
            var alreadyShow = false
            for children in navigationHomeViewController.childViewControllers {
                if children.isKindOfClass(OndePareiViewController) {
                    alreadyShow = true
                }
            }
            
            if(alreadyShow == false && (activeViewController?.isKindOfClass(OndePareiViewController))! == false) {
                
                navigationHomeViewController.pushViewController(viewController, animated: true)
                self.window?.rootViewController = activeViewController
            }
        }
    }
    
    func openPagarEstacionamento() {
        var activeViewController = self.visibleViewController()
        var navigationHomeViewController: UINavigationController!
        
        if activeViewController!.isKindOfClass(SidePanelViewController) {
            navigationHomeViewController = (activeViewController as! SidePanelViewController).centerPanel as! UINavigationController
            
            let viewController = UIStoryboard(name: "Parking Storyboard", bundle: nil).instantiateViewControllerWithIdentifier("parkingVC")
            
            var alreadyShow = false
            for children in navigationHomeViewController.childViewControllers {
                if children.isKindOfClass(PaymentViewController) {
                    alreadyShow = true
                }
            }
            
            if(alreadyShow == false && (activeViewController?.isKindOfClass(PaymentViewController))! == false) {
                
                navigationHomeViewController.pushViewController(viewController, animated: true)
                self.window?.rootViewController = activeViewController
            }
        } else if activeViewController!.isKindOfClass(ScannerViewController) {
            activeViewController?.dismissViewControllerAnimated(false, completion: { 
                activeViewController = self.visibleViewController()
                navigationHomeViewController = (activeViewController as! SidePanelViewController).centerPanel as! UINavigationController
                
                let viewController = UIStoryboard(name: "Parking Storyboard", bundle: nil).instantiateViewControllerWithIdentifier("parkingVC")
                
                var alreadyShow = false
                for children in navigationHomeViewController.childViewControllers {
                    if children.isKindOfClass(PaymentViewController) {
                        alreadyShow = true
                    }
                }
                
                if(alreadyShow == false && (activeViewController?.isKindOfClass(PaymentViewController))! == false) {
                    
                    navigationHomeViewController.pushViewController(viewController, animated: true)
                    self.window?.rootViewController = activeViewController
                }
            })
        } else {
            
            let viewController = UIStoryboard(name: "Parking Storyboard", bundle: nil).instantiateViewControllerWithIdentifier("parkingVC")
            
            var alreadyShow = false
            for children in navigationHomeViewController.childViewControllers {
                if children.isKindOfClass(PaymentViewController) {
                    alreadyShow = true
                }
            }
            
            if(alreadyShow == false && (activeViewController?.isKindOfClass(PaymentViewController))! == false) {
                
                navigationHomeViewController.pushViewController(viewController, animated: true)
                self.window?.rootViewController = activeViewController
            }
        }
    }
    
    //MARK: - News Notification
    
    func getNewNotifications(){
        
        self.showPopupNewsNotification()
        
        let session = SessionManager.sharedInstance
        if session.isVerifyNewsNotification == false {
            let api = ApiServices()
            
            api.successCase = {(obj) in
                self.showPopupNewsNotification()
            }
            
            api.failureCase = { (msg) in
            }
            
            SystemInfo.getLocationStringInfo({ (lCity, lState) in
                api.getNewsNotifications(lCity, state: lState)
            })
            
            
        }
    }
    
    private func showPopupNewsNotification(){
        localNews = NewsNotificationPersistence.getLastNotificationAvaliable()
        if localNews != nil {
            
            let session = SessionManager.sharedInstance
            session.isVerifyNewsNotification = true
            
            newsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("NewsNotificationViewController") as! NewsNotificationViewController
            newsViewController.urlString = localNews!.src
            
            let deviceBounds = UIScreen.mainScreen().bounds
            formSheet = MZFormSheetController.init(viewController: newsViewController)
            formSheet.shouldDismissOnBackgroundViewTap = true;
            formSheet.cornerRadius = 8.0;
            formSheet.portraitTopInset = 6.0;
            formSheet.landscapeTopInset = 6.0;
            formSheet.presentedFormSheetSize = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height - 40);
            formSheet.formSheetWindow.transparentTouchEnabled = false;
        }
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.presentNewsNotification()
    }
    
    private func presentNewsNotification(){
        
        NewsNotificationPersistence.updateToRead(localNews!)
        formSheet.presentAnimated(true) { (viewController: UIViewController!) in
        }
    }
}
