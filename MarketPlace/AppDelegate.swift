//
//  AppDelegate.swift
//  Shopping Total
//
//  Created by 4all on 6/2/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Fabric
import Crashlytics
import Locksmith
import GoogleMaps
import GooglePlaces
import CoreLocation
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    //MARK: - App delegate methods
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let enviroment4all = Environment(rawValue: ENVIROMENT_4ALL)
        Lib4all.setEnvironment(enviroment4all!)
        let applicationID = "IOS_App4all_" + SystemInfo.getVersionApp()
        Lib4all.setApplicationID(applicationID)
        Lib4all.setApplicationVersion(SystemInfo.getVersionApp())
        
        Util.putsInfoInDeviceSettings()
        
        GMSPlacesClient.provideAPIKey("AIzaSyBSyuQQarLBRBT7E5ZwBR")

        //Remove all title on back button on Navigation Bar
        let barAppearace = UIBarButtonItem.appearance()
        barAppearace.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics:UIBarMetrics.Default)
        
        self.realmMigration()
        
        //Mobility
        let api = ApiServicesMobility()
        api.successCase = { (response) in
        }
        api.failureCase = { (cod, msg) in
        }
        api.consultaHome()
        
        //4Park
        GMSServices.provideAPIKey(keyAPIServices)
        GMSPlacesClient.provideAPIKey(keyAPIPlacesClient)
        
        GoogleAnalytics.sharedInstance
        PageManager.sharedInstance
        
        LayoutManager.loadConfig()
        
        let session = SessionManager.sharedInstance
        session.isVerifyNewsNotification = false //use to get news notification

        SystemInfo.getSystemInfo()
        Fabric.with([Crashlytics.self])
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
 
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
            print("Push received: \(remoteNotification)")
            
            let aps = remoteNotification["aps"] as! [String: AnyObject]
            let alert = aps["alert"] as! [String: AnyObject]
            
            let realm = try! Realm()
            let push = Push()
            push.id = PushPersistence.getNextKey(realm, aClass: Push.self)
            
            if remoteNotification["firstCustomField"] != nil {
                push.firstCustomField = (remoteNotification["firstCustomField"] as! String)
            }
            
            push.title = (alert["title"] as? String)!
            push.type = (remoteNotification["type"] as! Int)
            push.body = (alert["body"] as? String)!
            
            try! realm.write {
                realm.create(Push.self, value: push, update: true)
                
                let currentCountStr = UIApplication.sharedApplication().applicationIconBadgeNumber.description
                let currentCount = Int(currentCountStr)
                if(currentCount > 0) {
                    UIApplication.sharedApplication().applicationIconBadgeNumber = currentCount! + 1
                } else {
                    UIApplication.sharedApplication().applicationIconBadgeNumber = 1
                }
                SessionManager.sharedInstance.checkStatusOrders()
                NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_PUSHES_UPDATE, object: nil)
                
                //pagamento
                if push.type == PushesType.MakePayment.rawValue {
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let paymentViewController = mainStoryboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
                    
                    let payloadPayment = remoteNotification["pushPay"] as! NSDictionary
                    paymentViewController.strMerchantName = "\((payloadPayment.objectForKey("merchantName") as! String))"
                    
                    let paymentValue = payloadPayment.objectForKey("amount") as! Double
                    paymentViewController.strAmount = "\(paymentValue/100)"
                    
                    paymentViewController.strTransactionId = "\(payloadPayment.objectForKey("transactionId"))"
                    
                    let activeViewController = self.visibleViewController() as! SidePanelViewController
                    (activeViewController.centerPanel as! UINavigationController).pushViewController(paymentViewController, animated: true)
                    
                    self.window?.rootViewController = activeViewController
                } else if push.type == PushesType.Generic.rawValue {
                    Util.showIterativeMessage(typeActions: .OneOption,message:push.body!,title:push.title!)
                }
            }
        }
        
        self.verifyUserLocation()
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    func applicationWillEnterForeground(application: UIApplication) {
    }

    func applicationDidBecomeActive(application: UIApplication) {
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil))
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        //Request all data from Serve
        let apiUnities = ApiServices()
        apiUnities.listUnities()
        apiUnities.successCase = {(obj) in
            
            if let objAux = obj {
                if let listUnities = objAux.valueForKey("unities") as? [[String:AnyObject]] {
                    
                    var listIDs = ""
                    
                    for object in listUnities {
                        if let unity = object as? Unity {
                            for objectShift in unity.shifts {
                                if let shift = objectShift as? Shifts {
                                    listIDs += "," + String(shift.id)
                                }
                            }
                        }
                    }
                    
                    if listUnities.count > 0 {
                        let listIdsReceived = UnityPersistence.importFromArray(listUnities)
                        Util.syncObject(Unity.className(), stringIDs: listIdsReceived)
                        Util.syncObject(Shifts.className(), stringIDs: listIDs)
                    }
                }
            }
        }
        apiUnities.failureCase = {(msg) in
        }
        
        let apiFact = ApiServices()
        apiFact.listActiveFacts()
        apiFact.successCase = {(obj) in
            if let objAux = obj {
                if let facts = objAux.valueForKey("facts") as? NSArray {
                    if facts.count > 0 {
                        Util.print("Data: \(facts)")
                        TicketEntityManager.deleteAllTikects()
                        _ = FactEntityManager.importFromArray(facts)
                    }
                }
            }
        }
        apiFact.failureCase = {(msg) in
        }
    }

    func applicationWillTerminate(application: UIApplication) {
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
        }
        else {
            SessionManager.sharedInstance.registerDevice()
            application.unregisterForRemoteNotifications()
        }
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print (deviceToken)
        let token = NSString.removeSpecialCharacters(deviceToken.description)
        
        //Deixa em memória esse token
        PushTokenPersistence.save(token)
        SessionManager.sharedInstance.registerDevice()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Register Push fail: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        self.receiveRemoteNotification(application, userInfo: userInfo as [NSObject : AnyObject])
    }
    
    func receiveRemoteNotification(application: UIApplication, userInfo: [NSObject : AnyObject]) {
        print("Push received: \(userInfo)")
        
        let aps = userInfo["aps"] as! [String: AnyObject]
        let alert = aps["alert"] as! [String: AnyObject]
        
        let realm = try! Realm()
        
        let push = Push()
        push.id = PushPersistence.getNextKey(realm, aClass: Push.self)
        if(userInfo["firstCustomField"] != nil){
            push.firstCustomField = (userInfo["firstCustomField"] as! String)
        }
        push.title = (alert["title"] as? String)!
        push.type = (userInfo["type"] as! Int)
        push.body = (alert["body"] as? String)!
        
        try! realm.write {
            realm.create(Push.self, value: push, update: true)
            
            let currentCountStr = UIApplication.sharedApplication().applicationIconBadgeNumber.description
            let currentCount = Int(currentCountStr)
            if(currentCount > 0) {
                UIApplication.sharedApplication().applicationIconBadgeNumber = currentCount! + 1
            } else {
                UIApplication.sharedApplication().applicationIconBadgeNumber = 1
            }
            SessionManager.sharedInstance.checkStatusOrders()
            NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_PUSHES_UPDATE, object: nil)
            
            //pagamento
            if push.type == PushesType.MakePayment.rawValue {
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let paymentViewController = mainStoryboard.instantiateViewControllerWithIdentifier("PaymentQRViewController") as! PaymentQRViewController
                
                let payloadPayment = userInfo["pushPay"] as! NSDictionary
                
                paymentViewController.strMerchantName = "\((payloadPayment.objectForKey("merchantName") as! String))"
                
                let paymentValue = payloadPayment.objectForKey("amount") as! Double
                paymentViewController.strAmount = "\(paymentValue/100)"
                
                paymentViewController.strTransactionId = "\(payloadPayment.objectForKey("transactionId"))"
                
                let activeViewController = self.visibleViewController() as! SidePanelViewController
                (activeViewController.centerPanel as! UINavigationController).pushViewController(paymentViewController, animated: true)
                
            } else if push.type == PushesType.Generic.rawValue {
                Util.showIterativeMessage(typeActions: .OneOption,message:push.body!,title:push.title!)
            }
        }
    }
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.window?.rootViewController {
            return self.getVisibleViewControllerFrom(rootViewController)
        }
        return nil
    }
    
    func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        
        if vc.isKindOfClass(UINavigationController.self) {
            
            let navigationController = vc as! UINavigationController
            return self.getVisibleViewControllerFrom( navigationController.visibleViewController!)
            
        } else if vc.isKindOfClass(UITabBarController.self) {
            
            let tabBarController = vc as! UITabBarController
            return self.getVisibleViewControllerFrom(tabBarController.selectedViewController!)
            
        } else {
            
            if let presentedViewController = vc.presentedViewController {
                
                return self.getVisibleViewControllerFrom(presentedViewController)
                
            } else {

                return vc;
            }
        }
    }
    
    @available(iOS 9.0, *)
    func application(application: UIApplication, performActionForShortcutItem shortcutItem: UIApplicationShortcutItem, completionHandler: (Bool) -> Void) {
        
        if shortcutItem.type == ShortcutItem.ShortcutScanQR.rawValue {
            PageManager.sharedInstance.openQRScan()
        } else if shortcutItem.type  == ShortcutItem.ShortcutOndeParei.rawValue {
            PageManager.sharedInstance.openOndeParei()
        } else if shortcutItem.type  == ShortcutItem.ShortcutPagarEstacionamento.rawValue {
            PageManager.sharedInstance.openPagarEstacionamento()
        }
    }
    
    //MARK: - Realm Migration
    
    private func realmMigration() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 31,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 28) {
                    migration.renamePropertyForClass("Order", oldName: "type", newName: "idOrderType")
                }
        })
    }
    
    //MARK: - Location
    
    func verifyUserLocation(){
        
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        let loccationMKTP = LocationManagerMKTP.sharedInstance
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = loccationMKTP
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
}

