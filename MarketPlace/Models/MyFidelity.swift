//
//  MyFidelity.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class MyFidelity: Object {
    dynamic var id                  :CLong  = 0
    dynamic var idUnity             :Int = 0
    dynamic var maxStamp            :Int = 0
    dynamic var stampCount          :Int = 0
    dynamic var stampThumb          :String?
    dynamic var descriptionFidelity :String?
    dynamic var terms               :String?
    dynamic var winningDesc         :String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
