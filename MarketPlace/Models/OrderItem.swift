//
//  OrderItem.swift
//  MarketPlace
//
//  Created by Luciano on 8/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class OrderItem: Object {
    
    dynamic var id              : CLong  = 0
    dynamic var idOrder         : CLong  = 0 // id do pedido em que esse item está
    dynamic var parentId        : CLong  = 0 // id do OrderItem pai, caso seja um subItem
    dynamic var idFact          : CLong  = 0 // referencia a uma promoção
    dynamic var unityItem       : UnityItem?
    dynamic var quantity        : Double  = 0.0
    dynamic var discount        : Double  = 0.0
    dynamic var total           : Double  = 0.0 // subtotal do item, considerando os subItens
    dynamic var observation     : String?
    var subItems                = List<OrderItem>()
    dynamic var title           : String?
    dynamic var itemPrice       : Double  = 0.0

    override static func primaryKey() -> String? {
        return "id"
    }
}
