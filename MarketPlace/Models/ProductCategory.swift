//
//  ProductCategory.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ProductCategory: Object {
    
    dynamic var id                      :CLong = 0
    dynamic var name                    :String!
    dynamic var parentId                :Int = 0
    dynamic var unityId                 :Int = 0
    dynamic var productCategoryTypeId   :ProductCategoryType.RawValue = ProductCategoryType.invalid.rawValue
    let unityItems                      = List<UnityItem>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

