//
//  Building.swift
//  MarketPlace
//
//  Created by Luciano on 8/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Building: Object {
    
    dynamic var id              : CLong  = 0
    dynamic var name            : String!
    dynamic var desc            : String?
    let parkingFloors           = List<Floor>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
