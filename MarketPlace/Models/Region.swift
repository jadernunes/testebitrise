//
//  Region.swift
//  MarketPlace
//
//  Created by Luciano on 8/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Region: Object {
    
    dynamic var id          : CLong  = 0
    dynamic var name        : String!
    dynamic var desc        : String?
    dynamic var idFloor     : CLong  = 0
    let parkingSpaces       = List<Space>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
