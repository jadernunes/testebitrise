//
//  TicketEntityManager.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 16/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import RealmSwift

class TicketEntityManager : Object {
    
    typealias TicketCompletion = (Ticket?) -> ()
    
    class func getTicketBy(itsId id: CLong, completion: TicketCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Ticket.self).filter("id = \(id)").first)
        } else {
            return
        }
    }
    
    //MARK: - delete all objects
    static func deleteAllTikects()
    {
        let realm = try! Realm()
        var result: Results<Ticket>
        result = realm.objects(Ticket)
        
        try! realm.write {
            realm.delete(result)
        }
    }
}
