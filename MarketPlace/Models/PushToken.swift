//
//  PushToken.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 08/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class PushToken: Object {
    dynamic var id                      :CLong = 0
    dynamic var token                   :String!
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
