//
//  Order.swift
//  MarketPlace
//
//  Created by Luciano on 8/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Order: Object {
    
    dynamic var id              : Int  = 0
    dynamic var idTokPdv        : Int  = 0
    //dynamic var idExternal      : String = ""
    dynamic var idUnity         : Int  = 0
    var factId                  : RealmOptional<Int>!
    dynamic var timestamp       : Double = 0.0
    dynamic var dthrLastStatus  : Double = 0.0
    dynamic var status          : Int    = 0
    dynamic var paymentStatus   : Int    = 0
    dynamic var idOrderType     : Int    = 0
    dynamic var productTotal    : Double = 0.0
    dynamic var discount        : Double = 0.0
    dynamic var deliveryFee     : Double = 0.0
    dynamic var takeAwayFee     : Double = 0.0
    dynamic var total           : Double = 0.0
    dynamic var placeLabel = ""
    dynamic var observation     : String?
    let orderItems                   = List<OrderItem>()
    dynamic var address         : Address?
    let statusList              = List<OrderStatus>()
    dynamic var sessionToken    : String?
    dynamic var cardId          : String?
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["STATUS_SENT","STATUS_CONFIRMED", "STATUS_IN_PRODUCTION", "STATUS_TO_DELIVERY", "STATUS_DELIVERED", "STATUS_CANCELED"]
    }
    
    func getIdentificator() -> String {
        if idTokPdv > 0 {
            return "\(idTokPdv)"
        } else {
            return "\(id)"
        }
    }
    
    func getTextStatus() -> String{
        switch self.status {
        case 20, 27, 30: return "Pedido Recebido"
        case 50: return "Em Produção"
        case 60: return "Pronto para Entrega"
        case 70: return "Saiu para Entrega"
        case 80: return "Entregue"
        default: return ""
        }
        
    }
    
}
