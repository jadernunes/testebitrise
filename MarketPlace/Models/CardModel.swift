//
//  CardModel.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 17/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CardModel : Object {
    
    dynamic var issuerId :Int = 0
    dynamic var productId :Int = 0
    dynamic var cityId :Int = 0
    dynamic var productName :String = ""
    dynamic var issuerNr :String = ""
    dynamic var thumb :String = ""
    dynamic var balance :Double = 0
    dynamic var incomingBalance :Double = 0
    dynamic var rechargeable :Bool = false
    dynamic var checkBalance :Bool = false
    dynamic var balanceDate :Int64 = 0
    
    override static func primaryKey() -> String? {
        return "issuerNr"
    }
}
