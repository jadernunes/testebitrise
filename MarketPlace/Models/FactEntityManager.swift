//
//  FactEntityManager.swift
//  MarketPlace
//
//  Created by Luciano on 6/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class FactEntityManager: NSObject {

    static func importFromArray(content : NSArray) -> String {
        
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            for item in content {
                realm.create(Fact.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        FactEntityManager.inactivateFactsNoInListIds(listIdsReceived)
        
        return listIdsReceived
    }
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Fact.self, value: content, update: true)
        }
    }
    
    /// Set active to false all facts is not in list ids
    ///
    /// - Parameter listIds: List of Fact's id that separated by ','
    static func inactivateFactsNoInListIds(listIds: String) {
        let realm = try! Realm()
        let result = realm.objects(Fact.self).filter("NOT id IN {\(listIds)}")
        
        try! realm.write {
            result.forEach({ (fact: Fact) in
                fact.active = false
            })
        }
    }

    static func getFacts(factType : Int) -> NSMutableArray{
        let realm = try! Realm()
        let listFacts = NSMutableArray()
        
        try! realm.write {
            let result = realm.objects(Fact).filter(String(format: "factTypeId = \(factType) AND active = 1"))
            for item in result {
                listFacts.addObject(Fact(value: item))
            }
        }
        
        return listFacts
    }
    
    static func getFactByID(idFact : String) -> Fact? {
        
        let realm = try! Realm()
        let result: Fact?
        result = realm.objects(Fact.self).filter("id = \(idFact)").first
        
        return result
    }

    //pegar lista de idUntiy != 0 dos Facts com status = ativo e tipo promoção
    static func getUnityIDsWithPromotion()-> NSMutableArray
    {
        let realm = try! Realm()
        let listUnities = NSMutableArray()
        let resultIdsUnities = realm.objects(Fact).filter(String(format: "idUnity != 0 AND factTypeId = 2"))
        
        for fact in resultIdsUnities {
            if fact.timestampEnd > NSDate.timeIntervalNow() {
                listUnities.addObject(realm.objects(Unity.self).filter(String(format: "id = \(fact.idUnity)")).first!)
            }
        }
        return listUnities
    }
    
    static func getFactsByUnityId(idUnity : Int)-> Results<Fact>
    {
        let realm = try! Realm()
        var resultFacts: Results<Fact>
        resultFacts = realm.objects(Fact).filter(String(format: "idUnity = \(idUnity)"))
        return resultFacts
    }
    
    static func getAllFacts() -> Results<Fact> {
        let realm = try! Realm()
        let result = realm.objects(Fact)
        return result
    }
    
    //MARK: - delete all objects
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Fact>
        result = realm.objects(Fact.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
    
}
