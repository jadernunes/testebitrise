//
//  UnityItemModifier.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class UnityItemModifier: Object {
    
    dynamic var id                              :CLong = 0
    dynamic var name                            :String!
    dynamic var desc                            :String!
    dynamic var idUnityItem                     :Int = 0
    dynamic var idProductCategory               :Int = 0
    dynamic var minQuantity                     :Int = 0
    dynamic var maxQuantity                     :Int = 0
    dynamic var position                        :Int = 0
    dynamic var idUnityItemModifierPriceType    :UnityItemModifierPriceType.RawValue = UnityItemModifierPriceType.sum_total.rawValue
    dynamic var productCategory                 :ProductCategory?
    let unityItems                              = List<UnityItem>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
