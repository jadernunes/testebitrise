//
//  CardIssuerModel.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 18/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CardIssuerModel : Object {
    
    dynamic var issuer_id :Int = 0
    dynamic var nr_digits :Int = 0
    dynamic var checkBalance :Bool = false
    dynamic var thumb :String = ""
    dynamic var name :String = ""
    dynamic var mask :String = ""
    let products = List<CardModel>()
}
