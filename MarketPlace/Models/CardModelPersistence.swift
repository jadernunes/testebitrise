//
//  CardModelPersistence.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 18/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CardModelPersistence {
    
    static func storeCardIfNotExists(card:CardModel) -> Bool {
    
        let realm = try! Realm()
        var alreadyExists = false
        let issuerNr = card.issuerNr as String
        let storedCard = realm.objects(CardModel.self).filter(String(format: "issuerNr = '\(issuerNr)'")).first
        if (storedCard != nil) {
            alreadyExists = true
        }
        if (alreadyExists){
                return false
        }else {
            realm.beginWrite()
            realm.add(card, update:true)
            try! realm.commitWrite()
        }
        return true
    }
    
    static func removeCardFromStorageIfHave(issuerNumber: String) {
    
        let realm = try! Realm()
        let storedCard = realm.objects(CardModel.self).filter(String(format: "issuerNr = '\(issuerNumber)'")).first
    
        if (storedCard != nil) {
            realm.beginWrite()
            realm.delete(storedCard!)
            try! realm.commitWrite()
        }
    }
    
    static func getProductIfExists(issuerNumber: String) -> CardModel {
        
        let realm = try! Realm()
        let storedCard = realm.objects(CardModel.self).filter(String(format: "issuerNr = '\(issuerNumber)'")).first
        if storedCard != nil {
            return storedCard!
        }
        return CardModel()
        
    }
    
    static func getProductIfDateBalanceEqualsZero(issuerNumber: String) -> CardModel {
        
        let realm = try! Realm()
        let storedCard = realm.objects(CardModel.self).filter(String(format: "issuerNr = '\(issuerNumber)' AND balanceDate = 0")).first
        if storedCard != nil {
            return storedCard!
        }
        return CardModel()
    }

}
