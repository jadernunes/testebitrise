//
//  Ticket.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Ticket: Object {
    
    dynamic var id              : CLong     = 0
    dynamic var name            : String    = ""
    dynamic var idBatch         : Int       = 0
    dynamic var idEventSection  : Int       = 0
    dynamic var price           : Int       = 0
    dynamic var ticketLimit     : Int       = 0
    dynamic var ticketSold      : Int       = 0
    dynamic var batch           : Batch?
    dynamic var eventSection    : EventSection?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
