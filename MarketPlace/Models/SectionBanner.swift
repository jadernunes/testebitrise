//
//  SectionBanner.swift
//  MarketPlace
//
//  Created by Luciano on 8/19/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class SectionBanner: Object {
    
    dynamic var idSection   : Int   = 0
    dynamic var idFact      : Int   = 0
    dynamic var fact        : Fact?
    
}
