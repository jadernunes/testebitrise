//
//  EventSectionEntityManager.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 16/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import RealmSwift

class EventSectionEntityManager : Object {
    
    typealias EventsSectionCompletion = (Results<EventSection>) -> ()
    
    class func getEventsSectionBy(factId id: CLong, completion: EventsSectionCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(EventSection.self).filter("idFact = \(id)"))
        } else {
            return
        }
    }
    
    class func getEventsSectionAvaliable(factId id: CLong, completion: EventsSectionCompletion) {
        if let realm = try? Realm() {
            
                let listBatchs = realm.objects(Batch.self).filter("idFact = \(id) AND active = 1")
                
                if listBatchs.count > 0 {
                    var listEventSections = [EventSection]()
                    listBatchs.forEach({ (batch) in
                        
                        let timeEnd = (batch.sellingDateEnd.dateFromString("yyyy-MM-dd")?.timeIntervalSince1970)! * 1000
                        let timeBegin = (batch.sellingDateStart.dateFromString("yyyy-MM-dd")?.timeIntervalSince1970)! * 1000
                        
                        let today = NSDate.timeIntervalNow()
                        
                        if timeBegin <= today && timeEnd >= today {
                            batch.eventSections.forEach({ (eventSection) in
                                listEventSections.append(eventSection)
                            })
                        }
                    })
                    
                    if listEventSections.count > 0 {
                        var listIdsEventSection = ""
                        for index in 0...listEventSections.count-1 {
                            let eventSection = listEventSections[index]
                            if index == 0 {
                                listIdsEventSection = "\(eventSection.id)"
                            } else {
                                listIdsEventSection += "," + String(eventSection.id)
                            }
                        }
                        
                        let result = realm.objects(EventSection.self).filter("id IN {\(listIdsEventSection)}")
                        
                        completion(result)
                    }
                }
            
        } else {
            return
        }
    }
}

