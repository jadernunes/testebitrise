//
//  Voucher.swift
//  MarketPlace
//
//  Created by Leandro on 10/13/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Voucher: Object {
    dynamic var id                  :CLong                      = 0
    dynamic var uuid                :String?
    dynamic var idVoucherType       :VoucherType.RawValue       = 0
    dynamic var idVoucherStatus     :VoucherStatus.RawValue     = 0
    dynamic var timestamp           :Double                     = 0
    dynamic var timestampExpiration :Double                     = 0
    dynamic var order               :Order?
    dynamic var fidelity            :UnityFidelity?
    dynamic var ticket              :Ticket?
    dynamic var fact                :Fact?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
