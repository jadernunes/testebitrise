//
//  FileUtil.m
//  Recarga
//
//  Created by Jean Cainelli on 18/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import "MobilityFilesUtil.h"

@implementation MobilityFilesUtil


+ (BOOL)storeInFile:(NSDictionary *) dic fileName: (NSString *) fileName {
    
    NSString *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:paths]){
        NSError *err;
        [[NSFileManager defaultManager] createDirectoryAtPath:paths withIntermediateDirectories:NO attributes:nil error:&err];
        
        if (err!=nil){
            return NO;
        }
    }
    NSString *filePath = [paths stringByAppendingPathComponent:fileName];

    NSString* path = [[NSBundle mainBundle] pathForResource:@"mobilityCatalog"
                                                     ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    
    if(jsonData)
    {
        
        if(jsonData.bytes > 0)
        {
            jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
            
            if (!jsonData) {
                return NO;
            } else {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                NSError *err;
                [jsonString writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&err];
                
                if(err != nil){
                    return NO;
                } else {
                    return YES;
                }
                
            }
            
            return YES;
            
        }
        else
        {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSError *err;
            [jsonString writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&err];
            
            if(err != nil){
                return NO;
            } else {
                return YES;
            }
        }
    }
    else
    {
        return NO;
    }
}

+ (NSDictionary *)loadFromFile: (NSString *) fileName{
    
    NSString *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [paths stringByAppendingPathComponent:fileName];
    
    NSString *jsonText = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    @try {
        NSError *err;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[jsonText dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];

        //if (err!=nil){
        return dic;
        //}
    }
    
    @catch (NSException *exception) {
        return NULL;
    }
    return NULL;
}

@end
