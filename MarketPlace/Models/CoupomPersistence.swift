//
//  CoupomPersistence.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CoupomPersistence {

    //MARK: - Incremente ultima key
 
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Coupom.self, value: content, update: true)
        }
    }
    
    //MARK: - Import from Array
    
    static func importFromArray(content : NSArray) {
        let realm = try! Realm()
        
        try! realm.write {
            for object in content {
                realm.create(Coupom.self, value: object, update: true)
            }
        }
    }
    
    //MARK: - Get Coupom by ID
    
    static func getCoupomById(idCoupom : Int) -> Coupom?{
        
        let realm = try! Realm()
        let result = realm.objects(Coupom.self).filter(String(format: "id = \(idCoupom)")).first
        
        return result
    }
    
    //MARK: - Get all Coupons
    
    static func getAllLocalCoupons() -> Results<Coupom> {
        
        let realm = try! Realm()
        var result: Results<Coupom>
        result = realm.objects(Coupom.self)
        
        return result
    }
    
    //MARK: - Get Coupom by ID Campaign
    
    static func getCoupomByIdCampaign(idCampaign : Int) -> Coupom?{
        
        let realm = try! Realm()
        let result = realm.objects(Coupom.self).filter(String(format: "idCampaign = \(idCampaign)")).first
        
        return result
    }
    
    static func updateStatusCoupom(status: Bool,coupom: Coupom?){
        
        if coupom != nil {
            let realm = try! Realm()
            
            try! realm.write {
                coupom!.redeemed = status
                realm.add(coupom!, update: true)
            }
        }
    }
    
    //MARK: - delete all objects
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Coupom>
        result = realm.objects(Coupom.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
}
