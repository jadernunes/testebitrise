//
//  FileUtil.h
//  Recarga
//
//  Created by Jean Cainelli on 18/05/16.
//  Copyright © 2016 4all. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobilityFilesUtil : NSObject

+ (BOOL) storeInFile:(NSDictionary *) dic fileName: (NSString *) fileName ;

+ (NSDictionary *) loadFromFile: (NSString *) fileName ;

@end
