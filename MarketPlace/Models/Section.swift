//
//  Section.swift
//  MarketPlace
//
//  Created by Luciano on 8/19/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Section: Object {
    
    dynamic var id              : Int  = 0
    dynamic var title           : String = ""
    dynamic var position        : Int = 0
    dynamic var sectionTypeId   : Int = 0
    dynamic var sectionType     : Int = 0
    dynamic var screenConfig    : ScreenConfig?
    dynamic var content         : Content?
    dynamic var banner          : SectionBanner?
    dynamic var idHome          : Int = 0
    dynamic var inside          : Bool = false
    dynamic var active          : Bool = false
    dynamic var seeMore         : Bool = false
    dynamic var timeBegin       : String?
    dynamic var timeEnd         : String?
    dynamic var daysOfWeek      : String?
    
    override static func primaryKey() -> String? {
        return "id"
    }

}



