//
//  GroupEntityManager.swift
//  MarketPlace
//
//  Created by Luciano on 6/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class GroupEntityManager: NSObject {
    
    static func importFromArray(content : NSArray) -> String {
        
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            for item in content{
                realm.create(Group.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        return listIdsReceived
    }
    
    static func getGroupsChildren(groupId : Int) -> NSArray {
        let realm = try! Realm()
        
        realm.refresh()
        
        let listGroups = NSMutableArray()
        try! realm.write {
            let result = realm.objects(Group).filter(String(format: "id = \(groupId)")).first
            if result != nil {
                for item in result!.children {
                    listGroups.addObject(Group(value: item))
                }
            }
        }
        
        return listGroups
    }
    
    static func getGroupById(groupId : Int) -> Group?{
        let realm = try! Realm()
        var result : Group?
        try! realm.write {
            result = realm.objects(Group).filter(String(format: "id = \(groupId)")).first
        }
        
        return result
    }
    
    static func getGroupByTypeName(typeName : String) -> Group?{
        let realm = try! Realm()
        var result : Group?
        try! realm.write {  
            result = realm.objects(Group).filter(String(format: "groupTreeTypeName = '\(typeName)'")).first
        }
        
        return result
    }
    
    static func getGroupCustom(condition : String) -> Group?{
        let realm = try! Realm()
        var result : Group?
        try! realm.write {
            result = realm.objects(Group).filter(String(format: condition)).first
        }
        
        return result
    }

    static func getAllGroups() -> [Group]{
        let realm = try! Realm()
        var result = [Group]()
        try! realm.write {
            result.appendContentsOf(realm.objects(Group))
        }
        
        return result
    }
    
    //MARK: - delete all objects
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Group>
        result = realm.objects(Group.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }

}
