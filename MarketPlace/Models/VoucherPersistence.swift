//
//  VoucherPersistence.swift
//  MarketPlace
//
//  Created by Leandro on 10/13/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class VoucherPersistence: Object {
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Voucher.self, value: content, update: true)
        }
    }
    
    //MARK: - Save objects from Array
    
    static func importFromArray(content : NSArray) -> String {
        
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            for item in content{
                realm.create(Voucher.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        return listIdsReceived
    }
    
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Voucher>
        result = realm.objects(Voucher.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
    
    //MARK: - Get all vouchers
    
    static func getAllLocalVouchers(listType: [VoucherType]) -> Results<Voucher> {
        
        let realm = try! Realm()
        var result: Results<Voucher>
        
        var listTypes = ""
        for index in 0...listType.count-1 {
            let type = listType[index]
            if index == 0 {
                listTypes = "\(type.rawValue)"
            } else {
                listTypes += "," + String(type.rawValue)
            }
        }
        
        result = realm.objects(Voucher.self).filter("idVoucherType IN {\(listTypes)}")
        
        return result
    }
    
    static func getAllLocalNewVouchers(listType: [VoucherType], listStatus: [VoucherStatus]) -> [Voucher] {
        
        var result = VoucherPersistence.getAllLocalVouchers(listType).sort { (first: Voucher, second:Voucher) -> Bool in
            return first.timestampExpiration < second.timestampExpiration
        }
        
        result = result.filter { (voucher:Voucher) -> Bool in
            return listStatus.contains(VoucherStatus(rawValue: voucher.idVoucherStatus)!)
        }
        
        if listType.contains(VoucherType(rawValue: VoucherType.Cinema.rawValue)!) ||
            listType.contains(VoucherType(rawValue: VoucherType.Event.rawValue)!) {
            
            result = result.filter { (voucher: Voucher) -> Bool in
                if let ticket = voucher.ticket {
                    if let batch = ticket.batch {
                        if FactEntityManager.getFactByID(String(batch.idFact)) != nil {
                            return true
                        }
                    }
                }
                return false
            }
        }
        
        return result
    }
    
    //MARK: - Voucher by idFact
    
    static func getVouchersByIdFact(idFact: Int!) -> Results<Voucher> {
        
        let realm = try! Realm()
        var result: Results<Voucher>
        result = realm.objects(Voucher.self).filter(String(format: "ticket.batch.idFact = \(idFact)"))
        
        return result
    }
    
    //MARK: - Voucher by idOrder
    
    static func getVouchersByIdOrder(idOrder: Int!) -> Results<Voucher> {
        
        let realm = try! Realm()
        var result: Results<Voucher>
        result = realm.objects(Voucher.self).filter(String(format: "order.id = \(idOrder)"))
        
        return result
    }
    
    static func countVouchersNoUsed() -> Int{
        
        return 0
    }
    
    static func countIngressosNoUsed() -> Int{
        
        return 0
    }
}
