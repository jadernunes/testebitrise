 //
//  ShiftEntityManager.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 30/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class ShiftEntityManager : Object {

    typealias ShiftCompletion = (Shifts?) -> ()
    typealias ShiftsCompletion = (Results<Shifts>?) -> ()
    
    class func getCurrentShiftOfWeekByUnityId(unityId id: Int, completion: ShiftsCompletion) {
        if let realm = try? Realm() {
            let day = Util.currentDayOfWeek()
            
            let shifts = realm.objects(Shifts.self).filter("idUnity == \(id)")
            if shifts.count > 0 {
                completion(shifts.filter("dayOfWeek == '\(day)'"))
            } else {
                completion(nil)
            }
        } else {
            return
        }
    }
    
    class func getShiftByUnityId(unityId id: Int, completion: ShiftsCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Shifts.self).filter("idUnity == \(id)"))
        } else {
            return
        }
    }
    
    class func getShifstOfWeekByUnityIdAndDay(unityId id: Int,
                                                day: String,
                                                completion: ShiftsCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Shifts.self)
                .filter("idUnity == \(id)")
                .filter("dayOfWeek == '\(day)'"))
        }
        
    }
}
