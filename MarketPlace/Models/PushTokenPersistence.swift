//
//  PushTokenPersistence.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 08/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class PushTokenPersistence: Object {
    
    //MARK: - Incremente ultima key
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func save(token : String) {
        let realm = try! Realm()
        
        let localToken = realm.objects(PushToken.self).filter("token = '\(token)'").first
        if localToken == nil {
            
            //create a new token
            try! realm.write {
                realm.create(
                PushToken.self,
                value: [
                "id": getNextKey(realm, aClass: PushToken.self),
                "token":token],
                update: true)
            }
        }
    }
    
    static func getPushToken() -> String {
        let realm = try! Realm()
        
        let pushToken = realm.objects(PushToken.self).last
        if pushToken != nil {
            return (pushToken?.token)!
        }
        
        return ""
    }
}
