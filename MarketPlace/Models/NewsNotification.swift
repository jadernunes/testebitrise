//
//  NewsNotification.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class NewsNotification: Object {
    
    dynamic var id                  :CLong = 0
    dynamic var title               :String!
    dynamic var src                 :String!
    dynamic var dateTimeExpiration  :String!
    dynamic var active              :Bool = false
    dynamic var idMarketplace       :Int = 0
    dynamic var isRead              :Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
