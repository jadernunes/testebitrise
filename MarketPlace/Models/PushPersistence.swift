//
//  PushPersistence.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class PushPersistence {
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Get all pushes
    
    static func getAllPushes() -> Results<Push>{
        let realm = try! Realm()
        return realm.objects(Push.self)
    }
    
}
