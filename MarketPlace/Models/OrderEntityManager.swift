//
//  OrderEntityManager.swift
//  MarketPlace
//
//  Created by Luciano on 8/6/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class OrderEntityManager: NSObject {
    
    var total : Double = 0.0
    
    static let sharedInstance = OrderEntityManager()

    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    func getOrderTotalValue() -> Double {
        
        //tem carrinho ativo?
        let verifyCart = hasActiveCart()
        let cart = verifyCart.cart
        var tot = 0.0
        
        for order in cart.orderItems {
            tot += order.total
        }
        
        return tot
    }
    
    func getOrderItemByID(idOrderItem:Int) -> OrderItem? {
        
        let realm = try! Realm()
        var object: OrderItem?
        object = realm.objects(OrderItem.self).filter("id = \(idOrderItem)").first
        return object
    }
    
    func hasActiveOrder() -> Bool {
        let realm = try! Realm()
        let activeOrder = realm.objects(Order.self)
            .filter("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
            .first
        
        if activeOrder != nil {
            return true
        }
        
        return false
    }
    
    func hasOrderItemsInCart(order:Order?) -> Bool {
        
        if(order != nil ) {
            if order?.idUnity != 0 && (order?.orderItems == nil || order?.orderItems.count == 0) {
                return false
            }
        } else {
            return false
        }
        
        return true
    }
    
    func hasActiveCart() -> (active:Bool,cart:Order!) {
       
        //tem carrinho ativo?
        let realm = try! Realm()
        let cart = realm.objects(Order.self)
            .filter("status = 0")
            .first
        
        return (cart != nil ? (true,cart) : (false,nil))
    }
    
    func addOrderItemToCart(orderItem: OrderItem!,isFirstAdd:Bool) throws -> Bool {
        
        var storedOrderItem = orderItem
        let realm = try! Realm()
        realm.beginWrite()
        
        //tem carrinho ativo?
        let verifyCart = hasActiveCart()
        var cart = verifyCart.cart
        
        if hasActiveOrder() {
            realm.cancelWrite()
            throw NSError(domain: "Carrinho", code: 1, userInfo: ["message":"Outro pedido ja esta ativo :("])
        }
        
        if verifyCart.active == false {
            cart = realm.create(Order.self,value: ["id":getNextKey(realm, aClass: Order.self)])
            cart?.timestamp = NSDate().timeIntervalSince1970
            cart?.total = 0.0
            cart?.idUnity = (orderItem.unityItem?.unityId)!
            cart?.deliveryFee = 0.0
            cart?.status = 0
            
        } else if hasOrderItemsInCart(cart!) == false {//zera carrinho
            cart?.idUnity = (orderItem.unityItem?.unityId)!
        
        }else{
        
            //verifica se é a mesma loja
            if cart?.idUnity != orderItem.unityItem?.unityId {
                
                realm.cancelWrite()
                throw NSError(domain: "Carrinho", code: 1, userInfo: ["message":"Outra loja já adicionou itens ao carrinho :("])
            }
            
            if cart?.orderItems != nil {
                let auxOrderItem = hasOrderItemInCart(cart, orderItem: orderItem)
                if auxOrderItem != nil {
                    storedOrderItem = auxOrderItem
                    storedOrderItem!.quantity += 1
                    
                    storedOrderItem!.total += (storedOrderItem.itemPrice * storedOrderItem!.quantity)
                }
            }
            
        }
        
        // adicionando o item ao carrinho
        cart?.orderItems.append(storedOrderItem!)

        // atualiza o total do carrinho
        atualizaTotalDoCarrinho(cart)
        
        try! realm.commitWrite()
        return true
    }

    
    
    /// DEVE ESTAR DENTRO DE UMA TRANSAÇÃO NO MÉTODO QUE CHAMA !!!
    ///
    /// - Parameter cart: Order
    func atualizaTotalDoCarrinho(cart : Order) {
        
        var total = 0.0
        
        // somando o total de produtos
        for orderItem in cart.orderItems {
            total += orderItem.total
        }
        
        cart.productTotal = total
        cart.total = total + cart.deliveryFee - cart.discount
        
    }
    
    func addItemToCard(item : UnityItem, unity : Unity) throws -> Bool {
        
        var storedOrderItem : OrderItem?
        let realm = try! Realm()
        
        //tem carrinho ativo?
        let verifyCart = hasActiveCart()
        var cart = verifyCart.cart
        
        //tem order ativa?
        realm.beginWrite()
        
        if hasActiveOrder() {
            realm.cancelWrite()
            throw NSError(domain: "Carrinho", code: 1, userInfo: ["message":"Outro pedido ja esta ativo :("])
        }
        
        //caso não tenha order com status 0, é criado um novo objeto do tipo Order
        if verifyCart.active == false {
            cart = realm.create(Order.self,value: ["id":getNextKey(realm, aClass: Order.self)])
            cart?.timestamp = NSDate().timeIntervalSince1970
            cart?.total = 0.0
            cart?.idUnity = unity.id
            cart?.deliveryFee = 0.0
            cart?.status = 0
            
        } else if hasOrderItemsInCart(cart!) == false {//zera carrinho
            cart?.idUnity = unity.id

        }else{
            //verifica se é a mesma loja
            if cart?.idUnity != unity.id {
                realm.cancelWrite()
                throw NSError(domain: "Carrinho", code: 1, userInfo: ["message":"Outra loja já adicionou itens ao carrinho :("])
            }
            
            if cart?.orderItems != nil {
                storedOrderItem = isOnCart(cart, item: item)//verifica se o item está no carrinho
            }
            
        }
        
        //se storedOrderItem for != nil, encontra-se no carrinho
        if storedOrderItem != nil   {
            
            storedOrderItem!.quantity = storedOrderItem!.quantity + 1

            storedOrderItem?.total = (storedOrderItem?.itemPrice)! * (storedOrderItem?.quantity)!
            
            
        }else{
            
            storedOrderItem = realm.create(OrderItem.self, value: ["id": getNextKey(realm, aClass: OrderItem.self)])
            storedOrderItem?.quantity = 1
            storedOrderItem?.idOrder = cart!.id
            
            realm.add(item, update: true)
            
            storedOrderItem?.unityItem = item
            
            storedOrderItem?.itemPrice = item.price
            storedOrderItem?.total = item.price

            // adiciona aos itens do carrinho
            cart?.orderItems.append(storedOrderItem!)
        }
        
        atualizaTotalDoCarrinho(cart)
        
        realm.add(storedOrderItem!, update: true)
        
        try! realm.commitWrite()
        
        return true
    }
    
    func removeOrderItemInCart(orderItem:OrderItem!) -> Bool {
        let realm = try! Realm()
        var itemWasRemoved = false
        var cart : Order?
        try! realm.write {
            cart = realm.objects(Order.self).filter("status = 0").first
            if cart != nil {
                var index = 0
                for orderItemCart in (cart?.orderItems)! {
                    if orderItemCart.id == orderItem.id {
                        //OrderEntityManager.sharedInstance.getCart(0)?.total -= orderItem.total
                        if orderItemCart.quantity > 1 {
                            orderItemCart.quantity -= 1
                            //total += (storedOrderItem?.unityItem!.price)! * storedOrderItem!.quantity
                            var totalModifiers = 0.0
                            //Se há modificadores, multiplica o valor pela quantidade da UnityItem
                            if orderItemCart.unityItem?.unityItemModifiers.count > 0 {
                                for modifier in orderItemCart.subItems {
                                    totalModifiers += modifier.total * orderItemCart.quantity
                                }
                            }
                            //total += ((storedOrderItem?.unityItem!.price)! * storedOrderItem!.quantity) + totalModifiers
                            orderItemCart.total = orderItemCart.unityItem!.price * orderItemCart.quantity + totalModifiers
                            OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart!)
                        } else {
                            cart?.orderItems.removeAtIndex(index)
                            OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart!)
                        }
                        realm.refresh()
                        itemWasRemoved = true
                    }
                    index += 1
                }
            }
        }
        return itemWasRemoved
    }
    
    /**
     * remove item do carrinho ou diminui a quantidade
     */
    
    func removeItemFromCart(item : UnityItem, unity : Unity) -> Bool{
        
        let realm = try! Realm()
        var itemWasRemoved = false
        try! realm.write {
            //tem carrinho ativo?
            let cart = realm.objects(Order.self).filter("status = 0").first
            
            if cart != nil {
                var storedOrderItem : OrderItem?
                
                storedOrderItem = isOnCart(cart, item: item) //verifica se o item está no carrinho
                
                if storedOrderItem != nil && storedOrderItem?.quantity > 0{
                    //item no carrinho
                    if storedOrderItem?.quantity == 1 {
                        var indexItem   = 0
                        for itemOrder in cart!.orderItems {
                            if itemOrder.id == storedOrderItem?.id{
                                cart?.orderItems.removeAtIndex(indexItem)
                                break
                            }
                            indexItem += 1
                        }
                    }
                    
                    storedOrderItem!.quantity -= 1
                    storedOrderItem!.total = storedOrderItem!.unityItem!.price * storedOrderItem!.quantity
                    
                    OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart!)
                    realm.refresh()
                    itemWasRemoved = true
                }
            }
        }
        return itemWasRemoved
    }
    
    /*
        Retorna uma order ativo onde stauts == 0
     */
    
    func getCart(status : Int) -> Order?{
        let realm = try! Realm()
        
        //tem carrinho ativo?
        let cart = realm.objects(Order.self).filter("status = \(status)").first
        return cart
    }

    /*
     Retorna uma order ativo pelo id
     */
    
    func getOrderById(id : Int, idUnity: Int) -> Order?{
        let realm = try! Realm()
        
        //tem carrinho ativo?
        let cart = realm.objects(Order.self).filter("id = \(id) AND idUnity = \(idUnity)").first
        return cart
    }
    
    /**
     * remove item do carrinho ou diminui a quantidade
     */
    
    func getItemQuantity(item : UnityItem, unity : Unity) -> Int{

        var storedOrderItem : OrderItem?
        
        let realm = try! Realm()
        
        //tem carrinho ativo?
        let cart = realm.objects(Order.self)
            .filter("status = 0")
            .first
        
        if cart?.orderItems != nil {
            storedOrderItem = isOnCart(cart, item: item)//verifica se o item está no carrinho
        }
        
        //se storedOrderItem for != nil, encontra-se no carrinho
        if storedOrderItem != nil {
            return Int(storedOrderItem!.quantity)
        }
        
        return 0
    }

    /*
        atualiza o carrinho
     */
    func updateOrder(order : Order){
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Order.self, value: order, update: true)
        }
    }
    
    /*
        Incremente ultima key
     */
    
    func getNextKey(realm : Realm, aClass : Object.Type) -> Int{
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        }else{
            return 1
        }
    }
    
    /*
     atualiza status do order
     */

    func updateStatus(cart: Order, status : Int){
        let realm = try! Realm()
        
        realm.beginWrite()
        cart.status = status
        try! realm.commitWrite()
    }
    
    /**
     * retorna um objeto do tipo OrderItem. Caso seja null, o item não encontra-se no carrinho
     **/
    func isOnCart(order : Order?, item : UnityItem) -> OrderItem? {
        
        if order != nil{
            for orderItem in order!.orderItems {
                if orderItem.unityItem != nil {
                    if orderItem.unityItem!.id == item.id {
                        //achou item no carrinho
                        return orderItem
                    }
                }
            }
        }
    
        return nil
    }
    
    func hasOrderItemInCart(order : Order?, orderItem: OrderItem) -> OrderItem? {
        
        if order != nil{
            for orderItemInCart in order!.orderItems {
                if orderItemInCart.id == orderItem.id {
                    return orderItemInCart
                }
            }
        }
        
        return nil
    }
    
    func getOrders(filter : String = "status <> 0") -> NSArray {
        
        let realm = try! Realm()
        var arrOrders = [Order]()

        realm.refresh()

        let orders = realm.objects(Order.self)
            .filter(filter)
        arrOrders = orders.sorted("timestamp", ascending: false).get(0, limit: 10)
        
      /*  for order in orders {
            arrOrders.append(order)
        }*/
        
        return arrOrders
        
    }
    
    func getOrderUnity(idUnity : Int) -> Unity? {
        
        let realm = try! Realm()
        
        let unities = realm.objects(Unity.self)
        for item in unities {
            if item.id == idUnity {
                return item
            }
        }
        
        return nil
    }
    
    func importFromArray(content : NSArray) {
        
        let realm = try! Realm()
        try! realm.write {
            for item in content{
                realm.create(Order.self, value: item, update: true)
            }
        }
    }
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Order.self, value: content, update: true)
        }
    }
    
    func deleteCart() {
        let realm = try! Realm()
        if let cart = self.getCart(0) {
            try! realm.write {
                realm.delete(cart)
            }
        }
    }
    
    func somarItem(orderItrem: OrderItem)  throws -> Bool {
        
        let realm = try! Realm()
        realm.beginWrite()
        
        let verifyCart = hasActiveCart()
        let cart = verifyCart.cart
        
        orderItrem.quantity += 1
        
        var totalModifiers = 0.0
        if orderItrem.unityItem?.unityItemModifiers.count > 0 {
            for modifier in orderItrem.subItems {
                totalModifiers += modifier.total * orderItrem.quantity
            }
        }
        
        orderItrem.total = (orderItrem.unityItem!.price) * orderItrem.quantity + totalModifiers
        OrderEntityManager.sharedInstance.atualizaTotalDoCarrinho(cart!)
        
        realm.add(orderItrem, update: true)
        
        try! realm.commitWrite()
        
        return true
    }
}
