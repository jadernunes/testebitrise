//
//  ScreenConfig.swift
//  MarketPlace
//
//  Created by Luciano on 8/19/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ScreenConfig: Object {

    dynamic var id                  : Int   = 0
    dynamic var idSection           : Int   = 0
    dynamic var columns             : Int   = 0
    dynamic var height              : Int   = 0
    dynamic var width               : Int   = 0
    dynamic var seeMore             : Bool  = false
    dynamic var seeMoreLink         : String?
    dynamic var seeMoreTitle        : String?
    dynamic var allContent          : Bool  = false
    dynamic var idContentType       : Int   = 0
    dynamic var contentTypeName     : String?
    dynamic var colorBg             : String?
    dynamic var colorFont           : String?
    dynamic var parkingLocalization : Bool  = false
    dynamic var parkingPayment      : Bool  = false
    dynamic var elevation           : Int   = 0

    override static func primaryKey() -> String? {
        return "id"
    }
    
}
