//
//  UnityItemPersistence.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 09/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class UnityItemPersistence {
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int
    {
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        }else{
            return 1
        }
    }
    
    static func deleteAll(){
        let realm = try! Realm()
        var result: Results<UnityItem>
        result = realm.objects(UnityItem.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
}
