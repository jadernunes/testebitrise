//
//  NewsNotificationPersistence.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 22/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class NewsNotificationPersistence: Object {
    
    //MARK: - Incremente ultima key
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    static func save(data: NSDictionary) {
        
        let localNews = self.getNewsNotificationByID(data.objectForKey("id") as! Int)
        if localNews == nil {
            
            let realm = try! Realm()
            try! realm.write {
                realm.create(NewsNotification.self, value: data, update: true)
            }
        }
    }
    
    static func updateToRead(newsNotification:NewsNotification){
        let realm = try! Realm()
        try! realm.write {
            newsNotification.isRead = true
        }
    }
    
    static func getNewsNotificationByID(id:Int!) -> NewsNotification? {
        
        let realm = try! Realm()
        let newsAvaliable = realm.objects(NewsNotification.self).filter("id = \(id)").first
        
        return newsAvaliable
    }
    
    static func getLastNotificationAvaliable() -> NewsNotification? {
        
        let realm = try! Realm()
        let newsAvaliable = realm.objects(NewsNotification.self).filter("isRead = false").first
        
        return newsAvaliable
    }
}
