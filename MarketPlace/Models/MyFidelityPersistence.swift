//
//  MyFidelityPersistence.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class MyFidelityPersistence: Object {
    //MARK: - Incremente ultima key
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Save custom data
    private class func saveCustomData(content : NSDictionary) {
        let realm = try! Realm()
        let newContent = NSMutableDictionary(dictionary: content)
        
        let newID = content.objectForKey("id")
        let idUnity = content.objectForKey("idUnity")
        let maxStamp = content.objectForKey("maxStamp")
        let stampCount = content.objectForKey("stampCount")
        
        if newID != nil {
            if (newID!.isKindOfClass(NSNull) == false && idUnity!.isKindOfClass(NSNull) == false && maxStamp!.isKindOfClass(NSNull) == false && stampCount!.isKindOfClass(NSNull) == false) {
                try! realm.write {
                    
                    let oldDescription = content.objectForKey("description")
                    
                    var descriptionFidelity = ""
                    if(oldDescription != nil && oldDescription?.isKindOfClass(NSNull) == false) {
                        descriptionFidelity = oldDescription as! String
                    }
                    newContent.setObject(descriptionFidelity, forKey: "descriptionFidelity")
                    newContent.removeObjectForKey("description")
                    realm.create(MyFidelity.self, value: newContent, update: true)
                }
            }
        } else if (idUnity!.isKindOfClass(NSNull) == false && maxStamp!.isKindOfClass(NSNull) == false && stampCount!.isKindOfClass(NSNull) == false) {
            try! realm.write {
                
                let oldDescription = content.objectForKey("description")
                
                var descriptionFidelity = ""
                if(oldDescription != nil && oldDescription?.isKindOfClass(NSNull) == false) {
                    descriptionFidelity = oldDescription as! String
                }
                newContent.setObject(descriptionFidelity, forKey: "descriptionFidelity")
                newContent.removeObjectForKey("description")
                realm.create(MyFidelity.self, value: newContent, update: true)
            }
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func importFromDictionary(content : NSDictionary) {
        self.saveCustomData(content)
    }
    
    //MARK: - Import from Array
    
    static func importFromArray(content : NSArray) {
        
        for object in content {
            self.saveCustomData(object as! NSDictionary)
        }
    }
    
    //MARK: - Get all MyFidelity
    
    static func getAllLocalFidelitys() -> Results<MyFidelity> {
        
        let realm = try! Realm()
        var result: Results<MyFidelity>
        result = realm.objects(MyFidelity.self).filter(String(format: "stampCount > 0"))
        
        return result
    }
    
    //MARK: - Get MyFidelity by ID Unity
    
    static func getMyFidelityByIdUnity(idUnity : Int) -> MyFidelity?{
        
        let realm = try! Realm()
        let result = realm.objects(MyFidelity.self).filter(String(format: "idUnity = \(idUnity)")).first
        
        return result
    }
    
    //MARK: - delete all objects
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<MyFidelity>
        result = realm.objects(MyFidelity.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
}
