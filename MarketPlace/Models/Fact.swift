//
//  Fact.swift
//  MarketPlace
//
//  Created by Luciano on 6/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Fact: Object {

    dynamic var id              : CLong     = 0
    dynamic var factTypeId      : CLong     = 0
    dynamic var title           : String    = ""
    dynamic var desc            : String    = ""
    dynamic var subtitle        : String    = ""
    dynamic var timestampBegin  : Double    = 0
    dynamic var timestampEnd    : Double    = 0
    dynamic var thumb           : String?
    dynamic var qrRead          : Bool      = false
    dynamic var active          : Bool      = false
    dynamic var idUnity         : CLong     = 0
    dynamic var hasVoucher      : Bool      = false
    let batches                 = List<Batch>()
    let unities                 = List<Unity>()
    dynamic var season          : Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }

    func getFormatedDateString(beginDate : Bool, pattern : String) -> String{
        // convert to seconds
        let timeInMilliseconds = beginDate == true ? self.timestampBegin : self.timestampEnd
        let timeInSeconds = Double(timeInMilliseconds) / 1000
        
        // get the NSDate
        let dateTime    = NSDate(timeIntervalSince1970: timeInSeconds)
        let dateFormatter  = NSDateFormatter()
        
        dateFormatter.dateFormat = pattern
        dateFormatter.locale = NSLocale.currentLocale()
        
        return dateFormatter.stringFromDate(dateTime)
    }
}
