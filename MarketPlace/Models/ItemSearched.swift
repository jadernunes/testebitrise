//
//  ItemSearched.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/13/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class ItemSearched: NSObject {
    
    var id         : Int!
    var logo       : String?
    var subType    : Int?
    var title      : String?
    var type       : String?
}