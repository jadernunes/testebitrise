//
//  Address.swift
//  MarketPlace
//
//  Created by Luciano on 8/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Address: Object {

    dynamic var id         : CLong = 0
    dynamic var name         : String?
    dynamic var zip          : String?
    dynamic var street       : String?
    dynamic var number       : String?
    dynamic var complement   : String?
    dynamic var neighborhood : String?
    dynamic var city         : String?
    dynamic var state        : String?
    dynamic var selected     : Bool = false
    dynamic var latitude     : Double = 0.0
    dynamic var longitude    : Double = 0.0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
