//
//  Schedule.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Schedule: Object {
    
    dynamic var id              :CLong = 0
    dynamic var idUnity         :String?
    dynamic var unityName       :String?
    dynamic var unityItemTitle  :String?
    dynamic var timeBegin       :String?
    dynamic var timeEnd         :String?
    dynamic var dateString      :String?
    dynamic var idStatus        :StatusSchedule.RawValue = StatusSchedule.Sent.rawValue
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
