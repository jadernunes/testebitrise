//
//  Floor.swift
//  MarketPlace
//
//  Created by Luciano on 8/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Floor: Object {
    dynamic var id          : CLong  = 0
    dynamic var name        : String!
    dynamic var desc        : String?
    dynamic var idBuilding  : CLong  = 0
    let parkingRegions      = List<Region>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
