//
//  Unity.swift
//  MarketPlace
//
//  Created by Luciano on 6/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Unity: Object {
    
    dynamic var id:                         Int = 0
    dynamic var name:                       String?
    dynamic var desc:                       String?
    dynamic var address:                    String?
    dynamic var phoneNumber:                String?
    dynamic var siteUrl:                    String?
    dynamic var facebookUrl:                String?
    dynamic var twitterUrl:                 String?
    dynamic var instagramUrl:               String?
    dynamic var orderEnabled:               Bool  = false // 1
    dynamic var opened:                     Bool  = false
    dynamic var idUnityType:                Int   = 0
    dynamic var unityTypeName:              String?
    dynamic var favorite:                   Bool  = false
    dynamic var merchantKey:                String?
    dynamic var authKeyAll4Mobile:          String?
    dynamic var idStoreAll4Mobile:          String?
    dynamic var latitude:                   Double = 0.0
    dynamic var longitude:                  Double = 0.0
    dynamic var schedulingEnabled:          Bool = false // 3
    dynamic var immediateScheduling:        Bool = false
    dynamic var indoorMapLuc:               Luc?
    dynamic var active:                     Bool = false
    dynamic var fidelity:                   UnityFidelity?
    dynamic var orderDeliveryEnabled:       Bool = false
    dynamic var orderShortDeliveryEnabled:  Bool = false
    dynamic var orderTakeAwayEnabled:       Bool = false
    dynamic var orderCheckEnabled:          Bool = false // 2
    dynamic var fourPayEnabled:             Bool = false
    dynamic var soon:                       Bool = false
    dynamic var idParent:                   Int   = 0
    dynamic var orderVoucherEnabled:        Bool = false
    dynamic var distanceFromUser            : Double = 0.0
    dynamic var distance                    : Double = 0.0
    dynamic var deliveryEstimatedTime       : Double = 0.0
    dynamic var takeAwayEstimatedTime       : Double = 0.0
    dynamic var zip                         : String? = ""
    
    let media:                              List<Media>     = List<Media>()
    let children:                           List<Unity>     = List<Unity>()
    let shifts:                             List<Shifts>    = List<Shifts>()
    let facts:                              List<Fact>      = List<Fact>()
    
    override static func primaryKey() -> String? {
        return "id"
    }

    func getThumb() -> Media?{
        for item in self.media{
            if item.mediaContextName == "thumb"{
                return item
            }
        }	
        return nil
    }
    
    func getLogo() -> Media?{
        for item in self.media{
            if item.mediaContextName == "logo"{
                return item
            }
        }
        return nil
    }
    
    func getHomeBanner() -> Media?{
        for item in self.media{
            if item.mediaContextName == "homebanner"{
                return item
            }
        }
        return nil
    }
    
    func getChildren() -> [Unity] {
        return self.children.filter({ (unity: Unity) -> Bool in
            return true
        })
    }
    
    func getMedia() -> [Media] {
        return self.media.filter({ (media: Media) -> Bool in
            return true
        })
    }
    
    func getShifts() -> [Shifts] {
        return self.shifts.filter({ (shift: Shifts) -> Bool in
            return true
        })
    }
    
    func md5(object: Any) -> String {
        
        
        
        return ""
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        guard let unity = object as? Unity else {
            return false
        }
        
        let instanceVarComparison =
                id == unity.id && name == unity.name! && desc! == unity.desc! &&
                address == unity.address &&
                phoneNumber  == unity.phoneNumber &&
                siteUrl == unity.siteUrl &&
                facebookUrl == unity.facebookUrl &&
                twitterUrl == unity.twitterUrl &&
                instagramUrl == unity.instagramUrl &&
                orderEnabled == unity.orderEnabled &&
                opened == unity.opened &&
                idUnityType == unity.idUnityType &&
                unityTypeName == unity.unityTypeName &&
                favorite == unity.favorite &&
                merchantKey == unity.merchantKey &&
                authKeyAll4Mobile == unity.authKeyAll4Mobile &&
                idStoreAll4Mobile == unity.idStoreAll4Mobile &&
                latitude == unity.latitude &&
                longitude == unity.longitude &&
                schedulingEnabled == unity.schedulingEnabled &&
                immediateScheduling == unity.immediateScheduling &&
                indoorMapLuc == unity.indoorMapLuc &&
                active == unity.active &&
                fidelity == unity.fidelity &&
                orderDeliveryEnabled == unity.orderDeliveryEnabled &&
                orderShortDeliveryEnabled == unity.orderShortDeliveryEnabled &&
                orderTakeAwayEnabled == unity.orderTakeAwayEnabled &&
                orderCheckEnabled == unity.orderCheckEnabled &&
                fourPayEnabled == unity.fourPayEnabled &&
                soon == unity.soon &&
                idParent == unity.idParent &&
                orderVoucherEnabled == unity.orderVoucherEnabled &&
                distanceFromUser == unity.distanceFromUser &&
                distance == unity.distance &&
                deliveryEstimatedTime == unity.deliveryEstimatedTime &&
                takeAwayEstimatedTime == unity.takeAwayEstimatedTime &&
                zip == unity.zip
        
        var childrenComparison = true
        if getChildren().count > 0 && unity.getChildren().count > 0 {
            childrenComparison = getChildren().reduce(true) { $0 && unity.getChildren().contains($1) } && unity.getChildren().reduce(true) { $0 && getChildren().contains($1) }
        }
        
        var mediaComparison = true
        if getMedia().count > 0 && unity.getMedia().count > 0 {
            mediaComparison = getMedia().reduce(true) { $0 && unity.getMedia().contains($1) } && unity.getMedia().reduce(true) { $0 && getMedia().contains($1) }
        } else if unity.getMedia().count > 0 {
            mediaComparison = false
        }
        
        var shiftsComparison = true
        if getShifts().count > 0 && unity.getShifts().count > 0 {
            shiftsComparison = getShifts().reduce(true) { $0 && unity.getShifts().contains($1) } && unity.getShifts().reduce(true) { $0 && getShifts().contains($1) }
        } else if unity.getShifts().count > 0 {
            shiftsComparison = false
        }
        
        return instanceVarComparison && childrenComparison && mediaComparison && shiftsComparison
    }
}
