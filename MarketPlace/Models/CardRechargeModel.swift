//
//  CardRechargeModel.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 08/05/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CardRechargeModel : Object {
    
    dynamic var issuerId :Int = 0
    dynamic var productId :Int = 0
    dynamic var productName :String = ""
    dynamic var issuerNr :String = ""
    dynamic var valuePaid :Double = 0.0
    dynamic var issuerThumb :String = ""
    dynamic var date :NSDate = NSDate()
    
    override static func primaryKey() -> String? {
        return "issuerNr"
    }
}
