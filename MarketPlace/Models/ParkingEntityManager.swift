//
//  ParkingEntityManager.swift
//  MarketPlace
//
//  Created by Luciano on 8/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ParkingEntityManager: Object {

    static func importFromArray(content : NSArray) -> String {
        
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            let result = realm.objects(Building.self)
            realm.delete(result)
            for item in content{
                realm.create(Building.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        return listIdsReceived
    }
    
    static func getBuildings() -> [Building]{
        let realm = try! Realm()
        var listBuildings = [Building]()
        try! realm.write {
            let result = realm.objects(Building)
            for item in result {
                listBuildings.append(item)
            }
            
        }
        
        return listBuildings
    }
    
    static func getFloors(buildingId : Int) -> [Floor]{
        
        let realm = try! Realm()
        var listFloors = [Floor]()
        try! realm.write {
            let result = realm.objects(Floor).filter(String("idBuilding = \(buildingId)"))
            
            for item in result {
                listFloors.append(item)
            }
        }
        
        return listFloors
    }
    

    static func getRegions(floorId : Int) -> [Region]{
        
        let realm = try! Realm()
        var listFloors = [Region]()
        try! realm.write {
            let result = realm.objects(Region).filter(String("idFloor = \(floorId)"))
            
            for item in result {
                listFloors.append(item)
            }
        }
        
        return listFloors
    }
    
    static func getSpaces(regionId : Int) -> [Space]{
        
        let realm = try! Realm()
        var listFloors = [Space]()
        try! realm.write {
            let result = realm.objects(Space).filter(String("idRegion = \(regionId)"))
            
            for item in result {
                listFloors.append(item)
            }
        }
        
        return listFloors
    }
}
