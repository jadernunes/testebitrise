//
//  CupomPersistence.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class CampaignPersitence {
    
    /*
     Incremente ultima key
     */
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Campaign.self, value: content, update: true)
        }
    }
    
    //MARK: - Save objects from Array
    
    static func importFromArray(content : NSArray) -> String {
        
        var listIdsReceived = ""
        
        if content.count > 0 {
            let realm = try! Realm()
            listIdsReceived = String((content.firstObject?.objectForKey("id"))!)
            
            try! realm.write {
                for item in content{
                    realm.create(Campaign.self, value: item, update: true)
                    
                    if listIdsReceived.characters.count > 0 {
                        listIdsReceived += "," + String(item.objectForKey("id")!)
                    }
                }
            }
        }
    
        return listIdsReceived
    }
    
    //MARK: - Get all schedules
    
    static func getAllLocalCampaigns() -> Results<Campaign> {
        
        let realm = try! Realm()
        var result: Results<Campaign>
        result = realm.objects(Campaign.self)
        
        return result
    }
    
    //MARK: - Get Campaign by ID
    
    static func getCampaignById(idCampaign : Int) -> Campaign?{
        
        let realm = try! Realm()
        
        var result : Campaign?
        try! realm.write {
            result = realm.objects(Campaign.self).filter(String(format: "id = \(idCampaign)")).first
        }
        
        return result
    }
    
    //MARK: - Get Campaign by UUID
    
    static func getCampaignByUUID(uuid : String) -> Campaign?{
        
        let realm = try! Realm()
        let result = realm.objects(Campaign.self).filter(String(format: "uuid = '\(uuid)'")).first
        return result
    }
    
    static func getCampaingsByActiveUnities() -> Results<Campaign> {
        
        let realm = try! Realm()
        var listIdsUnity = ""
        
        if let listUnities: Results<Unity> = realm.objects(Unity.self).filter("active = 1") {
            if listUnities.count > 0 {
                for i in 0...listUnities.count-1 {
                    let unity = listUnities[i]
                    if i == 0 {
                        listIdsUnity = "\(unity.id)"
                    } else {
                        listIdsUnity += "," + "\(unity.id)"
                    }
                }
            }
        }
        
        let result = realm.objects(Campaign.self).filter("idUnity IN {\(listIdsUnity)}")
        return result
    }
    
    static func getCampaingsByActiveUnitiesWithIDs(listIds: String) -> Results<Campaign> {
        
        let realm = try! Realm()
        var listIdsUnity = ""
        
        if let listUnities: Results<Unity> = realm.objects(Unity.self).filter("active = 1") {
            if listUnities.count > 0 {
                for i in 0...listUnities.count-1 {
                    
                    let unity = listUnities[i]
                    if i == 0 {
                        listIdsUnity = "\(unity.id)"
                    } else {
                        listIdsUnity += "," + "\(unity.id)"
                    }
                }
            }
        }
        
        let result = realm.objects(Campaign.self).filter("idUnity IN {\(listIdsUnity)} AND id IN {\(listIds)}")
        return result
    }
}
