//
//  Group.swift
//  MarketPlace
//
//  Created by Luciano on 6/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Group: Object {

    dynamic var id                  : CLong = 0
    dynamic var idGroupTreeType     : CLong = 0
    dynamic var groupTreeTypeName   : String? = ""
    dynamic var name                : String = ""
    dynamic var thumb               : String?
    var parentId                    = RealmOptional<CLong>(0)
    var children                    = List<Group>()
    var unities                     = List<Unity>()

    override static func primaryKey() -> String? {
        return "id"
    }
    
    func loadDataFromRealm() -> Void {
        let result = GroupEntityManager.getGroupById(self.id)
        
        if result != nil {
            let realm = try! Realm()
            
            do{
                try realm.write {
                    self.groupTreeTypeName = result?.groupTreeTypeName
                    if result?.name != nil {
                        self.name              = (result?.name)!
                    } else {
                        self.name = ""
                    }
                    self.thumb             = result?.thumb
                    self.parentId          = (result?.parentId)!
                    self.children          = (result?.children)!
                    self.unities           = (result?.unities)!
                }
            }catch {
            }
        }
    }
}
