//
//  OrderStatus.swift
//  MarketPlace
//
//  Created by Luciano on 8/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class OrderStatus: Object {

    dynamic var id           : CLong = 0
    dynamic var status       : CLong = 0
    dynamic var dthr         : CLong = 0
    dynamic var observation  : String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
