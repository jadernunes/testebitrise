//
//  CellMenu.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class CellMenu: NSObject {

    var titleCell: String?
    var enumTypeCell: NSDictionary?
    
    
    init(titleCell: String, enumTypeCell: NSDictionary) {
        self.titleCell = titleCell
        self.enumTypeCell = enumTypeCell
    }
}
