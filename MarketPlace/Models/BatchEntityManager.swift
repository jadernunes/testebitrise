//
//  BatchEntityManager.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 13/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import RealmSwift

class BatchEntityManager : Object {
    
    typealias BatchesCompletion = (Results<Batch>) -> ()
    typealias BatcheCompletion = (Batch?) -> ()
    
    class func getBatchesBy(factId id: CLong, completion: BatchesCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Batch.self).filter("idFact = \(id)"))
        } else {
            return
        }
    }
    
    class func getBatchBy(itsId id: CLong, completion: BatcheCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Batch.self).filter("id = \(id)").first)
        } else {
            return
        }
    }
    
    class func getAllBatches(completion: BatchesCompletion) {
        if let realm = try? Realm() {
            completion(realm.objects(Batch.self))
        } else {
            return
        }
    }
}
