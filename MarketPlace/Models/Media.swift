//
//  Media.swift
//  MarketPlace
//
//  Created by Luciano on 7/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Media: Object {
    dynamic var id               : CLong = 0
    dynamic var value            : String?
    dynamic var idMediaType      : Int = 0
    dynamic var mediaTypeName    : String?
    dynamic var idMediaContext   : Int = 0
    dynamic var mediaContextName : String?
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        guard let media = object as? Media else {
            return false
        }
        
        let instanceVarComparison =
            id == media.id &&
            value == media.value &&
            idMediaType == media.idMediaType &&
            mediaTypeName == media.mediaTypeName &&
            idMediaContext == media.idMediaContext &&
            mediaContextName == media.mediaContextName
        
        
        return instanceVarComparison
    }
}
