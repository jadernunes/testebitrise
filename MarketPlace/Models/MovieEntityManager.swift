//
//  MovieEntityManager.swift
//  MarketPlace
//
//  Created by Luciano on 7/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class MovieEntityManager: NSObject {
    static func importFromArray(content : NSArray) -> String {
        
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            let result = realm.objects(Movie.self)
            realm.delete(result)
            for item in content{
                realm.create(Movie.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        return listIdsReceived
    }
    
    static func getMovies(listAll : Bool = false) -> NSMutableArray{
        let realm = try! Realm()
        let listGroups = NSMutableArray()
        try! realm.write {
            let result = realm.objects(Movie.self)
            for item in result {
                if listAll == true {
                    listGroups.addObject(item)
                }else if item.getAvailableSessions().count > 0{
                    listGroups.addObject(item)
                }
            }
        }
        
        return listGroups
    }

    static func getMovieByID(idMovie : String) -> Movie? {
        
        let realm = try! Realm()
        let result: Movie?
        result = realm.objects(Movie.self).filter("id = \(idMovie)").first
        
        return result
    }
    
    //MARK: - delete all objects
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Movie>
        result = realm.objects(Movie.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
}
