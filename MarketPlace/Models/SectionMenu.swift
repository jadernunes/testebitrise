//
//  SectionMenu.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

class SectionMenu: NSObject {
    var titleSection: String?
    var listItems: Array<NSDictionary>
    
    init(titleSection: String, listItems: Array<NSDictionary>) {
        self.titleSection = titleSection
        self.listItems = listItems
    }
}
