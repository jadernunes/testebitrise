//
//  Push.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Push: Object {

    dynamic var type :           PushesType.RawValue = 0
    dynamic var title :          String?
    dynamic var id :             Int = 0
    dynamic var body :           String?
    dynamic var firstCustomField : String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
