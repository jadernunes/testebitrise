//
//  Cupom.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Campaign: Object {
    
    dynamic var id                      :CLong  = 0
    dynamic var idUnity                 :CLong  = 0
    dynamic var timestamp               :Double = 0
    dynamic var dateBeginClaim          :String!
    dynamic var dateEndClaim            :String!
    dynamic var dateBeginRedeem         :String!
    dynamic var dateEndRedeem           :String!
    dynamic var shortDesc               :String!
    dynamic var longDesc                :String!
    dynamic var terms                   :String!
    dynamic var couponLimit             :Int = 0
    dynamic var couponClaimed           :Int = 0
    dynamic var idCampaignDiscountType  :TypeDiscountCampaign.RawValue = TypeDiscountCampaign.price.rawValue
    dynamic var discount                :Int = 0
    dynamic var originalPrice           :Int = 0
    dynamic var uuid                    :String!
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
