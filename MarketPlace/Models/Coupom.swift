//
//  Coupom.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class Coupom: Object {
    
    dynamic var id          :CLong  = 0
    dynamic var idCampaign  :CLong  = 0
    dynamic var timestamp   :Double = 0
    dynamic var redeemed    :Bool   = false
    dynamic var uuid        :String!
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
