//
//  TermsFidelityViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 17/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TermsFidelityViewController: UIViewController {

    var descriptionTerms: String!
    @IBOutlet weak var textViewTerms: UITextView!
    @IBOutlet weak var navigationBarCustom: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Util.adjustTableViewInsetsFromTarget(self)

        self.navigationBarCustom.barTintColor = SessionManager.sharedInstance.cardColor
        self.navigationBarCustom.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.textViewTerms.text = descriptionTerms
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @IBAction func closeWindown(sender: AnyObject) {
        self.formSheetController?.dismissAnimated(true, completionHandler: { (viewController: UIViewController!) in
        })
    }
}
