//
//  Batch.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 24/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Batch: Object {
    
    dynamic var id:                 CLong = 0
    dynamic var active:             Bool = false
    dynamic var desc:               String!
    dynamic var idFact:             Int = 0
    dynamic var name:               String!
    dynamic var sellingDateEnd:     String!
    dynamic var sellingDateStart:   String!
    
    /**
     Relationships cannot be assigned as dynamic
     that is why we are using let in this case
     To see more: https://realm.io/docs/swift/latest/#to-many-relationships
     */
    let eventSections                  = List<EventSection>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
