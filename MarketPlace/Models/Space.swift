//
//  Space.swift
//  MarketPlace
//
//  Created by Luciano on 8/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Space: Object {
    
    dynamic var id              : CLong  = 0
    dynamic var name            : String!
    dynamic var desc            : String?
    dynamic var idRegion        : CLong  = 0
    dynamic var idMarketplace   : CLong  = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
