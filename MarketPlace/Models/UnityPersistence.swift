//
//  UnityPersistence.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import CoreLocation
import RealmSwift

class UnityPersistence {

    static func getUnityByID(idUnity : String) -> Unity? {
        
        let realm = try! Realm()
        
        var result: Unity? = nil
        let query = "id = \(idUnity)"
        
        try! realm.write {
            result = realm.objects(Unity.self).filter(query).first
        }
        
        return result
    }
    
    static func importFromDictionary(content : NSDictionary) {
        do {
            let realm = try Realm()
            if realm.inWriteTransaction == false {
                try realm.write({
                    realm.create(Unity.self, value: content, update: true)
                })
            }
        } catch {
            print("########### Error when save in realm")
        }
    }

    static func importFromArray(content : NSArray) -> String {
                
        let realm = try! Realm()
        var listIdsReceived: String = String((content.firstObject?.objectForKey("id"))!)
        
        try! realm.write {
            for item in content{
                realm.create(Unity.self, value: item, update: true)
                
                if listIdsReceived.characters.count > 0 {
                    listIdsReceived += "," + String(item.objectForKey("id")!)
                }
            }
        }
        
        return listIdsReceived
    }
    
    static func getAllUnities() -> Results<Unity> {
        
        let realm = try! Realm()
        realm.refresh()
        
        let unities = realm.objects(Unity.self).filter("idParent = 0")
        
        return unities
    }
    
    static func getUnityByName(name: String) -> Unity? {
        let realm = try! Realm()
        realm.refresh()
        let unity = realm.objects(Unity.self).filter("name = '\(name)'").first
        return unity
    }

    /// Gastronomia: Rota para ECs perto da rota
    ///
    /// - Parameter cep: cep que o usuário informa no cadastro de endereco
    /// - Returns: a lista de estabelecimentos encontrada
    static func getUnitiesByCep(cep: String) -> Results<Unity> {
        let realm = try! Realm()
        realm.refresh()
        let unities = realm.objects(Unity.self).filter("zip = '\(cep)'")
        return unities
    }
    
    static func updateLocationFromUser(listUnity:[Unity],location:CLLocation!) {
        let realm = try! Realm()
        try! realm.write {
            for unity in listUnity {
                let locationUnity = CLLocation(latitude: unity.latitude, longitude: unity.longitude)
                unity.distanceFromUser = locationUnity.distanceFromLocation(location)
            }
        }
    }

    static func getActiveUnities() -> Results<Unity> {
        
        let realm = try! Realm()
        realm.refresh()
        
        let unities = realm.objects(Unity.self).filter("active = true")
        return unities
    }
    
    func getFactsByIdUnity(idUntiy:Int) -> Results<Fact> {
        
        let realm = try! Realm()
        let listFacts = realm.objects(Fact.self).filter("idUnity = \(idUntiy)")
        return listFacts
    }
    
    static func getUnitiesByListIds(listIds:String, tipoPedido: Int?) -> [Unity] {
        
        let realm = try! Realm()
        var query = ""
        if let tipo = tipoPedido {
            if tipo > 0 {
                switch tipo {
                case 1:
                    query = "AND orderDeliveryEnabled = 1"
                    break
                case 2:
                    query = "AND (orderShortDeliveryEnabled = 1 OR orderTakeAwayEnabled = 1)"
                    break
                case 3:
                    query = "AND orderCheckEnabled = 1"
                    break
                default:
                    break
                }
            }
        }
        
        var listUnities = [Unity]()
        listUnities.appendContentsOf(realm.objects(Unity.self).filter("id IN {\(listIds)} \(query)"))
        
        if let location = LocationManager.shared.location {
            
            listUnities = listUnities.sort { CLLocation(latitude: $0.0.latitude, longitude: $0.0.longitude).distanceFromLocation(location) < CLLocation(latitude: $0.1.latitude, longitude: $0.1.longitude).distanceFromLocation(location)
            }
        }
        
        return listUnities
    }
    
    static func getUnitiesBannerByListIds(listIds:String) -> [Unity] {
        
        let realm = try! Realm()
        let query = " AND (orderCheckEnabled = 1 OR orderShortDeliveryEnabled = 1 OR orderTakeAwayEnabled = 1 OR orderCheckEnabled = 1 OR fourPayEnabled = 1)"
        var listUnities = [Unity]()
        listUnities.appendContentsOf(realm.objects(Unity.self).filter("id IN {\(listIds)} \(query)"))
        
        if let location = LocationManager.shared.location {
            
            listUnities = listUnities.sort { CLLocation(latitude: $0.0.latitude, longitude: $0.0.longitude).distanceFromLocation(location) < CLLocation(latitude: $0.1.latitude, longitude: $0.1.longitude).distanceFromLocation(location)
            }
        }
        
        return listUnities
	}
	
    static func getUnitiesByOrderType(orderType:String) -> Results<Unity> {
        let realm = try! Realm()
        realm.refresh()
        
        let unities = realm.objects(Unity.self).filter("\(orderType) = true")
        return unities
    }
    
    static func manipulateAnyUnities(listUnities: [[String:AnyObject]], tipoPedido: Int? = 0) -> [Unity]{
        var listIDs = ""
        if listUnities.count > 0 {
            for index in 0..<listUnities.count-1 {
                
                let unity = Unity(value: listUnities[index])
                UnityPersistence.importFromDictionary(listUnities[index])
                
                if index == 0 {
                    listIDs = String(unity.id)
                } else {
                    listIDs += "," + String(unity.id)
                }
            }
        }
        
        let listUnities = UnityPersistence.getUnitiesByListIds(listIDs, tipoPedido: tipoPedido)
        return listUnities
    }
    
    static func manipulateUnities(listUnities: [[String:AnyObject]]) -> [Unity]{
        var listIDs = ""
        if listUnities.count > 0 {
            for index in 0..<listUnities.count-1 {
                
                let unity = Unity(value: listUnities[index])
                
                if index == 0 {
                    listIDs = String(unity.id)
                } else {
                    listIDs += "," + String(unity.id)
                }
            }
        }
        
        let listUnities = UnityPersistence.getUnitiesBannerByListIds(listIDs)
        return listUnities
    }
}
