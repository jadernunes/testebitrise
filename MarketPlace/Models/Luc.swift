//
//  Luc.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/26/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class Luc: Object {
    
    dynamic var id                      : Int  = 0
    dynamic var luc                     : String = ""
    dynamic var pointX                  : Double = 0.0
    dynamic var pointY                  : Double = 0.0
    dynamic var idIndoorMapLevel        : Int = 0
    dynamic var idMarketPlace           : Int = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
