//
//  Locations.swift
//  Geotify
//
//  Created by Matheus Luz on 8/18/16.
//  Copyright © 2016 Ken Toh. All rights reserved.
//

import Foundation
class Locations: NSObject {
  var latitude: Double = 0.0
  var longitude: Double = 0.0
  
  init(lat: Double,long: Double) {
    self.latitude = lat
    self.longitude = long
  }
  
}