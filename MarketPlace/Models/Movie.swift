//
//  Movie.swift
//  MarketPlace
//
//  Created by Luciano on 7/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift
class Movie: Object {
    
    dynamic var id                  : CLong  = 0
    dynamic var title               : String = ""
    dynamic var desc                : String = ""
    dynamic var sinopsis            : String?
    dynamic var youtube             : String?
    dynamic var duration            : String?
    dynamic var thumb               : String?
    dynamic var director            : String?
    dynamic var ageClassification   : Int    = 0
    dynamic var cast                : String?
    dynamic var active              : Bool   = false
    let movieSessions               = List<MovieSession>()
    let media                       = List<Media>()

    override static func primaryKey() -> String?{
        return "id"
    }
    
    func getAvailableSessions(forDate : NSDate = NSDate()) -> NSMutableArray{
        
       
        let arrSessions = NSMutableArray()
        
        let dateFormatter  = NSDateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        let selectedDate     = dateFormatter.stringFromDate(forDate)
        dateFormatter.dateFormat = "HH"
        let selectedHour     = Int(dateFormatter.stringFromDate(forDate))
        dateFormatter.dateFormat = "mm"
        let selectedMin      = Int(dateFormatter.stringFromDate(forDate))


        
        // get the NSDate
        
        for session in movieSessions {
            if session.sessionDatetime.value != nil {
            let dateTime = NSDate(timeIntervalSince1970: Double(session.sessionDatetime.value! / 1000))
                dateFormatter.dateFormat = "HH"
                let sessionHour     = Int(dateFormatter.stringFromDate(dateTime))
                dateFormatter.dateFormat = "mm"
                let sessionMin      = Int(dateFormatter.stringFromDate(dateTime))
                
                dateFormatter.dateFormat = "ddMMyyyy"
                let sessionDate     = dateFormatter.stringFromDate(dateTime)
                
                if sessionDate == selectedDate{
                
                    if sessionHour > selectedHour {
                        arrSessions.addObject(session)
                    }else if sessionHour == selectedHour && sessionMin >= selectedMin {
                        arrSessions.addObject(session)
                    }
                }
            }
        }
        
        return arrSessions
    }
}
