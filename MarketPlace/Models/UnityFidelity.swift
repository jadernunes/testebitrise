//
//  UnityFidelity.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class UnityFidelity: Object {
    
    dynamic var id              :CLong  = 0
    dynamic var idUnity         :Int = 0
    dynamic var intervalStamp   :Int = 0
    dynamic var totalStamp      :Int = 0
    dynamic var active          :Bool = false
    dynamic var desc            :String?
    dynamic var terms           :String?
    dynamic var thumb           :String?
    dynamic var winningDesc     :String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
