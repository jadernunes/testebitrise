//
//  MovieSession.swift
//  MarketPlace
//
//  Created by Luciano on 7/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class MovieSession: Object {
    
    dynamic var id                  : CLong = 0
    dynamic var movieId             : CLong = 0
    var sessionDatetime             = RealmOptional<Double>(0.0)
    dynamic var subtitled           = false
    dynamic var room                : String?
    let availableTickets            = RealmOptional<Int>(0)
    dynamic var language            : String?
    let price                       = RealmOptional<Double>(0.0)
    let media                       = List<Media>()
    
    override static func primaryKey() -> String?{
        return "id"
    }

    func getFormatedDateString(pattern : String) -> String{
        // convert to seconds
        let timeInMilliseconds = sessionDatetime.value
        let timeInSeconds = Double(timeInMilliseconds!) / 1000
        
        // get the NSDate
        let dateTime    = NSDate(timeIntervalSince1970: timeInSeconds)
        let dateFormatter  = NSDateFormatter()
        
        dateFormatter.dateFormat = pattern
        
        return dateFormatter.stringFromDate(dateTime)
    }

}
