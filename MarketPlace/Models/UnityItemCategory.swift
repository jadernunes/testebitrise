//
//  UnityItemCategory.swift
//  MarketPlace
//
//  Created by Luciano on 6/28/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class UnityItemCategory: Object {
    
    dynamic var id      : CLong  = 0
    dynamic var unityId : CLong  = 0
    dynamic var type    : Int    = 0
    dynamic var title   : String = ""
    dynamic var thumb   : String = ""
    dynamic var color   : String = ""
    let items           = List<UnityItem>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
