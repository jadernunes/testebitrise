//
//  UnityFidelityPersistence.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class UnityFidelityPersistence: Object {
    //MARK: - Incremente ultima key
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Dictionary
    
    static func importFromDictionary(content : NSDictionary) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(UnityFidelity.self, value: content, update: true)
        }
    }
    
    //MARK: - Import from Array
    
    static func importFromArray(content : NSArray) {
        let realm = try! Realm()
        
        try! realm.write {
            for object in content {
                realm.create(UnityFidelity.self, value: object, update: true)
            }
        }
    }
    
    //MARK: - Get all UnityFidelity
    
    static func getAllLocalFidelitys() -> Results<UnityFidelity> {
        
        let realm = try! Realm()
        var result: Results<UnityFidelity>
        result = realm.objects(UnityFidelity.self)
        
        return result
    }
    
    //MARK: - Get UnityFidelity by ID Unity
    
    static func getUnityFidelityByIdUnity(idUnity : Int) -> UnityFidelity?{
        
        let realm = try! Realm()
        let result = realm.objects(UnityFidelity.self).filter(String(format: "idUnity = \(idUnity)")).first
        
        return result
    }
}
