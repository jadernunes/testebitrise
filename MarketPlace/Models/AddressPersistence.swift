//
//  AddressPersistence.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/27/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class AddressPersistence {
    
    //MARK: - Incremente ultima key
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int {
        
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        } else {
            return 1
        }
    }
    
    //MARK: - Import from Array
    
    static func importFromArray(content : NSArray) {
        let realm = try! Realm()
        
        try! realm.write {
            for object in content {
                realm.create(Address.self, value: object, update: true)
            }
        }
    }

    static func getAddressById(idAddress : Int) -> Address?{
        
        let realm = try! Realm()
        let result = realm.objects(Address.self).filter(String(format: "id = \(idAddress)")).first
        
        return result
    }
    
    
    /// Validate user saved address
    ///
    /// - Returns: returns a Address, if it exists
    static func getAddressBySelected() -> Address?{
        
        let realm = try! Realm()
        let result = realm.objects(Address.self).filter(String(format: "selected = \(true)")).first
        
        return result
    }
    
    //MARK: - Get all Addresses
    
    static func getAllLocalAddresses() -> Results<Address> {
        
        let realm = try! Realm()
        var result: Results<Address>
        result = realm.objects(Address.self)
        
        return result
    }
    
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Address>
        result = realm.objects(Address.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }

}
