//
//  SchedulePersistence.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation

import RealmSwift

class SchedulePersitence {

    /*
     Incremente ultima key
     */
    
    static func getNextKey(realm : Realm, aClass : Object.Type) -> Int
    {
        if realm.objects(aClass.self).count != 0 {
            return (realm.objects(aClass).max("id")! + 1)
        }else{
            return 1
        }
    }
    
    static func getAllLocalSchedules() -> Results<Schedule>
    {
        let realm = try! Realm()
        var result: Results<Schedule>
        result = realm.objects(Schedule.self).sorted("dateString", ascending: false)
        
        return result
    }
    
    static func saveData(dataObject:NSDictionary)
    {
        let realm = try! Realm()
        
        try! realm.write {
            realm.create(Schedule.self, value: dataObject, update: true)
        }
    }
    
    static func saveManySchedule(listDataSchedule: NSArray)
    {
        let realm = try! Realm()
        
        try! realm.write {
            for schedule in listDataSchedule
            {
                let unityItem = schedule.objectForKey("unityItem")
                let unity = schedule.objectForKey("unity")
                
                let idSchedule = schedule.objectForKey("id") as! Int
                let unityName  = unity?.objectForKey("desc") as! String
                let serviceName = unityItem?.objectForKey("title") as! String
                let timeBegin  = schedule.objectForKey("timeBegin") as! String
                let timeEnd  = schedule.objectForKey("timeEnd") as! String
                let dateStr = schedule.objectForKey("date") as! String
                let idUnity = String((unity?.objectForKey("id"))!)
                let idStatus = schedule.objectForKey("idStatus") as! Int
                
                let dicSave = ["id":idSchedule,
                    "idUnity":idUnity,
                    "unityName":unityName,
                    "unityItemTitle":serviceName,
                    "timeBegin":timeBegin,
                    "timeEnd":timeEnd,
                    "dateString":dateStr,
                    "idStatus":idStatus] as NSDictionary!
                
                realm.create(Schedule.self, value: dicSave, update: true)
            }
        }
    }
    
    static func deleteAllObjects()
    {
        let realm = try! Realm()
        var result: Results<Schedule>
        result = realm.objects(Schedule.self)
        
        try! realm.write {
            realm.delete(result)
        }
    }
    
    static func getScheduleWithStatus(filter : String = "status <> 0") -> [Schedule] {
        
        let realm = try! Realm()
        var listSchedule = [Schedule]()
        
        realm.refresh()
        
        let schedulesResult = realm.objects(Schedule.self)
            .filter(filter)
        
        for schedule in schedulesResult {
            listSchedule.append(schedule)
        }
        
        return listSchedule
        
    }
    
    static func getScheduleByID(idSchedule: Int) -> Schedule!
    {
        let realm = try! Realm()
        let query = "id = \(idSchedule)"
        
        let result = realm.objects(Schedule.self).filter(query).first
        
        return result
    }
}
