//
//  Shift.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 30/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class Shifts : Object {
    
    dynamic var id:         CLong = 0
    dynamic var idUnity:    CLong = 0
    dynamic var dayOfWeek:  String = ""
    dynamic var startAt:    String = ""
    dynamic var endAt:      String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}


