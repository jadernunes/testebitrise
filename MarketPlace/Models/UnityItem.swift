//
//  UnityItem.swift
//  MarketPlace
//
//  Created by Luciano on 6/29/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class UnityItem: Object {

    dynamic var id            : CLong  = 0
    dynamic var title         : String = ""
    dynamic var subTitle      : String?
    dynamic var desc          : String?
    dynamic var price         : Double = 0.0
    dynamic var thumb         : String?
    dynamic var unityId       : CLong  = 0
    dynamic var originalPrice : Double = 0.0
    dynamic var special       : Bool   = false
    dynamic var idExternal    : String?
    let media                 = List<Media>()
    dynamic var favorite      : Bool   = false
    dynamic var idUnityItemType : Int = 0
    let unityItemModifiers = List<UnityItemModifier>()
    
    override static func primaryKey() -> String?{
        return "id"
    }
    
    
}
