//
//  SessionManager.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 8/23/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import RealmSwift

class SessionManager {
    static let sharedInstance = SessionManager()
    
    var listCalendar        : [AnyObject] = []
    var unityShifts         : NSDictionary!
    var idUnitToSchedule    : Int!
    var idUnitItem          : Int!
    var unityItem           : UnityItem!
    var unity               : Unity!
    var categoryName        : String!
    var secondsVerifyStatus = 15.0
    var tableEnabledByQr    = ""
    var pushToken           = ""
    var listUnityItemsSeleted = [(index:Int!,modifier:UnityItemModifier,listUnityItem:[UnityItem]!)]()
    var isShowMessageLocality = false
    
    var deliveryChosen = false
    var takeawayChosen = false
    var tableChosen    = false
    var voucherChosen  = false
    var idAddressSelected = 0
    var deliveryFee = 0.0
    var cartActive = false
    
    var cardColor = UIColor().hexStringToUIColor(baseColor)
    var searchOrQrEnabled = false
    var isVerifyNewsNotification = false
    var isVerifyAppVersion = false
    
    var lib4all = Lib4all.sharedInstance()
    
    init() {
        startCheckingStatusOrder()
        startCheckingStatusSchedule()
    }
    
    func hourAvaliableInUnit(date: NSString, startDateString: NSString, nextDate: NSString, listHourUnit: NSArray) -> Bool
    {
        for dicBlocked in listHourUnit
        {
            var dicBlocked = dicBlocked
            
            if dicBlocked.isKindOfClass(NSArray)
            {
                dicBlocked = dicBlocked.objectAtIndex(0) as! NSDictionary
            }
            
            let hourStartBlocked = dicBlocked.objectForKey("startAt") as! String
            var dateStartBlocked = ((date as String) + " " + hourStartBlocked)
            dateStartBlocked = ((date as String) + " " + NSString.getHourByDate(dateStartBlocked))
            
            let hourEndBlocked = dicBlocked.objectForKey("endAt") as! String
            var dateEndBlocked = ((date as String) + " " + hourEndBlocked)
            dateEndBlocked = ((date as String) + " " + NSString.getHourByDate(dateEndBlocked))
            
            let dif_INI_INI = NSDate.secondsByDifferenceDates(startDateString, dateStringFinish: dateStartBlocked)
            let dif_INI_FIM = NSDate.secondsByDifferenceDates(startDateString, dateStringFinish: dateEndBlocked)
            let  dif_FIM_INI = NSDate.secondsByDifferenceDates(nextDate, dateStringFinish: dateStartBlocked)
            let dif_FIM_FIM = NSDate.secondsByDifferenceDates(nextDate, dateStringFinish: dateEndBlocked)
            
            if !(dif_INI_INI >= 0
                && dif_INI_FIM < 0
                && dif_FIM_INI > 0
                && dif_FIM_FIM <= 0)
            {
                return false
            }
        }
        
        return true
    }
    
    func hourAvaliable(date: NSString, startDateString: NSString, nextDate: NSString, listBlocked: NSArray) -> Bool
    {
        for dicBlocked in listBlocked
        {
            let hourStartBlocked = dicBlocked.objectForKey("startAt") as! String
            var dateStartBlocked = ((date as String) + " " + hourStartBlocked)
            dateStartBlocked = ((date as String) + " " + NSString.getHourByDate(dateStartBlocked))
            
            let hourEndBlocked = dicBlocked.objectForKey("endAt") as! String
            var dateEndBlocked = ((date as String) + " " + hourEndBlocked)
            dateEndBlocked = ((date as String) + " " + NSString.getHourByDate(dateEndBlocked))
            
            let dif_INI_INI = NSDate.secondsByDifferenceDates(startDateString, dateStringFinish: dateStartBlocked)
            let dif_INI_FIM = NSDate.secondsByDifferenceDates(startDateString, dateStringFinish: dateEndBlocked)
            let  dif_FIM_INI = NSDate.secondsByDifferenceDates(nextDate, dateStringFinish: dateStartBlocked)
            let dif_FIM_FIM = NSDate.secondsByDifferenceDates(nextDate, dateStringFinish: dateEndBlocked)
            
            if !((dif_INI_INI < 0 && dif_INI_FIM < 0 && dif_FIM_INI < 0 && dif_FIM_FIM < 0)
                || (dif_INI_FIM >= 0 || dif_FIM_INI <= 0)
                || (dif_INI_INI < 0 && dif_FIM_FIM >= 0)
                )
            {
                return false
            }
            else if (dif_FIM_INI > 0 && dif_FIM_FIM <= 0) || (dif_INI_INI < 0 && dif_INI_FIM <= 0 && dif_FIM_INI > 0)
            {
                return false
            }
        }
        
        return true
    }
    
    private func startCheckingStatusOrder(){
        let loadOrderStatusTask = NSTimer.scheduledTimerWithTimeInterval(secondsVerifyStatus, target: self, selector: #selector(SessionManager.checkStatusOrders), userInfo: nil, repeats: true)
        
        loadOrderStatusTask.fire()
    }
    
    private func startCheckingStatusSchedule(){
        let loadScheduleStatusTask = NSTimer.scheduledTimerWithTimeInterval(secondsVerifyStatus, target: self, selector: #selector(SessionManager.updateScheduleStatusSentAndConfirmed), userInfo: nil, repeats: true)
        
        loadScheduleStatusTask.fire()
    }
    
    @objc func checkStatusOrders(){
        let listPendingOrders = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
        
        for pendingOrder in listPendingOrders {
            let order = Order(value: pendingOrder)
            let api = ApiServices()
            
            api.failureCase = { (msg) in
                //error
            }
            
            api.successCase = { (arrayStatus) in
                if let arr = arrayStatus as? NSArray {
                    
                    //check if there is status 80 to avoid the polling
                    for item in arr {
                        
                        if let status = item as? Dictionary<String, AnyObject> {
                            let orderStatus = status["idOrderStatus"] as! Int
                            if orderStatus > order.status {
                                let realm = try! Realm()
                                try! realm.write({
                                    order.status = orderStatus
                                    order.dthrLastStatus = NSDate().timeIntervalSince1970 * 1000
                                    realm.create(Order.self, value: order, update: true)
                                })
                                
                                NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_HOME_CART_OBSERVER, object: nil)
                            }
                        }
                    }
                }
            }
            
            api.getOrderStatus(String(order.id))
        }
    }
    
    @objc private func updateScheduleStatusSentAndConfirmed(){
        let listPendingSchedule = SchedulePersitence.getScheduleWithStatus("idStatus = \(StatusSchedule.Sent.rawValue)")
        
        for pendingSchedule in listPendingSchedule {
            let scheduleLocal = Schedule(value: pendingSchedule)
            let date = NSString.getDateByServerConvertToShow(scheduleLocal.dateString)
            
            let dataSchedule = [
                "dataShow": [
                    "dateToShow":date,
                    "hourStartToShow":scheduleLocal.timeBegin!
                    ] as NSDictionary!
                ] as NSDictionary!
            
            if(Util.checkSchedule(dataSchedule, isConfirmSchedule: false)){
                
                let api = ApiServices()
                
                api.failureCase = { (msg) in
                    //error
                }
                
                api.successCase = { (objectReceived) in
                    if(objectReceived != nil){
                        let scheduleReceived = objectReceived as! NSDictionary
                        let auxSchedule = SchedulePersitence.getScheduleByID((scheduleReceived["id"] as! Int)) as Schedule
                        
                        let scheduleStatusReceived = scheduleReceived["idStatus"] as! Int
                        let statusOld = auxSchedule.idStatus
                        
                        let realm = try! Realm()
                        
                        try! realm.write {
                            auxSchedule.idStatus = scheduleStatusReceived
                        }
                        
                        if scheduleStatusReceived != statusOld {
                            NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_SCHEDULE_UPDATE, object: nil)
                        }
                    }
                }
                
                api.getScheduleById(String(scheduleLocal.id))
            }
        }
    }
    
    //MARK: - Register devices
    
    func registerDevice() {
        let parametersSendRegister = NSMutableDictionary()
        var sessionToken = ""
        
        let lib4all = Lib4all.sharedInstance()
        if lib4all.hasUserLogged() {
            let user = User.sharedUser()
            if user.token != nil {
                sessionToken = user.token
            }
        }
        
        parametersSendRegister.setDictionary([
            "uuid":SystemInfo.getUserUuid(),
            "osVersion":SystemInfo.getSystemVersion(),
            "pushToken":PushTokenPersistence.getPushToken(),
            "model":SystemInfo.getSystemName(),
            "type":SystemInfo.getDevice(),
            "appBuild":SystemInfo.getBuild(),
            "os":1,
            "sessionToken":sessionToken
            ])
        
        SystemInfo.getLocationInfo { (latitude, longitude, timestamp) in
            parametersSendRegister.setObject(latitude, forKey: "latitude")
            parametersSendRegister.setObject(longitude, forKey: "longitude")
        }
        
        let api = ApiServices()
        api.successCase = {(obj) in
            if let dataRegister = obj as? NSDictionary {
                if SessionManager.sharedInstance.isVerifyAppVersion == false {
                    SessionManager.sharedInstance.isVerifyAppVersion = true
                    
                    if (dataRegister.objectForKey("blockVersion")?.boolValue == true) {
                        let message = UIAlertView.init(title: kAlertTitle, message: (dataRegister.objectForKey("message") as! String), delegate: self, cancelButtonTitle: kAlertButtonOk)
                        message.show()
                    }
                }
            }
        }
        
        api.failureCase = {(msg) in
            
        }
        api.registerDeviceInfo(parametersSendRegister)
    }
    
    func showNavigationButtons(isShowCart: Bool, isShowSearch: Bool, viewController: UIViewController) {
        
        let customButton = UIButton(type: .Custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 30, height: 60)
        customButton.addTarget(self, action: #selector(self.buttonCartPressed), forControlEvents: .TouchUpInside)
        customButton.setImage(UIImage(named: "iconCart"), forState: .Normal)
        customButton.tintColor = UIColor.whiteColor()
        
        let cartButton = BBBadgeBarButtonItem(customUIButton: customButton)
        
        if OrderEntityManager.sharedInstance.getCart(0) != nil {
            cartButton.badgeValue = "\(CartCustomManager.sharedInstance.checkNumberOfItemsInCart())"
        } else {
            if (OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)").firstObject as? Order) != nil {
            
                cartButton.badgeValue = "1"
                customButton.setImage(UIImage(named: "iconPedidoEmAndamento"), forState: .Normal)
            }
        }
        
        cartButton.shouldHideBadgeAtZero = true;
        cartButton.badgePadding = 5.0;
        cartButton.badgeOriginX = 20.0;
        cartButton.badgeOriginY = 8.0;
        
        var listButtons: [UIBarButtonItem] = []
        
        if isShowCart {
            listButtons.append(cartButton)
        }
        
        if isShowSearch {
            let buttonSearch = UIBarButtonItem(image: UIImage(named: "iconSearch"), style: .Plain, target: self, action:
                #selector(self.buttonSearchPressed))
            listButtons.append(buttonSearch)
        }
        
        viewController.navigationItem.setRightBarButtonItems(listButtons, animated: true)
    }
    
    @objc func buttonCartPressed(){
        if OrderEntityManager.sharedInstance.getCart(0) != nil {
            let page = PageManager.sharedInstance
            let visibleVC = page.visibleViewController()!
            if let vc = visibleVC as? SidePanelViewController {
                if let cart = OrderEntityManager.sharedInstance.getCart(0) {
                    if cart.orderItems.count > 0 {
                        let cartManager = CartCustomManager.sharedInstance
                        cartManager.showCart(vc.centerPanel)
                        cartManager.cartView.openCartFromViewController(vc.centerPanel)
                    }
                }
            }
        } else {
            let page = PageManager.sharedInstance
            let visibleVC = page.visibleViewController()!
            if let vc = visibleVC as? SidePanelViewController {
                
                if let order = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)").firstObject as? Order {
                    
                    if let unity = UnityPersistence.getUnityByID(String(order.idUnity)) {
                        let destVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TrackingViewController") as! TrackingViewController
                        destVc.title = unity.name
                        destVc.order = order
                        
                        (vc.centerPanel as! UINavigationController).pushViewController(destVc, animated: true)
                    }
                }
            }
        }
    }
    
    @objc func buttonSearchPressed(){
        let page = PageManager.sharedInstance
        let visibleVC = page.visibleViewController()!
        if let vc = visibleVC as? SidePanelViewController {
            let mainStoryboard = UIStoryboard(name: "NewSearch", bundle: NSBundle.mainBundle())
            let newSearch = mainStoryboard.instantiateViewControllerWithIdentifier("NewSearchViewController") as! NewSearchViewController
            (vc.centerPanel as! UINavigationController).pushViewController(newSearch, animated: true)
        }
    }
}


