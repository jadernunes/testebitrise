//
//  IterativeMessageViewController.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 13/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

protocol IterativeMessageDelegate {
    func buttonRefusePressed(sender:IterativeMessageViewController)
    func buttonConfirmPressed(sender:IterativeMessageViewController)
}

class IterativeMessageViewController: UIViewController {
    
    var delegate:       IterativeMessageDelegate?
    var typeActions:    ActionIterativeMessage! = .OneOption
    var tag: Int!
    var titleMessage:   String!
    var message:        String!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelTitle.text = self.titleMessage!
        self.textViewMessage.text = self.message!
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let colorBackgroudButton = UIColor.clearColor()
        let colorTextButton = UIColor.redColor()
        let textFirstButton = self.typeActions! == .TwoOptions ? "NÃO" : "OK"
        let heightButton:   CGFloat = 44
        let yButton:        CGFloat = self.view.frame.size.height - 54
        
        let widthButton:    CGFloat = self.typeActions! == .TwoOptions
            ? (self.view.frame.size.width/2) - 15
            : self.view.frame.size.width - 20
        
        let button1 = UIButton(frame: CGRect(x: 10, y: yButton, width: widthButton, height: heightButton))
        
        button1.layer.cornerRadius = 10.0
        button1.setTitleColor(colorTextButton, forState: UIControlState.Normal)
        button1.backgroundColor = colorBackgroudButton
        button1.layer.borderColor = colorTextButton.CGColor
        button1.layer.borderWidth = 0.5
        button1.setTitle(textFirstButton, forState: UIControlState.Normal)
        
        button1.addTarget(self, action: (self.typeActions! == .TwoOptions ? #selector(buttonRefusePressed) : #selector(buttonConfirmPressed)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let button2 = UIButton(frame:CGRect(x:(button1.frame.origin.x + button1.frame.width) + 10, y: yButton, width: widthButton, height: heightButton))
        
        button2.layer.cornerRadius = 10.0
        button2.setTitleColor(colorTextButton, forState: UIControlState.Normal)
        button2.backgroundColor = colorBackgroudButton
        button2.layer.borderColor = colorTextButton.CGColor
        button2.layer.borderWidth = 0.5
        button2.setTitle("SIM", forState: UIControlState.Normal)
        
        button2.addTarget(self, action: #selector(buttonConfirmPressed), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(button1)
        
        switch(self.typeActions!){
        case .TwoOptions:
            self.view.addSubview(button2)
            break
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonRefusePressed(sender: UIButton) {
        if let delegate = self.delegate{
            delegate.buttonRefusePressed(self)
        }
        self.formSheetController?.dismissAnimated(true, completionHandler: { (viewController: UIViewController!) in
            
        })
    }
    
    @IBAction func buttonConfirmPressed(sender: UIButton) {
        if let delegate = self.delegate{
            delegate.buttonConfirmPressed(self)
        }
        self.formSheetController?.dismissAnimated(true, completionHandler: { (viewController: UIViewController!) in
            
        })
    }
}
