//
//  HoursTableViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class FooterTableViewCell: UITableViewCell {

    @IBOutlet weak var lineTop: UIImageView!
    @IBOutlet weak var lineVertical: UIImageView!
    @IBOutlet weak var lineHorizontal: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMondaySaturday: UILabel!
    @IBOutlet weak var labelHolidays: UILabel!
    @IBOutlet weak var labelMondayContent: UILabel!
    @IBOutlet weak var labelHolidayContent: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lineTop.backgroundColor = SessionManager.sharedInstance.cardColor
        
        labelTitle.font = LayoutManager.primaryFontWithSize(16)
        
        labelMondaySaturday.font      = LayoutManager.primaryFontWithSize(16)
        labelMondaySaturday.textColor = UIColor.grayColor()
        
        labelMondayContent.font      = LayoutManager.primaryFontWithSize(16)
        labelMondayContent.textColor = UIColor.whiteColor()
        
        labelHolidays.font      = LayoutManager.primaryFontWithSize(16)
        labelHolidays.textColor = UIColor.grayColor()
        
        labelHolidayContent.font      = LayoutManager.primaryFontWithSize(16)
        labelHolidayContent.textColor = UIColor.whiteColor()
        
        lineVertical.backgroundColor    = UIColor.lightGrayColor()
        lineHorizontal.backgroundColor  = UIColor.lightGrayColor()
        let tintableImage = logoImage.image!.imageWithRenderingMode(.AlwaysTemplate)
        logoImage.image = tintableImage
        logoImage.tintColor = SessionManager.sharedInstance.cardColor
        self.selectionStyle = .None;

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
