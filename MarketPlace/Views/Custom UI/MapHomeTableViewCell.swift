//
//  MapHomeTableViewCellTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/21/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import QuartzCore

class MapHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var button        : UIButton!
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var containerMap  : UIView!
    @IBOutlet weak var imageMap      : UIImageView!

    var rootViewController  : HomeTableViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.button.setNeedsLayout()
        self.button.layoutIfNeeded()
        
        self.backgroundColor    = LayoutManager.backgroundLight
        containerMap.backgroundColor = SessionManager.sharedInstance.cardColor
        imageMap.tintColor       = UIColor.whiteColor()
        button.titleLabel?.font = LayoutManager.primaryFontWithSize(14)
        button.setTitleColor(LayoutManager.backgroundFirstColor, forState: .Normal)

        self.button.backgroundColor     = UIColor.whiteColor()
        button.tintColor                = SessionManager.sharedInstance.cardColor

        button.layer.cornerRadius       = 6.0
        self.selectionStyle             = .None;
        
        LayoutManager.roundCustomCornersRadius(containerMap, radius: 6.0, corners: [.TopLeft])
        LayoutManager.roundCustomCornersRadius(containerMap, radius: 6.0, corners: [.BottomLeft])
//        containerView.layer.shadowRadius   = 1.0
//        containerView.layer.shadowColor    = UIColor.blackColor().colorWithAlphaComponent(0.6).CGColor
//        containerView.layer.shadowOffset   = CGSizeMake(0.0, -1.0);
//        containerView.layer.shadowOpacity  = 0.5
//        containerView.layer.masksToBounds  = false
        //LayoutManager.roundCustomCornersRadius(containerView, radius: 6.0, corners: [.TopLeft, .BottomLeft, .TopRight, .BottomRight])

        // Rounded corners.
        containerView.layer.cornerRadius = 6
        containerView.clipsToBounds = true
        // A thin border.
        containerView.layer.borderColor = UIColor.blackColor().CGColor
        containerView.layer.borderWidth = 0.0
        
        // Drop shadow.
        containerView.layer.shadowColor = UIColor.blackColor().CGColor
        containerView.layer.shadowOpacity = 0.6
        containerView.layer.shadowRadius = 2.0
        containerView.layer.shadowOffset = CGSizeMake(0, 2)

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showMap(sender: UIButton) {
        let vc = rootViewController.storyboard!.instantiateViewControllerWithIdentifier("mapShopping")as! UINavigationController
        (vc.viewControllers[0] as! MapWebViewController).urlString = BASE_URL_MAP
        rootViewController.presentViewController(vc, animated: true, completion: nil)
    }
}
