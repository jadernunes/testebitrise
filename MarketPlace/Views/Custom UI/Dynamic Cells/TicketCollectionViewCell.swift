//
//  TicketCollectionViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 10/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelQuantity.font = LayoutManager.primaryFontWithSize(24)
        self.labelPrice.font = LayoutManager.primaryFontWithSize(18)
        self.backgroundColor = UIColor.clearColor()
        self.labelPrice.textColor = SessionManager.sharedInstance.cardColor
        self.labelQuantity.textColor = SessionManager.sharedInstance.cardColor
        self.layer.cornerRadius = 6
        self.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        self.layer.borderWidth  = 1.0
    }
}
