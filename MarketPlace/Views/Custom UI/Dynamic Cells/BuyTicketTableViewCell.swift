//
//  BuyTicketTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 10/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class BuyTicketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageTicket: UIImageView!
    @IBOutlet weak var labelBuyTicket: UILabel!
    @IBOutlet weak var viewButtonBuyTicket: UIView!
    @IBOutlet weak var marginLeft: NSLayoutConstraint!
    var rootVc : UIViewController!
    var fact   : Fact?
    override func awakeFromNib() {
        super.awakeFromNib()
        viewButtonBuyTicket.layer.cornerRadius = 6
        viewButtonBuyTicket.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        viewButtonBuyTicket.layer.borderWidth  = 1.0
        viewButtonBuyTicket.layer.backgroundColor = SessionManager.sharedInstance.cardColor.CGColor
        let origImage = UIImage(named: "Icone ticket")
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        imageTicket.image = tintedImage
        imageTicket.tintColor = LayoutManager.white
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false) -> BuyTicketTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(BUY_TICKET_BUTTON) as! BuyTicketTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        cell.labelBuyTicket.text = "Comprar Ingresso"
        cell.labelBuyTicket?.font   = LayoutManager.primaryFontWithSize(18)
        cell.labelBuyTicket?.textColor = LayoutManager.white
        let tap = UITapGestureRecognizer(target: cell, action: #selector(handleTap))
        cell.viewButtonBuyTicket.addGestureRecognizer(tap)
        
        return cell
    }
    
    func handleTap() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("buyTicketVC") as! BuyTicketsEventViewController
        vc.fact = fact
        rootVc.navigationController?.pushViewController(vc, animated: true)    }
}
