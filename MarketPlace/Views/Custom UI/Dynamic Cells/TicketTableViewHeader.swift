//
//  TicketTableViewHeaderCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 16/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketTableViewHeader : UITableViewHeaderFooterView {

    @IBOutlet weak var hairViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.hairViewHeightConstraint.constant = 0.4
        }
    }
    
    @IBOutlet weak var hairView: UIView! {
        didSet {
            self.hairView.backgroundColor = isOneMarketplace ?
                LayoutManager.white : SessionManager.sharedInstance.cardColor
        }
    }
    
    @IBOutlet weak var headerTitleLabel: UILabel! {
        didSet {
            self.headerTitleLabel.font = LayoutManager.primaryFontWithSize(18)
            self.headerTitleLabel.textColor = isOneMarketplace ?
                LayoutManager.white : LayoutManager.labelFirstColor
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = isOneMarketplace ?
            LayoutManager.backgroundDark : LayoutManager.white
    }
    
    func configureWithEventSectionTitle(title: String) {
        self.headerTitleLabel.text = title
    }
}
