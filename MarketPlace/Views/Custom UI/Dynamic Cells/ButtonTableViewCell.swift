//
//  ButtonTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/7/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var button : UIButton!
    @IBOutlet weak var constraintLeftMargin: NSLayoutConstraint!
    @IBOutlet weak var imageViewCalendar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        button.layer.cornerRadius = 4
        button.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        button.layer.borderWidth  = 0.5
        button.titleLabel?.font   = LayoutManager.primaryFontWithSize(15)
        button.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        imageViewCalendar.tintImage(SessionManager.sharedInstance.cardColor)
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false) -> ButtonTableViewCell{

        let cell = tableView.dequeueReusableCellWithIdentifier(BUTTON_IDENTIFIER) as! ButtonTableViewCell
        cell.backgroundColor = UIColor.clearColor()
    
        return cell
    }
}
