//
//  StandardTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class StandardTableViewCell: UITableViewCell {

    @IBOutlet weak var constraintLineHeight: NSLayoutConstraint!
    @IBOutlet weak var viewSeparator: UIImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        constraintLineHeight.constant = 0.5
        labelTitle.font      = LayoutManager.primaryFontWithSize(16)
        labelTitle.textColor = LayoutManager.labelContrastColor
        self.viewSeparator.tintImage(LayoutManager.labelContrastColor)
        self.backgroundColor = LayoutManager.backgroundFirstColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, group : Group) -> StandardTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(STANDARD_IDENTIFIER) as! StandardTableViewCell
        cell.labelTitle.text      = group.name
        
        return cell

    }
}
