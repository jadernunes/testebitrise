//
//  TicketTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 12/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketTableViewCell : UITableViewCell {
    
    @IBOutlet weak var ticketTitleLabel: UILabel!
    @IBOutlet weak var ticketPriceLabel: UILabel!
    @IBOutlet weak var ticketCellBuyView: UIView!
    @IBOutlet weak var ticketImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        
        ticketCellBuyView.layer.cornerRadius    = 6
        ticketCellBuyView.layer.borderColor     = SessionManager.sharedInstance.cardColor.CGColor
        ticketCellBuyView.layer.borderWidth     = 1.0
        ticketCellBuyView.layer.backgroundColor = SessionManager.sharedInstance.cardColor.CGColor
        
        let origImage           = UIImage(named: "Icone ticket")
        let tintedImage         = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        ticketImage.image       = tintedImage
        ticketImage.tintColor   = LayoutManager.white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureWithTicket(ticket: Ticket) {
        self.ticketTitleLabel.text      = ticket.name
        self.ticketTitleLabel.font      = LayoutManager.primaryFontWithSize(18)
        self.ticketTitleLabel.textColor = LayoutManager.white
        
        /// For some reason the value retrieved from url comes * 100
        self.ticketPriceLabel.text              = Util.formatCurrency(Double(ticket.price) / 100.0)
        self.ticketPriceLabel.font              = LayoutManager.primaryFontWithSize(18)
        self.ticketPriceLabel.textColor         = LayoutManager.white
    }
}
