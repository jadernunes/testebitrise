//
//  TicketButtonTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Luz on 10/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewTicket: UIImageView!
    @IBOutlet weak var marginLeft: NSLayoutConstraint!
    @IBOutlet weak var labelAccessTicket: UILabel!
    @IBOutlet weak var viewAccessTicket: UIView!
    var superViewController: UIViewController!
    var fact: Fact?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewAccessTicket.layer.cornerRadius = 6
        viewAccessTicket.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        viewAccessTicket.layer.borderWidth  = 1.0
        viewAccessTicket.layer.backgroundColor = UIColor.clearColor().CGColor
        labelAccessTicket?.font   = LayoutManager.primaryFontWithSize(19)
        labelAccessTicket?.textColor = SessionManager.sharedInstance.cardColor
        labelAccessTicket?.text = "Acessar Ingresso"
        imageViewTicket.tintImage(SessionManager.sharedInstance.cardColor)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false,superViewController: UIViewController) -> TicketButtonTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(ACCESS_TICKET_BUTTON) as! TicketButtonTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        cell.fact = fact
        let tap = UITapGestureRecognizer(target: cell, action: #selector(showMyTickets))
        cell.viewAccessTicket.addGestureRecognizer(tap)
        cell.superViewController = superViewController
        
        return cell
    }
    
    func showMyTickets(){
        
        if self.fact != nil {
            let viewController = UIStoryboard(name: "VoucherEIngresso", bundle: nil).instantiateViewControllerWithIdentifier("NewVoucherAndIngressoViewController") as! NewVoucherAndIngressoViewController
            viewController.isListVouchers = false
            
            self.superViewController.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
