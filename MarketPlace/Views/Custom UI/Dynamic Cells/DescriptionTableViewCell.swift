//
//  DescriptionTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var textDescription : UITextView!
    @IBOutlet weak var marginLeft: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle             = .None;
        textDescription.font            = LayoutManager.primaryFontWithSize(16)
        textDescription.backgroundColor = UIColor.clearColor()

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false) -> DescriptionTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(DESCRIPTION_IDENTIFIER) as! DescriptionTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        
        cell.textDescription.text = fact.desc
        if isOneMarketplace == true {
            cell.backgroundColor = LayoutManager.backgroundDark
        }else{
            cell.backgroundColor = LayoutManager.white
        }
        
        cell.textDescription.textColor = isOneMarketplace ? LayoutManager.labelSecondColor : LayoutManager.labelFirstColor
        cell.contentView.hidden = false
        
        return cell
    }

}
