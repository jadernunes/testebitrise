//
//  MapTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/4/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle   : UILabel!
    @IBOutlet weak var viewLine     : UIView!
    @IBOutlet weak var webView      : UIWebView!
    var url = BASE_URL_MAP

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle      = .None;
        self.backgroundColor     = LayoutManager.backgroundLight
        viewLine.backgroundColor = SessionManager.sharedInstance.cardColor
        labelTitle.font          = LayoutManager.primaryFontWithSize(18)
        
        let url = NSURL(string: self.url)
        let requestObj = NSURLRequest(URL: url!)
        webView.scrollView.scrollEnabled = false
        webView.userInteractionEnabled = false
        webView.scrollView.bounces = false
        
        webView.loadRequest(requestObj);
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    static func getConfiguredCell(tableView : UITableView, styleDark : Bool = false, luc: String = "") -> MapTableViewCell{

        let cell = tableView.dequeueReusableCellWithIdentifier(MAP_IDENTIFIER) as! MapTableViewCell
        cell.contentView.hidden = false
        if luc != "" {
            cell.url = BASE_URL_MAP + "/?store=true#location-" + luc
        }
        
        let url = NSURL(string: cell.url)
        let requestObj = NSURLRequest(URL: url!)
        cell.webView.loadRequest(requestObj);

        return cell
    }
}
