//
//  AllMoviesTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class AllMoviesTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //IBOutlets
    @IBOutlet weak var collectView      : UICollectionView!
    @IBOutlet weak var buttonSeeMore    : UIButton!
    @IBOutlet weak var labelTitle       : UILabel!
    
    //constraints
    @IBOutlet weak var titleHeight  : NSLayoutConstraint!
    @IBOutlet weak var buttonHeight : NSLayoutConstraint!
    
    //Variables
    var rootVc          : UIViewController!
    var arrContents     = NSMutableArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.collectView.registerNib(UINib(nibName: "CardCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CARD_IDENTIFIER)
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
        
        arrContents = MovieEntityManager.getMovies(true)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrContents.count;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        //Split the view to display two columns
        
        collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
        let size = CGSizeMake(self.collectView.frame.size.width/2-15, self.collectView.frame.size.width/2+60)
        
        return size
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let customCell = self.collectView.dequeueReusableCellWithReuseIdentifier(CARD_IDENTIFIER, forIndexPath: indexPath) as! CardCollectionViewCell
        
        customCell.showTitle = false
        customCell.layoutIfNeeded()
        
        var movie : Movie!
        if let obj = arrContents.objectAtIndex(indexPath.row) as? Movie{
            movie = obj
        }else{
            movie = Movie(value:arrContents.objectAtIndex(indexPath.row) as! [String:AnyObject])
        }
        var url = ""
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        //Check if it should download thumb or externalThumb
        if movie?.thumb != nil {
            if let urlString = movie.thumb as String! {
                if urlString.characters.count > 0 {
                    url = urlString
                }
            }
        }
        
        var concatItems = ""
        
        if movie.movieSessions.count > 0{
            for session : MovieSession in movie.movieSessions{
                if session.sessionDatetime.value != 0 {
                    concatItems = concatItems + " - " + session.getFormatedDateString("HH:mm")
                }
            }
            
            if concatItems.characters.count > 0{
                concatItems = concatItems.substringFromIndex(concatItems.startIndex.advancedBy(2))
            }
        }
        
        customCell.imagePoster.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                customCell.imagePoster.contentMode      = .ScaleAspectFill
            }
        }
        
        return customCell;
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        Util.animateGridCell(cell, indexPath: indexPath, bounceSize: 1.10)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        var movie : Movie
        
        if let obj = arrContents.objectAtIndex(indexPath.row) as? Movie{
            movie = obj
        }else{
            movie = Movie(value:arrContents.objectAtIndex(indexPath.row) as! [String:AnyObject])
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MovieDetailsVC") as! MovieDetailsViewController
        
        vc.movie = movie
        
        rootVc.navigationController?.pushViewController(vc, animated: true)
    }
 
}
