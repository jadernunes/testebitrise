//
//  BannerImageTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 6/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import SDWebImage

class BannerImageTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imageBackground        : UIImageView!
    @IBOutlet weak var labelRangeTime         : UILabel!
    @IBOutlet weak var labelTitle             : UILabel!
    @IBOutlet weak var imageLine              : UIView!
    @IBOutlet weak var imageLineDesc          : UIView!
    @IBOutlet weak var labelAuthor            : UILabel!
    @IBOutlet weak var viewContainer          : UIView!
    @IBOutlet weak var viewContainerEvent     : UIView!
    @IBOutlet weak var labelFirstDescription  : UILabel!
    @IBOutlet weak var labelSecondDescription : UILabel!
    @IBOutlet weak var labelDay               : UILabel!
    @IBOutlet weak var labelMonth             : UILabel!
    @IBOutlet weak var labelTime              : UILabel!
    var gradientMask : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        labelRangeTime.font = LayoutManager.primaryFontWithSize(14)
        labelFirstDescription.font = LayoutManager.primaryFontWithSize(18)
        labelSecondDescription.font = LayoutManager.primaryFontWithSize(17)
        labelTitle.font = LayoutManager.primaryFontWithSize(17)
        labelAuthor.font = LayoutManager.primaryFontWithSize(16)
        labelDay.font = LayoutManager.primaryFontWithSize(50)
        viewContainerEvent.backgroundColor = SessionManager.sharedInstance.cardColor.colorWithAlphaComponent(0.8)
        
        labelMonth.font            = LayoutManager.primaryFontWithSize(14)
        labelTime.font             = LayoutManager.primaryFontWithSize(14)
        
        labelAuthor.hidden = true
        self.selectionStyle = .None;
        
        
        labelAuthor.textColor               = UIColor.whiteColor()
        labelTitle.textColor                = UIColor.whiteColor()
        labelRangeTime.textColor            = UIColor.whiteColor()
        labelFirstDescription.textColor     = UIColor.whiteColor()
        labelSecondDescription.textColor    = UIColor.whiteColor()
        labelTime.textColor                 = LayoutManager.regularFontColor
        labelMonth.textColor                = LayoutManager.regularFontColor
        imageLine.backgroundColor           = SessionManager.sharedInstance.cardColor
        imageLineDesc.backgroundColor       = SessionManager.sharedInstance.cardColor
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, showDetailsBanner : Bool) -> BannerImageTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(BANNER_IMAGE_IDENTIFIER) as! BannerImageTableViewCell
        cell.backgroundColor = LayoutManager.backgroundFirstColor
        
        cell.labelFirstDescription.textColor = LayoutManager.labelSecondColor
        cell.labelSecondDescription.textColor = LayoutManager.labelSecondColor
        
        var url = ""
        //Check if it should download thumb or externalThumb
        if fact.thumb != nil {
            if fact.thumb!.characters.count > 0{
                url = fact.thumb!
            }
        }
        
        cell.imageBackground.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                cell.imageBackground.contentMode      = .ScaleAspectFill
            }
        }
        
        if showDetailsBanner == true {
            cell.labelRangeTime.text = "de \(fact.getFormatedDateString(true, pattern: "dd/MM")) a \(fact.getFormatedDateString(true, pattern: "dd/MM/yyyy"))"
            cell.labelTitle.text = fact.title
            cell.viewContainer.hidden       = false
            cell.viewContainerEvent.hidden  = true
            
        }else{
            
            cell.viewContainer.hidden           = true
            cell.viewContainerEvent.hidden      = false
            cell.labelDay.text                  = fact.getFormatedDateString(true, pattern: "dd")
            cell.labelTime.text                 = "às \(fact.getFormatedDateString(true, pattern: "HH:mm"))hs".uppercaseString
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            cell.labelMonth.text                = months[Int(fact.getFormatedDateString(true, pattern: "MM"))!-1].uppercaseString
            cell.labelFirstDescription.text     = fact.title
            
            cell.labelSecondDescription.text    = fact.subtitle
            
        }
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
}
