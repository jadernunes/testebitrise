//
//  PageMenuTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 8/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import PageMenu

class PageMenuTableViewCell: UITableViewCell {

    var pageMenu        : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var contentArray    : NSArray!
    var store           : Unity!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)           // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, contentArray : NSArray, store : Unity, referencesToViewController : UIViewController) -> PageMenuTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(PAGES_IDENTIFIER) as! PageMenuTableViewCell
        cell.contentArray = contentArray
        cell.store = store
        cell.configurePages(referencesToViewController)
        cell.backgroundColor = UIColor.clearColor()
        tableView.backgroundColor = LayoutManager.backgroundFirstColor
        
        return cell
    }

    func configurePages(referencesToViewController : UIViewController) -> Void {
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        
        for item in contentArray {
            if let dict = item as? NSDictionary {
                let controller   = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListProdsVC") as! ListProductsViewController
                
                controller.referenceToSuperViewController = referencesToViewController
                SessionManager.sharedInstance.categoryName = dict.valueForKey("name") as? String
                controller.title = dict.valueForKey("name") as? String
                controller.loadView()
                
                if let prods = dict.valueForKey("unityItems") as? NSArray{
                    controller.listProducts = prods
                    controller.unity        = store
                }
            
                controllerArray.append(controller)
            }
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(SessionManager.sharedInstance.cardColor),
            .ViewBackgroundColor(UIColor.whiteColor()),
            .MenuItemSeparatorWidth(4.3),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.1),
            .SelectionIndicatorColor(LayoutManager.backgroundFirstColor),
            .MenuItemFont(LayoutManager.primaryFontWithSize(16)),
            .MenuHeight(40.0),
            .UnselectedMenuItemLabelColor(LayoutManager.regularFontColor),
            .CenterMenuItems(true)
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.frame.width, self.frame.height), pageMenuOptions: parameters)
        pageMenu?.menuHeight = 60
        pageMenu?.scrollMenuBackgroundColor = SessionManager.sharedInstance.cardColor
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.addSubview(pageMenu!.view)
        
        if self.pageMenu?.childViewControllers.count > 0 {
            let cont = (self.pageMenu?.childViewControllers[0] as! ListProductsViewController)
            cont.tableView.reloadData()
            cont.tableView.scrollEnabled    = false
        }

    }
    
    
}
