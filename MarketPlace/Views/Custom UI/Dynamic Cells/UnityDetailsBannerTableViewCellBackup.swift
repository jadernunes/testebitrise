//////
//////  UnityDetailsBannerTableViewCell.swift
//////  MarketPlace
//////
//////  Created by Luciano on 7/20/16.
//////  Copyright © 2016 4all. All rights reserved.
//////
////
//import UIKit
////
//class UnityDetailsBannerTableViewCellBackup: UITableViewCell {
////
////    @IBOutlet weak var imageBanner      : UIImageView!
////    @IBOutlet weak var labelDistance    : UILabel!
////    @IBOutlet weak var labelCategory    : UILabel!
//////    @IBOutlet weak var labelDelivery    : UILabel!
////    @IBOutlet weak var labelStatusOpen  : UILabel!
////    @IBOutlet weak var labelTimeOpen    : UILabel!
////    
////    var unity : Unity!
////    
//    override func awakeFromNib() {
//        super.awakeFromNib()
////        // Initialization code
////        
////        self.backgroundColor          = LayoutManager.backgroundLight
////        self.labelDistance.font       = LayoutManager.primaryFontWithSize(14)
////        self.labelDistance.textColor  = UIColor.whiteColor()
////        self.labelDistance.hidden     = true
////        self.labelCategory.font       = LayoutManager.primaryFontWithSize(14)
////        self.labelCategory.textColor  = LayoutManager.regularFontColor
////        
//////        self.labelDelivery.font       = LayoutManager.primaryFontWithSize(14)
//////        self.labelDelivery.textColor  = LayoutManager.regularFontColor
////        
////        self.labelStatusOpen.font               = LayoutManager.primaryFontWithSize(14)
////        self.labelStatusOpen.textColor          = UIColor.whiteColor()
////        self.labelStatusOpen.layer.cornerRadius = 10
////        self.labelStatusOpen.clipsToBounds      = true
////        self.labelTimeOpen.font                 = LayoutManager.primaryFontWithSize(14)
////        self.labelTimeOpen.textColor            = LayoutManager.regularFontColor
////        
////        self.selectionStyle = .None;
////        
//    }
////
//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
////
////        // Configure the view for the selected state
//    }
////
////    static func getConfiguredCell(tableView : UITableView, unity : Unity) -> UnityDetailsBannerTableViewCell{
////        let cell = tableView.dequeueReusableCellWithIdentifier(STORE_BANNER_DETAILS_IDENTIFIER) as! UnityDetailsBannerTableViewCell
////        cell.backgroundColor = UIColor.clearColor()
////        tableView.backgroundColor = LayoutManager.backgroundDark
////        
////        cell.unity = unity
////        
////        if unity.opened {
////            cell.labelStatusOpen.backgroundColor    = UIColor().hexStringToUIColor("#03c318")
////            cell.labelStatusOpen.text               = "Aberto"
////        }else{
////            cell.labelStatusOpen.backgroundColor    = UIColor().hexStringToUIColor("#C50C18")
////            cell.labelStatusOpen.text               = "Fechado"
////        }
////        
////        var url     = ""
//////        var phone   = ""
//////        var site    = ""
////        //Check if it should download thumb or externalThumb
////        
////        if unity.getThumb() != nil {
////            if unity.getThumb()!.value != nil && unity.getThumb()!.value?.characters.count > 0 {
////                url = unity.getThumb()!.value!
////            }
////        }
////        
////        cell.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
////            if image != nil {
////                cell.imageBanner.contentMode = .ScaleAspectFill
////            }
////        }
////        
////        if unity.desc != nil {
////            cell.labelCategory.text = unity.desc
////        }
////        
//////        cell.labelName.text = unity.name
//////        if unity.address != nil && unity.address != "" {
//////            cell.labelAddress.text = unity.address
//////        }else{
//////            cell.labelAddress.text = "Endereço não informado"
//////        }
//////        
//////        if unity.phoneNumber != nil {
//////            phone = unity.phoneNumber!
//////        }
//////        
//////        if unity.siteUrl != nil {
//////            site = unity.siteUrl!
//////        }
//////        cell.labelInfo.text     = "Fone: \(phone)\n\(site)"
//////        
//////        
//////        if unity.twitterUrl != nil {
//////            if let _ = NSURL(string: unity.twitterUrl!) {
//////                cell.buttonTwitter.enabled = true
//////            }else{
//////                cell.buttonTwitter.enabled = false
//////            }
//////        }
//////        
//////        if unity.facebookUrl != nil {
//////            if let _ = NSURL(string: unity.facebookUrl!) {
//////                cell.buttonFacebook.enabled = true
//////            }else{
//////                cell.buttonFacebook.enabled = false
//////            }
//////        }
////        
////        return cell
////    }
////
////
////    @IBAction func openFacebook(sender: AnyObject) {
////        var url : NSURL
////        
////        if unity.twitterUrl!.containsString("http://") ||
////            unity.twitterUrl!.containsString("https://") {
////            url = NSURL(string: unity.facebookUrl!)!
////        }else{
////            url = NSURL(string: "http://" + unity.facebookUrl!)!
////        }
////        
////        UIApplication.sharedApplication().openURL(url)
////
////    }
////    
////    @IBAction func openTwitter(sender: AnyObject) {
////        var url : NSURL
////        
////        if unity.twitterUrl!.containsString("http://") ||
////            unity.twitterUrl!.containsString("https://") {
////            url = NSURL(string: unity.twitterUrl!)!
////        }else{
////            url = NSURL(string: "http://" + unity.twitterUrl!)!
////        }
////        
////        UIApplication.sharedApplication().openURL(url)
////    }
//}
