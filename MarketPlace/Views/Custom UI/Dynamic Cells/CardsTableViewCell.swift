//
//  CollectionTableViewTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/11/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //MARK: Variables
    
    //IBOutlets
    @IBOutlet weak var collectView       : UICollectionView!
    @IBOutlet weak var labelCategory     : UILabel!
    @IBOutlet weak var labelNumItems     : UILabel!
    @IBOutlet weak var segmentedControl  : UISegmentedControl!
    
    //Variables
    var arrContents     = NSMutableArray()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.collectView.registerNib(UINib(nibName: "CardCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cardCollectionCell")
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
        
        let layout = self.collectView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .Horizontal
        //self.collectView.pagingEnabled = true
        self.selectionStyle = .None;

    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Methods
    
    func configureLayout(data : Dictionary<String, AnyObject>){
                
        labelCategory.font          = LayoutManager.primaryFontWithSize(13)
        labelCategory.font          = LayoutManager.primaryFontWithSize(13)
        
        self.backgroundColor = LayoutManager.backgroundFirstColor
        self.selectionStyle = .None;
    }
    
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrContents.count;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        //Split the view to display two columns
        
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let size = CGSizeMake(self.collectView.frame.size.width/2-10, self.collectView.bounds.height-10)
        
        return size
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let customCell = self.collectView.dequeueReusableCellWithReuseIdentifier(CARD_COL_IDENTIFIER, forIndexPath: indexPath) as! CardCollectionViewCell
        
        var unity : UnityItem!
        if let obj = arrContents.objectAtIndex(indexPath.row) as? UnityItem{
            unity = obj
        }else{
            unity = UnityItem(value:arrContents.objectAtIndex(indexPath.row) as! [String:AnyObject])
        }
        var url = ""
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        //Check if it should download thumb or externalThumb
        if unity.thumb != nil{
            if unity.thumb!.characters.count > 0{
                url = unity.thumb!
            }
        }
        
        customCell.labelTitle.text          = unity.title
        customCell.imagePoster.contentMode  = .ScaleAspectFit
        customCell.imagePoster.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                customCell.imagePoster.contentMode      = .ScaleToFill
            }
        }

        return customCell;
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
            }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false) -> CardsTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CARDS_IDENTIFIER) as! CardsTableViewCell
        
        return cell
    }

}
