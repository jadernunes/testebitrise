//
//  StoreBannerTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/15/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class StoreBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var imageBanner       : UIImageView!
    @IBOutlet weak var labelTitle        : UILabel!
    @IBOutlet weak var labelAgendamentos : UILabel!
    @IBOutlet weak var labelPedidos      : UILabel!
    @IBOutlet weak var imagePedidos      : UIImageView!
    @IBOutlet weak var imageAgendamentos : UIImageView!
    @IBOutlet weak var lineSeparator: UIView!
    @IBOutlet weak var image4all: UIImageView!
    @IBOutlet weak var label4all: UILabel!
    @IBOutlet weak var viewSoon: UIView!
    @IBOutlet weak var labelSoon: UILabel!
    @IBOutlet weak var imagePin: UIImageView!
    @IBOutlet weak var labelDistance: UILabel!
    
    @IBOutlet weak var constraintImageOrderToPayment: NSLayoutConstraint!
    @IBOutlet weak var constraintImageOrderToSuperview: NSLayoutConstraint!
    @IBOutlet weak var constraintImageScheduleToOrder: NSLayoutConstraint!
    @IBOutlet weak var constraintImageScheduleToPayment: NSLayoutConstraint!
    @IBOutlet weak var constraintImageScheduleToSuperview: NSLayoutConstraint!
    
    static let distanceFromUser = Double(1000)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor     = UIColor.clearColor()
        lineSeparator.backgroundColor = SessionManager.sharedInstance.cardColor
        labelTitle.textColor     = LayoutManager.white
        labelTitle.font          = LayoutManager.primaryFontWithSize(22)
        labelTitle.numberOfLines = 2
        labelTitle.lineBreakMode = .ByWordWrapping
        
        labelAgendamentos.textColor = LayoutManager.white
        labelAgendamentos.font      = LayoutManager.primaryFontWithSize(14)
        
        labelPedidos.textColor = LayoutManager.white
        labelPedidos.font      = LayoutManager.primaryFontWithSize(14)
        
        label4all.textColor = LayoutManager.white
        label4all.font      = LayoutManager.primaryFontWithSize(14)
        
        labelSoon.textColor = LayoutManager.white
        labelSoon.font      = LayoutManager.primaryFontWithSize(16)
        
        imagePedidos.tintImage(LayoutManager.white)
        imageAgendamentos.tintImage(LayoutManager.white)
        image4all.tintImage(LayoutManager.white)
        hideAllLabels()

        viewSoon.backgroundColor = SessionManager.sharedInstance.cardColor
        LayoutManager.roundCustomCornersRadius(viewSoon, radius: 6.0, corners: [.BottomLeft, .TopLeft])

        labelDistance.textColor = LayoutManager.white
        labelDistance.font      = LayoutManager.primaryFontWithSize(18)
        
        self.selectionStyle = .None
    }

    func hideAllLabels() {
        labelPedidos.hidden = true
        labelAgendamentos.hidden = true
        imagePedidos.hidden = true
        imageAgendamentos.hidden = true
        label4all.hidden = true
        image4all.hidden = true
        viewSoon.hidden = true
        labelSoon.hidden = true
        imagePin.hidden = true
        labelDistance.hidden = true
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func adjustConstraintPriorities() {
        constraintImageOrderToPayment.priority = 999
        constraintImageOrderToSuperview.priority = 998
        constraintImageScheduleToOrder.priority = 999
        constraintImageScheduleToPayment.priority = 998
        constraintImageScheduleToSuperview.priority = 997
    }
    
    static func getConfiguredCell(tableView : UITableView, unity : Unity) -> StoreBannerTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(STORE_IDENTIFIER) as! StoreBannerTableViewCell
        cell.hideAllLabels()
        cell.adjustConstraintPriorities()
        
        var url = ""
        let thumb = unity.getThumb()
        //Check if it should download thumb or externalThumb
        if thumb != nil && thumb?.value != nil {
            url = (thumb?.value)!
        }
        
        cell.imageBanner.contentMode      = .ScaleAspectFill
        cell.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                cell.imageBanner.contentMode = .ScaleToFill
            }
        }
        
        cell.labelTitle.text    = unity.name
        
        if unity.fourPayEnabled == true {
            if unity.orderEnabled == false {
                cell.constraintImageScheduleToOrder.priority = 998
                cell.constraintImageScheduleToPayment.priority = 999
                cell.constraintImageScheduleToSuperview.priority = 997
            }
        }
        
        else {
            cell.constraintImageOrderToPayment.priority = 998
            cell.constraintImageOrderToSuperview.priority = 999
            if unity.orderEnabled == true {
                cell.constraintImageScheduleToOrder.priority = 999
                cell.constraintImageScheduleToPayment.priority = 997
                cell.constraintImageScheduleToSuperview.priority = 998
            }
            else {
                cell.constraintImageScheduleToOrder.priority = 998
                cell.constraintImageScheduleToPayment.priority = 997
                cell.constraintImageScheduleToSuperview.priority = 999
            }
        }
        
        if unity.fourPayEnabled == true {
            cell.label4all.hidden = false
            cell.image4all.hidden = false
        }
        
        if unity.schedulingEnabled == true {
            cell.labelAgendamentos.hidden = false
            cell.imageAgendamentos.hidden = false
        }
        
        if unity.orderEnabled == true {
            cell.labelPedidos.hidden = false
            cell.imagePedidos.hidden = false
        }
        
        if unity.soon == true {
            cell.labelSoon.hidden = false
            cell.viewSoon.hidden = false
            cell.label4all.hidden = true
            cell.image4all.hidden = true
            cell.labelAgendamentos.hidden = true
            cell.imageAgendamentos.hidden = true
            cell.labelPedidos.hidden = true
            cell.imagePedidos.hidden = true
        }
        
        if unity.distanceFromUser > kDistanceMinPin && unity.distanceFromUser <= kDistanceMaxPin {
            cell.imagePin.hidden = false
            cell.labelDistance.hidden = false
            if (unity.distanceFromUser) > distanceFromUser {
                cell.labelDistance.text = "\(Int(unity.distanceFromUser/distanceFromUser)) " + kKilometers

            }
            else {
                
                cell.labelDistance.text = "\(Int(unity.distanceFromUser)) " + kMeters
            }
        }
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }

    
}
