//
//  TicktesListTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 12/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class TicketsListTableViewCell_ : UITableViewCell {
    
    /// Struct to represent a section
    struct EventHolder {
        var name: String
        var tickets: List<Ticket>
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    private let cellHeight                  = 80
    private let headerHeight                = 50
    
    /// Var that will tell to super what should be it's height based on how many
    /// UITableViewCell plus headers the inner UITableView has
    var superCellHeight: CGFloat!
    
    private var fact: Fact!
    private var rootViewController: UIViewController!
    
    private var events = [EventHolder]() {
        didSet {
            /// We gather all tickets across the various events
            /// just to adjust the UITableViewCell in the previous VC
            var tickets = [List<Ticket>]()
            for e in events {
                tickets.append(e.tickets)
            }
            tableView.reloadData()
            superCellHeight = CGFloat(tickets.flatMap{ $0 }.count * cellHeight) + (CGFloat(events.count) * CGFloat(headerHeight))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource        = self
        tableView.delegate          = self
        tableView.bounces           = false
        tableView.scrollEnabled     = false
        tableView.separatorStyle    = .None
        
        tableView.registerNib(UINib(nibName: "TicketTableViewCell", bundle: nil), forCellReuseIdentifier: CELL_TICKET)
        tableView.registerNib(UINib(nibName: "TicketTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: CELL_TICKET_HEADER)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureWithFact(fact: Fact, rootViewController: UIViewController) {
        self.rootViewController = rootViewController
        
        self.getTickets(fact)
    }
    
    func getTickets(fact: Fact){
        /// We should always re create the data source before use it
        self.fact = fact
        events = [EventHolder]()
        EventSectionEntityManager.getEventsSectionAvaliable(factId: fact.id) { (eventsSection) in
            if !eventsSection.isEmpty {
                for event in eventsSection {
                    let e = EventHolder(name: event.name, tickets: event.tickets)
                    self.events.append(e)
                }
            }
        }
    }
    
}

extension TicketsListTableViewCell_ : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return events.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events[section].tickets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_TICKET) as! TicketTableViewCell
        
        cell.backgroundColor = isOneMarketplace ?
            LayoutManager.backgroundDark : LayoutManager.white
        
        cell.configureWithTicket(events[indexPath.section].tickets[indexPath.row])
        return cell
    }
}

extension TicketsListTableViewCell_ : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(cellHeight)
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier(CELL_TICKET_HEADER) as! TicketTableViewHeader
        header.configureWithEventSectionTitle(events[section].name)
        return header
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(headerHeight)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let fact = self.fact {
            let buyTicketsEventViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(BUY_TICKETS_EVENT_VIEWCONTROLLER) as! BuyTicketsEventViewController
            
            if buyTicketsEventViewController.preShowWithFact(byId: fact.id.description) {
                buyTicketsEventViewController.ticketId =
                    events[indexPath.section].tickets[indexPath.row].id
                rootViewController.navigationController?.pushViewController(buyTicketsEventViewController, animated: true)
            } else {
                /// If fact could not be found we should not let user see the tickets.
            }
        }
    }
}
