//
//  ScanCodeTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 6/30/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ScanCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var button  : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.button.setNeedsLayout()
        self.button.layoutIfNeeded()
        
        self.backgroundColor    = LayoutManager.backgroundFirstColor
        let size                = button.frame.size.height
        let imgIcon             = UIImageView(frame: CGRectMake(0, 0, size, size))
        imgIcon.center          = button.titleLabel!.center
        imgIcon.image           = UIImage(named: "iconQr")
        imgIcon.contentMode     = .ScaleToFill
        
        self.button.titleLabel?.font = LayoutManager.primaryFontWithSize(14)
        self.button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        UIGraphicsBeginImageContextWithOptions(imgIcon.bounds.size, false, UIScreen.mainScreen().scale)
        imgIcon.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        let img = image
        
        self.button.setImage(img, forState: .Normal)
        button.tintColor = SessionManager.sharedInstance.cardColor
        button.transform = CGAffineTransformMakeScale(1.0, -1.0);
        button.titleLabel!.transform = CGAffineTransformMakeScale(1.0, -1.0);
        button.imageView!.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
        self.selectionStyle     = .None;

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, fact : Fact, styleDark : Bool = false) -> ScanCodeTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(SCAN_CODE_IDENTIFIER) as! ScanCodeTableViewCell
                
        return cell
    }

    
}
