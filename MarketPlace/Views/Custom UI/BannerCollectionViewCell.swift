//
//  BanneCollectionViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageBanner            : UIImageView!
    @IBOutlet weak var labelTitle             : UILabel!
    @IBOutlet weak var labelAuthor            : UILabel!
    @IBOutlet weak var labelRangeTime         : UILabel!
    @IBOutlet weak var viewContainer          : UIView!
    @IBOutlet weak var viewContainerEvent     : UIView!
    @IBOutlet weak var labelDay               : UILabel!
    @IBOutlet weak var labelMonth             : UILabel!
    @IBOutlet weak var labelTime              : UILabel!
    @IBOutlet weak var iamgeViewMask: UIImageView!
    
    var gradientMask : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        labelTitle.font            = LayoutManager.primaryFontWithSize(17)
        labelTitle.textColor       = UIColor.whiteColor()
        labelAuthor.font           = LayoutManager.primaryFontWithSize(14)
        labelAuthor.textColor      = UIColor.whiteColor()
        labelMonth.font            = LayoutManager.primaryFontWithSize(13)
        labelMonth.textColor       = LayoutManager.regularFontColor
        labelTime.font             = LayoutManager.primaryFontWithSize(13)
        labelTime.textColor        = LayoutManager.regularFontColor
        labelRangeTime.font        = LayoutManager.primaryFontWithSize(14)
        labelRangeTime.textColor   = UIColor.whiteColor()
        labelDay.font              = LayoutManager.primaryFontWithSize(50)
        viewContainerEvent.backgroundColor = SessionManager.sharedInstance.cardColor.colorWithAlphaComponent(0.8)
        imageBanner.contentMode     = .ScaleAspectFill
    }
    
    func getConfiguredFactCell(fact : Fact?, showDetailsBanner : Bool ,styleDark : Bool = false){
        
        self.backgroundColor = UIColor.clearColor()

        var url = ""
        
        if fact != nil {
            //Check if it should download thumb or externalThumb
            if fact!.thumb != nil {
                if fact!.thumb!.characters.count > 0{
                    url = fact!.thumb!
                }
            }
        }
        
        self.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imageBanner.contentMode      = .ScaleToFill
            }
        }
        
        if showDetailsBanner == true {
            
            self.labelRangeTime.text = "de \(fact!.getFormatedDateString(true, pattern: "dd/MM")) a \(fact!.getFormatedDateString(false, pattern: "dd/MM/yyyy"))"
            self.labelTitle.text = fact!.title
            
            self.viewContainer.hidden       = false
            self.viewContainerEvent.hidden  = true
        }else{
            
            self.viewContainer.hidden           = true
            self.viewContainerEvent.hidden      = false
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            
            self.labelDay.text                  = fact!.getFormatedDateString(true, pattern: "dd")
            self.labelTime.text                 = "às \(fact!.getFormatedDateString(true, pattern: "HH:mm"))hs".uppercaseString
            self.labelMonth.text                = months[Int(fact!.getFormatedDateString(true, pattern: "MM"))!-1].uppercaseString
        }
    }
    
    func getConfiguredInfoCell(url : String, showDetailsBanner : Bool ,styleDark : Bool = false){
        
        self.backgroundColor = UIColor.clearColor()
        self.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imageBanner.contentMode      = .ScaleToFill
            }
        }
        
        self.labelTitle.hidden = true
        self.viewContainerEvent.hidden  = true
        self.viewContainer.hidden           = true
        if showDetailsBanner == false {
            self.iamgeViewMask.hidden = true
        } else {
            self.iamgeViewMask.hidden = false
        }
    }
    
    func getConfiguredUnityCell(unity : Unity?, showDetailsBanner : Bool ,styleDark : Bool = false){
        
        var url = ""
        if unity != nil {
            
            if unity!.getHomeBanner() != nil {
                url = unity!.getHomeBanner()!.value!
            } else if unity!.getLogo() != nil {
                if unity!.getLogo()!.value!.characters.count > 0{
                    url = unity!.getLogo()!.value!
                }
            }
        }
        
        self.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imageBanner.contentMode      = .ScaleToFill
            }
        }
        
        if showDetailsBanner == true {
            self.viewContainer.hidden       = false
            self.viewContainerEvent.hidden  = true
        }else{
            self.labelTitle.hidden          = true
            self.viewContainerEvent.hidden  = true
            self.viewContainer.hidden       = true
            self.iamgeViewMask.hidden       = true
        }
    }
    
    func getConfiguredGroupCell(group : Group?, showDetailsBanner : Bool ,styleDark : Bool = false){
        
        var url = ""
        
        if let group = group {
            if let thumb = group.thumb {
                url = thumb.characters.count > 0 ? thumb : ""
            }
        }
        
        self.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                self.imageBanner.contentMode      = .ScaleToFill
            }
        }
        
        if showDetailsBanner == true {
            self.viewContainer.hidden       = false
            self.viewContainerEvent.hidden  = true
        }else{
            self.labelTitle.hidden          = true
            self.viewContainerEvent.hidden  = true
            self.viewContainer.hidden       = true
            self.iamgeViewMask.hidden       = true
        }
    }
}
