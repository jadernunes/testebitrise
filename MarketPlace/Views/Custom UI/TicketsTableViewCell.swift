//
//  TicketsTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/25/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TicketsTableViewCell: UITableViewCell, UIAlertViewDelegate {

    @IBOutlet weak var viewBuy      : UIView!
    @IBOutlet weak var labelHour    : UILabel!
    @IBOutlet weak var labelType    : UILabel!
    @IBOutlet weak var labelPrice   : UILabel!
    var superViewController         : MovieDetailsViewController!
    var movie                       :Movie!
    var session                     :MovieSession!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewBuy.layer.cornerRadius = 6
        viewBuy.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        viewBuy.layer.borderWidth  = 1
        
        self.backgroundColor       = UIColor.clearColor()
        self.selectionStyle        = .None
        
        labelHour.font      = LayoutManager.primaryFontWithSize(20)
        labelHour.textColor = UIColor.whiteColor()
        labelPrice.font     = LayoutManager.primaryFontWithSize(20)
        labelPrice.textColor = UIColor.whiteColor()
        labelType.font     = LayoutManager.primaryFontWithSize(18)
        labelType.textColor = UIColor.whiteColor()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(TicketsTableViewCell.buyTicket))
        viewBuy.addGestureRecognizer(tap)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - BYE Ticket
    func buyTicket() {
        
        if CAN_BYE_TIKET == true {
            var isOpen4allPaymentMode = true
            if URL_MOVIE != nil {
                if URL_MOVIE!.characters.count > 0 {
                    if UIApplication.sharedApplication().canOpenURL(NSURL(string: URL_MOVIE!)!) {
                        isOpen4allPaymentMode = false
                        UIApplication.sharedApplication().openURL(NSURL(string: URL_MOVIE!)!)
                    }
                }
            }
            
            if isOpen4allPaymentMode == true && self.session.availableTickets.value > 0 {
                let dataSendToByTikets = [
                    "movie":self.movie,
                    "movie":self.session
                ]
                
                self.superViewController.performSegueWithIdentifier("segueGotoChouseTickets", sender: dataSendToByTikets)
            }
        } else {
            self.showMessageByeTicketLoked()
        }
    }
    
    private func showMessageByeTicketLoked() {
        let alert = UIAlertView(title: kAlertBuyTicketTitle, message: kMessageTicketsSoonInApp, delegate: self, cancelButtonTitle: kAlertButtonOk)
        alert.show()
    }
}
