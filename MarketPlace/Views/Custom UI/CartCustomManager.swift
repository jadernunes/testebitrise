//
//  CartCustomManager.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift
class CartCustomManager {
    
    static let sharedInstance = CartCustomManager()
    var cartView: CartCustom!
    
    init() {
        self.cartView = CartCustom.instanceFromNib()
    }
    
    func showCart(viewController: UIViewController) {
        if shouldShowCart() {
            self.cartView.initCart()
            let order = OrderEntityManager.sharedInstance.getCart(0)! as Order
            let idUnity = order.idUnity
            var unityCart = OrderEntityManager.sharedInstance.getOrderUnity(idUnity)
            if unityCart != nil {
                SessionManager.sharedInstance.cartActive = true
                self.cartView.setCart((unityCart?.name)!, qtt: String(checkNumberOfItemsInCart()),rootVc:viewController)
//                viewController.navigationController!.view.addSubview(self.cartView)
//                viewController.navigationController!.view.bringSubviewToFront(self.cartView)
//                self.cartView.animateIn()
            }
            
            else {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(order)
                }
                
            }
        }
    }
    
    func shouldShowCart() -> Bool {
        let cart = OrderEntityManager.sharedInstance.getCart(0)
        if cart != nil  {
            let items = (cart! as Order).orderItems.count
            if items > 0 && cartView.isVisible == false {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }
    
    func hideCart() {
        SessionManager.sharedInstance.cartActive = false
        self.cartView.animateOut()
    }
    
    func updateCart() {
        let qtt = checkNumberOfItemsInCart()
        
        if qtt == 1 {
            self.cartView.labelQuantity.text = "\(qtt) " + kItem
        }
        else {
            self.cartView.labelQuantity.text = "\(qtt) " + kItems
        }
        
        if let cart = OrderEntityManager.sharedInstance.getCart(0) {
            if let unity = UnityPersistence.getUnityByID("\(cart.idUnity)") {
                self.cartView.labelUnityName.text = unity.name
            }
        }
    }
    
    func checkNumberOfItemsInCart() -> Int {
        var quantity = 0
        if OrderEntityManager.sharedInstance.getCart(0) != nil {
            let items = OrderEntityManager.sharedInstance.getCart(0)!.orderItems
            for item in items {
                quantity = quantity + Int(item.quantity)
            }
        }
        return quantity
    }
    
}
