//
//  GridTableViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

class GridTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectView      : UICollectionView!
    @IBOutlet weak var labelTitle       : UILabel!
    @IBOutlet weak var imageLine        : UIImageView!
    @IBOutlet weak var imageBanner      : UIImageView!
    @IBOutlet weak var constraintBannerHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    var arrContents         = NSArray()
    var rootViewController  : HomeTableViewController!
    var typeCell            : String!
    var rootGroup           : Group?
    var selectedGroup       : Group!
    var colorBg             : UIColor?
    var colorFont           : UIColor?
    var elevation           : Int = 0
    var factBanner          : Fact?
    var id                  : Int = 0
    var screenConfig        : ScreenConfig!
    var indexPaths          = [NSIndexPath]()
    
    //MARK: - Static variables control heigth sections home
    static let offsetEstimateRow    = CGFloat(40)
    static let offsetPaddingCell    = CGFloat(20)
    static let offsetFixedPadding   = CGFloat(4.5)
    static let offsetWidthScreen    = CGFloat(23)
    let offsetHeightSection         = CGFloat(40)
    let spaceItems                  = CGFloat(10)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.collectView.setNeedsLayout()
        self.collectView.layoutIfNeeded()
        
        self.collectView.registerNib(UINib(nibName: "GridCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CELL_COLLECTION_GRID_HOME)
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        layout.scrollDirection = .Vertical
        collectView.scrollEnabled = false
        collectView.collectionViewLayout = layout
        imageBanner.hidden = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        //Below this line, we got this super duper code that solves my two days search problem.. ;)
        collectView.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: Methods
    
    /**
     This method configures the cell
     - Parameter data: object from the configuration json
     */
    func configureLayout(data : Dictionary<String, AnyObject>){
        
        collectView.contentInset = UIEdgeInsetsZero
        fillContentArray(data)
        
        if let id = data["id"] as? Int {
            self.id = id
        }
        
        if let sectionBg = data["screenConfig"]!["colorBg"] as? String {
            colorBg = Util.hexStringToUIColor(sectionBg)
        }
        
        if let sectionFontColor = data["screenConfig"]!["colorFont"] as? String {
            colorFont = Util.hexStringToUIColor(sectionFontColor)
        }
        
        if let elevationConfig = data["screenConfig"]!["elevation"] as? Int {
            elevation = elevationConfig
        }
        
        self.labelTitle.text            = String(data["title"]!).uppercaseString
        
        labelTitle.font                 = LayoutManager.primaryFontWithSize(14)
        labelTitle.textColor            = colorFont
        imageLine.backgroundColor       = SessionManager.sharedInstance.cardColor
        self.backgroundColor            = colorBg
        self.selectionStyle = .None;
        
        imageBanner.layer.cornerRadius = 6.0
        
        if let factPromo = data["banner"] as? Dictionary<String, AnyObject>{
            if let idFact = factPromo["idFact"] as? NSNumber{
                let realm = try! Realm()
                let results = realm.objects(Fact.self).filter(String(format: "id = \(idFact.integerValue)"))
                if results.count > 0 {
                    factBanner = results.first
                    constraintBannerHeight.constant = 140
                    var url = ""
                    
                    if factBanner!.thumb != nil {
                       url = factBanner!.thumb!
                    }
                    
                    imageBanner.hidden = false
                    
                    imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
                        if image != nil {
                            self.imageBanner.contentMode = .ScaleAspectFill
                        }
                    }

                }else{
                    factBanner = nil
                }
            }else{
                factBanner = nil
            }
            
        }else{
            factBanner = nil
        }
        
        if factBanner == nil {
            imageBanner.hidden = true
            constraintBannerHeight.constant = 0
        }
        
        let tapGestureOnBannerImage = UITapGestureRecognizer(target: self, action: #selector(GridTableViewCell.showPromoFromBanner(_:)))
        self.imageBanner.userInteractionEnabled = true
        self.imageBanner.addGestureRecognizer(tapGestureOnBannerImage)
        
        dispatch_async(dispatch_get_main_queue()) { 
            self.collectView.reloadData()
        }
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func fillContentArray(data : Dictionary<String, AnyObject>){
        var allContent : Bool? = false
        if let showAllContent = data["screenConfig"]!["allContent"] as? Bool{
            allContent = showAllContent
            
            if allContent == true {
                if let groups = data["content"]!["groupTrees"] as? Array<Dictionary<String, AnyObject>>{
                    if groups.count > 0 {
                        rootGroup = Group(value: groups[0])
                        self.arrContents = GroupEntityManager.getGroupsChildren(rootGroup!.id)
                    }
                }
            }
        }
        
        if allContent == false {
            if let groups = data["content"]!["groupTrees"] as? Array<Dictionary<String, AnyObject>> {
                self.arrContents = NSMutableArray(array: groups)
            }
        }
        
        if let contentType = data["screenConfig"]!["contentTypeName"] as? String{
            typeCell = contentType
        }else{
            typeCell = ""
        }

        dispatch_async(dispatch_get_main_queue()) {
            self.collectView.reloadData()
        }
    }
    
    //MARK: - Height for section
    class func getHeightForSection(section : Dictionary<String, AnyObject>) -> CGFloat {
        
        var heightCell : CGFloat = 65
        
        if let factPromo = section["banner"] as? Dictionary<String, AnyObject>{
            if let idFact = factPromo["idFact"] as? NSNumber{
                let realm = try! Realm()
                let results = realm.objects(Fact.self).filter(String(format: "id = \(idFact.integerValue)"))
                if results.count > 0 {
                    heightCell += 170
                }
            }
        }
            
        if let groups = section["content"]!["groupTrees"] as? Array<Dictionary<String, AnyObject>>{
            if groups.count > 0 {
                let groupWithChildren = Group(value: groups[0])
                let countItems = GroupEntityManager.getGroupsChildren(groupWithChildren.id).count
                
                var valueToAddToPadding : CGFloat = 00.0
                let deviceModel = UIDevice().getDeviceModel()
                
                if deviceModel == .iPhone5      ||
                    deviceModel == .iPhone5s    ||
                    deviceModel == .iPhone5c    ||
                    deviceModel == .iPhoneSE
                {
                    valueToAddToPadding = offsetPaddingCell
                }
                
                let screenConfig = ScreenConfig(value: section["screenConfig"]!)
                let padding      = UIScreen.mainScreen().bounds.size.width / offsetFixedPadding + valueToAddToPadding
                let numberOfRows = ceil(Double(countItems) / Double(screenConfig.columns))

                //calculate estimated cellRow
                //Get screen width less estimated margins
                let estimatedRow = ((UIScreen.mainScreen().bounds.size.width - offsetWidthScreen) / CGFloat(screenConfig.columns) + offsetEstimateRow)
                
                let sectionHeight =  CGFloat(numberOfRows * Double(estimatedRow))
                return (sectionHeight + CGFloat(padding))
            }
        }
        
        return 0
    }
    
    //MARK: - Navigation
    func callList(group: Group) -> Void{
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        listVc.group = group
        listVc.title = group.name        
        if group.groupTreeTypeName != nil {
            listVc.subType = SubContentType(rawValue: group.groupTreeTypeName!)
        }
        
        rootViewController.navigationController?.pushViewController(listVc, animated: true)
    }
    
    func callDetails(unity: Unity?, group: Group?) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        detailsVc.title = group?.name
        detailsVc.idObjectReceived = group?.id
        detailsVc.typeObjet = TypeObjectDetail.Group
        rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    @IBAction func showPromoFromBanner(sender : AnyObject) -> Void{
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        detailsVc.title = factBanner?.title
        detailsVc.idObjectReceived = factBanner?.id
        detailsVc.typeObjet = TypeObjectDetail.Promotion
        rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
    }
    
    //MARK: UICollection DataSource & Delegate

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        let columns = screenConfig != nil ? Double(screenConfig.columns) + 0.1 : 1
        let size = ((UIScreen.mainScreen().bounds.size.width - (10 + (CGFloat(10) * CGFloat(columns)))) / CGFloat(columns))
        
        return CGSizeMake(size , size + offsetHeightSection)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if let group = arrContents.objectAtIndex(indexPath.row) as? Group {
            selectedGroup = group
        }else if let group = arrContents.objectAtIndex(indexPath.row) as? Dictionary<String, AnyObject> {
            selectedGroup = Group(value: group)
        }
        
        selectedGroup.loadDataFromRealm()
        
        if selectedGroup.children.count > 0 || selectedGroup.unities.count > 0{
            callList(selectedGroup)
        }else{
            callDetails(nil, group: selectedGroup)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return spaceItems
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrContents.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let customCell = self.collectView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_GRID_HOME, forIndexPath: indexPath) as! GridCollectionViewCell
        
        let item : Group
        if let obj = arrContents.objectAtIndex(indexPath.row) as? Dictionary<String, AnyObject>{
            item = Group(value: obj)
        }else{
            item = Group(value:arrContents.objectAtIndex(indexPath.row))
        }
        if elevation == 0 {
            customCell.containerView.backgroundColor = UIColor.clearColor()
        }
        customCell.labelTitle.text = item.name
        customCell.labelTitle.font = LayoutManager.primaryFontWithSize(13)
        customCell.labelTitle.textColor = colorFont
        var url = ""
        if item.thumb != nil {
            url = item.thumb!
        }
        
        customCell.imageIcon.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                customCell.imageIcon.contentMode = .ScaleAspectFill
            }
        }

        return customCell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if !self.rootViewController.indexPathsGrid.contains(indexPath) {
            self.rootViewController.indexPathsGrid.append(indexPath)
            Util.animateGridCell(cell, indexPath: indexPath, bounceSize: 1.15)
        }
    }
}
