//
//  ParkingTableViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ParkingTableViewCell: UITableViewCell {

    @IBOutlet weak var rootVc           : UIViewController!
    @IBOutlet weak var buttonWherePark  : UIButton!
    @IBOutlet weak var buttonPayPark    : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        buttonWherePark.titleLabel!.font = LayoutManager.primaryFontWithSize(13)
        buttonPayPark.titleLabel!.font   = LayoutManager.primaryFontWithSize(13)
        
        buttonWherePark.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.6).CGColor
        buttonWherePark.layer.borderWidth = 0.5
        
        buttonPayPark.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.6).CGColor
        buttonPayPark.layer.borderWidth = 0.5
        
        dispatch_async(dispatch_get_main_queue()) { 
            //Set WherePark Icon
            self.buttonWherePark.setIcon(UIImage(named: "iconPark")!, inLeft: true)
            self.buttonPayPark.setIcon(UIImage(named: "iconPayPark")!, inLeft: true)
        }
        
        self.selectionStyle     = .None;
    }
    
    @IBAction func actionOndeParei(sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("OndePareiVC")
        
        rootVc.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func payPark(sender: UIButton) {
        let st = UIStoryboard(name:"Parking Storyboard", bundle: nil)
        let vc = st.instantiateViewControllerWithIdentifier("parkingVC")
        self.rootVc.navigationController?.pushViewController(vc, animated: true)
    }
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Methods
    
}
