//
//  OrderHomeTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class OrderHomeTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionView   : UICollectionView!
    var viewController                  = UIViewController()
    var cartTintColor                   = UIColor.blackColor()
    
    var arrayOrders = NSArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.registerNib(UINib(nibName: "OrderHomeCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: COLLECTION_CELL_ORDER_HOME)
        self.collectionView.delegate    = self
        self.collectionView.dataSource  = self
        self.collectionView.backgroundColor = UIColor.clearColor()
        self.backgroundColor = LayoutManager.backgroundDark
        
        addObserverToCheckOrdersStatus()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        //Below this line, we got this super duper code that solves my two days search problem.. ;)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: Methods
    private func addObserverToCheckOrdersStatus(){
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(OrderHomeTableViewCell.reloadOrdersStatus),
            name: NOTIF_HOME_CART_OBSERVER,
            object: nil)
    }
    
    @objc private func reloadOrdersStatus() {
        arrayOrders = OrderEntityManager.sharedInstance.getOrders("status <> \(StatusOrder.Canceled.rawValue) AND status <> \(StatusOrder.Delivered.rawValue) AND status <> \(StatusOrder.Open.rawValue) AND idOrderType <> \(OrderType.Voucher.rawValue)")
        collectionView.reloadData()
    }
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return collectionView.frame.size
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOrders.count;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(COLLECTION_CELL_ORDER_HOME, forIndexPath: indexPath) as! OrderHomeCollectionViewCell
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let order = arrayOrders.objectAtIndex(indexPath.row) as! Order
        let customCell = cell as! OrderHomeCollectionViewCell
        customCell.imageViewCart.tintImage(cartTintColor)
        customCell.labelOrderNumber.text = "Pedido - \(order.getIdentificator())"
        customCell.labelStatus.text = order.getTextStatus()
        
        customCell.labelStatus.textColor = LayoutManager.labelFirstColor
        customCell.labelOrderNumber.textColor = LayoutManager.labelFirstColor
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let order = arrayOrders.objectAtIndex(indexPath.row) as! Order
        let destVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TrackingViewController") as! TrackingViewController
        destVc.title = UnityPersistence.getUnityByID(String(order.idUnity))!.name
        destVc.order = order
        destVc.hidesBottomBarWhenPushed = true
        if !isOneMarketplace {
            if let vc = viewController as? ListHomeViewController {
                vc.showNavBar()
            }
        }
        viewController.navigationController!.pushViewController(destVc, animated: true)
    }
}
