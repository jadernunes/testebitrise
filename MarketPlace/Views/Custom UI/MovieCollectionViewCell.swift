//
//  MovieCollectionViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/8/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imagePoster: UIImageView!
    @IBOutlet weak var labelHours: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelHours.font = LayoutManager.primaryFontWithSize(14)
        
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        LayoutManager.roundTopCornersRadius(imagePoster, radius: 6)
        
        self.contentView.layer.cornerRadius = 6.0
        self.contentView.layer.masksToBounds = true;
        self.contentView.backgroundColor = UIColor.whiteColor()
        self.layer.shadowColor = UIColor.blackColor().colorWithAlphaComponent(0.6).CGColor
        self.layer.shadowOffset = CGSizeMake(0, 1.0)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false;
        self.backgroundColor = UIColor.clearColor()
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).CGPath;
        
    }

}
