//
//  OrderHomeCollectionViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 9/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class OrderHomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelOrderNumber : UILabel!
    @IBOutlet weak var labelStatus      : UILabel!
    @IBOutlet weak var imageViewCart: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        initialConfiguration()
    }
    
    func initialConfiguration() {
        
        labelOrderNumber.font       = LayoutManager.primaryFontWithSize(14)
        labelOrderNumber.textColor  = UIColor.whiteColor()
        
        labelStatus.font            = LayoutManager.primaryFontWithSize(14)
        labelStatus.textColor       = UIColor.whiteColor()
        imageViewCart.tintImage(SessionManager.sharedInstance.cardColor)
    }

}
