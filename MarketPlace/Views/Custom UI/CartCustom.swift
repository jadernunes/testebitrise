//
//  CartCustom.swift
//  MarketPlace
//
//  Created by Matheus Luz on 9/2/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CartCustom: UIView {

    @IBOutlet weak var imageViewArrow: UIImageView!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var imageCart: UIImageView!
    @IBOutlet weak var labelUnityName: UILabel!
    
    
    var rootViewController: UIViewController?
    var isVisible = false
    var mutexLock : Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instanceFromNib() -> CartCustom {
        return UINib(nibName: "CartCustom", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! CartCustom
    }
    
    func setCart(name: String, qtt: String, rootVc: UIViewController) {
        
        self.labelQuantity.textColor = LayoutManager.white
        self.labelQuantity.font = LayoutManager.primaryFontWithSize(18)
        self.labelUnityName.font             = LayoutManager.primaryFontWithSize(18)
        self.labelUnityName.textColor        = LayoutManager.white
        self.backgroundColor = LayoutManager.greyCart

        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0, bounds.size.width, 0.5)
        topBorder.backgroundColor = SessionManager.sharedInstance.cardColor.CGColor
        self.layer.addSublayer(topBorder)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(openCartView))
        self.addGestureRecognizer(tap)
        
        if Int(qtt) == 1 {
            labelQuantity.text = "\(qtt) item"
        }
        else {
            labelQuantity.text = "\(qtt) itens"

        }
        labelUnityName.text = name
        rootViewController = rootVc

        self.imageCart.tintImage(LayoutManager.white)
        self.imageViewArrow.tintImage(LayoutManager.white)
        self.imageViewArrow.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
    }
    
    func initCart() {
        let deviceBounds = UIScreen.mainScreen().bounds
        self.frame = CGRectMake(0, deviceBounds.height, deviceBounds.width, 64)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func animateIn() {
        self.mutexLock = true
        UIView.animateWithDuration(0.75, delay: 0, options: [.CurveEaseInOut], animations: {
            let deviceBounds = UIScreen.mainScreen().bounds
            self.frame = CGRectMake(0, deviceBounds.height-64, deviceBounds.width, 64)
            self.layoutIfNeeded()
            }, completion: {(didEnd) in
                self.mutexLock = false
        })
        isVisible = true
    }
    
    func animateOut() {
        UIView.animateWithDuration(0.75, delay: 0, options: [.CurveEaseInOut], animations: {
            let deviceBounds = UIScreen.mainScreen().bounds
            self.frame = CGRectMake(0, deviceBounds.height, deviceBounds.width, 64)
            self.layoutIfNeeded()
            }, completion: { (didEnd) in
                if self.mutexLock == false {
                    self.removeFromSuperview()
                }
        })
        isVisible = false
    }
    
    func tappedCartView(sender: UIButton) {
        openCartView()
    }
    
    //MARK: - Methods Show Cart
    func openCartView() {
        if SessionManager.sharedInstance.searchOrQrEnabled == false {
            if let topVC = UIApplication.topViewController() {
                openCartFromViewController(topVC)//HomeTableViewController.rootVc)
            }
        }
        else {
            openCartFromViewController(rootViewController!)
        }
    }
    
    func openCartFromViewController(viewController: UIViewController) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let cartVc = mainStoryboard.instantiateViewControllerWithIdentifier("CartVC") as! CartViewController
        cartVc.rootViewController = viewController
        viewController.presentViewController(cartVc, animated: true, completion: nil)
    }
}
