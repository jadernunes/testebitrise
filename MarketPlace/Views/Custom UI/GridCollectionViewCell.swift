//
//  GridCollectionViewCell.swift
//  Shopping Total
//
//  Created by Luciano on 6/23/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var imageIcon  : UIImageView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 6
    }

}
