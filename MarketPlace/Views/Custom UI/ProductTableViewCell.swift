//
//  ProductTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 8/5/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class ProductTableViewCell: UITableViewCell, IterativeMessageDelegate {

    @IBOutlet weak var labelName     : UILabel!
    @IBOutlet weak var buttonAddCart : UIButton!
    @IBOutlet weak var viewPicker    : UIView!
    @IBOutlet weak var labelQuantity : UILabel!
    @IBOutlet weak var buttonAdd     : UIButton!
    @IBOutlet weak var buttonRemove  : UIButton!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var iconHourGlass: UIImageView!
    @IBOutlet weak var labelDescription: UILabel!
    
    @IBOutlet weak var constraintHeightlabelDescription: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLabelTitle: NSLayoutConstraint!
    
    var rootVc      : UIViewController!
    var product     : UnityItem!
    var orderItem   : OrderItem?
    var unity       : Unity!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonAddCart.tintColor = SessionManager.sharedInstance.cardColor
        buttonAdd.tintColor     = SessionManager.sharedInstance.cardColor
        buttonRemove.tintColor  = SessionManager.sharedInstance.cardColor
    }
        
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func removeItem(sender : UIButton) {
        if OrderEntityManager.sharedInstance.removeItemFromCart(product, unity: unity) == false {
            Util.showAlert(rootVc, title: kAlertTitle, message: kMessageErrorRemovingFromCart)
        }
        else{
            labelQuantity.text = "\(OrderEntityManager.sharedInstance.getItemQuantity(product, unity: unity))"
            if OrderEntityManager.sharedInstance.getItemQuantity(product, unity: unity) == 0{
                viewPicker.hidden    = true
                buttonAddCart.hidden = false
            }
            if CartCustomManager.sharedInstance.checkNumberOfItemsInCart() == 0 {
                    CartCustomManager.sharedInstance.hideCart()
                NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_CART_CHANGED, object: nil)
            }
            else {
                CartCustomManager.sharedInstance.updateCart()
            }
            
            SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self.rootVc)
        }
    }
    
    @IBAction func addItem(sender : UIButton) {
        do {
            try OrderEntityManager.sharedInstance.addItemToCard(product, unity: unity)
            labelQuantity.text = "\(OrderEntityManager.sharedInstance.getItemQuantity(product, unity: unity))"
    
             let quantity = CartCustomManager.sharedInstance.checkNumberOfItemsInCart()
            if quantity == 1 {
                CartCustomManager.sharedInstance.showCart((rootVc as! ListProductsViewController).referenceToSuperViewController)
                NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_CART_CHANGED, object: nil)
            }
            else {
                CartCustomManager.sharedInstance.updateCart()
            }
            
            SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self.rootVc)
        }catch _ as NSError {
        }
    }

    @IBAction func addCart(sender : UIButton?) {
        do {
            let realm = try! Realm()
            try OrderEntityManager.sharedInstance.addItemToCard(product, unity: unity)
            let cart = realm.objects(Order.self)
                .filter("status = 0")
                .first
            if cart!.orderItems.count == 1{
                CartCustomManager.sharedInstance.showCart(rootVc)
                NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_CART_CHANGED, object: nil)
            }
            CartCustomManager.sharedInstance.updateCart()
            viewPicker.hidden    = false
            buttonAddCart.hidden = true
            labelQuantity.text = "\(Int(labelQuantity.text!)! + 1)"
            SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self.rootVc)
        }catch _ as NSError {
            if OrderEntityManager.sharedInstance.hasActiveOrder() {
                Util.showIterativeMessage(self, typeActions: ActionIterativeMessage.OneOption, tag: 0, message: "Você possui um pedido ativo!\nAguarde para fazer novos.", title: kAlertTitle)
            } else {
                Util.showIterativeMessage(self, typeActions: ActionIterativeMessage.TwoOptions, tag: 0, message: "Já existe produtos no carrinho. Gostaria de remover?", title: kAlertTitle)
            }
        }
    }
    
    func buttonRefusePressed(sender:IterativeMessageViewController){
        
    }
    
    func buttonConfirmPressed(sender:IterativeMessageViewController){
        if sender.typeActions == ActionIterativeMessage.OneOption {
            return
        }
        OrderEntityManager.sharedInstance.deleteCart()
        self.addCart(nil)
        CartCustomManager.sharedInstance.updateCart()
        SessionManager.sharedInstance.showNavigationButtons(true, isShowSearch: false, viewController: self.rootVc)
    }
}
