//
//  BannerTableViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

class BannerTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //IBOutlets
    @IBOutlet weak var collectView      : UICollectionView!
    @IBOutlet weak var buttonSeeMore    : UIButton!
    @IBOutlet weak var labelTitle       : UILabel!
    @IBOutlet weak var pageControl: UIPageControl!

    //constraints
    @IBOutlet weak var titleHeight  : NSLayoutConstraint!
    @IBOutlet weak var buttonHeight : NSLayoutConstraint!
    
    //Variables
    var rootViewController  : HomeTableViewController!
    var txtTitle            : String?
    var txtButton           : String?
    var arrContents         = NSMutableArray()
    var cellHeight          = 100.0
    var exposedHeight       = 48.0
    var showInfo            = false
    var contentType         = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.collectView.registerNib(UINib(nibName: "BannerCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CELL_COLLECTION_BANNER_HOME)
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //Below this line, we got this super duper code that solve my two days search problem.. ;)
        collectView.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: - Page control
    
    @IBAction func pageControlPressIndex(sender: UIPageControl) {
        let indexPath = NSIndexPath.init(forRow: sender.currentPage, inSection: 0)
        self.collectView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
    }
    
    //MARK: Methods
    
    /// This method configures the cell
    /// :param: configuration object from the json
    
    func initialConfiguration(data : Dictionary<String, AnyObject>, rootVc : HomeTableViewController!){
        
        self.backgroundColor = LayoutManager.backgroundLight
        self.rootViewController = rootVc
        
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
        
        if let contentTypeName = data["screenConfig"]!["contentTypeName"] as? String {
            if contentTypeName == "event" {
                self.labelTitle.text         = String(data["title"]!).uppercaseString
                self.labelTitle.hidden       = false
                self.titleHeight.constant    = 48
                self.bringSubviewToFront(labelTitle)
            } else{
                self.labelTitle.hidden       = true
                self.titleHeight.constant    = 0
            }
        }
        
        if let seeMore = data["screenConfig"]!["seeMore"] as? Bool{
            
            if seeMore == true {
                self.showInfo                = true
                
                if let title = data["screenConfig"]!["seeMoreTitle"] as? String{
                    self.buttonSeeMore.setTitle(title.uppercaseString, forState: .Normal)
                }else{
                    self.buttonSeeMore.setTitle(kSeeAll, forState: .Normal)
                }
                
                self.buttonSeeMore.hidden    = false
                self.buttonHeight.constant   = 48
                
                self.bringSubviewToFront(buttonSeeMore)
                
            }else{
                self.showInfo                = false
                self.buttonSeeMore.hidden    = true
                self.buttonHeight.constant   = 0
            }
        }else{
            self.showInfo                = false
            self.labelTitle.hidden       = true
            self.buttonSeeMore.hidden    = true
            self.buttonHeight.constant   = 0
            self.titleHeight.constant    = 0
        }
        
        if let sectionBg = data["screenConfig"]!["colorBg"] as? String {
            self.backgroundColor = Util.hexStringToUIColor(sectionBg)
        }
        
        if let arrContents = data["content"]!["promos"] as? NSArray{
            let listContents = NSMutableArray.init(array: arrContents)
            self.arrContents = listContents
            self.contentType = "promo"
        }else if let arrContents = data["content"]!["events"] as? NSArray{
            let newListContents = NSMutableArray()
            
            for obj in arrContents {
                let timeEnd = NSTimeInterval(obj["timestampEnd"] as! Double) * 1000
                let active = obj["active"] as! Bool
                let today = NSDate.timeIntervalNow()
                
                if timeEnd >= today && active == true {
                    newListContents.addObject(obj)
                }
            }
            
            self.arrContents = newListContents
            self.contentType = "event"
        }else if let arrContents = data["content"]!["infos"] as? NSArray {
            let listContents = NSMutableArray.init(array: arrContents)
            self.arrContents = listContents
            self.contentType = "info"
        }else if let arrContents = data["content"]!["unities"] as? NSArray {
            let listContents = NSMutableArray.init(array: arrContents)
            self.arrContents = listContents
            self.contentType = "unities"
        }else if let arrContents = data["content"]!["groupTrees"] as? NSArray {
            let listContents = NSMutableArray.init(array: arrContents)
            self.arrContents = listContents
            self.contentType = "groupTrees"
        }
        
        if let showAllContent = data["screenConfig"]!["allContent"] as? Bool{
            if showAllContent == true {
                if contentType == "event"{
                    self.arrContents = FactEntityManager.getFacts(FACT_TYPE_EVENT)
                } else if contentType == "promo"{
                    self.arrContents = FactEntityManager.getFacts(FACT_TYPE_PROMO)
                }
            }
        }
        
        self.collectView.pagingEnabled = true
        self.collectView.contentInset = UIEdgeInsetsZero
        self.backgroundColor = LayoutManager.backgroundLight
        labelTitle.font      = LayoutManager.primaryFontWithSize(16)
        labelTitle.textColor = LayoutManager.regularFontColor
        buttonSeeMore.setTitleColor(LayoutManager.regularFontColor, forState: .Normal)
        buttonSeeMore.titleLabel?.font = LayoutManager.primaryFontWithSize(16)
        buttonSeeMore.setImage(Ionicons.IosArrowRight.image(20), forState: .Normal)
        buttonSeeMore.tintColor = SessionManager.sharedInstance.cardColor
        buttonSeeMore.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonSeeMore.titleLabel!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonSeeMore.imageView!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonSeeMore.addTarget(self, action: #selector(BannerTableViewCell.seeAllItems(_:)), forControlEvents: .TouchUpInside)
        self.selectionStyle = .None;
        self.clipsToBounds  = true
        self.collectView.setContentOffset(CGPointZero, animated: false)
        self.collectView.reloadData()

    }
    
    func seeAllItems(sender : UIButton) -> Void{
    
        callList()
    }
    
    //MARK: - Navigation
    func callList() -> Void{
        let listVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        
        if contentType == "promo" {
            listVc.subType = .Promo
        } else if contentType == "event" {
            listVc.subType = .Event
            listVc.title = labelTitle.text
        } else if contentType == "unities" {
            listVc.subType = .Store
            listVc.title = labelTitle.text
        } else if contentType == "groupTrees" {
            listVc.title = labelTitle.text
        }
        
        rootViewController.navigationController?.pushViewController(listVc, animated: true)
    }
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        return collectView.frame.size
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrContents.count > 1 {
            pageControl.hidden = false
            pageControl.numberOfPages = arrContents.count
        } else {
            pageControl.hidden = true
        }
        
        return arrContents.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_BANNER_HOME, forIndexPath: indexPath) as! BannerCollectionViewCell
        
        if(self.contentType == "info") {
            //Info
            var urlInfo = ""
            if let data = arrContents.objectAtIndex(indexPath.row) as? NSDictionary {
                urlInfo = data.objectForKey("thumb") as! String
            }
            
            cell.getConfiguredInfoCell(urlInfo, showDetailsBanner: false)
        } else if(self.contentType == "unities") {

            let unity = Unity(value: arrContents.objectAtIndex(indexPath.row))
            cell.getConfiguredUnityCell(unity, showDetailsBanner:false)
            
        } else if(self.contentType == "groupTrees"){
            
            let groupDictionary = arrContents.objectAtIndex(indexPath.row) as! NSDictionary
            let idGroup = groupDictionary.objectForKey("id") as! Int
            let group = GroupEntityManager.getGroupById(idGroup)
            
            if(group != nil){
            cell.getConfiguredGroupCell(group, showDetailsBanner:false)
            }
            
        } else {
            var fact : Fact!
            if let item = arrContents.objectAtIndex(indexPath.row) as? Fact {
                fact = item
            }else{
                fact = Fact(value: arrContents.objectAtIndex(indexPath.row))
            }
            
            cell.getConfiguredFactCell(fact, showDetailsBanner: contentType == "promo" ? true : false)
        }
        
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let indexPathSelect = self.collectView.indexPathsForVisibleItems()[0]
        pageControl.currentPage = indexPathSelect.row
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        (cell as! BannerCollectionViewCell).imageBanner.updateConstraints()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if(self.contentType == "promo" || self.contentType == "event") {
            var item : Fact!
            
            if let fact = arrContents.objectAtIndex(indexPath.row) as? NSDictionary {
                item = Fact(value: fact)
            }else{
                item = arrContents.objectAtIndex(indexPath.row) as! Fact
            }
            
            if self.contentType == "promo" {
                callDetails(item,typeObject: TypeObjectDetail.Promotion)
            } else if self.contentType == "event" {
                callDetails(item,typeObject: TypeObjectDetail.Event)
            }
        } else if (self.contentType == "unities"){
            let unity = Unity(value: arrContents.objectAtIndex(indexPath.row))
            callDetails(unity,typeObject: TypeObjectDetail.Store)
        } else if (self.contentType == "groupTrees"){
            
            let groupDictionary = arrContents.objectAtIndex(indexPath.row) as! NSDictionary
            let idGroup = groupDictionary.objectForKey("id") as! Int
            let group = GroupEntityManager.getGroupById(idGroup)
            
            if(group != nil){
                callDetails(group!,typeObject: TypeObjectDetail.Group)
            }
        }
    }
    
    func callDetails(object: AnyObject,typeObject:TypeObjectDetail!) -> Void{
        
        if typeObject == TypeObjectDetail.Group {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
            
            let group = object as! Group
            viewController.group = group
            viewController.title = group.name
            
            rootViewController.navigationController?.pushViewController(viewController, animated: true)
            
        } else {
            let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
            
            if typeObject == TypeObjectDetail.Promotion || typeObject == TypeObjectDetail.Event {
                
                let fact = object as! Fact
                detailsVc.idObjectReceived = fact.id
                detailsVc.title = fact.title
                detailsVc.typeObjet = typeObject
            } else if typeObject == TypeObjectDetail.Store {
                let unity = object as! Unity
                detailsVc.idObjectReceived = unity.id
                detailsVc.title = unity.name
                detailsVc.typeObjet = typeObject
            }
            
            rootViewController.navigationController?.pushViewController(detailsVc, animated: true)
        }
    }
}
