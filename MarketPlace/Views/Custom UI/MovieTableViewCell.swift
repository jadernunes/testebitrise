//
//  MovieTableViewCell.swift
//  Shopping Total
//
//  Created by 4all on 6/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import SDWebImage

class MovieTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    //MARK: Variables
    
    //IBOutlets
    @IBOutlet weak var collectView       : UICollectionView!
    @IBOutlet weak var buttonSeeAll      : UIButton!
    @IBOutlet weak var labelSection      : UILabel!
    @IBOutlet weak var labelDate         : UILabel!
    @IBOutlet weak var labelNextSessions : UILabel!
    @IBOutlet weak var lineSection       : UIImageView!
    @IBOutlet weak var labelMessage: UILabel!
    
    //Variables
    var arrContents = NSMutableArray()
    var rootVc      : UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // Initialization code
        self.collectView.registerNib(UINib(nibName: "MovieCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: CELL_COLLECTION_MOVIE_HOME)
        self.collectView.dataSource     = self
        self.collectView.delegate       = self
        
        let layout = self.collectView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .Horizontal
        
        labelDate.text = "hoje, \(NSDate.day()) de \(Util.getStringMonth(String(NSDate.month()))) de \(NSDate.year())".uppercaseString
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Methods
    
    func configureLayout(data : Dictionary<String, AnyObject>, rootVc : UIViewController){
        
        
        if let cellHeight = data["screenConfig"]!["height"] as? Double {
            self.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, CGFloat(cellHeight))
        }
        
        if let sectionBg = data["screenConfig"]!["colorBg"] as? String {
            self.backgroundColor = Util.hexStringToUIColor(sectionBg)
        }
        
        if let showAllContent = data["screenConfig"]!["allContent"] as? Bool{
            
            if showAllContent == true {
                self.arrContents = MovieEntityManager.getMovies()
                
            }
        }else{
        
            if let arrContents = data["content"]!["moviesWithSession"] as? NSMutableArray{
                self.arrContents = arrContents
            }
        }
        
        if arrContents.count == 0 {
            labelMessage.hidden = false
        }else{
            labelMessage.hidden = true
        }
        
        labelMessage.font = LayoutManager.primaryFontWithSize(16)
        self.rootVc = rootVc
        buttonSeeAll.setTitleColor(LayoutManager.regularFontColor, forState: .Normal)
        buttonSeeAll.setTitleColor(LayoutManager.regularFontColor, forState: .Normal)
        buttonSeeAll.titleLabel?.font = LayoutManager.primaryFontWithSize(16)
        buttonSeeAll.setImage(Ionicons.IosArrowRight.image(20), forState: .Normal)
        buttonSeeAll.tintColor = SessionManager.sharedInstance.cardColor
        buttonSeeAll.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonSeeAll.titleLabel!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonSeeAll.imageView!.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
        labelSection.font       = LayoutManager.primaryFontWithSize(16)
        labelSection.textColor  = LayoutManager.regularFontColor
        
        labelDate.font          = LayoutManager.primaryFontWithSize(18)
        labelDate.textColor     = LayoutManager.regularFontColor
        
        labelNextSessions.font          = LayoutManager.primaryFontWithSize(14)
        labelNextSessions.textColor     = LayoutManager.regularFontColor
        lineSection.backgroundColor = SessionManager.sharedInstance.cardColor

        
        self.backgroundColor = LayoutManager.backgroundLight
        self.selectionStyle = .None;
    }

    @IBAction func showAll(sender: AnyObject) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ListVC") as! ListViewController
        vc.subType = .Movie
        rootVc.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrContents.count;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
     
        //Split the view to display two columns
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let size = CGSizeMake(self.collectView.frame.size.width/2-10, self.collectView.bounds.height-10)

        return size
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let customCell = self.collectView.dequeueReusableCellWithReuseIdentifier(CELL_COLLECTION_MOVIE_HOME, forIndexPath: indexPath) as! MovieCollectionViewCell
        
        var movie : Movie!
        if let obj = arrContents.objectAtIndex(indexPath.row) as? Movie{
            movie = obj
        }else{
            movie = Movie(value:arrContents.objectAtIndex(indexPath.row) as! [String:AnyObject])
        }
        var url = ""
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        //Check if it should download thumb or externalThumb
        if movie?.thumb != nil {
            if movie.thumb!.characters.count > 0 {
                url = movie.thumb!
            }
        }
        var concatItems = ""
        
        if movie.movieSessions.count > 0{
            for sessionObj  in movie.getAvailableSessions(){
                let session = MovieSession(value: sessionObj)
                if session.sessionDatetime.value != 0 {
                    concatItems = concatItems + " - " + session.getFormatedDateString("HH:mm")
                }
            }
            
            if concatItems.characters.count > 0{
                concatItems = concatItems.substringFromIndex(concatItems.startIndex.advancedBy(2))
            }
        }
        
        customCell.labelHours.text = concatItems
                
        customCell.imagePoster.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                customCell.imagePoster.contentMode = .ScaleAspectFill
            }
        }

        return customCell;
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
            
        var movie : Movie
        
        if let obj = arrContents.objectAtIndex(indexPath.row) as? Movie{
            movie = obj
        }else{
            movie = Movie(value:arrContents.objectAtIndex(indexPath.row) as! [String:AnyObject])
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MovieDetailsVC") as! MovieDetailsViewController
        
        vc.movie = movie
        
        rootVc.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
