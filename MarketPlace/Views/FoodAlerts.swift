//
//  FoodAlerts.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 08/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodAlertsDelegate {
    func didCallCloseAlertBallon() -> Void
}

class FoodAlerts : NSObject {
    
    var tagAlertBalloon = 37
    var foodDelegate : FoodAlertsDelegate?
    
    func showHintAt(view:UIView) -> Void {
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPoint(x: 23, y: 21))
        bezierPath.addCurveToPoint(CGPoint(x: 33.94, y: 21), controlPoint1: CGPoint(x: 35.5, y: 21), controlPoint2: CGPoint(x: 33.94, y: 21))
        bezierPath.addLineToPoint(CGPoint(x: 28.47, y: 10))
        bezierPath.addLineToPoint(CGPoint(x: 23, y: 21))
        UIColor.whiteColor().setFill()
        bezierPath.fill()
    }
    
    
    /// Ask user if he wants to turn on geolocation
    func showAlertGeoLocation() {
        let alertController = UIAlertController(title: NSLocalizedString("O seu serviço de geolocalização está desativado.", comment: ""), message: NSLocalizedString("Acesse as configurações do telefone para permitir o acesso à sua localização.", comment: ""), preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Não permitir", comment: ""), style: .Cancel) { (UIAlertAction) in

        }
        let settingsAction = UIAlertAction(title: NSLocalizedString("Configurações", comment: ""), style: .Default) { (UIAlertAction) in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        if let controller = UIApplication.topViewController() {
            controller.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    /// Metodo para adicionar um alerta na controller desejada
    ///
    /// - Parameters:
    ///   - controller: a controller(view) alvo
    ///   - frame: o frame desejado para a view de alerta
    func showViewAlertBalloon(controller: UIViewController, frame: CGRect, tag: Int? = nil) {
        let width = frame.size.width*0.8
        let posX = frame.size.width*0.5 - width*0.5
        let alertView = FoodViewAlertBalloon(frame: CGRect(x: posX, y: 0, width: width, height: 160))
        alertView.foodDelegate = controller as? FoodViewAlertBalloonDelegate
        
        //se passar uma tag, a instancia salva
        if let tag = tag {
            tagAlertBalloon = tag
        }
        
        if controller.view.viewWithTag(tagAlertBalloon) == nil {
            let viewShadow = UIView(frame: frame)
            viewShadow.tag = tagAlertBalloon
            viewShadow.backgroundColor = UIColor.foodGreyishBrown80Color()
            viewShadow.addSubview(alertView)
            viewShadow.alpha = 0.0
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(FoodAlerts.removeViewAlert(_:)))
            viewShadow.addGestureRecognizer(gesture)
            controller.view.addSubview(viewShadow)
            
            UIView.animateWithDuration(0.3, animations: { 
                viewShadow.alpha = 1.0
                }, completion: { (finisherd) in
                    UIView.animateWithDuration(0.5,
                        delay: 0.3,
                        options: [UIViewAnimationOptions.Autoreverse, UIViewAnimationOptions.Repeat, UIViewAnimationOptions.AllowUserInteraction],
                        animations: {
                            alertView.frame = CGRect(x: alertView.frame.origin.x, y: alertView.frame.origin.y+10, width: alertView.frame.size.width, height: alertView.frame.size.height)
                        },
                        completion: { (success) in
                            
                    })
            })

        }
    }
    
    func removeAlert(controller: UIViewController) {
        if let alert = controller.view.viewWithTag(tagAlertBalloon) {
            alert.removeFromSuperview()
            foodDelegate?.didCallCloseAlertBallon()
        }
    }

    
    func removeViewAlert(gesture: UITapGestureRecognizer) {
        UIView.animateWithDuration(0.3) { 
            gesture.view?.removeFromSuperview()
        }
    }
}

