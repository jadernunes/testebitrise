//
//  CupomActionsTableViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CampaignActionsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelShortDescription    : UILabel!
    @IBOutlet weak var labelUnityAddress        : UILabel!
    @IBOutlet weak var labelDate                : UILabel!
    @IBOutlet weak var buttonOfferActions       : UIButton!
    @IBOutlet weak var labelCountCoupons        : UILabel!
    var coupom                                  :Coupom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    static func getConfiguredCell(tableView : UITableView, campaign : Campaign!,superViewController :CampaignDetailSource) -> CampaignActionsTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_ACTIONS_CAMPAING) as! CampaignActionsTableViewCell
        
        cell.coupom = CoupomPersistence.getCoupomByIdCampaign(campaign.id)
        
        var titleButtonActionCoupom = kDownloadCoupom
        
        if (cell.coupom != nil) {
            titleButtonActionCoupom = kCoupomDownloaded
        }
        
        //Action when claim coupom
        cell.buttonOfferActions.addTarget(superViewController, action: #selector(superViewController.buttonActionsOfferPressed), forControlEvents: UIControlEvents.TouchUpInside)
        
        //set title button
        cell.buttonOfferActions.setTitle(titleButtonActionCoupom, forState: UIControlState.Normal)
        
        //Count coupons
        let couponAvailable = (campaign.couponLimit - campaign.couponClaimed)
        cell.labelCountCoupons.text = String(couponAvailable)
        
        //Address
        let unity = UnityPersistence.getUnityByID(String(campaign.idUnity)) as Unity?
        if (unity != nil) {
            cell.labelUnityAddress.text = unity?.address
        }
        
        //Short Description
        cell.labelShortDescription.text = campaign.shortDesc
        
        //Data end claim
        cell.labelDate.text = String.getDateByServerConvertToShow((campaign!.dateEndClaim)!)
        
        return cell
    }
}
