    //
//  FoodStartVC.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodInitialVC: UIPageViewController {
    
    private let validator = FoodValidations()
    var controllers = [FoodGeoLocationVC(), FoodListEcsVC(), FoodAddressEditVC()]
    
    /// Define with EC List will show at first load
    var initialOrderType : kFoodStarterPageController = .entrega
    
    override func viewDidLoad() {
        self.delegate = self
        initControllers()
        
        FoodUtils.customizeViewTitle("Gastronomia", vc: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    /// Initiate the controllers array/list
    private func initControllers() {
        let geoController = storyboard?.instantiateViewControllerWithIdentifier("FoodGeoLocationVC") as? FoodGeoLocationVC
        geoController?.foodDelegate = self
        if initialOrderType == .entrega {
            geoController?.isDelivery = true
        }
        let listEcsController = storyboard?.instantiateViewControllerWithIdentifier("FoodListEcsVC") as? FoodListEcsVC
        listEcsController?.foodDelegate = self
        listEcsController?.startPage = self.initialOrderType
        
        controllers = [geoController!, listEcsController!]
        
        //valida se ja tem um endereco salvo, abre na controller certa
        if let address = AddressPersistence.getAddressBySelected() {
            didLoadAddress(address)
        } else {
            self.setViewControllers([controllers[1]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
    }
    
    private func getListEcsController() -> FoodListEcsVC? {
        if let vc = self.controllers.first as? FoodListEcsVC {
            return vc
        } else {
            if let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodListEcsVC") as? FoodListEcsVC {
                return vc
            }
        }
        return nil
    }
    
    private func getGeoLocationController() -> FoodGeoLocationVC? {
        return self.controllers.last as? FoodGeoLocationVC
    }
    
    
}
//MARK: FoodListEcsVCDelegate
extension FoodInitialVC: FoodListEcsVCDelegate {
    func noAddressSelected() {
        if let vc = getGeoLocationController() {
            self.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: { (success) in
                if success {
                    vc.viewDidAppear(true)
                }
            })
        }
    }
}
//MARK: FoodGeoLocationVCDelegate
extension FoodInitialVC: FoodGeoLocationVCDelegate {
    func didLoadAddress(address: Address?) {
        if let vc = getListEcsController() {
            if let vcLocation = self.getGeoLocationController() {
                NSNotificationCenter.defaultCenter().removeObserver(vcLocation, name: UIApplicationDidBecomeActiveNotification, object: nil)
            }
            vc.address = address
            vc.foodDelegate = self
            vc.startPage = initialOrderType
            self.setViewControllers([vc], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: { (success) in
                if success {
                    vc.viewDidAppear(true)
                }
            })
        }
    }
}

extension FoodInitialVC: FoodAddressEditVCDelegate {
    
}


//MARK: UIPageViewControllerDelegate, UIPageViewControllerDataSource
extension FoodInitialVC : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let index = controllers.indexOf(viewController)
        guard index >= 0 else {
            return nil
        }
        
        let nextIndex = index! + 1
        let count = controllers.count
        guard nextIndex != count else {
            return nil
        }
        guard count > nextIndex else {
            return nil
        }
        
        return controllers[nextIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let index = controllers.indexOf(viewController)
        guard index >= 0 else {
            return nil
        }
        
        let previusIndex = index! - 1
        let count = controllers.count-1
        guard previusIndex >= 0 else {
            return nil
        }
        guard count > previusIndex else {
            return nil
        }
        return controllers[previusIndex]
        
    }
    
//Descomentar para exibir o indicador de pagina
//    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
//        return controllers.count
//    }
//
//    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
//        return 0
//    }
    
}


