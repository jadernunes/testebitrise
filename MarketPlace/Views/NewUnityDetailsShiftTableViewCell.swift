//
//  NewUnityDetailsShiftTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 05/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewUnityDetailsShiftTableViewCell: UITableViewCell {

    private var shiftsString = String()
    
    @IBOutlet weak var labelDayOfWeek: UILabel! {
        didSet {
            self.labelDayOfWeek.font = LayoutManager.primaryFontWithSize(16)
            self.labelDayOfWeek.textColor = LayoutManager.black
            self.labelDayOfWeek.text = ""
            self.labelDayOfWeek.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelShifts: UILabel! {
        didSet {
            self.labelShifts.font = LayoutManager.primaryFontWithSize(16)
            self.labelShifts.textColor = LayoutManager.labelFourthColor
            self.labelShifts.text = ""
            self.labelShifts.alpha = 0.0
        }
    }

    @IBOutlet weak var viewHair: UIView! {
        didSet {
            self.viewHair.alpha = 0.0
        }
    }
    
    @IBOutlet weak var constraintHeightViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintHeightViewHair.constant = 0.4
        }
    }
    
    @IBOutlet weak var constraintLeadingViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintLeadingViewHair.constant = 16.0
        }
    }
    
    @IBOutlet weak var constraintTrailingViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintTrailingViewHair.constant = 16.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .None
    }
    
    func configureWithShift(shifts shifts: [Shifts], last: Bool) {
        if !shifts.isEmpty {
            self.labelDayOfWeek.text =
                Util.dayOfWeekInPortuguese(shifts.first!.dayOfWeek)
            
            for shift in shifts {
                let startAt = NSString.getHourByServerConvertToShow(shift.startAt)
                let endAt = NSString.getHourByServerConvertToShow(shift.endAt)
                
                if self.shiftsString.isEmpty {
                    self.shiftsString = "\(startAt) > \(endAt)"
                } else {
                    self.shiftsString += " \u{2022} \(startAt) > \(endAt)"
                }
            }
            
            if last {
                self.constraintLeadingViewHair.constant = 0.0
                self.constraintTrailingViewHair.constant = 0.0
                self.constraintHeightViewHair.constant = 1.0
            }
            
            self.labelShifts.text = self.shiftsString
            self.shiftsString = String()
        }
        self.present()
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsShiftTableViewCell
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelDayOfWeek.alpha = 1.0
            self.labelShifts.alpha = 1.0
            self.viewHair.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
}
