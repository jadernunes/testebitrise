//
//  FoodListEcsVC.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 15/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

enum kTableViewDataSource: Int {
    
    case Entrega            = 0
    case Retirada           = 1
    case Comanda            = 2
    case Undefined          = 3
    
}

protocol FoodListEcsVCDelegate {
    func noAddressSelected() -> Void
}

class FoodListEcsVC : UIViewController {
    
    @IBOutlet weak var contraintGadgetAddress   : NSLayoutConstraint! {
        didSet {
            contraintGadgetAddress.constant = -34
        }
    }
    @IBOutlet weak var gadgetAddress            : FoodViewGadgetAddress! {
        didSet {
            gadgetAddress.delegate = self
            setGadgetAddress()
            pageController?.address = address
        }
    }
    @IBOutlet weak var viewTabBarSelector       : FoodViewTabSelector! {
        didSet {
            viewTabBarSelector.delegate = self
            if let startPage = startPage {
                viewTabBarSelector.currentDatasource = kTableViewDataSource.init(rawValue: startPage.rawValue)!
            }
        }
    }
    
    @IBOutlet weak var segControlDistance: UISegmentedControl!
    
    var address                                 : Address? {
        didSet {
            if let street = address?.street {
                gadgetAddress?.lblAddress.text = street
            }
        }
    }

    private var pageController: FoodPageControllerEcs? {
        didSet {
            if let startPage = startPage {
                pageController?.setViewControllers([pageController!.controllers[startPage.rawValue]], direction: .Forward, animated: false, completion: { (success) in
                    
                })
            }
        }
    }
    private let alerts = FoodAlerts()
    var foodDelegate : FoodListEcsVCDelegate?
    var startPage : kFoodStarterPageController?
    private var timerOutLoader = NSTimer()
    
    override func viewDidLoad() {
        self.hidesBottomBarWhenPushed = true
    }
    override func viewWillAppear(animated: Bool) {
        //Arruma a navigation controller (titulo, etc...)
        ajustarNavigationController()
        FoodUtils.customizeViewTitle(NSLocalizedString("Gastronomia", comment: ""), vc: self)
        //esconder o cart, caso esteja ativo
        CartCustomManager.sharedInstance.hideCart()
        //valida se o endereco foi trocado no carrinho por ex.
        verifySelectedAddres()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //validateAddress()
    }
    
    override func viewDidDisappear(animated: Bool) {
        //caso haja algum alerta na tela, remove ao sair
        alerts.removeAlert(self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueContainer" {
            self.pageController = segue.destinationViewController as? FoodPageControllerEcs
            self.pageController?.foodDelegate = self
            self.pageController?.address = address
        }
    }
    
    private func setStarterPage() -> Void {
        if let startPage = startPage {
            viewTabBarSelector?.currentDatasource = kTableViewDataSource(rawValue: startPage.rawValue)!
            didPressButtonView(kTableViewDataSource(rawValue: startPage.rawValue)!)
//            self.startPage = nil
        }
    }
    
    /// Always validate selected address to reload unities
    private func verifySelectedAddres() {
        if let selected = AddressPersistence.getAddressBySelected() {
            if self.address != selected {
                self.address = selected
                pageController?.address = selected
                pageController?.loadUnities()
                setGadgetAddress()
            }
        }
    }
    
    /// Clear the navigation history
    private func ajustarNavigationController() {
        if let firstVC = self.navigationController?.viewControllers.first {
            self.navigationController?.viewControllers = [firstVC,self]
        }
    }
    
    func hideLoadingView() {
        self.timerOutLoader.invalidate()
        FoodUtils.hideLoadingView((self.pageController?.view)!)
        //setStarterPage()
    }
    
    private func setGadgetAddress() {
        if let street = address?.street {
            gadgetAddress.lblAddress.text = "\(street)"
            if let number = address?.number {
                gadgetAddress.lblAddress.text = "\(gadgetAddress.lblAddress.text!), \(number)"
            }
            if let complement = address?.complement {
                if complement != "" {
                    gadgetAddress.lblAddress.text = "\(gadgetAddress.lblAddress.text!), \(complement)"
                }
            }
        }
    }
    
    private func changeDataSource(datasource : kTableViewDataSource) -> Void {

    }
    
    func didSelectRowTableview() {
        
    }
    
    
    
}

//MARK: FoodViewTabSelectorDelegate
extension FoodListEcsVC: FoodViewTabSelectorDelegate {
    func didPressButtonView(datasource: kTableViewDataSource) {
        
        switch datasource {
        case .Entrega:
            //self.contraintGadgetAddress.constant = 0
            self.view.layoutIfNeeded()
            self.pageController?.self.setViewControllers([self.pageController!.controllers[0]],
                                                         direction: UIPageViewControllerNavigationDirection.Reverse,
                                                         animated: true,
                                                         completion: nil)
            break
        case .Retirada:
            //self.contraintGadgetAddress.constant = -34
            self.view.layoutIfNeeded()
            var direction = UIPageViewControllerNavigationDirection.Forward
            if self.viewTabBarSelector?.currentDatasource == .Comanda {
                direction = UIPageViewControllerNavigationDirection.Reverse
            }
            self.pageController?.self.setViewControllers([self.pageController!.controllers[1]],
                                                         direction: direction,
                                                         animated: true,
                                                         completion: nil)
            break
        case .Comanda:
            //self.contraintGadgetAddress.constant = -34
            self.view.layoutIfNeeded()
            self.pageController?.self.setViewControllers([self.pageController!.controllers[2]],
                                                         direction: UIPageViewControllerNavigationDirection.Forward,
                                                         animated: true,
                                                         completion: nil)
            break
        default:
            break
        }

        UIView.animateWithDuration(0.3) {
            self.view.setNeedsLayout()
        }
    }
}
//MARK: FoodViewGadgetAddressDelegate
extension FoodListEcsVC: FoodViewGadgetAddressDelegate {
    func didPressView(sender: UIView) {
        self.title = ""
        if FoodUtils.validateAddress() {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodAddressEditVC") as? FoodAddressEditVC {
                vc.address = address
                vc.editingAddress = true
                vc.hidesBottomBarWhenPushed = true
                self.showViewController(vc, sender: self)
            }
        } else {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodGeoLocationVC") as? FoodGeoLocationVC {
                vc.hidesBottomBarWhenPushed = true
                self.showViewController(vc, sender: self)
            }
        }
        
    }
}
//MARK: UIGestureRecognizerDelegate
extension FoodListEcsVC: UIGestureRecognizerDelegate {
    func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed {
            UIView.animateWithDuration(0.3, animations: { 
                self.viewTabBarSelector.viewIndicator.frame.size.height = 3.0
            })
            
            let translation = gestureRecognizer.translationInView(self.viewTabBarSelector)
            if(self.viewTabBarSelector.viewIndicator.center.x <= 100) {
                self.viewTabBarSelector.viewIndicator.center = CGPointMake(self.viewTabBarSelector.viewIndicator.center.x + (translation.x*2), self.viewTabBarSelector.viewIndicator.center.y)
            }else {
                self.viewTabBarSelector.viewIndicator.center = CGPointMake(100, self.viewTabBarSelector.viewIndicator.center.y)
            }
            gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.viewTabBarSelector)
        
        } else if gestureRecognizer.state == UIGestureRecognizerState.Ended {
            UIView.animateWithDuration(0.3, animations: {
                self.viewTabBarSelector.viewIndicator.frame.size.height = 2.0
            })
        }
        
    }
}
//MARK: FoodPageControllerEcsDelegate
extension FoodListEcsVC: FoodPageControllerEcsDelegate {
    func didChangePage(index: Int) {
        switch index {
        case 0:
            //self.contraintGadgetAddress.constant = 0
            self.view.layoutIfNeeded()
        default:
            //self.contraintGadgetAddress.constant = -34
            self.view.layoutIfNeeded()
        }
        self.viewTabBarSelector?.currentDatasource = kTableViewDataSource(rawValue: index)!
    }
    func didFinishedLoadData(success: Bool) {
        if success {
            self.hideLoadingView()
        }
    }
    func didStartLoadingData() {
        //self.showLoadingView()
    }
    func didPressEntregaAddress() {
        self.didPressView(self.view)
    }
}
