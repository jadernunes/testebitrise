//
//  FoodViewGeoActivation.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 17/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodViewGeoActivationDelegate {
    func didPressAtivar(button: UIButton) -> ()
    func didPressNaoAtivar(button: UIButton) -> ()
}

class FoodViewGeoActivation: UIView {
    
    var delegate:FoodViewGeoActivationDelegate?
    
    @IBOutlet weak var imgViewIcon: UIImageView!
    
    @IBOutlet weak var lblMessage: UILabel!{
        didSet {
            self.lblMessage.text = NSLocalizedString("telaGeoActivationMessage", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBOutlet weak var lblQuestion: UILabel! {
        didSet {
            self.lblQuestion.text = NSLocalizedString("telaGeoActivationQuestion", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBOutlet weak var btnAtivar: FoodRoundButton! {
        didSet {
            self.btnAtivar.titleLabel?.text = NSLocalizedString("telaGeoActivationQuestion", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBOutlet weak var btnNaoAtivar: FoodRoundButton! {
        didSet {
            self.btnNaoAtivar.titleLabel?.text = NSLocalizedString("btnNaoAtivar", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBAction func didPressAtivar(button: UIButton) -> () {
        self.delegate?.didPressAtivar(button)
    }
    
    @IBAction func didPressNaoAtivar(button: UIButton) -> () {
        self.delegate?.didPressNaoAtivar(button)
    }
    
    
    class func instanceFromNib() -> FoodViewGeoActivation {
        let view = UINib(nibName: "FoodViewGeoActivation", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FoodViewGeoActivation
        return view
    }
    
}
