//
//  FoodViewLoading.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 20/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodViewLoading: UIView {
    
    var images = [UIImage]() {
        didSet {
            self.viewGif.animationImages = images
        }
    }
    
    var message = "" {
        didSet {
            self.lblMessage.text = message
            if lblMessage.text == "" {
                viewGif.frame = CGRectMake(self.bounds.width/2 - viewGif.frame.width,
                                           self.bounds.height/2 - viewGif.frame.height,
                                           viewGif.frame.width,
                                           viewGif.frame.height)
            }
        }
    }
    
    @IBOutlet weak var viewGif: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    
    class func instanceFromNib() -> FoodViewLoading {
        let view = UINib(nibName: "FoodViewLoading", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FoodViewLoading
        return view
    }
    
}
