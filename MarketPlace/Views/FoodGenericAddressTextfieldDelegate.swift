//
//  FoodGenericAddressTextfieldDelegate.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 17/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodGenericAddressTextfieldDelegate: NSObject, UITextFieldDelegate {
    
    var maxSize: Int = 128
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.characters.count >= maxSize && string != "" {
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
}
