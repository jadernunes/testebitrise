//
//  UnityContactTableViewCell_New.swift
//  MarketPlace
//
//  Created by Luciano on 10/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class UnityContactTableViewCell_: UITableViewCell {
    @IBOutlet weak var viewTopLine      : UIView!
    @IBOutlet weak var labelTitle       : UILabel!
    @IBOutlet weak var labelAddress     : UILabel!
    @IBOutlet weak var labelDirections  : UILabel!
    @IBOutlet weak var labelPhoneNumber : UILabel!
    @IBOutlet weak var imageIconPhone   : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = LayoutManager.backgroundLight
        viewTopLine.backgroundColor = SessionManager.sharedInstance.cardColor
        labelAddress.font = LayoutManager.primaryFontWithSize(16)
        labelAddress.textColor = LayoutManager.regularFontColor
        
        labelTitle.font = LayoutManager.primaryFontWithSize(18)
        labelTitle.textColor = LayoutManager.regularFontColor
        
        labelDirections.font = LayoutManager.primaryFontWithSize(16)
        labelDirections.textColor = SessionManager.sharedInstance.cardColor
        
        labelPhoneNumber.font = LayoutManager.primaryFontWithSize(16)
        labelPhoneNumber.textColor = SessionManager.sharedInstance.cardColor
        
        //var frameAddress = self.labelAddress.frame
        //frameAddress.size.height = self.labelAddress.intrinsicContentSize().height
        //self.labelAddress.frame = frameAddress
        
        imageIconPhone.tintColor = SessionManager.sharedInstance.cardColor
        
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, unity : Unity) -> UnityContactTableViewCell_{
        let cell = tableView.dequeueReusableCellWithIdentifier(UNITY_CONTACT) as! UnityContactTableViewCell_
        
        if unity.address != nil && unity.address != "" {
            cell.labelAddress.text = unity.address!
        }
    
        if unity.phoneNumber != nil && unity.phoneNumber != "" {
            cell.labelPhoneNumber.text = unity.phoneNumber!
        }
        
        return cell
    }

}
