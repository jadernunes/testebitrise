//
//  CupomHeaderTableViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CampaignHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCampaign : UIImageView!
    @IBOutlet weak var labelTitle    : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .None;
        self.labelTitle.textColor = LayoutManager.white
        self.labelTitle.font = LayoutManager.primaryFontWithSize(20)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    static func getConfiguredCell(tableView : UITableView, campaign : Campaign!) -> CampaignHeaderTableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_HEADER_CAMPAING) as! CampaignHeaderTableViewCell
        
        var urlLogoUnity = ""
        
        let unity = UnityPersistence.getUnityByID(String(campaign.idUnity)) as Unity?
        if(unity != nil){
            let thumb = unity?.getThumb()
            if (thumb != nil) {
                urlLogoUnity = (thumb?.value)!
            }
            cell.labelTitle.text = unity?.name
        }
        
        cell.imageCampaign.addImage(urlLogoUnity) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                cell.imageCampaign.contentMode = .ScaleToFill
            }
        }
        
        return cell
    }
}
