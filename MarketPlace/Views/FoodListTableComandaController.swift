//
//  FoodListTableComandaDatasource.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 22/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

protocol TableComandaControllerDelegate {
    var locationComanda: CLLocation? {get set}
    func didPressComandaCell(indexPath: NSIndexPath, unity: Unity) -> Void
    func didCallRefreshComanda(tempAddress: Address?) -> Void
    func callRefreshViewComanda() -> Void
}

extension TableComandaControllerDelegate {
    func didCallRefreshComanda(tempAddress: Address? = nil) -> Void {
        return didCallRefreshComanda(tempAddress)
    }
    func callRefreshViewComanda() -> Void {
        //only to make this func optional
    }
}

class FoodListTableComandaController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegateFood: TableComandaControllerDelegate?
    
    @IBOutlet weak var tableComanda: UITableView!
    private var cellIdentifier = "FoodListaEcsCell"
    private var viewGeolocationError = FoodViewGeolocationError()
    var dados: Results<Unity>? {
        didSet{
            dados = dados?.sorted("distanceFromUser", ascending: true)
            self.dadosOrdered = FoodUtils.formatRealmData(dados)
            if tableComanda != nil {
                tableComanda.reloadData()
            }
            if dadosOrdered  != nil {
                FoodUtils.hideLoadingView(self.view)
            }
        }
    }
    
    private var dadosOrdered: (opened:[Unity],closed:[Unity])?
    private var didOpenSettings: Bool = false
    
    func validacoes() {
        self.delegateFood?.callRefreshViewComanda()
        if let address = AddressPersistence.getAddressBySelected() {
            self.delegateFood?.didCallRefreshComanda(address)
        } else if CLLocationManager.locationServicesEnabled() {
            print("location services enabled")
            if CLLocationManager.authorizationStatus() == .Denied ||
                CLLocationManager.authorizationStatus() == .NotDetermined {
                viewGeolocationError = FoodUtils.createGeolocationErrorView(self.view.frame, delegate: self, isDelivery: false)
                viewGeolocationError.alpha = 1.0
                view.addSubview(viewGeolocationError)
                view.bringSubviewToFront(viewGeolocationError)
            } else {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    FoodUtils.hideLoadingView(self.view)
                    if let address = address {
                        self.delegateFood?.didCallRefreshComanda(address)
                    }
                    //caso não ter conseguido buscar endereco
                    if errorDescription != nil {
                        
                    }
                })
            }
        }
    }
    
    //MARK: UITableView
    override func viewDidLoad() {
        tableComanda.registerNib(UINib(nibName: "FoodListaEcsCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        if #available(iOS 10.0, *) {
            let refreshControl = UIRefreshControl()
            refreshControl.attributedTitle = NSAttributedString(string: "")
            refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
            tableComanda.refreshControl = refreshControl
            tableComanda?.addSubview(refreshControl)
        }
        
        validacoes()
        
        //notificacao para validar se o usuario permitiu o gps
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.validacoes), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if dadosOrdered?.opened.count > 0 && dadosOrdered?.closed.count > 0 {
            return 2
        } else if dadosOrdered?.opened.count > 0 || dadosOrdered?.closed.count > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        tableComanda.backgroundView = UIView()
        tableComanda.separatorStyle = .SingleLine
        
        if sect == kSectionListEcs.opened {
            if let count = dadosOrdered?.opened.count {
                return count
            }
        } else if sect == kSectionListEcs.closed {
            if let count = dadosOrdered?.closed.count {
                return count
            }
        }
        
        tableComanda.backgroundView = FoodViewNoData()
        tableComanda.separatorStyle = .None
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! FoodListaEcsCell
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)
        
        //Valida que tipo de EC é, se aberto ou fechado
        var unity:Unity
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: true, userLocation: true)
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: false, userLocation: true)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var unity:Unity
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)
        
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
        }
        self.loadUnity(indexPath, unity: unity)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        
        //sem um titulo não chama a headerview
        if sect == kSectionListEcs.opened {
            return ""
        } else {
            return NSLocalizedString("Fechados neste momento", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //chama a view customizada
        let header = FoodViewHeaderListEcs(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        return header
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //personaliza o texto da view
        if let header = view as? FoodViewHeaderListEcs {
            header.textLabel?.font = UIFont.foodDosisLightFont()
            header.textLabel?.textColor = UIColor.foodGreyishBrownColor()
        }
    }
    
    func refresh(sender:AnyObject) {
        FoodUtils.showLoadingView([GastronomiaKit.imageOfLoaderGastro1,
            GastronomiaKit.imageOfLoaderGastro2,
            GastronomiaKit.imageOfLoaderGastro3,
            GastronomiaKit.imageOfLoaderGastro4,
            GastronomiaKit.imageOfLoaderGastro5,
            GastronomiaKit.imageOfLoaderGastro6,
            GastronomiaKit.imageOfLoaderGastro7,
            GastronomiaKit.imageOfLoaderGastro8,
            GastronomiaKit.imageOfLoaderGastro9,
            GastronomiaKit.imageOfLoaderGastro10,
            GastronomiaKit.imageOfLoaderGastro11,
            GastronomiaKit.imageOfLoaderGastro12],
                                  targetView: (self.view)!,
                                  message: NSLocalizedString("Carregando estabelecimentos", tableName: "GastronomiaStrings", comment: ""))
        
        delegateFood?.didCallRefreshComanda()
        if #available(iOS 10.0, *) {
            tableComanda.refreshControl?.endRefreshing()
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func loadUnity(indexPath: NSIndexPath, unity: Unity) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        detailsVc.title = unity.name
        detailsVc.idObjectReceived = unity.id
        detailsVc.typeObjet = TypeObjectDetail.Store
        if self.navigationController?.topViewController != detailsVc {
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
        
        let api = ApiServices()
        api.getUnityByIDService(unity.id)
    }
}
//MARK: - FoodViewGeolocationErrorDelegate
extension FoodListTableComandaController: FoodViewGeolocationErrorDelegate {
    func didPressAtivarGeolocalizacao(button: UIButton) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways ||
                CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    if let address = address {
                        self.delegateFood?.didCallRefreshComanda(address)
                    }
                })
            } else {
                didOpenSettings = true
                FoodAlerts().showAlertGeoLocation()
            }
            
            
        }
    }
    
    func didPressInformarEndereco(button: UIButton) {
        self.title = ""
        let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC")
        self.showViewController(vc!, sender: self)
    }
    
    func didMoveToView() {
        
    }
}
