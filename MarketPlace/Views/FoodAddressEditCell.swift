//
//  FoodAddressEditCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 07/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodAddressEditCell: UITableViewCell {
    
    @IBOutlet weak var txtNumero: UITextField!
    @IBOutlet weak var txtComplemento: UITextField!
    @IBOutlet weak var txtCep: UITextField!
    @IBOutlet weak var viewBgCEP: FoodRoundedView!
    @IBOutlet weak var viewBgComplemento: FoodRoundedView!
    @IBOutlet weak var viewBgNumero: FoodRoundedView!
    @IBOutlet weak var viewAlertInvalidField: UIView!
    
    var haveInvalidField : Bool = false {
        didSet {
            self.viewAlertInvalidField.hidden = !self.haveInvalidField
        }
    }
    
    private var sizeNormal:CGFloat = 210
    private var sizeInvalid:CGFloat = 240
    
    private func getSize() -> CGFloat {
        if haveInvalidField {
            return sizeInvalid
        } else {
            return sizeNormal
        }
    }
    
    override func didMoveToWindow() {
        //--- add UIToolBar on keyboard and Done button on UIToolBar ---//
        self.addDoneButtonOnKeyboard()
    }
    
    func showHideAlertCep(hide hide:Bool) -> Void {
        self.viewAlertInvalidField.hidden = hide
    }
    
    private func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.Default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Pronto", style: UIBarButtonItemStyle.Done, target: self, action: #selector(FoodAddressEditCell.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtCep.inputAccessoryView = doneToolbar
        txtNumero.inputAccessoryView = doneToolbar
        
    }
    
    @objc private func doneButtonAction()
    {
        
        txtCep.resignFirstResponder()
        txtNumero.resignFirstResponder()
    }
    
}
