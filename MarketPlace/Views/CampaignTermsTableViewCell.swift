//
//  CupomTermsTableViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/22/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CampaignTermsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var labelRegulation: UILabel!
    @IBOutlet weak var textViewRegulation: UITextView!
    @IBOutlet weak var viewLineSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelRegulation.font = LayoutManager.primaryFontWithSize(18)
        self.textViewRegulation.font = LayoutManager.primaryFontWithSize(16)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getConfiguredCell(tableView : UITableView, campaign : Campaign?) -> CampaignTermsTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_TERMS_CAMPAING) as! CampaignTermsTableViewCell
        cell.textViewRegulation.text = campaign?.terms
        cell.backgroundColor = LayoutManager.labelFirstColor
        cell.viewLineSeparator.backgroundColor = SessionManager.sharedInstance.cardColor
        cell.textViewRegulation.backgroundColor = UIColor.clearColor()
        
        return cell
    }
}
