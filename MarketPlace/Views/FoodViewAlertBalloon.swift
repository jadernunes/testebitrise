//
//  FoodViewAlertBalloon.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 29/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodViewAlertBalloonDelegate {
    func didPressBalloon() -> Void
}

class FoodViewAlertBalloon: UIView {
    
    var foodDelegate: FoodViewAlertBalloonDelegate?
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblAlerta: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UINib(nibName: "FoodViewAlertBalloon", bundle: nil).instantiateWithOwner(self, options: nil)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didPressBalloon(_:)))
        view.addGestureRecognizer(gesture)
        addSubview(view)
        view.frame = self.bounds
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        UINib(nibName: "FoodViewAlertBalloon", bundle: nil).instantiateWithOwner(self, options: nil)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didPressBalloon(_:)))
        view.addGestureRecognizer(gesture)
        addSubview(view)
        view.frame = self.bounds
    }
    
    func didPressBalloon(gesture: UITapGestureRecognizer) -> Void {
        foodDelegate?.didPressBalloon()
    }
}
