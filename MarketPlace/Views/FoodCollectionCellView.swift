//
//  FoodCollectionCellView.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 22/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodCollectionCellView: UIView {
        
    class func instanceFromNib() -> FoodCollectionCellView {
        let view = UINib(nibName: "FoodCollectionCellView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FoodCollectionCellView
        return view
    }
    
}
