//
//  CampaignDescriptionTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 13/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CampaignDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var viewLineSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelDescription.font = LayoutManager.primaryFontWithSize(18)
        self.textViewDescription.font = LayoutManager.primaryFontWithSize(16)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getConfiguredCell(tableView : UITableView, campaign : Campaign?) -> CampaignDescriptionTableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL_DESCRIPTION_CAMPAING) as! CampaignDescriptionTableViewCell
        cell.textViewDescription.text = campaign?.longDesc
        cell.backgroundColor = LayoutManager.backgroundFirstColor
        cell.viewLineSeparator.backgroundColor = SessionManager.sharedInstance.cardColor
        cell.textViewDescription.backgroundColor = UIColor.clearColor()
        cell.textViewDescription.textColor = LayoutManager.labelFirstColor
        return cell
    }
}
