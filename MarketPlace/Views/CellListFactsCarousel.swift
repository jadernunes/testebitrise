//
//  ListFactsCarousel.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 29/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class CellListFactsCarousel : UITableViewCell {
    
    @IBOutlet weak var carousel: iCarousel! {
        didSet {
            self.carousel.scrollEnabled = false
            self.carousel.type = .Linear
            self.carousel.scrollSpeed = 4
            self.carousel.pagingEnabled = true
            self.carousel.scrollToItemBoundary = true
            self.carousel.dataSource = self
            self.carousel.delegate = self
        }
    }
    
    @IBOutlet weak var pageControlListFacts: UIPageControl! {
        didSet {
            self.pageControlListFacts.currentPageIndicatorTintColor = LayoutManager.white
        }
    }
    
    private var referenceToSuperViewController: UIViewController!
    private var facts                           = [Fact]() {
        didSet{
            self.carousel.reloadData()
        }
    }
    
    private let secondsChangeFact   = NSTimeInterval(2.0)
    private var enableToScroll      = true
    
    private var shouldStartAnimate = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = LayoutManager.backgroundFirstColor
        
        let timerChangeFacts = NSTimer.scheduledTimerWithTimeInterval(secondsChangeFact, target: self, selector: #selector(self.changeFacts), userInfo: nil, repeats: true)
        timerChangeFacts.fire()
    }
    
    //MARK: - Change facts with timer
    @objc private func changeFacts() {
        if self.shouldStartAnimate && self.enableToScroll {
            var indexWillSelected = 0
            if self.carousel.currentItemIndex < self.carousel.numberOfItems - 1 {
                indexWillSelected = self.carousel.currentItemIndex + 1
            }
            self.carousel.scrollToItemAtIndex(indexWillSelected, duration: 1.0)
        }
    }
    
    func getHeight() -> CGFloat {
        return self.facts.isEmpty ? 0.0 : .heightUnityDetailsFactsTableViewCell
    }
    
    //MARK: - UIResponder methods
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        enableToScroll = false
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        enableToScroll = true
    }
}

extension CellListFactsCarousel : ConfigurableWithUnity {
    
    func configureWithUnity(unity: Unity, withReference: UIViewController?) {
        self.facts = [Fact]()
        
        let result = FactEntityManager.getFactsByUnityId(unity.id)
        
        if !result.isEmpty {
            self.facts = result.flatMap() { $0 }

            self.referenceToSuperViewController = withReference
            self.pageControlListFacts.numberOfPages = facts.count
            self.carousel.scrollEnabled = true
            
            self.shouldStartAnimate = true
        }
    }
}

//MARK: - DataSource iCarousel

extension CellListFactsCarousel : iCarouselDataSource {

    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return self.facts.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        if let view = view {
            return view
        } else {
            let viewCarousel = NSBundle.mainBundle().loadNibNamed("ViewFactCarousel_", owner: self, options: nil)!.first as! ViewFactCarousel_
            viewCarousel.configureWithFact(self.facts[index])
            return viewCarousel
        }
    }
}

//MARK: - Delegate iCarousel

extension CellListFactsCarousel : iCarouselDelegate {
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        let detailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        let fact = facts[index]
        
        detailsViewController.title = fact.title
        detailsViewController.idObjectReceived = fact.id
        detailsViewController.typeObjet = TypeObjectDetail.Event
        
        referenceToSuperViewController.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    func carouselDidEndScrollingAnimation(carousel: iCarousel) {
        enableToScroll = true
    }
    
    func carouselWillBeginScrollingAnimation(carousel: iCarousel) {
        enableToScroll = false
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        self.pageControlListFacts.currentPage = carousel.currentItemIndex
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        return value
    }
}

