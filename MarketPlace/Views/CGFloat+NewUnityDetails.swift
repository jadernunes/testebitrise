//
//  CGFloat+NewUnityDetails.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 19/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension CGFloat {
    
    private static let heightDefault: CGFloat = 65.0
    
    /// NewUnityDetailsBannerTableViewCell constants
    public static let heightUnityDetailsBannerTableViewCell: CGFloat = 290.0
    
    ///NewUnityDetailsItemTableViewCell constants
    public static let heightUnityDetailsItemTableViewCell: CGFloat = heightDefault
    
    ///NewUnityDetailsFactsTableViewCell constants
    public static let heightUnityDetailsFactsTableViewCell: CGFloat = 200.0
    
    ///NewUnityDetailsShiftTableViewCell constants
    public static let heightUnityDetailsShiftTableViewCell: CGFloat = heightDefault
    
    ///NewUnityDetailsHeader constants
    public static let heightUnityDetailsHeader: CGFloat = 75.0
    
    ///NewUnityDetailsAddressTableViewCell constants
    public static let heightUnityDetailsAddressTableViewCell: CGFloat = 100.0
    
    ///NewUnityDetailsPhoneTableViewCell constants
    public static let heightUnityDetailsPhoneTableViewCell: CGFloat = heightDefault
}
