//
//  FidelityCollectionViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class FidelityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewIconStamp: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
