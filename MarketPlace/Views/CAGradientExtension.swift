//
//  CAGradientExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 25/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.startPoint = CGPointMake(1.0,1.0)
        layer.endPoint = CGPointMake(0.1, 0.1)
        layer.colors = [Util.hexStringToUIColor("AB1033").CGColor, Util.hexStringToUIColor("FB0B40").CGColor]
        return layer
    }
}
