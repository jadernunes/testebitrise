//
//  NewUnityDetailsItemTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 03/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewUnityDetailsItemTableViewCell: UITableViewCell {

    @IBOutlet weak var constraintHeightViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintHeightViewHair.constant = 0.5
        }
    }

    @IBOutlet weak var viewHair: UIView! {
        didSet {
            self.viewHair.alpha = 0.0
        }
    }

    @IBOutlet weak var labelMessage: UILabel! {
        didSet {
            self.labelMessage.font = LayoutManager.primaryFontWithSize(16)
            self.labelMessage.textColor = LayoutManager.black
            self.labelMessage.text = ""
            self.labelMessage.alpha = 0.0
            self.labelMessage.enabled = false
        }
    }
    
    @IBOutlet weak var iconImage: UIImageView! {
        didSet {
            self.iconImage.clipsToBounds = true
            self.iconImage.contentMode = .ScaleAspectFit
            self.iconImage.tintImage(SessionManager.sharedInstance.cardColor)
            
            self.iconImage.image = self.iconImage.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            self.iconImage.tintColor = SessionManager.sharedInstance.cardColor

        }
    }
    
    var enabled = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsItemTableViewCell
    }
    
    func configureWith(message: String,
                       image: String,
                       item: NewUnityDetailsItemsTableViewCell.Item,
                       last: Bool,
                       shouldEnable: Bool) {
        
        self.labelMessage.text = message
        self.iconImage.image = UIImage(named: image)
        self.iconImage.tintImage(SessionManager.sharedInstance.cardColor)
        
        self.contentView.backgroundColor = SessionManager.sharedInstance.cardColor
        
        if item == NewUnityDetailsItemsTableViewCell.Item.orderCheckEnabled {
            self.setEnable(true)
        } else {
            self.setEnable(shouldEnable)
        }
        
        if last {
            self.constraintHeightViewHair.constant = 1.0
        }
        
        self.present()
    }
    
    func setEnable(enabled: Bool) {
        self.enabled = enabled
        
        if self.enabled {
            self.selectionStyle = .Gray
            self.userInteractionEnabled = enabled
            self.labelMessage.enabled = enabled
        } else {
            self.selectionStyle = .None
            self.userInteractionEnabled = enabled
            self.labelMessage.enabled = enabled
        }
    }
    
    func highlight() {
        self.labelMessage.textColor = UIColor.whiteColor()
        self.iconImage.tintImage(UIColor.whiteColor())
        
        UIView.animateWithDuration(0.4) {
            self.contentView.backgroundColor = SessionManager.sharedInstance.cardColor
        }
    }
    
    func unHighlight() {
        self.iconImage.tintImage(SessionManager.sharedInstance.cardColor)
        
        UIView.animateWithDuration(0.4) {
            self.contentView.backgroundColor = UIColor.whiteColor()
            self.labelMessage.textColor = LayoutManager.black
        }
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelMessage.alpha = 1.0
            self.viewHair.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
}
