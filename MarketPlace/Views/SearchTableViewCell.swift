//
//  SearchTableViewCell.swift
//  MarketPlace
//
//  Created by Jader Borba Nunes on 9/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Foundation
import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var lableTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
    }
    
    func pupulateData(itemSearched: ItemSearched) {
        
        self.lableTitle?.text = (itemSearched.title)!
        self.imageViewLogo.addImage(itemSearched.logo)
    }
}