//
//  FoodViewGadgetAddress.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 18/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodViewGadgetAddressDelegate {
    func didPressView(sender: UIView) -> ()
}

class FoodViewGadgetAddress: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    
    var delegate:FoodViewGadgetAddressDelegate?
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UINib(nibName: "FoodViewGadgetAddress", bundle: nil).instantiateWithOwner(self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didPressView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    func didPressView(sender: UIView) {
        self.delegate?.didPressView(sender)
    }
    
}
