//
//  FoodListaEcsCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 20/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation


class FoodListaEcsCell: UITableViewCell {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblCategorias: UILabel!
    @IBOutlet weak var lblDistancia: UILabel!
    @IBOutlet weak var lblTextFechado: UILabel!
    @IBOutlet weak var viewFechado: UIView!
    
}
