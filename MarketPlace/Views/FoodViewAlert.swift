//
//  FoodViewAlert.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 24/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import UIKit

class FoodViewAlert {
    
    let controller:UIViewController
    
    init(controller:UIViewController) {
        self.controller = controller
    }
    
    func show(title:String = "", message:String = "", btnTitle:String = "") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let ok = UIAlertAction(title: btnTitle, style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(ok)
        dispatch_async(dispatch_get_main_queue()) {
            self.controller.presentViewController(alert, animated: true, completion: {
                
            })
        }
    }
    
    func showConnectionError() -> Void {
        if controller is SidePanelViewController {
            let vc = controller as! SidePanelViewController
            if let centerVC = vc.centerPanel as? UINavigationController {
                if NSStringFromClass(centerVC.topViewController!.classForCoder).containsString("Food") {
                    let storyboard = UIStoryboard(name: "Gastronomia", bundle: nil)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("FoodConnectionProblemVC") as! FoodConnectionProblemVC
                    dispatch_async(dispatch_get_main_queue()) {
                        self.controller.presentViewController(vc, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
