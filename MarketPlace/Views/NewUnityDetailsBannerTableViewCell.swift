//
//  NewUnityDetailsBannerTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 27/12/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import RealmSwift

class NewUnityDetailsBannerTableViewCell : UITableViewCell, ConfigurableWithUnity {
    
    @IBOutlet weak var imageViewPin: UIImageView! {
        didSet {
            self.imageViewPin.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelDistanceFromUser: UILabel! {
        didSet {
            self.labelDistanceFromUser.alpha = 0.0
        }
    }
    
    @IBOutlet weak var viewGradient: ViewGradient! {
        didSet {
            self.viewGradient.alpha = 0.0
        }
    }

    @IBOutlet weak var constraintHeightViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintHeightViewHair.constant = 0.5
        }
    }

    @IBOutlet weak var imageViewBanner: UIImageView! {
        didSet {
            self.imageViewBanner.clipsToBounds = true
            self.imageViewBanner.contentMode = .ScaleAspectFill
            
            let viewActivity: UIActivityIndicatorView = {
                $0.hidesWhenStopped = true
                $0.center = imageViewBanner.center
                $0.startAnimating()
                return $0
            }(UIActivityIndicatorView(activityIndicatorStyle: .Gray))
            imageViewBanner.addSubview(viewActivity)
        }
    }
    
    @IBOutlet weak var labelActiveSign: UILabel! {
        didSet {
            self.labelActiveSign.layer.cornerRadius = labelActiveSign.frame.height / 2
            self.labelActiveSign.clipsToBounds = true
            self.labelActiveSign.font = LayoutManager.primaryFontWithSize(16)
            self.labelActiveSign.textColor = LayoutManager.white
            self.labelActiveSign.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelTimeRange: UILabel! {
        didSet {
            self.labelTimeRange.font = LayoutManager.primaryFontWithSize(16)
            self.labelTimeRange.textColor = LayoutManager.black
            self.labelTimeRange.alpha = 0.0
            self.labelTimeRange.text = ""
        }
    }
    
    @IBOutlet weak var labelMore: UILabel! {
        didSet {
            self.labelMore.font = LayoutManager.primaryFontWithSize(16)
            self.labelMore.textColor = SessionManager.sharedInstance.cardColor
            self.labelMore.alpha = 0.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsBannerTableViewCell
    }
    
    func configureWithUnity(unity: Unity, withReference: UIViewController?) {
        
        checkDistanceFromUser(unity)
        
        self.addImageToBanner(unity.getThumb())
        
        if unity.opened {
            self.labelActiveSign.text = .unityOpen
            self.labelActiveSign.backgroundColor = LayoutManager.activeUnityOpenColor
        } else {
            self.labelActiveSign.text = .unityClosed
            self.labelActiveSign.backgroundColor = LayoutManager.activeUnityClosedColor
            self.labelTimeRange.text = ""
        }
        
        ShiftEntityManager.getCurrentShiftOfWeekByUnityId(unityId: unity.id) { ( shifts ) in
            self.labelTimeRange.text = self.addHourAvaliableToBanner(shifts)
            self.isUnityOpen(shifts, withUnityOpen: unity.opened)
        }
        
        self.present()
    }
    
    private func checkDistanceFromUser(unity: Unity) {
        if unity.distanceFromUser > kDistanceMinPin && unity.distanceFromUser <= kDistanceMaxPin {
            
            let distanceFromUser = Double(1000)
            if (unity.distanceFromUser) > distanceFromUser {
                self.labelDistanceFromUser.text = "\(Int(unity.distanceFromUser/distanceFromUser)) " + kKilometers
            }
            else {
                self.labelDistanceFromUser.text = "\(Int(unity.distanceFromUser)) " + kMeters
            }
        } else {
            self.labelDistanceFromUser.hidden = true
            self.imageViewPin.hidden = true
        }
    }
    
    private func addImageToBanner(media: Media?) {
        if let _ = media {
            if let value = media!.value {
                self.imageViewBanner.addImage(value)
                self.imageViewBanner.removeActivityViewAsSubview()
                self.viewGradient.alpha = 1.0
            }
        }
    }
    
    private func addHourAvaliableToBanner(shifts: Results<Shifts>?) -> String {
        var hourAvaliable = ""
        
        if let shifts = shifts {
            if !shifts.isEmpty {
                
                var startAt:        String!
                var endAt:          String!
                
                for shift in shifts {
                    startAt =
                        NSString.getHourByServerConvertToShow(shift.startAt)
                    endAt =
                        NSString.getHourByServerConvertToShow(shift.endAt)
                    
                    hourAvaliable = hourAvaliable.isEmpty ?
                        "das \(startAt) às \(endAt)" :
                        hourAvaliable + " \u{2022} das \(startAt) às \(endAt)"
                }
            }
        }
        return hourAvaliable
    }
    
    private func isUnityOpen(shifts: Results<Shifts>?,
                                        withUnityOpen opened: Bool) -> Bool {
        if !opened {
            return false
        }
        
        if let shifts = shifts {
            if !shifts.isEmpty {
                for shift in shifts {
                    let currentDate = NSDate.currentDateInFormat()
                    if (NSDate.secondsByDifferenceDatesWithFormat(currentDate,
                            dateStringFinish: shift.startAt,
                            format: "HH:mm:ss") > 0) &&
                        (NSDate.secondsByDifferenceDatesWithFormat(currentDate,
                            dateStringFinish: shift.endAt,
                            format: "HH:mm:ss") < 0) {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelActiveSign.alpha = 1.0
            self.labelTimeRange.alpha = 1.0
            self.labelDistanceFromUser.alpha = 1.0
            self.imageViewPin.alpha = 1.0
            //self.labelMore.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
}
