//
//  NewUnityDetailsPhoneTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 09/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewUnityDetailsPhoneTableViewCell: UITableViewCell {

    @IBOutlet weak var constraintHeightViewHair: NSLayoutConstraint! {
        didSet {
            self.constraintHeightViewHair.constant = 0.4
        }
    }
    
    @IBOutlet weak var imageViewIcon: UIImageView! {
        didSet {
            self.imageViewIcon.tintImage(SessionManager.sharedInstance.cardColor)
        }
    }
    
    @IBOutlet weak var labelPhone: UILabel! {
        didSet {
            self.labelPhone.font = LayoutManager.primaryFontWithSize(16)
            self.labelPhone.textColor = SessionManager.sharedInstance.cardColor
            self.labelPhone.text = ""
            self.labelPhone.alpha = 0.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsPhoneTableViewCell
    }
    
    func configureWithPhone(phone: String?, last: Bool) {
        if let _ = phone {
            self.labelPhone.text = phone
        }
        
        if last {
            self.constraintHeightViewHair.constant = 1.0
        }
        
        self.present()
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelPhone.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
    
    func highlight() {
        self.labelPhone.textColor = UIColor.whiteColor()
        self.imageViewIcon.tintImage(UIColor.whiteColor())
        self.contentView.backgroundColor = SessionManager.sharedInstance.cardColor
    }
    
    func unHighlight() {
        self.labelPhone.textColor = SessionManager.sharedInstance.cardColor
        self.imageViewIcon.tintImage(SessionManager.sharedInstance.cardColor)
        self.contentView.backgroundColor = UIColor.whiteColor()
    }
}
