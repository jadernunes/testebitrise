//
//  NewUnityDetailsShiftsHeader.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 05/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewUnityDetailsHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var constraintHeightViewHair: NSLayoutConstraint! {
        didSet {
            constraintHeightViewHair.constant = 0.4
        }
    }
    
    @IBOutlet weak var viewHair: UIView! {
        didSet {
            self.viewHair.alpha = 0.0
        }
    }
    
    @IBOutlet weak var viewAbove: UIView! {
        didSet {
            self.viewAbove.backgroundColor = SessionManager.sharedInstance.cardColor
            self.viewAbove.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelShift: UILabel! {
        didSet {
            self.labelShift.font = LayoutManager.primaryFontWithSize(16)
            self.labelShift.textColor = LayoutManager.black
            self.labelShift.text = ""
            self.labelShift.alpha = 0.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = LayoutManager.backgroundLight
    }
    
    func configureWithTitle(title: String) {
        
        self.labelShift.text = title
        
        self.present()  
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsHeader
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelShift.alpha = 1.0
            self.viewAbove.alpha = 1.0
            self.viewHair.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
}
