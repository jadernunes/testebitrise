//
//  ViewFactCarousel_.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 26/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ViewFactCarousel_: UIView {
    
    @IBOutlet weak var imageViewThumb: UIImageView! {
        didSet {
            self.imageViewThumb.contentMode = .ScaleAspectFill
            self.imageViewThumb.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelDay: UILabel! {
        didSet {
            self.labelDay.font = LayoutManager.primaryFontWithSize(50)
            self.labelDay.textColor = LayoutManager.regularFontColor
            self.labelDay.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelMonth: UILabel! {
        didSet {
            self.labelMonth.font = LayoutManager.primaryFontWithSize(14)
            self.labelMonth.textColor = LayoutManager.regularFontColor
            self.labelMonth.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelHour: UILabel! {
        didSet {
            self.labelHour.font = LayoutManager.primaryFontWithSize(14)
            self.labelHour.textColor = LayoutManager.regularFontColor
            self.labelHour.alpha = 0.0
        }
    }
    
    @IBOutlet weak var viewContainerDate: UIView! {
        didSet {
            self.viewContainerDate.backgroundColor =
                SessionManager.sharedInstance.cardColor.colorWithAlphaComponent(0.8)
            self.viewContainerDate.alpha = 0.0
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureWithFact(fact: Fact) {
        dispatch_async(dispatch_get_main_queue(), {
            if fact.season {
                self.viewContainerDate.hidden = true
            }
            
            if let thumb = fact.thumb {
                self.imageViewThumb.addImage(thumb)
            }
            
            self.labelDay.text = fact.getFormatedDateString(false, pattern: "dd")
            
            self.labelHour.text =
                "às \(fact.getFormatedDateString(false, pattern: "HH:mm"))hs"
                    .uppercaseString
            
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            
            self.labelMonth.text =
                months[Int(fact.getFormatedDateString(false, pattern: "MM"))!-1]
                    .uppercaseString
            
            UIView.animateWithDuration(0.2) {
                self.imageViewThumb.alpha = 1.0
                self.labelDay.alpha = 1.0
                self.labelMonth.alpha = 1.0
                self.labelHour.alpha = 1.0
                self.viewContainerDate.alpha = 1.0
            }
        })
    }
}
