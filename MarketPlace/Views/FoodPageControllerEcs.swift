//
//  FoodPageControllerEcs.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 28/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import CoreLocation

protocol FoodPageControllerEcsDelegate {
    func didChangePage(index: Int) -> Void
    func didFinishedLoadData(success: Bool) -> Void
    func didStartLoadingData() -> Void
    func didPressEntregaAddress() -> Void
}

class FoodPageControllerEcs: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    var foodDelegate: FoodPageControllerEcsDelegate?
    var address: Address? {
        didSet {
            if let address = address {
                locationEntrega = CLLocation(latitude: address.latitude, longitude: address.longitude)
                locationRetirada = CLLocation(latitude: address.latitude, longitude: address.longitude)
                locationComanda = CLLocation(latitude: address.latitude, longitude: address.longitude)
                self.loadUnities()
            }
        }
    }
    lazy var controllers : [UIViewController] = self.initControllers()
    var locationEntrega: CLLocation?
    var locationRetirada: CLLocation?
    var locationComanda: CLLocation?
    
    override func viewDidLoad() {
        self.dataSource = self
        self.delegate = self

        if address == nil {
            self.loadUnities()
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        
        guard let index = controllers.indexOf(viewController) else {
            return nil
        }
        guard index >= 0 else {
            return nil
        }
        
        let nextIndex = index + 1
        let count = controllers.count
        guard nextIndex != count else {
            return nil
        }
        guard count > nextIndex else {
            return nil
        }
        
        return controllers[nextIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let index = controllers.indexOf(viewController)
        guard index >= 0 else {
            return nil
        }
        
        let previusIndex = index! - 1
        let count = controllers.count-1
        guard previusIndex >= 0 else {
            return nil
        }
        guard count > previusIndex else {
            return nil
        }
        return controllers[previusIndex]
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let vc = self.viewControllers!.last!
        let index: Int = controllers.indexOf(vc)!
        self.foodDelegate?.didChangePage(index)
    }
    
    func initControllers() -> [UIViewController] {
        let entregaController = storyboard?.instantiateViewControllerWithIdentifier("FoodListTableEntregaController") as? FoodListTableEntregaController
        entregaController?.delegateFood = self
        entregaController?.address = self.address
//        let navVC = UINavigationController(rootViewController: entregaController!)
//        navVC.navigationBarHidden = true
        
        let retiradaController = storyboard?.instantiateViewControllerWithIdentifier("FoodListTableRetiradaController") as? FoodListTableRetiradaController
        retiradaController?.delegateFood = self
        
        let comandaController = storyboard?.instantiateViewControllerWithIdentifier("FoodListTableComandaController") as? FoodListTableComandaController
        comandaController?.delegateFood = self
        
        let list = [entregaController!, retiradaController!, comandaController!]
        return list
    }
    
    func loadUnities(tmpAddress: Address? = nil) {
        let api = ApiServices()
        api.successCase = {(obj) in
            
            Util.print("Data: \((obj)!)")
            
            let array = obj as! NSArray
            for uni in array {
                if let newObj = uni as? NSDictionary {
                    let newUnity = NSMutableDictionary(dictionary: newObj)
                    if let zip = self.address!.zip {
                        newUnity.setObject(zip, forKey: "zip")
                    }
                    UnityPersistence.importFromDictionary(newUnity)
                }
            }

            
//            let dadosEntrega = UnityPersistence.getUnitiesByCep((self.address?.zip)!)
//            let datasourceEntrega = self.controllers[0] as! FoodListTableEntregaController
//            let datasourceEntrega = navVC.viewControllers.first as! FoodListTableEntregaController
//            datasourceEntrega.dados = dadosEntrega
//            datasourceEntrega.address = self.address
            
            self.loadUnitiesData()
        }
        api.failureCase = {(obj) in
            if let controller = UIApplication.sharedApplication().keyWindow?.rootViewController {
                let alert = FoodViewAlert.init(controller: controller)
                alert.showConnectionError()
            }
            self.foodDelegate?.didFinishedLoadData(false)
        }
        if let address = address {
            if  address.zip != nil {
                api.getUnitiesByCEP(address)
            }
        }  else {
            self.loadUnitiesData()
        }
    }
    
    private func loadUnitiesData(tmpAddres: Address? = nil) {
        let dadosRetirada = UnityPersistence.getUnitiesByOrderType("orderTakeAwayEnabled")
        let dadosComanda = UnityPersistence.getUnitiesByOrderType("orderCheckEnabled")
        if let tmpAddres = tmpAddres {
            let location = CLLocation.init(latitude: tmpAddres.latitude, longitude: tmpAddres.longitude)
            FoodUtils.setDistanceOffline(dadosRetirada, location: location)
            FoodUtils.setDistanceOffline(dadosComanda, location: location)
        } else if let userLocation = FoodLocationManager.shared.location {
            FoodUtils.setDistanceOffline(dadosRetirada, location: userLocation)
            FoodUtils.setDistanceOffline(dadosComanda, location: userLocation)
        }
        
        let datasourceRetirada = self.controllers[1] as! FoodListTableRetiradaController
        datasourceRetirada.dados = dadosRetirada
        
        let datasourceComanda = self.controllers[2] as! FoodListTableComandaController
        datasourceComanda.dados = dadosComanda
        
        self.foodDelegate?.didFinishedLoadData(true)
    }
    
    private func loadUnity(indexPath: NSIndexPath, unity: Unity) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController

        let api = ApiServices()
        api.getUnityByIDService(unity.id)
        
        api.successCase = {(obj) in
            
            detailsVc.title = unity.name
            detailsVc.idObjectReceived = unity.id
            detailsVc.typeObjet = TypeObjectDetail.Store
            if self.navigationController?.topViewController != detailsVc {
                self.navigationController?.pushViewController(detailsVc, animated: true)
            }
        }
        
        api.failureCase = {(msg) in
        }
    }
    
}

//MARK: TableEntregaDelegate
extension FoodPageControllerEcs: TableEntregaControllerDelegate {
    func didCallRefreshEntrega(tempAddress: Address?) {
         foodDelegate?.didStartLoadingData()
    }
    func didPressEntregaCell(indexPath: NSIndexPath, unity: Unity) {
        loadUnity(indexPath, unity: unity)
    }
    func didPressAddressGadget() {
        foodDelegate?.didPressEntregaAddress()
    }
}
//MARK: TableComandaDelegate
extension FoodPageControllerEcs: TableComandaControllerDelegate {
    func didCallRefreshComanda(tempAddress: Address?) {
        self.loadUnities()
        foodDelegate?.didStartLoadingData()
    }
    func didPressComandaCell(indexPath: NSIndexPath, unity: Unity) {
        loadUnity(indexPath, unity: unity)
    }
    func callRefreshViewComanda() {
        foodDelegate?.didStartLoadingData()
    }
}
//MARK: TableRetiradaDelegate
extension FoodPageControllerEcs: TableRetiradaControllerDelegate {
    func didCallRefreshRetirada(tempAddress: Address?) {
        self.loadUnities()
        foodDelegate?.didStartLoadingData()
    }
    func didPressRetiradaCell(indexPath: NSIndexPath, unity: Unity) {
        loadUnity(indexPath, unity: unity)
    }

}


