//
//  CartAddressTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/27/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CartAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var labelNickname: UILabel! {
        didSet {
            labelNickname.text = "ENDEREÇO"
        }
    }
    @IBOutlet weak var labelStreet: UILabel!
    @IBOutlet weak var labelNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelNickname.font = LayoutManager.primaryFontWithSize(18)
        labelStreet.font = LayoutManager.primaryFontWithSize(16)
        labelNumber.font = LayoutManager.primaryFontWithSize(16)
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
