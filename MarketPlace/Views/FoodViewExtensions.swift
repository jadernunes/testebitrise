//
//  UIViewExtensions.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 20/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension UIView {
    
    func slidePush(duration: NSTimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        // Create a CATransition
        let slidePushTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate
        if let delegate: CAAnimationDelegate = completionDelegate as? CAAnimationDelegate {
            slidePushTransition.delegate = delegate
        }
        
        // animation's properties
        slidePushTransition.type = kCATransitionPush
        slidePushTransition.subtype = kCATransitionFromRight
        slidePushTransition.duration = duration
        slidePushTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slidePushTransition.fillMode = kCAFillModeRemoved
        
        // Add it to the View's layer
        self.layer.addAnimation(slidePushTransition, forKey: "slidePushTransition")
    }
}
