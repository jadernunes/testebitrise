//
//  FoodRoundedView.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 03/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable public class FoodRoundedView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var shadowEnabled: Bool = true {
        didSet {
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.blackColor()
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 0.03 * bounds.size.width
        if shadowEnabled {
            layer.shadowColor = self.shadowColor.CGColor
            layer.shadowOffset = CGSize(width: 0, height: 3)
            layer.shadowOpacity = 5
            layer.shadowRadius = 1
        }
        clipsToBounds = false //para sombra
    }

}

