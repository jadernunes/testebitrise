//
//  FoodViewGeolocationError.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 16/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodViewGeolocationErrorDelegate {
    func didPressAtivarGeolocalizacao(button: UIButton) -> ()
    func didPressInformarEndereco(button: UIButton) -> ()
    func didMoveToView() -> ()
}

class FoodViewGeolocationError: UIView {
    
    var delegate:FoodViewGeolocationErrorDelegate?
    
    @IBOutlet weak var imgViewIcon: UIImageView!
    
    @IBOutlet weak var lblMessage: UILabel!{
        didSet {
            let text = NSLocalizedString("ViewGeolocationErrorMessage", tableName: "GastronomiaStrings", comment: "")
            self.lblMessage.text = text
        }
    }
    
    @IBOutlet weak var lblMessage2: UILabel! {
        didSet {
            let text = NSLocalizedString("ViewGeolocationErrorMessage2", tableName: "GastronomiaStrings", comment: "")
            self.lblMessage2.text = text
        }
    }
    
    @IBOutlet weak var constraintBtnAtivarGeoBottom  : NSLayoutConstraint!
    @IBOutlet weak var btnAtivarGeolocalizacao: FoodRoundButton! {
        didSet {
            self.btnAtivarGeolocalizacao.titleLabel?.text = NSLocalizedString("btnAtivarGeolocalizacao", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBOutlet weak var btnInformarEndereco: FoodRoundButton! {
        didSet {
            self.btnInformarEndereco.titleLabel?.text = NSLocalizedString("btnInformarEndereco", tableName: "GastronomiaStrings", comment: "")
        }
    }
    
    @IBAction func didPressAtivarGeolocalizacao(button: UIButton) -> () {
        self.delegate?.didPressAtivarGeolocalizacao(button)
    }
    
    @IBAction func didPressInformarEndereco(button: UIButton) -> () {
        self.delegate?.didPressInformarEndereco(button)
    }
    
    class func instanceFromNib() -> FoodViewGeolocationError {
        let view = UINib(nibName: "FoodViewGeolocationError", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FoodViewGeolocationError
        return view
    }
    
    override func didMoveToSuperview() {
        self.delegate?.didMoveToView()
    }
    
}
