//
//  CheckTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CheckTableViewCell: UITableViewCell {

    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var labelProduct: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelQuantity.font = LayoutManager.primaryFontWithSize(18)
        self.labelQuantity.textColor = LayoutManager.labelFirstColor
        
        self.labelProduct.font = LayoutManager.primaryFontWithSize(18)
        self.labelProduct.textColor = LayoutManager.labelFirstColor
        
        self.labelPrice.font = LayoutManager.primaryFontWithSize(18)
        self.labelPrice.textColor = LayoutManager.labelFirstColor
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
