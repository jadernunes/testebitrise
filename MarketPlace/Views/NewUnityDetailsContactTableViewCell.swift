//
//  NewUnityDetailsContactTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 06/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class NewUnityDetailsContactTableViewCell : UITableViewCell, ConfigurableWithUnity {
    
    enum Contact: Int {
        case address = 0, phone
    }
    
    var unity: Unity!
    
    var contacts = [Contact]()
    private var referenceToSuperViewController: UIViewController!
    
    private var address: String?
    private var phone:  String?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.bounces          = false
            self.tableView.scrollEnabled    = false
            self.tableView.separatorStyle   = .None
            self.tableView.dataSource       = self
            self.tableView.delegate         = self
            
            self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_HEADER_IDENTIFIER,
                bundle: NSBundle.mainBundle()),
                                       forHeaderFooterViewReuseIdentifier: NEW_HEADER_UNITY_DETAILS_IDENTIFIER)
            self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_ADDRESS_IDENTIFIER,
                bundle: NSBundle.mainBundle()),
                                       forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_ADDRESS_IDENTIFIER)
            self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_PHONE_IDENTIFIER,
                bundle: NSBundle.mainBundle()),
                                       forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_PHONE_IDENTIFIER)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func getHeight() -> CGFloat {
        var height: CGFloat = 0.0
        
        if let _  = self.address {
            height += NewUnityDetailsAddressTableViewCell.getHeight()
        }
        
        if let _ = self.phone {
            height += NewUnityDetailsPhoneTableViewCell.getHeight()
        }
        
        return NewUnityDetailsHeader.getHeight() + height
    }
    
    func configureWithUnity(unity: Unity, withReference: UIViewController?) {
        self.unity = unity
        self.referenceToSuperViewController = withReference!
        
        self.contacts = [Contact]()
        
        if let address = unity.address where !address.isEmpty {
            self.address = address
            self.contacts.append(.address)
        }
        
        if let phone = unity.phoneNumber where !phone.isEmpty {
            self.phone = phone
            self.contacts.append(.phone)
        }
    }
    
    func alertClose(gesture: UITapGestureRecognizer) {
        self.referenceToSuperViewController.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension NewUnityDetailsContactTableViewCell : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case Contact.address.rawValue:
            let addressCell = tableView.dequeueReusableCellWithIdentifier(NEW_CELL_UNITY_DETAILS_ADDRESS_IDENTIFIER) as! NewUnityDetailsAddressTableViewCell
            addressCell.configureWithAddress(self.address,
                                             latitude: self.unity.latitude,
                                             longitude:  self.unity.longitude,
                                             last: indexPath.row == (self.contacts.count - 1))
            return addressCell
            
        case Contact.phone.rawValue:
            let phoneCell = tableView.dequeueReusableCellWithIdentifier(NEW_CELL_UNITY_DETAILS_PHONE_IDENTIFIER) as! NewUnityDetailsPhoneTableViewCell
            phoneCell.configureWithPhone(self.phone,
                                         last: indexPath.row == (self.contacts.count - 1))
            return phoneCell
            
        default:
            return UITableViewCell()
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let address = self.address where !address.isEmpty {
            return 1
        }
        if let phone = self.phone where !phone.isEmpty {
            return 1
        }
        
        return 0
    }
}

extension NewUnityDetailsContactTableViewCell : UITableViewDelegate {
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier(NEW_HEADER_UNITY_DETAILS_IDENTIFIER) as! NewUnityDetailsHeader
            header.configureWithTitle(.contact)
            return header
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
            case Contact.address.rawValue:
            return NewUnityDetailsAddressTableViewCell.getHeight()
            
            case Contact.phone.rawValue:
            return NewUnityDetailsPhoneTableViewCell.getHeight()
            
        default:
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return NewUnityDetailsHeader.getHeight()
        default:
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        tableView.scrollEnabled = false

        switch indexPath.row {
            case Contact.address.rawValue:
                let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsAddressTableViewCell
                cell.highlight()
            break
            
            case Contact.phone.rawValue:
                 let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsPhoneTableViewCell
                 cell.highlight()
            break
            
        default:
            break
        }
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        tableView.scrollEnabled = true
        
        switch indexPath.row {
        case Contact.address.rawValue:
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsAddressTableViewCell
            cell.unHighlight()
            break
            
        case Contact.phone.rawValue:
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsPhoneTableViewCell
            cell.unHighlight()
            
        default:
            break
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.row {
        case Contact.address.rawValue:
            if let alert = Util.callMaps(CLLocationCoordinate2DMake(latitude, longitude),
                                         placeName: self.address ?? "",
                                         alertControllerStyle: .ActionSheet,
                                         viewController: self.referenceToSuperViewController) {
                
                self.referenceToSuperViewController.presentViewController(alert, animated: true, completion: {
                    alert.view.superview?.userInteractionEnabled = true
                    alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertClose(_:))))
                })
            }
            break
            
        case Contact.phone.rawValue:
            if let url = NSURL(string: "tel://\(self.phone ?? "")") {
                if UIApplication.sharedApplication().canOpenURL(url) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            /// Just for visual effects
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsPhoneTableViewCell
            cell.unHighlight()
            break
            
        default:
            break
        }
    }
}
