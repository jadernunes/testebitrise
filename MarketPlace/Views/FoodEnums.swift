//
//  FoodEnums.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation


enum SegueToVC: String {
    case FoodListEC         = "segueFoodListEC"
    case FoodDelivery       = "segueFoodDelivery"
    case FoodGeoLocation    = "segueFoodGeoLocationVC"
    case FoodAddress        = "segueFoodAddressVC"
    
    static func defaultVC() -> String {
        return SegueToVC.FoodGeoLocation.rawValue
    }
}
