//
//  NewUnityDetailsAddressTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 06/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import CoreLocation
import UberRides

class NewUnityDetailsAddressTableViewCell: UITableViewCell {
    
    var buttonUber: RideRequestButton!
    
    @IBOutlet weak var heightConstraintViewHair: NSLayoutConstraint! {
        didSet {
            self.heightConstraintViewHair.constant = 0.4
        }
    }
    
    @IBOutlet weak var imageViewIcon: UIImageView! {
        didSet {
            self.imageViewIcon.tintImage(SessionManager.sharedInstance.cardColor)
        }
    }
    
    @IBOutlet weak var labelAddress: UILabel! {
        didSet {
            self.labelAddress.font = LayoutManager.primaryFontWithSize(16)
            self.labelAddress.textColor = LayoutManager.black
            self.labelAddress.text = ""
            self.labelAddress.alpha = 0.0
        }
    }
    
    @IBOutlet weak var labelHowToGetThere: UILabel! {
        didSet {
            self.labelHowToGetThere.font = LayoutManager.primaryFontWithSize(16)
            self.labelHowToGetThere.textColor = SessionManager.sharedInstance.cardColor
            self.labelHowToGetThere.text = .howToGetThere
            self.labelHowToGetThere.alpha = 0.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func getHeight() -> CGFloat {
        return .heightUnityDetailsAddressTableViewCell + 60.0
    }
    
    func configureWithAddress(address: String?, latitude: Double?, longitude: Double?, last: Bool) {
        
        let uber = RideRequestButton()
        self.buttonUber = uber
        self.buttonUber.alpha = 0.0
        
        if let latitude = latitude, longitude = longitude {
            let dropoffLocation = CLLocation(latitude: latitude, longitude: longitude)
            
            let builder = RideParametersBuilder()
                .setDropoffLocation(dropoffLocation,
                                    nickname: "")
            buttonUber.rideParameters = builder.build()
        }
        
        
        self.contentView.addSubview(buttonUber)
        buttonUber.translatesAutoresizingMaskIntoConstraints = false
        
        let constraintLeadingToUpperLabel =
            NSLayoutConstraint(item: self.buttonUber,
                               attribute: .Leading,
                               relatedBy: .Equal,
                               toItem: self.labelHowToGetThere,
                               attribute: .Leading,
                               multiplier: 1,
                               constant: 0)
        
        let constraintBottomToUpperLabel =
            NSLayoutConstraint(item: self.buttonUber,
                               attribute: .Bottom,
                               relatedBy: .Equal,
                               toItem: self.contentView,
                               attribute: .Bottom,
                               multiplier: 1,
                               constant: -16)
        
        let constraintHeight =
            NSLayoutConstraint(item: self.buttonUber,
                               attribute: .Height,
                               relatedBy: .Equal,
                               toItem: nil,
                               attribute: .NotAnAttribute,
                               multiplier: 1,
                               constant: buttonUber.frame.height)
        
        
        let constranitEqualsWidth =
            NSLayoutConstraint(item: self.buttonUber,
                               attribute: .Width,
                               relatedBy: .Equal,
                               toItem: self.labelAddress,
                               attribute: .Width,
                               multiplier: 1,
                               constant: 0)
        
        self.contentView.addConstraints([constraintLeadingToUpperLabel, constraintBottomToUpperLabel, constraintHeight, constranitEqualsWidth])
        
        if let _ = address {
            self.labelAddress.text = address
        }
        
        if last {
             self.heightConstraintViewHair.constant = 1.0
        }
        self.present()
    }
    
    private func present() {
        self.layoutIfNeeded()
        UIView.animateWithDuration(0.5) {
            self.labelAddress.alpha = 1.0
            self.labelHowToGetThere.alpha = 1.0
            self.buttonUber.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
    
    func highlight() {
        self.labelAddress.textColor = UIColor.whiteColor()
        self.labelHowToGetThere.textColor = UIColor.whiteColor()
        self.imageViewIcon.tintImage(UIColor.whiteColor())
        self.contentView.backgroundColor = SessionManager.sharedInstance.cardColor
    }
    
    func unHighlight() {
        self.labelHowToGetThere.textColor = SessionManager.sharedInstance.cardColor
        self.imageViewIcon.tintImage(SessionManager.sharedInstance.cardColor)
        self.labelAddress.textColor = LayoutManager.black
        self.contentView.backgroundColor = UIColor.whiteColor()
    }
}
