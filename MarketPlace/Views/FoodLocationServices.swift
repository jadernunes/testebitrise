//
//  FoodLocationServces.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 16/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import CoreLocation

class FoodLocationServices: NSObject {
    
    static func getLocationStringInfo(completionHandler: (address: Address?, errorDescription: String?) -> Void){
        dispatch_async(dispatch_get_main_queue()) {
            if CLLocationManager.locationServicesEnabled() {
                if CLLocationManager.authorizationStatus() == .AuthorizedAlways ||
                CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse{
                    let locationManager = CLLocationManager()
                    if let location = locationManager.location {
                        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                            
                            if error != nil {
                                print("error on locate user: \(error.debugDescription)")
                                return
                            }
                            
                            if placemarks!.count > 0 {
                                if let pm = placemarks!.first {
                                    if let dict = pm.addressDictionary {
                                        let address = Address()
                                        if pm.thoroughfare != nil {
                                            address.street = pm.thoroughfare
                                        }
                                        if pm.locality != nil {
                                            address.city = pm.locality
                                        }
                                        if pm.subLocality != nil {
                                            address.neighborhood = pm.subLocality
                                        }
                                        if pm.administrativeArea != nil {
                                            address.state = pm.administrativeArea
                                        }
                                        if let lat = pm.location?.coordinate.latitude {
                                            address.latitude = lat
                                        }
                                        if let lon = pm.location?.coordinate.longitude {
                                            address.longitude = lon
                                        }
                                        if let formatedData = dict["FormattedAddressLines"] as? [AnyObject] {
                                            if let cep = formatedData[3] as? String {
                                                address.zip = cep
                                            }
                                        }
                                        
                                        completionHandler(address: address, errorDescription: nil)
                                    }
                                }
                            } else {
                                completionHandler(address: nil, errorDescription: "Location not found")
                            }
                            
                        })
                    } else {
                        completionHandler(address: nil, errorDescription: "Location not found")
                    }
                }
            }
        }
    }
    
}
