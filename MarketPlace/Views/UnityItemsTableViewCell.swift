//
//  UnityItemsTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 10/1/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class UnityItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonShowItems  : UIButton!
    
    var rootViewController : UIViewController!
    var contentArray       = NSArray()
    var unity              : Unity!
    var scheduleLoaded      = false
    var itemLoaded          = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor                = UIColor.clearColor()
        buttonShowItems.titleLabel?.font    = LayoutManager.primaryFontWithSize(16)
        // Rounded corners.
        buttonShowItems.layer.cornerRadius = 4
        buttonShowItems.clipsToBounds      = false
        buttonShowItems.layer.borderWidth  = 1
        
        let imageCatalogIcon = UIImage(named: "ic_catalog")
        dispatch_async(dispatch_get_main_queue(), {
            self.buttonShowItems.layoutIfNeeded()
            self.buttonShowItems.setIcon(imageCatalogIcon!, inLeft: true)
            self.buttonShowItems.addActivityIndicator(false)
        })
        self.buttonShowItems.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
      
        self.buttonShowItems.layer.borderColor  = UIColor.lightGrayColor().CGColor
        self.buttonShowItems.enabled = false
        self.scheduleLoaded = false
        self.itemLoaded = false
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.setEnabledItem),
            name: UNITY_ITEMS_OBSERVER,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.setEnabledSchedule),
            name: UNITY_SCHEDULE_OBSERVER,
            object: nil)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setEnabledItem() {
        self.itemLoaded = true
        if scheduleLoaded == true {
            self.setEnabledButton()
        }
    }
    
    func setEnabledSchedule() {
        self.scheduleLoaded = true
        if itemLoaded == true {
            self.setEnabledButton()
        }
    }
    
    func setEnabledButton() {
        self.buttonShowItems.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        self.buttonShowItems.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        self.buttonShowItems.enabled = true
    }
    
    func setLabelText(unity: Unity) {
        if (unity.orderEnabled == true && unity.schedulingEnabled == false) {
            self.buttonShowItems.setTitle("Faça seu Pedido", forState: .Normal)
        }
        else if (unity.orderEnabled == false && unity.schedulingEnabled == true) {
            self.buttonShowItems.setTitle("Agende seu Serviço", forState: .Normal)
        }
        else if (unity.orderEnabled == true && unity.schedulingEnabled == true) {
            self.buttonShowItems.setTitle("Agendamento / Pedidos", forState: .Normal)
        }
    }
    
    @IBAction func openItemsInStore(sender: UIButton) {
        if buttonShowItems.enabled == true {
            NSNotificationCenter.defaultCenter().postNotificationName(CELL_BUTTON_ITEMS_CLICKED, object: nil)
            sender.enabled = false
        }
    }
}
