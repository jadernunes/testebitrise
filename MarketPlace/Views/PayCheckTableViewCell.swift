//
//  PayCheckTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/25/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class PayCheckTableViewCell: UITableViewCell, ScannerQrCodeProtocol {

    @IBOutlet weak var buttonPayCheck: UIButton!
    
    var rootViewController : UIViewController!
    var unity              : Unity!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor                = UIColor.clearColor()
        buttonPayCheck.titleLabel?.font    = LayoutManager.primaryFontWithSize(16)
        // Rounded corners.
        buttonPayCheck.layer.cornerRadius = 4
        buttonPayCheck.clipsToBounds      = false
        // A thin border.
        buttonPayCheck.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonPayCheck.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonPayCheck.layer.borderWidth  = 1
        /*let imageCatalogIcon = UIImage(named: "ic_qr_home")
        dispatch_async(dispatch_get_main_queue(), {
            self.buttonPayCheck.layoutIfNeeded()
            self.buttonPayCheck.setIcon(imageCatalogIcon!, inLeft: true)
        })*/
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonPayCheckClicked(sender: AnyObject) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
        (vc.viewControllers[0] as! ScannerViewController).delegate = self
        (vc.viewControllers[0] as! ScannerViewController).titleString = "PAGAR COMANDA"
        rootViewController.presentViewController(vc, animated: true, completion: nil)
    }
    

    func didScanInvalidCode() {
        Util.showAlert(rootViewController, title: "Atenção", message: "Comanda Inválida!")
    }
    
    func didScanValidCode(value: String) {
      //  let qrRead = value.componentsSeparatedByString("_")
     //   if (qrRead.contains("CHK")) {
        
        if value.isNumeric {
            let vc = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("CheckViewController") as! CheckViewController
            vc.placeLabel = value
            vc.idUnity = self.unity.id
            vc.rootViewController = rootViewController
            rootViewController.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            Util.showAlert(rootViewController, title: "Atenção", message: "Comanda Inválida!")
        }
    //    }
    //    else {
    //        let alert = UIAlertController(title: "Erro", message: "Comanda Inválida", preferredStyle: UIAlertControllerStyle.Alert)
   //         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
   //         rootViewController.presentViewController(alert, animated: true, completion: nil)
   //     }
    }
}
