//
//  FidelityTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 11/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class FidelityTableViewCell: UITableViewCell {

    @IBOutlet weak var viewShowStamp: UIView!
    var rootViewController : UIViewController!
    var unity              : Unity!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clearColor()
        self.viewShowStamp.layer.masksToBounds = true
        self.viewShowStamp.layer.cornerRadius = 10
        self.viewShowStamp.layer.shadowOffset = CGSizeMake(0, 20);
        self.viewShowStamp.layer.shadowRadius = 15;
        self.viewShowStamp.layer.shadowOpacity = 0.3
        self.viewShowStamp.layer.masksToBounds = true
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.updateCell),
            name: UNITY_FIDELITY_OBSERVER,
            object: nil)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateCell() {
        
        let fidelity = MyFidelityPersistence.getMyFidelityByIdUnity(unity.id)
        
        let viewStamp = FidelityView.instanceFromNib()
        viewStamp.maxItems = unity.fidelity?.totalStamp
        self.viewShowStamp.layer.borderWidth = 0.5
        self.viewShowStamp.layer.borderColor = SessionManager.sharedInstance.cardColor.CGColor
        
        if fidelity != nil {
            viewStamp.countItems = fidelity?.stampCount
        } else {
            viewStamp.countItems = 0
        }
        viewStamp.urlImage = unity.fidelity?.thumb
        viewStamp.descriptionTerms = unity.fidelity?.terms
        
        let unityFidelity = UnityFidelityPersistence.getUnityFidelityByIdUnity(unity.id)
        var description = ""
        
        if fidelity?.descriptionFidelity?.characters.count > 0 {
            description = (fidelity?.descriptionFidelity)!
        } else if unityFidelity != nil {
            description = (unityFidelity?.desc)!
        }
        
        viewStamp.labelDesc.text = description
        let deviceBounds = UIScreen.mainScreen().bounds
        viewStamp.frame = CGRectMake(0, 0, deviceBounds.width-20, Util.getHeightViewFidelityByCountItems((unityFidelity?.totalStamp)!))
        self.viewShowStamp.addSubview(viewStamp)
        self.viewShowStamp.setNeedsLayout()
        self.viewShowStamp.layoutIfNeeded()
        viewStamp.setNeedsLayout()
        viewStamp.layoutIfNeeded()
    }
}
