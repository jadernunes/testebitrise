//
//  File.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 02/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodValidations: NSObject {
    
    /// Validate the pre requisites to run the Gastronomia
    ///
    /// - Returns: a boolean that means if the controller can move tho target view
    func verify() -> Bool {
        if self.verifyUserLocation() {
            
        }
        
        if self.verifyUserConnection() {
            
        }
        
        return true
    }
    
    
    /// Validate user location
    ///
    /// - Returns: return a boolean that say if the user has location activated
    private func verifyUserLocation() -> Bool {
        return true
    }
    
    
    /// Validate user connection
    ///
    /// - Returns: return a boolean that say if the user has a saved address
    private func verifyUserConnection() -> Bool {
        return true
    }
    
    
    /// Verify if the user CEP has only numbers
    ///
    /// - Parameter cep: String or Textfield
    /// - Returns: Returns if the CEP is valid
    func verifyUserCEP(cep:AnyObject) -> Bool {
        if let cep = cep as? UITextField  {
            if let str = cep.text {
                return self.checkIfCPFisValid(str)
            }
        } else if let cep = cep as? String {
            return self.checkIfCPFisValid(cep)
        }
        
        return false
    }
    
    
    /// Verify if the user CPF contains only numbers
    ///
    /// - Parameter obj: String or a Textfield
    /// - Returns: Returns a boolan indicating if the CPF is valid
    private func checkIfCPFisValid(obj: AnyObject?) -> Bool
    {
        var input: String = ""
        
        if let object = obj as? UITextField {
            input = object.text!
        }
        
        if let str = obj as? String {
            input = str
        }
        
        if (input.isEmpty) {
            return false
        }
        
        let cpf = input.characters.filter{"0123456789".characters.contains($0)}
        if cpf.count == 8 {
            return true
        }
        
        return false
    }
    
    
    /// Verify if the string is a numeric one
    ///
    /// - Parameter string: String to be tested
    /// - Returns: Returns a boolean that indicate if is numerical or not
    static func isStringNumerical(string : String) -> Bool {
        // Only allow numbers. Look for anything not a number.
        let range = string.rangeOfCharacterFromSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        return (range == nil)
    }
    
    static func shouldShowAddressAlertBalloon() -> Bool {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.boolForKey(kFoodBalloonAlert) == true {
            return false
        } else {
            if FoodUtils.validateAddress() {
                userDefaults.setBool(true, forKey: kFoodBalloonAlert)
                return true
            } else {
                return false
            }
        }
    }
    
}
