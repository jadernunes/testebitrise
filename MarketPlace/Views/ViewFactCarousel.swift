//
//  ViewFactCarousel.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 02/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class ViewFactCarousel: UIView {
    
    @IBOutlet weak var imageViewThumb: UIImageView!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelHour: UILabel!
    @IBOutlet weak var viewBackgroundContainerDate: UIView!
    @IBOutlet weak var viewContainerDate: UIView!
    
    private var fact:Fact!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    class func instanceFromNib(fact:Fact) -> ViewFactCarousel {
        let nib = UINib(nibName: "ViewFactCarousel", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! ViewFactCarousel
        nib.fact = fact
        if nib.fact.season == true {
            nib.viewBackgroundContainerDate.hidden = true
            nib.viewContainerDate.hidden = true
        }
        return nib
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        labelMonth.font            = LayoutManager.primaryFontWithSize(14)
        labelMonth.textColor       = LayoutManager.regularFontColor
        labelHour.font             = LayoutManager.primaryFontWithSize(14)
        labelHour.textColor        = LayoutManager.regularFontColor
        labelDay.font              = LayoutManager.primaryFontWithSize(50)
        labelDay.textColor         = LayoutManager.regularFontColor
        viewBackgroundContainerDate.backgroundColor = SessionManager.sharedInstance.cardColor.colorWithAlphaComponent(0.8)
    }
    
    func populateData() {
        
        dispatch_async(dispatch_get_main_queue(),{
            let urlString = self.fact.thumb != nil ? self.fact.thumb : ""
            self.imageViewThumb.addImage(urlString)
            
            self.labelDay.text                  = self.fact.getFormatedDateString(false, pattern: "dd")
            self.labelHour.text                 = "às \(self.fact.getFormatedDateString(false, pattern: "HH:mm"))hs".uppercaseString
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            self.labelMonth.text                = months[Int(self.fact.getFormatedDateString(false, pattern: "MM"))!-1].uppercaseString
        })
    }
}
