//
//  FoodViewTabSelector.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 21/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

protocol FoodViewTabSelectorDelegate {
    func didPressButtonView(datasource: kTableViewDataSource)
}

class FoodViewTabSelector: UIView {
    
    var delegate:FoodViewTabSelectorDelegate?
    
    @IBOutlet weak var viewBtnEntrega: UIView!
    @IBOutlet weak var viewBtnRetirada: UIView!
    @IBOutlet weak var viewBtnComanda: UIView!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var lblBtnEntrega: UILabel!
    @IBOutlet weak var lblBtnRetirada: UILabel!
    @IBOutlet weak var lblBtnComanda: UILabel!
    @IBOutlet weak var imgIconEntrega: UIImageView!
    @IBOutlet weak var imgIconRetirada: UIImageView!
    @IBOutlet weak var imgIconComanda: UIImageView!
    
    @IBOutlet var view: UIView!
    
    var currentDatasource = kTableViewDataSource.Undefined {
        didSet {
            let realm = try! Realm()
            var cart = OrderEntityManager.sharedInstance.getCart(0)
            if cart == nil {
                //caso não tenha order com status 0, é criado um novo objeto do tipo Order
                let verifyCart = OrderEntityManager.sharedInstance.hasActiveCart()
                if verifyCart.active == false {
                    realm.beginWrite()
                    cart = realm.create(Order.self,value: ["id":OrderEntityManager.sharedInstance.getNextKey(realm, aClass: Order.self)])
                    cart?.timestamp = NSDate().timeIntervalSince1970
                    cart?.total = 0.0
                    cart?.deliveryFee = 0.0
                    cart?.status = 0
                    try! realm.commitWrite()
                }
            }
            if currentDatasource == .Entrega {
                animateIndicator(viewBtnEntrega.frame, animated: true)
                ajustarLabels(0)
                ajustarIcon(0)
                try! realm.write {
                    cart?.idOrderType = OrderType.Delivery.rawValue
                }
            } else if currentDatasource == .Retirada {
                animateIndicator(viewBtnRetirada.frame, animated: true)
                ajustarLabels(1)
                ajustarIcon(1)
                try! realm.write {
                    cart?.idOrderType = OrderType.TakeAway.rawValue
                }
            } else if currentDatasource == .Comanda {
                animateIndicator(viewBtnComanda.frame, animated: true)
                ajustarLabels(2)
                ajustarIcon(2)
                try! realm.write {
                    cart?.idOrderType = OrderType.Check.rawValue
                }
            }
        }
    }
    
    override func didMoveToWindow() {
        if currentDatasource == .Entrega {
            animateIndicator(viewBtnEntrega.frame, animated: true)
        } else if currentDatasource == .Retirada {
            animateIndicator(viewBtnRetirada.frame, animated: true)
        } else if currentDatasource == .Comanda {
            animateIndicator(viewBtnComanda.frame, animated: true)
        }
    }
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiateView()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiateView()
    }
    
    func instantiateView(initialDatasource: kTableViewDataSource? = .Entrega) {
        UINib(nibName: "FoodViewTabSelector", bundle: nil).instantiateWithOwner(self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        self.setNeedsLayout()
        
        let tapEntrega = UITapGestureRecognizer.init(target: self, action: #selector(didPressEntrega(_:)))
        viewBtnEntrega.addGestureRecognizer(tapEntrega)
        let tapRetirada = UITapGestureRecognizer.init(target: self, action: #selector(didPressRetirada(_:)))
        viewBtnRetirada.addGestureRecognizer(tapRetirada)
        let tapComanda = UITapGestureRecognizer.init(target: self, action: #selector(didPressComanda(_:)))
        viewBtnComanda.addGestureRecognizer(tapComanda)
        
        self.currentDatasource = initialDatasource!
    }
    
    class func instanceFromNib(initialDatasource: kTableViewDataSource? = .Entrega) -> FoodViewTabSelector {
        let view = UINib(nibName: "FoodViewTabSelector", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FoodViewTabSelector
        
        let tapEntrega = UITapGestureRecognizer.init(target: self, action: #selector(didPressEntrega(_:)))
        view.viewBtnEntrega.addGestureRecognizer(tapEntrega)
        let tapRetirada = UITapGestureRecognizer.init(target: self, action: #selector(didPressRetirada(_:)))
        view.viewBtnRetirada.addGestureRecognizer(tapRetirada)
        let tapComanda = UITapGestureRecognizer.init(target: self, action: #selector(didPressComanda(_:)))
        view.viewBtnComanda.addGestureRecognizer(tapComanda)
        
        view.currentDatasource = initialDatasource!
        return view
    }
    
    
    func didPressEntrega(viewBtn: UIView) {
        delegate?.didPressButtonView(kTableViewDataSource.Entrega)
        currentDatasource = .Entrega
    }
    func didPressRetirada(viewBtn: UIView) {
        delegate?.didPressButtonView(kTableViewDataSource.Retirada)
        currentDatasource = .Retirada
    }
    func didPressComanda(viewBtn: UIView) {
        delegate?.didPressButtonView(kTableViewDataSource.Comanda)
        currentDatasource = .Comanda
    }

    func animateIndicator(tFrame: CGRect, animated: Bool) {
        if animated {
            UIView.animateWithDuration(0.3) {
                self.moveIndicator(tFrame)
            }
        } else {
            self.moveIndicator(tFrame)
        }
    }
    
    private func moveIndicator(tFrame: CGRect) {
        self.viewIndicator?.frame = CGRect.init(x: tFrame.origin.x,
                                               y: tFrame.size.height-2.0,
                                               width: tFrame.size.width,
                                               height: 2.0)
    }
    
    private func ajustarLabels(target: Int) {
        switch target {
        case 0:
            lblBtnEntrega.textColor = UIColor.foodGreyishBrownColor()
            lblBtnRetirada.textColor = UIColor.foodWarmGreyColor()
            lblBtnComanda.textColor = UIColor.foodWarmGreyColor()
        case 1:
            lblBtnEntrega.textColor = UIColor.foodWarmGreyColor()
            lblBtnRetirada.textColor = UIColor.foodGreyishBrownColor()
            lblBtnComanda.textColor = UIColor.foodWarmGreyColor()
        case 2:
            lblBtnEntrega.textColor = UIColor.foodWarmGreyColor()
            lblBtnRetirada.textColor = UIColor.foodWarmGreyColor()
            lblBtnComanda.textColor = UIColor.foodGreyishBrownColor()
        default:
            lblBtnEntrega.textColor = UIColor.foodGreyishBrownColor()
            lblBtnRetirada.textColor = UIColor.foodWarmGreyColor()
            lblBtnComanda.textColor = UIColor.foodWarmGreyColor()
        }
    }
    
    private func ajustarIcon(target: Int) {
        switch target {
        case 0:
            imgIconEntrega.highlighted = true
            imgIconRetirada.highlighted = false
            imgIconComanda.highlighted = false
        case 1:
            imgIconEntrega.highlighted = false
            imgIconRetirada.highlighted = true
            imgIconComanda.highlighted = false
        case 2:
            imgIconEntrega.highlighted = false
            imgIconRetirada.highlighted = false
            imgIconComanda.highlighted = true
        default:
            imgIconEntrega.highlighted = true
            imgIconRetirada.highlighted = false
            imgIconComanda.highlighted = false
        }
    }
    
}
