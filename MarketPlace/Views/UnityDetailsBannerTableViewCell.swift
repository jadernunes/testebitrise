//
//  UnityDetailsBannerTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 7/20/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class UnityDetailsBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var imageBanner      : UIImageView!
    @IBOutlet weak var viewMap          : UIView!
    @IBOutlet weak var labelName        : UILabel!
    @IBOutlet weak var labelCategory    : UILabel!
    @IBOutlet weak var labelAddress     : UILabel!
    @IBOutlet weak var labelInfo        : UILabel!
    @IBOutlet weak var buttonFacebook   : UIButton!
    @IBOutlet weak var buttonTwitter    : UIButton!
    @IBOutlet weak var labelStatusOpen  : UILabel!
    @IBOutlet weak var imageViewHeart: UIImageView!
    @IBOutlet weak var imageViewMap: UIImageView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var imageLine: UIView!

    var referenceToSuperViewController  : UIViewController!
    var unity                           : Unity!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor         = LayoutManager.backgroundFirstColor
        self.labelName.font          = LayoutManager.primaryFontWithSize(24)
        self.labelName.textColor     = LayoutManager.white
        self.labelCategory.font      = LayoutManager.primaryFontWithSize(14)
        self.labelCategory.textColor = LayoutManager.white
        self.labelAddress.font       = LayoutManager.primaryFontWithSize(16)
        self.labelAddress.textColor  = LayoutManager.labelSecondColor
        self.labelInfo.font          = LayoutManager.primaryFontWithSize(16)
        self.labelInfo.textColor     = LayoutManager.labelSecondColor
        self.labelInfo.numberOfLines = 2
        self.labelStatusOpen.font               = LayoutManager.primaryFontWithSize(14)
        self.labelStatusOpen.textColor          = UIColor.whiteColor()
        self.labelStatusOpen.layer.cornerRadius = 10
        self.labelStatusOpen.clipsToBounds      = true
        self.viewLine.backgroundColor = SessionManager.sharedInstance.cardColor
        self.imageLine.backgroundColor = SessionManager.sharedInstance.cardColor
        self.imageViewMap.tintImage(SessionManager.sharedInstance.cardColor)
        self.imageViewHeart.tintImage(SessionManager.sharedInstance.cardColor)
        self.buttonTwitter.tintButton(SessionManager.sharedInstance.cardColor, image: UIImage(named: "ic_twitter")!)
        self.buttonFacebook.tintButton(SessionManager.sharedInstance.cardColor, image: UIImage(named: "ic_facebook")!)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UnityDetailsBannerTableViewCell.openMapIndoor))

        viewMap.userInteractionEnabled = true
        viewMap.addGestureRecognizer(tapGesture)
        self.selectionStyle = .None;
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, unity : Unity, referenceViewController : UIViewController) -> UnityDetailsBannerTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(STORE_BANNER_DETAILS_IDENTIFIER) as! UnityDetailsBannerTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        tableView.backgroundColor = LayoutManager.backgroundFirstColor
        cell.referenceToSuperViewController = referenceViewController
        cell.unity = unity
        cell.labelStatusOpen.hidden = !unity.orderEnabled

        var url     = ""
        var phone   = ""
        var site    = ""
        //Check if it should download thumb or externalThumb
        
        if unity.getThumb() != nil {
            if unity.getThumb()!.value != nil && unity.getThumb()!.value?.characters.count > 0 {
                url = unity.getThumb()!.value!
            }
        }
        cell.imageBanner.clipsToBounds = true
        cell.imageBanner.contentMode = .ScaleAspectFill
        cell.imageBanner.addImage(url) { (image: UIImage!,error: NSError!,cacheType: SDImageCacheType,url: NSURL!) in
            if image != nil {
                cell.imageBanner.contentMode = .ScaleAspectFill
            }
        }
        
        cell.labelName.text = unity.name
        if unity.address != nil && unity.address != "" {
            cell.labelAddress.text = unity.address?.capitalizedString
        }else{
            cell.labelAddress.text = "Endereço não informado"
        }
        
        if unity.phoneNumber != nil {
            phone = unity.phoneNumber!
        }
        
        if unity.siteUrl != nil {
            site = unity.siteUrl!
        }
        cell.labelInfo.text     = "Fone: \(phone)\n\(site.lowercaseString)"
        
        cell.buttonFacebook.hidden = !cell.checkUrlIsValid(unity.facebookUrl)
        
        cell.buttonTwitter.hidden = !cell.checkUrlIsValid(unity.twitterUrl)
        
        cell.labelCategory.hidden = true
        
        if unity.opened {
            cell.labelStatusOpen.backgroundColor    = UIColor().hexStringToUIColor("#03c318")
            cell.labelStatusOpen.text               = "Aberto"
        }else{
            cell.labelStatusOpen.backgroundColor    = UIColor().hexStringToUIColor("#C50C18")
            cell.labelStatusOpen.text               = "Fechado"
        }
        
        return cell
    }


    @IBAction func openFacebook(sender: AnyObject) {
        var url : NSURL
        
        if unity.facebookUrl!.lowercaseString.containsString("http://") ||
            unity.facebookUrl!.lowercaseString.containsString("https://") {
            url = NSURL(string: unity.facebookUrl!)!
        }else{
            url = NSURL(string: "http://" + unity.facebookUrl!)!
        }
        
        UIApplication.sharedApplication().openURL(url)

    }
    
    @IBAction func openTwitter(sender: AnyObject) {
        var url : NSURL
        
        if unity.twitterUrl!.lowercaseString.containsString("http://") ||
            unity.twitterUrl!.lowercaseString.containsString("https://") {
            url = NSURL(string: unity.twitterUrl!)!
        }else{
            url = NSURL(string: "http://" + unity.twitterUrl!)!
        }
        
        UIApplication.sharedApplication().openURL(url)
    }
    
    func openMapIndoor() {
        if unity.latitude != 0 && unity.longitude != 0 {
            Util.openMapDefault(CLLocationCoordinate2DMake(unity.latitude, unity.longitude), placeName: unity.name!, viewController: self.referenceToSuperViewController)
        }
    }

    func callPhoneNumber() {
        if unity.phoneNumber != nil {
            let phone:String = unity.phoneNumber!
            if let url = NSURL(string: "telprompt://\(phone)") where UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func checkUrlIsValid(stringUrl : String?) -> Bool {
        
        if stringUrl != nil && stringUrl!.stringByReplacingOccurrencesOfString(" ", withString: "").characters.count > 0{
            if stringUrl!.lowercaseString.containsString("http://") ||
                stringUrl!.lowercaseString.containsString("https://") {
                if let _ = NSURL(string: stringUrl!) {
                    return true
                }else{
                    return false
                }
            }else{
                if let _ = NSURL(string: "http://\(stringUrl!)") {
                    return true
                }else{
                    return false
                }

            }
        }else{
            return false
        }

    }
}
