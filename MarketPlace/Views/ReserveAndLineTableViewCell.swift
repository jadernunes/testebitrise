//
//  ReserveAndLineTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 10/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ReserveAndLineTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainerGetInLine : UIView!
    @IBOutlet weak var viewContainerReserve   : UIView!
    @IBOutlet weak var labelGetInLine         : UILabel!
    @IBOutlet weak var labelLineCount         : UILabel!
    @IBOutlet weak var imageIconGetInLine     : UIImageView!
    @IBOutlet weak var labelReserve           : UILabel!
    @IBOutlet weak var imageIconReserve       : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelGetInLine.font         = LayoutManager.primaryFontWithSize(15)
        labelGetInLine.textColor    = LayoutManager.regularFontColor
        labelReserve.font           = LayoutManager.primaryFontWithSize(15)
        labelReserve.textColor      = LayoutManager.regularFontColor
        labelLineCount.font         = LayoutManager.primaryFontWithSize(15)
        labelLineCount.textColor    = LayoutManager.regularFontColor
        
        self.backgroundColor        = LayoutManager.backgroundLight
        
        // Rounded corners.
        viewContainerGetInLine.layer.cornerRadius   = 4
        viewContainerGetInLine.clipsToBounds        = false
        viewContainerReserve.layer.cornerRadius     = 4
        viewContainerReserve.clipsToBounds          = false
        
        // A thin border.
        viewContainerGetInLine.layer.borderColor    = UIColor.blackColor().CGColor
        viewContainerGetInLine.layer.borderWidth    = 0.0
        viewContainerReserve.layer.borderColor      = UIColor.blackColor().CGColor
        viewContainerReserve.layer.borderWidth      = 0.0
        
        // Drop shadow.
        viewContainerGetInLine.layer.shadowColor   = UIColor.darkGrayColor().CGColor
        viewContainerGetInLine.layer.shadowOpacity = 0.6
        viewContainerGetInLine.layer.shadowRadius  = 1.0
        viewContainerGetInLine.layer.shadowOffset  = CGSizeMake(0, 1)
        viewContainerReserve.layer.shadowColor   = UIColor.darkGrayColor().CGColor
        viewContainerReserve.layer.shadowOpacity = 0.6
        viewContainerReserve.layer.shadowRadius  = 1.0
        viewContainerReserve.layer.shadowOffset  = CGSizeMake(0, 1)
        
        self.selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func getConfiguredCell(tableView : UITableView, unity : Unity) -> ReserveAndLineTableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(UNITY_RESERVE_AND_LINE) as! ReserveAndLineTableViewCell
        
        return cell
    }

    
}
