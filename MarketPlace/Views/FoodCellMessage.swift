//
//  FoodCellMessage.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 26/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodCellMessage: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet var textLabel: UILabel!
    
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiateView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiateView()
    }
    
    func instantiateView() {
        UINib(nibName: "FoodCellMessage", bundle: nil).instantiateWithOwner(self, options: nil)
        addSubview(view)
        view.frame = self.bounds
    }
    
}
