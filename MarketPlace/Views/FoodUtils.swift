//
//  FoodUtils.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 15/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class FoodUtils : NSObject {
    
    /// Personaliza a barra de navegação conforme o padrão da gastronomia
    ///
    /// - Parameters:
    ///   - title: o título a ser exibido na view
    ///   - vc: a view que deseja personalizar
    static func customizeViewTitle(title: String, vc: UIViewController) -> Void {
        vc.title = title
        vc.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        if let navController = vc.navigationController {
            if let font = UIFont(name: "Dosis-Regular", size: 22) {
                let navBarAttributesDictionary: [String: AnyObject]? = [
                    NSForegroundColorAttributeName: UIColor.whiteColor(),
                    NSFontAttributeName: font
                ]
                navController.navigationBar.titleTextAttributes = navBarAttributesDictionary
                FoodUtils.setNavigationBarWithGradient(navController.navigationBar)
            }
        }
    }
    
    /// Formata a distancia passada para texto
    ///
    /// - Parameter distance: a distaência que foi passada pela Unity
    /// - Returns: retorna um texto formatado para: "XXX Km"
    static func formatDistanceText(distance: Double) -> String {
        let distanceFix = distance/1000
        var distanceText = ""
        var medida = ""
        if distanceFix < 1 {
            medida = "Metros"
            distanceText = String(format: "%.1f", distance)
        } else {
            medida = "Km"
            distanceText = String(format: "%.1f", distanceFix)
        }
        
        let finalDistanceText = "\(distanceText) \(medida)"
        return finalDistanceText
    }
    
    /// Verify is the EC is opened based on shift
    ///
    /// - Parameters:
    ///   - shifts: the shifts of the unity
    ///   - opened: the value if is opened
    /// - Returns: return is the EC is opened based in his shifts
    static func isUnityOpen(shifts: Results<Shifts>?,
                             withUnityOpen opened: Bool) -> Bool {
        if !opened {
            return false
        }
        
        if let shifts = shifts {
            if !shifts.isEmpty {
                for shift in shifts {
                    let currentDate = NSDate.currentDateInFormat()
                    if (NSDate.secondsByDifferenceDatesWithFormat(currentDate,
                        dateStringFinish: shift.startAt,
                        format: "HH:mm:ss") > 0) &&
                        (NSDate.secondsByDifferenceDatesWithFormat(currentDate,
                            dateStringFinish: shift.endAt,
                            format: "HH:mm:ss") < 0) {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    
    private enum Days: String {
        case Sunday , Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    }
    
    /// Transforms int into a Day
    ///
    /// - Parameter day: the integer that represents the day. (ex: 4 = wednesday)
    /// - Returns: return the day in String Format
    static func dayOfWeek(day: Int) -> String {
        switch day {
        case 1:
            return Days.Monday.rawValue
        case 2:
            return Days.Tuesday.rawValue
        case 3:
            return Days.Wednesday.rawValue
        case 4:
            return Days.Thursday.rawValue
        case 5:
            return Days.Friday.rawValue
        case 6:
            return Days.Saturday.rawValue
        case 7:
            return Days.Sunday.rawValue
        default:
            return ""
        }
    }
    
    static func getDayOfWeek(today:String)->Int? {
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.dateFromString(today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.Weekday, fromDate: todayDate)
            let weekDay = myComponents.weekday
            return weekDay
        } else {
            return nil
        }
    }
    
    static func formatRealmData(dados:Results<Unity>?) -> (opened: [Unity],closed: [Unity]) {
        var tuple = (opened: [Unity](), closed:[Unity]())
        if let dados = dados {
            for unity in dados {
                if unity.opened {
                    tuple.opened.append(unity)
                } else {
                    tuple.closed.append(unity)
                }
            }
        }
        return tuple
    }
    
    
    /// Returns the currently selected address
    ///
    /// - Returns: the selected address in Realm
    static func getSelectedAddress() -> Address? {
        if let address = AddressPersistence.getAddressBySelected() {
            return address
        } else {
            return nil
        }
    }
    
    /// Create the formated ListEcsCell
    ///
    /// - Parameters:
    ///   - unity: The Unity to be used
    ///   - cell: The cell, dequeued from tableview
    ///   - opened: if the cell must show opened or not
    /// - Returns: returns a FoodListaEcsCell formated
    class func formatListEcsCell(unity: Unity, cell: FoodListaEcsCell, opened: Bool?, userLocation: Bool) -> FoodListaEcsCell {
        
        cell.lblNome.text = unity.name
        if userLocation  {
            cell.lblDistancia.text = FoodUtils.formatDistanceText(unity.distanceFromUser)
        } else {
            cell.lblDistancia.text = FoodUtils.formatDistanceText(unity.distance)
        }
        cell.lblCategorias.text = ""
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let imgName = unity.getLogo()?.value {
            cell.imgLogo.sd_setImageWithURL( NSURL(string: "\(BASE_URL_IMAGES)\(imgName)"), placeholderImage: UIImage.init(named: "placeholder")) { (image, error, cacheType, url) in
                if image != nil{
                    cell.imgLogo.contentMode = .ScaleAspectFill
                    cell.imgLogo.clipsToBounds = true
                }
            }
        }
        
        if let opened = opened {
            if opened {
                cell.viewFechado.hidden = true
            } else {
                cell.viewFechado.hidden = false
            }
        }
        
        return cell
    }
    
    
    /// Get the current section based on indexPath
    ///
    /// - Parameters:
    ///   - dados: the tuple that contains the data
    ///   - indexPath: the current indexPath of the table
    ///   - section: the specific section
    /// - Returns: returns a kSectionListsEcs enum with the correctly section
    class func getListEcSection(dados: (opened: [Unity], closed: [Unity]), indexPath: NSIndexPath?, section: Int?) -> kSectionListEcs {
        
        var sect: Int = -1
        if let indexSection = indexPath?.section {
            sect = indexSection
        } else if let section = section {
            sect = section
        }
        
        guard sect >= 0 else {
            return kSectionListEcs.none
        }
        
        if sect == kSectionListEcs.opened.rawValue && dados.opened.count > 0 {
            return kSectionListEcs.opened
        } else if (sect == kSectionListEcs.closed.rawValue) && (dados.closed.count > 0) || (dados.opened.count == 0 && dados.closed.count > 0) {
            return kSectionListEcs.closed
        }
        
        return kSectionListEcs.none
    }
    
    
    
    /// Show a loading view on target view
    ///
    /// - Parameters:
    ///   - images: the images list to animate
    ///   - targetView: the view that will be covered by the loader view
    ///   - loaderAnimDuration: time in that the "gif" will take to complete animation
    ///   - message: the message to show
    static func showLoadingView(images: [UIImage]? = [GastronomiaKit.imageOfLoader1,
        GastronomiaKit.imageOfLoader2,
        GastronomiaKit.imageOfLoader3,
        GastronomiaKit.imageOfLoader4,
        GastronomiaKit.imageOfLoader5,
        GastronomiaKit.imageOfLoader6,
        GastronomiaKit.imageOfLoader7,
        GastronomiaKit.imageOfLoader8,
        GastronomiaKit.imageOfLoader9,
        GastronomiaKit.imageOfLoader10,
        GastronomiaKit.imageOfLoader11,
        GastronomiaKit.imageOfLoader12],
                                targetView: UIView,
                                loaderAnimDuration: NSTimeInterval? = 1.3,
                                message: String? = "") {
        
        let viewLoading = FoodViewLoading.instanceFromNib()
        guard !viewLoading.isDescendantOfView(targetView) else {
            return
        }
        
        viewLoading.images = images!
        if let loaderAnimDuration = loaderAnimDuration {
            viewLoading.viewGif.animationDuration = loaderAnimDuration
        }
        if let message = message {
            viewLoading.message = NSLocalizedString(message, tableName: "GastronomiaStrings", comment: "")
        }
        viewLoading.viewGif.startAnimating()
        viewLoading.frame = targetView.frame
        viewLoading.bounds = targetView.bounds
        targetView.addSubview(viewLoading)
        targetView.bringSubviewToFront(viewLoading)
        viewLoading.layoutIfNeeded()
        viewLoading.alpha = 1.0
        
        NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(FoodUtils.timeOutLoadingView(_:)), userInfo: viewLoading, repeats: false)
    }
    
    
    /// Hide the loading view on target view
    ///
    /// - Parameter targetView: The view with loader... or not
    static func hideLoadingView(targetView: UIView) {
        for view in targetView.subviews {
            if view.isMemberOfClass(FoodViewLoading) {
                dispatch_async(dispatch_get_main_queue(), { 
                    UIView.animateWithDuration(0.3, animations: {
                        view.alpha = 0.0
                        }, completion: { (finished) in
                            view.removeFromSuperview()
                    })
                })
                break
            }
        }
    }
    
    static func timeOutLoadingView(timer: NSTimer) {
        if let tView = timer.userInfo as? UIView {
            UIView.animateWithDuration(0.3, animations: {
                tView.alpha = 0.0
                }, completion: { (finished) in
                    tView.removeFromSuperview()
            })
        }
    }
    
    /// Verify if the address exists, it can be deleted in cart
    ///
    /// - Returns: returns a boolean that indicate if the address was deleted
    static func validateAddress() -> Bool {
        if AddressPersistence.getAddressBySelected() != nil {
            return true
        } else {
            return false
        }
    }
    
    /// Calls the address edit view
    ///
    /// - Parameters:
    ///   - address: the address to pre load the edit view
    ///   - navController: if you need to push this view, pass this parameter or it will be a modal view
    static func callEditAddressVC(address:Address, navController: UINavigationController? = nil) {
        let storyboard = UIStoryboard(name: "Gastronomia", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("FoodAddressEditVC") as? FoodAddressEditVC {
            vc.address = address
            vc.editingAddress = true
            vc.isGeolocalized = true
            if let navController = navController {
                let kind = navController.topViewController?.isKindOfClass(FoodAddressEditVC)
                if kind == false {
                    navController.showViewController(vc, sender: self)
                }
            } else {
                UIApplication.topViewController()?.presentViewController(vc, animated: true, completion: nil)
            }
        }
    }
    
    /// Create a instance of geolocation deacivated view
    ///
    /// - Parameters:
    ///   - frame: the frame for the view
    ///   - delegate: delegate to methods of this view
    ///   - isDelivery: if is delivery, this view will show an aditional button to inform manually the address
    /// - Returns: return FoodViewGeolocationError instance
    static func createGeolocationErrorView(frame:CGRect, delegate: FoodViewGeolocationErrorDelegate, isDelivery: Bool) -> FoodViewGeolocationError {
        let viewGeoError = FoodViewGeolocationError.instanceFromNib()
        viewGeoError.delegate = delegate
        viewGeoError.frame = frame
        viewGeoError.layoutIfNeeded()
        viewGeoError.hidden = false
        if !isDelivery {
            viewGeoError.btnInformarEndereco.hidden = true
            viewGeoError.constraintBtnAtivarGeoBottom.constant = 0
            viewGeoError.bringSubviewToFront(viewGeoError.btnAtivarGeolocalizacao)
        }
        return viewGeoError
    }
    
    
    /// Create one  Geolocation Activation View instance
    ///
    /// - Parameters:
    ///   - frame: the frame for the view
    ///   - delegate: delegate for this view
    /// - Returns: returns its instance
    static func createGeolocationActivationView(frame: CGRect, delegate: FoodViewGeoActivationDelegate) -> FoodViewGeoActivation {
        let viewGeoActivation = FoodViewGeoActivation.instanceFromNib()
        viewGeoActivation.delegate = delegate
        viewGeoActivation.frame = frame
        viewGeoActivation.layoutIfNeeded()
        viewGeoActivation.hidden = true
        return viewGeoActivation
    }
    
    static func setNavigationBarWithGradient(navigationBar: UINavigationBar){
        navigationBar.translucent = false
        navigationBar.tintColor = UIColor.whiteColor()
        navigationBar.setBackgroundImage(FoodUtils.imageLayerForGradientBackground(navigationBar), forBarMetrics: UIBarMetrics.Default)
    }
    
    static func imageLayerForGradientBackground(targetView: UIView) -> UIImage {
        
        var updatedFrame = targetView.bounds
        // take into account the status bar
        updatedFrame.size.height += 20
        let layer = CAGradientLayer.gradientLayerForBounds(updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    static func setDistanceOffline(unities:Results<Unity>, location: CLLocation) {
        let realm = try! Realm()
        for unity in unities {
            let ecLocation = CLLocation(latitude: unity.latitude, longitude: unity.longitude)
            try! realm.write {
                unity.distanceFromUser = ecLocation.distanceFromLocation(location)
                try! realm.commitWrite()
            }
        }
    }
    
    static func setDistanceOfflineDelivery(unities:Results<Unity>, location: CLLocation) {
        let realm = try! Realm()
        for unity in unities {
            let ecLocation = CLLocation(latitude: unity.latitude, longitude: unity.longitude)
            try! realm.write {
                unity.distance = ecLocation.distanceFromLocation(location)
                try! realm.commitWrite()
            }
        }
    }
    
}
