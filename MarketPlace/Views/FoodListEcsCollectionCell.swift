//
//  FoodListEcsCollectionCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 22/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodListEcsCollectionCell: UICollectionViewCell {
    
    @IBOutlet var view: UICollectionViewCell!
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UINib(nibName: "FoodListEcsCollectionCell", bundle: nil).instantiateWithOwner(self, options: nil)
        view.frame = self.bounds
        addSubview(view)
        self.layoutIfNeeded()
    }
    
    
}
