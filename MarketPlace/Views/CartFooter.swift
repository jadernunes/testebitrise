//
//  CartFooter.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/20/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CartFooter: UIView, ScannerQrCodeProtocol {

    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var labelChooseOrder: UILabel!
    @IBOutlet weak var buttonDelivery: UIButton!
    @IBOutlet weak var buttonTable: UIButton!
    @IBOutlet weak var buttonTakeaway: UIButton!
    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var viewDeliveryFee: UIView!
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var labelDeliveryFee: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelTextDeliveryFee: UILabel!
    @IBOutlet weak var labelTextTotal: UILabel!
    @IBOutlet weak var buttonVoucher: UIButton!
    
    
    
    @IBOutlet weak var constraintButtonVoucherToStore: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonVoucherToTable: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonVoucherToDelivery: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonVoucherToSuperview: NSLayoutConstraint!
    
    @IBOutlet weak var constraintButtonStoreToTable: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonStoreToDelivery: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonStoreToSuperview: NSLayoutConstraint!
    
    @IBOutlet weak var constraintButtonTableToDelivery: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonTableToSuperview: NSLayoutConstraint!
    
    @IBOutlet weak var constraintButtonDeliveryToSuperview: NSLayoutConstraint!
    
    var rootVc = UIViewController()
    var arrayQrDecoded = [String]()
    var idUnity = Int()

    
    override func awakeFromNib() {
        let frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 299)
        self.frame = frame
        
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0, bounds.size.width, 0.5)
        topBorder.backgroundColor = LayoutManager.backgroundFirstColor.CGColor
        viewTotal.layer.addSublayer(topBorder)
        
        viewSeparator.backgroundColor = SessionManager.sharedInstance.cardColor
    
        labelChooseOrder.font = LayoutManager.primaryFontWithSize(22)
        labelDeliveryFee.font = LayoutManager.primaryFontWithSize(22)
        labelTotal.font = LayoutManager.primaryFontWithSize(22)
        labelTextDeliveryFee.font = LayoutManager.primaryFontWithSize(22)
        labelTextTotal.font = LayoutManager.primaryFontWithSize(22)
        labelPlace.font = LayoutManager.primaryFontWithSize(20)
        
        labelChooseOrder.textColor = LayoutManager.labelContrastColor
        labelDeliveryFee.textColor = LayoutManager.labelSecondColor
        labelTotal.textColor = LayoutManager.labelSecondColor
        labelTextDeliveryFee.textColor = LayoutManager.labelSecondColor
        labelTextTotal.textColor = LayoutManager.labelSecondColor
        labelPlace.textColor = LayoutManager.labelContrastColor

        buttonDelivery.layer.cornerRadius = 6
        buttonDelivery.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonDelivery.layer.borderWidth  = 1.5
        buttonDelivery.titleLabel?.font   = LayoutManager.primaryFontWithSize(16)
        buttonDelivery.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonDelivery.setTitleColor(LayoutManager.labelThirdColor, forState: .Disabled)
        
        buttonTable.layer.cornerRadius = 6
        buttonTable.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonTable.layer.borderWidth  = 1.5
        buttonTable.titleLabel?.font   = LayoutManager.primaryFontWithSize(16)
        buttonTable.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonTable.setTitleColor(LayoutManager.labelThirdColor, forState: .Disabled)
        
        buttonTakeaway.layer.cornerRadius = 6
        buttonTakeaway.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonTakeaway.layer.borderWidth  = 1.5
        buttonTakeaway.titleLabel?.font   = LayoutManager.primaryFontWithSize(16)
        buttonTakeaway.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonTakeaway.setTitleColor(LayoutManager.labelThirdColor, forState: .Disabled)
        
        buttonVoucher.layer.cornerRadius = 6
        buttonVoucher.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonVoucher.layer.borderWidth  = 1.5
        buttonVoucher.titleLabel?.font   = LayoutManager.primaryFontWithSize(16)
        buttonVoucher.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonVoucher.setTitleColor(LayoutManager.labelThirdColor, forState: .Disabled)

        
        viewDeliveryFee.backgroundColor = SessionManager.sharedInstance.cardColor
        viewTotal.backgroundColor = SessionManager.sharedInstance.cardColor
        
        labelPlace.text = ""
    }

    @IBAction func deliveryButtonClicked(sender: AnyObject) {
        if SessionManager.sharedInstance.deliveryChosen == false {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("CartAddressViewController") as! UINavigationController
            (vc.viewControllers[0] as! CartAddressViewController).idUnity = idUnity
            self.rootVc.presentViewController(vc, animated: true, completion: nil)
        }
        else {
            let deliveryEnabledAlert = UIAlertController(title: kAlertTitle, message: kMessageChangeAddress, preferredStyle: UIAlertControllerStyle.Alert)
            
            deliveryEnabledAlert.addAction(UIAlertAction(title: kAlertButtonYes, style: .Default, handler: { (action: UIAlertAction!) in
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc = mainStoryboard.instantiateViewControllerWithIdentifier("CartAddressViewController") as! UINavigationController
                (vc.viewControllers[0] as! CartAddressViewController).idUnity = self.idUnity
                self.rootVc.presentViewController(vc, animated: true, completion: nil)
            }))
            
            deliveryEnabledAlert.addAction(UIAlertAction(title: kAlertButtonNo, style: .Cancel, handler:  { (action: UIAlertAction!) in
                self.labelPlace.text = SessionManager.sharedInstance.tableEnabledByQr
            }))
            rootVc.presentViewController(deliveryEnabledAlert, animated: true, completion: nil)
        }
    }

    @IBAction func takeawayButtonClicked(sender: AnyObject) {
        labelPlace.text = kLabelTakeaway
        SessionManager.sharedInstance.tableEnabledByQr = kLabelTakeaway
        SessionManager.sharedInstance.deliveryChosen = false
        SessionManager.sharedInstance.takeawayChosen = true
        SessionManager.sharedInstance.tableChosen    = false
        SessionManager.sharedInstance.voucherChosen  = false
        SessionManager.sharedInstance.idAddressSelected = 0
        SessionManager.sharedInstance.deliveryFee = 0.0
        NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_UPDATE_LABELS, object: nil)
        setButtonTakeawaySelected()
    }
    
    @IBAction func tableButtonClicked(sender: AnyObject) {
        
        if SessionManager.sharedInstance.tableChosen == false {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
            (vc.viewControllers[0] as! ScannerViewController).delegate = self
            (vc.viewControllers[0] as! ScannerViewController).titleString = kQRScannerTitle
            self.rootVc.presentViewController(vc, animated: true, completion: nil)
        }
        else {
            let tableEnabledAlert = UIAlertController(title: kAlertTitle, message: kMessageChangeTable, preferredStyle: UIAlertControllerStyle.Alert)
            
            tableEnabledAlert.addAction(UIAlertAction(title: kAlertButtonYes, style: .Default, handler: { (action: UIAlertAction!) in
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc = mainStoryboard.instantiateViewControllerWithIdentifier("qrscanVC") as! UINavigationController
                (vc.viewControllers[0] as! ScannerViewController).delegate = self
                (vc.viewControllers[0] as! ScannerViewController).titleString = kQRScannerTitle
                self.rootVc.presentViewController(vc, animated: true, completion: nil)
            }))
            
            tableEnabledAlert.addAction(UIAlertAction(title: kAlertButtonNo, style: .Cancel, handler:  { (action: UIAlertAction!) in
                self.labelPlace.text = SessionManager.sharedInstance.tableEnabledByQr
            }))
            rootVc.presentViewController(tableEnabledAlert, animated: true, completion: nil)
        }
    }

    @IBAction func voucherButtonClicked(sender: AnyObject) {
        labelPlace.text = kLabelVoucher
        SessionManager.sharedInstance.tableEnabledByQr = kLabelVoucher
        SessionManager.sharedInstance.deliveryChosen = false
        SessionManager.sharedInstance.takeawayChosen = false
        SessionManager.sharedInstance.tableChosen    = false
        SessionManager.sharedInstance.voucherChosen  = true

        SessionManager.sharedInstance.idAddressSelected = 0
        SessionManager.sharedInstance.deliveryFee = 0.0
        NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_UPDATE_LABELS, object: nil)
        setButtonVoucherSelected()
    }
    
    // function to adjust the priority of constraint, used to hide / show buttons in it's right place
    
    func didScanValidCode(value: String) {
        arrayQrDecoded = []
        var qrToBeDecoded = value
        qrToBeDecoded.removeAtIndex(qrToBeDecoded.startIndex.advancedBy(3))
        if let qrDecoded = qrToBeDecoded.fromBase64() {
            arrayQrDecoded = qrDecoded.componentsSeparatedByString("_")
        }
        if arrayQrDecoded.count > 0 {
            if arrayQrDecoded.contains("GRP") {
                if arrayQrDecoded[2] == "FOODCOURT" {
                    
                    if arrayQrDecoded.count == 4 {
                        SessionManager.sharedInstance.tableEnabledByQr = arrayQrDecoded[3]
                        self.labelPlace.text = arrayQrDecoded[3]
                        NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_UPDATE_LABELS, object: nil)
                        SessionManager.sharedInstance.deliveryChosen = false
                        SessionManager.sharedInstance.takeawayChosen = false
                        SessionManager.sharedInstance.tableChosen    = true
                        SessionManager.sharedInstance.voucherChosen  = false

                        SessionManager.sharedInstance.idAddressSelected = 0
                        SessionManager.sharedInstance.deliveryFee = 0.0
                        setButtonTableSelected()
                    }
                    else {
                        Util.showAlert(rootVc, title: kAlertTitle, message: kMessageInvalidQR)
                    }
                }
                
                else {
                    Util.showAlert(rootVc, title: kAlertTitle, message: kMessageInvalidQR)
                }
            }
            else if arrayQrDecoded.contains("UNT") {
                if arrayQrDecoded.contains("TBL") {
                    SessionManager.sharedInstance.tableEnabledByQr = arrayQrDecoded[arrayQrDecoded.indexOf("TBL")!+1]
                    self.labelPlace.text = arrayQrDecoded[arrayQrDecoded.indexOf("TBL")!+1]
                    NSNotificationCenter.defaultCenter().postNotificationName(NOTIF_UPDATE_LABELS, object: nil)
                    SessionManager.sharedInstance.deliveryChosen = false
                    SessionManager.sharedInstance.takeawayChosen = false
                    SessionManager.sharedInstance.tableChosen    = true
                    SessionManager.sharedInstance.voucherChosen  = false
                    SessionManager.sharedInstance.idAddressSelected = 0
                    SessionManager.sharedInstance.deliveryFee = 0.0
                    setButtonTableSelected()
                }
            }
            else {
                Util.showAlert(rootVc, title: kAlertTitle, message: kMessageInvalidQR)
            }
        }
        else {
            Util.showAlert(rootVc, title: kAlertTitle, message: kMessageInvalidQR)
        }
    }
    
    func didScanInvalidCode() {
        Util.showAlert(rootVc, title: kAlertTitle, message: kMessageInvalidQR)
    }
    
    func populateView(unity : Unity!) {
        adjustConstraintSize(unity)
        if unity.orderDeliveryEnabled {
            if unity.orderShortDeliveryEnabled {
                constraintButtonTableToDelivery.priority = 999
                
                if unity.orderTakeAwayEnabled {
                    constraintButtonStoreToTable.priority = 999
                    
                    if unity.orderVoucherEnabled {
                        constraintButtonVoucherToStore.priority = 999
                    }
                    else {
                        buttonVoucher.hidden = true
                    }
                }
                else {
                    constraintButtonVoucherToTable.priority = 999
                    buttonTakeaway.hidden = true
                }
            }
                
            else {
                buttonTable.hidden = true
                
                if unity.orderTakeAwayEnabled {
                    constraintButtonVoucherToStore.priority = 999
                    constraintButtonStoreToDelivery.priority = 999
                }
                else {
                    buttonTakeaway.hidden = true
                    constraintButtonVoucherToDelivery.priority = 999
                }
            }
        }
        else {
            buttonDelivery.hidden = true
            if unity.orderShortDeliveryEnabled {
                constraintButtonTableToSuperview.priority = 999
                
                if unity.orderTakeAwayEnabled {
                    constraintButtonStoreToTable.priority = 999
                    
                    if unity.orderVoucherEnabled {
                        constraintButtonVoucherToStore.priority = 999
                    }
                    else {
                        buttonVoucher.hidden = true
                    }
                }
                else {
                    constraintButtonVoucherToTable.priority = 999
                    buttonTakeaway.hidden = true
                }
            }
                
            else {
                buttonTable.hidden = true
                
                if unity.orderTakeAwayEnabled {
                    constraintButtonVoucherToStore.priority = 999
                    constraintButtonStoreToSuperview.priority = 999
                }
                else {
                    buttonTakeaway.hidden = true
                    buttonVoucher.hidden = false

                    constraintButtonVoucherToSuperview.priority = 999
                }
            }
            
        }
        
    }
    
    // adjust the size of the constraint using the available buttons for each unity
    func adjustConstraintSize(unity : Unity!) {
        var count = 0
        if  unity.orderVoucherEnabled {
            count = count + 1
        }
        if  unity.orderTakeAwayEnabled {
            count = count + 1
        }
        if  unity.orderShortDeliveryEnabled {
            count = count + 1
        }
        if  unity.orderDeliveryEnabled {
            count = count + 1
        }
        
        var constraintNewSize = (Float(self.rootVc.view.frame.width) - (Float(count) * Float(buttonTable.frame.size.width)))
        constraintNewSize = constraintNewSize/(Float(count)+1)
        
        constraintButtonVoucherToStore.constant = CGFloat(constraintNewSize)
        constraintButtonVoucherToTable.constant = CGFloat(constraintNewSize)
        constraintButtonVoucherToDelivery.constant = CGFloat(constraintNewSize)
        constraintButtonVoucherToSuperview.constant = CGFloat(constraintNewSize)
        constraintButtonStoreToTable.constant = CGFloat(constraintNewSize)
        constraintButtonStoreToDelivery.constant = CGFloat(constraintNewSize)
        constraintButtonStoreToSuperview.constant = CGFloat(constraintNewSize)
        constraintButtonTableToDelivery.constant = CGFloat(constraintNewSize)
        constraintButtonTableToSuperview.constant = CGFloat(constraintNewSize)
        constraintButtonDeliveryToSuperview.constant = CGFloat(constraintNewSize)
        
    }

    
    func setButtonTableSelected() {
        buttonTable.setTitleColor(LayoutManager.white, forState: .Normal)
        buttonTable.backgroundColor = SessionManager.sharedInstance.cardColor
        buttonTable.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        
        if buttonTakeaway.enabled {
            buttonTakeaway.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTakeaway.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTakeaway.backgroundColor = UIColor.clearColor()
        }
        
        if buttonDelivery.enabled {
            buttonDelivery.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonDelivery.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonDelivery.backgroundColor = UIColor.clearColor()
        }
        
        if buttonVoucher.enabled {
            buttonVoucher.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonVoucher.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonVoucher.backgroundColor = UIColor.clearColor()
        }
    }
    
    func setButtonDeliverySelected() {
        buttonDelivery.setTitleColor(LayoutManager.white, forState: .Normal)
        buttonDelivery.backgroundColor = SessionManager.sharedInstance.cardColor
        buttonDelivery.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        
        if buttonTable.enabled {
            buttonTable.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTable.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTable.backgroundColor = UIColor.clearColor()
        }
        
        if buttonTakeaway.enabled {
            buttonTakeaway.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTakeaway.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTakeaway.backgroundColor = UIColor.clearColor()
        }
        
        if buttonVoucher.enabled {
            buttonVoucher.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonVoucher.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonVoucher.backgroundColor = UIColor.clearColor()
        }
    }
    
    func setButtonTakeawaySelected() {
        buttonTakeaway.setTitleColor(LayoutManager.white, forState: .Normal)
        buttonTakeaway.backgroundColor = SessionManager.sharedInstance.cardColor
        buttonTakeaway.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        
        if buttonTable.enabled {
            buttonTable.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTable.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTable.backgroundColor = UIColor.clearColor()
        }
        
        if buttonDelivery.enabled {
            buttonDelivery.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonDelivery.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonDelivery.backgroundColor = UIColor.clearColor()
        }
        
        if buttonVoucher.enabled {
            buttonVoucher.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonVoucher.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonVoucher.backgroundColor = UIColor.clearColor()
        }
    }
    
    func setButtonVoucherSelected() {
        buttonVoucher.setTitleColor(LayoutManager.white, forState: .Normal)
        buttonVoucher.backgroundColor = SessionManager.sharedInstance.cardColor
        buttonVoucher.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        
        if buttonTable.enabled {
            buttonTable.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTable.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTable.backgroundColor = UIColor.clearColor()
        }
        
        if buttonDelivery.enabled {
            buttonDelivery.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonDelivery.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonDelivery.backgroundColor = UIColor.clearColor()
        }
        
        if buttonTakeaway.enabled {
            buttonTakeaway.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
            buttonTakeaway.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
            buttonTakeaway.backgroundColor = UIColor.clearColor()
        }
    }
}
