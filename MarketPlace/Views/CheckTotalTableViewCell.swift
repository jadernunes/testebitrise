//
//  CheckTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CheckTotalTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelTotal.font = LayoutManager.primaryFontWithSize(22)
        self.labelTotal.textColor = LayoutManager.white
        
        self.labelPrice.font = LayoutManager.primaryFontWithSize(22)
        self.labelPrice.textColor = LayoutManager.white
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
