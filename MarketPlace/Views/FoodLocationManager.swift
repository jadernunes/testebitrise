//
//  FoodLocationManager.swift
//  MarketPlace
//
//  Created by Leandro Lopes Nunes on 09/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import CoreLocation

class FoodLocationManager: CLLocationManager {
    static let shared = FoodLocationManager()
    
    override init() {
        super.init()
        
        requestWhenInUseAuthorization()
    }
    
    override func startUpdatingLocation() {
        desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        distanceFilter = 100.0
        super.startUpdatingLocation()
    }
}
