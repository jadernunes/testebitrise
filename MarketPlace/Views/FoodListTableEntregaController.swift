//
//  FoodListTableEntregaDatasource.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 22/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

protocol TableEntregaControllerDelegate {
    var locationEntrega: CLLocation? {get set}
    func didPressEntregaCell(indexPath: NSIndexPath, unity: Unity) -> Void
    func didPressAddressGadget() -> Void
    func didCallRefreshEntrega(tempAddress: Address?) -> Void
    func callRefreshViewEntrega() -> Void
}

extension TableEntregaControllerDelegate {
    func didCallRefreshEntrega(tempAddress: Address? = nil) -> Void {
        return didCallRefreshEntrega(tempAddress)
    }
    func callRefreshViewEntrega() -> Void {
        //only to make this func optional
    }
}

class FoodListTableEntregaController: UIViewController {
    var delegateFood: TableEntregaControllerDelegate?
    var address: Address? {
        didSet {
            setOfflineLocation()
            setGadgetAddress()
            if address != nil {
                loadUnities()
            }
        }
    }
    private var viewGeolocationError = FoodViewGeolocationError()
    @IBOutlet weak var tableEntrega: UITableView! {
        didSet {
            tableEntrega.registerNib(UINib(nibName: "FoodListaEcsCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
            if #available(iOS 10.0, *) {
                let refreshControl = UIRefreshControl()
                refreshControl.attributedTitle = NSAttributedString(string: "")
                refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
                self.tableEntrega.refreshControl = refreshControl
                self.tableEntrega.addSubview(refreshControl)
            }
        }
    }
    private var cellIdentifier = "FoodListaEcsCell"
    var dados : Results<Unity>? {
        didSet {
            setOfflineLocation()
            dados = dados?.sorted("distanceFromUser", ascending: true)
            self.dadosOrdered = FoodUtils.formatRealmData(dados)
            if self.tableEntrega != nil {
                self.tableEntrega.reloadData()
            }
        }
    }
    private var dadosOrdered: (opened:[Unity],closed:[Unity])?
    @IBOutlet weak var viewGadgetAddress: FoodViewGadgetAddress!{
        didSet {
            viewGadgetAddress.delegate = self
            if address != nil {
                setGadgetAddress()
                if FoodValidations.shouldShowAddressAlertBalloon() {
                    self.performSelector(#selector(self.showAlert), withObject: nil, afterDelay: 1.0)
                }
            } else {
                
            }
        }
    }
    
    private let alerts = FoodAlerts()
    private var didOpenSettings: Bool = false
    
    func validacoes() {
        FoodUtils.showLoadingView([GastronomiaKit.imageOfLoaderGastro1,
            GastronomiaKit.imageOfLoaderGastro2,
            GastronomiaKit.imageOfLoaderGastro3,
            GastronomiaKit.imageOfLoaderGastro4,
            GastronomiaKit.imageOfLoaderGastro5,
            GastronomiaKit.imageOfLoaderGastro6,
            GastronomiaKit.imageOfLoaderGastro7,
            GastronomiaKit.imageOfLoaderGastro8,
            GastronomiaKit.imageOfLoaderGastro9,
            GastronomiaKit.imageOfLoaderGastro10,
            GastronomiaKit.imageOfLoaderGastro11,
            GastronomiaKit.imageOfLoaderGastro12],
                                  targetView: (self.view)!,
                                  message: NSLocalizedString("Carregando estabelecimentos", tableName: "GastronomiaStrings", comment: ""))
        
        if let address = AddressPersistence.getAddressBySelected() {
            self.address = address
        } else if CLLocationManager.locationServicesEnabled() {
            print("location services enabled")
            if didOpenSettings {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    if let address = address {
                        self.title = ""
                        self.didOpenSettings = false
                        FoodUtils.callEditAddressVC(address, navController:  self.navigationController)
                    } else {
                        FoodUtils.hideLoadingView(self.view)
                    }
                })
            } else {
                showGeoLocationError()
            }
        } else {
            showGeoLocationError()
        }
    }
    
    override func viewDidLoad() {
        //verifica se deve mostrar o alerta balão na primeira vez
        validateAlertBalloon()
        validacoes()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //notificacao para validar se o usuario permitiu o gps
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.validacoes), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    private func showGeoLocationError() {
        viewGeolocationError = FoodUtils.createGeolocationErrorView(self.view.frame, delegate: self, isDelivery: true)
        view.addSubview(viewGeolocationError)
        view.bringSubviewToFront(viewGeolocationError)
        FoodUtils.hideLoadingView(self.view)
    }
    
    private func setGadgetAddress() {
        if let street = address?.street {
            viewGadgetAddress.lblAddress.text = "\(street)"
            if let number = address?.number {
                viewGadgetAddress.lblAddress.text = "\(viewGadgetAddress.lblAddress.text!), \(number)"
            }
            if let complement = address?.complement {
                if complement != "" {
                    viewGadgetAddress.lblAddress.text = "\(viewGadgetAddress.lblAddress.text!), \(complement)"
                }
            }
        }
    }
    
    /// Verify if needs to show the balloon alert
    private func validateAlertBalloon() {
        
    }
    
    @objc private func showAlert() {
        let size = UIScreen.mainScreen().bounds.size
        alerts.foodDelegate = self
        alerts.showViewAlertBalloon(self, frame: CGRect(x: 0,
            y: self.viewGadgetAddress.frame.size.height,
            width: size.width,
            height: size.height-30))
    }
    
    private func hideLoadingDelivery() {
        for view in self.view.subviews {
            if view.isMemberOfClass(FoodViewLoading) {
                UIView.animateWithDuration(0.3, animations: {
                    view.alpha = 0.0
                    }, completion: { (finished) in
                        self.view.bringSubviewToFront(self.tableEntrega)
                        view.removeFromSuperview()
                })
                break
            }
        }
    }
    
    func emptyTableview() {
        let viewNoData = FoodViewNoData()
        viewNoData.message.text = NSLocalizedString("Ops! Não foi possível carregar os estabelecimentos.", tableName: "GastronomiaStrings", comment: "")
        self.tableEntrega.backgroundView = FoodViewNoData()
        self.tableEntrega.separatorStyle = .None
    }
    
    private func setOfflineLocation() -> Void {
        if let address = self.address, let dados = self.dados {
            let location = CLLocation(latitude: address.latitude, longitude: address.longitude)
            FoodUtils.setDistanceOfflineDelivery(dados, location: location)
        }
    }
    
    private func loadUnity(indexPath: NSIndexPath, unity: Unity) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        detailsVc.title = unity.name
        detailsVc.idObjectReceived = unity.id
        detailsVc.typeObjet = TypeObjectDetail.Store
        if self.navigationController?.topViewController != detailsVc {
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
        
        let api = ApiServices()
        api.getUnityByIDService(unity.id)
    }
    
    func loadUnities(address: Address? = nil) {
        FoodUtils.showLoadingView([GastronomiaKit.imageOfLoaderGastro1,
            GastronomiaKit.imageOfLoaderGastro2,
            GastronomiaKit.imageOfLoaderGastro3,
            GastronomiaKit.imageOfLoaderGastro4,
            GastronomiaKit.imageOfLoaderGastro5,
            GastronomiaKit.imageOfLoaderGastro6,
            GastronomiaKit.imageOfLoaderGastro7,
            GastronomiaKit.imageOfLoaderGastro8,
            GastronomiaKit.imageOfLoaderGastro9,
            GastronomiaKit.imageOfLoaderGastro10,
            GastronomiaKit.imageOfLoaderGastro11,
            GastronomiaKit.imageOfLoaderGastro12],
                                  targetView: (self.view)!,
                                  message: NSLocalizedString("Carregando estabelecimentos", tableName: "GastronomiaStrings", comment: ""))
        
        let api = ApiServices()
        api.successCase = {(obj) in
            let array = obj as! NSArray
            for uni in array {
                if let newObj = uni as? NSDictionary {
                    let newUnity = NSMutableDictionary(dictionary: newObj)
                    if let address = address {
                        if let zip = address.zip {
                            newUnity.setObject(zip, forKey: "zip")
                        }
                    }
                    UnityPersistence.importFromDictionary(newUnity)
                }
            }
            if let address = self.address {
                if let zip = address.zip {
                    let dadosEntrega = UnityPersistence.getUnitiesByCep(zip)
                    self.dados = dadosEntrega
                }
            }
            self.hideLoadingDelivery()
            self.tableEntrega.reloadData()
        }
        api.failureCase = {(obj) in
            if let controller = UIApplication.sharedApplication().keyWindow?.rootViewController {
                let alert = FoodViewAlert.init(controller: controller)
                alert.showConnectionError()
            }
            self.hideLoadingDelivery()
        }

        if let address = self.address {
            api.getUnitiesByCEP(address)
        }
    }
}
//MARK: - UITableViewDelegate, UITableViewDataSource
extension FoodListTableEntregaController: UITableViewDelegate, UITableViewDataSource {
    //MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard address != nil &&  dados != nil else {
            emptyTableview()
            return 0
        }
        
        if dadosOrdered?.opened.count > 0 && dadosOrdered?.closed.count > 0 {
            return 2
        } else if dadosOrdered?.opened.count > 0 || dadosOrdered?.closed.count > 0 {
            return 1
        }
        
        emptyTableview()
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        self.tableEntrega.backgroundView = UIView()
        self.tableEntrega.separatorStyle = .SingleLine
        
        if sect == kSectionListEcs.opened {
            if let count = dadosOrdered?.opened.count {
                return count
            }
        } else if sect == kSectionListEcs.closed {
            if let count = dadosOrdered?.closed.count {
                return count
            }
        }
        
        emptyTableview()
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! FoodListaEcsCell
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)

        //Valida que tipo de EC é, se aberto ou fechado
        var unity:Unity
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: true, userLocation: false)
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: false, userLocation: false)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var unity:Unity
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)
        
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
        }

        //tenta carregar o carrinho
        let realm = try! Realm()
        let cart = OrderEntityManager.sharedInstance.getCart(0)
        
        //se não encontrou seta o idUnity que vai manipular
        if cart?.idUnity == nil || cart?.idUnity == 0 {
            realm.beginWrite()
            cart?.idUnity = unity.id
            try! realm.commitWrite()
        }
        self.loadUnity(indexPath, unity: unity)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        
        //sem um titulo não chama a headerview
        if sect == kSectionListEcs.opened {
            return ""
        } else {
            return NSLocalizedString("Fechados neste momento", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //chama a view customizada
        let header = FoodViewHeaderListEcs(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        return header
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //personaliza o texto da view
        if let header = view as? FoodViewHeaderListEcs {
            header.textLabel?.font = UIFont.foodDosisLightFont()
            header.textLabel?.textColor = UIColor.foodGreyishBrownColor()
        }
    }
    
    func refresh(sender:AnyObject) {
        self.loadUnities(self.address)
        if #available(iOS 10.0, *) {
            tableEntrega.refreshControl?.endRefreshing()
        } else {
            // Fallback on earlier versions
        }
    }
    
}
//MARK: - FoodViewGeolocationErrorDelegate
extension FoodListTableEntregaController: FoodViewGeolocationErrorDelegate {
    func didPressAtivarGeolocalizacao(button: UIButton) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways ||
                CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    if let address = address {
                        self.title = ""
                        FoodUtils.callEditAddressVC(address, navController:  self.navigationController)
                    }
                })
            } else {
                didOpenSettings = true
                FoodAlerts().showAlertGeoLocation()
            }
            
        }
        
    }
    
    func didPressInformarEndereco(button: UIButton) {
        self.title = ""
        let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC")
        self.showViewController(vc!, sender: self)
    }
    
    func didMoveToView() {
        
    }
}
//MARK: - FoodViewAlertBalloonDelegate
extension FoodListTableEntregaController: FoodViewAlertBalloonDelegate {
    func didPressBalloon() -> Void {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC")
        vc?.hidesBottomBarWhenPushed = true
        self.showViewController(vc!, sender: self)
    }
}
//MARK: - FoodAlertsDelegate
extension FoodListTableEntregaController: FoodAlertsDelegate {
    func didCallCloseAlertBallon() {
        
    }
}
//MARK: - FoodViewGadgetAddressDelegate
extension FoodListTableEntregaController: FoodViewGadgetAddressDelegate {
    func didPressView(sender: UIView) {
        self.title = ""
        if FoodUtils.validateAddress() && self.address != nil {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodAddressEditVC") as? FoodAddressEditVC {
                vc.address = address
                vc.editingAddress = true
                vc.hidesBottomBarWhenPushed = true
                self.showViewController(vc, sender: self)
            }
        } else {
            viewGeolocationError = FoodUtils.createGeolocationErrorView(self.view.frame, delegate: self, isDelivery: true)
            view.addSubview(viewGeolocationError)
            view.bringSubviewToFront(viewGeolocationError)
            FoodUtils.hideLoadingView(view)
        }
    }
}
