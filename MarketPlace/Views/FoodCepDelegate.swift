//
//  FoodUITextFieldExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 09/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodCepDelegate: NSObject, UITextFieldDelegate {

    private var timer = NSTimer()
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            startTimer(textField, interval: 1.0)
            return true
        }
        
        if !FoodValidations.isStringNumerical(string) {
            return false
        }

        if textField.text?.characters.count == 5 {
            textField.text? = textField.text! + "-"
        }
        
        if textField.text?.characters.count >= 9 {
            startTimer(textField, interval: 0.0)
            return false
        }
        
        startTimer(textField, interval: 1.0)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    
    @objc private func verifyCepField(timer:NSTimer) -> Void {
        
        let field = timer.userInfo as! UITextField
        
        if field.text?.characters.count >= 8 {
            let validator = FoodValidations()
            if validator.verifyUserCEP(field) {
                self.validateCepField(field)
            } else {
                self.invalidateCepField(field)
            }
        }else {
            self.invalidateCepField(field)
        }
    }
    
    private func validateCepField(field: UITextField) -> Void {
        if let spView = field.superview as? FoodRoundedView {
            spView.borderColor = UIColor(colorLiteralRed: 242, green: 242, blue: 242, alpha: 1)
            if let foodAddressCell = spView.superview?.superview as? FoodAddressEditCell {
                foodAddressCell.showHideAlertCep(hide: true)
            }
        }
    }
    
    private func invalidateCepField(field: UITextField) -> Void {
        if let spView = field.superview as? FoodRoundedView {
            spView.borderColor = UIColor.redColor()
            if let foodAddressCell = spView.superview?.superview as? FoodAddressEditCell {
                foodAddressCell.showHideAlertCep(hide: false)
            }
        }
    }
    
    private func startTimer(field:UITextField, interval: NSTimeInterval) -> Void {
        timer.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(interval,
                                                       target: self,
                                                       selector: #selector(verifyCepField(_:)),
                                                       userInfo: field,
                                                       repeats: false)
    }
    
    
    
}
