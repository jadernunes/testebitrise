//
//  TimeRangeTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 10/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class TimeRangeTableViewCell: UITableViewCell {

    @IBOutlet weak var labelStringDays  : UILabel!
    @IBOutlet weak var labelHours       : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelStringDays.font = LayoutManager.primaryFontWithSize(14)
        labelHours.font = LayoutManager.primaryFontWithSize(14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
