//
//  NewUnityDetailsItemsTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 02/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewUnityDetailsItemsTableViewCell : UITableViewCell, ConfigurableWithUnity {

    enum Item: Int {
        case orderEnabled = 0, orderCheckEnabled, schedulingEnabled
    }
    
    struct ItemContent {
        var item:       Item
        var message:    String
        var image:      String
    }
    
    var items = [ItemContent]() {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.bounces          = false
            self.tableView.scrollEnabled    = false
            self.tableView.separatorStyle   = .None
            self.tableView.dataSource       = self
            self.tableView.delegate         = self
            
            tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_ITEM_IDENTIFIER, bundle: nil), forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_ITEM_IDENTIFIER)
        }
    }
    
    private var unity: Unity!
    private var categories: [[String: AnyObject]]!
    private var referenceToSuperViewController: UIViewController!
    
    private var orderLoadIsFinished    = false
    private var scheduleLoadIsFinished = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.setEnabledOrder(_:)),
            name: UNITY_ITEMS_OBSERVER,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(self.setEnabledSchedule(_:)),
            name: UNITY_SCHEDULE_OBSERVER,
            object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter()
            .removeObserver(UNITY_SCHEDULE_OBSERVER)
        NSNotificationCenter.defaultCenter()
            .removeObserver(UNITY_ITEMS_OBSERVER)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setEnabledOrder(notification: NSNotification) {
        if !self.orderLoadIsFinished {
            let row = items.indexOf( { $0.item == .orderEnabled } )
            if let row = row {
                let rowToSelect: NSIndexPath = NSIndexPath(forRow: row, inSection: 0)
                
                let cell = self.tableView.cellForRowAtIndexPath(rowToSelect) as!
                    NewUnityDetailsItemTableViewCell
                
                let userInfo: [String: [[String: AnyObject]]] = notification.userInfo as! [String: [[String: AnyObject]]]
                
                if let categories: [[String: AnyObject]] = userInfo["categories"] {
                    self.categories = categories
                    cell.setEnable(true)
                    self.orderLoadIsFinished = true
                }
            }
        }
    }
    
    func setEnabledSchedule(notification: NSNotification) {
        if !self.scheduleLoadIsFinished {
            let row = items.indexOf( { $0.item == .schedulingEnabled } )
            if let row = row {
                let rowToSelect: NSIndexPath = NSIndexPath(forRow: row, inSection: 0)
                
                let cell = self.tableView.cellForRowAtIndexPath(rowToSelect) as!
                NewUnityDetailsItemTableViewCell
                
                let userInfo: [String: [[String: AnyObject]]] = notification.userInfo as! [String: [[String: AnyObject]]]
                
                if let categories: [[String: AnyObject]] = userInfo["categories"] {
                    self.categories = categories
                    cell.setEnable(true)
                    self.scheduleLoadIsFinished = true
                }
            }
        }
    }
    
    func getHeight() -> CGFloat {
        return NewUnityDetailsItemTableViewCell.getHeight() * CGFloat(items.count)
    }
    
    func configureWithUnity(unity: Unity, withReference reference: UIViewController?) {
        self.items = [ItemContent]()
        
        self.unity = unity
        self.referenceToSuperViewController = reference!
        
        if unity.orderEnabled {
            let item =
                ItemContent(item:       .orderEnabled,
                            message:    .orderMessage,
                            image:      .orderIconName)
            self.items.append(item)
        }
        
        if unity.schedulingEnabled {
            let item =
                ItemContent(item:       .schedulingEnabled,
                            message:    .schedulingMessage,
                            image:      .scheduleIconName)
            self.items.append(item)
        }
        
        if unity.orderCheckEnabled {
            let item =
                ItemContent(item:       .orderCheckEnabled,
                            message:    .orderCheckMessage,
                            image:      .tabIconName)
            self.items.append(item)
        }
    }
}

extension NewUnityDetailsItemsTableViewCell : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(NEW_CELL_UNITY_DETAILS_ITEM_IDENTIFIER) as! NewUnityDetailsItemTableViewCell
        
        var shouldEnable = true
        
        if items[indexPath.row].item == NewUnityDetailsItemsTableViewCell.Item.orderEnabled {
            shouldEnable = self.orderLoadIsFinished
        } else if items[indexPath.row].item == NewUnityDetailsItemsTableViewCell.Item.schedulingEnabled {
            shouldEnable = self.scheduleLoadIsFinished
        }
        
        cell.configureWith(items[indexPath.row].message,
                           image: items[indexPath.row].image,
                           item: items[indexPath.row].item,
                           last: indexPath.row == (self.items.count - 1),
                           shouldEnable: shouldEnable)
        return cell
    }
}

extension NewUnityDetailsItemsTableViewCell : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return NewUnityDetailsItemTableViewCell.getHeight()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch items[indexPath.row].item {
        case Item.orderEnabled:
            
            /// That is because we use didSelectRowAtIndexPath in setEnabledOrder
            /// to prevent execute the observer's event right after we enable order
            
            if self.orderLoadIsFinished {
                
                self.orderLoadIsFinished    = false
                self.scheduleLoadIsFinished = false
                
                let listUnityItemsViewController = UIStoryboard(name: "Main",
                    bundle: nil)
                    .instantiateViewControllerWithIdentifier("ListUnityItemsVC")
                    as! ListUnityItemsViewController
            
                listUnityItemsViewController.contentArray   = self.categories
                listUnityItemsViewController.unity          = self.unity
            
                self.referenceToSuperViewController.navigationController?.pushViewController(listUnityItemsViewController, animated: true)
            }
            break;
        case Item.orderCheckEnabled:
            
            let navigationViewController = UIStoryboard(name: "Main",
                bundle: nil)
                .instantiateViewControllerWithIdentifier("qrscanVC")
                as! UINavigationController
            
            let scannerViewController = navigationViewController.viewControllers.first as!
                ScannerViewController
            scannerViewController.delegate = self
            scannerViewController.titleString = .payTab

            self.referenceToSuperViewController
                .presentViewController(navigationViewController,
                                       animated: true,
                                       completion: nil)
            break;
        case Item.schedulingEnabled:
            
            /// That is because we use didSelectRowAtIndexPath in setEnabledSchedule
            /// to prevent execute the observer's event right after we enable schedule
            
            if self.scheduleLoadIsFinished {
                
                // We do this to make the both cell reload when went back from whatever view
                self.scheduleLoadIsFinished = false
                self.orderLoadIsFinished    = false
                
                let listUnityItemsViewController = UIStoryboard(name: "Main",
                    bundle: nil)
                    .instantiateViewControllerWithIdentifier("ListUnityItemsVC")
                    as! ListUnityItemsViewController
                
                listUnityItemsViewController.contentArray   = self.categories
                listUnityItemsViewController.unity          = self.unity
                
                self.referenceToSuperViewController.navigationController?.pushViewController(listUnityItemsViewController, animated: true)
            }
            break;
        }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? NewUnityDetailsItemTableViewCell {
            if cell.enabled {
                return true
            } else {
                false
            }
        }
        
        return false
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        tableView.scrollEnabled = false
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsItemTableViewCell
        
        cell.highlight()
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        tableView.scrollEnabled = true

        let cell = tableView.cellForRowAtIndexPath(indexPath) as! NewUnityDetailsItemTableViewCell
        cell.unHighlight()
    }
}

extension NewUnityDetailsItemsTableViewCell : ScannerQrCodeProtocol {
    func didScanInvalidCode() {
        Util.showAlert(self.referenceToSuperViewController, title: .attUnity, message: .invalidTab)
    }
    
    func didScanValidCode(value: String) {
        if value.isNumeric {
            let checkViewController = UIStoryboard(name: "Main",
                bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("CheckViewController") as! CheckViewController
            
            checkViewController.placeLabel = value
            checkViewController.idUnity = self.unity.id
            checkViewController.rootViewController = self.referenceToSuperViewController!
            
            if referenceToSuperViewController != nil {
                referenceToSuperViewController.navigationController?.pushViewController(checkViewController, animated: true)
            } else {
                if let controller = UIApplication.topViewController() {
                    controller.navigationController?.pushViewController(checkViewController, animated: true)
                }
                
            }
            
        }
        else {
            Util.showAlert(referenceToSuperViewController, title: .attUnity, message: .invalidTab)
        }
    }
}
