//
//  MyCardsTableViewCell.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 18/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class MyCardsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbBalance : UILabel?
    @IBOutlet weak var lbFutureBalance : UILabel?
    @IBOutlet weak var lbBalanceCents : UILabel?
    @IBOutlet weak var lbNumber : UILabel?
    @IBOutlet weak var lbNick : UILabel?
    @IBOutlet weak var lblBalanceDate : UILabel?
    @IBOutlet weak var im : UIImageView?
    @IBOutlet weak var viewRecharge : UIView?
    @IBOutlet weak var viewBase : UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
