//
//  ButtonDonna.swift
//  Donna
//
//  Created by Anderson Kloss Maia on 07/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class Button4allPayment: UIButton {
    
    weak var superViewController: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userInteractionEnabled = true
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowOpacity = 0.4
        
        titleLabel?.font = UIFont(name: "HelveticaNeue", size: 18.0)
        tintColor = UIColor.whiteColor()
        
        setBackgroundImage(Util.imageLayerForGradientBackgroundInView(self), forState: UIControlState.Normal)
        layer.masksToBounds = true
        layer.cornerRadius = 8.0
    }
    
}
