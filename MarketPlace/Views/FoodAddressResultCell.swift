//
//  FoodAddressResultCell.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 03/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodAddressResultCellDelegate {
    func didPressEditButton(sender: AnyObject)
}

class FoodAddressResultCell: UITableViewCell {
    
    @IBOutlet weak var lblAddressLine1: UILabel!
    @IBOutlet weak var lblAddressLine2: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnEditar: UIButton!
    var foodDelegate: FoodAddressResultCellDelegate?
    
    @IBAction func didPressEditButton(sender: AnyObject) {
        foodDelegate?.didPressEditButton(sender)
    }
}
