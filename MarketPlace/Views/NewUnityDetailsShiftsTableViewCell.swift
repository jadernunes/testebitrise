//
//  NewUnityDetailsShiftsTableViewCell.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 05/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit
import RealmSwift

class NewUnityDetailsShiftsTableViewCell: UITableViewCell, ConfigurableWithUnity {

    var shifts = [[Shifts]]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.bounces          = false
            self.tableView.scrollEnabled    = false
            self.tableView.separatorStyle   = .None
            self.tableView.dataSource       = self
            self.tableView.delegate         = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerNibs()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func registerNibs() {
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_HEADER_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forHeaderFooterViewReuseIdentifier: NEW_HEADER_UNITY_DETAILS_IDENTIFIER)
        self.tableView.registerNib(UINib(nibName: NEW_UNITY_DETAILS_SHIFT_IDENTIFIER,
            bundle: NSBundle.mainBundle()),
                                   forCellReuseIdentifier: NEW_CELL_UNITY_DETAILS_SHIFT_IDENTIFIER)
    }
    
    func getHeight() -> CGFloat {
        return (NewUnityDetailsShiftTableViewCell.getHeight() *
            CGFloat(shifts.count))
            + CGFloat(shifts.count > 0 ? NewUnityDetailsHeader.getHeight() : 0.0)
    }
    
    func configureWithUnity(unity: Unity, withReference: UIViewController?) {
        self.shifts = [[Shifts]]()
        
        for day in 1...7 {
            ShiftEntityManager.getShifstOfWeekByUnityIdAndDay(
                unityId: unity.id, day: Util.dayOfWeek(day), completion: { (shifts) in
                    if let shifts = shifts where shifts.count > 0 {
                        self.shifts.append(shifts.flatMap() { $0 } )
                    }
            })
        }
    }
}

extension NewUnityDetailsShiftsTableViewCell : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.shifts.isEmpty ? 0 : 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.shifts.isEmpty ? 0 : self.shifts.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(NEW_CELL_UNITY_DETAILS_SHIFT_IDENTIFIER) as! NewUnityDetailsShiftTableViewCell
        cell.configureWithShift(shifts: self.shifts[indexPath.row],
                                last: indexPath.row == (self.shifts.count - 1))
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return NewUnityDetailsShiftTableViewCell.getHeight()
    }
}

extension NewUnityDetailsShiftsTableViewCell : UITableViewDelegate {
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier(NEW_HEADER_UNITY_DETAILS_IDENTIFIER) as! NewUnityDetailsHeader
            header.configureWithTitle(.hourAvaliable)
            return header
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return NewUnityDetailsHeader.getHeight()
        default:
            return 0.0
        }
        
    }
}
