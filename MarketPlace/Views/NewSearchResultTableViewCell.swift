//
//  NewSearchResultTableViewCell.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import UIKit

class NewSearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewLogoUnity: UIImageView!
    @IBOutlet weak var labelNameUnity: UILabel!
    @IBOutlet weak var labelTypeUnity: UILabel!
    @IBOutlet weak var viewLIneSeparator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageViewLogoUnity.layer.cornerRadius = 4
        self.imageViewLogoUnity.layer.masksToBounds = true
        self.viewLIneSeparator.backgroundColor = UIColor.lightGray4all()
        self.labelTypeUnity.textColor = UIColor.gray4all()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
