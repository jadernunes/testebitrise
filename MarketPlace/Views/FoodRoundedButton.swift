//
//  FoodRoundedButton.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 16/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable public class FoodRoundButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var shadowEnabled: Bool = true {
        didSet {
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 0.03 * bounds.size.width
        if shadowEnabled {
            layer.shadowColor = UIColor.init(red: 0.639, green: 0.129, blue: 0.169, alpha: 1).CGColor
            layer.shadowOffset = CGSize(width: 0, height: 3)
            layer.shadowOpacity = 5
            layer.shadowRadius = 1
        }
        clipsToBounds = true //false para sombra
    }
}
