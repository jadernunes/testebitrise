//
//  FidelityView.swift
//  MarketPlace
//
//  Created by Jáder Borba Nunes on 10/10/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class FidelityView: UIView,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var labelDesc                : UILabel!
    @IBOutlet weak var viewHeaderFidelity       : UIView!
    @IBOutlet weak var collectionViewListStamp  : UICollectionView!
    @IBOutlet weak var buttonShowRegulation     : UIButton!
    var maxItems                                : Int!
    var countItems                              : Int!
    var urlImage                                : String!
    var descriptionTerms                        : String!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    class func instanceFromNib() -> FidelityView {
        return UINib(nibName: "FidelityView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! FidelityView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionViewListStamp.registerNib(UINib(nibName: "FidelityCollectionViewCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "cell")
        self.collectionViewListStamp.delegate = self
        self.collectionViewListStamp.dataSource = self
        self.viewHeaderFidelity.backgroundColor = SessionManager.sharedInstance.cardColor
        self.buttonShowRegulation.layer.cornerRadius = 17.5
        self.buttonShowRegulation.setTitleColor(SessionManager.sharedInstance.cardColor, forState: UIControlState.Normal)
    }
    
    //MARK: UICollection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = (collectionView.frame.size.width / 6)
        return CGSizeMake(size , size)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return maxItems
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! FidelityCollectionViewCell
        
        cell.layer.cornerRadius = 20
        
        dispatch_async(dispatch_get_main_queue(), {
            if indexPath.row < self.countItems {
                if (self.urlImage != nil) {
                    if(self.urlImage.characters.count > 0){
                        cell.imageViewIconStamp.addImage(self.urlImage)
                    } else {
                        cell.imageViewIconStamp.image = UIImage.init(named: "iconChecked")
                    }
                }else {
                    cell.imageViewIconStamp.image = UIImage.init(named: "iconChecked")
                }
        
            } else {
                cell.imageViewIconStamp.image = UIImage.init(named: "iconUnChecked")
            }
        })
        
        return cell
    }
    
    //MARK: Show regulation fidelity
    
    @IBAction func showRegulation(sender: AnyObject) {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TermsFidelityViewController") as! TermsFidelityViewController
        controller.descriptionTerms = self.descriptionTerms
        
        let deviceBounds = UIScreen.mainScreen().bounds
        let formSheet = MZFormSheetController.init(viewController: controller)
        formSheet.shouldDismissOnBackgroundViewTap = true;
        formSheet.cornerRadius = 8.0;
        formSheet.portraitTopInset = 6.0;
        formSheet.landscapeTopInset = 6.0;
        formSheet.presentedFormSheetSize = CGSizeMake(deviceBounds.width - 40.0, deviceBounds.height - 40);
        formSheet.formSheetWindow.transparentTouchEnabled = false;
        
        formSheet.presentAnimated(true) { (viewController: UIViewController!) in
            
        }
    }
    
}
