//
//  FoodAddressEditVC.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 06/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

protocol FoodAddressEditVCDelegate {
    
}

class FoodAddressEditVC: UIViewController {
    @IBOutlet var tableView         : UITableView!
    @IBOutlet weak var btnSalvar    : FoodRoundButton! {
        didSet {
            btnSalvar.setBackgroundImage(FoodUtils.imageLayerForGradientBackground(btnSalvar), forState: .Normal)
        }
    }
    @IBOutlet weak var btnEditar    : FoodRoundButton!
    var place                       : Place?
    private let cepDelegate         = FoodCepDelegate()
    private let txtgenericDelegate  = FoodGenericAddressTextfieldDelegate()
    private let txtNumDelegate      = FoodGenericAddressTextfieldDelegate()
    private let txtCompDelegate     = FoodGenericAddressTextfieldDelegate()
    var address                     : Address! {
        didSet {
            let temp = address
            address = Address()
            address.city = temp.city
            address.street = temp.street
            address.number = temp.number
            address.complement = temp.complement
            address.zip = temp.zip
            address.latitude = temp.latitude
            address.longitude = temp.longitude
            address.neighborhood = temp.neighborhood
            address.state = temp.state
            
            if let latitude = self.place?.latitude, let longitude = self.place?.longitude {
                address.latitude = latitude
                address.longitude = longitude
            }
        }
    }
    var editingAddress                              : Bool = false
    var isGeolocalized                              : Bool = false
    var foodDelegate                                : FoodAddressEditVCDelegate?
    @IBOutlet weak var contraintWidthSalvarHalf     : NSLayoutConstraint!
    @IBOutlet weak var contraintWidthSalvarFull     : NSLayoutConstraint!
    
    override func viewDidLoad() {
        tableView.registerNib(UINib(nibName: "FoodAddressResultCell", bundle: nil), forCellReuseIdentifier: "FoodAddressResultCell")
        tableView.registerNib(UINib(nibName: "FoodAddressEditCell", bundle: nil), forCellReuseIdentifier: "FoodAddressEditCell")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)

        validateAddress()
        setContraintPriority()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        FoodUtils.customizeViewTitle(NSLocalizedString("Informar um endereço", comment: ""),vc: self)
        self.resetInsets()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.resetButtons()
    }
    
    /// faz as ações necessárias caso seja passado um endereco (ex: geolozalicação)
    private func validateAddress() {
        
        if address == nil {
            address = Address()
            
            if let arrayText = place?.main_text?.componentsSeparatedByString(",") {
                address.street = arrayText.first
            } else {
                address.street = place?.main_text
            }

            if let arrayText = place?.secondary_text?.componentsSeparatedByString("-") {
                address.neighborhood = arrayText.first
                
                if arrayText.first?.componentsSeparatedByString(",").count > 0 {
                    address.neighborhood = arrayText.first?.componentsSeparatedByString(",")[0]
                    
                    if arrayText.first?.componentsSeparatedByString(",").count > 1 {
                        address.city = arrayText.first?.componentsSeparatedByString(",")[1]
                    }
                }
                
                if arrayText.last?.componentsSeparatedByString(",").count > 0 {
                    address.state = arrayText.last?.componentsSeparatedByString(",")[0].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                }
                
            }  else {
                address.neighborhood = ""
                address.state = ""
            }
        }
        
        validateBtnSalvar()
    }
    
    
    /// Valida se o botão salvar pode ser ativado
    ///
    /// - Returns: retorna um booleano indicando se o botão esta ativo ou nao
    private func validateBtnSalvar() -> Bool {
        
        guard address.number?.characters.count > 0 else {
            btnSalvar.enabled = false
            btnSalvar.alpha = 0.30
            return false
        }
        
        guard let _ = address.street,
            let _ = address.number,
            let _ = address.zip where
            address.zip?.characters.count == 9
            else {
                btnSalvar.enabled = false
                btnSalvar.alpha = 0.3
                return false
        }
        
        btnSalvar.enabled = true
        btnSalvar.alpha = 1
        return true
    }
    
    
    /// Salva o endereço preenchido no formulario
    ///
    /// - Parameter sender: Quem enviou a acao (no caso o UIButton)
    @IBAction private func salvarAddress(sender: AnyObject?) {
        self.btnEditar.enabled = false
        let realm = try! Realm()
        
        let listAddress = AddressPersistence.getAllLocalAddresses()
        for addr in listAddress {
            try! realm.write {
                addr.selected = false
            }
        }
        
        if let latitude = self.place?.latitude, let longitude = self.place?.longitude {
            address.latitude = latitude
            address.longitude = longitude
        }
        if !editingAddress {
            address.id = AddressPersistence.getNextKey(realm, aClass: Address.self)
        }
        
        address.selected = true
        if address.complement == nil {
            address.complement = ""
        }
        try! realm.write {
            realm.create(Address.self, value: address!, update: true)
        }
        
        if let navController = self.navigationController {
            if let listEcsController = storyboard?.instantiateViewControllerWithIdentifier("FoodListEcsVC") as? FoodListEcsVC {
                listEcsController.startPage = kFoodStarterPageController.entrega
                listEcsController.address = address
                navController.pushViewController(listEcsController, animated: true)
            }
        }
    }
    
    
    /// call the Controller to change the address, using google autocomplete
    ///
    /// - Parameter sender: the caller
    @IBAction private func editarAddress (sender: AnyObject?) {
        if let navController = self.navigationController {
            self.btnSalvar.enabled = false
            self.title = ""
            if let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC") {
                navController.pushViewController(vc , animated: true)
            }
        }
    }
    
    func keyboardWasShown(notification: NSNotification) {
        if isViewLoaded() && view.window != nil {
            let info: Dictionary = notification.userInfo!
            let keyboardSize: CGSize = (info[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue.size)!
            var contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            if let height = self.navigationController?.navigationBar.frame.size.height {
                contentInsets = UIEdgeInsetsMake(height+18, 0.0, keyboardSize.height, 0.0)
            }
            self.tableView.contentInset = contentInsets
            self.tableView.scrollIndicatorInsets = contentInsets
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        if isViewLoaded() && view.window != nil {
            self.resetInsets()
        }
    }
    
    /// reset buttons state, changed when clicked
    private func resetButtons() -> Void {
        self.btnEditar.enabled = true
        self.btnSalvar.enabled = true
    }
    
    private func resetInsets() {
        var contentInsets = UIEdgeInsetsZero
        if let height = self.navigationController?.navigationBar.frame.size.height {
            contentInsets = UIEdgeInsetsMake(height+18, 0.0, 0.0, 0.0)
        }
        self.tableView.contentInset = contentInsets
        self.tableView.scrollIndicatorInsets = contentInsets
    }
    
    func didEditTextNumber(sender:UITextField) {
        address.number = sender.text
        validateBtnSalvar()
    }
    
    func didReturnTextNumber(sender:UITextField) {
        sender.resignFirstResponder()
        validateBtnSalvar()
    }
    
    func didEditTextComplement(sender:UITextField) {
        address.complement = sender.text
        validateBtnSalvar()
    }
    
    func didEditTextCEP(sender:UITextField) {
        address.zip = sender.text
        validateBtnSalvar()
    }
    
}

extension FoodAddressEditVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("FoodAddressResultCell", forIndexPath: indexPath) as! FoodAddressResultCell
            
            
            
            cell.lblAddressLine1?.text = address.street
            if let neighborhood = address.neighborhood {
                cell.lblAddressLine2?.text = "\(neighborhood) - "
                if let state = address.state {
                    cell.lblAddressLine2?.text?.appendContentsOf("\(state)")
                }
            } else {
                cell.lblAddressLine2?.text = ""
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            cell.foodDelegate = self
            if editingAddress {
                cell.btnEditar.hidden = false
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("FoodAddressEditCell", forIndexPath: indexPath) as! FoodAddressEditCell
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            txtNumDelegate.maxSize=45
            cell.txtNumero.delegate = txtNumDelegate
            cell.txtNumero.addTarget(self,
                                     action: #selector(didEditTextNumber(_:)),
                                     forControlEvents: .EditingChanged)
            
            txtCompDelegate.maxSize=128
            cell.txtComplemento.delegate = txtCompDelegate
            cell.txtComplemento.addTarget(self,
                                          action: #selector(didEditTextComplement(_:)),
                                          forControlEvents: .EditingChanged)
            
            cell.txtCep.delegate = cepDelegate
            cell.txtCep.addTarget(self,
                                  action: #selector(didEditTextCEP(_:)),
                                  forControlEvents: .EditingChanged)
            
            //quando vem da geolocalização, preencher com o CEP
            if address != nil {
                if let cep = address.zip {
                    cell.txtCep.text = cep
                }
                if editingAddress {
                    if let number = address.number {
                        cell.txtNumero.text = number
                    }
                    if let complemento = address.complement {
                        cell.txtComplemento.text = complemento
                    }
                }
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 75
        } else {
            return 240
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //chama a view customizada
        if section == 0 && isGeolocalized {
            let header = FoodCellMessage(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 90))
            return header
        }
        return nil
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 && isGeolocalized {
            return NSLocalizedString("cellMessageGeolocation", tableName: "GastronomiaStrings", comment: "")
        }
        return nil
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && isGeolocalized {
            return 100
        }
        return 0
    }
    
    func setContraintPriority() {
//        if editingAddress {
//            self.btnEditar.hidden = false
//            self.contraintWidthSalvarFull.priority = 999
//            self.contraintWidthSalvarHalf.priority = 1000
//        } else {
            self.btnEditar.hidden = true
            self.contraintWidthSalvarFull.priority = 1000
            self.contraintWidthSalvarHalf.priority = 999
//        }
    }
}

extension FoodAddressEditVC: FoodAddressResultCellDelegate {
    func didPressEditButton(sender: AnyObject) {
        self.editarAddress(sender)
    }
}

