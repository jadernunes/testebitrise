//
//  FoodConstants.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 01/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation


let kFoodBalloonAlert = "alreadyShowBalloon"

enum kFoodStarterPageController: Int {
    case entrega    = 0
    case retirada   = 1
    case comanda    = 2
}

enum kSectionListEcs: Int {
    case opened = 0
    case closed = 1
    case none   = 3
}

enum kFoodSegControlDistance: Int {
    case km30   = 0
    case km15   = 1
    case km05   = 2
    case km01   = 3
    
    func getStringDescription(value: kFoodSegControlDistance) -> String {
        switch value {
        case .km30:
            return "30000"
        case .km15:
            return "15000"
        case .km05:
            return "5000"
        case .km01:
            return "1000"
        }
    }
}
