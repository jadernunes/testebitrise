//
//  FoodListTableRetiradaDatasource.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 22/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

protocol TableRetiradaControllerDelegate {
    var locationRetirada: CLLocation? {get set}
    func didPressRetiradaCell(indexPath: NSIndexPath, unity: Unity) -> Void
    func didCallRefreshRetirada(tempAddress: Address?) -> Void
    func callRefreshViewRetirada() -> Void
}

extension TableRetiradaControllerDelegate {
    func didCallRefreshRetirada(tempAddress: Address? = nil) -> Void {
        return didCallRefreshRetirada(tempAddress)
    }
    func callRefreshViewRetirada() -> Void {
        //only to make this func optional
    }
}

class FoodListTableRetiradaController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegateFood: TableRetiradaControllerDelegate?
    private var cellIdentifier = "FoodListaEcsCell"
    private var viewGeolocationError = FoodViewGeolocationError()
    var dados: Results<Unity>? {
        didSet {
            var distance = "30000"
            if self.segControlDistance != nil {
                let segDistance = kFoodSegControlDistance(rawValue: self.segControlDistance.selectedSegmentIndex)
                distance = segDistance!.getStringDescription(segDistance!)
            }
            dadosFiltered = dados?.filter(String(format: "distanceFromUser < \(distance)"))
            if dadosFiltered != nil {
                FoodUtils.hideLoadingView(self.view)
            }
        }
    }
    private var dadosFiltered: Results<Unity>? {
        didSet {
            dadosFiltered = dadosFiltered?.sorted("distanceFromUser", ascending: true)
            self.dadosOrdered = FoodUtils.formatRealmData(dadosFiltered)
        }
    }
    private var dadosOrdered: (opened:[Unity],closed:[Unity])? {
        didSet {
            if self.tableEcs != nil {
                self.tableEcs.reloadData()
            }
        }
    }
    @IBOutlet weak var tableEcs: UITableView! {
        didSet{
            tableEcs.registerNib(UINib(nibName: "FoodListaEcsCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
            
            if #available(iOS 10.0, *) {
                let refreshControl = UIRefreshControl()
                refreshControl.attributedTitle = NSAttributedString(string: "")
                refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
                self.tableEcs.refreshControl = refreshControl
                self.tableEcs.addSubview(refreshControl)
            }
            
            self.tableEcs.backgroundView = FoodViewNoData()
        }
    }
    @IBOutlet weak var segControlDistance: UISegmentedControl!
    private var didOpenSettings: Bool = false
    
    func validacoes() {
        self.delegateFood?.callRefreshViewRetirada()
        if let address = AddressPersistence.getAddressBySelected() {
            self.delegateFood?.didCallRefreshRetirada(address)
        } else if CLLocationManager.locationServicesEnabled() {
            print("location services enabled")
            if CLLocationManager.authorizationStatus() == .Denied ||
                CLLocationManager.authorizationStatus() == .NotDetermined {
                viewGeolocationError = FoodUtils.createGeolocationErrorView(self.view.frame, delegate: self, isDelivery: false)
                view.addSubview(viewGeolocationError)
                view.bringSubviewToFront(viewGeolocationError)
            } else {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    FoodUtils.hideLoadingView(self.view)
                    if let address = address {
                        self.delegateFood?.didCallRefreshRetirada(address)
                    }
                    //caso não ter conseguido buscar endereco
                    if errorDescription != nil {
                    }
                })
            }
        }
    }
    
    override func viewDidLoad() {
        validacoes()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //notificacao para validar se o usuario permitiu o gps
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.validacoes), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if dadosOrdered?.opened.count > 0 && dadosOrdered?.closed.count > 0 {
            return 2
        } else if dadosOrdered?.opened.count > 0 || dadosOrdered?.closed.count > 0 {
            return 1
        }
        
        emptyTableView()
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        self.tableEcs.backgroundView = UIView()
        self.tableEcs.separatorStyle = .SingleLine
        
        if sect == kSectionListEcs.opened {
            if let count = dadosOrdered?.opened.count {
                return count
            }
        } else if sect == kSectionListEcs.closed {
            if let count = dadosOrdered?.closed.count {
                return count
            }
        }
        
        emptyTableView()
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! FoodListaEcsCell
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)
        
        //Valida que tipo de EC é, se aberto ou fechado
        var unity:Unity
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: true, userLocation: true)
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
            return FoodUtils.formatListEcsCell(unity, cell: cell, opened: false, userLocation: true)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var unity:Unity
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: indexPath, section: nil)
        
        if sect == kSectionListEcs.opened {
            unity = dadosOrdered!.opened[indexPath.row]
        } else {
            unity = dadosOrdered!.closed[indexPath.row]
        }
        self.loadUnity(indexPath, unity: unity)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sect = FoodUtils.getListEcSection(dadosOrdered!, indexPath: nil, section: section)
        
        //sem um titulo não chama a headerview
        if sect == kSectionListEcs.opened {
            return ""
        } else {
            return NSLocalizedString("Fechados neste momento", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //chama a view customizada
        let header = FoodViewHeaderListEcs(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        return header
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //personaliza o texto da view
        if let header = view as? FoodViewHeaderListEcs {
            header.textLabel?.font = UIFont.foodDosisLightFont()
            header.textLabel?.textColor = UIColor.foodGreyishBrownColor()
        }
    }
    
    @IBAction func didChangeValue(sender: AnyObject) {
        if let sender = sender as? UISegmentedControl {
            refreshTableView(kFoodSegControlDistance(rawValue: sender.selectedSegmentIndex)!)
        }
    }
    
    private func emptyTableView() {
        self.tableEcs.backgroundView = FoodViewNoData()
        self.tableEcs.separatorStyle = .None
    }
    
    func refreshTableView(segValueType: kFoodSegControlDistance) {
        let desc = "distanceFromUser < \(segValueType.getStringDescription(segValueType))"
        dadosFiltered = dados?.filter(desc)
    }
    
    func refresh(sender:AnyObject) {
        FoodUtils.showLoadingView([GastronomiaKit.imageOfLoaderGastro1,
            GastronomiaKit.imageOfLoaderGastro2,
            GastronomiaKit.imageOfLoaderGastro3,
            GastronomiaKit.imageOfLoaderGastro4,
            GastronomiaKit.imageOfLoaderGastro5,
            GastronomiaKit.imageOfLoaderGastro6,
            GastronomiaKit.imageOfLoaderGastro7,
            GastronomiaKit.imageOfLoaderGastro8,
            GastronomiaKit.imageOfLoaderGastro9,
            GastronomiaKit.imageOfLoaderGastro10,
            GastronomiaKit.imageOfLoaderGastro11,
            GastronomiaKit.imageOfLoaderGastro12],
                                  targetView: (self.view)!,
                                  message: NSLocalizedString("Carregando estabelecimentos", tableName: "GastronomiaStrings", comment: ""))
        
        delegateFood?.didCallRefreshRetirada()
        if #available(iOS 10.0, *) {
            tableEcs.refreshControl?.endRefreshing()
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func loadUnity(indexPath: NSIndexPath, unity: Unity) {
        let detailsVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsTableViewController
        
        detailsVc.title = unity.name
        detailsVc.idObjectReceived = unity.id
        detailsVc.typeObjet = TypeObjectDetail.Store
        if self.navigationController?.topViewController != detailsVc {
            self.navigationController?.pushViewController(detailsVc, animated: true)
        }
        
        let api = ApiServices()
        api.getUnityByIDService(unity.id)
    }
}
//MARK: - FoodViewGeolocationErrorDelegate
extension FoodListTableRetiradaController: FoodViewGeolocationErrorDelegate {
    func didPressAtivarGeolocalizacao(button: UIButton) {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways ||
                CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    if let address = address {
                        self.title = ""
                        FoodUtils.callEditAddressVC(address, navController:  self.navigationController)
                    }
                })
            } else {
                didOpenSettings = true
                FoodAlerts().showAlertGeoLocation()
            }
        }
    }
    
    func didPressInformarEndereco(button: UIButton) {
        self.title = ""
        let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC")
        self.showViewController(vc!, sender: self)
    }
    
    func didMoveToView() {
        
    }
}
