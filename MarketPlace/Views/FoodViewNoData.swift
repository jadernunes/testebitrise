//
//  FoodViewNoData.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 20/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

protocol FoodViewNoDataDelegate {
    func didPressView(sender: UIView)
}

class FoodViewNoData: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var message: UILabel!
    var delegate: FoodViewNoDataDelegate?
    
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiateView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiateView()
    }
    
    func instantiateView() {
        UINib(nibName: "FoodViewNoData", bundle: nil).instantiateWithOwner(self, options: nil)
        addSubview(view)
        view.frame = self.bounds
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didPressView(_:)))
        view.addGestureRecognizer(tap)
    }
    
    func didPressView(sender: UIView) {
        self.delegate?.didPressView(sender)
    }
    
}
