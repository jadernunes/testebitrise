//
//  String+NewUnityDetails.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 19/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension String {
    
    /// NewUnityDetailsBannerTableViewCell constants
    public static let unityOpen     = "ABERTO"
    public static let unityClosed   = "FECHADO"
    
    ///NewUnityDetailsItemsTableViewCell
    public static let orderMessage      = "Veja o cardápio"
    public static let schedulingMessage = "Agendamento de Serviços"
    public static let orderCheckMessage = "Pagamento de Comanda"
    public static let invalidTab        = "Comanda Inválida!"
    public static let payTab            = "PAGAR"
    public static let attUnity          = "Atenção"
    
    public static let orderIconName     = "ic_menu"
    public static let scheduleIconName  = "ic_schedule"
    public static let tabIconName       = "ic_tab"
    
    ///NewUnityDetailsShiftsTableViewCell
    public static let hourAvaliable = "Horário de atendimento"
    
    /// NewUnitydetailsAddressTableViewCell constants
    public static let howToGetThere = "Como chegar?"
    
    /// NewUnitydetailsContactTableViewCell constants
    public static let contact = "Contato"
}
