//
//  FoodViewHeaderListEcs.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 04/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

class FoodViewHeaderListEcs: UIView {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textLabel: UILabel!
    
    required init? (coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiateView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiateView()
    }
    
    func instantiateView() {
        UINib(nibName: "FoodViewHeaderListEcs", bundle: nil).instantiateWithOwner(self, options: nil)
        addSubview(view)
        view.frame = self.bounds
    }
}
