//
//  HoursTableViewCell.swift
//  MarketPlace
//
//  Created by Luciano on 10/3/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class HoursTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tableView.registerNib(UINib(nibName: "TimeRangeTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: TIME_RANGE)


    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
