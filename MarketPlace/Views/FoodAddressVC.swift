//
//  FoodAddressCreationController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 23/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import GooglePlaces

class FoodAddressVC: UIViewController {
    
    private var place: Place?
    
    //"AIzaSyDI6j1Bz3Y37SgICkk2LXl51GmJmVRlW2o", homolog
    //"AIzaSyBSyuQQarLBRBT7E5ZwBR-S2rEpzOGMIYM", producao
    let gpaViewController = GooglePlacesAutocomplete(
        apiKey: KEY_GOOGLE_MAPS,//"AIzaSyB91F-pFZQI5IF9XjjNnuTfpouhujT6Ubg",
        placeType: .Address
    )
    
    override func viewDidLoad() {
        FoodUtils.customizeViewTitle(NSLocalizedString("Informar um endereço", comment: ""), vc: self)
        self.presentGoogleAutocomplete(false)
    }
    
    func presentGoogleAutocomplete(animated: Bool) {
        self.addChildViewController(gpaViewController)
        gpaViewController.placeDelegate = self // Conforms to GooglePlacesAutocompleteDelegate
        self.view.addSubview(gpaViewController.view)
        gpaViewController.view.layoutIfNeeded()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "FoodAddressEditVC" {
            if let destVC = segue.destinationViewController as? FoodAddressEditVC {
                destVC.place = self.place
            }
        }
    }
    
}

extension FoodAddressVC : GooglePlacesAutocompleteDelegate {
    func placesFound(places: [Place]) {
        print(places)
    }
    
    func placeSelected(place: Place) {
        place.getDetails { (details) in            
            self.place = place
            self.place?.latitude = details.latitude
            self.place?.longitude = details.longitude
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodAddressEditVC") as? FoodAddressEditVC {
                vc.hidesBottomBarWhenPushed = true
                vc.place = self.place
                if let navController = self.navigationController {
                    if !navController.topViewController!.isKindOfClass(FoodAddressEditVC) {
                        navController.pushViewController(vc, animated: true)
                    }
                }
            }
            
        }
    }
    
    func placeViewClosed() {
        self.presentedViewController?.dismissViewControllerAnimated(true, completion: {
            
        })
    }
    
    func placeBackButtonPressed() {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
    }
}


