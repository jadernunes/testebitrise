//
//  CheckFooter.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class CheckFooter: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelTextPrice: UILabel!    
    @IBOutlet weak var viewPrice: UIView!
    @IBOutlet weak var cellConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewCheck: UIImageView!
    @IBOutlet weak var labelTitleSuccess: UILabel!
    @IBOutlet weak var labelTextSuccess: UILabel!
    
    override func awakeFromNib() {
            
        viewPrice.backgroundColor = SessionManager.sharedInstance.cardColor
        labelPrice.textColor = LayoutManager.labelSecondColor
        labelTextPrice.textColor = LayoutManager.labelSecondColor
        labelPrice.font = LayoutManager.primaryFontWithSize(22)
        labelTextPrice.font = LayoutManager.primaryFontWithSize(22)
        
    }

}
