//
//  CheckTableViewCell.swift
//  MarketPlace
//
//  Created by Matheus Stefanello Luz on 10/24/16.
//  Copyright © 2016 4all. All rights reserved.
//

import UIKit

class ReserveViewCell: UITableViewCell {
    
    
    @IBOutlet weak var buttonReserve    : UIButton!
    var rootViewController              : UIViewController!
    var unity                           : Unity!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor                = UIColor.clearColor()
        buttonReserve.titleLabel?.font    = LayoutManager.primaryFontWithSize(16)
        // Rounded corners.
        buttonReserve.layer.cornerRadius = 4
        buttonReserve.clipsToBounds      = false
        // A thin border.
        buttonReserve.setTitleColor(SessionManager.sharedInstance.cardColor, forState: .Normal)
        buttonReserve.layer.borderColor  = SessionManager.sharedInstance.cardColor.CGColor
        buttonReserve.layer.borderWidth  = 1
       /* let imageCatalogIcon = UIImage(named: "4food - mesa")
        dispatch_async(dispatch_get_main_queue(), {
            self.buttonReserve.layoutIfNeeded()
            self.buttonReserve.setIcon(imageCatalogIcon!, inLeft: true)
        })*/
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonReserveClicked(sender: AnyObject) {
        let mainStoryboard = UIStoryboard(name: "4all", bundle: NSBundle.mainBundle())
        let vc = mainStoryboard.instantiateViewControllerWithIdentifier("ReservaMesaViewController") as! ReservaMesaViewController
        rootViewController.navigationController?.pushViewController(vc, animated: true)
    }
}
