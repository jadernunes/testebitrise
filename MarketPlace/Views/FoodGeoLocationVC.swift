//
//  FoodLoadingLocationController.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 15/02/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import CoreLocation

protocol FoodGeoLocationVCDelegate {
    func didLoadAddress(address: Address?) -> Void
}

class FoodGeoLocationVC : UIViewController {
    
    var foodDelegate: FoodGeoLocationVCDelegate?
    var locationManager = CLLocationManager()
    var isDelivery: Bool = false
    private var viewGeoActivation   = FoodViewGeoActivation()
    private var viewGeoError        = FoodViewGeolocationError()
    private var viewLoading         : FoodViewLoading!
    private var viewCurrentAlert    : UIView?
    private var didBecomeActive     : Bool = false
    private var timerOutLoader      = NSTimer()
    
    override func viewDidLoad() {
        self.hidesBottomBarWhenPushed = true
        
        //notificacao para validar se o usuario permitiu o gps
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setActive), name: UIApplicationDidBecomeActiveNotification, object: nil)
        
        self.loadViews()
        self.validacoes()
    }
    
    override func viewDidAppear(animated: Bool) {
        FoodUtils.customizeViewTitle("", vc: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        FoodUtils.customizeViewTitle(NSLocalizedString("Gastronomia", comment: ""), vc: self)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.manageButtons(true)
        self.manageLoadingView(false)
    }
    
    func validacoes() {
        self.manageButtons(true)
        manageLoadingView(true)
        if let address = AddressPersistence.getAddressBySelected() {
            foodDelegate?.didLoadAddress(address)
        } else if CLLocationManager.locationServicesEnabled() {
            print("location services enabled")
            if CLLocationManager.authorizationStatus() == .Denied ||
                CLLocationManager.authorizationStatus() == .NotDetermined {
                self.viewGeoError.alpha = 1.0
                self.view.bringSubviewToFront(self.viewGeoError)
                self.manageButtons(true)
            } else {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    self.viewGeoError.hidden = false
                    //localiza automaticamente e joga para o endereco
                    if let address = address {
                        self.loadViewAddressEdit(address)
                    }
                    if errorDescription != nil {
                        self.manageLoadingView(false)
                        self.manageButtons(true)
                    }
                })
            }
        }
    }
    
    func setActive() {
        didBecomeActive = true
        self.validacoes()
    }
    
    
    /// Load the private views
    func loadViews() -> Void {
        self.viewGeoError = FoodUtils.createGeolocationErrorView(self.view.frame, delegate: self, isDelivery: self.isDelivery)
        self.view.addSubview(viewGeoError)        
    }
    
    private func loadViewAddressEdit(address:Address) {
        if let navController = self.navigationController {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodAddressEditVC") as? FoodAddressEditVC {
                vc.address = address
                vc.editingAddress = true
                vc.isGeolocalized = true
                NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
                manageButtons(false)
                navController.showViewController(vc, sender: self)
            }
        }
    }
    
    private func loadViewListEcs(address:Address) {
        if let navController = self.navigationController {
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FoodListEcsVC") as? FoodListEcsVC {
                vc.address = address
                NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
                manageButtons(false)
                navController.showViewController(vc, sender: self)
            }
        }
    }
    
    private func manageButtons(enable: Bool) -> Void {
        if enable {
            self.viewGeoError.btnAtivarGeolocalizacao.enabled = true
            self.viewGeoError.btnAtivarGeolocalizacao.alpha = 1
            self.viewGeoError.btnInformarEndereco.enabled = true
            self.viewGeoError.btnInformarEndereco.alpha = 1
        } else {
            self.viewGeoError.btnAtivarGeolocalizacao.enabled = false
            self.viewGeoError.btnAtivarGeolocalizacao.alpha = 0.5
            self.viewGeoError.btnInformarEndereco.enabled = false
            self.viewGeoError.btnInformarEndereco.alpha = 0.5
        }
    }
    
    @objc private func popController() -> Void {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    private func manageLoadingView(show: Bool) {
        if show {
            if #available(iOS 10.0, *) {
                self.timerOutLoader = NSTimer.scheduledTimerWithTimeInterval(10, repeats: false, block: { (timer) in
                    self.navigationController?.popViewControllerAnimated(true)
                })
            } else {
                // Fallback on earlier versions
            }
            
            FoodUtils.showLoadingView([GastronomiaKit.imageOfLoader1,
                GastronomiaKit.imageOfLoader2,
                GastronomiaKit.imageOfLoader3,
                GastronomiaKit.imageOfLoader4,
                GastronomiaKit.imageOfLoader5,
                GastronomiaKit.imageOfLoader6,
                GastronomiaKit.imageOfLoader7,
                GastronomiaKit.imageOfLoader8,
                GastronomiaKit.imageOfLoader9,
                GastronomiaKit.imageOfLoader10,
                GastronomiaKit.imageOfLoader11,
                GastronomiaKit.imageOfLoader12],
                                      targetView: self.view,
                                      message: "Buscando sua localização")

        } else {
            self.timerOutLoader.invalidate()
            FoodUtils.hideLoadingView(self.view)
        }
    }
    
}
//MARK: CLLocationManagerDelegate
extension FoodGeoLocationVC : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedAlways {
            if CLLocationManager.isMonitoringAvailableForClass(CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                }
            }
        }
    }
}
//MARK: FoodViewGeolocationErrorDelegate
extension FoodGeoLocationVC : FoodViewGeolocationErrorDelegate {
    
    func didMoveToView() {
    }
    
    func didPressAtivarGeolocalizacao(button: UIButton) {
        self.manageButtons(false)
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways ||
            CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
                FoodLocationServices.getLocationStringInfo({ (address, errorDescription) in
                    self.viewGeoError.hidden = false
                    if let address = address {
                        self.title = ""
                        self.loadViewAddressEdit(address)
                    }
                })
            } else {
                FoodAlerts().showAlertGeoLocation()
            }
            
        }
        
    }
    
    func didPressInformarEndereco(button: UIButton) {
        self.manageButtons(false)
        self.title = ""
        let vc = storyboard?.instantiateViewControllerWithIdentifier("FoodAddressVC")
        manageButtons(false)
        self.showViewController(vc!, sender: self)
    }
    
    
}


