//
//  FoodSearchBarExtension.swift
//  MarketPlace
//
//  Created by Leandro Lopes on 04/03/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension UISearchBar {
    func changeSearchBarColor(color : UIColor) {
        for subView in self.subviews {
            for subSubView in subView.subviews {
                if subSubView.conformsToProtocol(UITextInputTraits.self) {
                    let textField = subSubView as! UITextField
                    textField.backgroundColor = color
                    break
                }
            }
        }
    }
}
