//
//  UIImage+NewUnityDetail.swift
//  MarketPlace
//
//  Created by Anderson Kloss Maia on 19/01/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation

extension UIImageView {
    func removeActivityViewAsSubview () {
        for view in self.subviews {
            if let _ = view as? UIActivityIndicatorView {
                view.removeFromSuperview()
            }
        }
    }
}
