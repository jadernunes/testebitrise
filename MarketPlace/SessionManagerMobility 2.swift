//
//  SessionManagerMobility.swift
//  MarketPlace
//
//  Created by Gabriel Miranda Silveira on 24/04/17.
//  Copyright © 2017 4all. All rights reserved.
//

import Foundation
import RealmSwift

class SessionManagerMobility {
    static let sharedInstance = SessionManagerMobility()
    var needUpdateList = false
    let mainColor = UIColor.init(red: 219.0/255, green: 202.0/255, blue: 44.0/255, alpha: 1.0)
    
    func customizeViewWithTitle(title: String, vc: UIViewController){
        vc.navigationItem.title = title
        vc.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
        vc.navigationController?.navigationBar.barTintColor = SessionManagerMobility.sharedInstance.mainColor
        vc.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        if let font = UIFont(name: "Dosis-Regular", size: 22) {
            let navBarAttributesDictionary: [String: AnyObject]? = [
                NSForegroundColorAttributeName: UIColor.blackColor(),
                NSFontAttributeName: font
            ]
            vc.navigationController?.navigationBar.titleTextAttributes = navBarAttributesDictionary
        }
    }
    
    func catalogReceived(dic: NSDictionary) {
        
        if (dic.count > 0) {
            MobilityFilesUtil.storeInFile(dic as [NSObject : AnyObject], fileName: "mobilityCatalog")
        }
    }
    
    func syncCardsWithServer(completion: (Void) -> (Void)){
        
        let realm = try! Realm()
        let cards:Results<CardModel> = realm.objects(CardModel)
        let totalItems = cards.count
        
        var itemsSync = 0;
        
        let api = ApiServicesMobility()
        
        api.successCase = { (response) in
            if (response as? NSDictionary) != nil {
                itemsSync += 1
            }
            
            //it wait until syncs everything
            NSLog("Log -> WAITING SYNC %d of %d", itemsSync, totalItems)
            
            if (itemsSync == totalItems) {
                //call completion handler
                NSLog("Log -> %@", "Completion Called");
                //DELETE ALL LOCAL CONTENTE AND REFRESHS WITH SERVER DATA
                
                realm.beginWrite()
                realm.deleteAll()
                try! realm.commitWrite()
                completion()
            }
            
        }
        
        api.failureCase = { (cod, message) in
            itemsSync += 1
            
            //it wait until syncs everything
            NSLog("Log -> WAITING SYNC %d of %d", itemsSync, totalItems);
            
            if (itemsSync == totalItems) {
                //call completion handler
                NSLog("Log -> %@", "Completion Called");
                completion();
            }
        };
        
        
        if (totalItems > 0) {
            for card:CardModel in cards {
                
                api.consultaSaldoECartaoValido(card.issuerNr, andIssuerId: "\(card.issuerId)", andCityID: "\(card.cityId)", andSessionToken:User.sharedUser().token)
            }
            
        }
        if (totalItems == 0 || itemsSync == totalItems) {
            //call completion handler
            NSLog("Log -> %@", "Completion Called");
            completion();
        }

    }
    
    
}
