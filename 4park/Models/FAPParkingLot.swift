//
//  FAPParkingLot.swift
//  4all Park
//
//  Created by Cristiano Matte on 15/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

import RealmSwift

class FAPParkingLot: Object {
    typealias JSONKeys = WPSClientConstants.JSONKeys.ParkingLot
    
    dynamic var id = -1
    dynamic var name = ""
    dynamic var address = ""
    dynamic var neighborhood = ""
    dynamic var city = ""
    dynamic var state = ""
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    dynamic var hasOpticalReader = false
    dynamic var hasMonthlyPaymentType = false
        
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(wpsClientDictionary dictionary: [String: AnyObject]) {
        self.init()
        
        self.id           = (dictionary[JSONKeys.IdKey] as? Int) ?? self.id
        self.name         = (dictionary[JSONKeys.NameKey] as? String) ?? self.name
        self.address      = (dictionary[JSONKeys.AddressKey] as? String) ?? self.address
        self.neighborhood = (dictionary[JSONKeys.NeighborhoodKey] as? String) ?? self.neighborhood
        self.city         = (dictionary[JSONKeys.CityKey] as? String) ?? self.city
        self.state        = (dictionary[JSONKeys.StateKey] as? String) ?? self.state
        
        if let latitude = dictionary[JSONKeys.LatitudeKey] as? String {
            self.latitude = Double(latitude) ?? self.latitude
        }
        
        if let longitude = dictionary[JSONKeys.LongitudeKey] as? String {
            self.longitude = Double(longitude) ?? self.longitude
        }
        
        if let mpt = dictionary[WPSClientConstants.JSONKeys.MonthlyPaymentTypeKey] as? [[String:AnyObject]] where mpt.count > 0 {
            hasMonthlyPaymentType = true
        }
    }
    
}
