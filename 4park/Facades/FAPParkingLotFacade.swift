//
//  FAPParkingLotFacade.swift
//  4all Park
//
//  Created by Cristiano Matte on 07/07/16.
//  Copyright © 2016 4all. All rights reserved.
//

import RealmSwift

class FAPParkingLotFacade {
    private static let LastParkingLotsUpdateKey = "FAPLastParkingLotsUpdate"

    static let sharedInstance = FAPParkingLotFacade()
    
    private init() { }
    
    func getParkingLotListFromServer(completionBlock completion:((didUpdateList: Bool, error: NSError?) -> Void)?) {
        let lastUpdate = NSUserDefaults.standardUserDefaults().objectForKey(FAPParkingLotFacade.LastParkingLotsUpdateKey) as? NSNumber ?? NSNumber(unsignedLongLong: UInt64(0))
        
        WPSClient().getParkingLotList(lastCallTimestamp: lastUpdate.unsignedLongLongValue) { response, error in
            // Só há atualização se há resposta e não há erro
            guard let response = response where error == nil else {
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion?(didUpdateList: false, error: error)
                })
                return
            }
            
            // Salva a timestamp da atualização nos user defaults
            if let timestamp = response[WPSClientConstants.JSONKeys.TimestampKey] as? NSNumber {
                NSUserDefaults.standardUserDefaults().setObject(timestamp, forKey: FAPParkingLotFacade.LastParkingLotsUpdateKey)
            }
            
            /*
             * Cria um vetor de ParkingLot a partir de dicionários retornados da WPS, remove
             * os dados atuais do banco de dados, adiciona o vetor no banco de dados e comunica
             * ao completionBlock a atualização de dados
             */
            if let parkingLotsDictionaries = response[WPSClientConstants.JSONKeys.ParkingLotsKey] as? [[String: AnyObject]] {
                var parkingLots = [FAPParkingLot]()
                for dictionary in parkingLotsDictionaries {
                    parkingLots.append(FAPParkingLot(wpsClientDictionary: dictionary))
                }
                
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(realm.objects(FAPParkingLot.self))
                    realm.add(parkingLots, update: true)
                }
                
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion?(didUpdateList: true, error: nil)
                })
            } else {
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion?(didUpdateList: false, error: nil)
                })
            }
        }
    }
    
}
