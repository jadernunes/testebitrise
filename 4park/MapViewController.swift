//
//  MapViewController.swift
//  Integracao4all
//
//  Created by Cristiano Matte on 26/10/16.
//  Copyright © 2016 Cristiano Matte. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RealmSwift
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // Views do container view de estacionamento
    @IBOutlet weak var parkingLotLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mapMarkerIcon: UIImageView!
    @IBOutlet weak var startRouteButon: UIButton!
    @IBOutlet weak var containerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    
    var locationManager: CLLocationManager!
    var lastLocation: CLLocation?
    var selectedParkingLot: FAPParkingLot?
    var containerViewIsVisible: Bool {
        return containerViewBottomConstraint.constant > 0.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Esconde o container view inicialmente
        containerViewBottomConstraint.constant = -(containerViewHeightConstraint.constant + 49.0)
        startRouteButon.layer.borderColor = UIColor(colorLiteralRed: 65.0/255.0,
                                                    green: 164.0/255.0,
                                                    blue: 240.0/255.0,
                                                    alpha: 1.0).CGColor
        
        searchBar.backgroundImage = UIImage()
        (searchBar.valueForKey("_searchField") as? UITextField)?.clearButtonMode = .Never
        searchBar.delegate = self
        
        self.configureMapView()
    }

    @IBAction func startRouteButtonTouched() {
        guard let parkingLot = selectedParkingLot else {
            return
        }
        
        // Tenta abrir a rota, em ordem de prioridade: Waze, Google Maps e Maps
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"waze://")!)) {
            UIApplication.sharedApplication().openURL(NSURL(string: "waze://?ll=\(parkingLot.latitude),\(parkingLot.longitude)&navigate=yes")!)
        } else if (UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!)) {
            UIApplication.sharedApplication().openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(parkingLot.latitude),\(parkingLot.longitude)&directionsmode=driving")!)
        } else {
            let coordinate = CLLocationCoordinate2D(latitude: parkingLot.latitude ,longitude: parkingLot.longitude)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = parkingLot.name
            
            // Se não for possível abrir no Maps, exibe alerta
            if !mapItem.openInMapsWithLaunchOptions([MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]) {
                let alert = UIAlertController(title: "Atenção",
                                              message: "Para acessar a rota, você deve possuir o Waze, Google Maps ou Maps instalado em seu celular.",
                                              preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func closeContainerViewButtonTouched() {
        containerViewBottomConstraint.constant = -(containerViewHeightConstraint.constant + 49.0)
        UIView.animateWithDuration(0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func configureMapView() {
        // Inicialmente, centraliza mapa no Brasil
        mapView.camera = GMSCameraPosition.cameraWithLatitude(-14.235, longitude: -55.925, zoom: 4)
        mapView.settings.rotateGestures = false;
        mapView.myLocationEnabled = true
        mapView.delegate = self
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else {
            locationManager.startUpdatingLocation()
        }
        
        /*
         * Desenha os marcadores de estacionamento salvos localmente, verifica se a lista de
         * estacionamentos foi atualizada e, caso positivo, atualiza os marcadores no mapa.
         */
         drawParkingLotMarkers()
        
        FAPParkingLotFacade.sharedInstance.getParkingLotListFromServer { didUpdateList, error in
            if didUpdateList {
                self.drawParkingLotMarkers()
            }
        }
    }

    func drawParkingLotMarkers() {
        mapView.clear()
        
        let realm = try! Realm()
        for parkingLot in realm.objects(FAPParkingLot.self).filter({ $0.hasMonthlyPaymentType }) {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: parkingLot.latitude, longitude: parkingLot.longitude)
            //marker.appearAnimation = kGMSMarkerAnimationPop
            marker.icon = UIImage(named: "marker")
            marker.title = "\(parkingLot.id)"
            marker.map = mapView
        }
    }
}

// MARK: - Location manager delegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // Se está autorizado a utilizar localização, ativa obtenção de localização do usuário
        if status == .AuthorizedWhenInUse || status == .AuthorizedAlways {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Centraliza na localização do usuário apenas na primeira vez que a localização é obtida
        if lastLocation == nil {
            mapView.animateToLocation(CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude))
            mapView.animateToZoom(15.0)
        }
        
        self.lastLocation = locations[0]
    }
}

// MARK: - Map view delegate

extension MapViewController: GMSMapViewDelegate {
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        if containerViewIsVisible {
            containerViewBottomConstraint.constant = -(containerViewHeightConstraint.constant + 49.0)
            UIView.animateWithDuration(0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        mapView.animateToLocation(marker.position)
        
        if let id = marker.title, parkingLot = try! Realm().objectForPrimaryKey(FAPParkingLot.self, key: Int(id)) {
            self.selectedParkingLot = parkingLot
            self.parkingLotLabel.text = parkingLot.name.uppercaseString
            self.addressLabel.text = parkingLot.address + ", " + parkingLot.neighborhood
            self.cityLabel.text = parkingLot.city
            
            // Calcula e exibe a distância do estacionamento
            if let lastLocation = self.lastLocation {
                self.distanceLabel.hidden = false
                self.mapMarkerIcon.hidden = false
                
                let distance = lastLocation.distanceFromLocation(CLLocation(latitude: parkingLot.latitude, longitude: parkingLot.longitude))
                
                // Formata a exibição da distância de acordo com seu valor
                if case 0..<1000 = distance {
                    self.distanceLabel.text = "\(Int(distance)) m"
                } else if case 1000..<100000 = distance {
                    let formatter = NSNumberFormatter()
                    formatter.numberStyle = .DecimalStyle
                    formatter.maximumFractionDigits = 2
                    formatter.minimumFractionDigits = 2
                    formatter.locale = NSLocale.currentLocale()
                    
                    self.distanceLabel.text = "\(formatter.stringFromNumber(NSNumber(double: distance/1000))!) km"
                } else {
                    self.distanceLabel.text = "\(Int(distance / 1000)) km"
                }
                
            } else {
                self.distanceLabel.hidden = true
                self.mapMarkerIcon.hidden = true
            }
            
            if !containerViewIsVisible {
                containerViewBottomConstraint.constant = 10
                UIView.animateWithDuration(0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        return true
    }
}


// MARK: - Search bar delegate

extension MapViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {

        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        
//        let navigationController = UINavigationController(rootViewController: acController)
//        navigationController.navigationBar.barStyle = .Default
//        navigationController.navigationBar.barTintColor = UIColor(red: (247.0/255.0), green: (247.0/255.0), blue: (247.0/255.0), alpha: 1)
//        navigationController.navigationBar.tintColor = Colors.Primary
//        UIApplication.sharedApplication().setStatusBarStyle(.Default, animated: true)
//        
//        self.presentViewController(navigationController, animated: true, completion: nil)
        
        self.presentViewController(acController, animated: true, completion: nil)
        
        return false
    }
}

// MARK: Autocomplete view controller delegate
extension MapViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        searchBar.text = place.name
        let cameraPosition = GMSCameraPosition.cameraWithTarget(place.coordinate, zoom: 15.0)
        
//        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        self.dismissViewControllerAnimated(true, completion: {
            self.mapView.animateToCameraPosition(cameraPosition)
        })
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
//        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func wasCancelled(viewController: GMSAutocompleteViewController) {
//        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        searchBar.text = nil
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
