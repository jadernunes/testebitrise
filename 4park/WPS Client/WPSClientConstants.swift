//
//  WPSClientConstants.swift
//  4all Park
//
//  Created by Cristiano Matte on 14/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

class WPSClientConstants {
    
    static var BaseURL = "https://indigo.parkingplus.com.br/parkingplusenterprise2"

    struct Methods {
        static let GetParkingLotList = "/servicos/integracaoportalmensalista/listargaragens"
    }
    
    struct JSONKeys {
        static let LastCallKey = "ultimachamada"
        static let TimestampKey = "timestamp"
        static let ParkingLotsKey = "garagens"
        static let MonthlyPaymentTypeKey = "tiposMensalidade"
        static let SessionIdKey = "idSessao"
        static let ParkingSpotsKey = "vagas"
        
        struct ParkingLot {
            static let IdKey = "idGaragem"
            static let NameKey = "nome"
            static let CityKey = "cidade"
            static let NeighborhoodKey = "bairro"
            static let StateKey = "ufEstado"
            static let LatitudeKey = "latitude"
            static let LongitudeKey = "longitude"
            static let AddressKey = "endereco"
        }
    }
    
}
