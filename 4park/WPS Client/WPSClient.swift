//
//  WPSClient.swift
//  4all Park
//
//  Created by Cristiano Matte on 14/06/16.
//  Copyright © 2016 4all. All rights reserved.
//

import Alamofire

class WPSClient {
    
    func getParkingLotList(lastCallTimestamp timestamp:UInt64, completionHandler:([String: AnyObject]?, NSError?) -> Void) {
        let parameters: [String:AnyObject] = [WPSClientConstants.JSONKeys.LastCallKey: NSNumber(unsignedLongLong: timestamp)]
        
        Alamofire.request(.GET, "\(WPSClientConstants.BaseURL)\(WPSClientConstants.Methods.GetParkingLotList)", parameters: parameters)
            .validate()
            .responseJSON { response in
                guard let urlResponse = response.response else {
                    completionHandler(nil, response.result.error as NSError?)
                    return
                }
                
                switch urlResponse.statusCode {
                case 200..<300:
                    completionHandler(response.result.value as? [String: AnyObject], nil)
                case 304:
                    completionHandler(nil, nil)
                default:
                    completionHandler(nil, response.result.error as NSError?)
                }
            }
    }
    
}
